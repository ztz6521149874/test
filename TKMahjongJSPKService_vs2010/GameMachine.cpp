#include ".\gamemachine.h"
#include "TKMahjongGame.h"
#include "TKMahjongServerObj.h"
#include "GameState.h"
#include "tchar.h"
#include "GameRule.h"
#include "tkbasefunc.h"

// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
CGameMachine::CGameMachine( CTKMahjongGame * pGame, CTKMahjongServerObj * pObj ) : m_pMahjongGame( pGame ), m_pMahjongObj( pObj )
{
	m_pFinalState = NULL;
}



// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
CGameMachine::~CGameMachine(void)
{
}



// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
BOOL CGameMachine::OnInitalUpdate( CGameRule * pRule )
{
	// 游戏规则
	m_pRule = pRule;

	// 根据规则取得事件-状态转换图文件名
	char szFSMFile[ _MAX_PATH ] = TEXT( ".\\mjstate-jspk.ini" ) ;
	return AssembleState(szFSMFile);
}



bool CGameMachine::AddState( std::string szStateName, IState * pState )
{
	if ( !CFSMachine::AddState( szStateName, pState ) )
	{
		return false;
	}

	IGameState * pGameState = ( IGameState * )pState;
	pGameState->m_pMahjongGame = m_pMahjongGame;
	pGameState->m_pMahjongObj = m_pMahjongObj;

	return true;
}

const int MAX_CHANGE_EVENT_NUM = 7;
struct StStateInit
{
	char* name;
	char* changeStateEventList[MAX_CHANGE_EVENT_NUM];
};

const static StStateInit g_stateList[] = {
	"CPrepareState",	      	       {"EVENT_START_GAME,CShuffleState"},
	"CShuffleState",			       {"EVENT_SHUFFLE_END,CCastDiceState"},
	"CCastDiceState",			       {"EVENT_CASTDICE_END,COpenDoorState"},
	"COpenDoorState",			       {"EVENT_OPENDOOR_END,CChangeAllFlowerState"},
	"CChangeAllFlowerState",	       {"EVENT_CHANGEALLFLOWER_END,CTianTingState"},
	"CTianTingState",	               {"EVENT_TIANTING_END,CDiscardTileState"},
	"CDiscardTileState",	           {"EVENT_DISCARD_TILE,CWaitState",
	                                    "EVENT_CALL,CWaitState",
										"EVENT_WIN_SELF,CWinState",
										"EVENT_BUGANG,CDrawTileAfterGangState",
										"EVENT_GANG,CDrawTileAfterGangState",
										"EVENT_CHANGE_FLOWER,CDrawTileAfterChangeFlowerState",
										"EVENT_DOUBLE,CDiscardTileState"},
	"CWaitState",				       {"EVENT_PENG,CDiscardAfterChiPengState",
	                                    "EVENT_CHI,CDiscardAfterChiPengState",
										"EVENT_ALLABORT,CDrawTileState",
										"EVENT_GANG,CDrawTileAfterGangState",
										"EVENT_WIN_PAO,CWinState",
										"EVENT_DOUBLE,CWaitState"},
	"CDrawTileState",		           {"EVENT_DRAWSUCCEED,CDiscardTileState",
	                                    "EVENT_NOTILE,CResultState"},
	"CDrawTileAfterGangState",	       {"EVENT_DRAWSUCCEED,CDiscardTileState",
	                                    "EVENT_NOTILE,CResultState"},
	"CDrawTileAfterChangeFlowerState", {"EVENT_DRAWSUCCEED,CDiscardTileState",
	                                    "EVENT_NOTILE,CResultState"},
	"CDiscardAfterChiPengState",	   {"EVENT_DISCARD_TILE,CWaitState",
	                                    "EVENT_CALL,CWaitState"},
	"CWinState",		               {"EVENT_LUCKY_TILE,CLuckyTileState"},
	"CLuckyTileState",                 {"EVENT_3PLAYER_NOT_WIN,CResultState",
	                                    "EVENT_2PLAYER_NOT_WIN,CResultState",
	                                    "EVENT_1PLAYER_NOT_WIN,CResultState"},
	"CResultState",                    {},
};

// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
BOOL CGameMachine::AssembleState( char * szFSMFile )
{
	for (int i = 0; i < (int)(sizeof(g_stateList) / sizeof(g_stateList[0])); i++)
	{
		IState * pState = CreateState(g_stateList[i].name);
		if (NULL == pState)
		{
			// 严重错误
			//WRITELOG( "错误:根据文件( %s )创建状态类( %s )失败,有可能是该类未注册,或文件有误, %s, %d", szFSMFile, szBuffer, __FILE__, __LINE__ );;
			return FALSE;
		}

		AddState(g_stateList[i].name, pState);
		// 设置最终状态
		m_pFinalState = pState;


		// 该类响应的事件
		for (int j = 0; j < MAX_CHANGE_EVENT_NUM; j++)
		{
			if (g_stateList[i].changeStateEventList[j] == nullptr)
			{
				break;
			}
			TCHAR szEvent[128], szNextState[128];
			TKInfoStringDetach(',', g_stateList[i].changeStateEventList[j], 2, szEvent, szNextState);

			int nEvent = EventString2Int(szEvent);
			if (0 == nEvent)
			{
				//WRITELOG( "错误:读取到的事件有误, IniFile( %s ), Section( %s ), Key( %s ), Value( %s ), %s, %d", 
				//	szFSMFile, szSection, szKey, szBuffer, __FILE__, __LINE__ );
				return false;
			}
			pState->AddEvent(nEvent, szNextState);
		}
	}

	// 设置初始状态
	if (!SetState("CPrepareState"))
	{
		TKWriteLog( "错误:根据设置g_stateList初始状态失败, 该状态可能未注册或不是合法状态中的一个或文件拼写有误");
		return false;
	}

	return true;
}



// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
int CGameMachine::EventString2Int( TCHAR szEvent[] )
{
	if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_BASEINFO" ) ) )
	{
		return EVENT_BASEINFO;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_VERSION" ) ) )
	{
		return EVENT_VERSION;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_FAN_TREE" ) ) )
	{
		return EVENT_FAN_TREE;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_PLACE" ) ) )
	{
		return EVENT_PLACE;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_WIND_PLACE" ) ) )
	{
		return EVENT_WIND_PLACE;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_REQ_RESULT" ) ) )
	{
		return EVENT_REQ_RESULT;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_ACK_RESULT" ) ) )
	{
		return EVENT_ACK_RESULT;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_SHUFFLE" ) ) )
	{
		return EVENT_SHUFFLE;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_CAST_DICE" ) ) )
	{
		return EVENT_CAST_DICE;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_REQ_OPEN_DOOR" ) ) )
	{
		return EVENT_REQ_OPEN_DOOR;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_ACK_OPEN_DOOR" ) ) )
	{
		return EVENT_ACK_OPEN_DOOR;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_HUN" ) ) )
	{
		return EVENT_HUN;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_REQ_REQUEST" ) ) )
	{
		return EVENT_REQ_REQUEST;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_ACK_REQUEST" ) ) )
	{
		return EVENT_ACK_REQUEST;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_CHANGE_FLOWER" ) ) )
	{
		return EVENT_CHANGE_FLOWER;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_DISCARD_TILE" ) ) )
	{
		return EVENT_DISCARD_TILE;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_DRAW_TILE" ) ) )
	{
		return EVENT_DRAW_TILE;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_CALL" ) ) )
	{
		return EVENT_CALL;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_REQ_CHI" ) ) )
	{
		return EVENT_REQ_CHI;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_REQ_DOUBLE") ) )
	{
		return EVENT_REQ_DOUBLE;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_REQ_PENG" ) ) )
	{
		return EVENT_REQ_PENG;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_REQ_GANG" ) ) )
	{
		return EVENT_REQ_GANG;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_REQ_WIN" ) ) )
	{
		return EVENT_REQ_WIN;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_ACK_WIN" ) ) )
	{
		return EVENT_ACK_WIN;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_NOTIFY" ) ) )
	{
		return EVENT_NOTIFY;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_START_GAME" ) ) )
	{
		return EVENT_START_GAME;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_SHUFFLE_END" ) ) )
	{
		return EVENT_SHUFFLE_END;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_CASTDICE_END" ) ) )
	{
		return EVENT_CASTDICE_END;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_OPENDOOR_END" ) ) )
	{
		return EVENT_OPENDOOR_END;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_DECIDEHUN_END" ) ) )
	{
		return EVENT_DECIDEHUN_END;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_NOTILE" ) ) )
	{
		return EVENT_NOTILE;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_CHANGEALLFLOWER_END" ) ) )
	{
		return EVENT_CHANGEALLFLOWER_END;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_TIANTING_END" ) ) )
	{
		return EVENT_TIANTING_END;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_BUGANG" ) ) )
	{
		return EVENT_BUGANG;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_WINGANG" ) ) )
	{
		return EVENT_WINGANG;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_NOWINGANG" ) ) )
	{
		return EVENT_NOWINGANG;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_ALLABORT" ) ) )
	{
		return EVENT_ALLABORT;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_CHI" ) ) )
	{
		return EVENT_CHI;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_DOUBLE") ) )
	{
		return EVENT_DOUBLE;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_PENG" ) ) )
	{
		return EVENT_PENG;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_GANG" ) ) )
	{
		return EVENT_GANG;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_WIN_PAO" ) ) )
	{
		return EVENT_WIN_PAO;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_WIN_SELF" ) ) )
	{
		return EVENT_WIN_SELF;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_DRAWSUCCEED" ) ) )
	{
		return EVENT_DRAWSUCCEED;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_1PLAYER_NOT_WIN" ) ) )
	{
		return EVENT_1PLAYER_NOT_WIN;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_2PLAYER_NOT_WIN" ) ) )
	{
		return EVENT_2PLAYER_NOT_WIN;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_3PLAYER_NOT_WIN" ) ) )
	{
		return EVENT_3PLAYER_NOT_WIN;
	}
	else if ( 0 == _tcscmp( szEvent, TEXT( "EVENT_LUCKY_TILE" ) ) )
	{
		return EVENT_LUCKY_TILE;
	}

	return 0;
}



// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
BOOL CGameMachine::ParseProperty( char * szProperty )
{
	return true;
}



// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
BOOL CGameMachine::OnPlayerMsg( DWORD dwUserID, PTKHEADER pMsg )
{
	
	return TRUE;
}

 

// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
BOOL CGameMachine::Stop()
{
	if ( NULL != m_pFinalState )
	{
		m_pCurrentState = m_pFinalState;
		//CEvent event( EVENT_1PLAYER_NOT_WIN, 0, NULL );
		CEvent event( EVENT_LUCKY_TILE, 0, NULL );
		m_pCurrentState->OnEnter( &event );

		return true;
	}

	return FALSE;
}



// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
BOOL CGameMachine::OnResetDump()
{
	if ( NULL == m_pCurrentState )
	{
		TKWriteLog( "ResetDump, 发现状态机异常，当前状态为空");
		return true;
	}

	TKWriteLog( "ResetDump, 当前状态( %s )", m_pCurrentState->Name().c_str() );
	if ( m_vszErrorMsg.size() != 0 )
	{
		TKWriteLog( "ResetDump, 错误消息列表：" );
		std::vector< std::string >::iterator it;
		for ( it = m_vszErrorMsg.begin(); it != m_vszErrorMsg.end(); ++it )
		{
			TKWriteLog( "ResetDump, %s", ( *it ).c_str() );
		}
	}

	return true;
}

