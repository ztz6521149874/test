#include <assert.h>

#ifndef ASSERT
#define ASSERT	assert
#endif

//#include <algorithm>
//#include <winsock2.h>
//#include <windows.h>

#include "ScoreCalculator.h"	// CScoreCalculator
#include "GameRule.h"

// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CScoreCalculator::CScoreCalculator()
{
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CScoreCalculator::~CScoreCalculator()
{
	ExitInstance() ;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CScoreCalculator::OnResetData()
{
	m_OweScore.clear() ;	// 清除这一盘的欠分信息
}

int CScoreCalculator::GetLastAddScore( int nSeat )
{
	ASSERT( IsValidSeat( nSeat ) ) ;
	if( m_Score.empty() )
	{ // 如果得分为空
		return 0 ;
	}

	PONETIMESCORE pScore = &m_Score.back() ;
	return pScore->pnScore[ nSeat ] ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
int CScoreCalculator::GetAwardScore( int nSeat )
{
	ASSERT( IsValidSeat( nSeat ) ) ;
	int nScore = 0 ;
	PONETIMESCORE pScore = NULL ;
	ScoreIterator iter ;
	for( iter = m_Score.begin() ; iter != m_Score.end() ; iter ++ )
	{ // 遍历所有的计分
		pScore = &( *iter ) ;
		if( pScore->nSeat != nSeat ) continue ;	// 不是这个座位号的玩家得分

		if( pScore->nStyle != 1 ) continue ;	// 不是只有这一个玩家加分

		nScore += pScore->pnScore[ nSeat ] ;
	}

	return nScore ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CScoreCalculator::GetPlayerScore( TKREQMAHJONGRESULT & reqMahJongResult )
{
	for( int i = 0 ; i < MAX_PLAYER_COUNT ; i ++ )
	{ // 遍历所有的玩家
		if ( 0 == m_pnPlayerMulti[ i ] )
		{
			// 这个座位上没人
			continue;
		}

		reqMahJongResult.asPlayerResult[ i ].nScore = m_pnPlayerLeftScore[ i ] - m_pnPlayerScore[ i ] ;
		reqMahJongResult.asPlayerResult[ i ].nTotalScore = m_pnPlayerLeftScore[ i ];
	}
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CScoreCalculator::AdjustScore( PONETIMESCORE pScore )
{
	OWESCORE owe = { 0 } ;	// 欠分信息

	if( 1 == pScore->nStyle )
	{ // 只有一个人加分
		ASSERT( IsValidSeat( pScore->nSeat ) ) ;

		int nSeat = pScore->nSeat ;	// 加分的玩家
		int nScore = pScore->pnScore[ nSeat ] ;	// 加的分
		ASSERT( nScore > 0 ) ;
		m_pnPlayerLeftScore[ nSeat ] += pScore->pnScore[ nSeat ] ;
	}
	else if( 2 == pScore->nStyle )
	{ // 只有一个人扣分
		ASSERT( IsValidSeat( pScore->nSeat ) ) ;

		int nSeat = pScore->nSeat ;	// 扣分的玩家
		int nScore = -pScore->pnScore[ nSeat ] ;
		ASSERT( nScore > 0 ) ;
		m_pnPlayerLeftScore[ nSeat ] -= nScore ;
	}
	else if( 3 == pScore->nStyle )
	{ // 这时，大家输赢和为0
		ASSERT( IsValidSeat( pScore->nSeat ) ) ;
		/*FOR TWO PLAYER*/
		ASSERT( pScore->pnScore[ 0 ] + pScore->pnScore[ 1 ] == 0 ) ;
		int nSeat = pScore->nSeat ;	// 加分的玩家
		int nScore = 0 ;
		for( int i = 0 ; i < MAX_PLAYER_COUNT ; i ++ )
		{ // 遍历所有的玩家
			if( i == nSeat ) continue ;	// 略过加分的玩家
			if( 0 == pScore->pnScore[ i ] ) continue ;	// 略过没有输分的玩家
			if ( 0 == m_pnPlayerMulti[ i ] )
			{
				// 这个座位上没人
				continue;
			}

			ASSERT( pScore->pnScore[ i ] < 0 ) ;	// 这个玩家应该是输分了（只有一个加分的玩家）
			nScore = -pScore->pnScore[ i ] ;	// 这个玩家输的分
			m_pnPlayerLeftScore[ i ] -= nScore ;
			m_pnPlayerLeftScore[ nSeat ] += nScore ;
		}
	}
}




// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CScoreCalculator::AddScore( TKACKMAHJONGWINDETAILEX & reqMahjongWin, int anShowSeat[], int cnShowSeat )
{
	int nWinSeat = reqMahjongWin.nWinSeat;
	int nPaoSeat = reqMahjongWin.nPaoSeat;
	ASSERT( IsValidSeat( reqMahjongWin.nWinSeat ) ) ;
	ASSERT( IsValidSeat( reqMahjongWin.nPaoSeat ) ) ;

	// 必须要初始化
	reqMahjongWin.nBaseScore = m_nMulti;
	memset( reqMahjongWin.asScoreDetail, 0, sizeof( reqMahjongWin.asScoreDetail ) );

	if ( nWinSeat == nPaoSeat )
	{
		// 自摸和
		for ( int i = 0; i < MAX_PLAYER_COUNT; ++i )
		{
			if ( i == nWinSeat || 0 == m_pnPlayerMulti[ i ] )
			{
				continue;
			}

			// 每个人输 ( 番数+8 ) * 基数 的分数
			//FOR TWO PLAYER 去掉8番底
//			int nScore = ( reqMahjongWin.sWinInfo.nScoreOfFan + 8 ) * m_nMulti;
			int nScore = reqMahjongWin.sWinInfo.nScoreOfFan * m_nMulti * m_nDouble;
			reqMahjongWin.asScoreDetail[ i ].anScore[ 5 ] -= nScore;
			reqMahjongWin.asScoreDetail[ nWinSeat ].anScore[ 5 ] += nScore;
		}
	}
	else
	{
		// 点炮和
		for ( int i = 0; i < MAX_PLAYER_COUNT; ++i )
		{
			if ( i == nWinSeat || 0 == m_pnPlayerMulti[ i ] )
			{
				continue;
			}

			if ( i == nPaoSeat )
			{
				// 点炮者还要输一个番数
				int nScore = 0;
				nScore = reqMahjongWin.sWinInfo.nScoreOfFan * m_nMulti * m_nDouble;
				reqMahjongWin.asScoreDetail[ i ].anScore[ 5 ] -= nScore;
				reqMahjongWin.asScoreDetail[ nWinSeat ].anScore[ 5 ] += nScore;
			}
		}
	}

	// 计算所有玩家实际分数
	for ( int i = 0; i < MAX_PLAYER_COUNT; ++i )
	{
		if ( 0 == m_pnPlayerMulti[ i ] || i == nWinSeat )
		{
			// 没人
			continue;
		}

		// 实际输分
		int nLossScore = -reqMahjongWin.asScoreDetail[ i ].anScore[ 5 ];
		ASSERT( nLossScore >= 0 );

        if ( m_pRule->GetRule( RULE_SCORE_LIMIT ) 
            && !(m_pRule->GetRule(RULE_SCORE_LOWER_LIMIT) == 1
            && (m_pTKGame->m_nScoreType == TK_GAME_SCORETYPE_CHIP
            || m_pTKGame->m_nScoreType == TK_GAME_SCORETYPE_LIFE)))
		{
			// 有上下封顶限制
			// 不能输成负分
			nLossScore = min( nLossScore, m_pnPlayerScore[ i ] );
			// 也不能超过和牌玩家拥有的筹码数
			nLossScore = min( nLossScore, m_pnPlayerScore[ nWinSeat ] );
            reqMahjongWin.asScoreDetail[ i ].anScore[ 6 ] = -nLossScore;
		}
        else
        {
            reqMahjongWin.asScoreDetail[ i ].anScore[ 6 ] = -nLossScore;
        }

		// 累计和牌人实际得分
		reqMahjongWin.asScoreDetail[ nWinSeat ].anScore[ 6 ] += nLossScore;
	}
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************


