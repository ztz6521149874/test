#pragma once
#include "tkmahjongobj.h"
#include <vector>


// *********************************************************************************************************************
//
// 服务器使用的麻将类，这个类实现服务器这边算法相关的所有操作
//
//
// *********************************************************************************************************************
class CTKMahjongGame;
class CTKMahjongServerObj : public CTKMahjongObj
{
public:
	// 构造函数
	CTKMahjongServerObj( CTKMahjongGame * pGame ) ;
	// 析构函数
	virtual ~CTKMahjongServerObj() ;

public:
    // 每一局开始时，重置游戏数据
    virtual void ResetGameData() ;

	// 填充玩家的开牌信息
    void FillOpenDoorMsg(int nSeat, int nAwardSeat, int nFlowerCount, PTKREQMAHJONGOPENDOOR pReqMahJongOpenDoor);

	// 随机的从剩下的牌中选一张牌
	/// 选择nColor这种花色的牌，如果为-1表示随便选择一个花色；
	PMAHJONGTILECOUNTINFO RandomSelectOneLeftTile( int nColor = -1, int avoidColor1 = -1, int avoidColor2 = -1 ) ;

    // 判断玩家是否可以和牌
	BOOL CheckWin( PTKREQMAHJONGWIN pReqMahJongWin ) ;
	// 判断断线玩家是否胡牌，是则填写胡牌消息
	CHECKRESULT CheckBotWin( int seat, bool ziMo, int huId, PTKREQMAHJONGWIN pReqMahJongWin ); 

	// 计算各玩家得分
	/// 返回有底分和番种的分（不包括混杠的分，并且这些分也不包括倍分信息）
	void CalculateScore( TKREQMAHJONGRESULT &sMahJongResult , int& nBaseScore , int& nFanScore ) ;

	// 用剩下可用的牌随机的替换掉某个玩家手上指定的n张排
	BOOL RandomChangeHandTile( int nSeat, int anOldTile[], int anNewTile[], int cnChangeTile );

	// 返回某个玩家刚摸的那张牌
	CMahJongTile * GetDrawTile( int nSeat );


	// 检查牌墙中某个位置的牌，并返回这张牌的ID号
	int CheckWallTileID( BOOL bFromWallHeader , int nOffset ) ;

	// nSeat玩家抓一张牌,如果没牌了返回0,成功返回所抓的牌的ID号
	int PlayerDrawOneTile( int nSeat , int nDrawTileType , BOOL bFromWallHeader , int nOffset );

	// 玩家吃牌
	void PlayerChi( PTKREQMAHJONGCHI pReqMahJongChi );

	// 玩家碰牌
	void PlayerPeng( PTKREQMAHJONGPENG pReqMahJongPeng );

	// 玩家杠牌
	void PlayerGang( PTKREQMAHJONGGANG pReqMahJongGang, bool bAfterPeng = false );

	// 计算杠牌得分
	int CalcGangScore(int nSeat);

	// 玩家和牌
	void PlayerWin( PTKREQMAHJONGWIN pReqMahJongWin ) ;

	// 玩家请示出牌
	bool PlayerReqDiscardTile( PTKREQMAHJONGDISCARDTILE pReqMahJongDiscardTile );

	// 玩家请求听牌并出牌
	bool PlayerReqCallAndDiscardTile( PTKREQMAHJONGCALL pReqMahJongCall );

    void _sendOtherTile(int nSeat);


	// 发玩家吃碰杠的牌
	bool SendMsgShowTiles( int seat, bool toWatch = false );

	// 玩家请求和牌
	bool PlayerReqWin( PTKREQMAHJONGWIN pReqMahJongWin, bool setAck = true );

	// 玩家请求吃牌
	bool PlayerReqChi( PTKREQMAHJONGCHI pReqMahJongChi );

	// 玩家请求和牌
	bool PlayerReqDbl( PTKREQMAHJONGDBL pReqMahjongDbl );

	// 玩家请求碰牌
	bool PlayerReqPeng( PTKREQMAHJONGPENG pReqMahJongPeng );

	// 玩家请求杠牌(杠其它玩家打的牌)
	bool PlayerReqGang( PTKREQMAHJONGGANG pReqMahJongGang, bool bSelfGang, bool bAfterPeng = false  );

	// 玩家托管
	bool PlayerTrustPlay( int nSeat, int nTrustType );

	//取消托管
	void CancelTrustPlay( int nSeat );

	// 发送其它玩家放弃操作消息
	void SendGiveUpRequestMsg( int nSeat , int nRequestType , int nParam );

	// 指定的几张牌是同一个ID
	bool IsNoSameTileID( int anTileID[], int cnTile );

	// 添加一个和牌信息
	void AddWinInfo( TKACKMAHJONGWINDETAILEX & win );

	// 取得所有和牌信息
	int GetWinInfo( TKACKMAHJONGWINDETAILEX asWin[] );
	// 清除所有胡牌信息
	bool ClearWinInfo();

	BOOL OnResetDump();

	// 验听信息
	void CheckCall( int seat );

    bool SetCallInfo
    (
        int in_nSeat, 
        int in_nTakeoutTileID, 
        int in_nCallType
    );

	int GetLeftTileCount( int nTileID );
	void CountHandTile( int seat );
	bool IsTwoTing( );
	void AckDouble( int seat );
	bool CheckCanDouble( int seat );
	bool GetTingNum( int seat, int &tingNum );
	void CheckCallResult( int seat, CALLINFOVECTOR ** pvsCheckCallInfo );
	void ResetDoubleData( );
	int  GetAllTingCount( int seat );

	void MemCallDisID( int seat, int id );

	void SendDblTrip( int seat );

	// 重置听牌信息
	void ResetCallInfo();
	// 检测用户是否还有可胡的牌
	bool CheckCallTile();

	//判断玩家是否可以和牌
	bool CheckCanWin( PTKREQMAHJONGWIN pReqMahJongWin, int nSeat, int nPaoSeat, CMahJongTile* nTile ) ;
	bool CheckCanZimo(const int& nSeat, const int& nTile);
	// 发送加番牌信息
	void SendSpecialTileInfo();

    bool CheckBigFlowerAward(int& nWaitSecond);
	bool CheckFullBloomAward(int& nWaitSecond);

    void _awardToPlayer(int nSeat, int nType, int nCount);
    void _sendBigFlowerLamp(int nSeat);
    void _sendBigFlowerAward(int nSeat, int nIsOver);

	void _sendFullBloomLamp(int nSeat);
	void _sendFullBloomAward(int nSeat);


	// 获取加番牌个数
	int GetSpecialCount(CHECKPARAM &sCheckParam);

	// 获取奖花牌
	int GetLuckyTile(TKACKMAHJONGLUCKYTILE & stLuckyTile);
	// 发送奖花消息
	void SendLuckyTileInfo(PTKACKMAHJONGLUCKYTILE pAckMsg);

	int GenerateRandomTileId(std::vector<int>& vUsedTileId, int nTotal);

protected:
	// 创建玩家对象
	/// nSeat是这个玩家的座位号
	virtual CTKMahjongPlayer* CreatePlayer(int nSeat, CTKGamePlayer * pPlayer) ;

	// 创建Judge
	virtual IJudge * CreateJudge( int nMahjongType );

	// 创建牌墙
	virtual ITileWall * CreateTileWall( int nMahjongType );

private :
	// 给所有人发送某个座位玩家牌的信息
	/// 给nSeat上的玩家发真的信息pvMsgTrue, 其它座位的玩家及旁观者发假的信息pvMsgFalse
	void SendSeatTileInfo( int nSeat , PTKHEADER pvMsgTrue , PTKHEADER pvMsgFalse );

	// 发送消息给某个座位的玩家
	BOOL Send2SeatPlayer( int nSeat, PTKHEADER pMsg );

	// 发送消息给旁观者
	void Send2Observer(PTKHEADER pMsg);

	// 广播
	BOOL Broadcast( PTKHEADER pMsg );
	bool istracedreshuchance() const;
private:
	CTKMahjongGame * m_pMahjongGame;
	typedef std::vector< TKACKMAHJONGWINDETAILEX > WININFOVECTOR;
	WININFOVECTOR m_vecWinInfo;

	CALLINFOVECTOR m_vsCheckCallInfo[MAX_PLAYER_COUNT];// 最近一次验听的详细信息(clear,init,change)
	int			  m_dblNumOne[MAX_PLAYER_COUNT];			// 对家和牌剩一张时，两人加倍次数记录(双听牌后)
	int			  m_dblNumZero[MAX_PLAYER_COUNT];			// 对家和牌为0时，两人加倍次数记录(双听牌后)
	int			  m_dblAllNum[MAX_PLAYER_COUNT];        // 两人加倍总次数
	int			  m_acnHandTile[MAX_PLAYER_COUNT][34]; 
	int			  m_nLastCallDisID[MAX_PLAYER_COUNT];   // 两个玩家听牌时所出的那张牌
	// int			m_acnHandTile[34];	   // 手中牌计数 seat = 0
	// int			m_acnHandTileEx[34];  // seat = 1

	bool		m_bCheckCallTile;
	int			m_nSpecialTileID;

public:
    std::vector<int> m_vecPengCardsIDJust;  // 最近一次碰牌牌张ID信息

};
