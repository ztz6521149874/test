#include "StdAfx.h"
#include "TKMahjongGamePlayer.h"
#include "../Common/TKMahjongProtocol.h"
#include "TKMahjongJSPKService.h"
#include "TKMatchS2GameSProtocolSrv.h"
#include "tkgame2pivotprotocol.h"
#include "TKMahjongServerPlayer.h"
#include "TKMatchAgent.h"

#include <json.h>

//////////////////////////////////////////RateTool//////////////////////////////////////////////////
RateTool::RateTool()
{
}

RateTool::~RateTool()
{
}

void RateTool::InitData(int rate0, int rate2, int rate5)
{
	//TKWriteLog( "开启宝藏牌的概率：%d %d %d", rate0, rate2, rate5);

	double rt0 = double(rate0) / double(1000);
	m_vRates.push_back(rt0);
	m_vMultiples.push_back(0);

	m_vRates.push_back(0);//该等级为客户端保留，概率为0
	m_vMultiples.push_back(1);

	double rt2 = double(rate2) / double(1000);
	m_vRates.push_back(rt2);
	m_vMultiples.push_back(2);

	double rt5 = double(rate5) / double(1000);
	m_vRates.push_back(rt5);
	m_vMultiples.push_back(5);
}

int RateTool::Random(int& multiple) const
{
	UINT nRndIndex = TKRandomChoose(m_vRates);
	if (nRndIndex < m_vRates.size())
	{
		multiple = m_vMultiples[nRndIndex];
		return nRndIndex;
	}
	return -1;
}

//////////////////////////////CTKMahjongMatchSvrUser///////////////////////////////////
CTKMahjongMatchSvrUser::CTKMahjongMatchSvrUser()
{
}

CTKMahjongMatchSvrUser::~CTKMahjongMatchSvrUser()
{
}

BOOL CTKMahjongMatchSvrUser::OnSyncMsg(PTKHEADER pMsg)
{
	if (ACK_TYPE(TKID_GS2MS_USERREQHEMATINIC) == pMsg->dwType)
	{
		PTKUSERACKHEMATINIC pAck = (PTKUSERACKHEMATINIC)pMsg;

		CTKMatchAgent *pMatchAgent = tk_pGameService->GetMatchAgent(pAck->dwMatchID);
		if (pMatchAgent == NULL)
		{
			return TRUE;
		}
		CTKGamePlayer *pMatchPlayer = pMatchAgent->GetGamePlayer(pAck->dwUserID);
		if (pMatchPlayer)
		{
			CTKMahjongGamePlayer* pMahjongPlayer = dynamic_cast<CTKMahjongGamePlayer*>(pMatchPlayer);
			if (pMahjongPlayer && pAck->header.dwParam == TK_ACKRESULT_SUCCESS)
			{
				//用户在抽奖界面补血，更新比赛积分
				pMahjongPlayer->addHematinic(pAck->nHematinic);
			}
			RELREF_TKOBJECT(pMatchPlayer);
		}
		RELREF_TKOBJECT(pMatchAgent);
	}

	return __super::OnSyncMsg(pMsg);
}

//////////////////////////////CTKMahjongGamePlayer///////////////////////////////////
CTKMahjongGamePlayer::CTKMahjongGamePlayer()
	:m_nWinScore(-1)
	,m_nPickCount(0)
	,m_nUptScore(0)
	,m_nContinueRule(0)
{
}


CTKMahjongGamePlayer::~CTKMahjongGamePlayer()
{
	if (m_nPickCount > 0)
	{
		Statistic();
	}
}

BOOL CTKMahjongGamePlayer::OnBreak()
{
	ResetData();
	return CTKGamePlayer::OnBreak();
}

void CTKMahjongGamePlayer::ResetData()
{
	if (m_nPickCount > 0)
	{
		Statistic();
	}

	m_nWinScore = -1;
	m_nPickCount = 0;
	m_nUptScore = 0;

	memset(m_aRoundPay, 0, sizeof(m_aRoundPay));
	memset(m_aRoundGain, 0, sizeof(m_aRoundGain));
	// GetPivotGrow(GV_TreasureRecord);

	m_nContinueRule = 0;
	m_bLastTreasureIs0 = false;
}

void CTKMahjongGamePlayer::SetWinScore(int nScore)
{
	m_nWinScore = nScore;
}

void CTKMahjongGamePlayer::addHematinic(int nHematinic)
{
	if (m_nWinScore > 0)
	{
		m_nScore += nHematinic;
	}
}

/*
void CTKMahjongGamePlayer::Test()
{
	std::vector<int> ddd;
	for (int i = 0; i < 100; ++i)
	{
		int nMultiple = 1;
		int nLevel = RateTool::get_const_instance().random(nMultiple);
		ddd.push_back(nLevel);
	}
	
	int s = m_nScore;
}*/

void CTKMahjongGamePlayer::UpdateScore(int nUptScore)
{
	m_nWinScore += nUptScore;
	m_nScore += nUptScore;

	TKREQUPDATEUSERCHIP sUserChipReq = {0};
	sUserChipReq.header.dwType = REQ_TYPE(TKID_GS2MS_GAME_UPDATE_USERCHIP);
	sUserChipReq.header.dwLength = MSG_LENGTH(sUserChipReq);

	sUserChipReq.dwTourneyID = m_dwTourneyID;
	sUserChipReq.dwMatchID = m_dwMatchID;
	sUserChipReq.wStageID = m_wStageID;
	sUserChipReq.dwUserID = m_dwUserID;
	sUserChipReq.nChip = m_nScore;

	PostMsg2MS((PTKHEADER)&sUserChipReq);
}

void CTKMahjongGamePlayer::SendPickResult(int nLevel, int nMultiple)
{
	TKMobileAckMsg rAck;
	MahJongJSPKAckMsg *pMahJong = rAck.mutable_mahjongjspk_ack_msg();
	MahJongTreasurePickAck *pTreasurePickAck = pMahJong->mutable_mahjongtreasurepick_ack_msg();

	pTreasurePickAck->set_callrule(m_nContinueRule);
	pTreasurePickAck->set_cardlevel(nLevel);
	pTreasurePickAck->set_cardmultiple(nMultiple);

	SendToClient(rAck);
}

void CTKMahjongGamePlayer::SendToClient(TKMobileAckMsg& rMsg)
{
	MahJongJSPKAckMsg * pAckMsg = rMsg.mutable_mahjongjspk_ack_msg();

	pAckMsg->set_matchid(m_dwMatchID);
	// 序列化
	std::string szSerializeMsg;
	rMsg.SerializeToString(&szSerializeMsg);

	// 发送
	BYTE* pszPack = new BYTE[sizeof(TKHEADER) + rMsg.ByteSize()];    // 分配空间
	ZeroMemory(pszPack, sizeof(TKHEADER) + rMsg.ByteSize());

	((PTKHEADER)pszPack)->dwType = TK_ACK | TK_MSG_MAHJONG_PROTOBUF;
	((PTKHEADER)pszPack)->dwLength = rMsg.ByteSize();   // size
	BYTE* pData = pszPack + sizeof(TKHEADER);
	memcpy(pData, szSerializeMsg.c_str(), rMsg.ByteSize());

	SendMsg((PTKHEADER)pszPack);
	delete[] pszPack;
	pszPack = NULL;
}

void CTKMahjongGamePlayer::GetPivotGrow(DWORD nGrowId)
{
	msg_gs2pivot_req_get_singleGrow rReqGrow;
	memset(&rReqGrow, 0, sizeof(rReqGrow));
	rReqGrow.dwType = TKID_GS2PIVOT_GET_SINGLEGROW | TK_REQ;
	rReqGrow.dwLength = sizeof(rReqGrow) - sizeof(TKHEADER);
	rReqGrow.dwUserID = m_dwUserID;
	rReqGrow.dwGrowID = nGrowId;
	rReqGrow.dwMPID = m_nProductID;
	rReqGrow.dwGameID = m_nGameID;
	rReqGrow.dwPlatType = m_wCntTID;
	rReqGrow.byOSType = m_byOSType;
	rReqGrow.dwAppID = m_dwAppID;
	rReqGrow.dwSiteID = m_dwSiteID;

	CCallback* pCallBack = new CCallback();
	tk_pGameService->PostMsg2Pivot_GSBase(&rReqGrow, m_dwUserID, m_dwMatchID
		, m_wStageID, m_wRoundID, pCallBack);
}

void CTKMahjongGamePlayer::SavePivotGrow(DWORD nGrowId, int nValue)
{
	msg_gs2pivot_req_deal_grow rReqGrow;
	memset(&rReqGrow, 0, sizeof(rReqGrow));
	rReqGrow.dwType = TKID_GS2PIVOT_DEAL_GROW | TK_REQ;
	rReqGrow.dwLength = sizeof(rReqGrow) - sizeof(TKHEADER);
	rReqGrow.dwUserID = m_dwUserID;
	rReqGrow.dwGrowID = nGrowId;
	rReqGrow.nValue = nValue;
	rReqGrow.dwMPID = m_nProductID;
	rReqGrow.dwGameID = m_nGameID;
	rReqGrow.dwPlatType = m_wCntTID;
	rReqGrow.byOSType = m_byOSType;
	rReqGrow.dwAppID = m_dwAppID;
	rReqGrow.dwSiteID = m_dwSiteID;

	CCallback* pCallBack = new CCallback();
	tk_pGameService->PostMsg2Pivot_GSBase(&rReqGrow, m_dwUserID, m_dwMatchID, m_wStageID, m_wRoundID, pCallBack);
}

void CTKMahjongGamePlayer::Statistic()
{
	Json::Value atomData;
	atomData["TS"] = time(NULL);
	atomData["Round"] = m_nPickCount;
	atomData["MPID"] = this->m_nProductID;
	atomData["GameID"] = this->m_nGameID;

	for (int i = 0; i < m_nPickCount; ++i)
	{
		std::ostringstream key;
		key << "RoundPay" << i;
		atomData[key.str()] = m_aRoundPay[i];
	}

	for (int i = 0; i < m_nPickCount; ++i)
	{
		std::ostringstream key;
		key << "RoundGain" << i;
		atomData[key.str()] = m_aRoundGain[i];
	}

	std::string jsonStr = atomData.toStyledString();
	int nMsgLen = sizeof(TKREQOTHER2HEBBROKERPUSHDATA) + jsonStr.length();
	std::unique_ptr<BYTE, default_delete<BYTE[]>> pMsgBuffer(new BYTE[nMsgLen]);

	PTKREQOTHER2HEBBROKERPUSHDATA pStatisticData = (PTKREQOTHER2HEBBROKERPUSHDATA)(pMsgBuffer.get());
	memset(pStatisticData, 0, nMsgLen);

	pStatisticData->header.dwLength = MSG_LENGTH(TKREQOTHER2HEBBROKERPUSHDATA) + jsonStr.length();
	pStatisticData->header.dwType = REQ_TYPE(TK_MSG_OTHER2HEBBROKER_PUSHDATA);

	pStatisticData->dwPID = this->m_dwUserID;
	// pStatisticData->dwMPID = this->m_nProductID;
	pStatisticData->dwSID = 10000245;
	pStatisticData->dwDataOffset = sizeof(TKREQOTHER2HEBBROKERPUSHDATA);
	pStatisticData->dwDataSize = jsonStr.length();

	// strncpy(pStatisticData->szUserName, this->m_szNickName, sizeof(pStatisticData->szUserName));
	strncpy((char*)(pStatisticData + 1), jsonStr.c_str(), jsonStr.length());

	gtk_pMahjongJSPKService->PostMsg2Hebbroker(this, (PTKHEADER)pStatisticData);
}

BOOL CTKMahjongGamePlayer::OnMsg(PTKHEADER pMsg)
{
	if (REQ_TYPE(TK_MSG_MAHJONG_PROTOBUF) == pMsg->dwType)
	{
		size_t szLen = pMsg->dwLength + sizeof(TKHEADER);
		std::string szSerializeData((BYTE*)pMsg + sizeof(TKHEADER), (BYTE*)pMsg + szLen);

		TKMobileReqMsg mobileReqMsg;
		mobileReqMsg.ParseFromString(szSerializeData);  // 反序列化
		const MahJongJSPKReqMsg& reqMsg = mobileReqMsg.mahjongjspk_req_msg();
		if (reqMsg.has_mahjongtreasurepick_req_msg())
		{
			if (++m_nPickCount > MAX_PICK_COUNT)
			{
				TKWriteLog("player[%d] treasure pick failed, c[%d]. file[%s], line[%d].", 
					m_dwUserID, m_nPickCount, __FILE__, __LINE__);
				return FALSE;
			}

			//if (m_nContinueRule && m_bLastTreasureIs0)
			//{
			//	//开启宝藏牌的规则为上局开到不合格就不继续 && 上次开到不合格
			//	TKWriteLog("player[%d] treasure pick end （last is 0）, c[%d]. file[%s], line[%d].",
			//		m_dwUserID, m_nPickCount, __FILE__, __LINE__);
			//	return FALSE;
			//}

			const MahJongTreasurePickReq& sTreasurePickReq = reqMsg.mahjongtreasurepick_req_msg();
			int nLeftScore = std::min<int>(m_nWinScore, m_nScore);
			if (0 >= nLeftScore || sTreasurePickReq.selectedchip() > nLeftScore) //抽奖筹码不能大于剩余积分
			{
				TKWriteLog("player[%d] treasure pick failed, c[%d] l[%d] s[%u]. file[%s], line[%d].", 
					m_dwUserID, m_nPickCount, nLeftScore, sTreasurePickReq.selectedchip(), __FILE__, __LINE__);
				return FALSE;
			}
			
			int nMultiple = 0;
			int nLevel = m_pRateKit->Random(nMultiple);
			TKWriteLog("开启宝藏牌的等级：%d，倍数：%d", nLevel, nMultiple);
			
			if (nLevel < 0)
			{
				TKWriteLog("player[%d] treasure random failed, l[%d] s[%u]. file[%s], line[%d].", 
					m_dwUserID, nLevel, sTreasurePickReq.selectedchip(), __FILE__, __LINE__);
				return FALSE;
			}

			m_aRoundPay[m_nPickCount - 1] = sTreasurePickReq.selectedchip();
			m_aRoundGain[m_nPickCount - 1] = nMultiple * sTreasurePickReq.selectedchip();

			int nUptScore = m_aRoundGain[m_nPickCount - 1] - m_aRoundPay[m_nPickCount - 1];
			SavePivotGrow(GV_TreasureRecord, nUptScore); //抽奖记录
			if (nUptScore != 0)
			{
				UpdateScore(nUptScore); //修改比赛积分
			}

			SendPickResult(nLevel, nMultiple); //发送结果到客户端

			SetLastTreasureIs0( 0 == nLevel);
			return TRUE;
		}
		else if (reqMsg.has_mahjonggetscore_req_msg())
		{
			TKMobileAckMsg rAck;
			MahJongJSPKAckMsg *pMahJong = rAck.mutable_mahjongjspk_ack_msg();
			MahJongGetScoreAck *pGetScore = pMahJong->mutable_mahjonggetscore_ack_msg();
			pGetScore->set_score(m_nScore);
			SendToClient(rAck);
			return TRUE;
		}
	}

	return __super::OnMsg(pMsg);
}

