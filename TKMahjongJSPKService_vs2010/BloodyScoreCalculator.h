#pragma once

#include "IScoreCalculator.h"

class CBloodyScoreCalculator : public IScoreCalculator
{
public:
	CBloodyScoreCalculator(void);
	~CBloodyScoreCalculator(void);

public:
	// 添加和牌分（基本分也在这个函数中添加）
	virtual void AddScore( TKACKMAHJONGWINDETAILEX & reqMahjongWin, int anShowSeat[], int cnShowSeat );

	// 获得这一盘各个玩家的得分
	virtual void GetPlayerScore( TKREQMAHJONGRESULT & reqMahJongResult );

protected:
	BOOL m_abWin[ MAX_PLAYER_COUNT ];
};
