#pragma once

#include "tkgameservice.h"
#include "FSMachine.h"
#include "TKMahJongJSPK.pb.h"

using namespace cn::jj::service::msg::protocol;

// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
class CTKMahjongJSPKService : public CTKGameService
{
public:
	CTKMahjongJSPKService(void);
	~CTKMahjongJSPKService(void);

public:
	// ��ʼ��
	virtual BOOL OnInitialUpdate();

	virtual BOOL OnRegisterTKObject();
	virtual CTKGame* OnCreateGame();
	virtual CTKGamePlayer* OnCreateTKGamePlayer();
	virtual CTKUserObject *OnCreateTKUserObject(int nOrigine);
	virtual int OnCalcMatchRank(int nMatchScore);

	virtual BOOL OnTimeout(struct tm *pTime);
	virtual BOOL OnTimer(PTKOBJTIMERHEADER pTKTimer);

	BOOL CalcBotScore(int nScore);
	BOOL OutPutBotScore();

    CTKGamePlayer * GetGamePlayer(DWORD dwUserID);
    void Broadcast(TKMobileAckMsg& rMsg);

	BOOL PostMsg2Hebbroker(CTKGamePlayer* pPlayer, PTKHEADER pTkHead);
	BOOL OnAsyncSendToHebbroker(PTKHEADER msgReq, DWORD dwParam, BOOL bWaitAck);
public:
	INT64 m_nBotWinCount;
	INT64 m_nBotLoseCount;
	INT64 m_nBotWinScoreTotal;
	INT64 m_nBotLoseScoreTotal;
	
	time_t m_timeLastOutputLog;
	
	CTKObjectLock	m_clsLockCalcBotScore;

	BOOL										m_bAsyncInitial;
	CTKAsyncSendObject<CTKMahjongJSPKService>	m_clsAsyncSendObj;
	CTKConnectionPool							m_clsHebbrokerPool;
};

extern CTKMahjongJSPKService *gtk_pMahjongJSPKService;

