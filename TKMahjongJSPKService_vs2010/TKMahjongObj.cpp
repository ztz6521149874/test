#include ".\tkmahjongobj.h"
#include "gamerule.h"
#include "mahjongtile.h"
#include "judge.h"
#include "TileWall.h"
#include <boost/tokenizer.hpp>
#include "TKMahjongGame.h"
#ifndef ASSERT
#define ASSERT	assert
#endif

// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
CTKMahjongObj::CTKMahjongObj(void)
{
	m_pJudge = NULL;
	m_pTileWall = NULL;
	memset( m_apPlayer , 0 , sizeof( m_apPlayer ) ) ;
	m_cnPlayer = 0;
    m_pMahjongGame = NULL;
	m_pGameRule = NULL;

	memset( m_apAllTile , 0 , sizeof( m_apAllTile ) ) ;
	m_nTileCount = 0 ;
	//memset( m_apTileCountInfo , 0 , sizeof( m_apTileCountInfo ) ) ;

	memset( m_acnShowTile, 0, sizeof( m_acnShowTile ) );

	m_nCurDiscardSeat = 0 ;
	m_pCurDiscardTile = NULL ;

	m_nCurDrawSeat = 0 ;
	m_nCurDrawType = DRAW_TILE_TYPE_NULL ;
	m_nLastDiscardSeat = 0;

	m_nLastGangSeat = -1 ;
	m_pLastGangTile = NULL ;
	m_nLastGangType = 0 ;
	m_nLastKongSeat = -1 ;
	m_nHunGangSeat  = -1 ;

	m_nBanker    = -1 ;
	m_nRoundWind = 0 ;

	/*FOR TWO PLAYER m_abTian[0] = m_abTian[1] = m_abTian[2] = m_abTian[3] = TRUE;*/
	m_abTian[0] = m_abTian[1] = TRUE;
}

// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
CTKMahjongObj::~CTKMahjongObj(void)
{
	int i = 0 ;
	
	// 删除Judge对象
	if( NULL != m_pJudge )
	{ // Judge对象有效
		delete m_pJudge ;
		m_pJudge = NULL ;
	}


	// 删除牌墙对象
	if( NULL != m_pTileWall )
	{ // Judge对象有效
		delete m_pTileWall ;
		m_pTileWall = NULL ;
	}


	// 删除牌的信息
	for( i = 0 ; i < MAX_TILE_COUNT ; i ++ )
	{ // 遍历所有的牌
		if( NULL != m_apAllTile[ i ] )
		{ // 对象有效
			delete m_apAllTile[ i ] ;
			m_apAllTile[ i ] = NULL ;
		}
	}

	// 删除玩家信息
	for ( i = 0; i < MAX_PLAYER_COUNT; ++i )
	{
		if ( NULL != m_apPlayer[ i ] )
		{
			delete m_apPlayer[ i ];
		}
	}
	memset( m_apPlayer, 0, sizeof( m_apPlayer ) );

	// 删除备用的牌
	MAHJONGTILEVECTOR::iterator it;
	for ( it = m_vecOccasionalTile.begin(); it != m_vecOccasionalTile.end(); it++ )
	{
		delete ( *it );
	}
	m_vecOccasionalTile.clear();
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CTKMahjongObj::ResetGameData()
{
	int i = 0 ;

	// 重置玩家的信息
	CTKMahjongPlayer *pPlayer = NULL ;
	for( i = 0 ; i < MAX_PLAYER_COUNT ; i ++ )
	{ // 遍历每个玩家
		if ( HasPlayer( i ) )
		{
			pPlayer = GetPlayerBySeat( i ) ;
			pPlayer->ResetGameData() ;
		}
	}

	// 重置每张牌的信息
	for( i = 0 ; i < MAX_TILE_COUNT ; i ++ )
	{ // 遍历所有的牌
		ASSERT( NULL != m_apAllTile[ i ] ) ;
		m_apAllTile[ i ]->ResetGameData() ;
	}

	// 删除备用的牌
	MAHJONGTILEVECTOR::iterator it;
	for ( it = m_vecOccasionalTile.begin(); it != m_vecOccasionalTile.end(); it++ )
	{
		delete ( *it );
	}
	m_vecOccasionalTile.clear();

	// 重置牌墙信息
	m_pTileWall->ResetGameData() ;

	// 重置每张牌的使用情况
	for( i = 0 ; i < sizeof( m_apTileCountInfo ) / sizeof( m_apTileCountInfo[ 0 ] ) ; i ++ )
	{ // 遍历每个牌
		m_apTileCountInfo[ i ].vUsed.clear();	// 每张牌都还没有用
		// 有可能会有换牌，这时候会增加每张牌的总张数
		m_apTileCountInfo[ i ].nTotal = ( 1 == m_apTileCountInfo[ i ].nTotal ) ? 1 : 4 ;
	}
	int nTileType = m_pGameRule->GetRule( RULE_TILETYPE );
	/// 万
	if( 0 == ( TILE_TYPE_BIT_WAN & nTileType ) )
	{ // 没有万
		for( i = 0 ; i < 9 ; i ++ )
		{ // 万
			m_apTileCountInfo[ i ].nTotal = 0 ;
		}
	}
	/// 条
	if( 0 == ( TILE_TYPE_BIT_TIAO & nTileType ) )
	{ // 没有条
		for( i = 9 ; i < 18 ; i ++ )
		{ // 条
			m_apTileCountInfo[ i ].nTotal = 0 ;
		}
	}
	/// 饼
	if( 0 == ( TILE_TYPE_BIT_BING & nTileType ) )
	{ // 没有饼
		for( i = 18 ; i < 27 ; i ++ )
		{ // 饼
			m_apTileCountInfo[ i ].nTotal = 0 ;
		}
	}
	/// 东南西北
	if( 0 == ( TILE_TYPE_BIT_WIND & nTileType ) )
	{ // 没有东南西北
		for( i = 27 ; i < 31 ; i ++ )
		{ // 东南西北
			m_apTileCountInfo[ i ].nTotal = 0 ;
		}
	}
	/// 中发白
	if( 0 == ( TILE_TYPE_BIT_JIAN & nTileType ) )
	{ // 没有中发白
		for( i = 31 ; i < 34 ; i ++ )
		{ // 中发白
			m_apTileCountInfo[ i ].nTotal = 0 ;
		}
	}
	// 目前需要支持可自定义配置花牌个数功能，对原有规则解析进行修改 addby zhangws 20180621
	int nChangeFlowerCount = m_pGameRule->GetRule(RULE_CF_COUNT);
	if (nChangeFlowerCount > 8) //最多只支持8张花牌
		nChangeFlowerCount = 8;
	/// 春夏秋冬
	if (nChangeFlowerCount <= 0 )
	{
		// 没有梅兰竹菊
		for( i = 34 ; i < 38 ; i ++ )
		{ 
			m_apTileCountInfo[ i ].nTotal = 0 ;
		}
		// 没有春夏秋冬
		for( i = 38 ; i < 42 ; i ++ )
		{
			m_apTileCountInfo[ i ].nTotal = 0 ;
		}
	}
	else if (nChangeFlowerCount > 0 && nChangeFlowerCount <= 4)
	{
		// 没有梅兰竹菊中的一个或多个
		for (i = 34 + nChangeFlowerCount; i < 38; i ++)
		{
			m_apTileCountInfo[ i ].nTotal = 0 ;
		}

		// 没有春夏秋冬
		for( i = 38 ; i < 42 ; i ++ )
		{
			m_apTileCountInfo[ i ].nTotal = 0 ;
		}
	}
	else if (nChangeFlowerCount > 4)
	{
		// 没有春夏秋冬的0种或多种
		for( i = 38 + nChangeFlowerCount - 4 ; i < 42 ; i ++ )
		{
			m_apTileCountInfo[ i ].nTotal = 0 ;
		}
	}
	memset( m_acnShowTile , 0 , sizeof( m_acnShowTile ) ) ;

	m_nCurDiscardSeat = -1 ;	// 当前没有出牌的玩家
	m_pCurDiscardTile = NULL ;	// 没有出的牌
	m_nLastDiscardSeat = -1;
	m_nCurDrawSeat    = -1 ;	// 当前没有抓牌的玩家
	m_nCurDrawType    = DRAW_TILE_TYPE_NULL ;	// 没有抓牌类型
	m_nLastGangSeat   = -1 ;	// 没有杠牌的玩家
	m_pLastGangTile   = NULL ;	// 没有杠的牌
	m_nLastGangType   = 0 ;		// 没有杠牌的类型
	m_nLastKongSeat   = -1 ;	// 最后一个杠牌没有点杠的玩家
	m_nHunGangSeat    = -1 ;	// 没有混杠的玩家

	/*FOR TWO PLAYER m_abTian[0] = m_abTian[1] = m_abTian[2] = m_abTian[3] = TRUE;*/
	m_abTian[0] = m_abTian[1] = TRUE;
}

void CTKMahjongObj::_split(const string& s, const string& delim, vector<string>& ret)
{
    boost::tokenizer<boost::char_separator<char> > token(s, boost::char_separator<char>(delim.c_str()));
    for (auto iter = token.begin(); iter != token.end(); ++iter)
    {
        ret.push_back(*iter);
    }
}


void CTKMahjongObj::_initTileManual(MAHJONGTILECOUNTINFO& rTileInfo, const string& strTile)
{
    if (strTile.find("万") != strTile.npos)
    {
        rTileInfo.nColor = COLOR_WAN;
        rTileInfo.nWhat = atoi(&strTile[0])-1;
        rTileInfo.vUsed.clear();
        rTileInfo.nTotal = 1;
        return;
    }
    static string szTile[3] = { "中","发","白" };
    for (int i = 0;i < 3;++i)
    {
        if (strTile.find(szTile[i]) != strTile.npos)
        {
            rTileInfo.nColor = COLOR_JIAN;
            rTileInfo.nWhat = i;
            rTileInfo.vUsed.clear();
            rTileInfo.nTotal = 1;
            return;
        }
    }
    static string szFlower[4] = { "梅","兰","竹","菊" };
    for (int i = 0;i < 4;++i)
    {
        if (strTile.find(szFlower[i]) != strTile.npos)
        {
            rTileInfo.nColor = COLOR_FLOWER;
            rTileInfo.nWhat = i;
            rTileInfo.vUsed.clear();
            rTileInfo.nTotal = 1;
            return;
        }
    }
}

void CTKMahjongObj::_initTilesManual(char * szIniFile)
{
	int nBanker = TKGetIniInt("InitTile", "Banker", -1, szIniFile);
	if (nBanker >= 0)
	{
		Banker(nBanker);
	}

    char szBuffer[1024] = { 0 };
    TKGetIniString("InitTile", "AllTiles", "", szBuffer, sizeof(szBuffer), szIniFile);

    vector<string> vStr;
    _split(szBuffer, ",", vStr);
    for (int i = 0; i < vStr.size() && i < 52; ++i)
    {
        MAHJONGTILECOUNTINFO rInfo;
        _initTileManual(rInfo, vStr[i]);
        m_vMannulTiles.push_back(rInfo);
    }
}

bool CTKMahjongObj::InitInstance(CTKMahjongGame* pGame, CGameRule * pRule)
{
    // 规则对象
    m_pMahjongGame = pGame;
    m_pGameRule = pRule;

    // 创建所有的麻将牌
    for (int i = 0; i < MAX_TILE_COUNT; i++)
    { // 遍历所有的牌
        m_apAllTile[i] = CreateTile(pRule);
        if (NULL == m_apAllTile[i])
        { // 创建失败
            TKWriteLog(TEXT("【CTKMahjongObj::InitInstance】create %d tile failed!"), i);
            return false;
        }
    }

    // 创建Judge对象
    m_pJudge = CreateJudge(MAHJONG_TYPE_JSPK);
    if (NULL == m_pJudge || !m_pJudge->Init(m_pGameRule))
    { // 分配失败或者初始化失败
        TKWriteLog(TEXT("【CTKMahjongObj::OnInitInstance】m_pJudge=0x%08X"), m_pJudge);
        return FALSE;
    }

    // 创建牌墙对象
    m_pTileWall = CreateTileWall(MAHJONG_TYPE_JSPK);

    m_nBanker = -1;				// 当前没有庄家
    m_nRoundWind = WIND_PLACE_SOUTH;	// 缺省是南风圈

    _initTiles(pRule);

    // 计算总共有多少张牌
    m_nTileCount = 0;
    for (int i = 0; i < sizeof(m_apTileCountInfo) / sizeof(m_apTileCountInfo[0]); i++)
    { // 遍历所有的牌
        m_nTileCount += m_apTileCountInfo[i].nTotal;
    }

    if (!OnInitInstance())
    { // 初始化失败
        TKWriteLog(TEXT("【CTKMahjongObj::InitInstance】call OnInitInstance() failed!"));
        return false;
    }

    return true;
}

void CTKMahjongObj::_initTiles(CGameRule * pRule)
{
    char szIniFile[MAX_PATH] = ".\\InitTiles_MahjongJS.ini";
    if (1 == GetPrivateProfileInt("InitTile", "EnableInitTile", 0, szIniFile))
    {
        _initTilesManual(szIniFile);
    }

    // 计算各个牌的张数信息
    PMAHJONGTILECOUNTINFO pTileInfo = NULL;
    int nTileType = pRule->GetRule(RULE_TILETYPE);
    if (0 != (TILE_TYPE_BIT_WAN & nTileType))
    { // 有万
        pTileInfo = m_apTileCountInfo;
        for (int i = 0; i < 9; i++, pTileInfo++)
        { // 1－9万
            pTileInfo->nColor = COLOR_WAN;
            pTileInfo->nWhat = TILE_NO1 + i;
            pTileInfo->vUsed.clear();
            pTileInfo->nTotal = 4;
        }
    }
    if (0 != (TILE_TYPE_BIT_TIAO & nTileType))
    { // 有条
        pTileInfo = m_apTileCountInfo + 9;
        for (int i = 0; i < 9; i++, pTileInfo++)
        { // 1－9条
            pTileInfo->nColor = COLOR_TIAO;
            pTileInfo->nWhat = TILE_NO1 + i;
            pTileInfo->vUsed.clear();
            pTileInfo->nTotal = 4;
        }
    }
    if (0 != (TILE_TYPE_BIT_BING & nTileType))
    { // 有饼
        pTileInfo = m_apTileCountInfo + 18;
        for (int i = 0; i < 9; i++, pTileInfo++)
        { // 1－9饼
            pTileInfo->nColor = COLOR_BING;
            pTileInfo->nWhat = TILE_NO1 + i;
            pTileInfo->vUsed.clear();
            pTileInfo->nTotal = 4;
        }
    }
    if (0 != (TILE_TYPE_BIT_WIND & nTileType))
    { // 有东南西北
        pTileInfo = m_apTileCountInfo + 27;
        for (int i = 0; i < 4; i++, pTileInfo++)
        { // 东南西北
            pTileInfo->nColor = COLOR_WIND;
            pTileInfo->nWhat = TILE_NO1 + i;
            pTileInfo->vUsed.clear();
            pTileInfo->nTotal = 4;
        }
    }
    if (0 != (TILE_TYPE_BIT_JIAN & nTileType))
    { // 有中发白
        pTileInfo = m_apTileCountInfo + 31;
        for (int i = 0; i < 3; i++, pTileInfo++)
        { // 中发白
            pTileInfo->nColor = COLOR_JIAN;
            pTileInfo->nWhat = TILE_NO1 + i;
            pTileInfo->vUsed.clear();
            pTileInfo->nTotal = 4;
        }
    }
    // 目前需要支持可自定义配置花牌个数功能，对原有规则解析进行修改 addby zhangws 20180621
    int nChangeFlowerCount = m_pGameRule->GetRule(RULE_CF_COUNT);
    if (nChangeFlowerCount > 8)
        nChangeFlowerCount = 8; //最多只支持8张花牌
    if (0 < nChangeFlowerCount)
    {
        int nChangeFlowerCountTemp = nChangeFlowerCount;
        if (4 < nChangeFlowerCount)
        {
            // 有春夏秋冬中的一种或这多种
            pTileInfo = m_apTileCountInfo + 38;
            for (int i = 0; i < nChangeFlowerCount - 4; i++, pTileInfo++)
            { // 春夏秋冬
                pTileInfo->nColor = COLOR_SEASON;
                pTileInfo->nWhat = TILE_NO1 + i;
                pTileInfo->vUsed.clear();
                pTileInfo->nTotal = 1;
            }
            nChangeFlowerCountTemp = 4;
        }

        pTileInfo = m_apTileCountInfo + 34;
        for (int i = 0; i < nChangeFlowerCountTemp; i++, pTileInfo++)
        {
            pTileInfo->nColor = COLOR_FLOWER;
            pTileInfo->nWhat = TILE_NO1 + i;
            pTileInfo->vUsed.clear();
            pTileInfo->nTotal = 1;
        }

    }
}

// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
CTKMahjongPlayer * CTKMahjongObj::AddPlayer(CTKGamePlayer * pPlayer)
{
    int nSeat = pPlayer->m_nSeatOrder;
    if (NULL == m_apPlayer[nSeat])
    {
        m_apPlayer[nSeat] = CreatePlayer(nSeat, pPlayer);
    }

    m_cnPlayer++;
    return m_apPlayer[nSeat];
}




// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
bool CTKMahjongObj::OnInitInstance()
{
	return true;
}




// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CTKMahjongObj::SetGameSeatAndBanker( int* pnChagePlaceRule , int nBanker )
{
	// 保存一下
	memcpy( m_anSeat, pnChagePlaceRule, sizeof( m_anSeat ) );

	CTKMahjongPlayer *pPlayer = NULL ;
	
	for( int i = 0 ; i < MAX_PLAYER_COUNT; i ++ )
	{ // 遍历各个座位
		pPlayer = GetPlayerBySeat( pnChagePlaceRule[ i ] ) ;
		if ( NULL != pPlayer )
		{
			pPlayer->GameSeat( i ) ;
			pPlayer->Rules( 0 ) ;
		}
	}

	// 确定玩家的行牌规则
	for(int i = 0 ; i < MAX_PLAYER_COUNT; i ++ )
	{ // 按照游戏座位遍历
		pPlayer = GetPlayerBySeat( i );
		if ( NULL == pPlayer )
		{
			// 这个位置上没人
			continue;
		}
		int nSeat = pPlayer->Seat() ;
		
		// 吃牌
		int nChiRules = 0;
		if ( m_pGameRule->GetRule( RULE_CANEAT ) ) 
		{
			nChiRules = ( 1 << GetPrevPlayerSeat( nSeat ) ) ;	// 只能吃上家
		}
		// 碰牌
		int nPengRules = 0;
		if ( m_pGameRule->GetRule( RULE_CANPONG ) ) 
		{
			nPengRules = 0xF ;
			nPengRules &= ~( 1 << nSeat ) ;	// 不能碰自己
		}
		// 杠牌
		int nGangRules = 0xF;	// 极速二麻修改：默认能杠牌
		// 和牌
		int nWinRules = 0xF ;	// 缺省可以和所有人的牌
		pPlayer->Rules( nChiRules | ( nPengRules << 8 ) | ( nGangRules << 16 ) | ( nWinRules << 24 ) ) ;
	}

	// 确定庄家
	if (Banker() < 0)
	{
		Banker(nBanker);
	}
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CTKMahjongObj::SetWind( int nRoundWind )
{
	// 保存圈风
	m_nRoundWind = nRoundWind ;

	// 根据当前的庄家和各个玩家的游戏座位号信息，设置各个玩家的门风
	int nBanker = Banker() ;
	if( -1 == nBanker )
	{ // 居然还没有设置庄家
		TKWriteLog( TEXT( "【CTKMahjongObj::SetWind(%d)】,banker=-1" ) , nRoundWind ) ;
		return FALSE ;
	}

	int nGameSeat = GetPlayerBySeat( nBanker )->GameSeat() ;	// 庄家的游戏座位号
	CTKMahjongPlayer * pPlayer = GetPlayerByGameSeat( ( nGameSeat ) % MAX_PLAYER_COUNT );	// 依次取游戏中的下一家
	if ( pPlayer )
		pPlayer->Wind( 0 ) ;	// 东风位
	pPlayer = GetPlayerByGameSeat( ( nGameSeat + 1 ) % MAX_PLAYER_COUNT );	// 依次取游戏中的下一家
	if ( pPlayer )
		pPlayer->Wind( 2 ) ;	// 西风位

	return TRUE ;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CTKMahjongObj::HasPlayer( int nSeat )
{
	if ( nSeat < 0 || nSeat >= MAX_PLAYER_COUNT )
	{
		return false;
	}

	return m_apPlayer[ nSeat ] != NULL;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CTKMahjongPlayer* CTKMahjongObj::GetPlayerBySeat( int nSeat )
{
	return m_apPlayer[ nSeat % MAX_PLAYER_COUNT ] ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CTKMahjongPlayer* CTKMahjongObj::GetPlayerByGameSeat( int nGameSeat )
{
	nGameSeat %= MAX_PLAYER_COUNT ;

	for( int i = 0 ; i < MAX_PLAYER_COUNT; i ++ )
	{ // 遍历所有玩家
		if( NULL != m_apPlayer[ i ] && nGameSeat == m_apPlayer[ i ]->GameSeat() )
		{ // 就是这个座位号
			return m_apPlayer[ i ] ;
		}
	}

	return NULL ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CTKMahjongPlayer* CTKMahjongObj::GetPlayerByWind( int nWind )
{
	nWind %= 4 ;
	
	for( int i = 0 ; i < MAX_PLAYER_COUNT ; i ++ )
	{ // 遍历所有玩家
		if ( NULL == m_apPlayer[ i ] )
		{
			// 没这个人
			continue;
		}

		if( nWind == m_apPlayer[ i ]->Wind() )
		{ // 就是这个座位号
			return m_apPlayer[ i ] ;
		}
	}
		
	return NULL ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CTKMahjongPlayer* CTKMahjongObj::GetPlayerByID( DWORD dwUserID )
{
	CTKMahjongPlayer *pPlayer = NULL ;
	for( int i = 0 ; i < MAX_PLAYER_COUNT ; i ++ )
	{ // 遍历所有玩家
		pPlayer = m_apPlayer[ i ] ;
		if( NULL != pPlayer && dwUserID == pPlayer->UserID() )
		{ // 就是这个玩家
			return pPlayer ;
		}
	}
	
	return NULL ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CTKMahjongObj::CurDiscardSeat( int nSeat )
{
	m_nCurDiscardSeat = nSeat ;
	m_pCurDiscardTile = NULL ;	// 清除玩家出的牌
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
int CTKMahjongObj::GetNextPlayerSeat( int nSeat, bool bIgnoreWinner )
{
	// 这个座位的玩家(决不可能是空)
	CTKMahjongPlayer * pPlayer = GetPlayerBySeat( nSeat );
	ASSERT( NULL != pPlayer );

	int nNextGameSeat = pPlayer->GameSeat();
	for ( int i = 0; i < MAX_PLAYER_COUNT; i++ )
	{
		// 循环查找下一个人
		nNextGameSeat++;
		if ( MAX_PLAYER_COUNT == nNextGameSeat )
		{
			nNextGameSeat = 0;
		}

		// 看看这个座位上有没有人
		pPlayer = GetPlayerByGameSeat( nNextGameSeat );
		if ( NULL != pPlayer )
		{
			if ( bIgnoreWinner )
			{
				// 跳过已和牌者
				if ( pPlayer->WinCount() > 0 )
				{
					// 和了,找下一个人
					continue;
				}
			}

			// 就这个人
			break;
		}
	}

	ASSERT( NULL != pPlayer );

	return pPlayer->Seat() ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
int CTKMahjongObj::GetPrevPlayerSeat( int nSeat, bool bIgnoreWinner )
{
	// 这个座位的玩家(决不可能是空)
	CTKMahjongPlayer * pPlayer = GetPlayerBySeat( nSeat );
	ASSERT( NULL != pPlayer );

	int nPrevGameSeat = pPlayer->GameSeat() ;	// 上家的游戏座位号
	for ( int i = 0; i < MAX_PLAYER_COUNT; i++ )
	{
		// 循环查找下一个人
		nPrevGameSeat--;
		if ( 0 > nPrevGameSeat )
		{
			nPrevGameSeat = MAX_PLAYER_COUNT - 1;
		}

		// 看看这个座位上有没有人
		pPlayer = GetPlayerByGameSeat( nPrevGameSeat );
		if ( NULL != pPlayer )
		{
			if ( bIgnoreWinner )
			{
				// 跳过已和牌者
				if ( pPlayer->WinCount() > 0 )
				{
					// 和了,找下一个人
					continue;
				}
			}

			// 就这个人
			break;
		}
	}

	ASSERT( NULL != pPlayer );

	return pPlayer->Seat() ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
BOOL CTKMahjongObj::IsDiscardTile( int nTileID )
{
	if( NULL == m_pCurDiscardTile )
	{ // 当前没有出的牌
		TKWriteLog( TEXT( "【CTKMahjongObj::IsDiscardTile】tile(0x%03X)" ) , nTileID ) ;
		return FALSE ;
	}

	return m_pCurDiscardTile->IDIs( nTileID ) ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
BOOL CTKMahjongObj::IsLastGangTile( int nTileID )
{
	if( NULL == m_pLastGangTile )
	{ // 当前没有杠的牌
//		WRITEDEBUGLOG( TEXT( "【CTKMahjongObj::IsLastGangTile(0x%03X)】" ) , nTileID ) ;
		return FALSE ;
	}
	
	return m_pLastGangTile->IDIs( nTileID ) ;
}

// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CTKMahjongObj::SetOpenDoorPos( int nOpenDoorSeat , int nOpenDoorFrusta )
{
	// 牌墙
	m_pTileWall->SetOpenDoorPos( nOpenDoorSeat, nOpenDoorFrusta );

	// 设置抓牌的玩家就是庄家
	m_nCurDrawSeat = Banker() ;
}

// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CMahJongTile* CTKMahjongObj::GetOneTileFromWall( bool bFromWallHeader , int nOffset )
{
	return m_pTileWall->GetOneTile( bFromWallHeader, nOffset );
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
int CTKMahjongObj::PlayerChangeFlower(PTKREQMAHJONGCHANGEFLOWER pReqMahJongChangeFlower)
{
    CTKMahjongPlayer *pPlayer = GetPlayerBySeat(pReqMahJongChangeFlower->nSeat);	// 这个补花的玩家
    ASSERT(NULL != pPlayer);	// 在前面已经检查过了pPlayer肯定不为空

    int nTileID = pReqMahJongChangeFlower->nTileID;			// 要补的这张花牌的ID号
    CMahJongTile *pTile = pPlayer->FindHandTile(nTileID, TRUE);	// 要删的这张牌（如果玩家手中牌不可见，就从牌尾选一张牌）
    ASSERT(NULL != pTile);
    if (pPlayer->RealHandTile())
    { // 玩家手中是真实的牌
        ASSERT(pTile->IDIs(nTileID));	// 肯定是这张牌
    }
    else
    { // 玩家手中不是真实的牌
        pTile->SetID(nTileID);	// 设置牌的ID号
    }
    return pPlayer->ChangeFlower(pTile);
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CMahJongTile* CTKMahjongObj::PlayerDrawTile( PTKREQMAHJONGDRAWTILE pReqMahJongDrawTile )
{
	CMahJongTile *pTile = GetOneTileFromWall( pReqMahJongDrawTile->bFromWallHeader , pReqMahJongDrawTile->nTileOffset ) ;
	ASSERT( NULL != pTile ) ;
	pTile->SetID( pReqMahJongDrawTile->nTileID ) ;	// 设置这张牌的ID号
	
	int nSeat = pReqMahJongDrawTile->nSeat ;	// 抓牌的玩家座位号
	// 如果是开始出牌前的抓牌（这时候只可能是补花），那么就不更改当前抓牌玩家的信息
	if( DRAW_TILE_TYPE_CHANGE_FLOWER_1 != pReqMahJongDrawTile->nDrawTileType )
	{ // 如果抓牌类型不是补花阶段的补花后抓牌
		m_nCurDrawSeat = nSeat ;	// 设置当前抓牌的玩家座位号
	}
	if (DRAW_TILE_TYPE_CHANGE_FLOWER_2 != pReqMahJongDrawTile->nDrawTileType)
	{
		m_nCurDrawType = pReqMahJongDrawTile->nDrawTileType;	//补花不更新类型，否则杠牌再补花后不能胡牌
	}
	
	CTKMahjongPlayer *pPlayer = GetPlayerBySeat( nSeat ) ;	// 这个玩家
	ASSERT( NULL != pPlayer );
	pPlayer->DrawTile( pTile , m_nCurDrawType ) ;
	
	return pTile ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CMahJongTile* CTKMahjongObj::PlayerDiscardTile( PTKREQMAHJONGDISCARDTILE pReqMahJongDiscardTile )
{
	int nSeat = pReqMahJongDiscardTile->nSeat ;					// 出牌的玩家座位号
	CTKMahjongPlayer *pPlayer = GetPlayerBySeat( nSeat ) ;		// 出牌玩家
	ASSERT( NULL != pPlayer );
	int nTileID = pReqMahJongDiscardTile->nTileID ;				// 这张出的牌的ID号
	CMahJongTile *pTile = pPlayer->FindHandTile( nTileID , TRUE ) ;	// 出的这张牌（如果玩家手中牌不可见，就从牌尾选一张牌）
	ASSERT( NULL != pTile ) ;
	if( pPlayer->RealHandTile() )
	{ // 玩家手中的牌是真实的
		ASSERT( pTile->IDIs( nTileID ) ) ;	// 肯定是这张牌
	}
	else
	{ // 玩家手中的牌不是真实的
		pTile->SetID( pReqMahJongDiscardTile->nTileID ) ;	// 设置这张牌的ID号
	}
	
	m_pCurDiscardTile = pTile ;
	m_nCurDiscardSeat = nSeat ;
	m_nLastDiscardSeat = nSeat;

	// 重置杠牌的信息
	ResetLastGangInfo() ;
	
	pPlayer->DiscardTile( pTile, pReqMahJongDiscardTile->bHide ) ;

	// 增加明牌张数
	AddShowTileCount( nTileID ) ;

	m_abTian[nSeat] = FALSE;
	
	return pTile ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CMahJongTile* CTKMahjongObj::PlayerChiTile( PTKREQMAHJONGCHI pReqMahJongChi , CMahJongTile* apChiTile[ 3 ] )
{
	// 
	// 从玩家手中减去要吃的这两张牌，并把要吃的牌加到吃碰杠的牌中；
	// 同时从出牌玩家出的牌中删除这张牌
	// 
	CTKMahjongPlayer *pPlayer = GetPlayerBySeat( pReqMahJongChi->nSeat ) ;	// 这个玩家
	ASSERT( NULL != pPlayer );
	
	// 从手中找到这两张要用来吃的牌
	int nTileID = pReqMahJongChi->nTileID ;	// 要吃的牌
	int *pnChiTileID = pReqMahJongChi->anChiTileID ;	// 要吃的这三张牌的ID号
	CMahJongTile *pTile = NULL ;
	for( int i = 0 ; i < 3 ; i ++ )
	{ // 遍历这三张吃的牌
		if( CMahJongTile::SameID( nTileID , pnChiTileID[ i ] ) )
		{ // 就是这张要吃的牌
			pTile = m_pCurDiscardTile ;	// 取玩家最后出的牌
			ASSERT( ( NULL != pTile ) && pTile->IDIs( nTileID ) ) ;	// 就是这张牌
			apChiTile[ i ] = pTile ;	// 保存这张牌
			continue ;
		}
		
		// 从玩家手中找到这张牌
		pTile = pPlayer->FindHandTile( pnChiTileID[ i ] , FALSE ) ;	// （如果玩家手中牌不可见，就从牌头选一张牌）
		ASSERT( NULL != pTile ) ;
		if( pPlayer->RealHandTile() )
		{ // 玩家手中的牌是真实的
			ASSERT( pTile->IDIs( pnChiTileID[ i ] ) ) ;	// 就是这张牌
		}
		else
		{ // 玩家手中的牌不是真实的
			pTile->SetID( pnChiTileID[ i ] ) ;	// 设置这张牌的ID号
		}
		pPlayer->DeleteHandTile( pTile ) ;	// 从玩家手中删除这张牌
		
		// 增加明牌张数
		AddShowTileCount( pnChiTileID[ i ] ) ;

		apChiTile[ i ] = pTile ;	// 保存这张牌
	}
	
	// 从出牌玩家出的牌中删除这张牌
	/// 【注意】这里没有重置m_pCurDiscardTile的值，这个值要到该轮到下一个玩家出牌时才重置
	CTKMahjongPlayer *pDiscardPlayer = GetPlayerBySeat( CurDiscardSeat() ) ;	// 当前出牌的玩家
	pTile = pDiscardPlayer->DeleteDiscardTile( nTileID ) ;
	ASSERT( pTile == m_pCurDiscardTile ) ;	// 就是玩家最后出的那张牌

	pPlayer->ChiTile( pTile , apChiTile, CurDiscardSeat() );

	// 所有人都不能计天地人和了
	memset( m_abTian , 0 , sizeof( m_abTian ) ) ;

	m_nCurDrawType = DRAW_TILE_TYPE_NULL ;	// 重置抓牌类型
	
	return pTile ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CMahJongTile* CTKMahjongObj::PlayerPengTile( PTKREQMAHJONGPENG pReqMahJongPeng , CMahJongTile* apPengTile[ 3 ] )
{
	// 
	// 从玩家手中减去要碰的这两张牌，并把要碰的牌加到吃碰杠的牌中
	// 同时从出牌玩家出的牌中删除这张牌
	// 
	CTKMahjongPlayer *pPlayer = GetPlayerBySeat( pReqMahJongPeng->nSeat ) ;	// 这个玩家
	ASSERT( NULL != pPlayer );
	
	// 从手中找到这两张要用来吃的牌
	int nTileID = pReqMahJongPeng->nTileID ;	// 要碰的牌
	int *pnPengTileID = pReqMahJongPeng->anPengTileID ;	// 要碰的这三张牌的ID号
	CMahJongTile *pTile = NULL ;
	for( int i = 0 ; i < 3 ; i ++ )
	{ // 遍历这三张碰的牌
		if( CMahJongTile::SameID( nTileID , pnPengTileID[ i ] ) )
		{ // 就是这张要碰的牌
			pTile = m_pCurDiscardTile ;	// 取玩家最后出的牌
			ASSERT( ( NULL != pTile ) && pTile->IDIs( nTileID ) ) ;	// 就是这张牌
			apPengTile[ i ] = pTile ;	// 保存这张牌
			continue ;
		}
		
		// 从玩家手中找到这张牌
		pTile = pPlayer->FindHandTile( pnPengTileID[ i ] , FALSE ) ;	// （如果玩家手中牌不可见，就从牌头选一张牌）
		ASSERT( NULL != pTile ) ;
		if( pPlayer->RealHandTile() )
		{ // 玩家手中的牌是真实的
			ASSERT( pTile->IDIs( pnPengTileID[ i ] ) ) ;	// 就是这张牌
		}
		else
		{ // 玩家手中的牌不是真实的
			pTile->SetID( pnPengTileID[ i ] ) ;	// 设置这张牌的ID号
		}
		pPlayer->DeleteHandTile( pTile ) ;	// 从玩家手中删除这张牌
		
		apPengTile[ i ] = pTile ;	// 保存这张牌
	}
	
	// 从出牌玩家出的牌中删除这张牌
	/// 【注意】这里没有重置m_pCurDiscardTile的值，这个值要到该轮到下一个玩家出牌时才重置
	CTKMahjongPlayer *pDiscardPlayer = GetPlayerBySeat( CurDiscardSeat() ) ;	// 当前出牌的玩家
	pTile = pDiscardPlayer->DeleteDiscardTile( nTileID ) ;
	ASSERT( pTile == m_pCurDiscardTile ) ;	// 就是玩家最后出的那张牌

	pPlayer->PengTile( pTile , apPengTile, CurDiscardSeat() ) ;

	// 增加明牌张数
	AddShowTileCount( nTileID , 2 ) ;	// 碰的那张牌已经被加过了
	
	// 所有人都不能计天地人和了
	memset( m_abTian , 0 , sizeof( m_abTian ) ) ;

	m_nCurDrawType = DRAW_TILE_TYPE_NULL ;	// 重置抓牌类型
	
	return pTile ;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CMahJongTile* CTKMahjongObj::PlayerGangTile( PTKREQMAHJONGGANG pReqMahJongGang , CMahJongTile* apGangTile[ 4 ] , int& nCount, bool bAfterPeng )
{
	int nSeat = pReqMahJongGang->nSeat ;	// 这个要杠牌的玩家的座位号	
	m_nLastGangSeat = nSeat ;	// 保存这个杠牌玩家的座位号
	m_nLastGangType = pReqMahJongGang->nGangType ;	// 保存杠牌的类型
	
	// 
	// 从玩家手中减去要杠的牌，并把要杠的牌加到吃碰杠的牌中
	// 同时从出牌玩家出的牌中删除这张牌
	// 【注意】要区分直杠、补杠，明杠、暗杠。
	// 
	CTKMahjongPlayer *pPlayer = GetPlayerBySeat( nSeat ) ;	// 这个要杠牌的玩家
	int nGangType = pReqMahJongGang->nGangType ;			// 杠牌类型
	int nTileID = pReqMahJongGang->nTileID ;				// 要杠的这张牌的ID号
	int *pnGangTileID = pReqMahJongGang->anGangTileID ;		// 要杠的这四张牌的ID号
	CMahJongTile *pTile = NULL ;
	CMahJongTile *pRetTile = NULL ;	// 返回的牌值

	if (bAfterPeng && IS_GANG_TYPE_DIRECT(nGangType) && IS_GANG_TYPE_PUBLIC(nGangType))
	{
		// 玩家碰了三张，手中一张
		// 从玩家手中找到这张牌
		pTile = pPlayer->FindHandTile( nTileID , FALSE ) ;	// （如果玩家手中牌不可见，就从牌头选一张牌）
		ASSERT( NULL != pTile ) ;
		if( pPlayer->RealHandTile() )
		{ // 玩家手中的牌是真实的
			ASSERT( pTile->IDIs( nTileID ) ) ;	// 就是这张牌
		}
		else
		{ // 玩家手中的牌不是真实的
			pTile->SetID( nTileID ) ;	// 设置这张牌的ID号
		}
		pPlayer->DeleteHandTile( pTile ) ;	// 从玩家手中删除这张牌
		apGangTile[ 0 ] = pTile ;

		m_nLastKongSeat = nSeat ;	// 点杠的就是这个杠牌的玩家

		m_pLastGangTile = pTile ;		
		pPlayer->GangTile( pTile , apGangTile , 1, nSeat ) ;

		// 增加明牌张数
		AddShowTileCount( nTileID , 1 ) ;	// 从手上亮出1张牌
	} 
	else if( IS_GANG_TYPE_DIRECT( nGangType ) )
	{ // 直杠
		if( IS_GANG_TYPE_PUBLIC( nGangType ) )
		{ // 明杠
			// 玩家手中三张，杠别人打的一张
			for( int i = 0 ; i < 4 ; i ++ )
			{ // 遍历这四张牌
				if( CMahJongTile::SameID( nTileID , pnGangTileID[ i ] ) )
				{ // 就是要杠的这张牌
					pTile = m_pCurDiscardTile ;	// 取玩家最后出的牌
					apGangTile[ i ] = pTile ;
					continue ;
				}
				
				// 从玩家手中找到这张牌
				pTile = pPlayer->FindHandTile( pnGangTileID[ i ] , FALSE ) ;	// （如果玩家手中牌不可见，就从牌头选一张牌）
				ASSERT( NULL != pTile ) ;
				if( pPlayer->RealHandTile() )
				{ // 玩家手中的牌是真实的
					ASSERT( pTile->IDIs( pnGangTileID[ i ] ) ) ;	// 就是这张牌
				}
				else
				{ // 玩家手中的牌不是真实的
					pTile->SetID( pnGangTileID[ i ] ) ;	// 设置这张牌的ID号
				}
				pPlayer->DeleteHandTile( pTile ) ;	// 从玩家手中删除这张牌

				apGangTile[ i ] = pTile ;
			}
			
			// 从出牌玩家出的牌中删除这张牌
			/// 【注意】这里没有重置m_pCurDiscardTile的值，这个值要到该轮到下一个玩家出牌时才重置
			CTKMahjongPlayer *pDiscardPlayer = GetPlayerBySeat( CurDiscardSeat() ) ;	// 当前出牌的玩家
			pTile = pDiscardPlayer->DeleteDiscardTile( nTileID ) ;
			ASSERT( pTile == m_pCurDiscardTile ) ;	// 就是玩家最后出的那张牌

			m_nLastKongSeat = CurDiscardSeat() ;	// 点杠的是当前出牌玩家

			m_pLastGangTile = pTile ;
			pPlayer->GangTile( pTile , apGangTile , 4, CurDiscardSeat() ) ;
			pRetTile = pTile ;

			// 增加明牌张数
			AddShowTileCount( nTileID , 3 ) ;	// 从手上亮出3张牌
		}
		else if( IS_GANG_TYPE_HIDDEN( nGangType ) )
		{ // 暗杠
			// 玩家手中四张
			for( int i = 0 ; i < 4 ; i ++ )
			{ // 遍历这四张牌
				// 从玩家手中找到这张牌
				pTile = pPlayer->FindHandTile( pnGangTileID[ i ] , FALSE ) ;	// （如果玩家手中牌不可见，就从牌头选一张牌）
				ASSERT( NULL != pTile ) ;
				if( pPlayer->RealHandTile() )
				{ // 玩家手中的牌是真实的
					ASSERT( pTile->IDIs( pnGangTileID[ i ] ) ) ;	// 就是这张牌
				}
				else
				{ // 玩家手中的牌不是真实的
					pTile->SetID( pnGangTileID[ i ] ) ;	// 设置这张牌的ID号
				}
				pPlayer->DeleteHandTile( pTile ) ;	// 从玩家手中删除这张牌
				
				apGangTile[ i ] = pTile ;
			}
			
			m_nLastKongSeat = nSeat ;	// 点杠的就是这个杠牌的玩家
			m_pLastGangTile = pTile ;	// 暗杠就随便指定一张牌做为这张要杠的牌
			pPlayer->GangTile( NULL , apGangTile , 4, nSeat ) ;

			// 增加明牌张数
			AddShowTileCount( nTileID , 4 ) ;	// 从手上亮出4张牌
		}
	}
	else if( IS_GANG_TYPE_PATCH( nGangType ) )
	{ // 补杠
		// 玩家碰了三张，手中一张
		// 从玩家手中找到这张牌
		pTile = pPlayer->FindHandTile( nTileID , FALSE ) ;	// （如果玩家手中牌不可见，就从牌头选一张牌）
		ASSERT( NULL != pTile ) ;
		if( pPlayer->RealHandTile() )
		{ // 玩家手中的牌是真实的
			ASSERT( pTile->IDIs( nTileID ) ) ;	// 就是这张牌
		}
		else
		{ // 玩家手中的牌不是真实的
			pTile->SetID( nTileID ) ;	// 设置这张牌的ID号
		}
		pPlayer->DeleteHandTile( pTile ) ;	// 从玩家手中删除这张牌
		apGangTile[ 0 ] = pTile ;
		
		m_nLastKongSeat = nSeat ;	// 点杠的就是这个杠牌的玩家

		m_pLastGangTile = pTile ;		
		pPlayer->GangTile( pTile , apGangTile , 1, nSeat ) ;

		// 增加明牌张数
		AddShowTileCount( nTileID , 1 ) ;	// 从手上亮出1张牌
	}

	// 所有人都不能计天地人和了
	memset( m_abTian , 0 , sizeof( m_abTian ) ) ;

	m_nCurDrawType = DRAW_TILE_TYPE_NULL ;	// 重置抓牌类型

	return pRetTile ;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
PMAHJONGTILECOUNTINFO CTKMahjongObj::GetTileCountInfo( int nColor , int nWhat )
{
	// 首先是从这张牌的花色、点数，找到所对应的基类m_apTileCountInfo中的索引号
	int nIndex = nWhat ;
	
	switch( nColor )
	{
	case COLOR_WAN :	// 万
		break ;
	case COLOR_TIAO :	// 条
		nIndex += 9 ;
		break ;
	case COLOR_BING :	// 饼
		nIndex += 18 ;
		break ;
	case COLOR_WIND :	// 风牌（东南西北）
		nIndex += 27 ;
		break ;
	case COLOR_JIAN :	// 箭牌（中发白）
		nIndex += 31 ;
		break ;
	case COLOR_FLOWER :	// 梅兰竹菊
		nIndex += 34 ;
		break ;
	case COLOR_SEASON :	// 春夏秋冬
		nIndex += 38 ;
		break ;
	}
	
	PMAHJONGTILECOUNTINFO pTileCountInfo = m_apTileCountInfo + nIndex ;	// 这张牌的张数信息
	ASSERT( nColor == pTileCountInfo->nColor ) ;
	ASSERT( nWhat == pTileCountInfo->nWhat ) ;

	return pTileCountInfo ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CMahJongTile* CTKMahjongObj::FindDiscardTile( int nTileID )
{
	CMahJongTile *pTile = NULL ;
	CTKMahjongPlayer *pPlayer = NULL ;

	for( int i = 0 ; i < MAX_PLAYER_COUNT ; i ++ )
	{ // 遍历所有玩家的座位号
		pPlayer = GetPlayerBySeat( i ) ;	// 这个座位的玩家
		if ( NULL == pPlayer )
		{
			continue;
		}

		pTile = pPlayer->FindDiscardTile( nTileID ) ;
		if( NULL != pTile )
		{ // 找到了
			return pTile ;
		}
	}

	return NULL ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CMahJongTile* CTKMahjongObj::FindShowTile( int nTileID )
{
	CMahJongTile *pTile = NULL ;
	CTKMahjongPlayer *pPlayer = NULL ;
	
	for( int i = 0 ; i < MAX_PLAYER_COUNT ; i ++ )
	{ // 遍历所有玩家的座位号
		pPlayer = GetPlayerBySeat( i ) ;	// 这个座位的玩家
		if ( NULL != pPlayer )
		{
			continue;
		}

		pTile = pPlayer->FindShowTile( nTileID ) ;
		if( NULL != pTile )
		{ // 找到了
			return pTile ;
		}
	}
	
	return NULL ;
}




// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CMahJongTile* CTKMahjongObj::CreateTile( CGameRule * pRule )
{
	CMahJongTile *pTile = new CMahJongTile( pRule ) ;
	if( NULL == pTile )
	{ // 分配失败
		TKWriteLog( TEXT( "【CTKMahjongObj::CreateTile】malloc failed!" ) ) ;
	}

	return pTile ;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CMahJongTile * CTKMahjongObj::GetOccasionalTile( int nTileID )
{
	CMahJongTile * pTile = CreateTile( m_pGameRule );
	pTile->SetID( nTileID );
	m_vecOccasionalTile.push_back( pTile );

	return pTile;
}



// **************************************************************************************
// 
// 增加亮在桌面上的明牌张数
// 
// **************************************************************************************
bool CTKMahjongObj::AddShowTileCount( int nTileID, int nIncrement )
{
	if( nTileID == 0 )
	{ // 牌ID号无效
		return FALSE ;
	}

	int nIndex = GetTileValue( nTileID ) ;
	if( nIndex >= 34 )
	{ // 索引号无效（牌ID号无效）
		ASSERT( FALSE ) ;
		return FALSE ;
	}
	
	m_acnShowTile[ nIndex ] += nIncrement ;

	return TRUE;
}


// **************************************************************************************
// 
// 是否绝张
// 
// **************************************************************************************
BOOL CTKMahjongObj::IsOnlyOne( int nTileID, BOOL bWinSelf )
{
	if( nTileID == 0 )
	{ // 牌ID号无效
		ASSERT( FALSE ) ;
		return FALSE ;
	}

	int nIndex = GetTileValue( nTileID ) ;
	if( nIndex >= 34 )
	{ // 索引号无效（牌ID号无效）
		ASSERT( FALSE ) ;
		return FALSE ;
	}

	if ( bWinSelf )
	{
		return m_acnShowTile[nIndex] >= 3;
	}

	return m_acnShowTile[ nIndex ] > 3 ;
}

// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CTKMahjongObj::ResetLastGangInfo()
{
	m_nLastGangSeat = -1 ;
	m_nLastGangType = 0 ;
	m_pLastGangTile = NULL ;
	m_nLastKongSeat = -1 ;
}


// **************************************************************************************
// 
// 
// 
// **************************************************************************************
int CTKMahjongObj::GetTileValue( int nTileID )
{
	ASSERT( nTileID != 0 ) ;
	int nColor = CMahJongTile::TileColor( nTileID ) ;	// 牌的花色
	int nWhat  = CMahJongTile::TileWhat( nTileID ) ;	// 牌的点数
	//ASSERT( nColor >= 0 && nColor <= 4 ) ;
	
	int nIndex = 0 ;
	if( nColor < 4 )
	{ // 万、条、饼、东南西北
		nIndex = nColor + ( nColor << 3 ) + nWhat ; // nColor * 9 + nWhat ;
	}
	else 
	{ // 中发白
		nIndex = 31 + nWhat ;
	}
	return nIndex ;
}


// **************************************************************************************
// 
// 生成和牌方式组合	
// 杠上开花 // 抢杠 // 和绝张 // 海底 // 自摸 // 天和 // 地和 // 人和 // 天听
// 
// **************************************************************************************
int	CTKMahjongObj::WinMode( int nWinSeat, int nPaoSeat, int nWinTileID )
{
	int nMode = 0 ;
	
	// 杠上开花 // 抢杠
	if( m_nLastGangSeat != -1 )
	{ // 和牌前确实有人开杠了
		if( m_nLastGangSeat == nWinSeat )
		{ // 是我开杠（肯定是杠开）
			ASSERT( DRAW_TILE_TYPE_GANG == m_nCurDrawType ) ;	// 当前抓牌类型一定是杠后抓牌
			nMode |= WIN_MODE_GANGSHANGHU ;
		}
		else
		{ // 抢杠
			ASSERT( m_nLastGangSeat == nPaoSeat ) ;
			nMode |= WIN_MODE_QIANGGANG ;
		}
	}

	// 杠上炮
	if( ( nWinSeat != nPaoSeat ) && ( DRAW_TILE_TYPE_GANG == m_nCurDrawType ) )
	{ // 放炮，并且最后的抓牌类型是杠后抓牌
		nMode |= WIN_MODE_GANGPAO ;
	}
	
	// 和绝张
	if( IsOnlyOne( nWinTileID, nWinSeat == nPaoSeat ) )
	{ // 这张牌只有一张了
		nMode |= WIN_MODE_HUJUEZHANG ;
	}
	
	// 海底
	if( m_pTileWall->Empty() )
	{ // 牌墙中没牌了
		nMode |= WIN_MODE_HAIDI ;
	}
	
	// 自摸
	if( nWinSeat == nPaoSeat )
	{ // 自摸
		nMode |= WIN_MODE_ZIMO ;
	}
	
	// 天和// 地和 // 人和 
	CTKMahjongPlayer * pPlayer = GetPlayerBySeat( nWinSeat ) ;
	if( m_abTian[nWinSeat] )
	{ // 玩家还没有出过牌（也没有吃碰杠过牌）
		int nBankerSeat = Banker() ;	// 庄家的座位号
		if( nBankerSeat == nWinSeat )
		{ // 和牌的就是庄家（天和）
			nMode |= WIN_MODE_TIANHU ;
		}
		else if( nWinSeat == nPaoSeat )
		{
			nMode |= WIN_MODE_DIHU ;
		}
		else
		{
			nMode |= WIN_MODE_RENHU ;
		}
	}
	
	// 天听
	BOOL bIsTianCall = pPlayer->IsTianCall();
	if( bIsTianCall )
	{
		nMode |= WIN_MODE_TIANTING ;
	}
	
	// 立直
	if (!bIsTianCall)
	{
		if( pPlayer->HasCalled() )
		{
			nMode |= WIN_MODE_LIZHI ;
		}
	}

	return nMode ;
}



void CTKMahjongObj::Shuffle( int anFrustaOfSeat[] )
{
	ShuffleParam shuffle = { m_apAllTile, m_nTileCount };
	m_pTileWall->Shuffle( &shuffle, anFrustaOfSeat );
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
IJudge * CTKMahjongObj::CreateJudge( int nMahjongType )
{
	return NULL;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
ITileWall * CTKMahjongObj::CreateTileWall( int nMahjongType )
{
	return NULL;
}
