#pragma once
#include "tkmahjongplayer.h"
#include "TKGameService.h"
#include "TKGame.h"

enum
{
    GV_GameCount = 50250005,      //游戏局数
    GV_WinCount = 50250006,       //胜局数
    GV_WinStreak = 50250007,      //连胜局数
    GV_LoseStreak = 50250008,     //连败局数
    GV_MostWin = 50250009,        //最高连胜局数
    GV_MostLose = 50250010,       //最高连败局数
    GV_WishCoin = 50250012,       //祝福币
    GV_SilverCoin = 50250015,     //祝福银币
    GV_CopperCoin = 50250016,     //祝福铜币
	GV_TreasureRecord = 50250017, //宝藏牌抽奖记录
    GV_WinFans = 50460005,        //胡牌番数
    GV_WinCoin = 60000025,        //赢金币积分

    EG_BrushGameHand = 60000172,		 //防刷-用户日盘数积分
    EG_BrushGameTime = 60000173,	     //防刷-用户日游戏时长积分
};

enum
{
    EVT_Win = 10000077,                       //胡牌次数
    EVT_GameResult = 10000078,                //二麻对局结果(0:失败  1:胜利)
    EVT_BrushPlayerJudge = 10000178,		  //刷子用户判定
};

class CCallback :public CFuncObjCallback
{
public:
    CCallback();
    ~CCallback();

public:
    virtual BOOL Execute(PTKHEADER in_pAckMsg);
};

class CTKMahjongServerPlayer : public CTKMahjongPlayer
{
public:
	CTKMahjongServerPlayer(void);
	virtual ~CTKMahjongServerPlayer(void);

public :
    void InitPlayer(CTKGamePlayer* pPlayer);

	// 设置听牌类型（1－普通听牌，2－天听，3－相公）
	virtual void SetCallType( int nCallType , BOOL bWinSelf , BOOL bAutoGang ) ;

	// 玩家吃牌
	/// pTile是吃的这张牌（别人出的那张牌），apChiTile是这吃的三张牌
	/// 基类的这个函数返回这个吃牌所在的分组索引号
	/// 【注意】调用这个函数的时候，相应的牌已经从玩家手中或者出牌队列中删除了
	///			调用基类的这个函数，才会把相应的牌加入到玩家吃碰杠的分组中
	virtual int ChiTile( CMahJongTile* pTile , CMahJongTile* apChiTile[ 3 ], int nDiscardSeat ) ;
	// 玩家碰牌
	/// pTile是碰的这张牌（别人出的那张牌），apPengTile是这碰的三张牌
	/// 基类的这个函数返回这个碰牌所在的分组索引号
	/// 【注意】调用这个函数的时候，相应的牌已经从玩家手中或者出牌队列中删除了
	///			调用基类的这个函数，才会把相应的牌加入到玩家吃碰杠的分组中
	virtual int PengTile( CMahJongTile* pTile , CMahJongTile* apPengTile[ 3 ], int nDiscardSeat ) ;
	// 玩家杠牌
	/// 直杠的明杠：pTile是别人出的那张牌，apGangTile是这要杠的四张牌，nCount为4
	/// 直杠的暗杠：pTile为NULL，apGangTile是这要杠的四张牌，nCount为4
	/// 补杠的明杠：pTile为自己要补杠的那张牌，apGangTile也是这张要补杠的牌，nCount为1
	/// 基类的这个函数返回这个杠牌所在的分组索引号
	/// 【注意】调用这个函数的时候，相应的牌已经从玩家手中或者出牌队列中删除了
	///			调用基类的这个函数，才会把相应的牌加入到玩家吃碰杠的分组中
	virtual int GangTile( CMahJongTile* pTile , CMahJongTile* apGangTile[ 4 ] , int nCount, int nDiscardSeat ) ;

public:
	// 搜索一张牌的信息
	BOOL FindTile( int nTileID, int & cnTile );

	// 是否替这个玩家自动玩牌
	/// 对于客户端：主动托管的时候，客户端帮玩家出牌；对于服务器端：超时托管、断线托管、托管退出时，服务器帮玩家出牌
	virtual BOOL IsAutoPlay() ;	

	void CountHandTile( int acnTile[] );
	int GetTileValue( int nTileID );


	// 重置听牌信息
	void ResetCallInfo();

	// 设置听牌信息
	void SetCallInfo(int nCount, int CallTile[34]);

	// 查找是否是所听牌
	bool IsCallTile(int nTile);

	// 删除非法听牌
	void DeleteCallTile(int nTile);
	// 听牌信息
	int FillCallTile( int pnTile[] ) ;

    void SaveGrowValue(CTKGame *pGame, int nWinSeat);
    void AddGrowValue(DWORD nGrowId, int nValue = 1);
    int GetGrowValue(DWORD nGrowId);
    void DealSingleGrow64ByGrowID(DWORD dwGrowID, __int64 nValue);

private:
    void _getPivotGrow(DWORD wGrowId);
    void _saveGrowValue();

protected:
	int	m_nCallTileIndex;	// 从哪一张牌开始听牌的
	SHOWGROUPINFO m_asShowGroupInfo[MAX_SHOW_COUNT];	// 吃碰杠的信息（吃的哪张，哪家的）

	int m_nCallTileCount;		// 听牌数量
	int m_nCallTile[34];		// 听的牌

private:
    map<DWORD, int> m_astGrowInfo;
};
