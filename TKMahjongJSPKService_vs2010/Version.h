/*
  获取工程的版本号
*/
#pragma once
#include <TCHAR.h>
#include <string>
/*************************************************
// 版本资源中获取信息
// 
// param：flag 想要获取的字段名称，可以使用下列字段 
// CompanyName 
// FileDescription 
// FileVersion 
// InternalName 
// LegalCopyright
// OriginalFilename
// ProductName 
// ProductVersion 
// Comments
// LegalTrademarks 
// PrivateBuild 
// SpecialBuild 
*************************************************/
inline std::string GetFileInfo(LPCTSTR strFilePath, LPCTSTR flag)
{
	std::string strVersion = "";
   
	TCHAR * pVersionBuffer = nullptr;
	DWORD dwVerSize;    
	DWORD dwHandle;

	dwVerSize = GetFileVersionInfoSize(strFilePath, &dwHandle);    
	if (dwVerSize == 0)    
		return "";   

	pVersionBuffer = new TCHAR[dwVerSize];
	ZeroMemory(pVersionBuffer,dwVerSize *sizeof(TCHAR));
	BOOL bRtn = GetFileVersionInfo(strFilePath, 0, dwVerSize, pVersionBuffer);
	if (bRtn)   
	{    
		LPVOID lpBuffer = NULL;
		UINT uLen = 0;
		char des[128];
		//0804中文
		//04b0即1252,ANSI
		//可以从ResourceView中的Version中BlockHeader中看到
		sprintf_s(des, "\\StringFileInfo\\080404b0\\%s", flag);
		DWORD dwRtn = VerQueryValue(pVersionBuffer, TEXT(des), &lpBuffer, &uLen); 

		if(dwRtn == 0) 
		{ 
			delete [] pVersionBuffer;
			return ""; 
		}
		strVersion = (char*)lpBuffer;
		delete [] pVersionBuffer;
		return strVersion;
	}
	delete [] pVersionBuffer;
	return "";
}

/*************************************************
// 获取服务产品版本号,字符串
// 服务编译使用自动化脚本，每次编译，产品号都会增加
// 格式如:1.0.2.4build
// 服务器获得此版本号可以有效定位服务编译版本
*************************************************/
inline void GetProductVersion(std::string& /*return*/strVersion)
{
	char szFullPath[MAX_PATH];
	ZeroMemory(szFullPath, MAX_PATH);
	::GetModuleFileName(NULL, szFullPath, MAX_PATH);
	strVersion = GetFileInfo(szFullPath, "ProductVersion");
}