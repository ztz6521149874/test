#include ".\bloodyscorecalculator.h"
#include "GameRule.h"
#include <assert.h>

#ifndef ASSERT
#define ASSERT	assert
#endif

CBloodyScoreCalculator::CBloodyScoreCalculator(void)
{
	memset( m_abWin, 0, sizeof( m_abWin ) );
}

CBloodyScoreCalculator::~CBloodyScoreCalculator(void)
{
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CBloodyScoreCalculator::AddScore( TKACKMAHJONGWINDETAILEX & reqMahjongWin, int anShowSeat[], int cnShowSeat )
{
	int nWinSeat = reqMahjongWin.nWinSeat;
	int nPaoSeat = reqMahjongWin.nPaoSeat;
	ASSERT( IsValidSeat( reqMahjongWin.nWinSeat ) ) ;
	ASSERT( IsValidSeat( reqMahjongWin.nPaoSeat ) ) ;

	// 先加入基本分
	int nScore = GetScore( SCORE_TYPE_BASE );

	// 再加入和牌分
	nScore += reqMahjongWin.sWinInfo.nMaxFans;

	// 先乘上基数
	nScore *= m_nMulti;
	nScore *= m_nDouble;
	reqMahjongWin.nBaseScore = m_nMulti;

	if ( nWinSeat == nPaoSeat )
	{
		// 自摸
		for ( int j = 0; j < MAX_PLAYER_COUNT; ++j )
		{
			if ( 0 == m_pnPlayerMulti[ j ] || j == nWinSeat || m_abWin[ j ] )
			{
				// 没人
				continue;
			}

			reqMahjongWin.asScoreDetail[ nWinSeat ].anScore[ 5 ] += nScore;
			reqMahjongWin.asScoreDetail[ j ].anScore[ 5 ] -= nScore;
			// 最终手
			reqMahjongWin.asScoreDetail[ j ].anScore[ 4 ] = reqMahjongWin.asScoreDetail[ j ].anScore[ 5 ];
		}
	}
	else
	{
		reqMahjongWin.asScoreDetail[ nWinSeat ].anScore[ 5 ] += nScore;
		reqMahjongWin.asScoreDetail[ nPaoSeat ].anScore[ 5 ] -= nScore;
		// 最终手
		reqMahjongWin.asScoreDetail[ nPaoSeat ].anScore[ 4 ] = reqMahjongWin.asScoreDetail[ nPaoSeat ].anScore[ 5 ];
	}
	// 最终手
	reqMahjongWin.asScoreDetail[ nWinSeat ].anScore[ 4 ] = reqMahjongWin.asScoreDetail[ nWinSeat ].anScore[ 5 ];


	// 计算所有玩家实际分数
	for ( int i = 0; i < MAX_PLAYER_COUNT; ++i )
	{
		if ( 0 == m_pnPlayerMulti[ i ] || i == nWinSeat || m_abWin[ i ] )
		{
			// 没人
			continue;
		}

		// 实际输分
		int nLossScore = -reqMahjongWin.asScoreDetail[ i ].anScore[ 5 ];
		ASSERT( nLossScore >= 0 );

        if ( m_pRule->GetRule( RULE_SCORE_LIMIT ) 
            && !(m_pRule->GetRule(RULE_SCORE_LOWER_LIMIT) == 1
            && (m_pTKGame->m_nScoreType == TK_GAME_SCORETYPE_CHIP
            || m_pTKGame->m_nScoreType == TK_GAME_SCORETYPE_LIFE)))
		{
			// 有上下封顶限制
			// 不能输成负分
			nLossScore = min( nLossScore, m_pnPlayerLeftScore[ i ] );
			// 也不能超过和牌玩家拥有的筹码数
			nLossScore = min( nLossScore, m_pnPlayerLeftScore[ nWinSeat ] );
			reqMahjongWin.asScoreDetail[ i ].anScore[ 6 ] = -nLossScore;
		}
        else
        {
            reqMahjongWin.asScoreDetail[ i ].anScore[ 6 ] = -nLossScore;
        }

		// 累计和牌人实际得分
		reqMahjongWin.asScoreDetail[ nWinSeat ].anScore[ 6 ] += nLossScore;
	}

	// 标识有人和牌了
	m_abWin[ nWinSeat ] = TRUE;
}




// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CBloodyScoreCalculator::GetPlayerScore( TKREQMAHJONGRESULT & reqMahJongResult )
{
	for( int i = 0 ; i < MAX_PLAYER_COUNT ; i ++ )
	{ // 遍历所有的玩家
		if ( 0 == m_pnPlayerMulti[ i ] )
		{
			// 这个座位上没人
			continue;
		}

		reqMahJongResult.asPlayerResult[ i ].nScore = m_pnPlayerLeftScore[ i ] - m_pnPlayerScore[ i ] ;
		reqMahJongResult.asPlayerResult[ i ].nTotalScore = m_pnPlayerLeftScore[ i ];
	}
}