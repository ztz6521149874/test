#include "IScoreCalculator.h"
#include "GameRule.h"
#include <algorithm>
#include <math.h>

#include <assert.h>

#ifndef ASSERT
#define ASSERT	assert
#endif

// 比较两个分数信息是否相同的类
// 用于ScoreType的比较函数子
class SameScore
{
private :
	int m_nType ;
	int m_nSeat ;
public :
	SameScore( int nType , int nSeat ) : m_nType( nType ) , m_nSeat( nSeat )
	{
	}
	bool operator() ( ONETIMESCORE score )
	{
		if( ( -1 != m_nType ) && ( m_nType != score.nType ) )
		{ // 要比较类型，并且类型不同
			return false ;
		}

		if( ( -1 != m_nSeat ) && ( m_nSeat != score.nSeat ) )
		{ // 要比较加分座位号，并且加分座位号不同
			return false ;
		}

		return true ;
	}
} ;




// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
IScoreCalculator::IScoreCalculator()
{
	m_nPlayerCount  = 0 ;
	m_nMulti        = 0 ;
	m_nDouble		= 1;

	memset( m_pnScore , 0 , sizeof( m_pnScore ) ) ;

	memset( m_pnPlayerScore , 0 , sizeof( m_pnPlayerScore ) ) ;
	memset( m_pnPlayerLeftScore , 0 , sizeof( m_pnPlayerLeftScore ) ) ;
	memset( m_pnPlayerMulti , 0 , sizeof( m_pnPlayerMulti ) ) ;

	m_pRule = NULL;
    m_pTKGame = NULL;
}




// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
IScoreCalculator::~IScoreCalculator()
{
    m_pTKGame = NULL;
}




// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
BOOL IScoreCalculator::InitInstance
(
    CGameRule* pRule, 
    int nPlayerCount, 
    int nMulti,
    CTKGame* in_pTKGame
)
{
	ASSERT( nPlayerCount > 0 && nPlayerCount <= MAX_PLAYER_COUNT ) ;
	ASSERT( nMulti > 0 ) ;
	m_nPlayerCount  = nPlayerCount ;
	m_nMulti        = nMulti;

	memset( m_pnScore , 0 , sizeof( m_pnScore ) ) ;

	m_pRule = pRule;

    m_pTKGame = in_pTKGame;

	return TRUE ;
}





// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
BOOL IScoreCalculator::ExitInstance()
{
	return TRUE ;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void IScoreCalculator::ResetData()
{
	m_pnScore[ SCORE_TYPE_WIN ]        = 0 ;	// 重置和牌的分数
	m_pnScore[ SCORE_TYPE_PLAY_AWARD ] = 0 ;	// 重置这一盘的累计彩金
	m_pnScore[ SCORE_TYPE_PLAY_TASK ]  = 0 ;	// 重置这一盘的任务彩金
	m_Score.clear() ;		// 清除这一盘的分数信息
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void IScoreCalculator::SetPlayerScore( int* pnPlayerScore , int* pnPlayerMulti )
{
	ASSERT( NULL != pnPlayerScore && NULL != pnPlayerMulti ) ;
	memcpy( m_pnPlayerScore , pnPlayerScore , sizeof( m_pnPlayerScore ) ) ;
	memcpy( m_pnPlayerLeftScore , m_pnPlayerScore , sizeof( m_pnPlayerLeftScore ) ) ;
	memcpy( m_pnPlayerMulti , pnPlayerMulti , sizeof( m_pnPlayerMulti ) ) ;
	for( int i = 0 ; i < MAX_PLAYER_COUNT ; i ++ )
	{ // 遍历这些玩家
		if( m_pnPlayerMulti[ i ] < 0 )
		{ // 倍数不合法
			m_pnPlayerMulti[ i ] = 1 ;	// 缺省是1倍
		}
	}
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
int IScoreCalculator::AddPlayerScore( int nSeat, int nScore )
{
	m_pnPlayerLeftScore[ nSeat ] += nScore;
	return m_pnPlayerLeftScore[ nSeat ];
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void IScoreCalculator::SetScore( int nType , int nScore )
{
	ASSERT( IsValidType( nType ) ) ;

	m_pnScore[ nType ] = nScore ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
int IScoreCalculator::GetScore( int nType )
{
	ASSERT( IsValidType( nType ) ) ;

	return m_pnScore[ nType ];
}

void IScoreCalculator::SetDouble( int cnDouble )
{
	m_nDouble = (int)pow( 2.0f, cnDouble );
}


void IScoreCalculator::ChangeRTScore(int nWinSeat, int aScores[])
{
	aScores[nWinSeat] = 0;
	for (int i = 0; i < MAX_PLAYER_COUNT; ++i)
	{
		if (i == nWinSeat)
		{
			continue;
		}

		int nLossScore = -aScores[i];
		//积分上下限
		if (m_pRule->GetRule(RULE_SCORE_LIMIT) &&
			!(m_pRule->GetRule(RULE_SCORE_LOWER_LIMIT) == 1
				&& (m_pTKGame->m_nScoreType == TK_GAME_SCORETYPE_CHIP || m_pTKGame->m_nScoreType == TK_GAME_SCORETYPE_LIFE)))
		{
			nLossScore = min(nLossScore, m_pnPlayerScore[i]);
			nLossScore = min(nLossScore, m_pnPlayerScore[nWinSeat]);
		}

		aScores[i] = -nLossScore;
		m_pnPlayerScore[i] += aScores[i];
		m_pnPlayerLeftScore[i] += aScores[i];

		aScores[nWinSeat] += nLossScore;
	}

	m_pnPlayerScore[nWinSeat] += aScores[nWinSeat];
	m_pnPlayerLeftScore[nWinSeat] += aScores[nWinSeat];
}

// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void IScoreCalculator::AddScore( int nType , int nAddScoreSeat , int nDecScoreSeat )
{
	ASSERT( IsValidType( nType ) ) ;
	int nScore = GetScore( nType ) * m_nMulti * m_nDouble;
	if( 0 == nScore )
	{ // 没有这种得分
		return ;
	}

	ONETIMESCORE score = { nType , nAddScoreSeat , nDecScoreSeat , 3 } ;
	int i = 0 ;

	switch( nType )
	{
	case SCORE_TYPE_BASE :		// 基本分( 加基本分的时候肯定只有一个加分的玩家 )
		ASSERT( IsValidSeat( nAddScoreSeat ) ) ;
		ASSERT( -1 == nDecScoreSeat ) ;
		for( i = 0 ; i < MAX_PLAYER_COUNT ; i ++ )
		{ // 遍历所有的玩家
			if( i == nAddScoreSeat ) continue ;	// 略过赢家
			if ( 0 == m_pnPlayerMulti[ i ] )
			{
				// 这个座位上没人
				continue;
			}

			score.pnScore[ i ] = -nScore ;
			score.pnScore[ nAddScoreSeat ] += nScore ;
		}
		break ;

	case SCORE_TYPE_WIN :		// 和牌得分
		ASSERT( FALSE ) ;
		break ;

	case SCORE_TYPE_ZHI_GANG :	// 直杠明杠
		ASSERT( IsValidSeat( nAddScoreSeat ) ) ;
		ASSERT( IsValidSeat( nDecScoreSeat ) ) ;
		nScore *= m_pnPlayerMulti[ nAddScoreSeat ] * m_pnPlayerMulti[ nDecScoreSeat ] ;

		score.pnScore[ nAddScoreSeat ] = nScore ;
		score.pnScore[ nDecScoreSeat ] = -nScore ;
		break ;

	case SCORE_TYPE_BU_GANG :	// 补杠明杠
	case SCORE_TYPE_AN_GANG :	// 暗杠
	case SCORE_TYPE_HUN_GANG :	// 混杠
		ASSERT( IsValidSeat( nAddScoreSeat ) ) ;
		ASSERT( -1 == nDecScoreSeat ) ;
		for( i = 0 ; i < MAX_PLAYER_COUNT ; i ++ )
		{ // 遍历所有的玩家
			if( i == nAddScoreSeat ) continue ;	// 略过赢家
			if ( 0 == m_pnPlayerMulti[ i ] )
			{
				// 这个座位上没人
				continue;
			}

			nScore = GetScore( nType ) * m_nMulti  * m_nDouble;	// 得分
			nScore *= m_pnPlayerMulti[ nAddScoreSeat ] * m_pnPlayerMulti[ i ] ;

			score.pnScore[ i ] = -nScore ;
			score.pnScore[ nAddScoreSeat ] += nScore ;
		}
		break ;

	case SCORE_TYPE_PLAY_TASK :			// 游戏任务
		ASSERT( IsValidSeat( nAddScoreSeat ) ) ;
		ASSERT( -1 == nDecScoreSeat ) ;
		nScore *= m_pnPlayerMulti[ nAddScoreSeat ] ;	// 需要考虑玩家设置的倍数
		score.pnScore[ nAddScoreSeat ] = nScore ;
		score.nStyle = 1 ;
		break ;
	}

	m_Score.push_back( score ) ;
}




// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void IScoreCalculator::AddScore(TKACKMAHJONGWINDETAIL & reqMahjongWin, int anShowSeat[], int cnShowSeat)
{
    int nWinSeat = reqMahjongWin.nWinSeat;
    int nPaoSeat = reqMahjongWin.nPaoSeat;
    ASSERT(IsValidSeat(reqMahjongWin.nWinSeat));
    ASSERT(IsValidSeat(reqMahjongWin.nPaoSeat));

    // 先加入基本分
    AddScore(SCORE_TYPE_BASE, nWinSeat, -1);

    // 然后是加入牌分
    ONETIMESCORE score = { SCORE_TYPE_WIN , nWinSeat , 3 };

    // 如果是自摸,先乘上系数
    CGameRule * pGameRule = m_pRule;
    int nScore = reqMahjongWin.sWinInfo.nScoreOfFan;

    // 再乘上基数
    nScore *= m_nMulti;
    nScore *= m_nDouble;

    if (nWinSeat == nPaoSeat)
    { // 自摸和
        for (int i = 0; i < MAX_PLAYER_COUNT; i++)
        { // 遍历这些玩家
            if (i == nWinSeat) continue;	// 略过赢家
            if (0 == m_pnPlayerMulti[i]) continue;	// 这个座位上没人

            int nSeatScore = nScore * m_pnPlayerMulti[nWinSeat] * m_pnPlayerMulti[i];
            score.pnScore[i] = -nSeatScore;
            score.pnScore[nWinSeat] += nSeatScore;
        }
    }
    else
    { // 放炮和
        nScore *= m_pnPlayerMulti[nWinSeat] * m_pnPlayerMulti[nPaoSeat];
        score.pnScore[nWinSeat] = nScore;
        score.pnScore[nPaoSeat] = -nScore;
    }

    m_Score.push_back(score);
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void IScoreCalculator::DelScore( int nType , int nSeat )
{
	if( -1 != nType )
	{ // 要考虑类型
		if( 0 == GetScore( nType ) )
		{ // 没有这种得分
			return ;
		}
	}

	SameScore obj( nType , nSeat ) ;
	ScoreIterator iter = remove_if( m_Score.begin() , m_Score.end() , obj ) ;
	m_Score.erase( iter , m_Score.end() ) ;
}




#if defined(DEBUG)||defined(_DEBUG)
// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void IScoreCalculator::Trace()
{
	PONETIMESCORE pScore = NULL ;
	ScoreIterator iter ;
	for( iter = m_Score.begin() ; iter != m_Score.end() ; iter ++ )
	{ // 遍历所有的分数
		pScore = &( *iter ) ;
		/*FOR TWO PLAYER*/
		TKWriteLog( TEXT( "type(%d),style(%d),addseat(%d),score(%d,%d)" ) , pScore->nType , pScore->nStyle , pScore->nSeat, pScore->pnScore[ 0 ] , pScore->pnScore[ 1 ]) ;
	}
}
#endif





// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
BOOL IScoreCalculator::IsValidType( int nType )
{
	return ( ( 0 <= nType ) && ( nType < sizeof( m_pnScore ) / sizeof( m_pnScore[ 0 ] ) ) ) ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
BOOL IScoreCalculator::IsValidSeat( int nSeat )
{
	return ( ( nSeat >= 0 ) && ( nSeat < MAX_PLAYER_COUNT ) && ( 0 != m_pnPlayerMulti[ nSeat ] ) ) ;
}