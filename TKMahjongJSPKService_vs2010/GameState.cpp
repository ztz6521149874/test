#include "GameState.h"
#include "assert.h"
#include "TKMahjongGame.h"
#include "TKMahjongServerPlayer.h"
#include "ScoreCalculator.h"
#include "TKMahjongServerObj.h"
#include "RequestObj.h"
#include "MahJongTile.h"
#include <math.h>
#include "TKMahjongJSPKService.h"
#include "TKMahjongGamePlayer.h"

#ifndef ASSERT
#define ASSERT	assert
#endif

// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
IGameState::IGameState()
{
	m_pMahjongGame = NULL;
	m_pMahjongObj = NULL;
	m_bCheckSeat = true;
}



// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
BOOL IGameState::Broadcast( PTKHEADER pMsg ) 
{ 
	ASSERT( NULL != m_pMahjongGame );
	return m_pMahjongGame->GameBroadcast( pMsg ); 
}



// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
BOOL IGameState::Send2SeatPlayer( int nSeat, PTKHEADER pMsg ) 
{ 
	return m_pMahjongGame->GameSend2SeatPlayer( nSeat, pMsg );
}



// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
bool IGameState::CheckMsgSeatAndLen( DWORD dwUserID, int nSeat, int nMsgLen, PTKHEADER pHeader, const char* pcszFile, int nLine )
{
	if ( nSeat < 0 || nSeat >=  MAX_PLAYER_COUNT )
	{
		TKWriteLog( "%s : %d, Error!!!nSeat( %d ) Invalidate", __FILE__, __LINE__, nSeat );
		return false;
	}
	if ( NULL == pHeader )
	{
		TKWriteLog( "%s : %d, Error!!! pHeader == NULL", pcszFile, nLine );
		return false;
	}

	int line = __LINE__;
	try
	{

	// 先看看这个座位上有没有人
	CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerBySeat( nSeat );
	line = __LINE__;
	if ( NULL == pPlayer )
	{
		line = __LINE__;
		TKWriteLog( "[%s:%d] %d sendmsg , the seat( %d ) is NULL ", pcszFile, nLine , dwUserID, nSeat ) ;
		line = __LINE__;
		TCHAR szError[ 128 ] = { 0 };
		line = __LINE__;
		wsprintf( szError, " 发送消息的UserID( %0x8 )Seat( %d )未找到 ", dwUserID, nSeat );
		line = __LINE__;
		PostErrorMsg( szError );
		line = __LINE__;
		return false;
	}
	line = __LINE__;

	// 再看看dwUserID是否对得上
	if ( m_bCheckSeat )
	{
		line = __LINE__;
		// 需要检查
		if ( dwUserID != pPlayer->UserID() )
		{
			line = __LINE__;
			TKWriteLog( "[%s:%d] Msg Sender( %d ), The seat player( %d )", pcszFile , nLine, dwUserID, pPlayer->UserID() ) ;
			line = __LINE__;
			TCHAR szError[ 128 ] = { 0 };
			line = __LINE__;
			wsprintf( szError, " 发送消息的UserID( %0x8 )不是Seat( %d )上的UserID( %08x) ", dwUserID, nSeat, pPlayer->UserID() );
			line = __LINE__;
			PostErrorMsg( szError );
			line = __LINE__;
			return false;
		}
	}
	line = __LINE__;

	// 再看看消息长度是否对得上
	if ( nMsgLen != pHeader->dwLength )
	{
		line = __LINE__;
		TKWriteLog( "[%s:%d] %d send msgLen=%d,Len=%d" , pcszFile , nLine , dwUserID, nMsgLen , pHeader->dwLength ) ;
		line = __LINE__;
		TCHAR szError[ 128 ] = { 0 };
		line = __LINE__;
		wsprintf( szError, " UserID( %0x8 )Seat( %d )消息长度( %d )不对，应为( %d ) ", dwUserID, nSeat, pHeader->dwLength, nMsgLen );
		line = __LINE__;
		PostErrorMsg( szError );
		line = __LINE__;
		return false;
	}
	line = __LINE__;

	}
	catch (...)
	{
		TKWriteLog( "%s : %d, Exception", __FILE__, line );
		TKWriteLog( "Param : this = %0x8, dwUserID = %0x8, nSeat = %d,  nMsgLen = %d, MsgID = %d, pcszFile = %s, nLine = %d", 
			this, dwUserID, nSeat, nMsgLen, pHeader->dwType, pcszFile, nLine );
		if ( NULL != this )
		{
			TKWriteLog( "this->m_bCheckSeat = %d, this->m_nLastEventID = %d, this->m_nTKOjbectID = %d, this->m_pFSMachine = %0x8, this->m_pMahjongGame = %0x8, this->m_pMahjongObj = %0x8, this->m_szName = %s, this->m_vecEventMap.size() = %d", 
				this->m_bCheckSeat, this->m_nLastEventID, this->m_nTKOjbectID, this->m_pFSMachine, this->m_pMahjongGame, this->m_pMahjongObj, this->m_szName, this->m_vecEventMap.size() );
		}

		return false;
	}

	return true;
}



// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
bool IGameState::CheckMsg( DWORD dwUserID, int nSeat, int nMsgLen, PTKHEADER pHeader, int nRequestID, const char* pcszFile, int nLine )
{
	// 先检查座位号和消息长度
	if ( !CheckMsgSeatAndLen( dwUserID, nSeat, nMsgLen, pHeader, pcszFile, nLine ) )
	{
		return false;
	}

	// 再检查是否预期的消息
	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	if( !pRequestObj->IsRequestID( nRequestID ) )
	{ // 请求的ID号不对
		//TKWriteLog( TEXT( "%s : %d, UserID( %d )Seat( %d )消息请求ID( %d )不对，应为( %d )" ) , __FILE__, __LINE__, dwUserID, nSeat , nRequestID, pRequestObj->RequestID() ) ;
		TCHAR szError[ 128 ] = { 0 };
		wsprintf( szError, " UserID( %d )Seat( %d )消息请求ID( %d )不对，应为( %d ) ", dwUserID, nSeat , nRequestID, pRequestObj->RequestID() );
		PostErrorMsg( szError );
		return false ;
	}
	if( !pRequestObj->IsRequestSeat( nSeat ) )
	{ 
		TKWriteLog( TEXT( "%s : %d, 没有向这个玩家UserID( %08x )Seat( %d )发过请求" ), __FILE__, __LINE__, dwUserID, nSeat );
		TCHAR szError[ 128 ] = { 0 };
		wsprintf( szError, " 没有向这个玩家UserID( %08x )Seat( %d )发过请求 ", dwUserID, nSeat );
		PostErrorMsg( szError );
		return false ;
	}
	if( pRequestObj->IsAckRequestSeat( nSeat ) )
	{ // 这个玩家已经回复请求了
		TKWriteLog( TEXT( "%s : %d, 玩家UserID( %08x )Seat( %d )已回复了" ), __FILE__, __LINE__, dwUserID, nSeat );
		TCHAR szError[ 128 ] = { 0 };
		wsprintf( szError, " 玩家UserID( %08x )Seat( %d )已回复了 ", dwUserID, nSeat );
		PostErrorMsg( szError );
		return false ;
	}

	return true;
}




// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
CGameRule * IGameState::Rule() 
{ 
	return m_pMahjongGame->Rule(); 
}



// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
IScoreCalculator * IGameState::Calculator()
{ 
	return m_pMahjongGame->Calculator();
}



// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
int IGameState::CheckAckRequest()
{
	// 先看看是否是所有人都回复了
	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	if( !pRequestObj->AllAckRequest() )
	{ // 还有人没有回复
		return -1;
	}

	return pRequestObj->GetOKAckRequestType() ;	// 玩家回复的最高级别的类型
}



bool IGameState::HasPlayer( int nSeat )
{
	return m_pMahjongObj->HasPlayer( nSeat );
}



// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
void IGameState::Notify( int nNotify, const char* pszText )
{
	char szTemp[ sizeof( TKREQMAHJONGNOTIFY ) + 1024 ] = { 0 } ;
	PTKREQMAHJONGNOTIFY pReqMahJongNotify = ( PTKREQMAHJONGNOTIFY ) szTemp ;

	pReqMahJongNotify->header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_NOTIFY ) ;
	pReqMahJongNotify->header.dwLength = MSG_LENGTH( TKREQMAHJONGNOTIFY ) ;
	pReqMahJongNotify->nNotify         = nNotify ;
	if( NULL != pszText )
	{ // 要客户端显示或者更新一条信息，并且提供了这个文本信息
		int nLen = 0 ;
		if( MAHJONG_NOTIFY_DISABLE_RECEIVE == nNotify )
		{ // 暂停接收网络消息
			nLen = sizeof( int ) ;	// 消息后面跟的是一个int
		}
		else
		{ // 其他情况
// 			ASSERT( MAHJONG_NOTIFY_SHOW_CHAT_MSG == nNotify || MAHJONG_NOTIFY_URL_HELP == nNotify 
// 				|| MAHJONG_NOTIFY_URL_MEMBER == nNotify || MAHJONG_NOTIFY_URL_MONEY == nNotify 
// 				|| MAHJONG_NOTIFY_URL_FEEDBACK == nNotify  || MAHJONG_NOTIFY_ERROR == nNotify ) ;
			nLen = strlen( pszText ) + 1 ;	// 字符串的长度
		}
		if( nLen < 1024 )
		{ // 长度没有越界
			char *p = ( char* ) ( pReqMahJongNotify + 1 ) ;	// 指向消息尾部
			memcpy( p , pszText , nLen ) ;
			pReqMahJongNotify->header.dwLength += nLen ;
		}
	}

	if ( !Broadcast( ( PTKHEADER )pReqMahJongNotify ) )
	{
		TKWriteLog( "%s : %d, Error! 发送消息出错, len( %d )", __FILE__, __LINE__, pReqMahJongNotify->header.dwLength );
	}
}



// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
CPrepareState::CPrepareState( /*std::string & szName*/ )// : IState( szName )
{
	// 注册感兴趣的事件
	//RegistEventFunction( EVENT_START_GAME, OnStartGame );
	REGIST_EVENT_FUNCTION( EVENT_START_GAME, &CPrepareState::OnStartGame );
}



// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
CPrepareState::~CPrepareState()
{

}



// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
bool CPrepareState::OnStartGame( CEvent * pEvent )
{
	// 发送游戏基本信息给客户端
	BroadcastGameBaseMsg() ;

	// 定庄、定游戏座位号、门风、圈风
	SetPlace() ;

	return true;
}



void CPrepareState::DecideGameSeatAndBanker() 
{
	// 先定游戏座位号
	// 默认只有一种顺序:座位号按顺时针排列,游戏方位号按逆时针顺序
	int anDefaultPlace[ MAX_PLAYER_COUNT ] = {1, 0}; /*FOR TWO PLAYER*/
	m_pMahjongObj->SetGameSeatAndBanker( anDefaultPlace , 0 ) ;
	m_pMahjongObj->ResetCallInfo();

	// 确定庄家信息
	int nBanker = m_pMahjongObj->Banker();
	int * pnSeat = (int *)m_pMahjongGame->m_pShareGameData;
	if (nBanker < 0)
	{
		if (0 == m_pMahjongGame->m_iGameHand)
		{
			// 这是第一局,采用随机定庄
			nBanker = TKGenRandom() % m_pMahjongObj->PlayerCount();	// 随机选择庄家的座位号
		}
		else
		{
			nBanker = m_pMahjongObj->GetNextPlayerSeat(*pnSeat, false);
		}
	}

	m_pMahjongObj->Banker( nBanker );

	// 保存本局的庄家信息
	*pnSeat = nBanker;

	// 通知客户端
	TKREQMAHJONGPLACE reqMahJongPlace = { 0 } ;
	reqMahJongPlace.header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_PLACE ) ;
	reqMahJongPlace.header.dwLength = MSG_LENGTH( TKREQMAHJONGPLACE ) ;
	reqMahJongPlace.nBanker         = nBanker;
	for( int i = 0; i < MAX_PLAYER_COUNT; i ++ )
	{ // 先填充所有玩家的游戏方位号信息
		reqMahJongPlace.anGameSeat[ anDefaultPlace[ i ] ] = i;
/*
		CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerBySeat( i );
		if ( NULL != pPlayer )
		{
			reqMahJongPlace.anGameSeat[ i ] = pPlayer->GameSeat() ;
		}
*/
	}

	// 发送给observer型的旁观者
	if ( !m_pMahjongGame->GameSend2Observer( ( PTKHEADER )&reqMahJongPlace ) )
	{
		TKWriteLog( "%s : %d, Error, 发送消息错误， len( %d )", __FILE__, __LINE__, reqMahJongPlace.header.dwLength );
	}

	// 发送给black型的旁观者
	if ( !m_pMahjongGame->GameSend2Black( ( PTKHEADER )&reqMahJongPlace ) )
	{
		TKWriteLog( "%s : %d, Error, 发送消息错误， len( %d )", __FILE__, __LINE__, reqMahJongPlace.header.dwLength );
	}

	DWORD dwDifMs = m_pMahjongGame->GetRtValue();
	CTKBuffer bufGameAction;
	bufGameAction.AppendFormatText("<a id=\"%d\" rt=\"%d\" rtms=\"%d\" o=\"%d\" gsl=\"%d,%d\" v=\"1.1.0\" cmp=\"%d\" />",
		RECORD_BANKER,
		dwDifMs / 1000,
		dwDifMs,
		nBanker,
		reqMahJongPlace.anGameSeat[ 0 ],
		reqMahJongPlace.anGameSeat[ 1 ],
		m_pMahjongGame->IsChampionhship());
	m_pMahjongGame->AddGameAction( bufGameAction.GetBufPtr(), true);

	// 按座位号向所有用户发送游戏座位号信息
	for(int i = 0 ; i < MAX_PLAYER_COUNT ; i ++ )
	{ // 遍历各个座位的玩家
		CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerBySeat( i );
		if ( NULL != pPlayer )
		{
			reqMahJongPlace.nRules = pPlayer->Rules() ;
			if (m_pMahjongGame)
			{
				if (m_pMahjongGame->IsHaveplayerSeat(i))
				{
					if ( !Send2SeatPlayer( i, ( PTKHEADER )&reqMahJongPlace ) )
					{
						TKWriteLog( "%s : %d, Error! 发送消息错误 len( %d )", __FILE__, __LINE__, reqMahJongPlace.header.dwLength );
					}
				}
			}
		}
	}
}



// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
void CPrepareState::SetPlace()
{
	DecideGameSeatAndBanker() ;	// 确定各家的游戏座位号和庄家
	int nRoundWind = (m_pMahjongGame->m_iGameHand % 4)/2*2;		// %4 结果为0 1 2 3 /2结果为(0 1)0 (2 3)1 *2结果为(0 1)0 (2 3)2
	m_pMahjongObj->SetWind( nRoundWind ) ;	// 设置圈风和门风信息

	// 发送圈风门风信息给所有客户端
	TKREQMAHJONGWINDPLACE reqWindPlace = { 0 };
	reqWindPlace.header.dwType = REQ_TYPE( TK_MSG_MAHJONG_WIND_PLACE );
	reqWindPlace.header.dwLength = MSG_LENGTH( TKREQMAHJONGWINDPLACE );
	reqWindPlace.nRoundWind = nRoundWind;

	if ( !Broadcast( ( PTKHEADER )&reqWindPlace ) )
	{
		TKWriteLog( "%s : %d, Error! SetWindPlace() 发送消息出错 长度( %d )", __FILE__, __LINE__, reqWindPlace.header.dwLength );
	}
}



// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
void CPrepareState::BroadcastGameBaseMsg()
{
	// 应该不用发,客户端自已解析字符串
}

// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
CShuffleState::CShuffleState()
{
	// 注册感兴趣的事件
	REGIST_EVENT_FUNCTION( EVENT_SHUFFLE_END, &CShuffleState::OnShuffleEnd );
}



// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
CShuffleState::~CShuffleState()
{

}



// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
void CShuffleState::OnEnter( CEvent * pEvent )
{
	// 开始洗牌
	Shuffle();
}



// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
void CShuffleState::Shuffle()
{
	// 这里并不是真正的洗牌，而仅仅是把各家需要砌的牌的墩数告诉大家
	TKREQMAHJONGSHUFFLE reqMahJongShuffle = { 0 } ;

	reqMahJongShuffle.header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_SHUFFLE ) ;
	reqMahJongShuffle.header.dwLength = MSG_LENGTH( TKREQMAHJONGSHUFFLE ) ;
	reqMahJongShuffle.cnTile = m_pMahjongObj->AllTileCount();

	// 通知OBJ对象开始洗牌
	m_pMahjongObj->Shuffle( reqMahJongShuffle.anFrustaOfSeat );

    // 墩数（修正）
    reqMahJongShuffle.anFrustaOfSeat[0] = reqMahJongShuffle.anFrustaOfSeat[1] = reqMahJongShuffle.cnTile / 4;

	if ( !Broadcast( ( PTKHEADER )&reqMahJongShuffle ) )
	{
		TKWriteLog( "%s : %d, Error! Shuffle()发送消息出错 len( %d ) ", __FILE__, __LINE__, reqMahJongShuffle.header.dwLength );
	}

	DWORD dwDifMs = m_pMahjongGame->GetRtValue();
	CTKBuffer bufGameAction;
	bufGameAction.AppendFormatText("<a id=\"%d\" rt=\"%d\" rtms=\"%d\" o=\"%d\" fl=\"%d,%d\" />",
		RECORD_SHUFFLE,
		dwDifMs / 1000,
		dwDifMs,
		reqMahJongShuffle.cnTile,
		reqMahJongShuffle.anFrustaOfSeat[ 0 ],
		reqMahJongShuffle.anFrustaOfSeat[ 1 ]);
	m_pMahjongGame->AddGameAction( bufGameAction.GetBufPtr() );

	// 通知游戏机,洗牌完毕
	PostEvent( EVENT_SHUFFLE_END );
}



// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
bool CShuffleState::OnShuffleEnd( CEvent * pEvent )
{
	// 什么都不用做,只是想返回true,好让状态机转换状态
	return true;
}



// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
CCastDiceState::CCastDiceState()
{
	// 注册感兴趣的事件
	REGIST_EVENT_FUNCTION( EVENT_CASTDICE_END, &CCastDiceState::OnCastDiceEnd );
}


// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
CCastDiceState::~CCastDiceState()
{

}



// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
void CCastDiceState::OnEnter( CEvent * pEvent )
{
	CastDice();
}



// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
void CCastDiceState::CastDice()
{
	// 掷骰子开牌

	int pnDicePoint[ 2 * 2 ] = { 0 } ;	// 一次掷2个骰子，一共掷2次
	char szTempCastDice[ sizeof( TKREQMAHJONGCASTDICE ) + sizeof( int ) * MAX_DICE_COUNT ] = { 0 } ;
	PTKREQMAHJONGCASTDICE pReqMahJongDice = ( PTKREQMAHJONGCASTDICE ) szTempCastDice ;
	int *pnDice = ( int * ) ( pReqMahJongDice + 1 ) ;
	pReqMahJongDice->header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_CAST_DICE ) ;
	pReqMahJongDice->header.dwLength = MSG_LENGTH( TKREQMAHJONGCASTDICE ) ;
	pReqMahJongDice->cnDice		     = 2 ;	// 都是掷2个骰子
	pReqMahJongDice->header.dwLength += pReqMahJongDice->cnDice * sizeof( int ) ;

	// 第一次掷骰子决定从哪个玩家手中开始开牌
	int nBanker = m_pMahjongObj->Banker() ;	// 庄家的座位号
	pReqMahJongDice->nSeat         = nBanker ;	// 庄家掷骰子
	pReqMahJongDice->nCastDiceType = CAST_DICE_TYPE_OPEN_DOOR ;	// 为了开牌掷骰子
	pnDice[ 0 ] = TKGenRandom() % 6 + 1;
	pnDice[ 1 ] = TKGenRandom() % 6 + 1 ;
	memcpy( pnDicePoint , pnDice , 2 * sizeof( int ) ) ;

	if ( !Broadcast( ( PTKHEADER )pReqMahJongDice ) )
	{
		TKWriteLog( "%s : %d, Error! 发送消息出错, len( %d )", __FILE__, __LINE__, pReqMahJongDice->header.dwLength );
	}

	// 第二次掷骰子决定从该玩家手中从右往左第多少墩开始摸牌
	int nGameSeat = m_pMahjongObj->GetPlayerBySeat( nBanker )->GameSeat() ;	// 庄家的游戏座位号
	pReqMahJongDice->nSeat         = m_pMahjongObj->GetSeatByGameSeat( ( nGameSeat + pnDice[ 0 ] + pnDice[ 1 ] - 1 ) % MAX_PLAYER_COUNT );// TODO:暂时这么干m_pMahjongObj->GetPlayerByGameSeat( ( nGameSeat + pnDice[ 0 ] + pnDice[ 1 ] - 1 ) % PlayerCount() )->Seat() ;
	pReqMahJongDice->nCastDiceType = CAST_DICE_TYPE_OPEN_DOOR_2 ;	// 为了开牌掷骰子
	pnDice[ 0 ] = TKGenRandom() % 6 + 1 ;
	pnDice[ 1 ] = TKGenRandom() % 6 + 1 ;
    memcpy(pnDicePoint + 2, pnDice, 2 * sizeof(int));

//     if (!Broadcast((PTKHEADER)pReqMahJongDice))
//     {
//         TKWriteLog("%s : %d, Error! 发送消息出错, len( %d )", __FILE__, __LINE__, pReqMahJongDice->header.dwLength);
//     }

	// 保存开牌玩家的座位号和开牌墩数信息
	m_anOpenDoor[ 0 ] = pReqMahJongDice->nSeat ;	// 开牌的玩家的座位号
	m_anOpenDoor[ 1 ] = pnDicePoint[ 0 ] + pnDicePoint[ 1 ] + pnDicePoint[ 2 ] + pnDicePoint[ 3 ] ;	// 开牌的墩数

	// 掷色子结束
	CEvent event( EVENT_CASTDICE_END, 0, ( void * )m_anOpenDoor );
	PostEvent( event );
}



// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
bool CCastDiceState::OnCastDiceEnd( CEvent * pEvent )
{
	// 什么都不用做,只是想返回true,好让状态机转换状态
	return true;
}



// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
COpenDoorState::COpenDoorState()
{
	// 注册感兴趣的事件
	REGIST_EVENT_FUNCTION( EVENT_ACK_OPEN_DOOR, &COpenDoorState::OnAckOpenDoor );
	REGIST_EVENT_FUNCTION( EVENT_OPENDOOR_END, &COpenDoorState::OnOpenDoorEnd );
	REGIST_EVENT_FUNCTION( EVENT_TRUSTPLAY, &COpenDoorState::OnTrustPlay );

	memset( m_abOpenDoor, 0, sizeof( m_abOpenDoor ) );
}



// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
COpenDoorState::~COpenDoorState()
{

}


// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
void COpenDoorState::OnEnter( CEvent * pEvent )
{
	// 记录一下进入时的时间
	m_dwStartTime = GetTickCount();

	// 设置算分子中玩家的分数和倍数信息
	int anScore[ MAX_PLAYER_COUNT ] = { 0 } ;
	int anPlayerMulti[ MAX_PLAYER_COUNT ] = { 0 } ;
	for( int i = 0 ; i < MAX_PLAYER_COUNT; i ++ )
	{ // 遍历所有玩家
		CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerBySeat( i ) ;
		if ( NULL != pPlayer )
		{
			anScore[ i ] = pPlayer->TotalScore() ;	// 这个玩家的总分
			anPlayerMulti[ i ] = 1;
		}
	}
	Calculator()->SetPlayerScore( anScore , anPlayerMulti ) ;

	// 开牌
	ASSERT( NULL != pEvent );
	int * pnOpenDoor = ( int * )pEvent->m_pParam;
	ASSERT( NULL != pnOpenDoor );
	OpenDoor( pnOpenDoor[ 0 ], pnOpenDoor[ 1 ] ) ;

    //////////////////////////////////////////////////////////////////////////
    // 增加定时器，避免客户端未通知造成的状态机状态转换延迟。
    if (m_pMahjongGame != NULL)
    {
        m_pMahjongGame->SetGameTimer(EVENT_OPENDOOR_END, m_pMahjongObj->Banker(), 5);
		//// FOR TEST
		//TKWriteLog("Set Open Door Timer! time = %u", GetTickCount());
    }
}



// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
void COpenDoorState::OpenDoor( int nOpenDoorSeat , int nOpenDoorFrusta )
{
	// 发送玩家抓的牌

	// 先确定开牌的位置信息
	m_pMahjongObj->SetOpenDoorPos( nOpenDoorSeat , nOpenDoorFrusta ) ;	// 设置开牌的位置信息

	// 发送抓牌消息给玩家
	char szTempOpenDoor[ sizeof( TKREQMAHJONGOPENDOOR ) + sizeof( int ) * ( MAX_HAND_COUNT + 1 ) ] = { 0 } ;
	PTKREQMAHJONGOPENDOOR pReqMahJongOpenDoor = ( PTKREQMAHJONGOPENDOOR ) szTempOpenDoor ;
	pReqMahJongOpenDoor->header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_OPEN_DOOR ) ;
	pReqMahJongOpenDoor->header.dwLength = MSG_LENGTH( TKREQMAHJONGOPENDOOR ) ;
	pReqMahJongOpenDoor->nOpenDoorSeat   = nOpenDoorSeat ;
	pReqMahJongOpenDoor->nOpenDoorFrusta = nOpenDoorFrusta ;

	DWORD dwDifMs = m_pMahjongGame->GetRtValue();
	CTKBuffer bufGameAction;
	bufGameAction.AppendFormatText("<a id=\"%d\" rt=\"%d\" rtms=\"%d\" dp1=\"%d\" dp2=\"%d\"",
		RECORD_OPEN,
		dwDifMs/1000,
		dwDifMs,
		nOpenDoorSeat, nOpenDoorFrusta);

    int rndRate = TKGenRandom() % 10000;
    int nAwardSeat = -1, nFlowerCount = 0;
    if (rndRate <= m_pMahjongGame->Rule()->GetRule(RULE_BIG_FLOWER_RATE))
    {
		nAwardSeat = TKGenRandom() % MAX_PLAYER_COUNT;
		nFlowerCount = m_pMahjongGame->Rule()->GetRule(RULE_CF_COUNT);
    }
	else if (rndRate <= m_pMahjongGame->Rule()->GetRule(RULE_FULL_BLOOM_RATE))
	{
		nAwardSeat = TKGenRandom() % MAX_PLAYER_COUNT;
		nFlowerCount = m_pMahjongGame->Rule()->GetRule(RULE_FB_COUNT);
	}

	for( int i = 0 ; i < MAX_PLAYER_COUNT ; i ++ )
	{ // 遍历各个座位的玩家
		CTKMahjongServerPlayer * pPlayer = ( CTKMahjongServerPlayer * )m_pMahjongObj->GetPlayerBySeat( i );
		if ( NULL == pPlayer )
		{
			// 这个座位上没人
			m_abOpenDoor[ i ] = TRUE;

			// 没人也要占个位子
			bufGameAction.AppendFormatText(",%d", 0);
			continue;
		}

		bufGameAction.AppendFormatText(" hti%d=\"", i );
        m_pMahjongObj->FillOpenDoorMsg(i, nAwardSeat, nFlowerCount, pReqMahJongOpenDoor);

		pReqMahJongOpenDoor->header.dwLength = MSG_LENGTH( TKREQMAHJONGOPENDOOR ) + sizeof( int ) * pReqMahJongOpenDoor->cnHandTile;
		pReqMahJongOpenDoor->nSeat = i;
		if (m_pMahjongGame)
		{
			if (m_pMahjongGame->IsHaveplayerSeat(i))
			{
				if ( !Send2SeatPlayer( i, ( PTKHEADER )pReqMahJongOpenDoor ) )
				{
					TKWriteLog( "%s : %d, Error! 发送消息错误 len( %d )", __FILE__, __LINE__, pReqMahJongOpenDoor->header.dwLength );
				}
			}
		}

		// 记录每个人发的牌
		char szTile[ 16 ] = { 0 };
		// 先是牌张数
		//sprintf( szTile, ",%d", pReqMahJongOpenDoor->cnHandTile );
		//strcat( szGameActionBuf, szTile );
		// 后跟pReqMahJongOpenDoor->cnHandTile张牌
		int * pTile = ( int * )( pReqMahJongOpenDoor + 1 );
		for ( int j = 0; j < pReqMahJongOpenDoor->cnHandTile; ++j )
		{
			bufGameAction.AppendFormatText("%d,", pTile[ j ] );
		}
		
		char* pLastChar = bufGameAction.GetBufPtr() + bufGameAction.GetBufLen() - 1;
		*pLastChar = '"';

		// 给Observer型的旁观者发消息
		if ( !m_pMahjongGame->GameSend2Observer( ( PTKHEADER )pReqMahJongOpenDoor ) )
		{
			TKWriteLog( "%s : %d, Error! 发送消息错误 len( %d )", __FILE__, __LINE__, pReqMahJongOpenDoor->header.dwLength );
		}

		// 给Black型的旁观者发消息
		int *pnTile = ( int * ) ( pReqMahJongOpenDoor + 1 ) ;
		memset( pnTile, 0, sizeof( int ) * pReqMahJongOpenDoor->cnHandTile );

		// 给Black型的旁观者发消息
		if ( !m_pMahjongGame->GameSend2Black( ( PTKHEADER )pReqMahJongOpenDoor ) )
		{
			TKWriteLog( "%s : %d, Error, 发送消息错误， len( %d )", __FILE__, __LINE__, pReqMahJongOpenDoor->header.dwLength );
		}

		if ( pPlayer->IsAutoPlay() ) 
		{
			// 标识断线的这个人已经开牌了,不然如果这个人迅速的回来解除托管了，他不会发开牌消息，也不是托管状态，那AllPlayerOpenDoor()
			// 这个函数将永远不会返回TRUE
			m_abOpenDoor[i] = TRUE;
		}
	}

	bufGameAction.AppendFormatText(" />");
	m_pMahjongGame->AddGameAction( bufGameAction.GetBufPtr() );

	// 看看是否所有玩家都已经开牌结束了
	if( AllPlayerOpenDoor() )
	{
		CEvent event( EVENT_OPENDOOR_END, m_pMahjongObj->Banker() );
		PostEvent( event );
	}
}



// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
bool COpenDoorState::OnAckOpenDoor( CEvent * pEvent )
{
	PTKACKMAHJONGOPENDOOR pAckMahJongOpenDoor = ( PTKACKMAHJONGOPENDOOR )pEvent->m_pParam;
	ASSERT( NULL != pAckMahJongOpenDoor );

	if ( pAckMahJongOpenDoor->header.dwLength != MSG_LENGTH( TKACKMAHJONGOPENDOOR ) )
	{
		return false;
	}

	int nSeat = pAckMahJongOpenDoor->nSeat ;	// 这个玩家的座位号

	// 检查消息中的座位号和消息长度是否合法
	if( !CheckMsgSeatAndLen( pEvent->m_dwPoster, nSeat, MSG_LENGTH( TKACKMAHJONGOPENDOOR ) , ( PTKHEADER )pAckMahJongOpenDoor, __FILE__, __LINE__ ) )
	{ // 座位号或者消息长度无效
		return false;
	}

	// 看看当前是否应该处理这个消息
	if( m_abOpenDoor[ nSeat ] )
	{ // 这个玩家已经发送了开牌结束消息了
		TKWriteLog( TEXT( "【CMahJongGame::OnAckOpenDoor】seat(%d),bOpenDoor(%d)" ), nSeat, m_abOpenDoor[ nSeat ] ) ;
		return true;
	}

	// 更新状态
	m_abOpenDoor[ nSeat ] = true ;	// 设置这个玩家已经开牌了

	// 看看是否所有玩家都已经开牌结束了
	if( AllPlayerOpenDoor() )
	{ // 所有玩家都已经开牌了
		//PostEvent( EVENT_OPENDOOR_END );
		CEvent event( EVENT_OPENDOOR_END, m_pMahjongObj->Banker() );
		PostEvent( event );
	}

	return true;
}



// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
bool COpenDoorState::AllPlayerOpenDoor()
{
	CTKMahjongServerPlayer * pPlayer = NULL ;

	for( int i = 0; i < MAX_PLAYER_COUNT; i++ )
	{ // 遍历所有玩家
		pPlayer = ( CTKMahjongServerPlayer * )m_pMahjongObj->GetPlayerBySeat( i ) ;
		if ( NULL == pPlayer )
		{
			continue;
		}

		if( !pPlayer->IsAutoPlay() && !m_abOpenDoor[ i ] )
		{ // 这个玩家服务器没有帮他游戏，并且这个玩家还没有开牌
			return false ;
		}
	}

	return true;
}



// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
bool COpenDoorState::OnOpenDoorEnd( CEvent * pEvent )
{
	m_pMahjongGame->KillGameTimer(EVENT_OPENDOOR_END);
	CGameRule * pRule = Rule();
    Notify(MAHJONG_NOTIFY_DEAL_FINISH);
	return true;
}



// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
bool COpenDoorState::OnTrustPlay( CEvent * pEvent )
{
	int nLine = __LINE__;
	try
	{
		// 标志这个人已开牌了
		nLine = __LINE__;
		CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerByID( pEvent->m_dwPoster );
		if ( NULL == pPlayer )
		{
			TKWriteLog( "%s : %d, Error! ( %d )托管了，但他不是本桌的玩家", __FILE__, __LINE__, pEvent->m_dwPoster );
			return false;
		}
		int nSeat = pPlayer->Seat();
		nLine = __LINE__;
		m_abOpenDoor[ nSeat ] = true;
		nLine = __LINE__;

		// 看看是否所有玩家都已经开牌结束了
		nLine = __LINE__;
		if( AllPlayerOpenDoor() )
		{ // 所有玩家都已经开牌了
			nLine = __LINE__;
			//PostEvent( EVENT_OPENDOOR_END );
			CEvent event( EVENT_OPENDOOR_END, m_pMahjongObj->Banker() );
			PostEvent( event );
			nLine = __LINE__;
		}
		nLine = __LINE__;

		return true;
	}
	catch (...)
	{	
		TKWriteLog( "%s : %d, Exception", __FILE__, nLine );
	}
	return false;
}

// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
CChangeAllFlowerState::CChangeAllFlowerState()
{
	// 注册感兴趣的事件
	REGIST_EVENT_FUNCTION( EVENT_CHANGE_FLOWER, &CChangeAllFlowerState::OnReqChangeFlower ); //有花消息
	REGIST_EVENT_FUNCTION( EVENT_ACK_REQUEST, &CChangeAllFlowerState::OnAckRequest ); //无花消息
	REGIST_EVENT_FUNCTION( EVENT_CHANGEALLFLOWER_END, &CChangeAllFlowerState::OnChangeAllFlowerEnd );
	REGIST_EVENT_FUNCTION( EVENT_TRUSTPLAY, &CChangeAllFlowerState::OnTrustPlay );
}



// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
void CChangeAllFlowerState::OnEnter( CEvent * pEvent )
{
    if (Rule()->GetRule(RULE_SPECIALTILE_FAN) != 0)
    {
        m_pMahjongObj->SendSpecialTileInfo();
    }
    int nWaitSecond = 0;
	if (m_pMahjongObj->CheckBigFlowerAward(nWaitSecond) && 
		m_pMahjongGame->Rule()->GetRule(RULE_CF_GAMEOVER))
	{
		m_pMahjongGame->NotifyGameOver(TK_GAMEOVERTYPE_NORNAL);
		return;
	}
	else
	{
		m_pMahjongObj->CheckFullBloomAward(nWaitSecond);
	}

	int nBanker = m_pMahjongObj->Banker() ;	// 庄家座位号

	// 对庄家手中的牌进行排序（定混之后才能正确的排序），主要是设置庄家最后抓的那张牌，用于对于庄家天和的时候进行相应的判断
	CTKMahjongPlayer *pPlayer = m_pMahjongObj->GetPlayerBySeat( nBanker ) ;
	if( NULL != pPlayer )
	{ // 有这个座位号的玩家（庄家确定了）
		pPlayer->SortHandTile() ;	// 排序
#if _DEBUG
		TKWriteLog(TEXT("【CChangeFlower】user(%d[%s]) draw tile(%s)"), pPlayer->UserID(), pPlayer->NickName(), pPlayer->GetDrawTile()->NameID());
#endif
		
	}

	// 让庄家补花
    SendRequestChangeFlowerMsg(nBanker, nWaitSecond);
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CChangeAllFlowerState::SendRequestChangeFlowerMsg(int nSeat, int nWaitSecond)
{
    int nRequestType = REQUEST_TYPE_CHANGE_FLOWER;
    CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
    pRequestObj->ResetRequest(nRequestType, nWaitSecond);

    // 先发送请求这个玩家补花的消息
    TKREQMAHJONGREQUEST reqMahJongRequest = { 0 };
    pRequestObj->FillRequestMsg(&reqMahJongRequest, nSeat);
    if (m_pMahjongGame)
    {
        if (m_pMahjongGame->IsHaveplayerSeat(nSeat))
        {
            if (!Send2SeatPlayer(nSeat, (PTKHEADER)&reqMahJongRequest))
            {
                TKWriteLog("%s : %d, Error! 发送消息错误 len( %d )", __FILE__, __LINE__, reqMahJongRequest.header.dwLength);
            }
        }
    }

    // 等待这个玩家补花
    m_nCurSeat = nSeat;

    // 如果这个玩家托管了，那么还要帮他补花
    if (m_pMahjongObj->GetPlayerBySeat(nSeat)->IsAutoPlay())
    { // 帮这个玩家游戏
        AutoChangeFlower(nSeat);
    }
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CChangeAllFlowerState::AutoChangeFlower( int nSeat )
{
	int nLine = __LINE__;
	try
	{

	nLine = __LINE__;
	CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerBySeat( nSeat ) ;	// 这个座位号的玩家
	nLine = __LINE__;
	if ( NULL == pPlayer )
	{
		TKWriteLog( TEXT( "自动补花 Seat( %d ), 这个座位上没有玩家" ), nSeat );
		return;
	}

	// 从这个玩家手中找花牌
	nLine = __LINE__;
	CMahJongTile *pTile = pPlayer->GetOneFlowerTile() ;
	nLine = __LINE__;
	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	if( NULL != pTile )
	{ // 手中有花牌
		// 处理一个补花消息
		nLine = __LINE__;
		PTKREQMAHJONGCHANGEFLOWER pReqMahJongChangeFlower = ( PTKREQMAHJONGCHANGEFLOWER )m_buffer ;
		nLine = __LINE__;
		pReqMahJongChangeFlower->header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_CHANGE_FLOWER ) ;
		nLine = __LINE__;
		pReqMahJongChangeFlower->header.dwLength = MSG_LENGTH( TKREQMAHJONGCHANGEFLOWER ) ;
		nLine = __LINE__;
		pReqMahJongChangeFlower->nSeat           = nSeat ;
		nLine = __LINE__;
		pReqMahJongChangeFlower->nRequestID      = pRequestObj->RequestID() ;
		nLine = __LINE__;
		pReqMahJongChangeFlower->nTileID         = pTile->ID() ;
		nLine = __LINE__;
		CEvent event( REQ_TYPE( TK_MSG_MAHJONG_CHANGE_FLOWER ), pPlayer->UserID(), pReqMahJongChangeFlower );
		nLine = __LINE__;
		PostEvent( event );
		nLine = __LINE__;
	}
	else
	{ // 手中没有花牌
		// 处理请求的回复消息
		nLine = __LINE__;
		TKACKMAHJONGREQUEST ackMahJongRequest = { 0 } ;
		nLine = __LINE__;
		ackMahJongRequest.header.dwType      = ACK_TYPE( TK_MSG_MAHJONG_REQUEST ) ;
		nLine = __LINE__;
		ackMahJongRequest.header.dwLength    = MSG_LENGTH( TKACKMAHJONGREQUEST ) ;
		nLine = __LINE__;
		ackMahJongRequest.nSeat              = nSeat ;
		nLine = __LINE__;
		ackMahJongRequest.nRequestID         = pRequestObj->RequestID() ;
		nLine = __LINE__;
		ackMahJongRequest.nGiveUpRequestType = 0 ;
		nLine = __LINE__;
//		CEvent event( ACK_TYPE( TK_MSG_MAHJONG_REQUEST ), pPlayer->UserID(), pReqMahJongChangeFlower );
		OnAckRequest( &ackMahJongRequest ) ;
		nLine = __LINE__;
	}

	}
	catch (...)
	{
		TKWriteLog( "%s : %d, Exception", __FILE__, nLine );
	}
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CChangeAllFlowerState::OnReqChangeFlower( CEvent * pEvent )
{
	PTKREQMAHJONGCHANGEFLOWER pReqChangeFlower = ( PTKREQMAHJONGCHANGEFLOWER )pEvent->m_pParam;
	ASSERT( NULL != pReqChangeFlower );
	if ( pReqChangeFlower->header.dwLength != MSG_LENGTH( TKREQMAHJONGCHANGEFLOWER ) )
	{
		return false;
	}

	// 检查消息中的座位号和消息长度是否合法
	if( !CheckMsgSeatAndLen( pEvent->m_dwPoster, pReqChangeFlower->nSeat , MSG_LENGTH( TKREQMAHJONGCHANGEFLOWER ), ( PTKHEADER ) pReqChangeFlower, __FILE__ , __LINE__ ) )
	{ // 座位号或者消息长度无效
		return false;
	}

	return OnReqChangeFlower( pReqChangeFlower );
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CChangeAllFlowerState::OnReqChangeFlower( PTKREQMAHJONGCHANGEFLOWER pReqChangeFlower )
{
	int nSeat = pReqChangeFlower->nSeat ;	// 这个玩家的座位号

	if ( m_nCurSeat != nSeat )
	{
		// 不该这个玩家补花
		ASSERT( false );
		//TKWriteLog( "[%s:%d] 不该这个玩家user(%d[%s])补花 " , __FILE__ , __LINE__ , pPlayer->UserID(), pPlayer->NickName() ) ;
		return false;
	}

	// 看看这个玩家手中是否有这张牌
	CTKMahjongPlayer* pPlayer = m_pMahjongObj->GetPlayerBySeat( nSeat ) ;	// 这个玩家
	int nTileID = pReqChangeFlower->nTileID ;	// 牌的ID号
	if( !pPlayer->HasTile( nTileID ) )
	{ // 玩家手中没有这张牌
		TKWriteLog( "[%s:%d]user(%d[%s]) no tile(0x%03X)" , __FILE__ , __LINE__ , pPlayer->UserID(), pPlayer->NickName(), nTileID ) ;
		pPlayer->TraceInfo() ;
		return false ;
	}

	// 让玩家补花
    int nFlowerCount = m_pMahjongObj->PlayerChangeFlower(pReqChangeFlower);
    m_pMahjongGame->SendChangeFlowerAck(nFlowerCount, pReqChangeFlower);

	// 记录补花
	DWORD dwDifMs = m_pMahjongGame->GetRtValue();
	CTKBuffer bufGameAction;
	bufGameAction.AppendFormatText("<a id=\"%d\" rt=\"%d\" rtms=\"%d\" s=\"%d\" fti=\"%d\" />",
		RECORD_FLOWER,
		dwDifMs / 1000,
		dwDifMs,
		nSeat,
		nTileID);
	m_pMahjongGame->AddGameAction( bufGameAction.GetBufPtr() );

	int nLoseSeat = m_pMahjongObj->GetNextPlayerSeat(nSeat);
	if (m_pMahjongGame->IsIslandMatch() && m_pMahjongGame->IsEndGameEarly(nLoseSeat))
	{
		CTKMahjongPlayer * pPlayOutPlayer = m_pMahjongObj->GetPlayerBySeat(nLoseSeat);
		std::string strNotify(pPlayOutPlayer->NickName());
		strNotify.append(" 玩家积分已输光，本局结束");
		Notify(MAHJONG_NOTIFY_PLAYER_NOCHIP, strNotify.c_str());

		IState * curResultState = m_pFSMachine->SetState("CResultState");
		curResultState->OnEnter(NULL);
		
		return true;
	}

	// 给这个玩家发送再抓的牌的信息
	nTileID = m_pMahjongObj->PlayerDrawOneTile( nSeat, DRAW_TILE_TYPE_CHANGE_FLOWER_1, FALSE, 0 );

	// 记录抓牌
	DWORD dwDifMsDraw = m_pMahjongGame->GetRtValue();
	bufGameAction.Clear();
	bufGameAction.AppendFormatText("<a id=\"%d\" rt=\"%d\" rtms=\"%d\" s=\"%d\" dti=\"%d\" dtt=\"%d\" fwh=\"%d\" tos=\"%d\" />",
		RECORD_DRAW,
		dwDifMsDraw /1000,
		dwDifMsDraw,
		nSeat,
		nTileID,
		DRAW_TILE_TYPE_CHANGE_FLOWER_1,
		0,
		0);
	m_pMahjongGame->AddGameAction( bufGameAction.GetBufPtr() );
	
	// 继续询问这个玩家是否补花
    SendRequestChangeFlowerMsg(nSeat, 0);


	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CChangeAllFlowerState::OnAckRequest( CEvent * pEvent )
{
	PTKACKMAHJONGREQUEST pAckMahJongRequest = ( PTKACKMAHJONGREQUEST )pEvent->m_pParam;
	ASSERT( NULL != pAckMahJongRequest );
	if ( pAckMahJongRequest->header.dwLength != MSG_LENGTH( TKACKMAHJONGREQUEST ) )
	{
		return false;
	}
	int nSeat = pAckMahJongRequest->nSeat;

	// 检查消息中的座位号和消息长度是否合法
	if( !CheckMsgSeatAndLen( pEvent->m_dwPoster, nSeat , MSG_LENGTH( TKACKMAHJONGREQUEST ), ( PTKHEADER ) pAckMahJongRequest, __FILE__ , __LINE__ ) )
	{ // 座位号或者消息长度无效
		return false;
	}

	// 看看当前是否应该处理这个消息
	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	if( !pRequestObj->IsRequestID( pAckMahJongRequest->nRequestID ) || !pRequestObj->IsRequestSeat( nSeat ) || pRequestObj->IsAckRequestSeat( nSeat ) )
	{ // 请求的ID号不对，或者没有向这个玩家发送请求信息，或者这个玩家已经回复请求了
		TKWriteLog( TEXT( "【CChangeAllFlowerState::OnAckRequest】,seat(%d),request(%d)" ) , nSeat , pAckMahJongRequest->nRequestID ) ;
		return true ;
	}

	return OnAckRequest( pAckMahJongRequest );
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CChangeAllFlowerState::OnAckRequest( PTKACKMAHJONGREQUEST pAckMahJongRequest )
{
	int nSeat = pAckMahJongRequest->nSeat ;	// 这个玩家的座位号

	if ( m_nCurSeat != nSeat )
	{
		// 不该这个玩家补花
		ASSERT( false );
//		TKWriteLog( "[%s:%d] 不该这个玩家user(%d[%s])补花 " , __FILE__ , __LINE__ , pPlayer->UserID(), pPlayer->NickName() ) ;
		return false;
	}

	CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerBySeat( nSeat ) ;	// 这个玩家
	// 这个时候，这个玩家手中应该没有花牌了
	CMahJongTile *pTile = pPlayer->GetOneFlowerTile() ;	// 看看玩家手中是否还有花牌
	if( NULL != pTile )
	{ // 玩家手中还有花牌
		TKWriteLog( "[%s:%d]user(%d[%s])has flower tile(0x%03X)" , __FILE__ , __LINE__ , pPlayer->UserID(), pPlayer->NickName(), pTile->ID() ) ;
		pPlayer->TraceInfo();
		return false;
	}

	int nNextSeat = m_pMahjongObj->GetNextPlayerSeat( nSeat ) ;	// 下一家的座位号
	if( nNextSeat == m_pMahjongObj->Banker() )
	{ // 下一家是庄家（说明所有的玩家都已经补花完毕了）
		//// 发送加番牌信息
		//if (Rule()->GetRule(RULE_SPECIALTILE_FAN) != 0 && Rule()->GetRule(RULE_SPECIALTILE_COUNT) != 0)
		//{
		//	m_pMahjongObj->SendSprcialTileInfo();
		//}

		// MOD BY LIULEI 20180514 因为补花后面直接转到出牌了 所以此处要带上首出的座位号 -> 庄家
		//PostEvent( EVENT_CHANGEALLFLOWER_END );
		CEvent event( EVENT_CHANGEALLFLOWER_END, m_pMahjongObj->Banker() );
		PostEvent( event );
	}
	else
	{ // 下一家还不是庄家
        SendRequestChangeFlowerMsg(nNextSeat, 0);	// 那么就让下一家补花
	}

	return true ;
}



bool CChangeAllFlowerState::OnChangeAllFlowerEnd( CEvent * pEvent )
{
	// 什么都不用做,只是想返回true
	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CChangeAllFlowerState::OnTrustPlay( CEvent * pEvent )
{
	int nLine = __LINE__;
	try
	{
		nLine = __LINE__;
		if ( NULL != pEvent->m_pParam )
		{
			nLine = __LINE__;
			PTKREQTRUSTPLAY pReqTrustPlay = ( PTKREQTRUSTPLAY )pEvent->m_pParam;
			nLine = __LINE__;
		}

		// 替他补花
		nLine = __LINE__;
		CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerByID( pEvent->m_dwPoster );
		if ( NULL == pPlayer )
		{
			TKWriteLog( "%s : %d, Error! ( %d )托管了，但他不是本桌的玩家", __FILE__, __LINE__, pEvent->m_dwPoster );
			return false;
		}
		int nSeat = pPlayer->Seat();
		nLine = __LINE__;
		if ( m_nCurSeat == nSeat )
		{
			nLine = __LINE__;
			AutoChangeFlower( nSeat );
			nLine = __LINE__;
		}

		return true;
	}
	catch (...)
	{
		TKWriteLog( "%s : %d, Exception", __FILE__, nLine );
	}
	return false;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CTianTingState::CTianTingState()
{
	// 注册感兴趣的事件
	REGIST_EVENT_FUNCTION( EVENT_ACK_REQUEST, &CTianTingState::OnAckRequest );
	REGIST_EVENT_FUNCTION( EVENT_CALL, &CTianTingState::OnReqCall );
	REGIST_EVENT_FUNCTION( EVENT_TIANTING_END, &CTianTingState::OnTianTingEnd );
	REGIST_EVENT_FUNCTION( EVENT_TRUSTPLAY, &CTianTingState::OnTrustPlay );
	REGIST_EVENT_FUNCTION( EVENT_CALLTILE, &CTianTingState::OnReqCallTile );
    REGIST_EVENT_FUNCTION( EVENT_CHECKTIMEOUT, &CTianTingState::OnCheckTimeout );

}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CTianTingState::OnEnter( CEvent * pEvent )
{
	int nBanker = m_pMahjongObj->Banker();
	// 询问其他几个玩家是否要听牌
	SendRequestCallMsg( nBanker ) ;	
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CTianTingState::SendRequestCallMsg( int nBanker )
{
	// 重置请求类型和等待时间
	int nRequestType = REQUEST_TYPE_CALL ;
	int nWaitSecond  = Rule()->GetRule( RULE_WAITTIME ) ;
	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	pRequestObj->ResetRequest( nRequestType , nWaitSecond ) ;

	// 给不是nBanker的玩家发送请求消息
	TKREQMAHJONGREQUEST reqMahJongRequest = { 0 } ;
	for( int i = 0; i < MAX_PLAYER_COUNT; i ++ )
	{ // 遍历所有座位号
		if( nBanker == i )
        {
            //发送消息
            TKREQCHECKTIMEOUT reqCheckTimeOut = {0};
            reqCheckTimeOut.header.dwType = REQ_TYPE(TK_MSG_CHECKTIMEOUT);
            reqCheckTimeOut.header.dwLength = MSG_LENGTH( TKREQCHECKTIMEOUT );
            reqCheckTimeOut.nWaitSecond = nWaitSecond;
            reqCheckTimeOut.m_dwUserID = -1;
            if (!Send2SeatPlayer( i, ( PTKHEADER )&reqCheckTimeOut ))
            {
                TKWriteLog( "%s : %d, Error! 发送消息错误 len( %d )", __FILE__, __LINE__, reqCheckTimeOut.header.dwLength );
            }

            continue ;	// 略过庄家（没有给庄家和他的旁观者发送消息）
        }
		CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerBySeat( i );
		if ( NULL == pPlayer )
		{
			// 这个座位上没人
			continue;
		}

		pRequestObj->FillRequestMsg( &reqMahJongRequest , i ) ;
		if (m_pMahjongGame)
		{
			if (m_pMahjongGame->IsHaveplayerSeat(i))
			{
				if ( !Send2SeatPlayer( i, ( PTKHEADER )&reqMahJongRequest ) )
				{
					TKWriteLog( "%s : %d, Error! 发送消息错误 len( %d )", __FILE__, __LINE__, reqMahJongRequest.header.dwLength );
				}
			}
		}

		// 如果这个座位的玩家托管了，那么还要帮他回答
		if( m_pMahjongObj->GetPlayerBySeat( i )->IsAutoPlay() )
		{ // 需要帮这个玩家处理这个消息
			pRequestObj->SetAckQuestSeat( i , 0 ) ;	// 设置这个玩家回复了
		}
	}

	// 检查回复的状态
	CheckAndDealAckRequest() ;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CTianTingState::OnAckRequest( CEvent * pEvent )
{
	PTKACKMAHJONGREQUEST pAckMahJongRequest = ( PTKACKMAHJONGREQUEST )pEvent->m_pParam;
	ASSERT( NULL != pAckMahJongRequest );
	if ( pAckMahJongRequest->header.dwLength != MSG_LENGTH( TKACKMAHJONGREQUEST ) )
	{
		return false;
	}

	// 检查消息是否合法
	if( !CheckMsg( pEvent->m_dwPoster, pAckMahJongRequest->nSeat , MSG_LENGTH( TKACKMAHJONGREQUEST ), 
		( PTKHEADER ) pAckMahJongRequest, pAckMahJongRequest->nRequestID, __FILE__ , __LINE__ ) )
	{ 
		return false;
	}

	return OnAckRequest( pAckMahJongRequest );
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CTianTingState::OnAckRequest( PTKACKMAHJONGREQUEST pAckMahJongRequest )
{
	// 设置这个玩家回复了请求，但是是放弃了操作
	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	pRequestObj->SetAckQuestSeat( pAckMahJongRequest->nSeat , 0 ) ;

	// 检查回复的状态
	CheckAndDealAckRequest();

	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CTianTingState::OnReqCall( CEvent * pEvent )
{
	PTKREQMAHJONGCALL pReqMahJongCall = ( PTKREQMAHJONGCALL )pEvent->m_pParam;
	ASSERT( NULL != pReqMahJongCall );
	if ( pReqMahJongCall->header.dwLength != MSG_LENGTH( TKREQMAHJONGCALL ) )
	{
		return false;
	}

	// 检查消息是否合法
	if( !CheckMsg( pEvent->m_dwPoster, pReqMahJongCall->nSeat , MSG_LENGTH( TKREQMAHJONGCALL ), 
		( PTKHEADER ) pReqMahJongCall, pReqMahJongCall->nRequestID, __FILE__ , __LINE__ ) )
	{ 
		return false;
	}

	return OnReqCall( pReqMahJongCall );
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CTianTingState::OnReqCallTile( CEvent * pEvent )
{
	PTKREQMAHJONGCALLTILE pReqMahJongCallTile = ( PTKREQMAHJONGCALLTILE )pEvent->m_pParam;
	ASSERT( NULL != pReqMahJongCallTile );
	if ( pReqMahJongCallTile->header.dwLength != MSG_LENGTH( TKREQMAHJONGCALLTILE ) )
	{
		return false;
	}

	// 检查消息是否合法
	if( !CheckMsg( pEvent->m_dwPoster, pReqMahJongCallTile->nSeat , MSG_LENGTH( TKREQMAHJONGCALLTILE ), 
		( PTKHEADER ) pReqMahJongCallTile, pReqMahJongCallTile->nRequestID, __FILE__ , __LINE__ ) )
	{ 
		return false;
	}

	return OnReqCallTile( pReqMahJongCallTile );
}


bool CTianTingState::OnCheckTimeout( CEvent * pEvent )
{
    PTKREQCHECKTIMEOUT pReqCheckTimeout = ( PTKREQCHECKTIMEOUT )pEvent->m_pParam;
    CTKMahjongPlayer *pPlayer = m_pMahjongObj->GetPlayerByID(pReqCheckTimeout->m_dwUserID);
    if ((NULL != pPlayer) && (m_pMahjongGame->RequestObj()->CheckOverTime(pPlayer->Seat(), m_pMahjongGame->Rule()->GetRule( RULE_WAITTIME ))))
    {
        TKREQTRUSTPLAY reqTrustPlay = { 0 } ;
        reqTrustPlay.header.dwType   = REQ_TYPE( TK_MSG_TRUSTPLAY ) ;
        reqTrustPlay.header.dwLength = MSG_LENGTH( TKREQTRUSTPLAY ) ;
        reqTrustPlay.dwUserID = pPlayer->UserID();
        reqTrustPlay.dwType = TRUST_OVERTIME;
        m_pMahjongGame->OnPlayerMsg( pPlayer->UserID(), pPlayer->Seat(), ( PTKHEADER )&reqTrustPlay );
    }

    return true;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CTianTingState::OnReqCall( PTKREQMAHJONGCALL pReqMahJongCall )
{
	int nSeat = pReqMahJongCall->nSeat ;	// 这个玩家的座位号

	// 设置玩家回复要听牌
	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	pRequestObj->SetAckQuestSeat( nSeat , REQUEST_TYPE_CALL , ( PTKHEADER ) pReqMahJongCall ) ;

	// 记录听牌
	DWORD dwDifMs = m_pMahjongGame->GetRtValue();
	CTKBuffer bufGameAction;
	bufGameAction.AppendFormatText("<a id=\"%d\" rt=\"%d\" rtms=\"%d\" s=\"%d\" tti=\"%d\" ag=\"%d\" ws=\"%d\" />",
		RECORD_TING,
		dwDifMs / 1000,
		dwDifMs,
		nSeat,
		0,
		pReqMahJongCall->bAutoGang,
		pReqMahJongCall->bWinSelf);
	m_pMahjongGame->AddGameAction( bufGameAction.GetBufPtr() );

	// 检查回复的状态
	CheckAndDealAckRequest();

	return true;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CTianTingState::OnReqCallTile( PTKREQMAHJONGCALLTILE pReqMahjongCallTile )
{
    return true;

	if (m_pMahjongObj)
	{
		CTKMahjongServerPlayer * pPlayer = (CTKMahjongServerPlayer *)m_pMahjongObj->GetPlayerBySeat( pReqMahjongCallTile->nSeat ) ;
		if (pPlayer)
		{
			pPlayer->SetCallInfo(pReqMahjongCallTile->cnCallTileCount, pReqMahjongCallTile->nCallTileID);
			return true;
		}
	}

	return false;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CTianTingState::OnTianTingEnd( CEvent * pEvent )
{
	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CTianTingState::OnTrustPlay( CEvent * pEvent )
{
	int nLine = __LINE__;
	try
	{
		if ( NULL != pEvent->m_pParam )
		{
			nLine = __LINE__;
			PTKREQTRUSTPLAY pReqTrustPlay = ( PTKREQTRUSTPLAY )pEvent->m_pParam;
			nLine = __LINE__;
		}

		// 替他回复
		nLine = __LINE__;
		CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerByID( pEvent->m_dwPoster );
		if ( NULL == pPlayer )
		{
			TKWriteLog( "%s : %d, Error! ( %d )托管了，但他不是本桌的玩家", __FILE__, __LINE__, pEvent->m_dwPoster );
			return false;
		}
		int nSeat = pPlayer->Seat();
		nLine = __LINE__;
		CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
		if ( pRequestObj->IsRequestSeat( nSeat ) && !pRequestObj->IsAckRequestSeat( nSeat ) )
		{
			m_pMahjongGame->RequestObj()->SetAckQuestSeat( nSeat , 0 ) ;	// 设置这个玩家回复了
		}
		nLine = __LINE__;

		// 检查回复的状态
		CheckAndDealAckRequest() ;

		return true;
	}
	catch (...)
	{
		TKWriteLog( "%s : %d, Exception", __FILE__, nLine );
	}
	return false;
}




// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CTianTingState::CheckAndDealAckRequest()
{
	int nAckType = CheckAckRequest();
	if ( -1 == nAckType )
	{
		// 还有人没有回复
		return;
	}

	if ( REQUEST_TYPE_CALL == nAckType )
	{
		// 有人选择了听牌
		int nCallType = 2 ;	// 听牌类型（天听）
		// 让所有听牌的玩家听牌
		for( int i = 0; i < MAX_PLAYER_COUNT; i++ )
		{ // 遍历所有座位号
			CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
			if( pRequestObj->IsRequestSeat( i ) && pRequestObj->IsPlayerAckRequestType( i , REQUEST_TYPE_CALL ) )
			{ // 请求了这个玩家，并且这个玩家回复说要听牌
				// 让这个玩家听牌
				PTKREQMAHJONGCALL pReqMahJongCall = ( PTKREQMAHJONGCALL ) pRequestObj->GetPlayerAckMsg( i ) ;	// 取听牌信息
				if ( NULL == pReqMahJongCall )
				{
					// 出错了
					TKWriteLog( "%s : %d, 取玩家天听消息出错，消息为NULL", __FILE__, __LINE__ );
					continue;
				}
				// 设置这个玩家听牌了
				CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerBySeat( i ) ;	// 取这个玩家
				ASSERT( NULL != pPlayer );
				pPlayer->SetCallType( nCallType , pReqMahJongCall->bWinSelf , pReqMahJongCall->bAutoGang ) ;

				pReqMahJongCall->nCallType = nCallType ;

				// 发对家牌给这个玩家
				//if ( m_pMahjongGame->Rule()->GetRule(RULE_DOUBLE_COUNT))
				{
					char playerTiles[1024] = { 0 };
					PTKACKTILES ackTiles = ( PTKACKTILES )playerTiles;
					ackTiles->header.dwType = ACK_TYPE( TK_MSG_PLAYER_TILES );
					ackTiles->header.dwLength = MSG_LENGTH( TKACKTILES );
					ackTiles->seat = 1 - i;
					CTKMahjongPlayer * pOPlayer = m_pMahjongObj->GetPlayerBySeat( 1 - i ) ;
					if ( NULL == pOPlayer )
					{
						TKWriteLog( "%s : %d, Error! 获取对家出错, seat( %d )", __FILE__, __LINE__, 1 - i );
						continue;
					}
					ackTiles->cnHandTile = pOPlayer->FillHandTileInfo( ackTiles->anHandTile );
					Send2SeatPlayer( i, PTKHEADER(ackTiles) );

					// 数据统计
					int nSeat = pReqMahJongCall->nSeat;
					m_pMahjongObj->CheckCall( nSeat ); // 需要打掉已出
                    if (!m_pMahjongObj->SetCallInfo(nSeat, pReqMahJongCall->nTileID, nCallType))
                    {
                        ASSERT(false);
                    }

					m_pMahjongObj->MemCallDisID( nSeat, pReqMahJongCall->nTileID );
					
					// 双听统计
					m_pMahjongObj->CountHandTile( nSeat );
					m_pMahjongObj->CountHandTile( 1 - nSeat );

					//CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerBySeat( 1 - nSeat ) ;
					//if ( pPlayer && pPlayer->HasCalled() )
					//{ // 双听
					//	m_pMahjongObj->CheckCall( nSeat );
					//	m_pMahjongObj->CheckCall( 1 - nSeat );
					//}
				}

				if ( !Broadcast( ( PTKHEADER )pReqMahJongCall ) )
				{
					TKWriteLog( "%s : %d, Error! 发送消息出错, len( %d )", __FILE__, __LINE__, pReqMahJongCall->header.dwLength );
				}
			}
		}
	}

	// 开始出牌了
	Notify( MAHJONG_NOTIFY_DISCARD_START ) ;	

	// 天听结束
	CEvent event( EVENT_TIANTING_END, m_pMahjongObj->Banker() );
	PostEvent( event );
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CDiscardTileState::CDiscardTileState()
{
	// 注册感兴趣的事件
	REGIST_EVENT_FUNCTION( EVENT_CHANGE_FLOWER, &CDiscardTileState::OnReqChangeFlower );
	REGIST_EVENT_FUNCTION( EVENT_REQ_GANG, &CDiscardTileState::OnReqGang );
	REGIST_EVENT_FUNCTION( EVENT_DISCARD_TILE, &CDiscardTileState::OnReqDiscardTile );
	REGIST_EVENT_FUNCTION( EVENT_CALL, &CDiscardTileState::OnReqCall );
	REGIST_EVENT_FUNCTION( EVENT_REQ_WIN, &CDiscardTileState::OnReqWin );
	REGIST_EVENT_FUNCTION( EVENT_GANG, &CDiscardTileState::OnAnGang );
	REGIST_EVENT_FUNCTION( EVENT_BUGANG, &CDiscardTileState::OnBuGang );
	REGIST_EVENT_FUNCTION( EVENT_WIN_SELF, &CDiscardTileState::OnPlayerWinSelf );
	REGIST_EVENT_FUNCTION( EVENT_TRUSTPLAY, &CDiscardTileState::OnTrustPlay );
	REGIST_EVENT_FUNCTION( EVENT_REQ_DOUBLE, &CDiscardTileState::OnReqDouble );
	REGIST_EVENT_FUNCTION( EVENT_DOUBLE, &CDiscardTileState::OnPlayerDouble );
	REGIST_EVENT_FUNCTION( EVENT_CHECKTIMEOUT, &CDiscardTileState::OnCheckTimeout );
	REGIST_EVENT_FUNCTION( EVENT_CALLTILE, &CDiscardTileState::OnReqCallTile )

	ResetDbl();
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CDiscardTileState::OnEnter( CEvent * pEvent )
{
	// 请玩家出牌
	int nSeat = pEvent->m_dwPoster;
	ASSERT( nSeat >= 0 && nSeat < MAX_PLAYER_COUNT );

	// SendRequestDiscardTileMsg( nSeat );
	SendRequestDiscardTileMsgEx(nSeat);
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
// 自动胡牌
void CDiscardTileState::AutoWin( int nSeat, CMahJongTile* nTile)
{
	CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerBySeat( nSeat );
	PTKREQMAHJONGWIN pReqMahJongWin = (PTKREQMAHJONGWIN)m_winBuffer;
	if (m_pMahjongObj->CheckCanWin(pReqMahJongWin, nSeat, nSeat, nTile))
	{
		CEvent event( REQ_TYPE( TK_MSG_MAHJONG_WIN ), pPlayer->UserID(), (void*)pReqMahJongWin );
		//PostEvent( event );
		OnReqWin(&event);
	}
	else
	{
		AutoDiscardTile(nSeat);
	}
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CDiscardTileState::SendRequestDiscardTileMsg( int nSeat )
{
	int nRequestType = REQUEST_TYPE_DISCARD_TILE | REQUEST_TYPE_CHANGE_FLOWER | REQUEST_TYPE_CALL | REQUEST_TYPE_GANG | REQUEST_TYPE_WIN ;
	int nWaitSecond  = m_pMahjongGame->Rule()->GetRule( RULE_DISCARDTIME );

	CTKMahjongPlayer* pTKMahjongPlayer = m_pMahjongObj->GetPlayerBySeat(nSeat);
	if ( pTKMahjongPlayer->GetDelayCount() >= 2 )
	{
		//玩家连续2次出牌等待时长超过配置RULE_WAIT_DISCARDTIME时间
		nWaitSecond  = m_pMahjongGame->Rule()->GetRule( RULE_WAIT_DISCARDTIME );
	}

	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	pRequestObj->ResetRequest( nRequestType , nWaitSecond ) ;

	// 先发送请求这个玩家出牌的消息
	TKREQMAHJONGREQUEST reqMahJongRequest = { 0 } ;
	pRequestObj->FillRequestMsg( &reqMahJongRequest , nSeat ) ;

	if ( !Broadcast( ( PTKHEADER )&reqMahJongRequest ) )
	{
		TKWriteLog( "%s : %d, Error! 发送消息出错, len( %d )", __FILE__, __LINE__, reqMahJongRequest.header.dwLength );
	}

	// 设置当前该这个玩家出牌了
	m_pMahjongObj->CurDiscardSeat( nSeat ) ;

// 如果这个玩家托管了，那么还要帮他出牌
	CTKMahjongServerPlayer * pPlayer = (CTKMahjongServerPlayer *)m_pMahjongObj->GetPlayerBySeat( nSeat );
	ASSERT( NULL != pPlayer );
	// 如果玩家退出或断线，且可胡，帮其应胡
	
	if ( pPlayer->IsBreak() || !m_pMahjongGame->IsPlayerPlaying( nSeat ) )
	{
		TKREQMAHJONGWIN reqMahJongWin = { 0 } ;
		CHECKRESULT eCheckWinResult = m_pMahjongObj->CheckBotWin( nSeat, true, 0, &reqMahJongWin );
		if ( T_OK == eCheckWinResult )
		{
			m_pMahjongGame->OnPlayerMsg( pPlayer->UserID(), nSeat, (PTKHEADER)&reqMahJongWin );
			return;
		}
	}
	// 托管状态，打出刚抓的牌
	CTKGamePlayer* pTKGamePlayer = m_pMahjongGame->GetGamePlayerBySeat(nSeat);
	if (pPlayer->IsAutoPlay()
		|| (pTKGamePlayer != NULL && pTKGamePlayer->m_nOpertorType == TK_GAMEPLAYEROPERATORTYPE_BOTTRUST))
	{ // 帮这个玩家游戏

		if (pPlayer->HasCalled())
		{
			CMahJongTile* nTile = pPlayer->GetDrawTile();
			if (nTile)
			{
				int tileid = nTile->ID();
				if ((pPlayer->IsCallTile(tileid)))
				{
					AutoWin(nSeat, nTile);
					return;
				}
			}
		}
	}

    if (pPlayer->IsAutoPlay())
    {
        AutoDiscardTile( nSeat ) ;
    }
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CDiscardTileState::SendRequestDiscardTileMsgEx( int nSeat )
{
	if ( !m_pMahjongGame->Rule()->GetRule( RULE_DOUBLE_COUNT ) )
	{
		SendRequestDiscardTileMsg(nSeat);
		return;
	}

	int nRequestType = REQUEST_TYPE_NULL;
	if ( m_canDbl )
	{
		nRequestType = REQUEST_TYPE_DISCARD_TILE | REQUEST_TYPE_CHANGE_FLOWER | REQUEST_TYPE_CALL | REQUEST_TYPE_GANG | REQUEST_TYPE_WIN | REQUEST_TYPE_DOUBLE;
		m_canDbl = false;
	}
	else
	{
		nRequestType = REQUEST_TYPE_DISCARD_TILE | REQUEST_TYPE_CHANGE_FLOWER | REQUEST_TYPE_CALL | REQUEST_TYPE_GANG ;
		m_canDbl = true;
	}
	
	int nWaitSecond  = m_pMahjongGame->Rule()->GetRule( RULE_DISCARDTIME );

	CTKMahjongPlayer* pTKMahjongPlayer = m_pMahjongObj->GetPlayerBySeat(nSeat);
	if (pTKMahjongPlayer->GetDelayCount() >= 2)
	{
		//玩家连续2次出牌等待时长超过配置RULE_WAIT_DISCARDTIME时间
		nWaitSecond = m_pMahjongGame->Rule()->GetRule(RULE_WAIT_DISCARDTIME);
	}

	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	pRequestObj->ResetRequest( nRequestType , nWaitSecond ) ;

	// 先发送请求这个玩家出牌的消息
	TKREQMAHJONGREQUEST reqMahJongRequest = { 0 } ;
	pRequestObj->FillRequestMsg( &reqMahJongRequest , nSeat ) ;
	if ( !Broadcast( ( PTKHEADER )&reqMahJongRequest ) )
	{
		TKWriteLog( "%s : %d, Error! 发送消息出错, len( %d )", __FILE__, __LINE__, reqMahJongRequest.header.dwLength );
	}

	// 设置当前该这个玩家出牌了
	m_pMahjongObj->CurDiscardSeat( nSeat ) ;

// 如果这个玩家托管了，那么还要帮他出牌
	CTKMahjongServerPlayer * pPlayer = (CTKMahjongServerPlayer *)m_pMahjongObj->GetPlayerBySeat( nSeat );
	assert( NULL != pPlayer );
	// 如果玩家退出或断线，且可胡，帮其应胡
	if ( (pPlayer->IsBreak() || !m_pMahjongGame->IsPlayerPlaying( nSeat ))
		&& !m_canDbl )
	{
		TKREQMAHJONGWIN reqMahJongWin = { 0 } ;
		CHECKRESULT eCheckWinResult = m_pMahjongObj->CheckBotWin( nSeat, true, 0, &reqMahJongWin );
		if ( T_OK == eCheckWinResult )
		{
			m_pMahjongGame->OnPlayerMsg( pPlayer->UserID(), nSeat, (PTKHEADER)&reqMahJongWin );
			return;
		}
	}
	// 托管状态，打出刚抓的牌
	CTKGamePlayer* pTKGamePlayer = m_pMahjongGame->GetGamePlayerBySeat(nSeat);
	if (pPlayer->IsAutoPlay()
		|| (pTKGamePlayer != NULL && pTKGamePlayer->m_nOpertorType == TK_GAMEPLAYEROPERATORTYPE_BOTTRUST))
	{ // 帮这个玩家游戏

		if (pPlayer->HasCalled())
		{
			CMahJongTile* nTile = pPlayer->GetDrawTile();
			if (nTile)
			{
				if ((pPlayer->IsCallTile(nTile->ID())))
				{
					AutoWin(nSeat, nTile);
					return;
				}
			}
		}
	}

    if (pPlayer->HasCalled())
    {// 玩家已听牌，听口不变情况下，自动补杠暗杠
//         TKREQMAHJONGGANG reqMahJongGang = {0} ;
//         reqMahJongGang.header.dwType = REQ_TYPE(TK_MSG_MAHJONG_GANG);
//         reqMahJongGang.header.dwLength = MSG_LENGTH(TKREQMAHJONGGANG);
// 
//         reqMahJongGang.nSeat = nSeat;
// 
//         CMahJongTile* nTile = pPlayer->GetDrawTile();
//         reqMahJongGang.nTileID = nTile->ID();
// 
//         int nTileID = reqMahJongGang.nTileID & 0xFFFFFFF0;
//         for (int i = 0; i<4; ++i)
//         {
//             reqMahJongGang.anGangTileID[i] = nTileID | (i+1);
//         }
// 
//         reqMahJongGang.nGangType = 2;   // 补杠可以直接杠
//         if (OnReqGang(&reqMahJongGang))
//         {// 自动补杠
//             pRequestObj->SetAckQuestSeat(nSeat, 0); // 设置这个玩家回复了
// 
//             return;
//         }
//         
//         reqMahJongGang.nGangType = 1;   // 暗杠不能直接杠
//         if (m_pMahjongObj->PlayerReqGang(&reqMahJongGang, true))
//         {
// 
//             return;
//         }
    }

    if (pPlayer->IsAutoPlay())
    {
        AutoDiscardTile(nSeat);
    }
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CDiscardTileState::AutoDiscardTile( int nSeat )
{
	ResetDbl();

	int nLine = __LINE__;
	try
	{
	// 
	// 1.如果玩家手中有花牌，那么先补花
	// 2.否则出抓的这张牌
	// 

	nLine = __LINE__;
	CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerBySeat( nSeat ) ;	// 这个座位号的玩家
	nLine = __LINE__;

	// 从这个玩家手中找花牌
	CMahJongTile* pTile = pPlayer->GetOneFlowerTile() ;
	nLine = __LINE__;
	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	nLine = __LINE__;
	if( NULL != pTile )
	{ // 手中有花牌
		// 处理一个补花消息
		nLine = __LINE__;
		PTKREQMAHJONGCHANGEFLOWER pReqMahJongChangeFlower = ( PTKREQMAHJONGCHANGEFLOWER )m_buffer ;
		nLine = __LINE__;
		pReqMahJongChangeFlower->header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_CHANGE_FLOWER ) ;
		nLine = __LINE__;
		pReqMahJongChangeFlower->header.dwLength = MSG_LENGTH( TKREQMAHJONGCHANGEFLOWER ) ;
		nLine = __LINE__;
		pReqMahJongChangeFlower->nSeat           = nSeat ;
		nLine = __LINE__;
		pReqMahJongChangeFlower->nRequestID      = pRequestObj->RequestID() ;
		nLine = __LINE__;
		pReqMahJongChangeFlower->nTileID         = pTile->ID() ;
		nLine = __LINE__;
		CEvent event( REQ_TYPE( TK_MSG_MAHJONG_CHANGE_FLOWER ), pPlayer->UserID(), pReqMahJongChangeFlower );
		nLine = __LINE__;
		PostEvent( event );
		nLine = __LINE__;

// 		TKREQMAHJONGCHANGEFLOWER reqMahJongChangeFlower = { 0 } ;
// 		reqMahJongChangeFlower.header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_CHANGE_FLOWER ) ;
// 		reqMahJongChangeFlower.header.dwLength = MSG_LENGTH( TKREQMAHJONGCHANGEFLOWER ) ;
// 		reqMahJongChangeFlower.nSeat           = nSeat ;
// 		reqMahJongChangeFlower.nRequestID      = pRequestObj->RequestID() ;
// 		reqMahJongChangeFlower.nTileID         = pTile->ID() ;
// 		OnReqChangeFlower( &reqMahJongChangeFlower ) ;
	}
	else
	{ // 手中没有花牌
		// 处理一个出牌消息
		PTKREQMAHJONGDISCARDTILE pReqMahJongDiscardTile = ( PTKREQMAHJONGDISCARDTILE )m_buffer;
		pReqMahJongDiscardTile->header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_DISCARD_TILE ) ;
		pReqMahJongDiscardTile->header.dwLength = MSG_LENGTH( TKREQMAHJONGDISCARDTILE ) ;
		pReqMahJongDiscardTile->nSeat           = nSeat ;
		pReqMahJongDiscardTile->nRequestID      = pRequestObj->RequestID() ;

		//机器人出牌逻辑修改，去掉之前智能出牌逻辑，改为抓啥打啥 addby zhangws 20180710
		//pReqMahJongDiscardTile->nTileID         = ( m_pMahjongGame->IsPlayerPlaying( nSeat ) && !pPlayer->IsBreak() ) ? 
			//pPlayer->GetAutoPlayTile()->ID() : pPlayer->GetAutoPlayTileEx()->ID();

		pReqMahJongDiscardTile->nTileID         = ( m_pMahjongGame->IsPlayerPlaying( nSeat ) && !pPlayer->IsBreak() ) ? 
			pPlayer->GetAutoPlayTile()->ID() : pPlayer->GetAutoPlayTile()->ID();

		pReqMahJongDiscardTile->bHide			= FALSE;

		CEvent event( REQ_TYPE( TK_MSG_MAHJONG_DISCARD_TILE ), pPlayer->UserID(), pReqMahJongDiscardTile );
		PostEvent( event );

// 		TKREQMAHJONGDISCARDTILE reqMahJongDiscardTile = { 0 } ;
// 		reqMahJongDiscardTile.header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_DISCARD_TILE ) ;
// 		reqMahJongDiscardTile.header.dwLength = MSG_LENGTH( TKREQMAHJONGDISCARDTILE ) ;
// 		reqMahJongDiscardTile.nSeat           = nSeat ;
// 		reqMahJongDiscardTile.nRequestID      = pRequestObj->RequestID() ;
// 		reqMahJongDiscardTile.nTileID         = pPlayer->GetAutoPlayTile()->ID() ;
// 		OnReqDiscardTile( &reqMahJongDiscardTile ) ;
	}

	}
	catch (...)
	{
		TKWriteLog( "%s : %d, Exception", __FILE__, __LINE__ );
	}
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardTileState::OnReqChangeFlower( CEvent * pEvent )
{
	PTKREQMAHJONGCHANGEFLOWER pReqMahJongChangeFlower = ( PTKREQMAHJONGCHANGEFLOWER )pEvent->m_pParam;
	ASSERT( NULL != pReqMahJongChangeFlower );
	if ( pReqMahJongChangeFlower->header.dwLength != MSG_LENGTH( TKREQMAHJONGCHANGEFLOWER ) )
	{
		return false;
	}

	// 检查消息是否合法
	if( !CheckMsg( pEvent->m_dwPoster, pReqMahJongChangeFlower->nSeat , MSG_LENGTH( TKREQMAHJONGCHANGEFLOWER ), 
		( PTKHEADER ) pReqMahJongChangeFlower, pReqMahJongChangeFlower->nRequestID, __FILE__ , __LINE__ ) )
	{ 
		return false;
	}

	return OnReqChangeFlower( pReqMahJongChangeFlower );
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardTileState::OnReqGang( CEvent * pEvent )
{
	PTKREQMAHJONGGANG pReqMahJongGang = ( PTKREQMAHJONGGANG )pEvent->m_pParam;
	ASSERT( NULL != pReqMahJongGang );
	if ( pReqMahJongGang->header.dwLength != MSG_LENGTH( TKREQMAHJONGGANG ) )
	{
		return false;
	}

	// 检查消息是否合法
	if( !CheckMsg( pEvent->m_dwPoster, pReqMahJongGang->nSeat , MSG_LENGTH( TKREQMAHJONGGANG ), 
		( PTKHEADER ) pReqMahJongGang, pReqMahJongGang->nRequestID, __FILE__ , __LINE__ ) )
	{ 
		return false;
	}

	return OnReqGang( pReqMahJongGang );
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardTileState::OnReqDouble( CEvent * pEvent )
{
	PTKREQMAHJONGDBL pReqMahJongDouble = ( PTKREQMAHJONGDBL )pEvent->m_pParam;
	assert( NULL != pReqMahJongDouble );
	if ( pReqMahJongDouble->header.dwLength != MSG_LENGTH( TKREQMAHJONGDBL ) )
	{
		return false;
	}
	if ( !m_pMahjongGame->Rule()->GetRule( RULE_DOUBLE_COUNT ) ) return false;

	// 检查消息是否合法
	if( !CheckMsg( pEvent->m_dwPoster, pReqMahJongDouble->seat , MSG_LENGTH( TKREQMAHJONGDBL ), 
		( PTKHEADER ) pReqMahJongDouble, pReqMahJongDouble->nRequestID, __FILE__ , __LINE__ ) )
	{ 
		return false;
	}

	// 检测下请求状态（是否允许加倍、和牌）
	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	if ( !pRequestObj->IsRequestType( REQUEST_TYPE_WIN ) )
	{
		return false;
	}

	return OnReqDouble( pReqMahJongDouble );
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardTileState::OnPlayerDouble( CEvent * pEvent )
{
	if ( !m_pMahjongGame->Rule()->GetRule(RULE_DOUBLE_COUNT) ) return false;
	
	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	int nSeat = pRequestObj->GetAckMsgSeat( REQUEST_TYPE_DOUBLE ) ;	// 这个说要吃牌的玩家
	PTKREQMAHJONGDBL pReqMahJongDbl = ( PTKREQMAHJONGDBL )pRequestObj->GetPlayerAckMsg( nSeat ) ;
	if ( NULL == pReqMahJongDbl )
	{
		// 出错了
		TKWriteLog( "%s : %d, 取玩家加倍消息出错，消息为NULL", __FILE__, __LINE__ );

		AutoDiscardTile( nSeat );
		return false;
	}

	if ( !pReqMahJongDbl->isDouble ) return false;
	int seat = pReqMahJongDbl->seat;

	// 次数累计
	m_pMahjongObj->AckDouble( seat );

	// 广播加倍消息
	TKACKDOUBLING ackDoubling	= { 0 };
	ackDoubling.header.dwType	= ACK_TYPE( TK_MSG_DOUBLING );
	ackDoubling.header.dwLength = MSG_LENGTH( TKACKDOUBLING );
	ackDoubling.seat			= seat;
	ackDoubling.count			= m_pMahjongObj->GetAllTingCount(seat);
	PTKREQMAHJONGWIN pReqMahJongWin = (PTKREQMAHJONGWIN)&pReqMahJongDbl->dblMsg;
	if ( NULL != pReqMahJongWin )
	{
		ackDoubling.tileId			= pReqMahJongWin->nTileID;
	}

	if ( !Broadcast( PTKHEADER(&ackDoubling) ) )
	{
		TKWriteLog( "%s : %d, Error! 发送加倍消息出错, len( %d )", __FILE__, __LINE__, ackDoubling.header.dwLength );
	}

	// 加倍提示处理
	m_pMahjongObj->SendDblTrip( seat );

	// 记录加倍消息
	char szGameActionBuf[ 512 ] = { 0 };
	sprintf_s( szGameActionBuf, "<action name=\"double\" time=\"%d\" seat=\"%d\" objects=\"%d\" />", time(NULL), seat, ackDoubling.count );
	// m_pMahjongGame->AddGameAction( szGameActionBuf ); //做录像标准化时同策划确定，这个功能已撤销，不需要记录了

	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardTileState::OnReqDouble( PTKREQMAHJONGDBL pReqMahjongDbl )
{
	PTKREQMAHJONGWIN pReqMahJongWin = (PTKREQMAHJONGWIN)&pReqMahjongDbl->dblMsg;
	int nSeat = pReqMahJongWin->nSeat;
	CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerBySeat( nSeat );
	if( NULL == pPlayer ) return false;
	CMahJongTile *pDrawTile = pPlayer->GetDrawTile() ;	// 这个玩家最后抓的牌
	TKMARK_LASTEXECLINE;
	if( NULL == pDrawTile || !pDrawTile->IDIs( pReqMahJongWin->nTileID ) )
	{ 
		// 不是抓的那张牌
		//TKWriteLog( "[%s:%d]user(%d[%s]) not win draw tile(0x%03X)" , __FILE__ , __LINE__ , pPlayer->UserID() , pPlayer->NickName() , ( NULL == pDrawTile ) ? 0 : pDrawTile->ID() ) ;
		// 测试中发现，当玩家天和的时候，有时候服务器端和客户端的“刚抓的牌”并不一致，所以这里对天和单独处理
		if( m_pMahjongObj->IsTian( nSeat ) && ( nSeat == m_pMahjongObj->Banker() ) )
		{ // 这个玩家是天和
			if( !pPlayer->HasTile( pReqMahJongWin->nTileID ) )
			{ // 玩家手中没有这张牌
				TKWriteLog( "[%s:%d]user(%d[%s]) tian win,no tile(0x%03X)" , __FILE__ , __LINE__ , pPlayer->UserID() , pPlayer->NickName() , pReqMahJongWin->nTileID ) ;
				return false ;
			}
		}
		else
		{ // 不是天和
			return false ;
		}
	}

	// 看看能否和牌
	if ( !m_pMahjongObj->PlayerReqWin( pReqMahJongWin, false ) )  return false;

	// 能否加倍
	if ( !m_pMahjongObj->PlayerReqDbl(pReqMahjongDbl) ) return false;

	CEvent event( EVENT_DOUBLE, pReqMahjongDbl->seat );
	PostEvent( event );
	m_canDbl = false;

	return true;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardTileState::OnReqDiscardTile( CEvent * pEvent )
{
	PTKREQMAHJONGDISCARDTILE pReqMahJongDiscardTile = ( PTKREQMAHJONGDISCARDTILE )pEvent->m_pParam;
	ASSERT( NULL != pReqMahJongDiscardTile );
	if ( pReqMahJongDiscardTile->header.dwLength != MSG_LENGTH( TKREQMAHJONGDISCARDTILE ) )
	{
		return false;
	}

	// 检查消息是否合法
	if( !CheckMsg( pEvent->m_dwPoster, pReqMahJongDiscardTile->nSeat , MSG_LENGTH( TKREQMAHJONGDISCARDTILE ), 
		( PTKHEADER ) pReqMahJongDiscardTile, pReqMahJongDiscardTile->nRequestID, __FILE__ , __LINE__ ) )
	{ 
		return false;
	}

	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	CTKMahjongPlayer* pTKMahjongPlayer = m_pMahjongObj->GetPlayerBySeat(pReqMahJongDiscardTile->nSeat);
	if (pRequestObj->CheckDelayDiscard(m_pMahjongGame->Rule()->GetRule(RULE_WAIT_DISCARDTIME)))
	{
		pTKMahjongPlayer->AutoAddDelayCount();
	}
	else
	{
		pTKMahjongPlayer->ResetDelayCount();
	}

	return OnReqDiscardTile( pReqMahJongDiscardTile );
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardTileState::OnReqCall( CEvent * pEvent )
{
	PTKREQMAHJONGCALL pReqMahJongCall = ( PTKREQMAHJONGCALL )pEvent->m_pParam;
	ASSERT( NULL != pReqMahJongCall );
	if ( pReqMahJongCall->header.dwLength != MSG_LENGTH( TKREQMAHJONGCALL ) )
	{
		return false;
	}

	// 检查消息是否合法
	if( !CheckMsg( pEvent->m_dwPoster, pReqMahJongCall->nSeat , MSG_LENGTH( TKREQMAHJONGCALL ), 
		( PTKHEADER ) pReqMahJongCall, pReqMahJongCall->nRequestID, __FILE__ , __LINE__ ) )
	{ 
		return false;
	}

	return OnReqCall( pReqMahJongCall );
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardTileState::OnReqCallTile( CEvent * pEvent )
{
	PTKREQMAHJONGCALLTILE pReqMahJongCallTile = ( PTKREQMAHJONGCALLTILE )pEvent->m_pParam;
	ASSERT( NULL != pReqMahJongCallTile );
	if ( pReqMahJongCallTile->header.dwLength != MSG_LENGTH( TKREQMAHJONGCALLTILE ) )
	{
		return false;
	}

	// 检查消息是否合法
	if( !CheckMsg( pEvent->m_dwPoster, pReqMahJongCallTile->nSeat , MSG_LENGTH( TKREQMAHJONGCALLTILE ), 
		( PTKHEADER ) pReqMahJongCallTile, pReqMahJongCallTile->nRequestID, __FILE__ , __LINE__ ) )
	{ 
		return false;
	}

	return OnReqCallTile( pReqMahJongCallTile );
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardTileState::OnReqWin( CEvent * pEvent )
{
	PTKREQMAHJONGWIN pReqMahJongWin = ( PTKREQMAHJONGWIN )pEvent->m_pParam;
	ASSERT( NULL != pReqMahJongWin );
	if ( pReqMahJongWin->header.dwLength != MSG_LENGTH( TKREQMAHJONGWIN ) )
	{
		return false;
	}

	// 检查消息是否合法
	if( !CheckMsg( pEvent->m_dwPoster, pReqMahJongWin->nSeat , MSG_LENGTH( TKREQMAHJONGWIN ), 
		( PTKHEADER ) pReqMahJongWin, pReqMahJongWin->nRequestID, __FILE__ , __LINE__ ) )
	{ 
		return false;
	}

	// 检测下请求状态（若有人加倍，则不允许加倍、和牌）
	if ( m_pMahjongGame->Rule()->GetRule(RULE_DOUBLE_COUNT) )
	{
		CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
		if ( !pRequestObj->IsRequestType( REQUEST_TYPE_WIN ) )
		{
			return false;
		}
	}

	return OnReqWin( pReqMahJongWin );
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardTileState::OnReqChangeFlower( PTKREQMAHJONGCHANGEFLOWER pReqChangeFlower )
{
	// 看看这个玩家手中是否有这张牌
	int nSeat = pReqChangeFlower->nSeat;
	CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerBySeat( nSeat ) ;	// 这个玩家
	int nTileID = pReqChangeFlower->nTileID ;	// 牌的ID号
	if( !pPlayer->HasTile( nTileID ) )
	{ // 玩家手中没有这张牌
		TKWriteLog( "[%s:%d]user(%d[%s]) no tile(0x%03X)" , __FILE__ , __LINE__ , pPlayer->UserID(), pPlayer->NickName(), nTileID ) ;
		pPlayer->TraceInfo() ;
		return FALSE ;
	}

	// 设置这个玩家已经回复了,否则万一因为某种原因导致服务器陷入久等状态,这个玩家可能会被当作替罪羊
	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	pRequestObj->SetAckQuestSeat( nSeat, REQUEST_TYPE_CHANGE_FLOWER );

    // 让玩家补花
    int nFlowerCount = m_pMahjongObj->PlayerChangeFlower(pReqChangeFlower);
    m_pMahjongGame->SendChangeFlowerAck(nFlowerCount, pReqChangeFlower);

	// 记录补花
	DWORD dwDifMs = m_pMahjongGame->GetRtValue();
	CTKBuffer bufGameAction;
	bufGameAction.AppendFormatText("<a id=\"%d\" rt=\"%d\" rtms=\"%d\" s=\"%d\" fti=\"%d\" />",
		RECORD_FLOWER,
		dwDifMs / 1000,
		dwDifMs,
		nSeat,
		nTileID);
	m_pMahjongGame->AddGameAction( bufGameAction.GetBufPtr() );

	int nLoseSeat = m_pMahjongObj->GetNextPlayerSeat(nSeat);
	if (m_pMahjongGame->IsIslandMatch() && m_pMahjongGame->IsEndGameEarly(nLoseSeat))
	{
		CTKMahjongPlayer * pPlayOutPlayer = m_pMahjongObj->GetPlayerBySeat(nLoseSeat);
		std::string strNotify(pPlayOutPlayer->NickName());
		strNotify.append(" 玩家积分已输光，本局结束");
		Notify(MAHJONG_NOTIFY_PLAYER_NOCHIP, strNotify.c_str());

		IState * curResultState = m_pFSMachine->SetState("CResultState");
		curResultState->OnEnter(NULL);

		return true;
	}

	ResetDbl();
	return true;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardTileState::OnReqGang( PTKREQMAHJONGGANG pReqMahJongGang )
{
	if ( !m_pMahjongObj->PlayerReqGang( pReqMahJongGang, true ) )
	{
		// 不能杠牌
		return false;
	}

	// 让玩家杠牌
	m_pMahjongObj->PlayerGang( pReqMahJongGang );

	int nSeat = pReqMahJongGang->nSeat;

	// 如果是补杠，那么还要问其他玩家是否要抢杠
	if( IS_GANG_TYPE_PATCH( pReqMahJongGang->nGangType ) )
	{ // 补杠
		// 发个消息,有人补杠了
		CEvent event( EVENT_BUGANG, nSeat );
		PostEvent( event );
	}
	else
	{ // 不是补杠（暗杠或者明杠的直杠）
		CEvent event( EVENT_GANG, nSeat );
		PostEvent( event );
	}

	// 记录杠牌
	DWORD dwDifMs = m_pMahjongGame->GetRtValue();
	CTKBuffer bufGameAction;
	bufGameAction.AppendFormatText("<a id=\"%d\" rt=\"%d\" rtms=\"%d\" s=\"%d\" gti=\"%d\" gt=\"%d\" gtl=\"%d,%d,%d,%d\" />",
		RECORD_GANG,
		dwDifMs / 1000,
		dwDifMs,
		nSeat,
		pReqMahJongGang->nTileID,
		pReqMahJongGang->nGangType, 
		pReqMahJongGang->anGangTileID[0], 
		pReqMahJongGang->anGangTileID[1], 
		pReqMahJongGang->anGangTileID[2], 
		pReqMahJongGang->anGangTileID[3] );

	m_pMahjongGame->AddGameAction( bufGameAction.GetBufPtr() );

	ResetDbl();

	return true;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardTileState::OnReqDiscardTile( PTKREQMAHJONGDISCARDTILE pReqMahJongDiscardTile )
{
	//// for test
	//TCHAR szTileName[32] = {0};
	//CMahJongTile::GetTileName(pReqMahJongDiscardTile->nTileID, szTileName, sizeof(szTileName));
	//CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerBySeat( pReqMahJongDiscardTile->nSeat );
	//TKWriteLog("收到玩家[%s]出牌消息 出的牌是[%s] --time = %u", 
	//	pPlayer->NickName(),
	//	szTileName,
	//	GetTickCount());

	ResetDbl();

	// 让obj去处理
	bool ret = m_pMahjongObj->PlayerReqDiscardTile( pReqMahJongDiscardTile );

	if ( ret )
	{
		// 记录打牌
		DWORD dwDifMs = m_pMahjongGame->GetRtValue();
		CTKBuffer bufGameAction;
		bufGameAction.AppendFormatText("<a id=\"%d\" rt=\"%d\" 	rtms=\"%d\" s=\"%d\" dti=\"%d\" />",
			RECORD_DISCARD,
			dwDifMs / 1000,
			dwDifMs,
			pReqMahJongDiscardTile->nSeat,
			pReqMahJongDiscardTile->nTileID );

		m_pMahjongGame->AddGameAction( bufGameAction.GetBufPtr() );
	}
	

	return ret;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardTileState::OnReqCall( PTKREQMAHJONGCALL pReqMahJongCall )
{
	// 让obj去处理
	bool ret = m_pMahjongObj->PlayerReqCallAndDiscardTile( pReqMahJongCall );

	if ( ret )
	{
		// 记录听牌
		DWORD dwDifMs = m_pMahjongGame->GetRtValue();
		CTKBuffer bufGameAction;
		bufGameAction.AppendFormatText("<a id=\"%d\" rt=\"%d\" rtms=\"%d\" s=\"%d\" tti=\"%d\" ag=\"%d\" ws=\"%d\" />",
			RECORD_TING,
			dwDifMs / 1000,
			dwDifMs,
			pReqMahJongCall->nSeat, 
			pReqMahJongCall->nTileID, 
			pReqMahJongCall->bAutoGang, 
			pReqMahJongCall->bWinSelf );

		m_pMahjongGame->AddGameAction( bufGameAction.GetBufPtr() );
	}

	ResetDbl();

	return ret;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardTileState::OnReqCallTile( PTKREQMAHJONGCALLTILE pReqMahjongCallTile )
{
    return true;

	if (m_pMahjongObj)
	{
		CTKMahjongServerPlayer * pPlayer = (CTKMahjongServerPlayer *)m_pMahjongObj->GetPlayerBySeat( pReqMahjongCallTile->nSeat ) ;
		if (pPlayer)
		{
			pPlayer->SetCallInfo(pReqMahjongCallTile->cnCallTileCount, pReqMahjongCallTile->nCallTileID);
			return true;
		}
	}

	return false;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardTileState::OnReqWin( PTKREQMAHJONGWIN pReqMahJongWin )
{
	// 检查一下和的那张牌是不是刚抓的那张牌,如果不是说明是做弊
	int nSeat = pReqMahJongWin->nSeat;
	CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerBySeat( nSeat );
	ASSERT( NULL != pPlayer );
	CMahJongTile *pDrawTile = pPlayer->GetDrawTile() ;	// 这个玩家最后抓的牌
TKMARK_LASTEXECLINE;
	if( NULL == pDrawTile || !pDrawTile->IDIs( pReqMahJongWin->nTileID ) )
	{ 
		// 不是抓的那张牌
		//TKWriteLog( "[%s:%d]user(%d[%s]) not win draw tile(0x%03X)" , __FILE__ , __LINE__ , pPlayer->UserID() , pPlayer->NickName() , ( NULL == pDrawTile ) ? 0 : pDrawTile->ID() ) ;
		// 测试中发现，当玩家天和的时候，有时候服务器端和客户端的“刚抓的牌”并不一致，所以这里对天和单独处理
		if( m_pMahjongObj->IsTian( nSeat ) && ( nSeat == m_pMahjongObj->Banker() ) )
		{ // 这个玩家是天和
			if( !pPlayer->HasTile( pReqMahJongWin->nTileID ) )
			{ // 玩家手中没有这张牌
				TKWriteLog( "[%s:%d]user(%d[%s]) tian win,no tile(0x%03X)" , __FILE__ , __LINE__ , pPlayer->UserID() , pPlayer->NickName() , pReqMahJongWin->nTileID ) ;
				return false ;
			}
		}
		else
		{ // 不是天和
			return false ;
		}
	}

	// 再让obj检查一下这个玩家能不能和(能不能满足其它条件)
	if ( !m_pMahjongObj->PlayerReqWin( pReqMahJongWin ) )
	{
		return false;
	}

	// 设置玩家回复要和牌
	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	TKWriteLog( "玩家(%d)请求和牌", nSeat );
	pRequestObj->SetAckQuestSeat( nSeat , REQUEST_TYPE_WIN , ( PTKHEADER ) pReqMahJongWin ) ;

	// 让这个玩家和牌
	PostEvent( EVENT_WIN_SELF );

	ResetDbl();

	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardTileState::OnAnGang( CEvent * pEvent )
{
	m_pMahjongObj->CalcGangScore(pEvent->m_dwPoster);

	int nLoseSeat = m_pMahjongObj->GetNextPlayerSeat(pEvent->m_dwPoster);
	if (m_pMahjongGame->IsIslandMatch() && m_pMahjongGame->IsEndGameEarly(nLoseSeat))
	{
		//TKWriteLog("杠牌积分输光 IsEndGameEarly");

		CTKMahjongPlayer * pPlayOutPlayer = m_pMahjongObj->GetPlayerBySeat(nLoseSeat);
		std::string strNotify(pPlayOutPlayer->NickName());
		strNotify.append(" 玩家积分已输光，本局结束");
		Notify(MAHJONG_NOTIFY_PLAYER_NOCHIP, strNotify.c_str());

		IState * curResultState = m_pFSMachine->SetState("CResultState");
		curResultState->OnEnter(NULL);
	}

	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardTileState::OnBuGang( CEvent * pEvent )
{
	if (getNextState() == "CWaitWinGangState")
	{
		//等对家确定不被抢杠才扣分
		return true;
	}
	
	m_pMahjongObj->CalcGangScore(pEvent->m_dwPoster);

	int nLoseSeat = m_pMahjongObj->GetNextPlayerSeat(pEvent->m_dwPoster);
	if (m_pMahjongGame->IsIslandMatch() && m_pMahjongGame->IsEndGameEarly(nLoseSeat))
	{
		//TKWriteLog("杠牌积分输光 IsEndGameEarly");

		CTKMahjongPlayer * pPlayOutPlayer = m_pMahjongObj->GetPlayerBySeat(nLoseSeat);
		std::string strNotify(pPlayOutPlayer->NickName());
		strNotify.append(" 玩家积分已输光，本局结束");
		Notify(MAHJONG_NOTIFY_PLAYER_NOCHIP, strNotify.c_str());

		IState * curResultState = m_pFSMachine->SetState("CResultState");
		curResultState->OnEnter(NULL);
	}

	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardTileState::OnPlayerWinSelf( CEvent * pEvent )
{
	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardTileState::OnTrustPlay( CEvent * pEvent )
{
	try
	{

	if ( NULL != pEvent->m_pParam )
	{
		PTKREQTRUSTPLAY pReqTrustPlay = ( PTKREQTRUSTPLAY )pEvent->m_pParam;
	}

	// 替他出牌
	CTKMahjongServerPlayer * pPlayer = (CTKMahjongServerPlayer *)m_pMahjongObj->GetPlayerByID( pEvent->m_dwPoster );
	if ( NULL == pPlayer )
	{
		TKWriteLog( "%s : %d, Error! ( %d )托管了，但他不是本桌的玩家", __FILE__, __LINE__, pEvent->m_dwPoster );
		return false;
	}
	int nSeat = pPlayer->Seat();
	if ( m_pMahjongObj->CurDiscardSeat() == nSeat )
	{
		if (pPlayer->HasCalled())
		{
			CMahJongTile* nTile = pPlayer->GetDrawTile();
			if (nTile)
			{
				if (pPlayer->IsCallTile(nTile->ID()))
				{
					AutoWin(nSeat, nTile);
					return true;
				}
			}

            // 玩家已听牌，听口不变情况下，自动补杠暗杠

		}

        AutoDiscardTile( nSeat ) ;
	}

	return true;

	}
	catch (...)
	{
		TKWriteLog( "%s : %d, Exception", __FILE__, __LINE__ );
	}

    return false;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardTileState::OnCheckTimeout( CEvent * pEvent )
{
	PTKREQCHECKTIMEOUT pReqCheckTimeout = ( PTKREQCHECKTIMEOUT )pEvent->m_pParam;
	CTKMahjongPlayer *pPlayer = m_pMahjongObj->GetPlayerByID(pReqCheckTimeout->m_dwUserID);

	//// for test
	//TKWriteLog("收到checktimeout req 超时玩家为[%s] --time = %u", 
	//	pPlayer->NickName(),
	//	GetTickCount());

	if ((NULL != pPlayer) && (m_pMahjongGame->RequestObj()->CheckOverTime(pPlayer->Seat(), m_pMahjongGame->Rule()->GetRule( RULE_DISCARDTIME ))))
	{
		TKREQTRUSTPLAY reqTrustPlay = { 0 } ;
		reqTrustPlay.header.dwType   = REQ_TYPE( TK_MSG_TRUSTPLAY ) ;
		reqTrustPlay.header.dwLength = MSG_LENGTH( TKREQTRUSTPLAY ) ;
		reqTrustPlay.dwUserID = pPlayer->UserID();
		reqTrustPlay.dwType = TRUST_OVERTIME;
		m_pMahjongGame->OnPlayerMsg( pPlayer->UserID(), pPlayer->Seat(), ( PTKHEADER )&reqTrustPlay );
		//// for test
		//TKWriteLog("将玩家[%s]置成托管 --time = %u", pPlayer->NickName(), GetTickCount());
	}

	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CWaitWinGangState::CWaitWinGangState()
{
	REGIST_EVENT_FUNCTION( EVENT_ACK_REQUEST, &CWaitWinGangState::OnAckRequest );
	REGIST_EVENT_FUNCTION( EVENT_REQ_WIN, &CWaitWinGangState::OnReqWin );
	REGIST_EVENT_FUNCTION( EVENT_NOWINGANG, &CWaitWinGangState::OnNoReqWin );
	REGIST_EVENT_FUNCTION( EVENT_WINGANG, &CWaitWinGangState::OnWinGang );
	REGIST_EVENT_FUNCTION( EVENT_TRUSTPLAY, &CWaitWinGangState::OnTrustPlay );
	REGIST_EVENT_FUNCTION( EVENT_REQ_DOUBLE, &CWaitWinGangState::OnReqDouble );
	REGIST_EVENT_FUNCTION( EVENT_DOUBLE, &CWaitWinGangState::OnPlayerDouble );
	REGIST_EVENT_FUNCTION( EVENT_CHECKTIMEOUT, &CWaitWinGangState::OnCheckTimeout );

	m_canDbl = true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CWaitWinGangState::OnEnter( CEvent * pEvent )
{
	// 给所有人发消息,是不是要抢杠
	int nGangSeat = m_pMahjongObj->LastGangSeat();
	// SendRequestWinGangMsg( nGangSeat );
	SendRequestWinGangMsgEx( nGangSeat );
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
// 自动胡牌
void CWaitWinGangState::AutoWin( int nSeat, CMahJongTile* nTile)
{
	CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerBySeat( nSeat );
	PTKREQMAHJONGWIN pReqMahJongWin = (PTKREQMAHJONGWIN)m_winBuffer;
	int  nPaoSeat = m_pMahjongObj->LastGangSeat();
	if (m_pMahjongObj->CheckCanWin(pReqMahJongWin, nSeat, nPaoSeat, nTile))
	{        
		CEvent event( REQ_TYPE( TK_MSG_MAHJONG_WIN ), pPlayer->UserID(), (void*)pReqMahJongWin );
		PostEvent( event );
	}
	else
	{
		CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
		pRequestObj->SetAckQuestSeat( nSeat , 0 ) ;	// 设置这个玩家回复了
	}
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CWaitWinGangState::SendRequestWinGangMsg( int nGangSeat )
{
	int nRequestType = REQUEST_TYPE_WIN ;
	int nWaitSecond  = Rule()->GetRule( RULE_WAITTIME );
	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	pRequestObj->ResetRequest( nRequestType , nWaitSecond ) ;

	// 给不是nGangSeat的玩家发送请求消息
	TKREQMAHJONGREQUEST reqMahJongRequest = { 0 } ;
	for( int i = 0; i < MAX_PLAYER_COUNT; i ++ )
	{ // 遍历所有座位号
		if( nGangSeat == i ) continue ;	// 略过杠牌的玩家（没有给杠牌的玩家和他的旁观者发送消息）
		if ( !m_pMahjongObj->HasPlayer( i ) ) 
		{
			// 这个座位上没人
			continue;
		}
		CTKMahjongServerPlayer * pPlayer = (CTKMahjongServerPlayer *)m_pMahjongObj->GetPlayerBySeat( i );

		pRequestObj->FillRequestMsg( &reqMahJongRequest , i ) ;
		if ( !Send2SeatPlayer( i, ( PTKHEADER )&reqMahJongRequest ) )
		{
			TKWriteLog( "%s : %d, Error! 发送消息错误 len( %d )", __FILE__, __LINE__, reqMahJongRequest.header.dwLength );
		}

		// 如果这个座位的玩家托管了，那么还要帮他回答
		CTKGamePlayer* pTKGamePlayer = m_pMahjongGame->GetGamePlayerBySeat(i);
		if (pPlayer->IsAutoPlay()
			|| (pTKGamePlayer != NULL && pTKGamePlayer->m_nOpertorType == TK_GAMEPLAYEROPERATORTYPE_BOTTRUST))
		{ // 需要帮这个玩家处理这个消息

			if (pPlayer->HasCalled())
			{
				CMahJongTile* nTile = m_pMahjongObj->GetLastGangTile();
				if (nTile)
				{
					if (pPlayer->IsCallTile(nTile->ID()))
					{
						if( pPlayer->HasCalledPower( REQUEST_TYPE_WIN , nGangSeat ) )
						{
							AutoWin(i, nTile);
						}
						else
						{
							pRequestObj->SetAckQuestSeat( i , 0 ) ;	// 设置这个玩家回复了
						}
					}
					else
					{
						pRequestObj->SetAckQuestSeat( i , 0 ) ;	// 设置这个玩家回复了
					}
				}
				else
				{
					pRequestObj->SetAckQuestSeat( i , 0 ) ;	// 设置这个玩家回复了
				}
			}
			else
			{
				pRequestObj->SetAckQuestSeat( i , 0 ) ;	// 设置这个玩家回复了
			}
		}
	}

	// 检查回复的状态
	CheckAndDealAckRequest() ;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CWaitWinGangState::SendRequestWinGangMsgEx( int nGangSeat )
{
	if ( !m_pMahjongGame->Rule()->GetRule(RULE_DOUBLE_COUNT) )
	{
		SendRequestWinGangMsg(nGangSeat);
		return;
	}

	int nRequestType = REQUEST_TYPE_NULL;
	if ( !m_canDbl )
	{
		m_canDbl = true;
	}
	else
	{
		nRequestType = REQUEST_TYPE_WIN | REQUEST_TYPE_DOUBLE;
		m_canDbl = false;
	}

	int nWaitSecond  = Rule()->GetRule( RULE_WAITTIME );
	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	pRequestObj->ResetRequest( nRequestType , nWaitSecond ) ;

	// 给不是nGangSeat的玩家发送请求消息
	TKREQMAHJONGREQUEST reqMahJongRequest = { 0 } ;
	for( int i = 0; i < MAX_PLAYER_COUNT; i ++ )
	{ // 遍历所有座位号
		if( nGangSeat == i ) continue ;	// 略过杠牌的玩家（没有给杠牌的玩家和他的旁观者发送消息）
		if ( !m_pMahjongObj->HasPlayer( i ) ) 
		{
			// 这个座位上没人
			continue;
		}
		CTKMahjongServerPlayer * pPlayer = (CTKMahjongServerPlayer *)m_pMahjongObj->GetPlayerBySeat( i );
		pRequestObj->FillRequestMsg( &reqMahJongRequest , i ) ;
		if ( !Send2SeatPlayer( i, ( PTKHEADER )&reqMahJongRequest ) )
		{
			TKWriteLog( "%s : %d, Error! 发送消息错误 len( %d )", __FILE__, __LINE__, reqMahJongRequest.header.dwLength );
		}

		// 如果这个座位的玩家托管了，那么还要帮他回答
		CTKGamePlayer* pTKGamePlayer = m_pMahjongGame->GetGamePlayerBySeat(i);
		if (pPlayer->IsAutoPlay()
			|| (pTKGamePlayer != NULL && pTKGamePlayer->m_nOpertorType == TK_GAMEPLAYEROPERATORTYPE_BOTTRUST))
		{ // 需要帮这个玩家处理这个消息

			if (pPlayer->HasCalled())
			{
				CMahJongTile* nTile = m_pMahjongObj->GetLastGangTile();
				if (nTile)
				{
					if (pPlayer->IsCallTile(nTile->ID()))
					{
						if( pPlayer->HasCalledPower( REQUEST_TYPE_WIN , nGangSeat ) )
						{
							AutoWin(i, nTile);
						}
						else
						{
							pRequestObj->SetAckQuestSeat( i , 0 ) ;	// 设置这个玩家回复了
						}
					}
					else
					{
						pRequestObj->SetAckQuestSeat( i , 0 ) ;	// 设置这个玩家回复了
					}
				}
				else
				{
					pRequestObj->SetAckQuestSeat( i , 0 ) ;	// 设置这个玩家回复了
				}
			}
			else
			{
				pRequestObj->SetAckQuestSeat( i , 0 ) ;	// 设置这个玩家回复了
			}
		}
	}

	// 检查回复的状态
	CheckAndDealAckRequest() ;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWaitWinGangState::OnReqDouble( CEvent * pEvent )
{
	if ( !m_pMahjongGame->Rule()->GetRule( RULE_DOUBLE_COUNT ) ) return false;

	PTKREQMAHJONGDBL pReqMahJongDouble = ( PTKREQMAHJONGDBL )pEvent->m_pParam;
	assert( NULL != pReqMahJongDouble );
	if ( pReqMahJongDouble->header.dwLength != MSG_LENGTH( TKREQMAHJONGDBL ) )
	{
		return false;
	}

	// 检查消息是否合法
	if( !CheckMsg( pEvent->m_dwPoster, pReqMahJongDouble->seat , MSG_LENGTH( TKREQMAHJONGDBL ), 
		( PTKHEADER ) pReqMahJongDouble, pReqMahJongDouble->nRequestID, __FILE__ , __LINE__ ) )
	{ 
		return false;
	}

	// 检测下请求状态（是否允许加倍、和牌）
	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	if ( !pRequestObj->IsRequestType( REQUEST_TYPE_WIN ) )
	{
		return false;
	}

	if ( !OnReqDouble( pReqMahJongDouble ) )
	{
		// 为了让游戏顺利进行,替他回复放弃
		CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
		pRequestObj->SetAckQuestSeat( pReqMahJongDouble->seat, 0 ) ;	// 设置这个玩家回复了

		// 检查回复的状态
		CheckAndDealAckRequest() ;

		return false;
	}

	// 检查回复的状态
	CheckAndDealAckRequest() ;

	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWaitWinGangState::OnReqDouble( PTKREQMAHJONGDBL pReqMahjongDbl )
{
	// 看看和的那张是不是玩家刚杠的那张牌
	PTKREQMAHJONGWIN pReqMahJongWin = (PTKREQMAHJONGWIN)&pReqMahjongDbl->dblMsg;
	if ( NULL == pReqMahJongWin )
	{
		return false;
	}

	CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerBySeat( pReqMahJongWin->nSeat );
	if ( NULL == pPlayer ) return false;
	if( !m_pMahjongObj->IsLastGangTile( pReqMahJongWin->nTileID ) )
	{ 
		// 不是最后一个杠的牌
		//TKWriteLog( "[%s:%d]user(%d[%s]) not win other player gang tile" , __FILE__ , __LINE__ , pPlayer->UserID(), pPlayer->NickName() ) ;
		return false;
	}

	// 再由obj检查一下是否满足其它的和牌条件
	if ( !m_pMahjongObj->PlayerReqWin( pReqMahJongWin, false ) )  return false;

	return m_pMahjongObj->PlayerReqDbl(pReqMahjongDbl);
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CWaitWinGangState::CheckAndDealAckRequest()
{
	int nAckType = CheckAckRequest();
	if ( -1 == nAckType )
	{
		// 还有人没有回复
		return;
	}
	m_canDbl = ( REQUEST_TYPE_DOUBLE == nAckType ) ? false : true;

	if ( REQUEST_TYPE_WIN == nAckType )
	{
		// 有人请求抢杠
		PostEvent( EVENT_WINGANG );
	}
	else if ( REQUEST_TYPE_DOUBLE == nAckType )
	{
		// 有人加倍
		CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
		int nSeat = pRequestObj->GetAckMsgSeat( REQUEST_TYPE_DOUBLE );
		CEvent event( EVENT_DOUBLE, nSeat );
		PostEvent( event );
	}
	else
	{
		// 没人抢杠
		int nSeat = m_pMahjongObj->LastGangSeat();
		CEvent event( EVENT_NOWINGANG, nSeat );
		PostEvent( event );
	}
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWaitWinGangState::OnReqWin( CEvent * pEvent )
{
	PTKREQMAHJONGWIN pReqMahJongWin = ( PTKREQMAHJONGWIN )pEvent->m_pParam;
	ASSERT( NULL != pReqMahJongWin );
	if ( pReqMahJongWin->header.dwLength != MSG_LENGTH( TKREQMAHJONGWIN ) )
	{
		return false;
	}

	// 检查消息是否合法
	if( !CheckMsg( pEvent->m_dwPoster, pReqMahJongWin->nSeat , MSG_LENGTH( TKREQMAHJONGWIN ), 
		( PTKHEADER ) pReqMahJongWin, pReqMahJongWin->nRequestID, __FILE__ , __LINE__ ) )
	{ 
		return false;
	}

	// 检测下请求状态（若有人加倍，则不允许加倍、和牌）
	if ( m_pMahjongGame->Rule()->GetRule(RULE_DOUBLE_COUNT) )
	{
		CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
		if ( !pRequestObj->IsRequestType( REQUEST_TYPE_WIN ) )
		{
			return false;
		}
	}

	if ( !OnReqWin( pReqMahJongWin ) )
	{
		// 为了让游戏顺利进行,替他回复放弃
		CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
		pRequestObj->SetAckQuestSeat( pReqMahJongWin->nSeat, 0 ) ;	// 设置这个玩家回复了

		// 检查回复的状态
		CheckAndDealAckRequest() ;
		
		return false;
	}

	// 检查回复的状态
	CheckAndDealAckRequest();

	return true;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWaitWinGangState::OnNoReqWin( CEvent * pEvent )
{
	m_pMahjongObj->CalcGangScore(pEvent->m_dwPoster);

	int nLoseSeat = m_pMahjongObj->GetNextPlayerSeat(pEvent->m_dwPoster);
	if (m_pMahjongGame->IsIslandMatch() && m_pMahjongGame->IsEndGameEarly(nLoseSeat))
	{
		//TKWriteLog("杠牌积分输光 IsEndGameEarly");

		CTKMahjongPlayer * pPlayOutPlayer = m_pMahjongObj->GetPlayerBySeat(nLoseSeat);
		std::string strNotify(pPlayOutPlayer->NickName());
		strNotify.append(" 玩家积分已输光，本局结束");
		Notify(MAHJONG_NOTIFY_PLAYER_NOCHIP, strNotify.c_str());

		IState * curResultState = m_pFSMachine->SetState("CResultState");
		curResultState->OnEnter(NULL);
	}

	// 返回true转换到下一状态
	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWaitWinGangState::OnWinGang( CEvent * pEvent )
{
	// 让玩家和牌
	// TODO:
	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWaitWinGangState::OnPlayerDouble( CEvent * pEvent )
{
	if ( !m_pMahjongGame->Rule()->GetRule(RULE_DOUBLE_COUNT) ) return false;

	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	int nSeat = pRequestObj->GetAckMsgSeat( REQUEST_TYPE_DOUBLE ) ;	// 这个说要吃牌的玩家
	PTKREQMAHJONGDBL pReqMahJongDbl = ( PTKREQMAHJONGDBL )pRequestObj->GetPlayerAckMsg( nSeat ) ;
	if ( NULL == pReqMahJongDbl )
	{
		// 出错了
		TKWriteLog( "%s : %d, 取玩家加倍消息出错，消息为NULL", __FILE__, __LINE__ );

		// 没人抢杠
		int nSeat = m_pMahjongObj->LastGangSeat();
		CEvent event( EVENT_NOWINGANG, nSeat );
		PostEvent( event );
	}

	if ( !pReqMahJongDbl->isDouble ) return false;
	int seat = pReqMahJongDbl->seat;

	// 次数累计
	m_pMahjongObj->AckDouble( seat );

	// 广播加倍消息
	TKACKDOUBLING ackDoubling	= { 0 };
	ackDoubling.header.dwType	= ACK_TYPE( TK_MSG_DOUBLING );
	ackDoubling.header.dwLength = MSG_LENGTH( TKACKDOUBLING );
	ackDoubling.seat			= seat;
	ackDoubling.count			= m_pMahjongObj->GetAllTingCount(seat);
	PTKREQMAHJONGWIN pReqMahJongWin = (PTKREQMAHJONGWIN)&pReqMahJongDbl->dblMsg;
	if ( NULL != pReqMahJongWin )
	{
		ackDoubling.tileId			= pReqMahJongWin->nTileID;
	}

	if ( !Broadcast( PTKHEADER(&ackDoubling) ) )
	{
		TKWriteLog( "%s : %d, Error! 发送加倍消息出错, len( %d )", __FILE__, __LINE__, ackDoubling.header.dwLength );
	}

	// 加倍提示处理
	m_pMahjongObj->SendDblTrip( seat );

	// 记录加倍消息
	char szGameActionBuf[ 512 ] = { 0 };
	sprintf_s( szGameActionBuf, "<action name=\"double\" time=\"%d\" seat=\"%d\" objects=\"%d\" />", time(NULL), seat, ackDoubling.count );
	//m_pMahjongGame->AddGameAction( szGameActionBuf ); //做录像标准化时同策划确定，这个功能已撤销，不需要记录了

	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWaitWinGangState::OnReqWin( PTKREQMAHJONGWIN pReqMahJongWin )
{
	// 看看和的那张是不是玩家刚杠的那张牌
	CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerBySeat( pReqMahJongWin->nSeat );
	if( !m_pMahjongObj->IsLastGangTile( pReqMahJongWin->nTileID ) )
	{ 
		// 不是最后一个杠的牌
		//TKWriteLog( "[%s:%d]user(%d[%s]) not win other player gang tile" , __FILE__ , __LINE__ , pPlayer->UserID(), pPlayer->NickName() ) ;
		return false;
	}

	// 能和返回true,不能和返回false
	if ( !m_pMahjongObj->PlayerReqWin( pReqMahJongWin ) )
	{
		return false;
	}

	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWaitWinGangState::OnAckRequest( CEvent * pEvent )
{
	PTKACKMAHJONGREQUEST pAckMahJongRequest = ( PTKACKMAHJONGREQUEST )pEvent->m_pParam;
	ASSERT( NULL != pAckMahJongRequest );
	if ( pAckMahJongRequest->header.dwLength != MSG_LENGTH( TKACKMAHJONGREQUEST ) )
	{
		return false;
	}

	// 检查消息是否合法
	if( !CheckMsg( pEvent->m_dwPoster, pAckMahJongRequest->nSeat , MSG_LENGTH( TKACKMAHJONGREQUEST ), 
		( PTKHEADER ) pAckMahJongRequest, pAckMahJongRequest->nRequestID, __FILE__ , __LINE__ ) )
	{ 
		return false;
	}

	return OnAckRequest( pAckMahJongRequest );
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWaitWinGangState::OnAckRequest( PTKACKMAHJONGREQUEST pAckMahJongRequest )
{
	// 设置这个玩家回复了请求，但是是放弃了操作
	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	pRequestObj->SetAckQuestSeat( pAckMahJongRequest->nSeat , 0 ) ;

	// 检查回复的状态
	CheckAndDealAckRequest();

	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWaitWinGangState::OnTrustPlay( CEvent * pEvent )
{
	int nLine = __LINE__;
	try
	{

	if ( NULL != pEvent->m_pParam )
	{
		nLine = __LINE__;
		PTKREQTRUSTPLAY pReqTrustPlay = ( PTKREQTRUSTPLAY )pEvent->m_pParam;
		nLine = __LINE__;
	}

	// 替他回复
	nLine = __LINE__;
	CTKMahjongServerPlayer * pPlayer = (CTKMahjongServerPlayer *)m_pMahjongObj->GetPlayerByID( pEvent->m_dwPoster );
	if ( NULL == pPlayer )
	{
		TKWriteLog( "%s : %d, Error! ( %d )托管了，但他不是本桌的玩家", __FILE__, __LINE__, pEvent->m_dwPoster );
		return false;
	}
	int nSeat = pPlayer->Seat();
	nLine = __LINE__;
	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	if ( pRequestObj->IsRequestSeat( nSeat ) && !pRequestObj->IsAckRequestSeat( nSeat ) )
	{
		if (pPlayer->HasCalled())
		{
			CMahJongTile* nTile = m_pMahjongObj->GetLastGangTile();
			if (nTile)
			{
				if (pPlayer->IsCallTile(nTile->ID()))
				{
					int nGangSeat = m_pMahjongObj->LastGangSeat();
					if( pPlayer->HasCalledPower( REQUEST_TYPE_WIN , nGangSeat ) )
					{
						AutoWin(nSeat, nTile);
					}
					else
					{
						pRequestObj->SetAckQuestSeat( nSeat , 0 ) ;	// 设置这个玩家回复了
					}
				}
				else
				{
					pRequestObj->SetAckQuestSeat( nSeat , 0 ) ;	// 设置这个玩家回复了
				}
			}
			else
			{
				pRequestObj->SetAckQuestSeat( nSeat , 0 ) ;	// 设置这个玩家回复了
			}
		}
		else
		{
			pRequestObj->SetAckQuestSeat( nSeat , 0 ) ;	// 设置这个玩家回复了
		}
	}
	nLine = __LINE__;

	// 检查回复的状态
	nLine = __LINE__;
	CheckAndDealAckRequest() ;
	nLine = __LINE__;

	return true;

	}
	catch (...)
	{
		TKWriteLog( "%s : %d, Exception", __FILE__, nLine );
	}

    return false;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWaitWinGangState::OnCheckTimeout( CEvent * pEvent )
{
	PTKREQCHECKTIMEOUT pReqCheckTimeout = ( PTKREQCHECKTIMEOUT )pEvent->m_pParam;
	CTKMahjongPlayer *pPlayer = m_pMahjongObj->GetPlayerByID(pReqCheckTimeout->m_dwUserID);
	if ((NULL != pPlayer) && (m_pMahjongGame->RequestObj()->CheckOverTime(pPlayer->Seat(), m_pMahjongGame->Rule()->GetRule( RULE_WAITTIME ))))
	{
		TKREQTRUSTPLAY reqTrustPlay = { 0 } ;
		reqTrustPlay.header.dwType   = REQ_TYPE( TK_MSG_TRUSTPLAY ) ;
		reqTrustPlay.header.dwLength = MSG_LENGTH( TKREQTRUSTPLAY ) ;
		reqTrustPlay.dwUserID = pPlayer->UserID();
		reqTrustPlay.dwType = TRUST_OVERTIME;
		m_pMahjongGame->OnPlayerMsg( pPlayer->UserID(), pPlayer->Seat(), ( PTKHEADER )&reqTrustPlay );
	}

	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CWaitState::CWaitState()
{
	m_canDbl = true;

	REGIST_EVENT_FUNCTION( EVENT_ACK_REQUEST, &CWaitState::OnAckRequest );
	REGIST_EVENT_FUNCTION( EVENT_REQ_WIN, &CWaitState::OnReqWin );
	REGIST_EVENT_FUNCTION( EVENT_REQ_GANG, &CWaitState::OnReqGang );
	REGIST_EVENT_FUNCTION( EVENT_REQ_PENG, &CWaitState::OnReqPeng );
	REGIST_EVENT_FUNCTION( EVENT_REQ_CHI, &CWaitState::OnReqChi );
	REGIST_EVENT_FUNCTION( EVENT_REQ_DOUBLE, &CWaitState::OnReqDouble );

	REGIST_EVENT_FUNCTION( EVENT_WIN_PAO, &CWaitState::OnPlayerWin );
	REGIST_EVENT_FUNCTION( EVENT_GANG, &CWaitState::OnPlayerGang );
	REGIST_EVENT_FUNCTION( EVENT_PENG, &CWaitState::OnPlayerPeng );
	REGIST_EVENT_FUNCTION( EVENT_CHI, &CWaitState::OnPlayerChi );
	REGIST_EVENT_FUNCTION( EVENT_DOUBLE, &CWaitState::OnPlayerDouble );
	REGIST_EVENT_FUNCTION( EVENT_ALLABORT, &CWaitState::OnAllAbort );
	REGIST_EVENT_FUNCTION( EVENT_TRUSTPLAY, &CWaitState::OnTrustPlay );
	REGIST_EVENT_FUNCTION( EVENT_CHECKTIMEOUT, &CWaitState::OnCheckTimeout );
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CWaitState::OnEnter( CEvent * pEvent )
{
	// SendRequestMsg();
	SendRequestMsgEx();
}

// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
// 自动胡牌
void CWaitState::AutoWin( int nSeat, CMahJongTile* nTile)
{
	CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerBySeat( nSeat );
	int nPaoSeat = m_pMahjongObj->CurDiscardSeat();
	PTKREQMAHJONGWIN pReqMahJongWin = (PTKREQMAHJONGWIN)m_winBuffer;
	if (m_pMahjongObj->CheckCanWin(pReqMahJongWin, nSeat, nPaoSeat, nTile))
	{
		CEvent event( REQ_TYPE( TK_MSG_MAHJONG_WIN ), pPlayer->UserID(), (void*)pReqMahJongWin );
		PostEvent( event );
	}
	else
	{
		CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
		pRequestObj->SetAckQuestSeat( nSeat , 0 ) ;	// 设置这个玩家回复了
	}
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CWaitState::SendRequestMsgEx()
{
	if ( !Rule()->GetRule(RULE_DOUBLE_COUNT) )
	{
		SendRequestMsg();
		return;
	}
	
	int nRequestType = REQUEST_TYPE_NULL;
	if ( m_canDbl )
	{
		nRequestType = REQUEST_TYPE_CHI | REQUEST_TYPE_PENG | REQUEST_TYPE_GANG | REQUEST_TYPE_DOUBLE | REQUEST_TYPE_WIN;
		m_canDbl = false;
	}
	else
	{ // 或者有发，或者玩家全部放弃
		nRequestType = REQUEST_TYPE_CHI | REQUEST_TYPE_PENG | REQUEST_TYPE_GANG;
		m_canDbl = true;
	}
	
	int nWaitSecond  = Rule()->GetRule( RULE_WAITTIME );
	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	pRequestObj->ResetRequest( nRequestType , nWaitSecond ) ;

	// 给不是nDiscardTileSeat的玩家发送请求消息
	int nDiscardTileSeat = m_pMahjongObj->CurDiscardSeat();
	TKREQMAHJONGREQUEST reqMahJongRequest = { 0 } ;
	for( int i = 0; i < MAX_PLAYER_COUNT; i ++ )
	{ // 遍历所有座位号
		if( nDiscardTileSeat == i ) 	// 略过出牌的玩家（没有给出牌的玩家和他的旁观者发送消息）
		{
			//发送消息
			TKREQCHECKTIMEOUT reqCheckTimeOut = {0};
			reqCheckTimeOut.header.dwType = REQ_TYPE(TK_MSG_CHECKTIMEOUT);
			reqCheckTimeOut.header.dwLength = MSG_LENGTH( TKREQCHECKTIMEOUT );
			reqCheckTimeOut.nWaitSecond = nWaitSecond;
			reqCheckTimeOut.m_dwUserID = 0;
			if (!Send2SeatPlayer( i, ( PTKHEADER )&reqCheckTimeOut ))
			{
				TKWriteLog( "%s : %d, Error! 发送消息错误 len( %d )", __FILE__, __LINE__, reqCheckTimeOut.header.dwLength );
			}
			continue ;
		}
		if ( !m_pMahjongObj->HasPlayer( i ) )
		{
			// 没这个玩家
			continue;
		}
		CTKMahjongServerPlayer * pPlayer = (CTKMahjongServerPlayer *)m_pMahjongObj->GetPlayerBySeat( i );
		pRequestObj->FillRequestMsg( &reqMahJongRequest , i ) ;

		// 发送给这个座位号的玩家
		if ( !Send2SeatPlayer( i, ( PTKHEADER )&reqMahJongRequest ) )
		{
			TKWriteLog( "%s : %d, Error! 发送消息错误 len( %d )", __FILE__, __LINE__, reqMahJongRequest.header.dwLength );
		}

		// 如果玩家退出或断线，且可胡，帮其应胡
		if ( ( pPlayer->IsBreak()
			|| !m_pMahjongGame->IsPlayerPlaying( i ) )
			&& !m_canDbl && m_pMahjongObj->CurDiscardTile() )
		{ // 抢杠时类似处理，但使用杠的牌id( 暂不加 )
			TKREQMAHJONGWIN reqMahJongWin = { 0 } ;
			CHECKRESULT eCheckWinResult = m_pMahjongObj->CheckBotWin( i, false, m_pMahjongObj->CurDiscardTile()->ID(), &reqMahJongWin );
			if ( T_OK == eCheckWinResult )
			{
				m_pMahjongGame->OnPlayerMsg( pPlayer->UserID(), i, (PTKHEADER)&reqMahJongWin );
				return;
			}
		}

		// 如果这个座位的玩家托管了，那么还要帮他回答
		CTKGamePlayer* pTKGamePlayer = m_pMahjongGame->GetGamePlayerBySeat(i);
		if (pPlayer->IsAutoPlay()
			|| (pTKGamePlayer != NULL && pTKGamePlayer->m_nOpertorType == TK_GAMEPLAYEROPERATORTYPE_BOTTRUST))
		{
			if (pPlayer->HasCalled())
			{
				CMahJongTile* nTile = m_pMahjongObj->CurDiscardTile();
				if (nTile)
				{
					if (pPlayer->IsCallTile(nTile->ID()))
					{
						if( pPlayer->HasCalledPower( REQUEST_TYPE_WIN , nDiscardTileSeat ) )
						{
							AutoWin(i, nTile);
						}
						else
						{
							pRequestObj->SetAckQuestSeat( i , 0 ) ;	// 设置这个玩家回复了
						}
					}
					else
					{
						pRequestObj->SetAckQuestSeat( i , 0 ) ;	// 设置这个玩家回复了
					}
				}
				else
				{
					pRequestObj->SetAckQuestSeat( i , 0 ) ;	// 设置这个玩家回复了
				}
			}
			else
			{
				pRequestObj->SetAckQuestSeat( i , 0 ) ;	// 设置这个玩家回复了
			}
		}
	}

	// 检查回复的状态
	CheckAndDealAckRequest() ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CWaitState::SendRequestMsg()
{
	int nRequestType = REQUEST_TYPE_CHI | REQUEST_TYPE_PENG | REQUEST_TYPE_GANG | REQUEST_TYPE_WIN ;
	int nWaitSecond  = Rule()->GetRule( RULE_WAITTIME );
	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	pRequestObj->ResetRequest( nRequestType , nWaitSecond ) ;

	// 给不是nDiscardTileSeat的玩家发送请求消息
	int nDiscardTileSeat = m_pMahjongObj->CurDiscardSeat();
	TKREQMAHJONGREQUEST reqMahJongRequest = { 0 } ;
	for( int i = 0; i < MAX_PLAYER_COUNT; i ++ )
	{ // 遍历所有座位号
		if( nDiscardTileSeat == i ) 	// 略过出牌的玩家（没有给出牌的玩家和他的旁观者发送消息）
		{
			//发送消息
			TKREQCHECKTIMEOUT reqCheckTimeOut = {0};
			reqCheckTimeOut.header.dwType = REQ_TYPE(TK_MSG_CHECKTIMEOUT);
			reqCheckTimeOut.header.dwLength = MSG_LENGTH( TKREQCHECKTIMEOUT );
			reqCheckTimeOut.nWaitSecond = nWaitSecond;
			reqCheckTimeOut.m_dwUserID = 0;
			if (!Send2SeatPlayer( i, ( PTKHEADER )&reqCheckTimeOut ))
			{
				TKWriteLog( "%s : %d, Error! 发送消息错误 len( %d )", __FILE__, __LINE__, reqCheckTimeOut.header.dwLength );
			}
			continue ;
		}
		if ( !m_pMahjongObj->HasPlayer( i ) )
		{
			// 没这个玩家
			continue;
		}
		CTKMahjongServerPlayer * pPlayer = (CTKMahjongServerPlayer *)m_pMahjongObj->GetPlayerBySeat( i );
		pRequestObj->FillRequestMsg( &reqMahJongRequest , i ) ;
		
		// 发送给这个座位号的玩家
		if ( !Send2SeatPlayer( i, ( PTKHEADER )&reqMahJongRequest ) )
		{
			TKWriteLog( "%s : %d, Error! 发送消息错误 len( %d )", __FILE__, __LINE__, reqMahJongRequest.header.dwLength );
		}


		// 如果玩家退出或断线，且可胡，帮其应胡
		if ( ( pPlayer->IsBreak()
			|| !m_pMahjongGame->IsPlayerPlaying( i ) )
			&& m_pMahjongObj->CurDiscardTile() )
		{
			TKREQMAHJONGWIN reqMahJongWin = { 0 } ;
			CHECKRESULT eCheckWinResult = m_pMahjongObj->CheckBotWin( i, false, m_pMahjongObj->CurDiscardTile()->ID(), &reqMahJongWin );
			if ( T_OK == eCheckWinResult )
			{
				m_pMahjongGame->OnPlayerMsg( pPlayer->UserID(), i, (PTKHEADER)&reqMahJongWin );
				return;
			}
		}

		// 如果这个座位的玩家托管了，那么还要帮他回答
		CTKGamePlayer* pTKGamePlayer = m_pMahjongGame->GetGamePlayerBySeat(i);
		if (pPlayer->IsAutoPlay()
			|| (pTKGamePlayer != NULL && pTKGamePlayer->m_nOpertorType == TK_GAMEPLAYEROPERATORTYPE_BOTTRUST))
		{ 
			if (pPlayer->HasCalled())
			{
				CMahJongTile* nTile = m_pMahjongObj->CurDiscardTile();
				if (nTile)
				{
					if (pPlayer->IsCallTile(nTile->ID()))
					{
						if( pPlayer->HasCalledPower( REQUEST_TYPE_WIN , nDiscardTileSeat ) )
						{
							AutoWin(i, nTile);
						}
						else
						{
							pRequestObj->SetAckQuestSeat( i , 0 ) ;	// 设置这个玩家回复了
						}
					}
					else
					{
						pRequestObj->SetAckQuestSeat( i , 0 ) ;	// 设置这个玩家回复了
					}
				}
				else
				{
					pRequestObj->SetAckQuestSeat( i , 0 ) ;	// 设置这个玩家回复了
				}
			}
			else
			{
				pRequestObj->SetAckQuestSeat( i , 0 ) ;	// 设置这个玩家回复了
			}
		}
	}

	// 检查回复的状态
	CheckAndDealAckRequest() ;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWaitState::OnReqGang( CEvent * pEvent )
{
	PTKREQMAHJONGGANG pReqMahJongGang = ( PTKREQMAHJONGGANG )pEvent->m_pParam;
	ASSERT( NULL != pReqMahJongGang );
	if ( pReqMahJongGang->header.dwLength != MSG_LENGTH( TKREQMAHJONGGANG ) )
	{
		return false;
	}

	// 检查消息是否合法
	if( !CheckMsg( pEvent->m_dwPoster, pReqMahJongGang->nSeat , MSG_LENGTH( TKREQMAHJONGGANG ), 
		( PTKHEADER ) pReqMahJongGang, pReqMahJongGang->nRequestID, __FILE__ , __LINE__ ) )
	{ 
		return false;
	}

	if ( !OnReqGang( pReqMahJongGang ) )
	{
		// 为了让游戏顺利进行,替他回复放弃
		CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
		pRequestObj->SetAckQuestSeat( pReqMahJongGang->nSeat, 0 ) ;	// 设置这个玩家回复了

		// 检查回复的状态
		CheckAndDealAckRequest() ;

		return false;
	}
	 
	// 检查回复的状态
	CheckAndDealAckRequest() ;

	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWaitState::OnReqChi( CEvent * pEvent )
{
	PTKREQMAHJONGCHI pReqMahJongChi = ( PTKREQMAHJONGCHI )pEvent->m_pParam;
	ASSERT( NULL != pReqMahJongChi );
	if ( pReqMahJongChi->header.dwLength != MSG_LENGTH( TKREQMAHJONGCHI ) )
	{
		return false;
	}

	// 检查消息是否合法
	if( !CheckMsg( pEvent->m_dwPoster, pReqMahJongChi->nSeat , MSG_LENGTH( TKREQMAHJONGCHI ), 
		( PTKHEADER ) pReqMahJongChi, pReqMahJongChi->nRequestID, __FILE__ , __LINE__ ) )
	{ 
		return false;
	}

	if ( !OnReqChi( pReqMahJongChi ) )
	{
		// 为了让游戏顺利进行,替他回复放弃
		CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
		pRequestObj->SetAckQuestSeat( pReqMahJongChi->nSeat, 0 ) ;	// 设置这个玩家回复了

		// 检查回复的状态
		CheckAndDealAckRequest() ;

		return false;
	}

	// 检查回复的状态
	CheckAndDealAckRequest() ;


	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWaitState::OnReqDouble( CEvent * pEvent )
{
	PTKREQMAHJONGDBL pReqMahJongDouble = ( PTKREQMAHJONGDBL )pEvent->m_pParam;
	assert( NULL != pReqMahJongDouble );
	if ( pReqMahJongDouble->header.dwLength != MSG_LENGTH( TKREQMAHJONGDBL ) )
	{
		return false;
	}
	if ( !m_pMahjongGame->Rule()->GetRule( RULE_DOUBLE_COUNT ) ) return false;

	// 检查消息是否合法
	if( !CheckMsg( pEvent->m_dwPoster, pReqMahJongDouble->seat , MSG_LENGTH( TKREQMAHJONGDBL ), 
		( PTKHEADER ) pReqMahJongDouble, pReqMahJongDouble->nRequestID, __FILE__ , __LINE__ ) )
	{ 
		return false;
	}

	// 检测下请求状态（是否允许加倍、和牌）
	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	if ( !pRequestObj->IsRequestType( REQUEST_TYPE_WIN ) )
	{
		return false;
	}

	if ( !OnReqDouble( pReqMahJongDouble ) )
	{
		// 为了让游戏顺利进行,替他回复放弃
		 CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
		 pRequestObj->SetAckQuestSeat( pReqMahJongDouble->seat, 0 ) ;	// 设置这个玩家回复了

		// 检查回复的状态
		 CheckAndDealAckRequest() ;

		return false;
	}

	// 检查回复的状态
	CheckAndDealAckRequest() ;

	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWaitState::OnReqPeng( CEvent * pEvent )
{
	PTKREQMAHJONGPENG pReqMahJongPeng = ( PTKREQMAHJONGPENG )pEvent->m_pParam;
	ASSERT( NULL != pReqMahJongPeng );
	if ( pReqMahJongPeng->header.dwLength != MSG_LENGTH( TKREQMAHJONGPENG ) )
	{
		return false;
	}

	// 检查消息是否合法
	if( !CheckMsg( pEvent->m_dwPoster, pReqMahJongPeng->nSeat , MSG_LENGTH( TKREQMAHJONGPENG ), 
		( PTKHEADER ) pReqMahJongPeng, pReqMahJongPeng->nRequestID, __FILE__ , __LINE__ ) )
	{ 
		return false;
	}

	if ( !OnReqPeng( pReqMahJongPeng ) )
	{
		// 为了让游戏顺利进行,替他回复放弃
		CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
		pRequestObj->SetAckQuestSeat( pReqMahJongPeng->nSeat, 0 ) ;	// 设置这个玩家回复了

		// 检查回复的状态
		CheckAndDealAckRequest() ;

		return false;
	}

	// 检查回复的状态
	CheckAndDealAckRequest() ;

	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWaitState::OnReqWin( CEvent * pEvent )
{
	PTKREQMAHJONGWIN pReqMahJongWin = ( PTKREQMAHJONGWIN )pEvent->m_pParam;
	ASSERT( NULL != pReqMahJongWin );
	if ( pReqMahJongWin->header.dwLength != MSG_LENGTH( TKREQMAHJONGWIN ) )
	{
		return false;
	}

	// 检查消息是否合法
	if( !CheckMsg( pEvent->m_dwPoster, pReqMahJongWin->nSeat , MSG_LENGTH( TKREQMAHJONGWIN ), 
		( PTKHEADER ) pReqMahJongWin, pReqMahJongWin->nRequestID, __FILE__ , __LINE__ ) )
	{ 
		return false;
	}

	// 检测下请求状态（若有人加倍，则不允许加倍、和牌）
	if ( m_pMahjongGame->Rule()->GetRule(RULE_DOUBLE_COUNT) )
	{
		CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
		if ( !pRequestObj->IsRequestType( REQUEST_TYPE_WIN ) )
		{
			return false;
		}
	}

	if ( !OnReqWin( pReqMahJongWin ) )
	{
		// 为了让游戏顺利进行,替他回复放弃
		CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
		pRequestObj->SetAckQuestSeat( pReqMahJongWin->nSeat, 0 ) ;	// 设置这个玩家回复了

		// 检查回复的状态
		CheckAndDealAckRequest() ;

		return false;
	}

	// 检查回复的状态
	CheckAndDealAckRequest() ;

	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWaitState::OnAckRequest( CEvent * pEvent )
{
	PTKACKMAHJONGREQUEST pAckMahJongRequest = ( PTKACKMAHJONGREQUEST )pEvent->m_pParam;
	ASSERT( NULL != pAckMahJongRequest );
	if ( pAckMahJongRequest->header.dwLength != MSG_LENGTH( TKACKMAHJONGREQUEST ) )
	{
		return false;
	}

	// 检查消息是否合法
	if( !CheckMsg( pEvent->m_dwPoster, pAckMahJongRequest->nSeat , MSG_LENGTH( TKACKMAHJONGREQUEST ), 
		( PTKHEADER ) pAckMahJongRequest, pAckMahJongRequest->nRequestID, __FILE__ , __LINE__ ) )
	{ 
		return false;
	}

	return OnAckRequest( pAckMahJongRequest );
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWaitState::OnReqChi( PTKREQMAHJONGCHI pReqMahJongChi )
{
	return m_pMahjongObj->PlayerReqChi( pReqMahJongChi );
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWaitState::OnReqDouble( PTKREQMAHJONGDBL pReqMahjongDbl )
{
	// 看看要和的是不是玩家刚打出来的那张牌
	PTKREQMAHJONGWIN pReqMahJongWin = (PTKREQMAHJONGWIN)&pReqMahjongDbl->dblMsg;
	if ( NULL == pReqMahJongWin )
	{
		return false;
	}

	// 点炮牌检验
	CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerBySeat( pReqMahJongWin->nSeat );
	if( !m_pMahjongObj->IsDiscardTile( pReqMahJongWin->nTileID ) )
	{ // 不是玩家最后出的那张牌
		//TKWriteLog( "[%s:%d]user(%d[%s]) not win other player discard tile" , __FILE__ , __LINE__ , pPlayer->UserID() , pPlayer->NickName() ) ;
		return false ;
	}

	// 再由obj检查一下是否满足其它的和牌条件
	if ( !m_pMahjongObj->PlayerReqWin( pReqMahJongWin, false ) )  return false;

	return m_pMahjongObj->PlayerReqDbl(pReqMahjongDbl);
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWaitState::OnReqPeng( PTKREQMAHJONGPENG pReqMahJongPeng )
{
	return m_pMahjongObj->PlayerReqPeng( pReqMahJongPeng );
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWaitState::OnReqGang( PTKREQMAHJONGGANG pReqMahJongGang )
{

	return m_pMahjongObj->PlayerReqGang( pReqMahJongGang, false );
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWaitState::OnReqWin( PTKREQMAHJONGWIN pReqMahJongWin )
{
	// 看看要和的是不是玩家刚打出来的那张牌
	CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerBySeat( pReqMahJongWin->nSeat );
	if( !m_pMahjongObj->IsDiscardTile( pReqMahJongWin->nTileID ) )
	{ // 不是玩家最后出的那张牌
		//TKWriteLog( "[%s:%d]user(%d[%s]) not win other player discard tile" , __FILE__ , __LINE__ , pPlayer->UserID() , pPlayer->NickName() ) ;
		return false ;
	}

	// 再由obj检查一下是否满足其它的和牌条件
	return m_pMahjongObj->PlayerReqWin( pReqMahJongWin );
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWaitState::OnAckRequest( PTKACKMAHJONGREQUEST pAckMahJongRequest )
{
	// 设置这个玩家回复了请求，但是是放弃了操作
	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	pRequestObj->SetAckQuestSeat( pAckMahJongRequest->nSeat , 0 ) ;

	// 检查回复的状态
	CheckAndDealAckRequest();

	return true;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CWaitState::CheckAndDealAckRequest()
{
	int nAckType = CheckAckRequest();
	if ( -1 == nAckType )
	{
		// 还有人没有回复
		return;
	}
	m_canDbl = ( REQUEST_TYPE_DOUBLE == nAckType ) ? false : true;

	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	if ( REQUEST_TYPE_WIN == nAckType )
	{
		// 有人和牌
		PostEvent( EVENT_WIN_PAO );
	}
	else if ( REQUEST_TYPE_GANG == nAckType )
	{
		// 有人杠牌
		int nSeat = pRequestObj->GetAckMsgSeat( REQUEST_TYPE_GANG );
		CEvent event( EVENT_GANG, nSeat );
		PostEvent( event );
	}
	else if ( REQUEST_TYPE_PENG == nAckType )
	{
		// 有人碰牌
		int nSeat = pRequestObj->GetAckMsgSeat( REQUEST_TYPE_PENG );
		CEvent event( EVENT_PENG, nSeat );
		PostEvent( event );
	}
	else if ( REQUEST_TYPE_CHI == nAckType )
	{
		// 有人吃牌
		int nSeat = pRequestObj->GetAckMsgSeat( REQUEST_TYPE_CHI );
		CEvent event( EVENT_CHI, nSeat );
		PostEvent( event );
	}
	else if ( REQUEST_TYPE_DOUBLE == nAckType )
	{
		// 有人加倍
		int nSeat = pRequestObj->GetAckMsgSeat( REQUEST_TYPE_DOUBLE );
		CEvent event( EVENT_DOUBLE, nSeat );
		PostEvent( event );
	}
	else
	{
		// 全都放弃了
		int nSeat = m_pMahjongObj->CurDiscardSeat();
		nSeat = m_pMahjongObj->GetNextPlayerSeat( nSeat );
		CEvent event( EVENT_ALLABORT, nSeat );
		PostEvent( event );
	}
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWaitState::OnPlayerGang( CEvent * pEvent )
{
	// 让玩家杠牌
	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	int nSeat = pRequestObj->GetAckMsgSeat( REQUEST_TYPE_GANG ) ;	// 这个说要杠牌的玩家
	PTKREQMAHJONGGANG pReqMahJongGang = ( PTKREQMAHJONGGANG )pRequestObj->GetPlayerAckMsg( nSeat ) ;
	if ( NULL == pReqMahJongGang )
	{
		// 出错了
		TKWriteLog( "%s : %d, 取玩家杠消息出错，消息为NULL", __FILE__, __LINE__ );
		// 全都放弃了
		int nSeat = m_pMahjongObj->CurDiscardSeat();
		nSeat = m_pMahjongObj->GetNextPlayerSeat( nSeat );
		CEvent event( EVENT_ALLABORT, nSeat );
		PostEvent( event );
		return false;
	}
	m_pMahjongObj->PlayerGang( pReqMahJongGang );
	int nScore = m_pMahjongObj->CalcGangScore(nSeat);

	// 记录杠牌
	DWORD dwDifMs = m_pMahjongGame->GetRtValue();
	CTKBuffer bufGameAction;
	bufGameAction.AppendFormatText("<a id=\"%d\" rt=\"%d\" rtms=\"%d\" s=\"%d\" gti=\"%d\" gt=\"%d\" gtl=\"%d,%d,%d,%d\" />",
			RECORD_GANG,
		    dwDifMs / 1000,
		    dwDifMs,
			pReqMahJongGang->nSeat,
			pReqMahJongGang->nTileID, 
			pReqMahJongGang->nGangType, 
			pReqMahJongGang->anGangTileID[0], 
			pReqMahJongGang->anGangTileID[1], 
			pReqMahJongGang->anGangTileID[2], 
			pReqMahJongGang->anGangTileID[3] );

	m_pMahjongGame->AddGameAction( bufGameAction.GetBufPtr() );

	int nLoseSeat = m_pMahjongObj->GetNextPlayerSeat(nSeat);
	if (m_pMahjongGame->IsIslandMatch() && m_pMahjongGame->IsEndGameEarly(nLoseSeat))
	{
		//TKWriteLog("杠牌积分输光 IsEndGameEarly");

		CTKMahjongPlayer * pPlayOutPlayer = m_pMahjongObj->GetPlayerBySeat(nLoseSeat);
		std::string strNotify(pPlayOutPlayer->NickName());
		strNotify.append(" 玩家积分已输光，本局结束");
		Notify(MAHJONG_NOTIFY_PLAYER_NOCHIP, strNotify.c_str());

		IState * curResultState = m_pFSMachine->SetState("CResultState");
		curResultState->OnEnter(NULL);
	}


/*
	// 允许杠牌，发一张牌给这个要杠牌的玩家，并请他出牌
	if( 0 != m_pMahjongObj->PlayerDrawOneTile( nSeat , DRAW_TILE_TYPE_GANG , FALSE , 0 ) )
	{ // 的确发牌了（没有黄庄）
		SendRequestDiscardTileMsg( nSeat ) ;	// 请杠牌的玩家出牌
	}
	else
	{
		// 黄庄了
		PostEvent( EVENT_NOTILE );

		// 不能再出牌了
		return false;
	}
*/

	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWaitState::OnPlayerChi( CEvent * pEvent )
{
	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	int nSeat = pRequestObj->GetAckMsgSeat( REQUEST_TYPE_CHI ) ;	// 这个说要吃牌的玩家
	PTKREQMAHJONGCHI pReqMahJongChi = ( PTKREQMAHJONGCHI )pRequestObj->GetPlayerAckMsg( nSeat ) ;
	if ( NULL == pReqMahJongChi )
	{
		// 出错了
		TKWriteLog( "%s : %d, 取玩家杠消息出错，消息为NULL", __FILE__, __LINE__ );
		// 全都放弃了
		int nSeat = m_pMahjongObj->CurDiscardSeat();
		nSeat = m_pMahjongObj->GetNextPlayerSeat( nSeat );
		CEvent event( EVENT_ALLABORT, nSeat );
		PostEvent( event );
		return false;
	}

	m_pMahjongObj->PlayerChi( pReqMahJongChi );

	// 记录吃牌
	DWORD dwDifMs = m_pMahjongGame->GetRtValue();
	CTKBuffer bufGameAction;
	bufGameAction.AppendFormatText("<a id=\"%d\" rt=\"%d\" rtms=\"%d\" s=\"%d\" cti=\"%d\" ctl=\"%d,%d,%d\" />",
			RECORD_CHI, 
		    dwDifMs / 1000,
		    dwDifMs,
			pReqMahJongChi->nSeat, 
			pReqMahJongChi->nTileID, 
			pReqMahJongChi->anChiTileID[ 0 ], 
			pReqMahJongChi->anChiTileID[ 1 ],
			pReqMahJongChi->anChiTileID[ 2 ]);

	m_pMahjongGame->AddGameAction( bufGameAction.GetBufPtr() );

	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWaitState::OnPlayerDouble( CEvent * pEvent )
{
	if ( !m_pMahjongGame->Rule()->GetRule(RULE_DOUBLE_COUNT) ) return false;

	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	int nSeat = pRequestObj->GetAckMsgSeat( REQUEST_TYPE_DOUBLE ) ;	// 这个说要吃牌的玩家
	PTKREQMAHJONGDBL pReqMahJongDbl = ( PTKREQMAHJONGDBL )pRequestObj->GetPlayerAckMsg( nSeat ) ;
	if ( NULL == pReqMahJongDbl )
	{
		// 出错了
		TKWriteLog( "%s : %d, 取玩家加倍消息出错，消息为NULL", __FILE__, __LINE__ );

		// 全都放弃了
		int nSeat = m_pMahjongObj->CurDiscardSeat();
		nSeat = m_pMahjongObj->GetNextPlayerSeat( nSeat );
		CEvent event( EVENT_ALLABORT, nSeat );
		PostEvent( event );
		return false;
	}

	if ( !pReqMahJongDbl->isDouble ) return false;
	int seat = pReqMahJongDbl->seat;

	// 次数累计
	m_pMahjongObj->AckDouble( seat );

	// 广播加倍消息
	TKACKDOUBLING ackDoubling	= { 0 };
	ackDoubling.header.dwType	= ACK_TYPE( TK_MSG_DOUBLING );
	ackDoubling.header.dwLength = MSG_LENGTH( TKACKDOUBLING );
	ackDoubling.seat			= seat;
	ackDoubling.count			= m_pMahjongObj->GetAllTingCount(seat);
	PTKREQMAHJONGWIN pReqMahJongWin = (PTKREQMAHJONGWIN)&pReqMahJongDbl->dblMsg;
	if ( NULL != pReqMahJongWin )
	{
		ackDoubling.tileId			= pReqMahJongWin->nTileID;
	}

	if ( !Broadcast( PTKHEADER(&ackDoubling) ) )
	{
		TKWriteLog( "%s : %d, Error! 发送加倍消息出错, len( %d )", __FILE__, __LINE__, ackDoubling.header.dwLength );
	}

	// 加倍提示处理
	m_pMahjongObj->SendDblTrip( seat );

	// 记录加倍消息
	char szGameActionBuf[ 512 ] = { 0 };
	sprintf_s( szGameActionBuf, "<action name=\"double\" time=\"%d\" seat=\"%d\" objects=\"%d\" />", time(NULL), seat, ackDoubling.count );
	//m_pMahjongGame->AddGameAction( szGameActionBuf ); //做录像标准化时同策划确定，这个功能已撤销，不需要记录了

	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWaitState::OnPlayerPeng( CEvent * pEvent )
{
	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	int nSeat = pRequestObj->GetAckMsgSeat( REQUEST_TYPE_PENG ) ;	// 这个说要碰牌的玩家
	PTKREQMAHJONGPENG pReqMahJongPeng = ( PTKREQMAHJONGPENG )pRequestObj->GetPlayerAckMsg( nSeat ) ;
	if ( NULL == pReqMahJongPeng )
	{
		// 出错了
		TKWriteLog( "%s : %d, 取玩家杠消息出错，消息为NULL", __FILE__, __LINE__ );
		// 全都放弃了
		int nSeat = m_pMahjongObj->CurDiscardSeat();
		nSeat = m_pMahjongObj->GetNextPlayerSeat( nSeat );
		CEvent event( EVENT_ALLABORT, nSeat );
		PostEvent( event );
		return false;
	}

	m_pMahjongObj->PlayerPeng( pReqMahJongPeng );

	// 记录碰牌
	DWORD dwDifMs = m_pMahjongGame->GetRtValue();
	CTKBuffer bufGameAction;
	bufGameAction.AppendFormatText("<a id=\"%d\" rt=\"%d\" 	rtms=\"%d\" s=\"%d\" pti=\"%d\" ptl=\"%d,%d,%d\" />", 
			RECORD_PENG, 
			dwDifMs / 1000,
			dwDifMs,
			pReqMahJongPeng->nSeat, 
			pReqMahJongPeng->nTileID, 
			pReqMahJongPeng->anPengTileID[ 0 ], 
			pReqMahJongPeng->anPengTileID[ 1 ],
			pReqMahJongPeng->anPengTileID[ 2 ] );
	m_pMahjongGame->AddGameAction( bufGameAction.GetBufPtr() );

	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWaitState::OnPlayerWin( CEvent * pEvent )
{
	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWaitState::OnAllAbort( CEvent * pEvent )
{
/*
	if( 0 != PlayerDrawOneTile( nNextSeat , DRAW_TILE_TYPE_NORMAL , TRUE , 0 ) )
	{ // 的确发牌了（没有黄庄）
		SendRequestDrawTileMsg( nNextSeat ) ;	// 请下一家出牌
	}
*/
	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWaitState::OnTrustPlay( CEvent * pEvent )
{
	int nLine = __LINE__;
	try
	{

	if ( NULL != pEvent->m_pParam )
	{
		nLine = __LINE__;
		PTKREQTRUSTPLAY pReqTrustPlay = ( PTKREQTRUSTPLAY )pEvent->m_pParam;
		nLine = __LINE__;
	}

	// 替他回复
	nLine = __LINE__;
	CTKMahjongServerPlayer * pPlayer = (CTKMahjongServerPlayer *)m_pMahjongObj->GetPlayerByID( pEvent->m_dwPoster );
	if ( NULL == pPlayer )
	{
		TKWriteLog( "%s : %d, Error! ( %d )托管了，但他不是本桌的玩家", __FILE__, __LINE__, pEvent->m_dwPoster );
		return false;
	}
	int nSeat = pPlayer->Seat();
	nLine = __LINE__;
	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	if ( pRequestObj->IsRequestSeat( nSeat ) && !pRequestObj->IsAckRequestSeat( nSeat ) )
	{
		if (pPlayer->HasCalled())
		{
			CMahJongTile* nTile = m_pMahjongObj->CurDiscardTile();
			if (nTile)
			{
				if (pPlayer->IsCallTile(nTile->ID()))
				{
					int nDiscardTileSeat = m_pMahjongObj->CurDiscardSeat();
					if( pPlayer->HasCalledPower( REQUEST_TYPE_WIN , nDiscardTileSeat ) )
					{
						AutoWin(nSeat, nTile);
					}
					else
					{
						pRequestObj->SetAckQuestSeat( nSeat , 0 ) ;	// 设置这个玩家回复了
					}
				}
				else
				{
					pRequestObj->SetAckQuestSeat( nSeat , 0 ) ;	// 设置这个玩家回复了
				}
			}
			else
			{
				pRequestObj->SetAckQuestSeat( nSeat , 0 ) ;	// 设置这个玩家回复了
			}
		}
		else
		{
			pRequestObj->SetAckQuestSeat( nSeat , 0 ) ;	// 设置这个玩家回复了
		}
	}
	nLine = __LINE__;

	// 检查回复的状态
	nLine = __LINE__;
	CheckAndDealAckRequest() ;
	nLine = __LINE__;

	return true;

	}
	catch (...)
	{
		TKWriteLog( "%s : %d, Exception", __FILE__, nLine );
	}

    return false;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWaitState::OnCheckTimeout( CEvent * pEvent )
{
	PTKREQCHECKTIMEOUT pReqCheckTimeout = ( PTKREQCHECKTIMEOUT )pEvent->m_pParam;
	CTKMahjongPlayer *pPlayer = m_pMahjongObj->GetPlayerByID(pReqCheckTimeout->m_dwUserID);
	if ((NULL != pPlayer) && (m_pMahjongGame->RequestObj()->CheckOverTime(pPlayer->Seat(), m_pMahjongGame->Rule()->GetRule( RULE_WAITTIME ))))
	{
		TKREQTRUSTPLAY reqTrustPlay = { 0 } ;
		reqTrustPlay.header.dwType   = REQ_TYPE( TK_MSG_TRUSTPLAY ) ;
		reqTrustPlay.header.dwLength = MSG_LENGTH( TKREQTRUSTPLAY ) ;
		reqTrustPlay.dwUserID = pPlayer->UserID();
		reqTrustPlay.dwType = TRUST_OVERTIME;
		m_pMahjongGame->OnPlayerMsg( pPlayer->UserID(), pPlayer->Seat(), ( PTKHEADER )&reqTrustPlay );
	}

	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CDrawTileState::CDrawTileState()
{
	// 注册感兴趣的事件
	REGIST_EVENT_FUNCTION( EVENT_DRAWSUCCEED, &CDrawTileState::OnDrawSucceed );
	REGIST_EVENT_FUNCTION( EVENT_NOTILE, &CDrawTileState::OnNoTile );
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CDrawTileState::OnEnter( CEvent * pEvent )
{
	// 请求玩家抓牌
	int nSeat = pEvent->m_dwPoster;
	ASSERT( nSeat >= 0 && nSeat <  MAX_PLAYER_COUNT );
	int nTileID = m_pMahjongObj->PlayerDrawOneTile( nSeat, DRAW_TILE_TYPE_NORMAL, true, 0 );
	if ( 0 == nTileID )
	{
		// 没牌了
		PostEvent( EVENT_NOTILE );
	}
	else
	{
		// 抓牌成功
		CEvent event( EVENT_DRAWSUCCEED, nSeat );
		PostEvent( event );

		// 记录抓牌
		DWORD dwDifMs = m_pMahjongGame->GetRtValue();
		CTKBuffer bufGameAction;
		bufGameAction.AppendFormatText("<a id=\"%d\" rt=\"%d\" rtms=\"%d\" s=\"%d\" dti=\"%d\" dtt=\"%d\" fwh=\"%d\" tos=\"%d\" />", 
			RECORD_DRAW, 
			dwDifMs / 1000,
			dwDifMs,
			nSeat, nTileID, 
			DRAW_TILE_TYPE_NORMAL, 1, 0 );

		m_pMahjongGame->AddGameAction( bufGameAction.GetBufPtr() );
	}
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDrawTileState::OnDrawSucceed( CEvent * pEvent )
{
	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDrawTileState::OnNoTile( CEvent * pEvent )
{
	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CDrawTileAfterGangState::CDrawTileAfterGangState()
{
	// 注册感兴趣的事件
	REGIST_EVENT_FUNCTION( EVENT_DRAWSUCCEED, &CDrawTileAfterGangState::OnDrawSucceed );
	REGIST_EVENT_FUNCTION( EVENT_NOTILE, &CDrawTileAfterGangState::OnNoTile );
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CDrawTileAfterGangState::OnEnter( CEvent * pEvent )
{
	// 请求玩家抓牌
	int nSeat = pEvent->m_dwPoster;
	ASSERT( nSeat >= 0 && nSeat <  MAX_PLAYER_COUNT );
	int nTileID = m_pMahjongObj->PlayerDrawOneTile( nSeat, DRAW_TILE_TYPE_GANG, false, 0 );
	if ( 0 == nTileID )
	{
		// 没牌了
		PostEvent( EVENT_NOTILE );
	}
	else
	{
		// 抓牌成功
		CEvent event( EVENT_DRAWSUCCEED, nSeat );
		PostEvent( event );
	
		// 记录抓牌
		DWORD dwDifMs = m_pMahjongGame->GetRtValue();
		CTKBuffer bufGameAction;
		bufGameAction.AppendFormatText("<a id=\"%d\" rt=\"%d\"	rtms=\"%d\" s=\"%d\" dti=\"%d\" dtt=\"%d\" fwh=\"%d\" tos=\"%d\" />", 
			RECORD_DRAW, 
			dwDifMs/1000,
			dwDifMs,
			nSeat, 
			nTileID, 
			DRAW_TILE_TYPE_GANG, 0, 0 );

		m_pMahjongGame->AddGameAction( bufGameAction.GetBufPtr() );
	}
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDrawTileAfterGangState::OnDrawSucceed( CEvent * pEvent )
{
	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDrawTileAfterGangState::OnNoTile( CEvent * pEvent )
{
	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CDrawTileAfterChangeFlowerState::CDrawTileAfterChangeFlowerState()
{
	// 注册感兴趣的事件
	REGIST_EVENT_FUNCTION( EVENT_DRAWSUCCEED, &CDrawTileAfterChangeFlowerState::OnDrawSucceed );
	REGIST_EVENT_FUNCTION( EVENT_NOTILE, &CDrawTileAfterChangeFlowerState::OnNoTile );
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CDrawTileAfterChangeFlowerState::OnEnter( CEvent * pEvent )
{
	// 请求玩家抓牌
	PTKREQMAHJONGCHANGEFLOWER pReqMahJongChangeFlower = ( PTKREQMAHJONGCHANGEFLOWER )pEvent->m_pParam;
	ASSERT( NULL != pReqMahJongChangeFlower );
	int nSeat = pReqMahJongChangeFlower->nSeat;

	ASSERT( nSeat >= 0 && nSeat <  MAX_PLAYER_COUNT );
	int nTileID = m_pMahjongObj->PlayerDrawOneTile( nSeat, DRAW_TILE_TYPE_CHANGE_FLOWER_2, true, 0 );
	if ( 0 == nTileID )
	{
		// 没牌了
		PostEvent( EVENT_NOTILE );
	}
	else
	{
		// 抓牌成功
		CEvent event( EVENT_DRAWSUCCEED, nSeat );
		PostEvent( event );

		// 记录抓牌
		DWORD dwDifMs = m_pMahjongGame->GetRtValue();
		CTKBuffer bufGameAction;
		bufGameAction.AppendFormatText("<a id=\"%d\" rt=\"%d\" rtms=\"%d\" s=\"%d\" dti=\"%d\" dtt=\"%d\" fwh=\"%d\" tos=\"%d\" />", 
			RECORD_DRAW, 
			dwDifMs / 1000,
			dwDifMs,
			nSeat, 
			nTileID, 
			DRAW_TILE_TYPE_CHANGE_FLOWER_2, 
			1, 0 );

		m_pMahjongGame->AddGameAction( bufGameAction.GetBufPtr() );
	}
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDrawTileAfterChangeFlowerState::OnDrawSucceed( CEvent * pEvent )
{
	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDrawTileAfterChangeFlowerState::OnNoTile( CEvent * pEvent )
{
	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CDiscardAfterChiPengState::CDiscardAfterChiPengState()
{
	// 注册感兴趣的事件
	REGIST_EVENT_FUNCTION( EVENT_DISCARD_TILE, &CDiscardAfterChiPengState::OnReqDiscardTile );
	REGIST_EVENT_FUNCTION( EVENT_CALL, &CDiscardAfterChiPengState::OnReqCall );
	REGIST_EVENT_FUNCTION( EVENT_TRUSTPLAY, &CDiscardAfterChiPengState::OnTrustPlay );
	REGIST_EVENT_FUNCTION( EVENT_CHECKTIMEOUT, &CDiscardAfterChiPengState::OnCheckTimeout );
	REGIST_EVENT_FUNCTION( EVENT_CALLTILE, &CDiscardAfterChiPengState::OnReqCallTile )
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CDiscardAfterChiPengState::OnEnter( CEvent * pEvent )
{
	// 请玩家出牌
	int nSeat = pEvent->m_dwPoster;
	ASSERT( nSeat >= 0 && nSeat < MAX_PLAYER_COUNT );

	SendRequestDiscardTileMsg( nSeat );
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CDiscardAfterChiPengState::SendRequestDiscardTileMsg( int nSeat )
{
	int nRequestType = REQUEST_TYPE_DISCARD_TILE | REQUEST_TYPE_CALL ;
	int nWaitSecond  = m_pMahjongGame->Rule()->GetRule( RULE_DISCARDTIME );

	CTKMahjongPlayer* pTKMahjongPlayer = m_pMahjongObj->GetPlayerBySeat(nSeat);
	if (pTKMahjongPlayer->GetDelayCount() >= 2)
	{
		//玩家连续2次出牌等待时长超过配置RULE_WAIT_DISCARDTIME时间
		nWaitSecond = m_pMahjongGame->Rule()->GetRule(RULE_WAIT_DISCARDTIME);
	}

	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	pRequestObj->ResetRequest( nRequestType , nWaitSecond ) ;

	// 先发送请求这个玩家出牌的消息
	TKREQMAHJONGREQUEST reqMahJongRequest = { 0 } ;
	pRequestObj->FillRequestMsg( &reqMahJongRequest , nSeat ) ;
	if ( !Broadcast( ( PTKHEADER )&reqMahJongRequest ) )
	{
		TKWriteLog( "%s : %d, Error! 发送消息出错, len( %d )", __FILE__, __LINE__, reqMahJongRequest.header.dwLength );
	}

	// 设置当前该这个玩家出牌了
	m_pMahjongObj->CurDiscardSeat( nSeat ) ;

	// 如果这个玩家托管了，那么还要帮他出牌
	CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerBySeat( nSeat );
	ASSERT( NULL != pPlayer );
	if( pPlayer->IsAutoPlay() )
	{ // 帮这个玩家游戏
		AutoDiscardTile( nSeat ) ;
	}
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CDiscardAfterChiPengState::AutoDiscardTile( int nSeat )
{
	int nLine = __LINE__;
	try
	{
	nLine = __LINE__;
	CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerBySeat( nSeat ) ;	// 这个座位号的玩家
	nLine = __LINE__;
	ASSERT( NULL != pPlayer );

	// 处理一个出牌消息
	nLine = __LINE__;
	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	PTKREQMAHJONGDISCARDTILE pReqMahJongDiscardTile = ( PTKREQMAHJONGDISCARDTILE )m_buffer;
	nLine = __LINE__;
	pReqMahJongDiscardTile->header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_DISCARD_TILE ) ;
	nLine = __LINE__;
	pReqMahJongDiscardTile->header.dwLength = MSG_LENGTH( TKREQMAHJONGDISCARDTILE ) ;
	nLine = __LINE__;
	pReqMahJongDiscardTile->nSeat           = nSeat ;
	nLine = __LINE__;
	pReqMahJongDiscardTile->nRequestID      = pRequestObj->RequestID() ;
	nLine = __LINE__;
	pReqMahJongDiscardTile->nTileID         = ( m_pMahjongGame->IsPlayerPlaying( nSeat ) && !pPlayer->IsBreak() ) ? 
		pPlayer->GetAutoPlayTile()->ID() : pPlayer->GetAutoPlayTileEx()->ID() ;
	nLine = __LINE__;
	pReqMahJongDiscardTile->bHide			= FALSE;
	nLine = __LINE__;
	CEvent event( REQ_TYPE( TK_MSG_MAHJONG_DISCARD_TILE ), pPlayer->UserID(), pReqMahJongDiscardTile );
	nLine = __LINE__;
	PostEvent( event );
	nLine = __LINE__;

	}
	catch (...)
	{
		TKWriteLog( "%s : %d, Exception", __FILE__, nLine );
	}
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardAfterChiPengState::OnReqDiscardTile( CEvent * pEvent )
{
	PTKREQMAHJONGDISCARDTILE pReqMahJongDiscardTile = ( PTKREQMAHJONGDISCARDTILE )pEvent->m_pParam;
	ASSERT( NULL != pReqMahJongDiscardTile );
	if ( pReqMahJongDiscardTile->header.dwLength != MSG_LENGTH( TKREQMAHJONGDISCARDTILE ) )
	{
		return false;
	}

	// 检查消息是否合法
	if( !CheckMsg( pEvent->m_dwPoster, pReqMahJongDiscardTile->nSeat , MSG_LENGTH( TKREQMAHJONGDISCARDTILE ), 
		( PTKHEADER ) pReqMahJongDiscardTile, pReqMahJongDiscardTile->nRequestID, __FILE__ , __LINE__ ) )
	{ 
		return false;
	}

	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	CTKMahjongPlayer* pTKMahjongPlayer = m_pMahjongObj->GetPlayerBySeat(pReqMahJongDiscardTile->nSeat);
	if (pRequestObj->CheckDelayDiscard(m_pMahjongGame->Rule()->GetRule(RULE_WAIT_DISCARDTIME )))
	{
		pTKMahjongPlayer->AutoAddDelayCount();
	}
	else
	{
		pTKMahjongPlayer->ResetDelayCount();
	}

	return OnReqDiscardTile( pReqMahJongDiscardTile );
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardAfterChiPengState::OnReqCall( CEvent * pEvent )
{
	PTKREQMAHJONGCALL pReqMahJongCall = ( PTKREQMAHJONGCALL )pEvent->m_pParam;
	ASSERT( NULL != pReqMahJongCall );
	if ( pReqMahJongCall->header.dwLength != MSG_LENGTH( TKREQMAHJONGCALL ) )
	{
		return false;
	}

	// 检查消息是否合法
	if( !CheckMsg( pEvent->m_dwPoster, pReqMahJongCall->nSeat , MSG_LENGTH( TKREQMAHJONGCALL ), 
		( PTKHEADER ) pReqMahJongCall, pReqMahJongCall->nRequestID, __FILE__ , __LINE__ ) )
	{ 
		return false;
	}

	return OnReqCall( pReqMahJongCall );
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardAfterChiPengState::OnReqCallTile( CEvent * pEvent )
{
	PTKREQMAHJONGCALLTILE pReqMahJongCallTile = ( PTKREQMAHJONGCALLTILE )pEvent->m_pParam;
	ASSERT( NULL != pReqMahJongCallTile );
	if ( pReqMahJongCallTile->header.dwLength != MSG_LENGTH( TKREQMAHJONGCALLTILE ) )
	{
		return false;
	}

	// 检查消息是否合法
	if( !CheckMsg( pEvent->m_dwPoster, pReqMahJongCallTile->nSeat , MSG_LENGTH( TKREQMAHJONGCALLTILE ), 
		( PTKHEADER ) pReqMahJongCallTile, pReqMahJongCallTile->nRequestID, __FILE__ , __LINE__ ) )
	{ 
		return false;
	}

	return OnReqCallTile( pReqMahJongCallTile );
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardAfterChiPengState::OnReqDiscardTile( PTKREQMAHJONGDISCARDTILE pReqMahJongDiscardTile )
{
	// 让obj去处理
	bool ret = m_pMahjongObj->PlayerReqDiscardTile( pReqMahJongDiscardTile );

	if ( ret )
	{
		// 记录打牌
		DWORD dwDifMs = m_pMahjongGame->GetRtValue();
		CTKBuffer bufGameAction;
		bufGameAction.AppendFormatText("<a id=\"%d\" rt=\"%d\" rtms=\"%d\" s=\"%d\" dti=\"%d\" />", 
			RECORD_DISCARD, 
			dwDifMs / 1000,
			dwDifMs,
			pReqMahJongDiscardTile->nSeat, 
			pReqMahJongDiscardTile->nTileID );

		m_pMahjongGame->AddGameAction( bufGameAction.GetBufPtr() );
	}

	return ret;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardAfterChiPengState::OnReqCall( PTKREQMAHJONGCALL pReqMahJongCall )
{
	// 让obj去处理
	bool ret = m_pMahjongObj->PlayerReqCallAndDiscardTile( pReqMahJongCall );

	if ( ret )
	{
		// 记录听牌
		DWORD dwDifMs = m_pMahjongGame->GetRtValue();
		CTKBuffer bufGameAction;
		bufGameAction.AppendFormatText("<a id=\"%d\" rt=\"%d\" rtms=\"%d\" s=\"%d\" tti=\"%d\" ag=\"%d\" ws=\"%d\" />", 
			RECORD_TING, 
			dwDifMs / 1000,
			dwDifMs,
			pReqMahJongCall->nSeat, 
			pReqMahJongCall->nTileID, 
			pReqMahJongCall->bAutoGang, 
			pReqMahJongCall->bWinSelf );

		m_pMahjongGame->AddGameAction( bufGameAction.GetBufPtr() );
	}

	return ret;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardAfterChiPengState::OnReqCallTile( PTKREQMAHJONGCALLTILE pReqMahjongCallTile )
{
    return true;

	if (m_pMahjongObj)
	{
		CTKMahjongServerPlayer * pPlayer = (CTKMahjongServerPlayer *)m_pMahjongObj->GetPlayerBySeat( pReqMahjongCallTile->nSeat ) ;
		if (pPlayer)
		{
			pPlayer->SetCallInfo(pReqMahjongCallTile->cnCallTileCount, pReqMahjongCallTile->nCallTileID);
			return true;
		}
	}

	return false;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardAfterChiPengState::OnTrustPlay( CEvent * pEvent )
{
	int nLine = __LINE__;
	try
	{
		nLine = __LINE__;
		if ( NULL != pEvent->m_pParam )
		{
			nLine = __LINE__;
			PTKREQTRUSTPLAY pReqTrustPlay = ( PTKREQTRUSTPLAY )pEvent->m_pParam;
			nLine = __LINE__;
		}

		// 替他出牌
		nLine = __LINE__;
		CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerByID( pEvent->m_dwPoster );
		if ( NULL == pPlayer )
		{
			TKWriteLog( "%s : %d, Error! ( %d )托管了，但他不是本桌的玩家", __FILE__, __LINE__, pEvent->m_dwPoster );
			return false;
		}
		int nSeat = pPlayer->Seat();
		nLine = __LINE__;
		if ( m_pMahjongObj->CurDiscardSeat() == nSeat )
		{
			nLine = __LINE__;
			AutoDiscardTile( nSeat );
			nLine = __LINE__;
		}

		return true;
	}
	catch (...)
	{
		TKWriteLog( "" , __FILE__, nLine );
	}
	return false;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardAfterChiPengState::OnCheckTimeout( CEvent * pEvent )
{
	PTKREQCHECKTIMEOUT pReqCheckTimeout = ( PTKREQCHECKTIMEOUT )pEvent->m_pParam;
	CTKMahjongPlayer *pPlayer = m_pMahjongObj->GetPlayerByID(pReqCheckTimeout->m_dwUserID);
	if ((NULL != pPlayer) && (m_pMahjongGame->RequestObj()->CheckOverTime(pPlayer->Seat(), m_pMahjongGame->Rule()->GetRule( RULE_DISCARDTIME ))))
	{
		TKREQTRUSTPLAY reqTrustPlay = { 0 } ;
		reqTrustPlay.header.dwType   = REQ_TYPE( TK_MSG_TRUSTPLAY ) ;
		reqTrustPlay.header.dwLength = MSG_LENGTH( TKREQTRUSTPLAY ) ;
		reqTrustPlay.dwUserID = pPlayer->UserID();
		reqTrustPlay.dwType = TRUST_OVERTIME;
		m_pMahjongGame->OnPlayerMsg( pPlayer->UserID(), pPlayer->Seat(), ( PTKHEADER )&reqTrustPlay );
	}

	return true;
}





// ************************************************************************************************************************
// 
//  吃后杠，碰后杠
// 
// ************************************************************************************************************************
CDiscardGangAfterChiPengState::CDiscardGangAfterChiPengState()
{
	// 注册感兴趣的事件
	REGIST_EVENT_FUNCTION( EVENT_DISCARD_TILE, &CDiscardGangAfterChiPengState::OnReqDiscardTile );
	REGIST_EVENT_FUNCTION( EVENT_CALL, &CDiscardGangAfterChiPengState::OnReqCall );
	REGIST_EVENT_FUNCTION( EVENT_TRUSTPLAY, &CDiscardGangAfterChiPengState::OnTrustPlay );
	REGIST_EVENT_FUNCTION( EVENT_CHECKTIMEOUT, &CDiscardGangAfterChiPengState::OnCheckTimeout );
	REGIST_EVENT_FUNCTION( EVENT_CALLTILE, &CDiscardGangAfterChiPengState::OnReqCallTile );
	REGIST_EVENT_FUNCTION( EVENT_REQ_GANG, &CDiscardGangAfterChiPengState::OnReqGang);
	REGIST_EVENT_FUNCTION( EVENT_BUGANG, &CDiscardGangAfterChiPengState::OnBuGang );
	REGIST_EVENT_FUNCTION( EVENT_GANG, &CDiscardGangAfterChiPengState::OnPlayerGang);
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CDiscardGangAfterChiPengState::OnEnter( CEvent * pEvent )
{
	// 请玩家出牌
	int nSeat = pEvent->m_dwPoster;
	ASSERT( nSeat >= 0 && nSeat < MAX_PLAYER_COUNT );

	SendRequestDiscardTileMsg( nSeat );
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CDiscardGangAfterChiPengState::SendRequestDiscardTileMsg( int nSeat )
{
	int nRequestType = REQUEST_TYPE_DISCARD_TILE | REQUEST_TYPE_CALL | REQUEST_TYPE_GANG ;
	int nWaitSecond  = m_pMahjongGame->Rule()->GetRule( RULE_DISCARDTIME );

	CTKMahjongPlayer* pTKMahjongPlayer = m_pMahjongObj->GetPlayerBySeat(nSeat);
	if (pTKMahjongPlayer->GetDelayCount() >= 2)
	{
		//玩家连续2次出牌等待时长超过配置RULE_WAIT_DISCARDTIME时间
		nWaitSecond = m_pMahjongGame->Rule()->GetRule(RULE_WAIT_DISCARDTIME);
	}

	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	pRequestObj->ResetRequest( nRequestType , nWaitSecond ) ;

	// 先发送请求这个玩家出牌的消息
	TKREQMAHJONGREQUEST reqMahJongRequest = { 0 } ;
	pRequestObj->FillRequestMsg( &reqMahJongRequest , nSeat ) ;
	if ( !Broadcast( ( PTKHEADER )&reqMahJongRequest ) )
	{
		TKWriteLog( "%s : %d, Error! 发送消息出错, len( %d )", __FILE__, __LINE__, reqMahJongRequest.header.dwLength );
	}

	// 设置当前该这个玩家出牌了
	m_pMahjongObj->CurDiscardSeat( nSeat ) ;

	// 如果这个玩家托管了，那么还要帮他出牌
	CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerBySeat( nSeat );
	ASSERT( NULL != pPlayer );
	if( pPlayer->IsAutoPlay() )
	{ // 帮这个玩家游戏
		AutoDiscardTile( nSeat ) ;
	}
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CDiscardGangAfterChiPengState::AutoDiscardTile( int nSeat )
{
	int nLine = __LINE__;
	try
	{
		nLine = __LINE__;
		CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerBySeat( nSeat ) ;	// 这个座位号的玩家
		nLine = __LINE__;
		ASSERT( NULL != pPlayer );

		// 处理一个出牌消息
		nLine = __LINE__;
		CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
		PTKREQMAHJONGDISCARDTILE pReqMahJongDiscardTile = ( PTKREQMAHJONGDISCARDTILE )m_buffer;
		nLine = __LINE__;
		pReqMahJongDiscardTile->header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_DISCARD_TILE ) ;
		nLine = __LINE__;
		pReqMahJongDiscardTile->header.dwLength = MSG_LENGTH( TKREQMAHJONGDISCARDTILE ) ;
		nLine = __LINE__;
		pReqMahJongDiscardTile->nSeat           = nSeat ;
		nLine = __LINE__;
		pReqMahJongDiscardTile->nRequestID      = pRequestObj->RequestID() ;
		nLine = __LINE__;
		pReqMahJongDiscardTile->nTileID         = ( m_pMahjongGame->IsPlayerPlaying( nSeat ) && !pPlayer->IsBreak() ) ? 
			pPlayer->GetAutoPlayTile()->ID() : pPlayer->GetAutoPlayTileEx()->ID() ;
		nLine = __LINE__;
		pReqMahJongDiscardTile->bHide			= FALSE;
		nLine = __LINE__;
		CEvent event( REQ_TYPE( TK_MSG_MAHJONG_DISCARD_TILE ), pPlayer->UserID(), pReqMahJongDiscardTile );
		nLine = __LINE__;
		PostEvent( event );
		nLine = __LINE__;

	}
	catch (...)
	{
		TKWriteLog( "%s : %d, Exception", __FILE__, nLine );
	}
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardGangAfterChiPengState::OnReqDiscardTile( CEvent * pEvent )
{
	PTKREQMAHJONGDISCARDTILE pReqMahJongDiscardTile = ( PTKREQMAHJONGDISCARDTILE )pEvent->m_pParam;
	ASSERT( NULL != pReqMahJongDiscardTile );
	if ( pReqMahJongDiscardTile->header.dwLength != MSG_LENGTH( TKREQMAHJONGDISCARDTILE ) )
	{
		return false;
	}

	// 检查消息是否合法
	if( !CheckMsg( pEvent->m_dwPoster, pReqMahJongDiscardTile->nSeat , MSG_LENGTH( TKREQMAHJONGDISCARDTILE ), 
		( PTKHEADER ) pReqMahJongDiscardTile, pReqMahJongDiscardTile->nRequestID, __FILE__ , __LINE__ ) )
	{ 
		return false;
	}

	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	CTKMahjongPlayer* pTKMahjongPlayer = m_pMahjongObj->GetPlayerBySeat(pReqMahJongDiscardTile->nSeat);
	if (pRequestObj->CheckDelayDiscard(m_pMahjongGame->Rule()->GetRule(RULE_WAIT_DISCARDTIME)))
	{
		pTKMahjongPlayer->AutoAddDelayCount();
	}
	else
	{
		pTKMahjongPlayer->ResetDelayCount();
	}

	return OnReqDiscardTile( pReqMahJongDiscardTile );
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardGangAfterChiPengState::OnReqCall( CEvent * pEvent )
{
	PTKREQMAHJONGCALL pReqMahJongCall = ( PTKREQMAHJONGCALL )pEvent->m_pParam;
	ASSERT( NULL != pReqMahJongCall );
	if ( pReqMahJongCall->header.dwLength != MSG_LENGTH( TKREQMAHJONGCALL ) )
	{
		return false;
	}

	// 检查消息是否合法
	if( !CheckMsg( pEvent->m_dwPoster, pReqMahJongCall->nSeat , MSG_LENGTH( TKREQMAHJONGCALL ), 
		( PTKHEADER ) pReqMahJongCall, pReqMahJongCall->nRequestID, __FILE__ , __LINE__ ) )
	{ 
		return false;
	}

	return OnReqCall( pReqMahJongCall );
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardGangAfterChiPengState::OnReqCallTile( CEvent * pEvent )
{
	PTKREQMAHJONGCALLTILE pReqMahJongCallTile = ( PTKREQMAHJONGCALLTILE )pEvent->m_pParam;
	ASSERT( NULL != pReqMahJongCallTile );
	if ( pReqMahJongCallTile->header.dwLength != MSG_LENGTH( TKREQMAHJONGCALLTILE ) )
	{
		return false;
	}

	// 检查消息是否合法
	if( !CheckMsg( pEvent->m_dwPoster, pReqMahJongCallTile->nSeat , MSG_LENGTH( TKREQMAHJONGCALLTILE ), 
		( PTKHEADER ) pReqMahJongCallTile, pReqMahJongCallTile->nRequestID, __FILE__ , __LINE__ ) )
	{ 
		return false;
	}

	return OnReqCallTile( pReqMahJongCallTile );
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardGangAfterChiPengState::OnReqDiscardTile( PTKREQMAHJONGDISCARDTILE pReqMahJongDiscardTile )
{
	// 让obj去处理
	bool ret = m_pMahjongObj->PlayerReqDiscardTile( pReqMahJongDiscardTile );

	if ( ret )
	{
		// 记录打牌
		DWORD dwDifMs = m_pMahjongGame->GetRtValue();
		CTKBuffer bufGameAction;
		bufGameAction.AppendFormatText("<a id=\"%d\" rt=\"%d\" rtms=\"%d\" s=\"%d\" dti=\"%d\" />", 
			RECORD_DISCARD, 
			dwDifMs / 1000,
			dwDifMs,
			pReqMahJongDiscardTile->nSeat, 
			pReqMahJongDiscardTile->nTileID );

		m_pMahjongGame->AddGameAction( bufGameAction.GetBufPtr() );
	}

    if (ret)
    {
        m_pMahjongObj->m_vecPengCardsIDJust.clear();
    }

	return ret;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardGangAfterChiPengState::OnReqCall( PTKREQMAHJONGCALL pReqMahJongCall )
{
	// 让obj去处理
	bool ret = m_pMahjongObj->PlayerReqCallAndDiscardTile( pReqMahJongCall );

	if ( ret )
	{
		// 记录听牌
		DWORD dwDifMs = m_pMahjongGame->GetRtValue();
		CTKBuffer bufGameAction;
		bufGameAction.AppendFormatText("<a id=\"%d\" rt=\"%d\" rtms=\"%d\" s=\"%d\" tti=\"%d\" ag=\"%d\" ws=\"%d\" />", 
			RECORD_TING, 
			dwDifMs / 1000,
			dwDifMs,
			pReqMahJongCall->nSeat, 
			pReqMahJongCall->nTileID, 
			pReqMahJongCall->bAutoGang, 
			pReqMahJongCall->bWinSelf);

		m_pMahjongGame->AddGameAction( bufGameAction.GetBufPtr() );
	}

    if (ret)
    {
        m_pMahjongObj->m_vecPengCardsIDJust.clear();
    }

	return ret;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardGangAfterChiPengState::OnReqCallTile( PTKREQMAHJONGCALLTILE pReqMahjongCallTile )
{
    return true;

	if (m_pMahjongObj)
	{
		CTKMahjongServerPlayer * pPlayer = (CTKMahjongServerPlayer *)m_pMahjongObj->GetPlayerBySeat( pReqMahjongCallTile->nSeat ) ;
		if (pPlayer)
		{
			pPlayer->SetCallInfo(pReqMahjongCallTile->cnCallTileCount, pReqMahjongCallTile->nCallTileID);
			return true;
		}
	}

	return false;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardGangAfterChiPengState::OnTrustPlay( CEvent * pEvent )
{
	int nLine = __LINE__;
	try
	{
		nLine = __LINE__;
		if ( NULL != pEvent->m_pParam )
		{
			nLine = __LINE__;
			PTKREQTRUSTPLAY pReqTrustPlay = ( PTKREQTRUSTPLAY )pEvent->m_pParam;
			nLine = __LINE__;
		}

		// 替他出牌
		nLine = __LINE__;
		CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerByID( pEvent->m_dwPoster );
		if ( NULL == pPlayer )
		{
			TKWriteLog( "%s : %d, Error! ( %d )托管了，但他不是本桌的玩家", __FILE__, __LINE__, pEvent->m_dwPoster );
			return false;
		}
		int nSeat = pPlayer->Seat();
		nLine = __LINE__;
		if ( m_pMahjongObj->CurDiscardSeat() == nSeat )
		{
			nLine = __LINE__;
			AutoDiscardTile( nSeat );
			nLine = __LINE__;
		}

		return true;
	}
	catch (...)
	{
		TKWriteLog( "" , __FILE__, nLine );
	}
	return false;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CDiscardGangAfterChiPengState::OnCheckTimeout( CEvent * pEvent )
{
	PTKREQCHECKTIMEOUT pReqCheckTimeout = ( PTKREQCHECKTIMEOUT )pEvent->m_pParam;
	CTKMahjongPlayer *pPlayer = m_pMahjongObj->GetPlayerByID(pReqCheckTimeout->m_dwUserID);
	if ((NULL != pPlayer) && (m_pMahjongGame->RequestObj()->CheckOverTime(pPlayer->Seat(), m_pMahjongGame->Rule()->GetRule( RULE_DISCARDTIME ))))
	{
		TKREQTRUSTPLAY reqTrustPlay = { 0 } ;
		reqTrustPlay.header.dwType   = REQ_TYPE( TK_MSG_TRUSTPLAY ) ;
		reqTrustPlay.header.dwLength = MSG_LENGTH( TKREQTRUSTPLAY ) ;
		reqTrustPlay.dwUserID = pPlayer->UserID();
		reqTrustPlay.dwType = TRUST_OVERTIME;
		m_pMahjongGame->OnPlayerMsg( pPlayer->UserID(), pPlayer->Seat(), ( PTKHEADER )&reqTrustPlay );
	}

	return true;
}


bool CDiscardGangAfterChiPengState::OnReqGang( CEvent * pEvent )
{
	PTKREQMAHJONGGANG pReqMahJongGang = ( PTKREQMAHJONGGANG )pEvent->m_pParam;
	ASSERT( NULL != pReqMahJongGang );

	// 检查消息是否合法
	if( !CheckMsg( pEvent->m_dwPoster, pReqMahJongGang->nSeat , MSG_LENGTH( TKREQMAHJONGGANG ), 
		( PTKHEADER ) pReqMahJongGang, pReqMahJongGang->nRequestID, __FILE__ , __LINE__ ) )
	{ 
		return false;
	}

    // 校验
    if (IS_GANG_TYPE_DIRECT(pReqMahJongGang->nGangType) &&
        IS_GANG_TYPE_PUBLIC(pReqMahJongGang->nGangType))
    {// 碰后直杠，必须是刚刚碰过的牌张值
        if (m_pMahjongObj->m_vecPengCardsIDJust.empty())
        {
            return false;
        }

        int nColorReq = TILE_COLOR(pReqMahJongGang->nTileID);  // 花色
        int nWhatReq = TILE_WHAT(pReqMahJongGang->nTileID);    // 点数
        for (int i=0; i<m_pMahjongObj->m_vecPengCardsIDJust.size(); ++i)
        {
            if (SAME_TILE_ID(pReqMahJongGang->nTileID, m_pMahjongObj->m_vecPengCardsIDJust[i]))
            {
                return false;   // 请求牌张在刚碰的刻子里
            }

            int nColor = TILE_COLOR(m_pMahjongObj->m_vecPengCardsIDJust[i]);    // 花色
            int nWhat = TILE_WHAT(m_pMahjongObj->m_vecPengCardsIDJust[i]);      // 点数

            if (nColorReq != nColor ||
                nWhatReq != nWhat)
            {
                return false;   // 牌值不等
            }
        }
    }
    else if (IS_GANG_TYPE_HIDDEN(pReqMahJongGang->nGangType) &&
        IS_GANG_TYPE_DIRECT(pReqMahJongGang->nGangType))
    {// 暗杠（直杠），不能有刻子
        CTKMahjongPlayer* pPlayer = m_pMahjongObj->GetPlayerBySeat(pReqMahJongGang->nSeat);
        if (pPlayer == NULL)
        {
            assert(false);
            return false;
        }

        int nColorReq = TILE_COLOR(pReqMahJongGang->nTileID);  // 花色
        int nWhatReq = TILE_WHAT(pReqMahJongGang->nTileID);    // 点数
        if (pPlayer->HasPeng(nColorReq, nWhatReq))
        {
            return false;
        }
    }
    else if (IS_GANG_TYPE_PATCH(pReqMahJongGang->nGangType) &&
        IS_GANG_TYPE_PUBLIC(pReqMahJongGang->nGangType))   // 碰后补杠，不能是刚碰到的牌张
    {
        int nColorReq = TILE_COLOR(pReqMahJongGang->nTileID);  // 花色
        int nWhatReq = TILE_WHAT(pReqMahJongGang->nTileID);    // 点数

        if (!m_pMahjongObj->m_vecPengCardsIDJust.empty())
        {
            int nColor = TILE_COLOR(m_pMahjongObj->m_vecPengCardsIDJust[0]);    // 花色
            int nWhat = TILE_WHAT(m_pMahjongObj->m_vecPengCardsIDJust[0]);      // 点数

            if (nColorReq == nColor &&
                nWhatReq == nWhat)
            {
                return false;
            }
        }
    }
    else
    {
        ASSERT(false);
        return false;
    }

	return OnReqGang( pReqMahJongGang );
}

bool CDiscardGangAfterChiPengState::OnReqGang( PTKREQMAHJONGGANG pReqMahJongGang )
{
	if ( !m_pMahjongObj->PlayerReqGang( pReqMahJongGang, true, true ) )
	{
		// 不能杠牌
		return false;
	}

	// 让玩家杠牌
	m_pMahjongObj->PlayerGang( pReqMahJongGang, true );

	int nSeat = pReqMahJongGang->nSeat;

	// 如果是补杠，那么还要问其他玩家是否要抢杠
	if( IS_GANG_TYPE_PATCH( pReqMahJongGang->nGangType ) )
	{ // 补杠
		// 发个消息,有人补杠了
		CEvent event( EVENT_BUGANG, nSeat );
		PostEvent( event );
	}
	else
	{ // 不是补杠（暗杠或者明杠的直杠）
		CEvent event( EVENT_GANG, nSeat );
		PostEvent( event );
	}

	int nGangType = pReqMahJongGang->nGangType;
	// 记录杠牌
	DWORD dwDifMs = m_pMahjongGame->GetRtValue();
	CTKBuffer bufGameAction;
	bufGameAction.AppendFormatText("<a id=\"%d\" rt=\"%d\" rtms=\"%d\" s=\"%d\" gti=\"%d\" gt=\"%d\" gtl=\"%d,%d,%d,%d\" />", 
		RECORD_GANG, 
		dwDifMs / 1000,
		dwDifMs,
		nSeat, 
		pReqMahJongGang->nTileID, 
		nGangType, 
		pReqMahJongGang->anGangTileID[ 0 ], 
		pReqMahJongGang->anGangTileID[ 1 ], 
		pReqMahJongGang->anGangTileID[ 2 ], 
		pReqMahJongGang->anGangTileID[ 3 ] );

	m_pMahjongGame->AddGameAction( bufGameAction.GetBufPtr() );

	return true;
}

bool CDiscardGangAfterChiPengState::OnPlayerGang( CEvent * pEvent )
{
    m_pMahjongObj->m_vecPengCardsIDJust.clear();

	return true;
}

bool CDiscardGangAfterChiPengState::OnBuGang( CEvent * pEvent )
{
    m_pMahjongObj->m_vecPengCardsIDJust.clear();

	return true;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CWinState::CWinState()
{
	// 注册感兴趣的事件
	//REGIST_EVENT_FUNCTION( EVENT_1PLAYER_NOT_WIN, &CWinState::On1PlayerNotWin );
	//REGIST_EVENT_FUNCTION( EVENT_2PLAYER_NOT_WIN, &CWinState::On2PlayerNotWin );
	//REGIST_EVENT_FUNCTION( EVENT_3PLAYER_NOT_WIN, &CWinState::On3PlayerNotWin );
	REGIST_EVENT_FUNCTION( EVENT_LUCKY_TILE, &CWinState::OnLuckyTile );

	m_cnWinner = 0;
	memset(m_szBuffer, 0, sizeof(m_szBuffer));
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CWinState::OnEnter( CEvent * pEvent )
{
	// 找出应和牌的玩家
	PTKREQMAHJONGWIN apReqMahJongWin[ MAX_PLAYER_COUNT - 1 ] = { 0 };
	int cnReqWin = 0;
	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	if ( EVENT_WIN_SELF != pEvent->ID() )
	{
		// 不是自摸,可能有多个人和
		// 先找出点炮的玩家
		int nPaoSeat = -1;
		if ( EVENT_WIN_PAO == pEvent->ID() )
		{
			// 如果是点炮和的,点炮的玩家就是出牌玩家
			nPaoSeat = m_pMahjongObj->CurDiscardSeat();
		}
		else if ( EVENT_WINGANG == pEvent->ID() )
		{
			// 抢杠和的,点炮的玩家就是杠牌玩家
			nPaoSeat = m_pMahjongObj->LastGangSeat();
		}

		int nSeat = nPaoSeat;
        m_cnWinner++;
        for (int i = 1; i < m_pMahjongObj->PlayerCount(); i++)
        {
            nSeat = m_pMahjongObj->GetNextPlayerSeat(nSeat, false);	// 取下一家的座位号
            if (pRequestObj->IsRequestSeat(nSeat) && pRequestObj->IsPlayerAckRequestType(nSeat, REQUEST_TYPE_WIN))
            { // 请求了这个玩家，并且这个玩家说和牌了
                apReqMahJongWin[cnReqWin] = (PTKREQMAHJONGWIN)pRequestObj->GetPlayerAckMsg(nSeat);
                cnReqWin++;
                break;
            }
        }
	}
	else
	{
		// 是自摸的,只可能有一个人和
		int nWinSeat = pRequestObj->GetAckMsgSeat( REQUEST_TYPE_WIN );
		m_cnWinner++;
		apReqMahJongWin[ cnReqWin ] = ( PTKREQMAHJONGWIN ) pRequestObj->GetPlayerAckMsg( nWinSeat );
		cnReqWin++;
	}

	Win( apReqMahJongWin, cnReqWin, EVENT_WIN_SELF == pEvent->ID() );
	// 计算一下还有几个人未和牌
	int cnNotWin = m_pMahjongObj->PlayerCount() - m_cnWinner;
	if ( cnNotWin < 1 )
	{
		ASSERT( false );
		TKWriteLog( "所有人都和牌了???傻了吧~状态配置文件写错了~" );
		return;
	}

	// 把未胡牌玩家数量和下一个座位号发送出去
	memset(m_szBuffer, 0, sizeof(m_szBuffer));
	int* pBuffer = (int*)m_szBuffer;
	*pBuffer = cnNotWin;
	pBuffer++;
	*pBuffer = 0;

	//CEvent event( EVENT_1PLAYER_NOT_WIN + cnNotWin - 1, nNextSeat );
	// 为客户端处理动画 延迟进入下一状态的时间
	if (m_pMahjongGame != NULL)
	{
		m_pMahjongGame->SetGameTimerMilli(EVENT_LUCKY_TILE, (ULONG_PTR)m_szBuffer, 0);
	}
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWinState::On1PlayerNotWin( CEvent * pEvent )
{
	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWinState::On2PlayerNotWin( CEvent * pEvent )
{
	return true;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWinState::On3PlayerNotWin( CEvent * pEvent )
{
	return true;
}

// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWinState::OnLuckyTile( CEvent * pEvent )
{
	return true;
}

// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CWinState::Win( PTKREQMAHJONGWIN apReqMahJongWin[], int cnReqWin, bool bWinSelf )
{
//	if ( 1 != cnReqWin )
//	{
//		TKWriteLog( "%s : %d, 无效的参数cnReqWin = %d", __FILE__, __LINE__, cnReqWin );
//		return false;
//	}

	// 让和牌的玩家先做一些和牌处理
	TKACKMAHJONGWIN ackWin = { 0 };
	ackWin.header.dwType = ACK_TYPE( TK_MSG_MAHJONG_WIN );
	ackWin.header.dwLength = MSG_LENGTH( TKACKMAHJONGWIN );
	ackWin.nPaoSeat = apReqMahJongWin[ 0 ]->nPaoSeat;
	if ( bWinSelf )
	{
		ackWin.nPaoSeat = -1;
	}
	ackWin.nWinTile = apReqMahJongWin[ 0 ]->nTileID;
	for ( int i = 0; i < cnReqWin; ++i )
	{
		m_pMahjongObj->PlayerWin( apReqMahJongWin[ i ] );
		int winSeat = apReqMahJongWin[ i ]->nSeat;
		ackWin.anWinSeat[ ackWin.cnWinSeat ] = winSeat;
		ackWin.cnWinSeat++;
		int winDbl = 0; // 加倍次数
		if ( m_pMahjongGame->Rule()->GetRule( RULE_DOUBLE_COUNT ) )
		{
			winDbl= m_pMahjongObj->GetAllTingCount( winSeat );
			ackWin.cnDouble[ ackWin.cnWinSeat ] = winDbl;
		}

		CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerBySeat( winSeat );
		WININFO & sWinInfo = pPlayer->WinInfo();
		ackWin.anWinMode[ i ] = sWinInfo.nWinMode;
	}

	// 广播和牌消息给所有玩家
	if ( !Broadcast( ( PTKHEADER )&ackWin ) )
	{
		TKWriteLog( "%s : %d, Error! 发送消息出错, len( %d )", __FILE__, __LINE__, ackWin.header.dwLength );
	}

	// 记录和牌
	DWORD dwDifMs = m_pMahjongGame->GetRtValue();
	CTKBuffer bufGameAction;
	bufGameAction.AppendFormatText("<a id=\"%d\" rt=\"%d\" rtms=\"%d\" wti=\"%d\" ps=\"%d\" wc=\"%d\"", 
		RECORD_WIN, 
		dwDifMs / 1000,
		dwDifMs,
		ackWin.nWinTile, 
		ackWin.nPaoSeat, 
		cnReqWin);

	// 构造和牌详细信息给和牌者
	TKACKMAHJONGWINDETAILEX ackDetail = { 0 };
	ackDetail.header.dwType = ACK_TYPE( TK_MSG_MAHJONG_WINDETAILEX );
	ackDetail.header.dwLength = MSG_LENGTH( TKACKMAHJONGWINDETAILEX );
	ackDetail.nTime = 20;
	for ( int i = 0; i < cnReqWin; ++i )
	{
		int nSeat = apReqMahJongWin[ i ]->nSeat;
		int cnDbl = m_pMahjongObj->GetAllTingCount( nSeat );
		ackDetail.nWinSeat = nSeat;
		ackDetail.winDouble = cnDbl;
		ackDetail.nPaoSeat = apReqMahJongWin[ i ]->nPaoSeat;
		ackDetail.nWinTile = apReqMahJongWin[ i ]->nTileID;
		memset( ackDetail.asScoreDetail, 0, sizeof( ackDetail.asScoreDetail ) );
		CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerBySeat( nSeat );
		WININFO & sWinInfo = pPlayer->WinInfo();
		memcpy( &( ackDetail.sWinInfo ), &sWinInfo, sizeof( WININFO ) );

		// 给赢的玩家加分,输的玩家减分
		IScoreCalculator * pCalculator = m_pMahjongGame->Calculator();
		// 根据加倍调整玩家最大番数
		if ( m_pMahjongGame->Rule()->GetRule( RULE_DOUBLE_COUNT ) )
		{
			pCalculator->SetDouble( cnDbl );
		}
		// 喂过的人
		int cnShowSeat = 0;
		int *pnShowSeat = pPlayer->GetShowSeat( cnShowSeat );
		pCalculator->AddScore( ackDetail, pnShowSeat, cnShowSeat );


		// 最终各玩家实际得分传给Calculator，Calculator要计算各玩家剩下分数
		for ( int j = 0; j < MAX_PLAYER_COUNT; ++j )
		{
			if ( !m_pMahjongObj->HasPlayer( j ) )
			{
				continue;
			}
			// 不更新calculater的成员变量 避免后续算重复
			ackDetail.asScoreDetail[ j ].nLeftScore = pCalculator->GetPlayerLeftScoer(j) + ackDetail.asScoreDetail[ j ].anScore[ 6 ];
		}

		// 给和牌的玩家发完整的消息
		if (m_pMahjongGame)
		{
			if (m_pMahjongGame->IsHaveplayerSeat(nSeat))
			{
				if ( !Send2SeatPlayer( nSeat, ( PTKHEADER )&ackDetail ) )
				{
					TKWriteLog( "%s : %d, Error! 发送消息错误 len( %d )", __FILE__, __LINE__, ackDetail.header.dwLength );
				}
			}
		}

		// 把这个消息保存下来,一局结束后还要发给所有玩家
		m_pMahjongObj->AddWinInfo( ackDetail );
 
		// 记下和牌者座位号和和牌方式
		bufGameAction.AppendFormatText(" wi%d=\"%d,%d,%d", i, nSeat, sWinInfo.nWinMode, ackDetail.nBaseScore);

		// 每把和牌得分明细
		for ( int k = 0; k < MAX_PLAYER_COUNT; ++k )
		{
			for ( int m = 0; m < 8; ++m )
			{
				bufGameAction.AppendFormatText(",%d", ackDetail.asScoreDetail[ k ].anScore[ m ]);
			}
			bufGameAction.AppendFormatText(",%d", ackDetail.asScoreDetail[ k ].nLeftScore);
		}
		bufGameAction.AppendFormatText("\"");
		bufGameAction.AppendFormatText(" nwfi%d=\"%d", i, ackDetail.sWinInfo.nMaxFans);

		for(int j=0; j < MAXFANS; j++)
		{
			if(ackDetail.sWinInfo.anFans[j] > 0)
			{
				bufGameAction.AppendFormatText(",%d,%d", 9020 + j, ackDetail.sWinInfo.anFans[j]);
			}
		}
		
		// 加番牌番数判断
		CHECKPARAM sCheckParam = { 0 } ;
		pPlayer->FillCheckParam( &sCheckParam ) ;
		// 如果不是自摸的，还要将别人打的牌或杠的牌传进去
		if(ackDetail.nWinSeat == ackDetail.nPaoSeat)
		{ // 不是自摸
			int nID = ackDetail.nWinTile ;	// 和的这张牌的ID号
			sCheckParam.asHandStone[ 0 ].nID    = nID ;
			sCheckParam.asHandStone[ 0 ].nColor = CMahJongTile::TileColor( nID ) ;
			sCheckParam.asHandStone[ 0 ].nWhat  = CMahJongTile::TileWhat( nID ) ;
			sCheckParam.cnHandStone++;
		}
		int nSpecialTileCount = m_pMahjongObj->GetSpecialCount(sCheckParam);
		if (nSpecialTileCount > 0)
		{
			bufGameAction.AppendFormatText(",%d,%d", 9020 + FAN_SPECIAL, nSpecialTileCount);
		}
		bufGameAction.AppendFormatText("\"");
	}

	bufGameAction.AppendFormatText(" />");

	m_pMahjongGame->AddGameAction( bufGameAction.GetBufPtr() );

	//发送亮牌消息 add by zhangws
	for(int k = 0; k < MAX_PLAYER_COUNT; k++)
	{
		char playerTiles[1024] = { 0 };
		PTKACKTILES ackTiles = ( PTKACKTILES )playerTiles;
		ackTiles->header.dwType = ACK_TYPE( TK_MSG_PLAYER_TILES );
		ackTiles->header.dwLength = MSG_LENGTH( TKACKTILES );
		ackTiles->seat = k;
		CTKMahjongPlayer * pOPlayer = m_pMahjongObj->GetPlayerBySeat( k ) ;
		if ( NULL == pOPlayer )
		{
			TKWriteLog( "%s : %d, Error! 获取玩家出错, seat( %d )", __FILE__, __LINE__, k );
			continue;
		}
		ackTiles->cnHandTile = pOPlayer->FillHandTileInfo( ackTiles->anHandTile );
		
		// 分发给所有玩家
		if ( !Broadcast( ( PTKHEADER )ackTiles ) )
		{
			TKWriteLog( "%s : %d, Error! 发送消息出错, len( %d )", __FILE__, __LINE__, ackTiles->header.dwLength );
		}
	}

	return TRUE;
}






// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CLuckyTileState::CLuckyTileState()
{
	// 注册感兴趣的事件
	REGIST_EVENT_FUNCTION( EVENT_1PLAYER_NOT_WIN, &CLuckyTileState::On1PlayerNotWin );
	REGIST_EVENT_FUNCTION( EVENT_2PLAYER_NOT_WIN, &CLuckyTileState::On2PlayerNotWin );
	REGIST_EVENT_FUNCTION( EVENT_3PLAYER_NOT_WIN, &CLuckyTileState::On3PlayerNotWin );
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CLuckyTileState::OnEnter( CEvent * pEvent )
{
	// 发奖花牌
	TKACKMAHJONGLUCKYTILE ackLuckyTile = { 0 };
	ackLuckyTile.header.dwType = ACK_TYPE( TK_MSG_MAHJONG_LUCKYTILE );
	ackLuckyTile.header.dwLength = MSG_LENGTH( TKACKMAHJONGLUCKYTILE );

	// 获得奖花牌信息
	m_pMahjongObj->GetLuckyTile(ackLuckyTile);
	// 发送奖花信息
	m_pMahjongObj->SendLuckyTileInfo(&ackLuckyTile);
	// 重新计算分数
	CalcScore(ackLuckyTile.nHitTileCount);

	// 为客户端处理动画 延迟进入下一状态的时间
	if (m_pMahjongGame != NULL)
	{
		int nDelayTime = ackLuckyTile.nHitTileCount > 0 ? 1000 : 0;
		m_pMahjongGame->SetGameTimerMilli(EVENT_1PLAYER_NOT_WIN, (ULONG_PTR)pEvent->m_pParam, nDelayTime);
	}

	// 记录奖花
	std::string strLuckyTileid,strHitTileid;
	for (int i =0;i < ackLuckyTile.nLuckyTileCount;i++)
	{
		strLuckyTileid += std::to_string((long long)ackLuckyTile.anLuckyTileID[i]);
		if (i != ackLuckyTile.nLuckyTileCount - 1)
		{
			strLuckyTileid += ",";
		}
	}
	for (int i =0;i < ackLuckyTile.nHitTileCount;i++)
	{
		strHitTileid += std::to_string((long long)ackLuckyTile.anHitTileID[i]);
		if (i != ackLuckyTile.nHitTileCount - 1)
		{
			strHitTileid += ",";
		}
	}
	DWORD dwDifMs = m_pMahjongGame->GetRtValue();
	CTKBuffer bufGameAction;
	bufGameAction.AppendFormatText("<a id=\"%d\" rt=\"%d\" rtms=\"%d\" ltc=\"%d\" lti=\"%s\" htc=\"%d\" hti=\"%s\" eltf=\"%d\"/>",
		RECORD_LUCKYTILE,
		dwDifMs / 1000,
		dwDifMs,
		ackLuckyTile.nLuckyTileCount,
		strLuckyTileid.c_str(),
		ackLuckyTile.nHitTileCount,
		strHitTileid.c_str(),
		ackLuckyTile.nEachLuckyTileFan);
	m_pMahjongGame->AddGameAction( bufGameAction.GetBufPtr() );
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CLuckyTileState::On1PlayerNotWin( CEvent * pEvent )
{
	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CLuckyTileState::On2PlayerNotWin( CEvent * pEvent )
{
	return true;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CLuckyTileState::On3PlayerNotWin( CEvent * pEvent )
{
	return true;
}

// ************************************************************************************************************************
// 
// 根据奖花信息重算分数
// 
// ************************************************************************************************************************
bool CLuckyTileState::CalcScore(int nLuckyTileHitCount)
{
	// 取得WININFO 并且清除旧的WININFO
	char szBuffer[sizeof(TKACKMAHJONGWINDETAILEX) * MAX_PLAYER_COUNT] = {0};
	PTKACKMAHJONGWINDETAILEX pAckMahJongWin = (PTKACKMAHJONGWINDETAILEX) szBuffer;
	int nWinCount = m_pMahjongObj->GetWinInfo(pAckMahJongWin);
	m_pMahjongObj->ClearWinInfo();

	// 构造和牌详细信息
	TKACKMAHJONGWINDETAILEX ackDetail = { 0 };
	ackDetail.header.dwType = ACK_TYPE( TK_MSG_MAHJONG_WINDETAILEX );
	ackDetail.header.dwLength = MSG_LENGTH( TKACKMAHJONGWINDETAILEX );
	ackDetail.nTime = 20;
	for ( int i = 0; i < nWinCount; ++i )
	{
		// 更新玩家番种信息
		int nSeat = pAckMahJongWin->nWinSeat;
		CTKMahjongServerPlayer * pPlayer = (CTKMahjongServerPlayer *)m_pMahjongObj->GetPlayerBySeat( nSeat );
		WININFO & sWinInfo = pPlayer->WinInfo();
		int nLuckyTileFan = m_pMahjongGame->Rule()->GetLuckTileFan(nLuckyTileHitCount);
        sWinInfo.anFans[FAN_JSPKLUCKYTILE] = nLuckyTileFan;
		sWinInfo.nMaxFans += nLuckyTileFan;
		sWinInfo.nScoreOfFan = sWinInfo.nMaxFans;
		pPlayer->WinInfo(sWinInfo);
		// 根据新的番种信息更新windetail信息
		int cnDbl = m_pMahjongObj->GetAllTingCount( nSeat );
		ackDetail.nWinSeat = nSeat;
		ackDetail.winDouble = cnDbl;
		ackDetail.nPaoSeat = pAckMahJongWin->nPaoSeat;
		ackDetail.nWinTile = pAckMahJongWin->nWinTile;
		memset( ackDetail.asScoreDetail, 0, sizeof( ackDetail.asScoreDetail ) );
		memcpy( &( ackDetail.sWinInfo ), &sWinInfo, sizeof( WININFO ) );

		// 给赢的玩家加分,输的玩家减分
		IScoreCalculator * pCalculator = m_pMahjongGame->Calculator();
		// 根据加倍调整玩家最大番数
		if ( m_pMahjongGame->Rule()->GetRule( RULE_DOUBLE_COUNT ) )
		{
			pCalculator->SetDouble( cnDbl );
		}
		// 喂过的人
		int cnShowSeat = 0;
		int *pnShowSeat = pPlayer->GetShowSeat( cnShowSeat );
		pCalculator->AddScore( ackDetail, pnShowSeat, cnShowSeat );

		// 通知基类产生了一个得分记录,基类可能会做一些处理(如抽水)以产生最终的得分记录
		int nCashFlow = ackDetail.asScoreDetail[ nSeat ].anScore[ 6 ];
		m_pMahjongGame->SetGameResult( nSeat, 0, 0, 0, ackDetail.asScoreDetail[ nSeat ].anScore[ 6 ] );	
        //记录岛屿赛赢取金币
        if (!m_pMahjongGame->IsChampionhship() && ackDetail.asScoreDetail[nSeat].anScore[6] > 0)
        {
            int nExchangeRate = m_pMahjongGame->m_pMatchRuler->ExchangeRate();
            __int64 nGold = ackDetail.asScoreDetail[nSeat].anScore[6] * (nExchangeRate / 10000.0);   //1积分回兑成nExchangeRate/10000金币
            pPlayer->DealSingleGrow64ByGrowID(GV_WinCoin, nGold);
        }
        //记录胡牌番数
        pPlayer->AddGrowValue(GV_WinFans, ackDetail.sWinInfo.nScoreOfFan);

		int nTax = nCashFlow - ackDetail.asScoreDetail[ nSeat ].anScore[ 6 ];
		if ( nTax > 0 )
		{
			m_pMahjongGame->AddCashFlow( nCashFlow, nTax );
		}
		// 最终各玩家实际得分传给Calculator，Calculator要计算各玩家剩下分数
		for ( int j = 0; j < MAX_PLAYER_COUNT; ++j )
		{
			if ( !m_pMahjongObj->HasPlayer( j ) )
			{
				continue;
			}
			ackDetail.asScoreDetail[ j ].nLeftScore = pCalculator->AddPlayerScore( j, ackDetail.asScoreDetail[ j ].anScore[ 6 ] );
		}

		// 把这个消息保存下来,一局结束后还要发给所有玩家
		m_pMahjongObj->AddWinInfo( ackDetail );

		pAckMahJongWin = (PTKACKMAHJONGWINDETAILEX)((char*)pAckMahJongWin + sizeof(TKACKMAHJONGWINDETAILEX));
	}

	return true;
}

// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CResultState::CResultState()
{
}


#include "TKGameService.h"

// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CResultState::OnEnter( CEvent * pEvent )
{
	// 发送结果消息
	char buf[ 8192 ] = { 0 };
	PTKREQMAHJONGRESULT pResultMsg = ( PTKREQMAHJONGRESULT )buf;
	pResultMsg->header.dwType = REQ_TYPE( TK_MSG_MAHJONG_RESULTEX );
	pResultMsg->header.dwLength = MSG_LENGTH( TKREQMAHJONGRESULT );

	// 取得各家得分
	IScoreCalculator * pCalculator = m_pMahjongGame->Calculator();
	pCalculator->GetPlayerScore( *pResultMsg );

	int winSeat = -1; // 本局赢家(-1说明流局)
	for ( int i=0; i<MAX_PLAYER_COUNT; i++ )
	{
		CTKMahjongServerPlayer * pPlayer = ( CTKMahjongServerPlayer * )m_pMahjongObj->GetPlayerBySeat( i );
		if ( NULL != pPlayer && pPlayer->WinCount() > 0 )
		{
			winSeat = i;
			break;
		}
	}

    //触发原生事件，修改相应积分
    for (int i = 0; i < MAX_PLAYER_COUNT; i++)
    {
        CTKMahjongServerPlayer * pPlayer = (CTKMahjongServerPlayer *)m_pMahjongObj->GetPlayerBySeat(i);
        if (NULL != pPlayer)
        {
            pPlayer->SaveGrowValue(m_pMahjongGame, winSeat);
        }
    }

    int nWinSeat = -1;
	// 没有和牌的人抽水
	for ( int i = 0; i < MAX_PLAYER_COUNT; i++ )
	{
		CTKMahjongServerPlayer * pPlayer = ( CTKMahjongServerPlayer * )m_pMahjongObj->GetPlayerBySeat( i );
		if ( NULL == pPlayer || pPlayer->WinCount() > 0 )
		{
            nWinSeat = i;
			// 这个坐位上没人或者已和过牌了
			continue;
		}

		int nScorePlus	= pResultMsg->asPlayerResult[ i ].nScore; // 本局得分
		int cnDouble	= 0;
		if ( m_pMahjongGame->Rule()->GetRule( RULE_DOUBLE_COUNT ) && winSeat != -1 )
		{
			cnDouble = m_pMahjongObj->GetAllTingCount( winSeat );
		}
        m_pMahjongGame->SetGameResult( i, 0, 0, 0, nScorePlus );
		int  nTax = pResultMsg->asPlayerResult[ i ].nScore - nScorePlus;
		if ( nTax > 0 )
		{
			m_pMahjongGame->AddCashFlow( 0, nTax );
		}

		pResultMsg->asPlayerResult[ i ].nTotalScore -= pResultMsg->asPlayerResult[ i ].nScore - nScorePlus;
		pResultMsg->asPlayerResult[ i ].nScore = nScorePlus;
		pResultMsg->asPlayerResult[ i ].nDouble = cnDouble;
	}

    // 剩余分数 输家积分最小化
    if (m_pMahjongGame->Rule()->GetRule(RULE_SCORE_LOWER_LIMIT) == 1
        && (m_pMahjongGame->m_nScoreType == TK_GAME_SCORETYPE_CHIP
        || m_pMahjongGame->m_nScoreType == TK_GAME_SCORETYPE_LIFE))
    {
        for (int i=0; i<MAX_PLAYER_COUNT; ++i)
        {
            if (pResultMsg->asPlayerResult[ i ].nTotalScore < 0)
            {
                pResultMsg->asPlayerResult[ i ].nTotalScore = 0;
				// 修正玩家本局实际得分
				pResultMsg->asPlayerResult[ i ].nScore = pResultMsg->asPlayerResult[ i ].nTotalScore - pCalculator->GetPlayerBeginScore(i);
            }
        }
    }

	// 记录结果
	DWORD dwDifMs = m_pMahjongGame->GetRtValue();
	CTKBuffer bufGameAction;
	bufGameAction.AppendFormatText("<a id=\"%d\" rt=\"%d\" rtms=\"%d\"", RECORD_RESULTS, dwDifMs/1000, dwDifMs);

    //////////////////////////////////////////////////////////////////////////
    // 猎人赛
    if (m_pMahjongGame->m_stStageRuler.bEnableBounty && 
        nWinSeat != -1)
    {
        int nIndex = 0;
		TKHUNTER_I10X3 stHunterInfo;
        for (int i=0; i<MAX_PLAYER_COUNT; i++)
        {
            if (i == nWinSeat)
                continue;

            CTKGamePlayer* pPlayer = m_pMahjongGame->GetGamePlayerBySeat(i);
            if (pPlayer != NULL && (pPlayer->m_nScore < m_pMahjongGame->m_nOutRoundScore || (m_pMahjongGame->m_nOutRoundScore == 0 && pPlayer->m_nScore <= m_pMahjongGame->m_nOutRoundScore) ) )
            {
                stHunterInfo.liParam[nIndex][0] = nWinSeat;
                stHunterInfo.liParam[nIndex][1] = i;
                stHunterInfo.liParam[nIndex][2] = 100;   // 获取猎杀该玩家赏金的百分之100
                nIndex++;
            }
        }

        if (nIndex > 0)
        {
            CTKGamePlayer* pPlayer = m_pMahjongGame->GetGamePlayerBySeat(nWinSeat);
            if (pPlayer != NULL)
            {
				m_pMahjongGame->HunterAwardByPlayer(pPlayer->m_dwUserID, &stHunterInfo);
            }
        }
    }

    //////////////////////////////////////////////////////////////////////////
    // 赢牌奖励金币
    if (m_pMahjongGame != NULL
        && m_pMahjongGame->m_stStageRuler.bEnableHandWinAward
        && nWinSeat != -1
        && pResultMsg->asPlayerResult[nWinSeat].nScore >= m_pMahjongGame->Rule()->GetRule(RULE_HAND_WIN_SCORE))
    {
        CTKGamePlayer* pGamePlayer = m_pMahjongGame->GetGamePlayerBySeat(nWinSeat);
        if (pGamePlayer != NULL)
        {
            CTKFixList<DWORD> fixlist;
            fixlist.AddTail(&pGamePlayer->m_dwUserID);
            BOOL bResult = m_pMahjongGame->HandWinAwardByPlayer(pGamePlayer->m_dwUserID, 0, 0, fixlist);
            if (!bResult)
            {
                assert(false);
                TKWriteLog("%s : %d, ywx, 奖励金币失败！userid：%lu\n", __FILE__, __LINE__, pGamePlayer->m_dwUserID);
            }
        }
    }

    //////////////////////////////////////////////////////////////////////////
	char szResult[ 128 ] = { 0 };
	// 填写各玩家最终信息
	for ( int i = 0; i < MAX_PLAYER_COUNT; i++ )
	{
		if ( !m_pMahjongObj->HasPlayer( i ) )
		{
			// 这个坐位上没人
			continue;
		}

		CTKMahjongServerPlayer * pPlayer = ( CTKMahjongServerPlayer * )m_pMahjongObj->GetPlayerBySeat( i );
		PPLAYERRESULT pPlayerResult = pResultMsg->asPlayerResult + i;
		pPlayerResult->cnWin = pPlayer->WinCount();
		pPlayerResult->cnGang = pPlayer->GangCount();
		pPlayerResult->cnPao = pPlayer->PaoCount();
		pPlayerResult->cnHandTile = pPlayer->FillHandTileInfo( pPlayerResult->anHandTile );

		memset(szResult, 0, sizeof(szResult));

		bufGameAction.AppendFormatText(" ri%d=\"%d,%d,%d,%d,%d,%d,%d\"", 
			i, i, 
			pPlayerResult->cnWin, 
			pPlayerResult->cnGang, 
			pPlayerResult->cnPao,
			0, pPlayerResult->nScore, 
			pPlayerResult->nTotalScore);

		// 机器人胜率统计
		if (ISBOTUSERID(pPlayer->UserID()))
		{
			gtk_pMahjongJSPKService->CalcBotScore(pPlayerResult->nScore);
		}

		/*CTKBuffer bufResultInfo;
		bufResultInfo.AppendFormatText("<result seat=\"%d\" score=\"%d\" />", i, pPlayerResult->nScore);
		m_pMahjongGame->AddGameResult(bufResultInfo.GetBufPtr());*/
	}
	pResultMsg->nTime = 20;


	// 取得各玩家的和牌信息
	PTKACKMAHJONGWINDETAILEX pAckMahJongWin = ( PTKACKMAHJONGWINDETAILEX )( pResultMsg + 1 );
	pResultMsg->cnWinInfo = m_pMahjongObj->GetWinInfo( pAckMahJongWin );
	pResultMsg->header.dwLength += pResultMsg->cnWinInfo * sizeof( TKACKMAHJONGWINDETAILEX );
	//char szWinInfo[ 256 ] = { 0 };
	//sprintf( szWinInfo, ",%d\" />", pResultMsg->cnWinInfo );
	//strcat( szGameActionBuf, szWinInfo );

	// 先附上暗杠牌
	m_pMahjongObj->SendMsgShowTiles( 0, true );
	m_pMahjongObj->SendMsgShowTiles( 1, true );

	if ((NULL != pEvent && pResultMsg->cnWinInfo == 0))
	{
		//正常流局
		pResultMsg->nResultType = 2;
	}
	else if (NULL == pEvent && pResultMsg->cnWinInfo == 0)
	{
		//积分输光
		pResultMsg->nResultType = 1;
	}
	else
	{
		//正常结束
		pResultMsg->nResultType = 0;
	}

	// 广播给各玩家
	if ( !Broadcast( ( PTKHEADER )pResultMsg ) )
	{
		TKWriteLog( "%s : %d, Error! 发送消息出错, len( %d )", __FILE__, __LINE__, pResultMsg->header.dwLength );
	}

	bufGameAction.AppendFormatText(" />");
	m_pMahjongGame->AddGameAction( bufGameAction.GetBufPtr() );

	m_pMahjongGame->State();

	//记录Result
	for ( int i = 0; i < MAX_PLAYER_COUNT; i++ )
	{
		PPLAYERRESULT pPlayerResult = pResultMsg->asPlayerResult + i;

		CTKBuffer bufResultInfo;
		bufResultInfo.AppendFormatText("<result seat=\"%d\" score=\"%d\" />", i, pPlayerResult->nScore);
		m_pMahjongGame->AddGameResult(bufResultInfo.GetBufPtr());
	}

	if (0 <= nWinSeat && 1 == m_pMahjongGame->Rule()->GetRule(RULE_TREASURE_CARD))
	{
		CTKMahjongGamePlayer* pGamePlayer = dynamic_cast<CTKMahjongGamePlayer*>(m_pMahjongGame->GetGamePlayerBySeat(nWinSeat));
		if (NULL != pGamePlayer)
		{
			pGamePlayer->SetWinScore(pResultMsg->asPlayerResult[nWinSeat].nScore);

			RateTool* pRateKit = new RateTool;
			pRateKit->InitData(m_pMahjongGame->Rule()->GetRule(RULE_TREASURE_LEVEL0),
				m_pMahjongGame->Rule()->GetRule(RULE_TREASURE_LEVEL2),
				m_pMahjongGame->Rule()->GetRule(RULE_TREASURE_LEVEL5));

			pGamePlayer->SetTreasureContinueRule(m_pMahjongGame->Rule()->GetRule(RULE_TREASURE_PICK));
			pGamePlayer->SetTreasureRateKit(pRateKit);

#if 0
			TK_GAME_TIMER_REC stTimer;
			memset(&stTimer, 0, sizeof(stTimer));
			stTimer.header.nTimerType = 10;
			stTimer.header.dwLengthParam = sizeof(stTimer) - sizeof(stTimer.header);
			stTimer.header.dwOwnerID = pGamePlayer->m_dwUserID;
			stTimer.header.dwTimerID = 1;
			stTimer.dwMatchID = pGamePlayer->m_dwMatchID;
			stTimer.wStageID = pGamePlayer->m_wStageID;
			stTimer.wRoundID = pGamePlayer->m_wRoundID;
			stTimer.dwID = 1;
			stTimer.ulUserData = pGamePlayer->m_dwUserID;

			tk_pGameService->SetTimerMilli((PTKOBJTIMERHEADER)&stTimer, 5000);
#endif
		}
	}

	m_pMahjongGame->NotifyGameOver(TK_GAMEOVERTYPE_NORNAL);
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CBloodyWinState::CBloodyWinState()
{
	// 注册感兴趣的事件
	REGIST_EVENT_FUNCTION( EVENT_1PLAYER_NOT_WIN, &CBloodyWinState::On1PlayerNotWin );
	REGIST_EVENT_FUNCTION( EVENT_2PLAYER_NOT_WIN, &CBloodyWinState::On2PlayerNotWin );
	REGIST_EVENT_FUNCTION( EVENT_3PLAYER_NOT_WIN, &CBloodyWinState::On3PlayerNotWin );
	REGIST_EVENT_FUNCTION( EVENT_LUCKY_TILE, &CBloodyWinState::OnLuckyTile );

	m_cnWinner = 0;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CBloodyWinState::OnEnter(CEvent * pEvent)
{
    // 找出应和牌的玩家
    PTKREQMAHJONGWIN apReqMahJongWin[MAX_PLAYER_COUNT - 1] = { 0 };
    int cnReqWin = 0;
    CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
    if (EVENT_WIN_SELF != pEvent->ID())
    {
        // 不是自摸,可能有多个人和
        // 先找出点炮的玩家
        int nPaoSeat = -1;
        if (EVENT_WIN_PAO == pEvent->ID())
        {
            // 如果是点炮和的,点炮的玩家就是出牌玩家
            nPaoSeat = m_pMahjongObj->CurDiscardSeat();
        }
        else if (EVENT_WINGANG == pEvent->ID())
        {
            // 抢杠和的,点炮的玩家就是杠牌玩家
            nPaoSeat = m_pMahjongObj->LastGangSeat();
        }

        int nSeat = nPaoSeat;
        m_cnWinner++;
        for (int i = 1; i < m_pMahjongObj->PlayerCount(); i++)
        {
            nSeat = m_pMahjongObj->GetNextPlayerSeat(nSeat, false);	// 取下一家的座位号
            if (pRequestObj->IsRequestSeat(nSeat) && pRequestObj->IsPlayerAckRequestType(nSeat, REQUEST_TYPE_WIN))
            { // 请求了这个玩家，并且这个玩家说和牌了
                apReqMahJongWin[cnReqWin] = (PTKREQMAHJONGWIN)pRequestObj->GetPlayerAckMsg(nSeat);
                cnReqWin++;
                break;
            }
        }
    }
    else
    {
        // 是自摸的,只可能有一个人和
        int nWinSeat = pRequestObj->GetAckMsgSeat(REQUEST_TYPE_WIN);
        m_cnWinner++;
        apReqMahJongWin[cnReqWin] = (PTKREQMAHJONGWIN)pRequestObj->GetPlayerAckMsg(nWinSeat);
        cnReqWin++;
    }

    Win(apReqMahJongWin, cnReqWin, EVENT_WIN_SELF == pEvent->ID());
    //	if ( 1 != cnReqWin )
    //	{
    //		TKWriteLog( "%s : %d, 无效的消息, ID( %d, %d ), cnReqWin( %d ), m_cnWinner( %d )", __FILE__, __LINE__, pEvent->ID(), pEvent->ID() - REQ_TYPE( TK_MSG_TRUSTPLAY ), cnReqWin, m_cnWinner );
    //	}

        // 计算一下还有几个人未和牌
    int cnNotWin = m_pMahjongObj->PlayerCount() - m_cnWinner;
    if (cnNotWin < 1)
    {
        ASSERT(false);
        TKWriteLog("所有人都和牌了???傻了吧~状态配置文件写错了~");
        return;
    }

    CEvent event(EVENT_1PLAYER_NOT_WIN + cnNotWin - 1, 0);
    PostEvent(event);
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CBloodyWinState::On1PlayerNotWin( CEvent * pEvent )
{
	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CBloodyWinState::On2PlayerNotWin( CEvent * pEvent )
{
	return true;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CBloodyWinState::On3PlayerNotWin( CEvent * pEvent )
{
	return true;
}

// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CBloodyWinState::OnLuckyTile( CEvent * pEvent )
{
	return true;
}

// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CBloodyWinState::Win( PTKREQMAHJONGWIN apReqMahJongWin[], int cnReqWin, bool bWinSelf )
{
	if ( cnReqWin < 1 || cnReqWin > 3 )
	{
		TKWriteLog( "%s : %d, 无效的参数cnReqWin = %d", __FILE__, __LINE__, cnReqWin );
		return false;
	}

	// 让和牌的玩家先做一些和牌处理
	TKACKMAHJONGWIN ackWin = { 0 };
	ackWin.header.dwType = ACK_TYPE( TK_MSG_MAHJONG_WIN );
	ackWin.header.dwLength = MSG_LENGTH( TKACKMAHJONGWIN );
	ackWin.nPaoSeat = apReqMahJongWin[ 0 ]->nPaoSeat;
	if ( bWinSelf )
	{
		ackWin.nPaoSeat = -1;
	}
	ackWin.nWinTile = apReqMahJongWin[ 0 ]->nTileID;
	for ( int i = 0; i < cnReqWin; ++i )
	{
		m_pMahjongObj->PlayerWin( apReqMahJongWin[ i ] );
		ackWin.anWinSeat[ ackWin.cnWinSeat ] = apReqMahJongWin[ i ]->nSeat;
		ackWin.cnWinSeat++;
		CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerBySeat( apReqMahJongWin[ i ]->nSeat );
		WININFO & sWinInfo = pPlayer->WinInfo();
		ackWin.anWinMode[ i ] = sWinInfo.nWinMode;
	}

	// 广播和牌消息给所有玩家
	if ( !Broadcast( ( PTKHEADER )&ackWin ) )
	{
		TKWriteLog( "%s : %d, Error! 发送消息出错, len( %d )", __FILE__, __LINE__, ackWin.header.dwLength );
	}

	// 记录和牌
	DWORD dwDifMs = m_pMahjongGame->GetRtValue();
	CTKBuffer bufGameAction;
	bufGameAction.AppendFormatText("<a id=\"%d\" rt=\"%d\" rtms=\"%d\" wti=\"%d\" ps=\"%d\" wc=\"%d\"", 
		RECORD_WIN, 
		dwDifMs / 1000,
		dwDifMs,
		ackWin.nWinTile, 
		ackWin.nPaoSeat, 
		cnReqWin);

	// 构造和牌详细信息给和牌者
	TKACKMAHJONGWINDETAILEX ackDetail = { 0 };
	ackDetail.header.dwType = ACK_TYPE( TK_MSG_MAHJONG_WINDETAILEX );
	ackDetail.header.dwLength = MSG_LENGTH( TKACKMAHJONGWINDETAILEX );
	ackDetail.nTime = 60;
	TKREQSCORECHANGE reqScore = { 0 };
	for ( int i = 0; i < cnReqWin; ++i )
	{
		int nSeat = apReqMahJongWin[ i ]->nSeat;
		ackDetail.nWinSeat = nSeat;
		ackDetail.nPaoSeat = apReqMahJongWin[ i ]->nPaoSeat;
		ackDetail.nWinTile = apReqMahJongWin[ i ]->nTileID;
		memset( ackDetail.asScoreDetail, 0, sizeof( ackDetail.asScoreDetail ) );
		CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerBySeat( nSeat );
		WININFO & sWinInfo = pPlayer->WinInfo();
		memcpy( &( ackDetail.sWinInfo ), &sWinInfo, sizeof( WININFO ) );

		// 给赢的玩家加分,输的玩家减分
		IScoreCalculator * pCalculator = m_pMahjongGame->Calculator();
		// 喂过的人
		int cnShowSeat = 0;
		int *pnShowSeat = pPlayer->GetShowSeat( cnShowSeat );
		pCalculator->AddScore( ackDetail, pnShowSeat, cnShowSeat );

		// 取这个人以前输的分
		int nScore = pCalculator->GetPlayerScore( nSeat );

		// 通知基类产生了一个得分记录,基类可能会做一些处理(如抽水)以产生最终的得分记录
		int nTotalScore = nScore + ackDetail.asScoreDetail[ nSeat ].anScore[ 6 ];	// 加上这一把的得分
		int nCashFlow = nTotalScore;
		m_pMahjongGame->SetGameResult( nSeat, 0, 0, 0, nTotalScore );	// 得出总得分
		int nTax = nCashFlow - nTotalScore;
		if ( nTax > 0 )
		{
			m_pMahjongGame->AddCashFlow( ackDetail.asScoreDetail[ nSeat ].anScore[ 6 ], nTax );
		}
		ackDetail.asScoreDetail[ nSeat ].anScore[ 6 ] = nTotalScore - nScore;	// 这一把的实际得分

		// 最终各玩家实际得分传给Calculator，Calculator要计算各玩家剩下分数
		for ( int j = 0; j < MAX_PLAYER_COUNT; ++j )
		{
			if ( !m_pMahjongObj->HasPlayer( j ) )
			{
				continue;
			}

			ackDetail.asScoreDetail[ j ].nLeftScore = pCalculator->AddPlayerScore( j, ackDetail.asScoreDetail[ j ].anScore[ 6 ] );
			reqScore.anIncremental[ j ] += ackDetail.asScoreDetail[ j ].anScore[ 6 ];
		}

        // 剩余分数 输家积分最小化
        if (m_pMahjongGame->Rule()->GetRule(RULE_SCORE_LOWER_LIMIT) == 1
            && (m_pMahjongGame->m_nScoreType == TK_GAME_SCORETYPE_CHIP
            || m_pMahjongGame->m_nScoreType == TK_GAME_SCORETYPE_LIFE))
        {
            if (ackDetail.asScoreDetail[nSeat].nLeftScore < 0)
            {
                ackDetail.asScoreDetail[nSeat].nLeftScore = 0;
            }
        }

		// 给和牌的玩家发完整的消息
		if ( !Send2SeatPlayer( nSeat, ( PTKHEADER )&ackDetail ) )
		{
			TKWriteLog( "%s : %d, Error! 发送消息错误 len( %d )", __FILE__, __LINE__, ackDetail.header.dwLength );
		}

        if (m_pMahjongGame->Rule()->GetRule(RULE_SCORE_LOWER_LIMIT) == 1
            && (m_pMahjongGame->m_nScoreType == TK_GAME_SCORETYPE_CHIP
            || m_pMahjongGame->m_nScoreType == TK_GAME_SCORETYPE_LIFE))
        {// 通知其他人WinPlayer剩余分数情况
            TKACKMAHJONGWINDETAILEX ackDetailTemp = {0};
            ackDetailTemp.nWinSeat = ackDetail.nWinSeat;
            ackDetailTemp.asScoreDetail[nSeat].nLeftScore = ackDetail.asScoreDetail[nSeat].nLeftScore;

            for (int i=0; i<MAX_PLAYER_COUNT; ++i)
            {
                if (i == nSeat)
                {
                    continue;
                }

                if (m_pMahjongGame->IsHaveplayerSeat(i))
                {
                    if (!Send2SeatPlayer(i, (PTKHEADER)&ackDetailTemp))
                    {
                        TKWriteLog("%s : %d, Error! 发送消息错误 len( %d )", __FILE__, __LINE__, ackDetailTemp.header.dwLength);
                    }
                }
            }
        }

		// 把这个消息保存下来,一局结束后还要发给所有玩家
		m_pMahjongObj->AddWinInfo( ackDetail );

		// 记下和牌者座位号和和牌方式
		bufGameAction.AppendFormatText(" wi%d=\"%d,%d,%d", i, nSeat, sWinInfo.nWinMode, ackDetail.nBaseScore);

		// 每把和牌得分明细
		for ( int k = 0; k < MAX_PLAYER_COUNT; ++k )
		{
			for ( int m = 0; m < 8; ++m )
			{
				bufGameAction.AppendFormatText(",%d", ackDetail.asScoreDetail[ k ].anScore[ m ]);
			}
			bufGameAction.AppendFormatText(",%d", ackDetail.asScoreDetail[ k ].nLeftScore);
		}
		bufGameAction.AppendFormatText( "\"");
		
		bufGameAction.AppendFormatText(" nwfi%d=\"%d", i, ackDetail.sWinInfo.nMaxFans);

		for(int j=0; j < MAXFANS; j++)
		{
			if(ackDetail.sWinInfo.anFans[j] > 0)
			{
				bufGameAction.AppendFormatText(",%d,%d", 9020 + j, ackDetail.sWinInfo.anFans[j]);
			}
		}
		bufGameAction.AppendFormatText("\"");
	}

	bufGameAction.AppendFormatText(" />");
	m_pMahjongGame->AddGameAction( bufGameAction.GetBufPtr() );

	// 通知各玩家分数变化
	reqScore.header.dwType = REQ_TYPE( TK_MSG_SCORE_CHANGE );
	reqScore.header.dwLength = MSG_LENGTH( TKREQSCORECHANGE );
	Broadcast( ( PTKHEADER )&reqScore );

	return TRUE;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CBloodyResultState::OnEnter( CEvent * pEvent )
{
	// 发送结果消息
	char buf[ 8192 ] = { 0 };
	PTKREQMAHJONGRESULT pResultMsg = ( PTKREQMAHJONGRESULT )buf;
	pResultMsg->header.dwType = REQ_TYPE( TK_MSG_MAHJONG_RESULT );
	pResultMsg->header.dwLength = MSG_LENGTH( TKREQMAHJONGRESULT );

	// 取得各家得分
	IScoreCalculator * pCalculator = m_pMahjongGame->Calculator();
	pCalculator->GetPlayerScore( *pResultMsg );

	int winSeat = -1; // 本局赢家(-1说明流局)
	for ( int i=0; i<MAX_PLAYER_COUNT; i++ )
	{
		CTKMahjongServerPlayer * pPlayer = ( CTKMahjongServerPlayer * )m_pMahjongObj->GetPlayerBySeat( i );
		if ( NULL != pPlayer && pPlayer->WinCount() > 0 )
		{
			winSeat = i;
			break;
		}
	}

	// 没有和牌的人抽水
	for ( int i = 0; i < MAX_PLAYER_COUNT; i++ )
	{
		CTKMahjongServerPlayer * pPlayer = ( CTKMahjongServerPlayer * )m_pMahjongObj->GetPlayerBySeat( i );
		if ( NULL == pPlayer || pPlayer->WinCount() > 0 )
		{
			// 这个坐位上没人或者已和过牌了
			continue;
		}

		int nScorePlus = pResultMsg->asPlayerResult[ i ].nScore;
		m_pMahjongGame->SetGameResult( i, 0, 0, 0, nScorePlus );
		int nTax = pResultMsg->asPlayerResult[ i ].nScore - nScorePlus;
		if ( nTax > 0 )
		{
			m_pMahjongGame->AddCashFlow( 0, nTax );
		}
		pResultMsg->asPlayerResult[ i ].nTotalScore -= pResultMsg->asPlayerResult[ i ].nScore - nScorePlus;
		pResultMsg->asPlayerResult[ i ].nScore = nScorePlus;
	}

    // 剩余分数 输家积分最小化
    if (m_pMahjongGame->Rule()->GetRule(RULE_SCORE_LOWER_LIMIT) == 1
        && (m_pMahjongGame->m_nScoreType == TK_GAME_SCORETYPE_CHIP
        || m_pMahjongGame->m_nScoreType == TK_GAME_SCORETYPE_LIFE))
    {
        for (int i=0; i<MAX_PLAYER_COUNT; ++i)
        {
            if (pResultMsg->asPlayerResult[ i ].nTotalScore < 0)
            {
                pResultMsg->asPlayerResult[ i ].nTotalScore = 0;
            }
        }
    }

	// 记录结果
	DWORD dwDifMs = m_pMahjongGame->GetRtValue();
	CTKBuffer bufGameAction;
	bufGameAction.AppendFormatText("<a id=\"%d\" rt=\"%d\" rtms=\"%d\"", RECORD_RESULTS, dwDifMs/1000, dwDifMs);

	// 填写各玩家最终信息
	for ( int i = 0; i < MAX_PLAYER_COUNT; i++ )
	{
		if ( !m_pMahjongObj->HasPlayer( i ) )
		{
			// 这个坐位上没人
			continue;
		}

		CTKMahjongServerPlayer * pPlayer = ( CTKMahjongServerPlayer * )m_pMahjongObj->GetPlayerBySeat( i );
		PPLAYERRESULT pPlayerResult = pResultMsg->asPlayerResult + i;
		pPlayerResult->cnWin = pPlayer->WinCount();
		pPlayerResult->cnGang = pPlayer->GangCount();
		pPlayerResult->cnPao = pPlayer->PaoCount();
		pPlayerResult->cnHandTile = pPlayer->FillHandTileInfo( pPlayerResult->anHandTile );

		bufGameAction.AppendFormatText(" ri%d=\"%d,%d,%d,%d,%d,%d,%d\"", 
			i, i, 
			pPlayerResult->cnWin, 
			pPlayerResult->cnGang, 
			pPlayerResult->cnPao,
			0, pPlayerResult->nScore, 
			pPlayerResult->nTotalScore);
	
		/*CTKBuffer bufResultInfo;
		bufResultInfo.AppendFormatText("<result seat=\"%d\" score=\"%d\" />", i, pPlayerResult->nScore);
		m_pMahjongGame->AddGameResult(bufResultInfo.GetBufPtr());*/
	}
	pResultMsg->nTime = 40;


	// 取得各玩家的和牌信息
	PTKACKMAHJONGWINDETAILEX pAckMahJongWin = ( PTKACKMAHJONGWINDETAILEX )( pResultMsg + 1 );
	pResultMsg->cnWinInfo = m_pMahjongObj->GetWinInfo( pAckMahJongWin );
	pResultMsg->header.dwLength += pResultMsg->cnWinInfo * sizeof( TKACKMAHJONGWINDETAILEX );
	//char szWinInfo[ 256 ] = { 0 };
	//sprintf( szWinInfo, ",%d\" />", pResultMsg->cnWinInfo );
	//strcat( szGameActionBuf, szWinInfo );

	m_pMahjongObj->SendMsgShowTiles( 0, true );
	m_pMahjongObj->SendMsgShowTiles( 1, true );

	// 广播给各玩家
	if ( !Broadcast( ( PTKHEADER )pResultMsg ) )
	{
		TKWriteLog( "%s : %d, Error! 发送消息出错, len( %d )", __FILE__, __LINE__, pResultMsg->header.dwLength );
	}

	bufGameAction.AppendFormatText(" />");
	m_pMahjongGame->AddGameAction( bufGameAction.GetBufPtr() );
	
	m_pMahjongGame->State();

	//记录Result
	for ( int i = 0; i < MAX_PLAYER_COUNT; i++ )
	{
		PPLAYERRESULT pPlayerResult = pResultMsg->asPlayerResult + i;

		CTKBuffer bufResultInfo;
		bufResultInfo.AppendFormatText("<result seat=\"%d\" score=\"%d\" />", i, pPlayerResult->nScore);
		m_pMahjongGame->AddGameResult(bufResultInfo.GetBufPtr());
	}

	// 游戏结束了
	m_pMahjongGame->NotifyGameOver( TK_GAMEOVERTYPE_NORNAL );
}
