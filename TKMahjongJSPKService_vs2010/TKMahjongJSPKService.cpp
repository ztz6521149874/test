#include ".\tkMahjongJSPKservice.h"
#include "VersionTKMahjongsevice.h"
#include "TKMahjongGame.h"
#include "TKMahjongServerPlayer.h"
#include "GameState.h"
#include "Version.h"
#include "TKMatchAgent.h"
#include "TKMahjongGamePlayer.h"

#include <fstream>

CTKMahjongJSPKService *gtk_pMahjongJSPKService = NULL;

// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
CTKMahjongJSPKService::CTKMahjongJSPKService(void)
{
	CTKGameService::m_nGameID = TK_GAMEID_MJJSPK;
	m_nPort = 21082;
	m_nBotSrvPort = 31082;
	m_nBotWinCount = 0;
	m_nBotLoseCount = 0;
	m_nBotWinScoreTotal = 0;
	m_nBotLoseScoreTotal = 0;
	m_timeLastOutputLog = 0;
	gtk_pMahjongJSPKService = this;
	m_bAsyncInitial = FALSE;
}

// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
CTKMahjongJSPKService::~CTKMahjongJSPKService(void)
{
}


// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
BOOL CTKMahjongJSPKService::OnInitialUpdate()
{
	if ( !CTKGameService::OnInitialUpdate() ) 
	{
		return FALSE;
	}

	srand(time(NULL));
    RegisterVersion("TKMahjongJSPKService", "1.0.0.0");

	if (!m_clsAsyncSendObj.Init(this, &CTKMahjongJSPKService::OnAsyncSendToHebbroker, 1, 10))
	{
		TKWriteLog("%s : %d,m_clsAsyncSendObj.OnInitialUpdate failed!", __FILE__, __LINE__);
		return FALSE;
	}

#if 0
	std::fstream fHandle;
	fHandle.open("mahjongjspk-net.ini");
	if (!fHandle.is_open())
	{
		TKWriteLog("Open mahjongjspk-net.ini failed.");
		return TRUE;
	}

	CTKFixList<TKADDRINFO> addrLst;
	int nPort = 0;
	fHandle >> nPort;
	while (!fHandle.eof())
	{
		std::string ipAdrr;
		fHandle >> ipAdrr;

		if (!ipAdrr.empty() && ipAdrr.at(0) != ';')
		{
			TKADDRINFO addrInfo = { 0 };
			addrInfo.dwIP = inet_addr(ipAdrr.c_str());
			addrInfo.nPort = nPort;
			addrLst.AddTail(&addrInfo);
		}
	}

	if (!m_clsHebbrokerPool.OnInitialUpdate(addrLst, 10, TK_CONNECT_IDLEMODE_ONLYKEEP))
	{
		TKWriteLog("%s : %d,m_clsHebbrokerPool.OnInitialUpdate failed!TKHEBBrokerServer = IP : Port", __FILE__, __LINE__);
		return TRUE;
	}
#else
	//TODO GameSettings
	if (!m_clsHebbrokerPool.OnInitialUpdate("MahjongJSPKSpecific", "HEBBrokerServer", 10, TK_CONNECT_IDLEMODE_ONLYKEEP, tk_szGeneralServerIniFile))
	{
		TKWriteLog("%s : %d,m_clsHebbrokerPool.OnInitialUpdate failed!,Please Set [%s].TKHEBBrokerServer = IP : Port", __FILE__, __LINE__, "MahjongJSPKSpecific");
		return TRUE;
	}
#endif
	m_bAsyncInitial = TRUE;
	return TRUE;
}


// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
BOOL CTKMahjongJSPKService::OnRegisterTKObject()
{
	if ( !CTKGameService::OnRegisterTKObject() )
	{
		return FALSE;
	}

	// 注册需要用到的CTKGame派生类
	REGISTER_TKCLASS( CTKGame, CTKMahjongGame );
	REGISTER_TKCLASS( CTKGamePlayer, CTKMahjongGamePlayer );
	REGISTER_TKCLASS( CTKUserObject, CTKMahjongMatchSvrUser );

	// 如果有CTKMahjongPlayer，也需要注册

	REGISTER_TKCLASS( IState, CPrepareState );
	REGISTER_TKCLASS( IState, CShuffleState );
	REGISTER_TKCLASS( IState, CCastDiceState );
	REGISTER_TKCLASS( IState, COpenDoorState );
	REGISTER_TKCLASS( IState, CChangeAllFlowerState );
	REGISTER_TKCLASS( IState, CTianTingState );
	REGISTER_TKCLASS( IState, CDiscardTileState );
	REGISTER_TKCLASS( IState, CWaitWinGangState );
	REGISTER_TKCLASS( IState, CWaitState );
	REGISTER_TKCLASS( IState, CDrawTileState );
	REGISTER_TKCLASS( IState, CDrawTileAfterGangState );
	REGISTER_TKCLASS( IState, CDrawTileAfterChangeFlowerState );
	REGISTER_TKCLASS( IState, CDiscardAfterChiPengState );
	REGISTER_TKCLASS( IState, CWinState );
	REGISTER_TKCLASS( IState, CResultState );
	REGISTER_TKCLASS( IState, CBloodyWinState );
	REGISTER_TKCLASS( IState, CBloodyResultState );
	REGISTER_TKCLASS( IState, CDiscardGangAfterChiPengState );
	REGISTER_TKCLASS( IState, CLuckyTileState );

	return TRUE;
}


// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
CTKGame* CTKMahjongJSPKService::OnCreateGame()
{
	return CREATE_TKOBJECT( CTKGame, CTKMahjongGame );
}

CTKGamePlayer * CTKMahjongJSPKService::OnCreateTKGamePlayer()
{
	return  CREATE_TKOBJECT(CTKGamePlayer, CTKMahjongGamePlayer);
}

CTKUserObject * CTKMahjongJSPKService::OnCreateTKUserObject(int nOrigine)
{
	if (TK_CONNECTIONTYPE_MATCHSVR == nOrigine)
	{
		return CREATE_TKOBJECT(CTKUserObject, CTKMahjongMatchSvrUser);
	}
	return __super::OnCreateTKUserObject(nOrigine);
}



// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
int CTKMahjongJSPKService::OnCalcMatchRank(int nMatchScore)
{
	return nMatchScore;
}



// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
BOOL CTKMahjongJSPKService::OnTimeout(struct tm *pTime)
{
	if ( !CTKGameService::OnTimeout( pTime ) )
	{
		return FALSE;
	}

	// 输出机器人胜率信息
	time_t tNow = time(NULL);
	if (tNow - m_timeLastOutputLog >= 3600)
	{
		OutPutBotScore();
		m_timeLastOutputLog = tNow;
	}

	return TRUE;
}

BOOL CTKMahjongJSPKService::OnTimer(PTKOBJTIMERHEADER pTKTimer) 
{
#if 0
	if (pTKTimer->nTimerType == 10)
	{
		TK_GAME_TIMER_REC* gameTimer = (TK_GAME_TIMER_REC*)pTKTimer;
		CTKGamePlayer* pPlayer = GetGamePlayer(gameTimer->ulUserData);
		if (NULL != pPlayer)
		{
			CTKMahjongGamePlayer* pGlobalPlayer = (CTKMahjongGamePlayer*)pPlayer;
			pGlobalPlayer->Test();
		}
	}
#endif
	return __super::OnTimer(pTKTimer);
}

BOOL CTKMahjongJSPKService::CalcBotScore( int nScore )
{
	m_clsLockCalcBotScore.Lock();
	if (nScore > 0)
	{
		m_nBotWinCount++;
		m_nBotWinScoreTotal += abs(nScore);
	}
	else
	{
		m_nBotLoseCount++;
		m_nBotLoseScoreTotal += abs(nScore);
	}
	m_clsLockCalcBotScore.UnLock();

	return TRUE;
}

BOOL CTKMahjongJSPKService::OutPutBotScore()
{
	m_clsLockCalcBotScore.Lock();
	if (m_nBotWinCount <= 0 && m_nBotLoseCount <= 0)
	{
		m_clsLockCalcBotScore.UnLock();
		return FALSE;
	}

	INT64 dwTotalRoundCount = m_nBotWinCount + m_nBotLoseCount;
	double fWinRate = (double)m_nBotWinCount / dwTotalRoundCount;
	INT64 nIncome = m_nBotWinScoreTotal - m_nBotLoseScoreTotal;
	double fAverageIncome = (double)nIncome / dwTotalRoundCount;

	TKWriteLog("机器人胜率信息 : 胜局[%lld] 负局[%lld] 总局[%lld] 胜率[%.2f] 赢分[%lld] 输分[%lld] 收益[%lld] 平均收益[%.2f]",
		m_nBotWinCount,
		m_nBotLoseCount,
		dwTotalRoundCount,
		fWinRate,
		m_nBotWinScoreTotal,
		m_nBotLoseScoreTotal,
		nIncome,
		fAverageIncome);

	m_clsLockCalcBotScore.UnLock();

	return TRUE;
}

CTKGamePlayer * CTKMahjongJSPKService::GetGamePlayer(DWORD dwUserID)
{
    CTKGamePlayer * pPlayer = NULL;
    CTKPtrList<CTKMatchAgent> listMatchAgent;
    GetMatchAgentList(listMatchAgent);
    int cnMatchAgent = listMatchAgent.GetCount();
    for (int i = 0; i < cnMatchAgent; i++)
    {
        pPlayer = listMatchAgent.GetAt(i)->GetGamePlayer(dwUserID);
        if (NULL != pPlayer)
        {
            break;
        }
    }
    RELREF_TKOBJECT_LIST(listMatchAgent, CTKMatchAgent);
    return pPlayer;
}

void CTKMahjongJSPKService::Broadcast(TKMobileAckMsg& rMsg)
{
    std::string szSerializeMsg;
    rMsg.SerializeToString(&szSerializeMsg);

    // 发送
    BYTE* pszPack = new BYTE[sizeof(TKHEADER) + rMsg.ByteSize()];    // 分配空间
    ZeroMemory(pszPack, sizeof(TKHEADER) + rMsg.ByteSize());

    ((PTKHEADER)pszPack)->dwType = TK_ACK | TK_MSG_MAHJONG_PROTOBUF;
    ((PTKHEADER)pszPack)->dwLength = rMsg.ByteSize();   // size
    BYTE* pData = pszPack + sizeof(TKHEADER);
    memcpy(pData, szSerializeMsg.c_str(), rMsg.ByteSize());

    CTKPtrList<CTKMatchAgent> listMatchAgent;
    GetMatchAgentList(listMatchAgent);
    int cnMatchAgent = listMatchAgent.GetCount();
    for (int i = 0; i < cnMatchAgent; i++)
    {
        listMatchAgent.GetAt(i)->Broadcast2MatchClient((PTKHEADER)pszPack);
    }
    RELREF_TKOBJECT_LIST(listMatchAgent, CTKMatchAgent);
    delete[] pszPack;
    pszPack = NULL;
}

BOOL CTKMahjongJSPKService::PostMsg2Hebbroker(CTKGamePlayer* pPlayer, PTKHEADER pTkHead)
{
	if (!m_bAsyncInitial)
	{
		TKWriteLog("%s : %d, m_clsAsyncSendObj failed, network is uninitialized!", __FILE__, __LINE__);
		return FALSE;
	}

	if (!m_clsAsyncSendObj.AddMsg(pTkHead, 0, TRUE, pPlayer->m_dwUserID))
	{
		TKWriteLog("%s : %d, m_clsAsyncSendObj failed, AddMsg fail!", __FILE__, __LINE__);
		return FALSE;
	}
	return TRUE;
}

BOOL CTKMahjongJSPKService::OnAsyncSendToHebbroker(PTKHEADER msgReq, DWORD dwParam, BOOL bWaitAck)
{
	DWORD dwTickBegin = GetTickCount();

	CTKBuffer bufRecv;
	int nConnError = TK_CONN_ERROR_NULL;
	if (!m_clsHebbrokerPool.SendMsg(msgReq, bWaitAck, TK_ACK | msgReq->dwType, &bufRecv, &nConnError, __FILE__, __LINE__))
	{
		TKWriteLog("%s : %d, m_clsHebbrokerPool failed, SendMsg fail%d!", __FILE__, __LINE__, nConnError);
		return TRUE;
	}

	PTKHEADER pAck = (PTKHEADER)bufRecv.GetBufPtr();
	if (pAck->dwParam != TK_ACKRESULT_SUCCESS && pAck->dwType == (TK_MSG_OTHER2HEBBROKER_PUSHDATA | TK_ACK))
	{
		TKWriteLog("%s : %d, m_clsHebbrokerPool failed, AckMsg fail%d", __FILE__, __LINE__, pAck->dwParam);
	}

	int nDelt = GetTickCount() - dwTickBegin;
	if (nDelt < 0) nDelt = -nDelt;
	if (nDelt > 1000)
	{
		TKWriteLog("OnAsyncSendToHebbroker, Use Tick = %d", nDelt);
	}

	return TRUE;
}
