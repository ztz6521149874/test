/*
* Copyright (c) 2018,JJWorld
* All rights reserved.
* 
* 文件名称：TKMahjongProtobufFactory.h
* 摘    要：用于原始的C++协议与新协议protobuf 转换
* 
* 当前版本：1.0
* 作    者：wangyb01
* 完成日期：2018年1月11日
*/

#pragma once

#include "TKMahjongProtocol.h"
#include "TKProtocol.h"
#include "TKMahJongJSPK.pb.h"
#include "TKMahjongGameProxy.h"

using namespace cn::jj::service::msg::protocol;

#define PROTOLOG	TKWriteLog


// protobuf 协议转换工厂
class TKMahjongProtobufFactory
{
private:
	TKMahjongGameProxy* m_pMahjongGameProxy;		// GameProxy

public:
	TKMahjongProtobufFactory(TKMahjongGameProxy* pGameProxy);
	virtual ~TKMahjongProtobufFactory();
	
public:	// 发送组装好的protobuf消息包
	// 广播
	BOOL Broadcast(PTKHEADER pMsg);

	// 发送给observer型的旁观者
	BOOL Send2Observer(PTKHEADER pMsg);

	// 发送给玩家
	BOOL Send2SeatPlayer(int nSeat, PTKHEADER pMsg);

	// 发送给black型的旁观者
	BOOL Send2Black(PTKHEADER pMsg);

	BOOL	Send2SeatBlack(int nSeat, PTKHEADER pMsg);
	BOOL	Send2Player( DWORD dwUserID, PTKHEADER pMsg );

public:	// 收到的消息包进行转换成C++协议格式

	/************************************************************************/
	/* 把protobuf协议转换成c++协议										*/
	// 参数1 原始的 proto 协议
	// 参数2 返回的C++协议
	// 返回值 是否成功
	/************************************************************************/
	BOOL DeserializeProtobuf( TKHEADER* pMsg, TKHEADER ** ppTransMsg );

	// 获取protobuf消息体
	BOOL GetReqMsg(TKHEADER* pMsg, MahJongJSPKReqMsg& reqMsg);

private:
	/************************************************************************/
	/* 开牌结束消息 玩家抓完自己的牌后，发送此消息到服务器                  */
	/************************************************************************/
	TKHEADER* DeserializeProtoOpenDoorOver( const MahJongJSPKReqMsg& pReqMsg, const TKHEADER * pMsg);

	/************************************************************************/
	/* 玩家向服务器返回放弃的操作类型                                       */
	/************************************************************************/
	TKHEADER* DeserializeProtoRequstReq( const MahJongJSPKReqMsg& reqMsg, const TKHEADER * pMsg );

	/************************************************************************/
	/* 补花请求消息                                                         */
	/************************************************************************/
	TKHEADER* DeserializeProtoChangeFlowerReq( const MahJongJSPKReqMsg& reqMsg, const TKHEADER* pMsg );

	/************************************************************************/
	/* 出牌请求消息                                                         */
	/************************************************************************/
	TKHEADER* DeserializeProtoDiscardTileReq( const MahJongJSPKReqMsg& reqMsg, const TKHEADER* pMsg );

	/************************************************************************/
	/* 听牌请求消息                                                         */
	/************************************************************************/
	TKHEADER* DeserializeProtoCallReq( const MahJongJSPKReqMsg& reqMsg, const TKHEADER* pMsg );

	/************************************************************************/
	/* 吃牌请求消息                                                         */
	/************************************************************************/
	TKHEADER* DeserializeProtoChiReq( const MahJongJSPKReqMsg& reqMsg, const TKHEADER* pMsg );

	/************************************************************************/
	/* 碰牌请求消息                                                         */
	/************************************************************************/
	TKHEADER* DeserializeProtoPengReq( const MahJongJSPKReqMsg& reqMsg, const TKHEADER* pMsg );

	/************************************************************************/
	/* 杠牌请求消息                                                         */
	/************************************************************************/
	TKHEADER* DeserializeProtoGangReq( const MahJongJSPKReqMsg& reqMsg, const TKHEADER* pMsg );

	/************************************************************************/
	/* 和牌请求消息                                                         */
	/************************************************************************/
	TKHEADER* DeserializeProtoWinReq( const MahJongJSPKReqMsg& reqMsg, const TKHEADER* pMsg );

	/************************************************************************/
	/* 托管消息                                                             */
	/************************************************************************/
	TKHEADER* DeserializeProtoTrustPlayReq( const MahJongJSPKReqMsg& reqMsg, const TKHEADER* pMsg );

	/************************************************************************/
	/* 加倍消息                                                             */
	/************************************************************************/
	TKHEADER* DeserializeProtoDoublingReq( const MahJongJSPKReqMsg& reqMsg, const TKHEADER* pMsg );

	/************************************************************************/
	/* 超时检测消息                                                         */
	/************************************************************************/
	TKHEADER* DeserializeProtoCheckTimeOutReq( const MahJongJSPKReqMsg& reqMsg, const TKHEADER* pMsg );

	/************************************************************************/
	/* 听牌信息                                                             */
	/************************************************************************/
	TKHEADER* DeserializeProtoCallTileReq( const MahJongJSPKReqMsg& reqMsg, const TKHEADER* pMsg );

private: // 对发送出去的消息包进行封装成protobuf格式
	
	/************************************************************************/
	/* 把普通协议重新填充到proto协议中										*/
	// 参数1 原始的C++ 协议
	// 参数2 返回的proto协议，有可能与参数1 相等
	// 参数3 返回的proto协议使用之后是否要delete
	// 返回值 是否成功
	/************************************************************************/
	BOOL SerializeMsg2Protobuf( TKHEADER* pMsg, TKHEADER ** ppRetMsg, BOOL& bUseEndDel );

	// 组装ACK protobuf协议
	// 服务器向客户端发送的 MahJongJSPKAckMsg 结构体里的消息包
	void SerializeProtobufAckMsg( TKHEADER* pMsg, TKACKMAHJONGPROTOBUF& protoMsgAck );

	//组装ACK protobuf消息头
	void SerializeProtobufAckMsgHead( TKHEADER* pMsg, TKACKMAHJONGPROTOBUF& protoMsgAck );

	// 把需要发送的protobuf消息进行重组，使地址连续
	// 注：此函数返回的指针用完之后需要释放
	TKHEADER* CreateProtoAckMsgBuff(TKACKMAHJONGPROTOBUF* pAckMsg);
	
	// 为Protobuf协议填写MatchId
	void SerializeProtobufSetMatchID( MahJongJSPKAckMsg* pAckMsg );

	/************************************************************************/
	/*  游戏规则消息                                                        */
	/************************************************************************/
	void SerializeNetMsgRulerInfoAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg);

	/************************************************************************/
	/*  游戏坐位、庄家消息                                                  */
	/************************************************************************/
	void SerializeNetMsgPlaceAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg);

	/************************************************************************/
	/*  门风、圈风消息                                                      */
	/************************************************************************/
	void SerializeNetMsgWindPlaceAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg);

	/************************************************************************/
	/*  洗牌、砌牌消息                                                      */
	/************************************************************************/
	void SerializeNetMsgShuffleAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg);

	/************************************************************************/
	/*  掷骰子消息                                                          */
	/************************************************************************/
	void SerializeNetMsgCastDice(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg);

	/************************************************************************/
	/*  开牌消息                                                            */
	/************************************************************************/
	void SerializeNetMsgOpenDoorAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg);

	/************************************************************************/
	/*    通知消息                                                          */
	/************************************************************************/
	void SerializeNetMsgNotify(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg);

	/************************************************************************/
	/*    通知补花消息                                                       */
	/************************************************************************/
	void SerializeNetMsgChangeFlower(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg);

	/************************************************************************/
	/*    通知抓牌消息                                                      */
	/************************************************************************/
	void SerializeNetMsgDrawTile(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg);

	/************************************************************************/
	/*    服务器请求客户端执行操作消息									*/
	/************************************************************************/
	void SerializeNetMsgActionAck(TKACKMAHJONGPROTOBUF& protoMsgReq, TKHEADER* pMsg);

	/************************************************************************/
	/*    服务器向客户端广播其他玩家的操作消息                              */
	/************************************************************************/
	void SerializeNetMsgRequestAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg);

	/************************************************************************/
	/*    通知出牌消息                                                      */
	/************************************************************************/
	void SerializeNetMsgDiscardTile(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg);

	/************************************************************************/
	/*    结果消息，一局游戏结束时，服务器发送此消息给所有客户端			*/
	/************************************************************************/
	void SerializeNetMsgResultExAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg);

	/************************************************************************/
	/*    翻混消息			                                                 */
	/************************************************************************/
	void SerializeNetMsgHunAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg);

	/************************************************************************/
	/*    通知听牌消息			                                             */
	/************************************************************************/
	void SerializeNetMsgCallAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg );

	/************************************************************************/
	/*    通知吃牌消息			                                             */
	/************************************************************************/
	void SerializeNetMsgChiAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg);

	/************************************************************************/
	/*    通知碰牌消息			                                             */
	/************************************************************************/
	void SerializeNetMsgPengAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg);

	/************************************************************************/
	/*    通知杠牌消息	                                                   */
	/************************************************************************/
	void SerializeNetMsgGangAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg);

	/************************************************************************/
	/*    通知和牌消息	                                                   */
	/************************************************************************/
	void SerializeNetMsgWinAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg);

	/************************************************************************/
	/*    赢牌详细信息	                                                   */
	/************************************************************************/
	void SerializeNetMsgWinDetailAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg);

	/************************************************************************/
	/*    赢牌详细信息	                                                   */
	/************************************************************************/
	void SerializeNetMsgWinDetailExAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg);

	/************************************************************************/
	/*    托管消息	                                                   */
	/************************************************************************/
	void SerializeNetMsgTrustPlayAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg);

	/************************************************************************/
	/*    分数改变消息	                                                   */
	/************************************************************************/
	void SerializeNetMsgScoreChangeAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg);

	/************************************************************************/
	/*    通知亮牌消息                                                      */
	/************************************************************************/
	void SerializeNetMsgPlayerTiles(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg);

	/************************************************************************/
	/*    玩家吃碰杠的牌信息                                                 */
	/************************************************************************/
	void SerializeNetMsgShowTiles(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg);

	/************************************************************************/
	/*    加倍消息                                                      */
	/************************************************************************/
	void SerializeNetMsgDoublingAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg);

	/************************************************************************/
	/*    检查超时消息                                                      */
	/************************************************************************/
	void SerializeNetMsgCheckTimeOut(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg);

	/************************************************************************/
	/*    加番牌信息														*/
	/************************************************************************/
	void SerializeNetMsgSpecialTileAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg);

	/************************************************************************/
	/*    加番牌个数														*/
	/************************************************************************/
	void SerializeNetMsgSpecialCountAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg);

	/************************************************************************/
	/*    奖花消息														*/
	/************************************************************************/
	void SerializeNetMsgLuckyTileAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg);

	/************************************************************************/
	/*    超时检测														*/
	/************************************************************************/
	void SerializeNetMsgCheckTimeOutAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg);



};



/************************************************************************/
/* 字符串转换  GBKToUTF8                                                */
/************************************************************************/
inline std::string GBKToUTF8(const std::string& strGBK)  
{  
	std::string strOutUTF8 = "";  
	WCHAR * str1;  
	int n = MultiByteToWideChar(CP_ACP, 0, strGBK.c_str(), -1, NULL, 0);  
	str1 = new WCHAR[n];  
	MultiByteToWideChar(CP_ACP, 0, strGBK.c_str(), -1, str1, n);  
	n = WideCharToMultiByte(CP_UTF8, 0, str1, -1, NULL, 0, NULL, NULL);  
	char * str2 = new char[n];  
	WideCharToMultiByte(CP_UTF8, 0, str1, -1, str2, n, NULL, NULL);  
	strOutUTF8 = str2;  
	delete[]str1;  
	str1 = NULL;  
	delete[]str2;  
	str2 = NULL;  
	return strOutUTF8;  
}
/************************************************************************/
/* 检测一个字符串是否UTF8编码											*/
/************************************************************************/
inline bool IsStringUTF8(const char* str)
{
	unsigned int nBytes = 0;//UFT8可用1-6个字节编码,ASCII用一个字节  
	unsigned char chr = *str;
	bool bAllAscii = true;

	for (unsigned int i = 0; str[i] != '\0'; ++i){
		chr = *(str + i);
		//判断是否ASCII编码,如果不是,说明有可能是UTF8,ASCII用7位编码,最高位标记为0,0xxxxxxx 
		if (nBytes == 0 && (chr & 0x80) != 0){
			bAllAscii = false;
		}

		if (nBytes == 0) {
			//如果不是ASCII码,应该是多字节符,计算字节数  
			if (chr >= 0x80) {

				if (chr >= 0xFC && chr <= 0xFD){
					nBytes = 6;
				}
				else if (chr >= 0xF8){
					nBytes = 5;
				}
				else if (chr >= 0xF0){
					nBytes = 4;
				}
				else if (chr >= 0xE0){
					nBytes = 3;
				}
				else if (chr >= 0xC0){
					nBytes = 2;
				}
				else{
					return false;
				}

				nBytes--;
			}
		}
		else{
			//多字节符的非首字节,应为 10xxxxxx 
			if ((chr & 0xC0) != 0x80){
				return false;
			}
			//减到为零为止
			nBytes--;
		}
	}

	//违返UTF8编码规则 
	if (nBytes != 0)  {
		return false;
	}

	if (bAllAscii){ //如果全部都是ASCII, 也是UTF8
		return true;
	}

	return true;
}


/************************************************************************/
/* 检测一个 字符串是否是GBK编码                                          */
/************************************************************************/
inline bool IsStringGBK(const char* str)
{
	unsigned int nBytes = 0;//GBK可用1-2个字节编码,中文两个 ,英文一个 
	unsigned char chr = *str;
	bool bAllAscii = true; //如果全部都是ASCII,  

	for (unsigned int i = 0; str[i] != '\0'; ++i){
		chr = *(str + i);
		if ((chr & 0x80) != 0 && nBytes == 0){// 判断是否ASCII编码,如果不是,说明有可能是GBK
			bAllAscii = false;
		}

		if (nBytes == 0) {
			if (chr >= 0x80) {
				if (chr >= 0x81 && chr <= 0xFE){
					nBytes = +2;
				}
				else{
					return false;
				}

				nBytes--;
			}
		}
		else{
			if (chr < 0x40 || chr>0xFE){
				return false;
			}
			nBytes--;
		}//else end
	}

	if (nBytes != 0)  {		//违返规则 
		return false;
	}

	if (bAllAscii){ //如果全部都是ASCII, 也是GBK
		return true;
	}

	return true;
}