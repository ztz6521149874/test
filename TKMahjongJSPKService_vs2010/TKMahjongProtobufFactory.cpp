#include "stdafx.h"
#include "TKMahjongProtobufFactory.h"

#ifndef ASSERT
#define ASSERT	assert
#endif

TKMahjongProtobufFactory::TKMahjongProtobufFactory(TKMahjongGameProxy* pGameProxy)
	:m_pMahjongGameProxy(pGameProxy)
{
}

// 析构函数
TKMahjongProtobufFactory::~TKMahjongProtobufFactory()
{
}

// 获取protobuf消息体
BOOL TKMahjongProtobufFactory::GetReqMsg(TKHEADER* pMsg, MahJongJSPKReqMsg& reqMsg)
{
	int nMsgBodySize = pMsg->dwLength;

	BYTE* tempMsg = ((BYTE*)pMsg) + TKHEADERSIZE;

	TKMobileReqMsg tkMobileReqMsg;

	if (!tkMobileReqMsg.ParseFromArray(tempMsg, nMsgBodySize))
	{
		PROTOLOG("Error !!! ProtobufFactory::GetReqMsg parse failed!");
		return FALSE;
	}

	if ( tkMobileReqMsg.has_mahjongjspk_req_msg() )
	{
		reqMsg.CopyFrom(tkMobileReqMsg.mahjongjspk_req_msg());
		return TRUE;
	}

	return FALSE;
}


//组装protobuf消息头
void TKMahjongProtobufFactory::SerializeProtobufAckMsgHead(TKHEADER* pMsg, TKACKMAHJONGPROTOBUF& protoMsgAck)
{
	protoMsgAck.header.dwType = TK_ACK | TK_MSG_MAHJONG_PROTOBUF;
	protoMsgAck.header.dwMagic = pMsg->dwMagic;
	protoMsgAck.header.dwParam = pMsg->dwParam;
	protoMsgAck.header.dwSerial = pMsg->dwSerial;
	protoMsgAck.header.wOrigine = pMsg->wOrigine;
	protoMsgAck.header.wReserve = pMsg->wReserve;
}


// 分配空间 保证发出去的消息包 地址空间连续
TKHEADER* TKMahjongProtobufFactory::CreateProtoAckMsgBuff(TKACKMAHJONGPROTOBUF* pAckMsg)
{
	// 分配空间 保证发出去的消息包 地址空间连续
	BYTE* pszPack = new BYTE[sizeof(TKHEADER) + pAckMsg->protoAckMsg.ByteSize()];
	ASSERT(NULL != pszPack);

	ZeroMemory(pszPack, sizeof(TKHEADER) + pAckMsg->protoAckMsg.ByteSize());

	// 向buf复制消息头
	BYTE* pData = pszPack;	
	memcpy(pData, &pAckMsg->header, sizeof(TKHEADER));

	// 向buf复制消息体
	pData += sizeof(TKHEADER);

	if ( !pAckMsg->protoAckMsg.SerializeToArray(pData, pAckMsg->protoAckMsg.ByteSize()) )
	{
		ASSERT(false);
		TKWriteLog( "%s : %d, Error! Protobuf() SerializeToString 错误 ", __FILE__, __LINE__);

		if (pszPack)
		{
			delete[] pszPack;
			pszPack = NULL;
		}

		return NULL;
	}

	return (PTKHEADER)pszPack;
}



/************************************************************************/
/* 把普通协议重新填充到proto协议中										*/
// 参数1 原始的C++ 协议
// 参数2 返回的proto协议，有可能与参数1 相等
// 参数3 返回的proto协议使用之后是否要delete
// 返回值 是否成功
/************************************************************************/
BOOL TKMahjongProtobufFactory::SerializeMsg2Protobuf( TKHEADER* pMsg, TKHEADER** ppRetMsg, BOOL& bUseEndDel )
{
	TKACKMAHJONGPROTOBUF protoMsgAck = { 0 };

	// 默认发送原始的数据包
	*ppRetMsg = pMsg;
	bUseEndDel = FALSE;

	// 对原始数据包进行协议改造
	// 注：若原始消息包只有消息头，则不会进行处理

	// 序列化 AckMsg
	SerializeProtobufAckMsg( pMsg, protoMsgAck );

	// 检测一下 protoMsgAck 是否被赋值过
	// 如果是原来消息只有消息头 则不会被赋值成 REQ_TYPE( TK_MSG_MAHJONG_PROTOBUF )
	if ( protoMsgAck.header.dwType == ACK_TYPE( TK_MSG_MAHJONG_PROTOBUF ) )
	{
		// 分配空间 保证发出去的消息包 地址空间连续
		*ppRetMsg = CreateProtoAckMsgBuff( &protoMsgAck );
		if (*ppRetMsg == NULL)
		{
			TKWriteLog( "%s : %d, Error! Protobuf() 分配空间 ", __FILE__, __LINE__);
			return FALSE;
		}

		bUseEndDel = TRUE;
	}
	return TRUE;
}


// 发送组装好的protobuf消息包
BOOL TKMahjongProtobufFactory::Broadcast(TKHEADER* pMsg)
{
	PTKHEADER pszPack = NULL;
	BOOL bUseEndDel = FALSE;

	// 填充协议
	if ( !SerializeMsg2Protobuf( pMsg, &pszPack, bUseEndDel ) )
	{
		TKWriteLog( "%s : %d, Error! FillProtoMsg 填充协议错误 ", __FILE__, __LINE__);
		return FALSE;
	}

	if (pszPack == NULL)
	{
		TKWriteLog( "%s : %d, Error! Protobuf() 分配空间 ", __FILE__, __LINE__);
		return FALSE;
	}

	// 发送消息
	if ( !m_pMahjongGameProxy->Broadcast(pszPack) )
	{
		ASSERT(FALSE);
		TKWriteLog( "%s : %d, Error! Protobuf() 发送消息出错 len( %d ) ", __FILE__, __LINE__, pszPack->dwLength );

		if ( pszPack &&  bUseEndDel )
		{
			delete[] pszPack;
			pszPack = NULL;
		}

		return FALSE;
	}

	if ( pszPack && bUseEndDel )
	{
		delete[] pszPack;
		pszPack = NULL;
	}

	return TRUE;
}


// 发送给observer型的旁观者
BOOL TKMahjongProtobufFactory::Send2Observer(TKHEADER* pMsg)
{
	PTKHEADER pszPack = NULL;
	BOOL bUseEndDel = FALSE;

	// 填充协议
	if ( !SerializeMsg2Protobuf( pMsg, &pszPack, bUseEndDel ) )
	{
		TKWriteLog( "%s : %d, Error! FillProtoMsg 填充协议错误 ", __FILE__, __LINE__);
		return FALSE;
	}

	if (pszPack == NULL)
	{
		TKWriteLog( "%s : %d, Error! Protobuf() 分配空间 ", __FILE__, __LINE__);
		return FALSE;
	}

	// 发送消息
	if ( !m_pMahjongGameProxy->Send2Observer(pszPack) )
	{
		ASSERT(FALSE);
		TKWriteLog( "%s : %d, Error! Protobuf() 发送消息出错 len( %d ) ", __FILE__, __LINE__, pszPack->dwLength );

		if (pszPack && bUseEndDel)
		{
			delete[] pszPack;
			pszPack = NULL;
		}

		return FALSE;
	}

	if (pszPack && bUseEndDel)
	{
		delete[] pszPack;
		pszPack = NULL;
	}

	return TRUE;
}

// 发送给玩家
BOOL TKMahjongProtobufFactory::Send2SeatPlayer(int nSeat, TKHEADER* pMsg)
{
	PTKHEADER pszPack = NULL;
	BOOL bUseEndDel = FALSE;

	// 填充协议
	if ( !SerializeMsg2Protobuf( pMsg, &pszPack, bUseEndDel ) )
	{
		TKWriteLog( "%s : %d, Error! FillProtoMsg 填充协议错误 ", __FILE__, __LINE__);
		return FALSE;
	}

	if (pszPack == NULL)
	{
		TKWriteLog( "%s : %d, Error! Protobuf() 分配空间 ", __FILE__, __LINE__);
		return FALSE;
	}

	// 发送消息
	if ( !m_pMahjongGameProxy->Send2SeatPlayer(nSeat, pszPack) )
	{
		ASSERT(FALSE);
		TKWriteLog( "%s : %d, Error! Protobuf() 发送消息出错 len( %d ) ", __FILE__, __LINE__, pszPack->dwLength );

		if (pszPack && bUseEndDel)
		{
			delete[] pszPack;
			pszPack = NULL;
		}

		return FALSE;
	}

	if (pszPack && bUseEndDel)
	{
		delete[] pszPack;
		pszPack = NULL;
	}

	return TRUE;
}

// 发送给black型的旁观者
BOOL TKMahjongProtobufFactory::Send2Black(TKHEADER* pMsg)
{
	PTKHEADER pszPack = NULL;
	BOOL bUseEndDel = FALSE;

	// 填充协议
	if ( !SerializeMsg2Protobuf( pMsg, &pszPack, bUseEndDel ) )
	{
		TKWriteLog( "%s : %d, Error! FillProtoMsg 填充协议错误 ", __FILE__, __LINE__);
		return FALSE;
	}

	if (pszPack == NULL)
	{
		TKWriteLog( "%s : %d, Error! Protobuf() 分配空间 ", __FILE__, __LINE__);
		return FALSE;
	}

	// 发送消息
	if ( !m_pMahjongGameProxy->Send2Black(pszPack) )
	{
		ASSERT(FALSE);
		TKWriteLog( "%s : %d, Error! Protobuf() 发送消息出错 len( %d ) ", __FILE__, __LINE__, pszPack->dwLength );

		if (pszPack && bUseEndDel)
		{
			delete[] pszPack;
			pszPack = NULL;
		}

		return FALSE;
	}

	if (pszPack && bUseEndDel)
	{
		delete[] pszPack;
		pszPack = NULL;
	}

	return TRUE;
}

BOOL	TKMahjongProtobufFactory::Send2SeatBlack(int nSeat, PTKHEADER pMsg)
{
	PTKHEADER pszPack = NULL;
	BOOL bUseEndDel = FALSE;

	// 填充协议
	if ( !SerializeMsg2Protobuf( pMsg, &pszPack, bUseEndDel ) )
	{
		TKWriteLog( "%s : %d, Error! FillProtoMsg 填充协议错误 ", __FILE__, __LINE__);
		return FALSE;
	}

	if (pszPack == NULL)
	{
		TKWriteLog( "%s : %d, Error! Protobuf() 分配空间 ", __FILE__, __LINE__);
		return FALSE;
	}

	// 发送消息
	if ( !m_pMahjongGameProxy->Send2SeatBlack(nSeat, pszPack) )
	{
		ASSERT(FALSE);
		TKWriteLog( "%s : %d, Error! Protobuf() 发送消息出错 len( %d ) ", __FILE__, __LINE__, pszPack->dwLength );

		if (pszPack && bUseEndDel)
		{
			delete[] pszPack;
			pszPack = NULL;
		}

		return FALSE;
	}

	if (pszPack && bUseEndDel)
	{
		delete[] pszPack;
		pszPack = NULL;
	}

	return TRUE;
}
BOOL	TKMahjongProtobufFactory::Send2Player( DWORD dwUserID, PTKHEADER pMsg )
{
	PTKHEADER pszPack = NULL;
	BOOL bUseEndDel = FALSE;

	// 填充协议
	if ( !SerializeMsg2Protobuf( pMsg, &pszPack, bUseEndDel ) )
	{
		TKWriteLog( "%s : %d, Error! FillProtoMsg 填充协议错误 ", __FILE__, __LINE__);
		return FALSE;
	}

	if (pszPack == NULL)
	{
		TKWriteLog( "%s : %d, Error! Protobuf() 分配空间 ", __FILE__, __LINE__);
		return FALSE;
	}

	// 发送消息
	if ( !m_pMahjongGameProxy->Send2Player(dwUserID, pszPack) )
	{
		ASSERT(FALSE);
		TKWriteLog( "%s : %d, Error! Protobuf() 发送消息出错 len( %d ) ", __FILE__, __LINE__, pszPack->dwLength );

		if (pszPack && bUseEndDel)
		{
			delete[] pszPack;
			pszPack = NULL;
		}

		return FALSE;
	}

	if (pszPack && bUseEndDel)
	{
		delete[] pszPack;
		pszPack = NULL;
	}

	return TRUE;
}

// ACK 组装protobuf协议
// 以下是服务器向客户端发送的 MahJongJSPKAckMsg 结构体里的消息包
void TKMahjongProtobufFactory::SerializeProtobufAckMsg( TKHEADER* pMsg, TKACKMAHJONGPROTOBUF& protoMsgAck )
{
	switch (pMsg->dwType)
	{
	case TK_REQ | TK_MSG_MAHJONG_GAME_BEGIN:

		// 只发消息头 没有消息体
		// 什么都不用做
		break;

	case TK_ACK | TK_MSG_GAME_RULERINFO:
		/************************************************************************/
		/*  游戏规则消息                                                        */
		/************************************************************************/
		SerializeNetMsgRulerInfoAck(protoMsgAck, pMsg);
		break;

	case TK_REQ | TK_MSG_MAHJONG_PLACE:
		/************************************************************************/
		/*  游戏坐位、庄家消息                                                  */
		/************************************************************************/
		SerializeNetMsgPlaceAck(protoMsgAck, pMsg);
		break;

	case TK_REQ | TK_MSG_MAHJONG_WIND_PLACE:
		/************************************************************************/
		/*  门风、圈风消息                                                      */
		/************************************************************************/
		SerializeNetMsgWindPlaceAck(protoMsgAck, pMsg);
		break;

	case TK_REQ | TK_MSG_MAHJONG_SHUFFLE:
		/************************************************************************/
		/*  洗牌、砌牌消息                                                      */
		/************************************************************************/
		SerializeNetMsgShuffleAck(protoMsgAck, pMsg);
		break;

	case TK_REQ | TK_MSG_MAHJONG_CAST_DICE:
		/************************************************************************/
		/*  掷骰子消息                                                          */
		/************************************************************************/
		SerializeNetMsgCastDice(protoMsgAck, pMsg);
		break;

	case TK_REQ | TK_MSG_MAHJONG_OPEN_DOOR:
		/************************************************************************/
		/*  开牌消息                                                            */
		/************************************************************************/
		SerializeNetMsgOpenDoorAck(protoMsgAck, pMsg);
		break;

	case TK_REQ | TK_MSG_MAHJONG_NOTIFY:
		/************************************************************************/
		/*    通知消息                                                          */
		/************************************************************************/
		SerializeNetMsgNotify(protoMsgAck, pMsg);
		break;

	case TK_REQ | TK_MSG_MAHJONG_CHANGE_FLOWER:
		/************************************************************************/
		/*    通知补花消息                                                       */
		/************************************************************************/
		SerializeNetMsgChangeFlower(protoMsgAck, pMsg);
		break;

	case TK_REQ |TK_MSG_MAHJONG_DRAW_TILE:
		/************************************************************************/
		/*    通知抓牌消息                                                      */
		/************************************************************************/
		SerializeNetMsgDrawTile(protoMsgAck, pMsg);
		break;

	case TK_REQ | TK_MSG_MAHJONG_DISCARD_TILE:
		/************************************************************************/
		/*    通知出牌消息                                                      */
		/************************************************************************/
		SerializeNetMsgDiscardTile(protoMsgAck, pMsg);
		break;

	case TK_REQ | TK_MSG_MAHJONG_RESULT:
	case TK_REQ | TK_MSG_MAHJONG_RESULTEX:
		/************************************************************************/
		/*    结果消息，一局游戏结束时，服务器发送此消息给所有客户端			*/
		/************************************************************************/
		SerializeNetMsgResultExAck( protoMsgAck, pMsg );
		break;

	case TK_REQ | TK_MSG_MAHJONG_HUN:
		/************************************************************************/
		/*    翻混消息			                                                 */
		/************************************************************************/
		SerializeNetMsgHunAck( protoMsgAck, pMsg );
		break;

	case TK_REQ | TK_MSG_MAHJONG_CALL:
		/************************************************************************/
		/*    通知听牌消息			                                             */
		/************************************************************************/
		SerializeNetMsgCallAck( protoMsgAck, pMsg );
		break;

	case TK_REQ | TK_MSG_MAHJONG_CHI:
		/************************************************************************/
		/*    通知吃牌消息			                                             */
		/************************************************************************/
		SerializeNetMsgChiAck( protoMsgAck, pMsg );
		break;

	case TK_REQ | TK_MSG_MAHJONG_PENG:
		/************************************************************************/
		/*    通知碰牌消息			                                             */
		/************************************************************************/
		SerializeNetMsgPengAck( protoMsgAck, pMsg );
		break;

	case TK_REQ| TK_MSG_MAHJONG_GANG:
		/************************************************************************/
		/*    通知杠牌消息	                                                   */
		/************************************************************************/
		SerializeNetMsgGangAck( protoMsgAck, pMsg );
		break;

	case TK_ACK | TK_MSG_MAHJONG_WIN:
		/************************************************************************/
		/*    通知和牌消息	                                                   */
		/************************************************************************/
		SerializeNetMsgWinAck( protoMsgAck, pMsg );
		break;

	case TK_ACK | TK_MSG_MAHJONG_WINDETAIL:
		/************************************************************************/
		/*    赢牌详细信息	                                                   */
		/************************************************************************/
		SerializeNetMsgWinDetailAck( protoMsgAck, pMsg );
		break;

	case TK_ACK | TK_MSG_MAHJONG_WINDETAILEX:
		/************************************************************************/
		/*    赢牌详细信息	                                                   */
		/************************************************************************/
		SerializeNetMsgWinDetailExAck( protoMsgAck, pMsg );
		break;

	case TK_REQ | TK_MSG_MAHJONG_REQUEST:
		/************************************************************************/
		/*    服务器请求客户端执行操作消息                              */
		/************************************************************************/
		SerializeNetMsgActionAck( protoMsgAck, pMsg );
		break;

	case TK_ACK | TK_MSG_MAHJONG_REQUEST:
		/************************************************************************/
		/*    服务器向客户端广播其他玩家的操作消息                              */
		/************************************************************************/
		SerializeNetMsgRequestAck( protoMsgAck, pMsg );
		break;

	case TK_REQ | TK_MSG_TRUSTPLAY:
		/************************************************************************/
		/*    托管消息	                                                   */
		/************************************************************************/
		SerializeNetMsgTrustPlayAck( protoMsgAck, pMsg );
		break;

	case TK_REQ | TK_MSG_SCORE_CHANGE:
		/************************************************************************/
		/*    分数改变消息	                                                   */
		/************************************************************************/
		SerializeNetMsgScoreChangeAck( protoMsgAck, pMsg );
		break;

	case TK_ACK | TK_MSG_PLAYER_TILES:
		/************************************************************************/
		/*    通知亮牌消息                                                      */
		/************************************************************************/
		SerializeNetMsgPlayerTiles(protoMsgAck, pMsg);
		break;

	case TK_ACK | TK_MSG_PLAYER_SHOWTILES:
		/************************************************************************/
		/*    玩家吃碰杠的牌信息                                                      */
		/************************************************************************/
		SerializeNetMsgShowTiles(protoMsgAck, pMsg);
		break;

	case TK_ACK | TK_MSG_DOUBLING:
		/************************************************************************/
		/*    加倍消息                                                      */
		/************************************************************************/
		SerializeNetMsgDoublingAck(protoMsgAck, pMsg);
		break;

	case TK_ACK | TK_MSG_MAHJONG_SPECIALTILE:
		/************************************************************************/
		/*    加番牌信息														*/
		/************************************************************************/
		SerializeNetMsgSpecialTileAck(protoMsgAck, pMsg);
		break;

	case TK_ACK | TK_MSG_MAHJONG_SPECIALCOUNT:
		/************************************************************************/
		/*    加番牌个数														*/
		/************************************************************************/
		SerializeNetMsgSpecialCountAck( protoMsgAck, pMsg );
		break;

	case TK_ACK | TK_MSG_MAHJONG_LUCKYTILE:
		/************************************************************************/
		/*    奖花消息														*/
		/************************************************************************/
		SerializeNetMsgLuckyTileAck( protoMsgAck, pMsg );
		break;

	case TK_REQ | TK_MSG_CHECKTIMEOUT:
		/************************************************************************/
		/*    超时检测														*/
		/************************************************************************/
		SerializeNetMsgCheckTimeOutAck( protoMsgAck, pMsg );
		break;


	default:
		break;
	}
}


/************************************************************************/
/* 把protobuf协议转换成c++协议										*/
// 参数1 原始的 proto 协议
// 参数2 返回的C++协议
// 返回值 是否成功
/************************************************************************/
BOOL TKMahjongProtobufFactory::DeserializeProtobuf( TKHEADER* pMsg, TKHEADER ** ppTransMsg )
{
	if (pMsg == nullptr)
	{
		PROTOLOG("ProtobufFactory::DeserializeProtobuf the msg is NULL, return");
		return FALSE;
	}

	if (pMsg->dwType != (TK_MSG_MAHJONG_PROTOBUF | TK_REQ))
	{
		PROTOLOG("ProtobufFactory::DeserializeProtobuf the msg type is not TK_MSG_MAHJONG_PROTOBUF");
		return FALSE;
	}

	*ppTransMsg = nullptr;

	MahJongJSPKReqMsg reqMsg;
	reqMsg.Clear();

	if ( !GetReqMsg( pMsg, reqMsg ) )
	{
		PROTOLOG("ProtobufFactory::DeserializeProtobuf GetAckMsg Error!!!");
		return FALSE;
	}

	if ( reqMsg.has_mahjongopendoorover_req_msg() )
	{
		//PROTOLOG("ProtobufFactory::DeserializeProtobuf mahjongopendoorover_req_msg ");
		//开牌结束消息 玩家抓完自己的牌后，发送此消息到服务器
		*ppTransMsg = DeserializeProtoOpenDoorOver( reqMsg, pMsg );
	}
	else if ( reqMsg.has_mahjongrequst_req_msg() )
	{
		//PROTOLOG("ProtobufFactory::DeserializeProtobuf mahjongrequst_req_msg ");
		//玩家向服务器返回放弃的操作类型
		*ppTransMsg = DeserializeProtoRequstReq( reqMsg, pMsg );
	}
	else if ( reqMsg.has_mahjongchangeflower_req_msg() )
	{
		//PROTOLOG("ProtobufFactory::DeserializeProtobuf mahjongchangeflower_req_msg ");
		//补花请求消息
		*ppTransMsg = DeserializeProtoChangeFlowerReq( reqMsg, pMsg );
	}
	else if ( reqMsg.has_mahjongdiscardtile_req_msg() )
	{
		//PROTOLOG("ProtobufFactory::DeserializeProtobuf mahjongdiscardtile_req_msg ");
		//出牌请求消息
		*ppTransMsg = DeserializeProtoDiscardTileReq( reqMsg, pMsg );
	}
	else if ( reqMsg.has_mahjongcall_req_msg() )
	{
		//PROTOLOG("ProtobufFactory::DeserializeProtobuf mahjongcall_req_msg ");
		//听牌请求消息
		*ppTransMsg = DeserializeProtoCallReq( reqMsg, pMsg );
	}
	else if ( reqMsg.has_mahjongchi_req_msg() )
	{
		//PROTOLOG("ProtobufFactory::DeserializeProtobuf mahjongchi_req_msg ");
		//吃牌请求消息
		*ppTransMsg = DeserializeProtoChiReq( reqMsg, pMsg );
	}
	else if ( reqMsg.has_mahjongpeng_req_msg() )
	{
		//PROTOLOG("ProtobufFactory::DeserializeProtobuf mahjongpeng_req_msg ");
		//碰牌请求消息
		*ppTransMsg = DeserializeProtoPengReq( reqMsg, pMsg );
	}
	else if ( reqMsg.has_mahjonggang_req_msg() )
	{
		//PROTOLOG("ProtobufFactory::DeserializeProtobuf mahjonggang_req_msg ");
		//杠牌请求消息
		*ppTransMsg = DeserializeProtoGangReq( reqMsg, pMsg );
	}
	else if ( reqMsg.has_mahjongwin_req_msg() )
	{
		//PROTOLOG("ProtobufFactory::DeserializeProtobuf mahjongwin_req_msg ");
		//和牌请求消息
		*ppTransMsg = DeserializeProtoWinReq( reqMsg, pMsg );
	}
	else if ( reqMsg.has_mahjongtrustplay_req_msg() )
	{
		//PROTOLOG("ProtobufFactory::DeserializeProtobuf mahjongtrustplay_req_msg ");
		//托管消息
		*ppTransMsg = DeserializeProtoTrustPlayReq( reqMsg, pMsg );
	}
	else if ( reqMsg.has_mahjongdouble_req_msg() )
	{
		//PROTOLOG("ProtobufFactory::DeserializeProtobuf mahjongdouble_req_msg ");
		//加倍消息
		*ppTransMsg = DeserializeProtoDoublingReq( reqMsg, pMsg );
	}
	else if ( reqMsg.has_mahjongchecktimeout_req_msg() )
	{
		//PROTOLOG("ProtobufFactory::DeserializeProtobuf mahjongchecktimeout_req_msg ");
		//超时检测消息
		*ppTransMsg = DeserializeProtoCheckTimeOutReq( reqMsg, pMsg );
	}
	else if ( reqMsg.has_mahjongcalltile_req_msg() )
	{
		//PROTOLOG("ProtobufFactory::DeserializeProtobuf mahjongcalltile_req_msg ");
		//听牌信息
		*ppTransMsg = DeserializeProtoCallTileReq( reqMsg, pMsg );
	}

	if ( *ppTransMsg == NULL )
	{
		return FALSE;
	}

	return TRUE;
}

/************************************************************************/
/* 开牌结束消息 玩家抓完自己的牌后，发送此消息到服务器                  */
/************************************************************************/
TKHEADER* TKMahjongProtobufFactory::DeserializeProtoOpenDoorOver(const MahJongJSPKReqMsg& reqMsg, const TKHEADER* pMsg)
{
	if ( !reqMsg.has_mahjongopendoorover_req_msg() )
	{
		return NULL;
	}

	TKACKMAHJONGOPENDOOR *pOpenDoorAck = new TKACKMAHJONGOPENDOOR();
	TKVerifyMemoryAlloc( pOpenDoorAck, __FILE__, __LINE__ );
	memset( pOpenDoorAck, 0, sizeof(TKACKMAHJONGOPENDOOR) );

	pOpenDoorAck->header.dwType   = ACK_TYPE( TK_MSG_MAHJONG_OPEN_DOOR );
	pOpenDoorAck->header.dwLength = MSG_LENGTH( TKACKMAHJONGOPENDOOR ) ;
	pOpenDoorAck->header.dwMagic = pMsg->dwMagic;
	pOpenDoorAck->header.dwParam = pMsg->dwParam;
	pOpenDoorAck->header.dwSerial = pMsg->dwSerial;
	pOpenDoorAck->header.wOrigine = pMsg->wOrigine;
	pOpenDoorAck->header.wReserve = pMsg->wReserve;

	const MahJongOpenDoorOverReq sProtoMsg = reqMsg.mahjongopendoorover_req_msg();

	pOpenDoorAck->nSeat = sProtoMsg.seat();

	return (PTKHEADER)pOpenDoorAck;
}

/************************************************************************/
/* 玩家向服务器返回放弃的操作类型                                       */
/************************************************************************/
TKHEADER* TKMahjongProtobufFactory::DeserializeProtoRequstReq(const MahJongJSPKReqMsg& reqMsg, const TKHEADER * pMsg )
{
	if ( !reqMsg.has_mahjongrequst_req_msg() )
	{
		return NULL;
	}

	TKACKMAHJONGREQUEST *pRequstReq = new TKACKMAHJONGREQUEST();
	TKVerifyMemoryAlloc( pRequstReq, __FILE__, __LINE__ );
	memset( pRequstReq, 0, sizeof(TKACKMAHJONGREQUEST) );

	pRequstReq->header.dwType   = ACK_TYPE( TK_MSG_MAHJONG_REQUEST );
	pRequstReq->header.dwLength = MSG_LENGTH( TKACKMAHJONGREQUEST ) ;
	pRequstReq->header.dwMagic = pMsg->dwMagic;
	pRequstReq->header.dwParam = pMsg->dwParam;
	pRequstReq->header.dwSerial = pMsg->dwSerial;
	pRequstReq->header.wOrigine = pMsg->wOrigine;
	pRequstReq->header.wReserve = pMsg->wReserve;

	const MahJongRequestReq sProtoMsg = reqMsg.mahjongrequst_req_msg();

	pRequstReq->nSeat            = sProtoMsg.seat();
	pRequstReq->nRequestID       = sProtoMsg.requestid();
	pRequstReq->nGiveUpRequestType = sProtoMsg.giveuprequesttype();

	return (PTKHEADER)pRequstReq;
}

/************************************************************************/
/* 补花请求消息                                                         */
/************************************************************************/
TKHEADER* TKMahjongProtobufFactory::DeserializeProtoChangeFlowerReq(const MahJongJSPKReqMsg& reqMsg, const TKHEADER* pMsg )
{
	if ( !reqMsg.has_mahjongchangeflower_req_msg() )
	{
		return NULL;
	}

	TKREQMAHJONGCHANGEFLOWER *pChangeFlowerReq = new TKREQMAHJONGCHANGEFLOWER();
	TKVerifyMemoryAlloc( pChangeFlowerReq, __FILE__, __LINE__ );
	memset( pChangeFlowerReq, 0, sizeof(TKREQMAHJONGCHANGEFLOWER) );

	pChangeFlowerReq->header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_CHANGE_FLOWER );
	pChangeFlowerReq->header.dwLength = MSG_LENGTH( TKREQMAHJONGCHANGEFLOWER ) ;
	pChangeFlowerReq->header.dwMagic = pMsg->dwMagic;
	pChangeFlowerReq->header.dwParam = pMsg->dwParam;
	pChangeFlowerReq->header.dwSerial = pMsg->dwSerial;
	pChangeFlowerReq->header.wOrigine = pMsg->wOrigine;
	pChangeFlowerReq->header.wReserve = pMsg->wReserve;

	const MahJongChangeFlowerReq sProtoMsg = reqMsg.mahjongchangeflower_req_msg();

	pChangeFlowerReq->nSeat = sProtoMsg.seat();
	if ( sProtoMsg.has_requestid() )
	{
		pChangeFlowerReq->nRequestID = sProtoMsg.requestid();
	}
	pChangeFlowerReq->nTileID = sProtoMsg.tileid();

	return (PTKHEADER)pChangeFlowerReq;
}

/************************************************************************/
/* 出牌请求消息                                                         */
/************************************************************************/
TKHEADER* TKMahjongProtobufFactory::DeserializeProtoDiscardTileReq( const MahJongJSPKReqMsg& reqMsg, const TKHEADER* pMsg )
{
	if ( !reqMsg.has_mahjongdiscardtile_req_msg() )
	{
		return NULL;
	}

	TKREQMAHJONGDISCARDTILE *pDiscardTileReq = new TKREQMAHJONGDISCARDTILE();
	TKVerifyMemoryAlloc( pDiscardTileReq, __FILE__, __LINE__ );
	memset( pDiscardTileReq, 0, sizeof(TKREQMAHJONGDISCARDTILE) );

	pDiscardTileReq->header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_DISCARD_TILE );
	pDiscardTileReq->header.dwLength = MSG_LENGTH( TKREQMAHJONGDISCARDTILE ) ;
	pDiscardTileReq->header.dwMagic = pMsg->dwMagic;
	pDiscardTileReq->header.dwParam = pMsg->dwParam;
	pDiscardTileReq->header.dwSerial = pMsg->dwSerial;
	pDiscardTileReq->header.wOrigine = pMsg->wOrigine;
	pDiscardTileReq->header.wReserve = pMsg->wReserve;

	const MahJongDiscardTileReq sProtoMsg = reqMsg.mahjongdiscardtile_req_msg();

	pDiscardTileReq->nSeat            = sProtoMsg.seat();
	if ( sProtoMsg.has_requestid() )
	{
		pDiscardTileReq->nRequestID   = sProtoMsg.requestid();
	}
	pDiscardTileReq->nTileID          = sProtoMsg.tileid();
	if ( sProtoMsg.has_hide() )
	{
		pDiscardTileReq->bHide        = sProtoMsg.hide();
	}

	return (PTKHEADER)pDiscardTileReq;
}

/************************************************************************/
/* 听牌请求消息                                                         */
/************************************************************************/
TKHEADER* TKMahjongProtobufFactory::DeserializeProtoCallReq( const MahJongJSPKReqMsg& reqMsg, const TKHEADER* pMsg )
{
	if ( !reqMsg.has_mahjongcall_req_msg() )
	{
		return NULL;
	}

	TKREQMAHJONGCALL *pCallReq = new TKREQMAHJONGCALL();
	TKVerifyMemoryAlloc( pCallReq, __FILE__, __LINE__ );
	memset( pCallReq, 0, sizeof(TKREQMAHJONGCALL) );

	pCallReq->header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_CALL );
	pCallReq->header.dwLength = MSG_LENGTH( TKREQMAHJONGCALL ) ;
	pCallReq->header.dwMagic = pMsg->dwLength;
	pCallReq->header.dwParam = pMsg->dwParam;
	pCallReq->header.dwSerial = pMsg->dwSerial;
	pCallReq->header.wOrigine = pMsg->wOrigine;
	pCallReq->header.wReserve = pMsg->wReserve;

	const MahJongCallReq sProtoMsg = reqMsg.mahjongcall_req_msg();

	if ( sProtoMsg.has_seat() )
	{
		pCallReq->nSeat            = sProtoMsg.seat();
	}
	if ( sProtoMsg.has_requestid() )
	{
		pCallReq->nRequestID       = sProtoMsg.requestid();
	}
	pCallReq->nTileID          = sProtoMsg.tileid();
	pCallReq->bWinSelf         = sProtoMsg.winself();
	pCallReq->bAutoGang        = sProtoMsg.autogang();
	if ( sProtoMsg.has_calltype() )
	{
		pCallReq->nCallType        = sProtoMsg.calltype();
	}
	if ( sProtoMsg.has_hide() )
	{
		pCallReq->bHide            = sProtoMsg.hide();
	}

	return (PTKHEADER)pCallReq;
}

/************************************************************************/
/* 吃牌请求消息                                                         */
/************************************************************************/
TKHEADER* TKMahjongProtobufFactory::DeserializeProtoChiReq( const MahJongJSPKReqMsg& reqMsg, const TKHEADER* pMsg )
{
	if ( !reqMsg.has_mahjongchi_req_msg() )
	{
		return NULL;
	}

	TKREQMAHJONGCHI *pChiReq = new TKREQMAHJONGCHI();
	TKVerifyMemoryAlloc( pChiReq, __FILE__, __LINE__ );
	memset( pChiReq, 0, sizeof(TKREQMAHJONGCHI) );

	pChiReq->header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_CHI );
	pChiReq->header.dwLength = MSG_LENGTH( TKREQMAHJONGCHI ) ;
	pChiReq->header.dwMagic = pMsg->dwMagic;
	pChiReq->header.dwParam = pMsg->dwParam;
	pChiReq->header.dwSerial = pMsg->dwSerial;
	pChiReq->header.wOrigine = pMsg->wOrigine;
	pChiReq->header.wReserve = pMsg->wReserve;

	const MahJongChiReq sProtoMsg = reqMsg.mahjongchi_req_msg();

	if ( sProtoMsg.has_seat() )
	{
		pChiReq->nSeat            = sProtoMsg.seat();
	}
	if ( sProtoMsg.has_requestid() )
	{
		pChiReq->nRequestID       = sProtoMsg.requestid();
	}
	pChiReq->nTileID          = sProtoMsg.tileid();
	
	if ( sProtoMsg.chitileid_size() == 3)
	{
		for (int i = 0; i < sProtoMsg.chitileid_size(); i++ )
		{
			pChiReq->anChiTileID[ i ] = sProtoMsg.chitileid( i );
		}
	}
	
	return (PTKHEADER)pChiReq;
}

/************************************************************************/
/* 碰牌请求消息                                                         */
/************************************************************************/
TKHEADER* TKMahjongProtobufFactory::DeserializeProtoPengReq( const MahJongJSPKReqMsg& reqMsg, const TKHEADER* pMsg )
{
	if ( !reqMsg.has_mahjongpeng_req_msg() )
	{
		return NULL;
	}
	TKREQMAHJONGPENG *pPengReq = new TKREQMAHJONGPENG();
	TKVerifyMemoryAlloc( pPengReq, __FILE__, __LINE__ );
	memset( pPengReq, 0, sizeof(TKREQMAHJONGPENG) );

	pPengReq->header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_PENG );
	pPengReq->header.dwLength = MSG_LENGTH( TKREQMAHJONGPENG ) ;
	pPengReq->header.dwMagic = pMsg->dwMagic;
	pPengReq->header.dwParam = pMsg->dwParam;
	pPengReq->header.dwSerial = pMsg->dwSerial;
	pPengReq->header.wOrigine = pMsg->wOrigine;
	pPengReq->header.wReserve = pMsg->wReserve;

	const MahJongPengReq sProtoMsg = reqMsg.mahjongpeng_req_msg();

	if ( sProtoMsg.has_seat() )
	{
		pPengReq->nSeat            = sProtoMsg.seat();
	}
	if ( sProtoMsg.has_requestid() )
	{
		pPengReq->nRequestID       = sProtoMsg.requestid();
	}
	pPengReq->nTileID          = sProtoMsg.tileid();

	if ( sProtoMsg.pengtileid_size() == 3 )
	{
		for (int i = 0; i < sProtoMsg.pengtileid_size(); i++ )
		{
			pPengReq->anPengTileID[ i ] = sProtoMsg.pengtileid( i );
		}
	}

	return (PTKHEADER)pPengReq;
}

/************************************************************************/
/* 杠牌请求消息                                                         */
/************************************************************************/
TKHEADER* TKMahjongProtobufFactory::DeserializeProtoGangReq( const MahJongJSPKReqMsg& reqMsg, const TKHEADER* pMsg )
{
	if ( !reqMsg.has_mahjonggang_req_msg() )
	{
		return NULL;
	}

	TKREQMAHJONGGANG *pGangReq = new TKREQMAHJONGGANG();
	TKVerifyMemoryAlloc( pGangReq, __FILE__, __LINE__ );
	memset( pGangReq, 0, sizeof(TKREQMAHJONGGANG) );

	pGangReq->header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_GANG );
	pGangReq->header.dwLength = MSG_LENGTH( TKREQMAHJONGGANG ) ;
	pGangReq->header.dwMagic = pMsg->dwMagic;
	pGangReq->header.dwParam = pMsg->dwParam;
	pGangReq->header.dwSerial = pMsg->dwSerial;
	pGangReq->header.wOrigine = pMsg->wOrigine;
	pGangReq->header.wReserve = pMsg->wReserve;

	const MahJongGangReq sProtoMsg = reqMsg.mahjonggang_req_msg();
	if ( sProtoMsg.has_seat() )
	{
		pGangReq->nSeat            = sProtoMsg.seat();
	}
	if ( sProtoMsg.has_requestid() )
	{
		pGangReq->nRequestID       = sProtoMsg.requestid();
	}
	pGangReq->nTileID          = sProtoMsg.tileid();
	pGangReq->nGangType        = sProtoMsg.gangtype();

	if ( sProtoMsg.gangtileid_size() == 4 )
	{
		for (int i = 0; i < sProtoMsg.gangtileid_size(); i++ )
		{
			pGangReq->anGangTileID[ i ] = sProtoMsg.gangtileid( i );
		}
	}

	return (PTKHEADER)pGangReq;
}

/************************************************************************/
/* 和牌请求消息                                                         */
/************************************************************************/
TKHEADER* TKMahjongProtobufFactory::DeserializeProtoWinReq( const MahJongJSPKReqMsg& reqMsg, const TKHEADER* pMsg )
{
	if ( !reqMsg.has_mahjongwin_req_msg() )
	{
		return NULL;
	}

	TKREQMAHJONGWIN *pWinReq = new TKREQMAHJONGWIN();
	TKVerifyMemoryAlloc( pWinReq, __FILE__, __LINE__ );
	memset( pWinReq, 0, sizeof(TKREQMAHJONGWIN) );

	pWinReq->header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_WIN );
	pWinReq->header.dwLength = MSG_LENGTH( TKREQMAHJONGWIN ) ;
	pWinReq->header.dwMagic = pMsg->dwLength;
	pWinReq->header.dwParam = pMsg->dwParam;
	pWinReq->header.dwSerial = pMsg->dwSerial;
	pWinReq->header.wOrigine = pMsg->wOrigine;
	pWinReq->header.wReserve = pMsg->wReserve;

	const MahJongWinReq sProtoMsg = reqMsg.mahjongwin_req_msg();

	if ( sProtoMsg.has_seat() )
	{
		pWinReq->nSeat            = sProtoMsg.seat();
	}
	if ( sProtoMsg.has_requestid() )
	{
		pWinReq->nRequestID       = sProtoMsg.requestid();
	}
	pWinReq->nTileID          = sProtoMsg.tileid();
	pWinReq->nPaoSeat         = sProtoMsg.paoseat();
	pWinReq->nResultant       = sProtoMsg.resultant();
	pWinReq->cnGroups         = sProtoMsg.groups();
	pWinReq->cnShowGroups     = sProtoMsg.showgroups();

	if ( sProtoMsg.group_size() <= 6 )
	{
		for (int i = 0; i < sProtoMsg.group_size(); i++ )
		{
			StoneGroup sStoneGroup = sProtoMsg.group( i );
			if ( sStoneGroup.stone_size() <= 4 )
			{
				for( int j = 0; j < sStoneGroup.stone_size(); j++)
				{
					Stone sStone = sStoneGroup.stone( j );
					pWinReq->asGroup[ i ].asStone[ j ].nID     = sStone.id();
					pWinReq->asGroup[ i ].asStone[ j ].nColor  = sStone.color();  
					pWinReq->asGroup[ i ].asStone[ j ].nWhat   = sStone.what(); 
				}
			}
			pWinReq->asGroup[ i ].nGroupStyle = sStoneGroup.groupstyle();
		}
	}

	if ( sProtoMsg.handtile_size() <= MAX_HAND_COUNT )
	{
		for( int i = 0; i < sProtoMsg.handtile_size(); i++)
		{
			Stone sStone = sProtoMsg.handtile( i );
			pWinReq->asHandTile[ i ].nID       = sStone.id();
			pWinReq->asHandTile[ i ].nColor    = sStone.color();
			pWinReq->asHandTile[ i ].nWhat     = sStone.what();
		}
	}

	return (PTKHEADER)pWinReq;
}

/************************************************************************/
/* 托管消息                                                             */
/************************************************************************/
TKHEADER* TKMahjongProtobufFactory::DeserializeProtoTrustPlayReq( const MahJongJSPKReqMsg& reqMsg, const TKHEADER* pMsg )
{
	if ( !reqMsg.has_mahjongtrustplay_req_msg() )
	{
		return NULL;
	}

	TKREQTRUSTPLAY *pTrustPlayReq = new TKREQTRUSTPLAY();
	TKVerifyMemoryAlloc( pTrustPlayReq, __FILE__, __LINE__ );
	memset( pTrustPlayReq, 0, sizeof(TKREQTRUSTPLAY) );

	pTrustPlayReq->header.dwType   = REQ_TYPE( TK_MSG_TRUSTPLAY );
	pTrustPlayReq->header.dwLength = MSG_LENGTH( TKREQTRUSTPLAY ) ;
	pTrustPlayReq->header.dwMagic = pMsg->dwMagic;
	pTrustPlayReq->header.dwParam = pMsg->dwParam;
	pTrustPlayReq->header.dwSerial = pMsg->dwSerial;
	pTrustPlayReq->header.wOrigine = pMsg->wOrigine;
	pTrustPlayReq->header.wReserve = pMsg->wReserve;

	const MahJongTrustPlayReq sProtoMsg = reqMsg.mahjongtrustplay_req_msg();

	pTrustPlayReq->dwUserID    = sProtoMsg.userid();
	pTrustPlayReq->dwType      = sProtoMsg.type();

	return (PTKHEADER)pTrustPlayReq;
}

/************************************************************************/
/* 加倍消息                                                             */
/************************************************************************/
TKHEADER* TKMahjongProtobufFactory::DeserializeProtoDoublingReq( const MahJongJSPKReqMsg& reqMsg, const TKHEADER* pMsg )
{
	if ( !reqMsg.has_mahjongdouble_req_msg() )
	{
		return NULL;
	}

	TKREQMAHJONGDBL *pDoublingReq = new TKREQMAHJONGDBL();
	TKVerifyMemoryAlloc( pDoublingReq, __FILE__, __LINE__ );
	memset( pDoublingReq, 0, sizeof(TKREQMAHJONGDBL) );

	pDoublingReq->header.dwType   = REQ_TYPE( TK_MSG_DOUBLING );
	pDoublingReq->header.dwLength = MSG_LENGTH( TKREQMAHJONGDBL ) ;
	pDoublingReq->header.dwMagic = pMsg->dwMagic;
	pDoublingReq->header.dwParam = pMsg->dwParam;
	pDoublingReq->header.dwSerial = pMsg->dwSerial;
	pDoublingReq->header.wOrigine = pMsg->wOrigine;
	pDoublingReq->header.wReserve = pMsg->wReserve;

	const MahJongDoubleJSPKReq sProtoMsg = reqMsg.mahjongdouble_req_msg();
	pDoublingReq->seat            = sProtoMsg.seat();
	pDoublingReq->nRequestID      = sProtoMsg.requestid();
	pDoublingReq->isDouble        = sProtoMsg.isdouble();

	if ( sProtoMsg.has_doubletype() )
	{
		pDoublingReq->dblType         = sProtoMsg.doubletype();
	}
	if ( sProtoMsg.has_doublemsg() )
	{
		MahJongWinReq sPBWinReq = sProtoMsg.doublemsg();

		pDoublingReq->dblMsg.nSeat            = sPBWinReq.seat();
		pDoublingReq->dblMsg.nRequestID       = sPBWinReq.requestid();
		pDoublingReq->dblMsg.nTileID          = sPBWinReq.tileid();
		pDoublingReq->dblMsg.nPaoSeat         = sPBWinReq.paoseat();
		pDoublingReq->dblMsg.nResultant       = sPBWinReq.resultant();
		pDoublingReq->dblMsg.cnGroups         = sPBWinReq.groups();
		pDoublingReq->dblMsg.cnShowGroups     = sPBWinReq.showgroups();

		if ( sPBWinReq.group_size() <= 6 )
		{
			for (int i = 0; i < sPBWinReq.group_size(); i++ )
			{
				StoneGroup sStoneGroup = sPBWinReq.group( i );
				if ( sStoneGroup.stone_size() <= 4 )
				{
					for( int j = 0; j < sStoneGroup.stone_size(); j++)
					{
						Stone sStone = sStoneGroup.stone( j );
						pDoublingReq->dblMsg.asGroup[ i ].asStone[ j ].nID     = sStone.id();
						pDoublingReq->dblMsg.asGroup[ i ].asStone[ j ].nColor  = sStone.color();  
						pDoublingReq->dblMsg.asGroup[ i ].asStone[ j ].nWhat   = sStone.what(); 
					}
				}
				pDoublingReq->dblMsg.asGroup[ i ].nGroupStyle = sStoneGroup.groupstyle();
			}
		}

		if ( sPBWinReq.handtile_size() <= MAX_HAND_COUNT )
		{
			for( int i = 0; i < MAX_HAND_COUNT; i++)
			{
				Stone sStone = sPBWinReq.handtile( i );

				pDoublingReq->dblMsg.asHandTile[ i ].nID       = sStone.id();
				pDoublingReq->dblMsg.asHandTile[ i ].nColor    = sStone.color();
				pDoublingReq->dblMsg.asHandTile[ i ].nWhat     = sStone.what();
			}
		}
	}

	return (PTKHEADER)pDoublingReq;
}

/************************************************************************/
/* 超时检测消息                                                         */
/************************************************************************/
TKHEADER* TKMahjongProtobufFactory::DeserializeProtoCheckTimeOutReq( const MahJongJSPKReqMsg& reqMsg, const TKHEADER* pMsg )
{
	if ( !reqMsg.has_mahjongchecktimeout_req_msg() )
	{
		return NULL;
	}

	TKREQCHECKTIMEOUT *pCheckTimeOutReq = new TKREQCHECKTIMEOUT();
	TKVerifyMemoryAlloc( pCheckTimeOutReq, __FILE__, __LINE__ );
	memset( pCheckTimeOutReq, 0, sizeof(TKREQCHECKTIMEOUT) );

	pCheckTimeOutReq->header.dwType   = REQ_TYPE( TK_MSG_CHECKTIMEOUT );
	pCheckTimeOutReq->header.dwLength = MSG_LENGTH( TKREQCHECKTIMEOUT ) ;
	pCheckTimeOutReq->header.dwMagic = pMsg->dwMagic;
	pCheckTimeOutReq->header.dwParam = pMsg->dwParam;
	pCheckTimeOutReq->header.dwSerial = pMsg->dwSerial;
	pCheckTimeOutReq->header.wOrigine = pMsg->wOrigine;
	pCheckTimeOutReq->header.wReserve = pMsg->wReserve;

	const MahJongCheckTimeOutReq sProtoMsg = reqMsg.mahjongchecktimeout_req_msg();

	pCheckTimeOutReq->m_dwUserID    = sProtoMsg.userid();
	if( sProtoMsg.has_waittime() )
	{
		pCheckTimeOutReq->nWaitSecond   = sProtoMsg.waittime();
	}

	return (PTKHEADER)pCheckTimeOutReq;
}

/************************************************************************/
/* 听牌信息                                                             */
/************************************************************************/
TKHEADER* TKMahjongProtobufFactory::DeserializeProtoCallTileReq( const MahJongJSPKReqMsg& reqMsg, const TKHEADER* pMsg )
{
	if ( !reqMsg.has_mahjongcalltile_req_msg() )
	{
		return NULL;
	}

	TKREQMAHJONGCALLTILE *pCallTileReq = new TKREQMAHJONGCALLTILE();
	TKVerifyMemoryAlloc( pCallTileReq, __FILE__, __LINE__ );
	memset( pCallTileReq, 0, sizeof(TKREQMAHJONGCALLTILE) );

	pCallTileReq->header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_CALLTILE );
	pCallTileReq->header.dwLength = MSG_LENGTH( TKREQMAHJONGCALLTILE ) ;
	pCallTileReq->header.dwMagic = pMsg->dwMagic;
	pCallTileReq->header.dwParam = pMsg->dwParam;
	pCallTileReq->header.dwSerial = pMsg->dwSerial;
	pCallTileReq->header.wOrigine = pMsg->wOrigine;
	pCallTileReq->header.wReserve = pMsg->wReserve;

	const MahJongCallTileReq spProtoMsg = reqMsg.mahjongcalltile_req_msg();

	pCallTileReq->nSeat       = spProtoMsg.seat();
	pCallTileReq->nRequestID  = spProtoMsg.requestid();
	pCallTileReq->cnCallTileCount = spProtoMsg.count();

	if ( spProtoMsg.id_size() <= 34 )
	{
		for (int i = 0; i < spProtoMsg.id_size(); i++ )
		{
			pCallTileReq->nCallTileID[ i ] = spProtoMsg.id( i );
		}
	}

	return (PTKHEADER)pCallTileReq;
}

// 为Protobuf协议填写MatchId
void TKMahjongProtobufFactory::SerializeProtobufSetMatchID( MahJongJSPKAckMsg* pAckMsg )
{
	pAckMsg->set_matchid( m_pMahjongGameProxy->GetMatchID() );
}


/************************************************************************/
/*  游戏规则消息                                                        */
/************************************************************************/
void TKMahjongProtobufFactory::SerializeNetMsgRulerInfoAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg)
{
	// 组装消息头
	SerializeProtobufAckMsgHead( pMsg, protoMsgAck );

	TKREQMAHJONGRULEINFO* pRulerInfoMsg = (TKREQMAHJONGRULEINFO*)pMsg;

	// 获取真正的消息体
	MahJongJSPKAckMsg* pJSPKAckMsg = protoMsgAck.protoAckMsg.mutable_mahjongjspk_ack_msg();
	ASSERT(NULL != pJSPKAckMsg);

	// 设置MatchID
	SerializeProtobufSetMatchID( pJSPKAckMsg );

	MahJongRuleInfoAck* pRulerInfoAck = pJSPKAckMsg->mutable_mahjongruleinfo_ack_msg();
	ASSERT(NULL != pRulerInfoAck);

	// 清空消息体 防止重入消息错误
	pRulerInfoAck->Clear();

	// 设置消息体中的所有参数
	pRulerInfoAck->set_rule(pRulerInfoMsg->nRule);
	pRulerInfoAck->set_winselfonly(pRulerInfoMsg->bWinSelfOnly);
	pRulerInfoAck->set_discardtime(pRulerInfoMsg->nDiscardTime);
	pRulerInfoAck->set_waittime(pRulerInfoMsg->nWaitTime);
	pRulerInfoAck->set_winneedminfan(pRulerInfoMsg->nWinNeedMinFan);
		
	// 在数据都添置好以后，来计算包体长度
	protoMsgAck.header.dwLength = protoMsgAck.protoAckMsg.ByteSize();
}

/************************************************************************/
/*  游戏坐位、庄家消息                                                  */
/************************************************************************/
void TKMahjongProtobufFactory::SerializeNetMsgPlaceAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg)
{
	// 组装消息头
	SerializeProtobufAckMsgHead( pMsg, protoMsgAck );

	TKREQMAHJONGPLACE* pPlaceMsg = (TKREQMAHJONGPLACE*)pMsg;

	// 获取真正的消息体
	MahJongJSPKAckMsg* pJSPKAckMsg = protoMsgAck.protoAckMsg.mutable_mahjongjspk_ack_msg();
	ASSERT(NULL != pJSPKAckMsg);

	// 设置MatchID
	SerializeProtobufSetMatchID( pJSPKAckMsg );

	MahJongPlaceAck* pPlaceAck = pJSPKAckMsg->mutable_mahjongplace_ack_msg();
	ASSERT(NULL != pPlaceAck);

	// 清空消息体 防止重入消息错误
	pPlaceAck->Clear();

	// 设置消息体中的所有参数
	pPlaceAck->set_banker(pPlaceMsg->nBanker);
	pPlaceAck->set_rules(pPlaceMsg->nRules);
	for (int i = 0; i < MAX_PLAYER_COUNT; i++)
	{
		pPlaceAck->add_gameseat( pPlaceMsg->anGameSeat[i] );
	}

	// 在数据都添置好以后，来计算包体长度
	protoMsgAck.header.dwLength = protoMsgAck.protoAckMsg.ByteSize();
}

/************************************************************************/
/*  门风、圈风消息                                                      */
/************************************************************************/
void TKMahjongProtobufFactory::SerializeNetMsgWindPlaceAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg)
{
	// 组装消息头
	SerializeProtobufAckMsgHead( pMsg, protoMsgAck );

	TKREQMAHJONGWINDPLACE* pWindPlaceMsg = (TKREQMAHJONGWINDPLACE*)pMsg;

	// 获取真正的消息体
	MahJongJSPKAckMsg* pJSPKAckMsg = protoMsgAck.protoAckMsg.mutable_mahjongjspk_ack_msg();
	ASSERT(NULL != pJSPKAckMsg);

	// 设置MatchID
	SerializeProtobufSetMatchID( pJSPKAckMsg );

	MahJongWindPlaceAck* pWindPlaceAck = pJSPKAckMsg->mutable_mahjongwindplace_ack_msg();
	ASSERT(NULL != pWindPlaceAck);

	// 清空消息体 防止重入消息错误
	pWindPlaceAck->Clear();

	// 设置消息体中的所有参数
	pWindPlaceAck->set_roundwind(pWindPlaceMsg->nRoundWind);

	// 在数据都添置好以后，来计算包体长度
	protoMsgAck.header.dwLength = protoMsgAck.protoAckMsg.ByteSize();
}


/************************************************************************/
/*  洗牌、砌牌消息                                                      */
/************************************************************************/
void TKMahjongProtobufFactory::SerializeNetMsgShuffleAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg)
{
	// 组装消息头
	SerializeProtobufAckMsgHead( pMsg, protoMsgAck );

	TKREQMAHJONGSHUFFLE* pShuffleMsg = (TKREQMAHJONGSHUFFLE*)pMsg;

	// 获取真正的消息体
	MahJongJSPKAckMsg* pJSPKAckMsg = protoMsgAck.protoAckMsg.mutable_mahjongjspk_ack_msg();
	ASSERT(NULL != pJSPKAckMsg);
	
	// 设置MatchID
	SerializeProtobufSetMatchID( pJSPKAckMsg );

	MahJongShuffleAck* pShuffAck = pJSPKAckMsg->mutable_mahjongshuffle_ack_msg();
	ASSERT(NULL != pShuffAck);

	// 清空消息体 防止重入消息错误
	pShuffAck->Clear();

	// 设置消息体中的所有参数
	pShuffAck->set_tilecount(pShuffleMsg->cnTile);
	pShuffAck->set_magicitem(pShuffleMsg->cnMagicItem);

	for (int i = 0; i < MAX_PLAYER_COUNT; i++)
	{
		pShuffAck->add_frustaofseat( pShuffleMsg->anFrustaOfSeat[i] );
	}

	// 在数据都添置好以后，来计算包体长度
	protoMsgAck.header.dwLength = protoMsgAck.protoAckMsg.ByteSize();
}

/************************************************************************/
/*  掷骰子消息                                                          */
/************************************************************************/
void TKMahjongProtobufFactory::SerializeNetMsgCastDice(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg)
{
	// 组装消息头
	SerializeProtobufAckMsgHead( pMsg, protoMsgAck );

	TKREQMAHJONGCASTDICE *pCastDiceMsg = (TKREQMAHJONGCASTDICE*)pMsg;

	// 获取真正的消息体
	MahJongJSPKAckMsg* pJSPKAckMsg = protoMsgAck.protoAckMsg.mutable_mahjongjspk_ack_msg();
	ASSERT(NULL != pJSPKAckMsg);

	// 设置MatchID
	SerializeProtobufSetMatchID( pJSPKAckMsg );

	MahJongCastDiceAck* pCastDice = pJSPKAckMsg->mutable_mahjongcastdice_ack_msg();
	ASSERT(NULL != pCastDice);

	// 清空消息体 防止重入消息错误
	pCastDice->Clear();

	pCastDice->set_seat(pCastDiceMsg->nSeat);
	pCastDice->set_castdicetype(pCastDiceMsg->nCastDiceType);
	pCastDice->set_dicecount(pCastDiceMsg->cnDice);
	int *pnDice = ( int * ) ( pCastDiceMsg + 1 ) ;
	pCastDice->add_dicevalue( pnDice[0] );
	pCastDice->add_dicevalue( pnDice[1] );

	// 在数据都添置好以后，来计算包体长度
	protoMsgAck.header.dwLength = protoMsgAck.protoAckMsg.ByteSize();
}


/************************************************************************/
/*  开牌消息                                                            */
/************************************************************************/
void TKMahjongProtobufFactory::SerializeNetMsgOpenDoorAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg)
{
	// 组装消息头
	SerializeProtobufAckMsgHead( pMsg, protoMsgAck );

	TKREQMAHJONGOPENDOOR* pOpenDoorMsg = (TKREQMAHJONGOPENDOOR*)pMsg;

	// 获取真正的消息体
	MahJongJSPKAckMsg* pJSPKAckMsg = protoMsgAck.protoAckMsg.mutable_mahjongjspk_ack_msg();
	ASSERT(NULL != pJSPKAckMsg);

	// 设置MatchID
	SerializeProtobufSetMatchID( pJSPKAckMsg );

	MahJongOpenDoorAck* pOpenDoor = pJSPKAckMsg->mutable_mahjongopendoor_ack_msg();
	ASSERT(NULL != pOpenDoor);

	// 清空消息体 防止重入消息错误
	pOpenDoor->Clear();

	pOpenDoor->set_seat(pOpenDoorMsg->nSeat);
	pOpenDoor->set_opendoorseat(pOpenDoorMsg->nOpenDoorSeat);
	pOpenDoor->set_opendoorfrusta(pOpenDoorMsg->nOpenDoorFrusta);
	pOpenDoor->set_handtile(pOpenDoorMsg->cnHandTile);

	int *pnTile = ( int * ) ( pOpenDoorMsg + 1 ) ;

	for ( int i = 0; i < pOpenDoorMsg->cnHandTile; i++)
	{
		pOpenDoor->add_tiles(pnTile[ i ]);
	}

	// 在数据都添置好以后，来计算包体长度
	protoMsgAck.header.dwLength = protoMsgAck.protoAckMsg.ByteSize();
}

/************************************************************************/
/*    通知消息                                                          */
/************************************************************************/
void TKMahjongProtobufFactory::SerializeNetMsgNotify(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg)
{
	// 组装消息头
	SerializeProtobufAckMsgHead( pMsg, protoMsgAck );

	TKREQMAHJONGNOTIFY* pNotifyMsg = (TKREQMAHJONGNOTIFY*)pMsg;

	// 获取真正的消息体
	MahJongJSPKAckMsg* pJSPKAckMsg = protoMsgAck.protoAckMsg.mutable_mahjongjspk_ack_msg();
	ASSERT(NULL != pJSPKAckMsg);

	// 设置MatchID
	SerializeProtobufSetMatchID( pJSPKAckMsg );

	MahJongNotifyAck* pNotifyAck = pJSPKAckMsg->mutable_mahjongnotify_ack_msg();
	ASSERT(NULL != pNotifyAck);

	// 清空消息体 防止重入消息错误
	pNotifyAck->Clear();

	// 设置消息体中的所有参数
	pNotifyAck->set_notify(pNotifyMsg->nNotify);

	// 指到消息尾
	char *pEnd = ( char* ) ( pNotifyMsg + 1 ) ;

	if( MAHJONG_NOTIFY_DISABLE_RECEIVE == pNotifyMsg->nNotify )
	{
		pNotifyAck->set_notifytype( atoi( pEnd ) );
	}
	else
	{
		string strInput = "";
		strInput.append( pEnd );

		string strSend = "";
		if ( IsStringGBK( strInput.c_str() ))
		{
			strSend = GBKToUTF8( strInput );
		}
		else
		{
			strSend.append( strInput );
		}
		
		pNotifyAck->set_notifystr( strSend.c_str(), strSend.length() );
	}

	// 在数据都添置好以后，来计算包体长度
	protoMsgAck.header.dwLength = protoMsgAck.protoAckMsg.ByteSize();
}

/************************************************************************/
/*    通知补花消息                                                       */
/************************************************************************/
void TKMahjongProtobufFactory::SerializeNetMsgChangeFlower(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg)
{
	// 组装消息头
	SerializeProtobufAckMsgHead( pMsg, protoMsgAck );

	TKREQMAHJONGCHANGEFLOWER* pChangeFlowerMsg = (TKREQMAHJONGCHANGEFLOWER*)pMsg;

	// 获取真正的消息体
	MahJongJSPKAckMsg* pJSPKAckMsg = protoMsgAck.protoAckMsg.mutable_mahjongjspk_ack_msg();
	ASSERT(NULL != pJSPKAckMsg);

	// 设置MatchID
	SerializeProtobufSetMatchID( pJSPKAckMsg );

	MahJongChangeFlowerAck* pChangeFlowerAck = pJSPKAckMsg->mutable_mahjongchangeflower_ack_msg();
	ASSERT(NULL != pChangeFlowerAck);
	
	// 清空消息体 防止重入消息错误
	pChangeFlowerAck->Clear();

	// 设置消息体中的所有参数
	pChangeFlowerAck->set_seat(pChangeFlowerMsg->nSeat);
	pChangeFlowerAck->set_tileid(pChangeFlowerMsg->nTileID);
	pChangeFlowerAck->set_requestid(pChangeFlowerMsg->nRequestID);

	// 在数据都添置好以后，来计算包体长度
	protoMsgAck.header.dwLength = protoMsgAck.protoAckMsg.ByteSize();
}

/************************************************************************/
/*    通知抓牌消息                                                      */
/************************************************************************/
void TKMahjongProtobufFactory::SerializeNetMsgDrawTile(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg)
{
	// 组装消息头
	SerializeProtobufAckMsgHead( pMsg, protoMsgAck );

	TKREQMAHJONGDRAWTILE* pDrawTileMsg = (TKREQMAHJONGDRAWTILE*)pMsg;

	// 获取真正的消息体
	MahJongJSPKAckMsg* pJSPKAckMsg = protoMsgAck.protoAckMsg.mutable_mahjongjspk_ack_msg();
	ASSERT(NULL != pJSPKAckMsg);

	// 设置MatchID
	SerializeProtobufSetMatchID( pJSPKAckMsg );

	MahJongDrawTileAck* pDrawTitleAck = pJSPKAckMsg->mutable_mahjongdrawtile_ack_msg();
	ASSERT(NULL != pDrawTitleAck);

	// 清空消息体 防止重入消息错误
	pDrawTitleAck->Clear();

	// 设置消息体中的所有参数
	pDrawTitleAck->set_seat(pDrawTileMsg->nSeat);
	pDrawTitleAck->set_drawtiletype(pDrawTileMsg->nDrawTileType);
	pDrawTitleAck->set_fromwallheader(pDrawTileMsg->bFromWallHeader);
	pDrawTitleAck->set_tileoffset(pDrawTileMsg->nTileOffset);
	pDrawTitleAck->set_tileid(pDrawTileMsg->nTileID);

	// 在数据都添置好以后，来计算包体长度
	protoMsgAck.header.dwLength = protoMsgAck.protoAckMsg.ByteSize();
}

/************************************************************************/
/*    服务器请求客户端执行操作消息                                      */
/************************************************************************/
void TKMahjongProtobufFactory::SerializeNetMsgActionAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg)
{
	// 组装消息头
	SerializeProtobufAckMsgHead( pMsg, protoMsgAck );

	TKREQMAHJONGREQUEST *pRequestMsg = (TKREQMAHJONGREQUEST*)pMsg;

	// 获取真正的消息体
	MahJongJSPKAckMsg* pJSPKAckMsg = protoMsgAck.protoAckMsg.mutable_mahjongjspk_ack_msg();
	ASSERT(NULL != pJSPKAckMsg);

	// 设置MatchID
	SerializeProtobufSetMatchID( pJSPKAckMsg );

	MahJongActionAck* pRequestReq = pJSPKAckMsg->mutable_mahjongaction_ack_msg();
	ASSERT(NULL != pRequestReq);

	// 清空消息体 防止重入消息错误
	pRequestReq->Clear();

	// 设置消息体中的所有参数
	pRequestReq->set_seat(pRequestMsg->nSeat);
	pRequestReq->set_requestid(pRequestMsg->nRequestID);
	pRequestReq->set_requesttype(pRequestMsg->nRequestType);
	pRequestReq->set_waitsecond(pRequestMsg->nWaitSecond);

	// 在数据都添置好以后，来计算包体长度
	protoMsgAck.header.dwLength = protoMsgAck.protoAckMsg.ByteSize();
	//// for test
	//TKWriteLog("请求客户端[%u]执行操作 type[%u]  time = %u", pRequestReq->seat(), pRequestReq->requesttype(), GetTickCount());
}


/************************************************************************/
/*    服务器向客户端广播其他玩家的操作消息                              */
/************************************************************************/
void TKMahjongProtobufFactory::SerializeNetMsgRequestAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg)
{
	// 组装消息头
	SerializeProtobufAckMsgHead( pMsg, protoMsgAck );

	TKACKMAHJONGREQUEST *pRequestMsg = (TKACKMAHJONGREQUEST*)pMsg;

	// 获取真正的消息体
	MahJongJSPKAckMsg* pJSPKAckMsg = protoMsgAck.protoAckMsg.mutable_mahjongjspk_ack_msg();
	ASSERT(NULL != pJSPKAckMsg);

	// 设置MatchID
	SerializeProtobufSetMatchID( pJSPKAckMsg );

	MahJongRequestAck* pRequestAck = pJSPKAckMsg->mutable_mahjongrequest_ack_msg();
	ASSERT(NULL != pRequestAck);

	// 清空消息体 防止重入消息错误
	pRequestAck->Clear();

	// 设置消息体中的所有参数
	pRequestAck->set_seat(pRequestMsg->nSeat);
	pRequestAck->set_requestid(pRequestMsg->nRequestID);
	pRequestAck->set_giveuprequesttype(pRequestMsg->nGiveUpRequestType);

	// Todo

	// 在数据都添置好以后，来计算包体长度
	protoMsgAck.header.dwLength = protoMsgAck.protoAckMsg.ByteSize();
}

/************************************************************************/
/*    通知出牌消息                                                      */
/************************************************************************/
void TKMahjongProtobufFactory::SerializeNetMsgDiscardTile(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg)
{
	// 组装消息头
	SerializeProtobufAckMsgHead( pMsg, protoMsgAck );

	TKREQMAHJONGDISCARDTILE* pDiscardTileMsg = (TKREQMAHJONGDISCARDTILE*)pMsg;

	// 获取真正的消息体
	MahJongJSPKAckMsg* pJSPKAckMsg = protoMsgAck.protoAckMsg.mutable_mahjongjspk_ack_msg();
	ASSERT(NULL != pJSPKAckMsg);

	// 设置MatchID
	SerializeProtobufSetMatchID( pJSPKAckMsg );

	MahJongDiscardTileAck* pDiscardTile = pJSPKAckMsg->mutable_mahjongdiscardtile_ack_msg();
	ASSERT(NULL != pDiscardTile);

	// 清空消息体 防止重入消息错误
	pDiscardTile->Clear();

	// 设置消息体中的所有参数
	pDiscardTile->set_seat(pDiscardTileMsg->nSeat);
	pDiscardTile->set_requestid(pDiscardTileMsg->nRequestID);
	pDiscardTile->set_tileid(pDiscardTileMsg->nTileID);
	pDiscardTile->set_hide(pDiscardTileMsg->bHide);

	// 在数据都添置好以后，来计算包体长度
	protoMsgAck.header.dwLength = protoMsgAck.protoAckMsg.ByteSize();
}

/************************************************************************/
/*    结果消息，一局游戏结束时，服务器发送此消息给所有客户端			*/
/************************************************************************/
void TKMahjongProtobufFactory::SerializeNetMsgResultExAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg)
{
	// 组装消息头
	SerializeProtobufAckMsgHead( pMsg, protoMsgAck );

	TKREQMAHJONGRESULT* pResultMsg = (TKREQMAHJONGRESULT*)pMsg;
	ASSERT(NULL != pResultMsg);

	// 获取真正的消息体
	MahJongJSPKAckMsg* pJSPKAckMsg = protoMsgAck.protoAckMsg.mutable_mahjongjspk_ack_msg();
	ASSERT(NULL != pJSPKAckMsg);

	// 设置MatchID
	SerializeProtobufSetMatchID( pJSPKAckMsg );

	MahJongResultJSPKAck* pResultexAck  = pJSPKAckMsg->mutable_mahjongresult_ack_msg();
	ASSERT(NULL != pResultexAck);

	// 清空消息体 防止重入消息错误
	pResultexAck->Clear();

	pResultexAck->set_wininfo(pResultMsg->cnWinInfo);
	pResultexAck->set_time(pResultMsg->nTime);
	pResultexAck->set_resulttype(pResultMsg->nResultType);

	for ( int i = 0; i < MAX_PLAYER_COUNT; i++)
	{
		PlayerResultJSPK* pPlayerResultJSPK = pResultexAck->add_playerresults( );
		ASSERT(NULL != pPlayerResultJSPK);

		pPlayerResultJSPK->set_win(pResultMsg->asPlayerResult[ i ].cnWin);
		pPlayerResultJSPK->set_pao(pResultMsg->asPlayerResult[ i ].cnPao);
		pPlayerResultJSPK->set_gang(pResultMsg->asPlayerResult[ i ].cnGang);

		pPlayerResultJSPK->set_score(pResultMsg->asPlayerResult[ i ].nScore);
		pPlayerResultJSPK->set_windouble(pResultMsg->asPlayerResult[ i ].nDouble);
		pPlayerResultJSPK->set_totalscore(pResultMsg->asPlayerResult[ i ].nTotalScore);
		pPlayerResultJSPK->set_handtilecount(pResultMsg->asPlayerResult[ i ].cnHandTile);
		for (int j = 0; j < MAX_HAND_TILE_COUNT; j++)
		{
			pPlayerResultJSPK->add_handtiles( pResultMsg->asPlayerResult[ i ].anHandTile[ j ] );
		}
	}

	if ( pResultMsg->cnWinInfo > 0)
	{
		PTKACKMAHJONGWINDETAILEX pAckMahJongWin = ( PTKACKMAHJONGWINDETAILEX )( pResultMsg + 1 );

		for ( int i = 0; i <  pResultMsg->cnWinInfo; i++)
		{
			WinDetailExJSPK* pWinDetailEx = pResultexAck->add_windetail( );
			ASSERT(NULL != pWinDetailEx);

			pWinDetailEx->set_winseat(pAckMahJongWin->nWinSeat);
			pWinDetailEx->set_windouble(pAckMahJongWin->winDouble);
			pWinDetailEx->set_paoseat(pAckMahJongWin->nPaoSeat);
			pWinDetailEx->set_wintile(pAckMahJongWin->nWinTile);
			pWinDetailEx->set_basescore(pAckMahJongWin->nBaseScore);
			pWinDetailEx->set_time(pAckMahJongWin->nTime);

			WinInfo* pWinInfo = pWinDetailEx->mutable_wininfo();
			ASSERT(NULL != pWinInfo);
			{
				pWinInfo->set_maxfans(pAckMahJongWin->sWinInfo.nMaxFans);
				for(int j = 0; j < MAXFANS; j++)
				{
					pWinInfo->add_fans(pAckMahJongWin->sWinInfo.anFans[ j ]);
				}
				pWinInfo->set_resultant(pAckMahJongWin->sWinInfo.nResultant);
				for ( int j = 0; j < 6; j++)
				{
					StoneGroup* pStoneGroup = pWinInfo->add_groups( );
					ASSERT(NULL != pStoneGroup);

					for ( int n = 0; n < 4; n++)
					{
						Stone *pStone = pStoneGroup->add_stone( );
						ASSERT(NULL != pStone);

						pStone->set_id( pAckMahJongWin->sWinInfo.asGroup[ j ].asStone [ n ].nID );
						pStone->set_color( pAckMahJongWin->sWinInfo.asGroup[ j ].asStone [ n ].nColor );
						pStone->set_what( pAckMahJongWin->sWinInfo.asGroup[ j ].asStone [ n ].nWhat );
					}
					pStoneGroup->set_groupstyle( pAckMahJongWin->sWinInfo.asGroup[ j ].nGroupStyle );
				}

				for ( int j = 0; j < MAX_HAND_COUNT; j++)
				{
					Stone* pHandStone = pWinInfo->add_handstone( );
					ASSERT(NULL != pHandStone);
					pHandStone->set_id( pAckMahJongWin->sWinInfo.asHandStone[ j ].nID);
					pHandStone->set_color( pAckMahJongWin->sWinInfo.asHandStone[ j ].nColor );
					pHandStone->set_what( pAckMahJongWin->sWinInfo.asHandStone[ j ].nWhat );
				}

				pWinInfo->set_groupcount( pAckMahJongWin->sWinInfo.cnGroups );
				pWinInfo->set_wintileid( pAckMahJongWin->sWinInfo.nHuTileID );
				for ( int j = 0; j < 8; j++)
				{
					Stone* pFlowerStone = pWinInfo->add_flowers( );
					ASSERT(NULL != pFlowerStone);
					pFlowerStone->set_id( pAckMahJongWin->sWinInfo.asFlower[ j ].nID);
					pFlowerStone->set_color( pAckMahJongWin->sWinInfo.asFlower[ j ].nColor );
					pFlowerStone->set_what( pAckMahJongWin->sWinInfo.asFlower[ j ].nWhat );
				}
				pWinInfo->set_flowercount( pAckMahJongWin->sWinInfo.cnFlower );
				pWinInfo->set_quanwind( pAckMahJongWin->sWinInfo.nQuanWind );
				pWinInfo->set_menwind( pAckMahJongWin->sWinInfo.nMenWind );
				pWinInfo->set_showgroups( pAckMahJongWin->sWinInfo.cnShowGroups );
				pWinInfo->set_winmode( pAckMahJongWin->sWinInfo.nWinMode );
				pWinInfo->set_scoreoffan ( pAckMahJongWin->sWinInfo.nScoreOfFan );
			}

			for ( int j = 0; j < MAX_PLAYER_COUNT; j++)
			{
				ScoreDetailEx* pScoreDetailEx = pWinDetailEx->add_scoredetails( );
				ASSERT(NULL != pWinInfo);

				pScoreDetailEx->set_leftscore( pAckMahJongWin->asScoreDetail[ j ].nLeftScore);
				for (int n = 0; n < 8; n++)
				{
					pScoreDetailEx->add_score(pAckMahJongWin->asScoreDetail[ j ].anScore[ n ]);
				}
			}

			pAckMahJongWin ++;
		}
	}

	// 在数据都添置好以后，来计算包体长度
	protoMsgAck.header.dwLength = protoMsgAck.protoAckMsg.ByteSize();
}

/************************************************************************/
/*    翻混消息			                                                 */
/************************************************************************/
void TKMahjongProtobufFactory::SerializeNetMsgHunAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg)
{
	// 组装消息头
	SerializeProtobufAckMsgHead( pMsg, protoMsgAck );

	TKREQMAHJONGHUN* pHunMsg = (TKREQMAHJONGHUN*)pMsg;

	// 获取真正的消息体
	MahJongJSPKAckMsg* pJSPKAckMsg = protoMsgAck.protoAckMsg.mutable_mahjongjspk_ack_msg();
	ASSERT(NULL != pJSPKAckMsg);

	// 设置MatchID
	SerializeProtobufSetMatchID( pJSPKAckMsg );

	MahJongHunAck* pHunAck = pJSPKAckMsg->mutable_mahjonghun_ack_msg();
	ASSERT(NULL != pHunAck);

	// 清空消息体 防止重入消息错误
	pHunAck->Clear();

	pHunAck->set_fromwallheader(pHunMsg->bFromWallHeader);
	pHunAck->set_tileoffset(pHunMsg->nTileOffset);
	pHunAck->set_tileid(pHunMsg->nTileID);
	pHunAck->set_huntilecount(pHunMsg->cnHunTile);
	 
	// 记录混牌
	int * pHun = ( int * )( pHunMsg + 1 );

	for ( int i = 0; i < pHunMsg->cnHunTile; i++ )
	{
		pHunAck->add_hunttileids( pHun[ i ] );
	}

	// 在数据都添置好以后，来计算包体长度
	protoMsgAck.header.dwLength = protoMsgAck.protoAckMsg.ByteSize();
}
/************************************************************************/
/*    通知听牌消息			                                             */
/************************************************************************/
void TKMahjongProtobufFactory::SerializeNetMsgCallAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg )
{
	// 组装消息头
	SerializeProtobufAckMsgHead( pMsg, protoMsgAck );

	TKREQMAHJONGCALL* pCallMsg = (TKREQMAHJONGCALL*)pMsg;

	// 获取真正的消息体
	MahJongJSPKAckMsg* pJSPKAckMsg = protoMsgAck.protoAckMsg.mutable_mahjongjspk_ack_msg();
	ASSERT(NULL != pJSPKAckMsg);

	// 设置MatchID
	SerializeProtobufSetMatchID( pJSPKAckMsg );

	MahJongCallAck* pCallAck = pJSPKAckMsg->mutable_mahjongcall_ack_msg();
	ASSERT(NULL != pCallAck);

	// 清空消息体 防止重入消息错误
	pCallAck->Clear();

	pCallAck->set_seat(pCallMsg->nSeat);
	pCallAck->set_tileid(pCallMsg->nTileID);
	pCallAck->set_requestid(pCallMsg->nRequestID);
	pCallAck->set_winself(pCallMsg->bWinSelf);
	pCallAck->set_autogang(pCallMsg->bAutoGang);
	pCallAck->set_calltype(pCallMsg->nCallType);
	pCallAck->set_hide(pCallMsg->bHide);

	// 在数据都添置好以后，来计算包体长度
	protoMsgAck.header.dwLength = protoMsgAck.protoAckMsg.ByteSize();
}


/************************************************************************/
/*    通知吃牌消息			                                             */
/************************************************************************/
void TKMahjongProtobufFactory::SerializeNetMsgChiAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg)
{
	// 组装消息头
	SerializeProtobufAckMsgHead( pMsg, protoMsgAck );

	TKREQMAHJONGCHI* pChiMsg = (TKREQMAHJONGCHI*)pMsg;

	// 获取真正的消息体
	MahJongJSPKAckMsg* pJSPKAckMsg = protoMsgAck.protoAckMsg.mutable_mahjongjspk_ack_msg();
	ASSERT(NULL != pJSPKAckMsg);

	// 设置MatchID
	SerializeProtobufSetMatchID( pJSPKAckMsg );

	MahJongChiAck* pChiAck = pJSPKAckMsg->mutable_mahjongchi_ack_msg();
	ASSERT(NULL != pChiAck);

	// 清空消息体 防止重入消息错误
	pChiAck->Clear();
	
	pChiAck->set_seat(pChiMsg->nSeat);
	pChiAck->set_requestid(pChiMsg->nRequestID);
	pChiAck->set_tileid(pChiMsg->nTileID);

	for ( int i = 0; i < 3; i++)
	{
		pChiAck->add_chitileid( pChiMsg->anChiTileID[ i ]);
	}

	// 在数据都添置好以后，来计算包体长度
	protoMsgAck.header.dwLength = protoMsgAck.protoAckMsg.ByteSize();
}


/************************************************************************/
/*    通知碰牌消息			                                             */
/************************************************************************/
void TKMahjongProtobufFactory::SerializeNetMsgPengAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg)
{
	// 组装消息头
	SerializeProtobufAckMsgHead( pMsg, protoMsgAck );

	TKREQMAHJONGPENG* pPengMsg = (TKREQMAHJONGPENG*)pMsg;

	// 获取真正的消息体
	MahJongJSPKAckMsg* pJSPKAckMsg = protoMsgAck.protoAckMsg.mutable_mahjongjspk_ack_msg();
	ASSERT(NULL != pJSPKAckMsg);

	// 设置MatchID
	SerializeProtobufSetMatchID( pJSPKAckMsg );

	MahJongPengAck* pPengAck = pJSPKAckMsg->mutable_mahjongpeng_ack_msg();
	ASSERT(NULL != pPengAck);

	// 清空消息体 防止重入消息错误
	pPengAck->Clear();

	pPengAck->set_seat(pPengMsg->nSeat);
	pPengAck->set_requestid(pPengMsg->nRequestID);
	pPengAck->set_tileid(pPengMsg->nTileID);

	for ( int i = 0; i < 3; i++)
	{
		pPengAck->add_pengtileid( pPengMsg->anPengTileID[ i ]);
	}

	// 在数据都添置好以后，来计算包体长度
	protoMsgAck.header.dwLength = protoMsgAck.protoAckMsg.ByteSize();
}

/************************************************************************/
/*    通知杠牌消息	                                                   */
/************************************************************************/
void TKMahjongProtobufFactory::SerializeNetMsgGangAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg)
{
	// 组装消息头
	SerializeProtobufAckMsgHead( pMsg, protoMsgAck );

	TKREQMAHJONGGANG* pGangMsg = (TKREQMAHJONGGANG*)pMsg;

	// 获取真正的消息体
	MahJongJSPKAckMsg* pJSPKAckMsg = protoMsgAck.protoAckMsg.mutable_mahjongjspk_ack_msg();
	ASSERT(NULL != pJSPKAckMsg);

	// 设置MatchID
	SerializeProtobufSetMatchID( pJSPKAckMsg );

	MahJongGangAck* pGangAck = pJSPKAckMsg->mutable_mahjonggang_ack_msg();
	ASSERT(NULL != pGangAck);

	// 清空消息体 防止重入消息错误
	pGangAck->Clear();

	pGangAck->set_seat(pGangMsg->nSeat);
	pGangAck->set_requestid(pGangMsg->nRequestID);
	pGangAck->set_tileid(pGangMsg->nTileID);
	pGangAck->set_gangtype(pGangMsg->nGangType);

	for ( int i = 0; i < 4; i++)
	{
		pGangAck->add_gangtileid( pGangMsg->anGangTileID[ i ]);
	}

	// 在数据都添置好以后，来计算包体长度
	protoMsgAck.header.dwLength = protoMsgAck.protoAckMsg.ByteSize();
}

/************************************************************************/
/*    通知和牌消息	                                                   */
/************************************************************************/
void TKMahjongProtobufFactory::SerializeNetMsgWinAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg)
{
	// 组装消息头
	SerializeProtobufAckMsgHead( pMsg, protoMsgAck );

	TKACKMAHJONGWIN* pWinJSPKMsg = (TKACKMAHJONGWIN*)pMsg;

	// 获取真正的消息体
	MahJongJSPKAckMsg* pJSPKAckMsg = protoMsgAck.protoAckMsg.mutable_mahjongjspk_ack_msg();
	ASSERT(NULL != pJSPKAckMsg);

	// 设置MatchID
	SerializeProtobufSetMatchID( pJSPKAckMsg );

	MahJongWinJSPKAck* pWinJSPKAck = pJSPKAckMsg->mutable_mahjongwin_ack_msg();
	ASSERT(NULL != pWinJSPKAck);

	// 清空消息体 防止重入消息错误
	pWinJSPKAck->Clear();

	for (int i = 0; i < MAX_PLAYER_COUNT - 1; i++)
	{
		pWinJSPKAck->add_winseats(pWinJSPKMsg->anWinSeat[ i ]);
		pWinJSPKAck->add_windouble(pWinJSPKMsg->cnDouble[ i ]);
		pWinJSPKAck->add_winmode(pWinJSPKMsg->anWinMode[ i ]);
	}
	pWinJSPKAck->set_winseatcount(pWinJSPKMsg->cnWinSeat);
	pWinJSPKAck->set_paoseat(pWinJSPKMsg->nPaoSeat);
	pWinJSPKAck->set_wintileid(pWinJSPKMsg->nWinTile);

	// 在数据都添置好以后，来计算包体长度
	protoMsgAck.header.dwLength = protoMsgAck.protoAckMsg.ByteSize();
}

/************************************************************************/
/*    赢牌详细信息	                                                   */
/************************************************************************/
void TKMahjongProtobufFactory::SerializeNetMsgWinDetailAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg)
{
	// 组装消息头
	SerializeProtobufAckMsgHead( pMsg, protoMsgAck );

	TKACKMAHJONGWINDETAIL* pWinDetailMsg = (TKACKMAHJONGWINDETAIL*)pMsg;

	// 获取真正的消息体
	MahJongJSPKAckMsg* pJSPKAckMsg = protoMsgAck.protoAckMsg.mutable_mahjongjspk_ack_msg();
	ASSERT(NULL != pJSPKAckMsg);

	// 设置MatchID
	SerializeProtobufSetMatchID( pJSPKAckMsg );

	MahJongWinDetailAck* pWinDetailAck = pJSPKAckMsg->mutable_mahjongwindetail_ack_msg();
	ASSERT(NULL != pWinDetailAck);

	// 清空消息体 防止重入消息错误
	pWinDetailAck->Clear();

	pWinDetailAck->set_winseat(pWinDetailMsg->nWinSeat);
	pWinDetailAck->set_paoseat(pWinDetailMsg->nPaoSeat);
	pWinDetailAck->set_wintile(pWinDetailMsg->nWinTile);
	pWinDetailAck->set_basescore(pWinDetailMsg->nBaseScore);
	pWinDetailAck->set_time(pWinDetailMsg->nTime);


	WinInfo* pWinInfo = pWinDetailAck->mutable_wininfo();
	ASSERT(NULL != pWinInfo);
	{
		pWinInfo->set_maxfans(pWinDetailMsg->sWinInfo.nMaxFans);
		for(int j = 0; j < MAXFANS; j++)
		{
			pWinInfo->add_fans(pWinDetailMsg->sWinInfo.anFans[ j ]);
		}
		pWinInfo->set_resultant(pWinDetailMsg->sWinInfo.nResultant);
		for ( int j = 0; j < 6; j++)
		{
			StoneGroup* pStoneGroup = pWinInfo->add_groups( );
			ASSERT(NULL != pStoneGroup);

			for ( int n = 0; n < 4; n++)
			{
				Stone *pStone = pStoneGroup->add_stone( );
				ASSERT(NULL != pStone);

				pStone->set_id( pWinDetailMsg->sWinInfo.asGroup[ j ].asStone [ n ].nID );
				pStone->set_color( pWinDetailMsg->sWinInfo.asGroup[ j ].asStone [ n ].nColor );
				pStone->set_what( pWinDetailMsg->sWinInfo.asGroup[ j ].asStone [ n ].nWhat );
			}
			pStoneGroup->set_groupstyle( pWinDetailMsg->sWinInfo.asGroup[ j ].nGroupStyle );
		}

		for ( int j = 0; j < MAX_HAND_COUNT; j++)
		{
			Stone* pHandStone = pWinInfo->add_handstone( );
			ASSERT(NULL != pHandStone);
			pHandStone->set_id( pWinDetailMsg->sWinInfo.asHandStone[ j ].nID);
			pHandStone->set_color( pWinDetailMsg->sWinInfo.asHandStone[ j ].nColor );
			pHandStone->set_what( pWinDetailMsg->sWinInfo.asHandStone[ j ].nWhat );
		}

		pWinInfo->set_groupcount( pWinDetailMsg->sWinInfo.cnGroups );
		pWinInfo->set_wintileid( pWinDetailMsg->sWinInfo.nHuTileID );
		for ( int j = 0; j < 8; j++)
		{
			Stone* pFlowerStone = pWinInfo->add_flowers( );
			ASSERT(NULL != pFlowerStone);
			pFlowerStone->set_id( pWinDetailMsg->sWinInfo.asFlower[ j ].nID);
			pFlowerStone->set_color( pWinDetailMsg->sWinInfo.asFlower[ j ].nColor );
			pFlowerStone->set_what( pWinDetailMsg->sWinInfo.asFlower[ j ].nWhat );
		}
		pWinInfo->set_flowercount( pWinDetailMsg->sWinInfo.cnFlower );
		pWinInfo->set_quanwind( pWinDetailMsg->sWinInfo.nQuanWind );
		pWinInfo->set_menwind( pWinDetailMsg->sWinInfo.nMenWind );
		pWinInfo->set_showgroups( pWinDetailMsg->sWinInfo.cnShowGroups );
		pWinInfo->set_winmode( pWinDetailMsg->sWinInfo.nWinMode );
		pWinInfo->set_scoreoffan ( pWinDetailMsg->sWinInfo.nScoreOfFan );
	}

	for ( int j = 0; j < MAX_PLAYER_COUNT; j++)
	{
		ScoreDetail* pScoreDetail = pWinDetailAck->add_scoredetails( );
		ASSERT(NULL != pWinInfo);

		pScoreDetail->set_leftscore( pWinDetailMsg->asScoreDetail[ j ].nLeftScore);
		for (int n = 0; n < 7; n++)
		{
			pScoreDetail->add_score(pWinDetailMsg->asScoreDetail[ j ].anScore[ n ]);
		}
	}

	// 在数据都添置好以后，来计算包体长度
	protoMsgAck.header.dwLength = protoMsgAck.protoAckMsg.ByteSize();
}

/************************************************************************/
/*    赢牌详细信息	                                                   */
/************************************************************************/
void TKMahjongProtobufFactory::SerializeNetMsgWinDetailExAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg)
{
	// 组装消息头
	SerializeProtobufAckMsgHead( pMsg, protoMsgAck );

	TKACKMAHJONGWINDETAILEX* pWinDetailExMsg = (TKACKMAHJONGWINDETAILEX*)pMsg;

	// 获取真正的消息体
	MahJongJSPKAckMsg* pJSPKAckMsg = protoMsgAck.protoAckMsg.mutable_mahjongjspk_ack_msg();
	ASSERT(NULL != pJSPKAckMsg);

	// 设置MatchID
	SerializeProtobufSetMatchID( pJSPKAckMsg );

	MahJongWinDetailExJSPKAck* pWinDetailExJSPKAck = pJSPKAckMsg->mutable_mahjongwindetailex_ack_msg();
	ASSERT(NULL != pWinDetailExJSPKAck);

	// 清空消息体 防止重入消息错误
	pWinDetailExJSPKAck->Clear();

	pWinDetailExJSPKAck->set_winseat(pWinDetailExMsg->nWinSeat);
	pWinDetailExJSPKAck->set_windouble(pWinDetailExMsg->winDouble);
	pWinDetailExJSPKAck->set_paoseat(pWinDetailExMsg->nPaoSeat);
	pWinDetailExJSPKAck->set_wintile(pWinDetailExMsg->nWinTile);
	pWinDetailExJSPKAck->set_basescore(pWinDetailExMsg->nBaseScore);
	pWinDetailExJSPKAck->set_time(pWinDetailExMsg->nTime);


	WinInfo* pWinInfo = pWinDetailExJSPKAck->mutable_wininfo();
	ASSERT(NULL != pWinInfo);
	{
		pWinInfo->set_maxfans(pWinDetailExMsg->sWinInfo.nMaxFans);
		for(int j = 0; j < MAXFANS; j++)
		{
			pWinInfo->add_fans(pWinDetailExMsg->sWinInfo.anFans[ j ]);
		}
		pWinInfo->set_resultant(pWinDetailExMsg->sWinInfo.nResultant);
		for ( int j = 0; j < 6; j++)
		{
			StoneGroup* pStoneGroup = pWinInfo->add_groups( );
			ASSERT(NULL != pStoneGroup);

			for ( int n = 0; n < 4; n++)
			{
				Stone *pStone = pStoneGroup->add_stone( );
				ASSERT(NULL != pStone);

				pStone->set_id( pWinDetailExMsg->sWinInfo.asGroup[ j ].asStone [ n ].nID );
				pStone->set_color( pWinDetailExMsg->sWinInfo.asGroup[ j ].asStone [ n ].nColor );
				pStone->set_what( pWinDetailExMsg->sWinInfo.asGroup[ j ].asStone [ n ].nWhat );
			}
			pStoneGroup->set_groupstyle( pWinDetailExMsg->sWinInfo.asGroup[ j ].nGroupStyle );
		}

		for ( int j = 0; j < MAX_HAND_COUNT; j++)
		{
			Stone* pHandStone = pWinInfo->add_handstone( );
			ASSERT(NULL != pHandStone);
			pHandStone->set_id( pWinDetailExMsg->sWinInfo.asHandStone[ j ].nID);
			pHandStone->set_color( pWinDetailExMsg->sWinInfo.asHandStone[ j ].nColor );
			pHandStone->set_what( pWinDetailExMsg->sWinInfo.asHandStone[ j ].nWhat );
		}

		pWinInfo->set_groupcount( pWinDetailExMsg->sWinInfo.cnGroups );
		pWinInfo->set_wintileid( pWinDetailExMsg->sWinInfo.nHuTileID );
		for ( int j = 0; j < 8; j++)
		{
			Stone* pFlowerStone = pWinInfo->add_flowers( );
			ASSERT(NULL != pFlowerStone);
			pFlowerStone->set_id( pWinDetailExMsg->sWinInfo.asFlower[ j ].nID);
			pFlowerStone->set_color( pWinDetailExMsg->sWinInfo.asFlower[ j ].nColor );
			pFlowerStone->set_what( pWinDetailExMsg->sWinInfo.asFlower[ j ].nWhat );
		}
		pWinInfo->set_flowercount( pWinDetailExMsg->sWinInfo.cnFlower );
		pWinInfo->set_quanwind( pWinDetailExMsg->sWinInfo.nQuanWind );
		pWinInfo->set_menwind( pWinDetailExMsg->sWinInfo.nMenWind );
		pWinInfo->set_showgroups( pWinDetailExMsg->sWinInfo.cnShowGroups );
		pWinInfo->set_winmode( pWinDetailExMsg->sWinInfo.nWinMode );
		pWinInfo->set_scoreoffan ( pWinDetailExMsg->sWinInfo.nScoreOfFan );
	}

	for ( int j = 0; j < MAX_PLAYER_COUNT; j++)
	{
		ScoreDetailEx* pScoreDetail = pWinDetailExJSPKAck->add_scoredetails( );
		ASSERT(NULL != pWinInfo);

		pScoreDetail->set_leftscore( pWinDetailExMsg->asScoreDetail[ j ].nLeftScore);
		for (int n = 0; n < 8; n++)
		{
			pScoreDetail->add_score(pWinDetailExMsg->asScoreDetail[ j ].anScore[ n ]);
		}
	}

	// 在数据都添置好以后，来计算包体长度
	protoMsgAck.header.dwLength = protoMsgAck.protoAckMsg.ByteSize();
}

/************************************************************************/
/*    托管消息	                                                   */
/************************************************************************/
void TKMahjongProtobufFactory::SerializeNetMsgTrustPlayAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg)
{
	// 组装消息头
	SerializeProtobufAckMsgHead( pMsg, protoMsgAck );

	TKREQTRUSTPLAY* pTrustPlayMsg = (TKREQTRUSTPLAY*)pMsg;

	// 获取真正的消息体
	MahJongJSPKAckMsg* pJSPKAckMsg = protoMsgAck.protoAckMsg.mutable_mahjongjspk_ack_msg();
	ASSERT(NULL != pJSPKAckMsg);

	// 设置MatchID
	SerializeProtobufSetMatchID( pJSPKAckMsg );

	MahJongTrustPlayAck* pTrustPlayAck = pJSPKAckMsg->mutable_mahjongtrustplay_ack_msg();
	ASSERT(NULL != pTrustPlayAck);

	// 清空消息体 防止重入消息错误
	pTrustPlayAck->Clear();

	// 设置消息体中的所有参数
	pTrustPlayAck->set_userid(pTrustPlayMsg->dwUserID);
	pTrustPlayAck->set_type(pTrustPlayMsg->dwType);

	// 在数据都添置好以后，来计算包体长度
	protoMsgAck.header.dwLength = protoMsgAck.protoAckMsg.ByteSize();
}


/************************************************************************/
/*    分数改变消息	                                                   */
/************************************************************************/
void TKMahjongProtobufFactory::SerializeNetMsgScoreChangeAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg)
{
	// 组装消息头
	SerializeProtobufAckMsgHead( pMsg, protoMsgAck );

	TKREQSCORECHANGE* pScoreChangeMsg = (TKREQSCORECHANGE*)pMsg;

	// 获取真正的消息体
	MahJongJSPKAckMsg* pJSPKAckMsg = protoMsgAck.protoAckMsg.mutable_mahjongjspk_ack_msg();
	ASSERT(NULL != pJSPKAckMsg);

	// 设置MatchID
	SerializeProtobufSetMatchID( pJSPKAckMsg );

	MahJongScoreChangeAck* pScoreChangeAck = pJSPKAckMsg->mutable_mahjongscorechange_ack_msg();
	ASSERT(NULL != pScoreChangeAck);

	// 清空消息体 防止重入消息错误
	pScoreChangeAck->Clear();

	// 设置消息体中的所有参数
	
	for ( int i = 0; i < MAX_PLAYER_COUNT; i++)
	{
		pScoreChangeAck->add_incremental(pScoreChangeMsg->anIncremental[ i ]);
	}
	pScoreChangeAck->set_type(pScoreChangeMsg->type);

	// 在数据都添置好以后，来计算包体长度
	protoMsgAck.header.dwLength = protoMsgAck.protoAckMsg.ByteSize();
}

/************************************************************************/
/*    通知亮牌消息                                                      */
/************************************************************************/
void TKMahjongProtobufFactory::SerializeNetMsgPlayerTiles(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg)
{
	// 组装消息头
	SerializeProtobufAckMsgHead( pMsg, protoMsgAck );

	TKACKTILES* pPlayerTilesMsg = (TKACKTILES*)pMsg;

	// 获取真正的消息体
	MahJongJSPKAckMsg* pJSPKAckMsg = protoMsgAck.protoAckMsg.mutable_mahjongjspk_ack_msg();
	ASSERT(NULL != pJSPKAckMsg);

	// 设置MatchID
	SerializeProtobufSetMatchID( pJSPKAckMsg );

	MahJongPlayerTilesJSPKAck* pPlayerTileAck = pJSPKAckMsg->mutable_mahjongplayertiles_ack_msg();
	ASSERT(NULL != pPlayerTileAck);

	// 清空消息体 防止重入消息错误
	pPlayerTileAck->Clear();

	// 设置消息体中的所有参数
	pPlayerTileAck->set_seat(pPlayerTilesMsg->seat);
	pPlayerTileAck->set_handtilecount(pPlayerTilesMsg->cnHandTile);

	for( int i = 0; i < MAX_HAND_TILE_COUNT; i++)
	{
		pPlayerTileAck->add_handtiles( pPlayerTilesMsg->anHandTile[ i ] );
	}

	// 在数据都添置好以后，来计算包体长度
	protoMsgAck.header.dwLength = protoMsgAck.protoAckMsg.ByteSize();
}

/************************************************************************/
/*    玩家吃碰杠的牌信息                                                       */
/************************************************************************/
void TKMahjongProtobufFactory::SerializeNetMsgShowTiles(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg)
{
	// 组装消息头
	SerializeProtobufAckMsgHead( pMsg, protoMsgAck );

	TKACKSHOWTILES* pShowTilesMsg = (TKACKSHOWTILES*)pMsg;

	// 获取真正的消息体
	MahJongJSPKAckMsg* pJSPKAckMsg = protoMsgAck.protoAckMsg.mutable_mahjongjspk_ack_msg();
	ASSERT(NULL != pJSPKAckMsg);

	// 设置MatchID
	SerializeProtobufSetMatchID( pJSPKAckMsg );

	MahJongPlayerShowTilesJSPKAck* pShowTilesAck = pJSPKAckMsg->mutable_mahjongplayershowtiles_ack_msg();
	ASSERT(NULL != pShowTilesAck);

	// 清空消息体 防止重入消息错误
	pShowTilesAck->Clear();

	// 设置消息体中的所有参数
	pShowTilesAck->set_seat(pShowTilesMsg->seat);
	pShowTilesAck->set_showcount(pShowTilesMsg->cnShow);

	for( int i = 0; i < 24; i++)
	{
		pShowTilesAck->add_showtiles( pShowTilesMsg->anShowTiles[i] );
	}

	// 在数据都添置好以后，来计算包体长度
	protoMsgAck.header.dwLength = protoMsgAck.protoAckMsg.ByteSize();
}

/************************************************************************/
/*    检查超时消息                                                      */
/************************************************************************/
void TKMahjongProtobufFactory::SerializeNetMsgCheckTimeOut(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg)
{
	// proto 文件中 不支持此消息
}

/************************************************************************/
/*    加倍消息                                                      */
/************************************************************************/
void TKMahjongProtobufFactory::SerializeNetMsgDoublingAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg)
{
	// 组装消息头
	SerializeProtobufAckMsgHead( pMsg, protoMsgAck );

	TKACKDOUBLING* pDoublingMsg = (TKACKDOUBLING*)pMsg;

	// 获取真正的消息体
	MahJongJSPKAckMsg* pJSPKAckMsg = protoMsgAck.protoAckMsg.mutable_mahjongjspk_ack_msg();
	ASSERT(NULL != pJSPKAckMsg);

	// 设置MatchID
	SerializeProtobufSetMatchID( pJSPKAckMsg );

	MahJongDoubleJSPKAck * pDoublingAck = pJSPKAckMsg->mutable_mahjongdouble_ack_msg();
	ASSERT(NULL != pDoublingAck);

	// 清空消息体 防止重入消息错误
	pDoublingAck->Clear();

	// 设置消息体中的所有参数
	pDoublingAck->set_seat(pDoublingMsg->seat);
	pDoublingAck->set_count(pDoublingMsg->count);
	pDoublingAck->set_tileid(pDoublingMsg->tileId);

	// 在数据都添置好以后，来计算包体长度
	protoMsgAck.header.dwLength = protoMsgAck.protoAckMsg.ByteSize();
}


/************************************************************************/
/*    加番牌信息														*/
/************************************************************************/
void TKMahjongProtobufFactory::SerializeNetMsgSpecialTileAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg)
{
	// 组装消息头
	SerializeProtobufAckMsgHead( pMsg, protoMsgAck );

	TKACKMAHJONGSPECIALTILE* pSpecialTileMsg = (TKACKMAHJONGSPECIALTILE*)pMsg;

	// 获取真正的消息体
	MahJongJSPKAckMsg* pJSPKAckMsg = protoMsgAck.protoAckMsg.mutable_mahjongjspk_ack_msg();
	ASSERT(NULL != pJSPKAckMsg);

	// 设置MatchID
	SerializeProtobufSetMatchID( pJSPKAckMsg );

	MahJongJSPKSpecialTileAck * pSpecialTileAck = pJSPKAckMsg->mutable_mahjongjspkspecialtile_ack_msg();
	ASSERT(NULL != pSpecialTileAck);

	// 清空消息体 防止重入消息错误
	pSpecialTileAck->Clear();

	// 设置消息体中的所有参数
	pSpecialTileAck->set_id(pSpecialTileMsg->nSpecialTileID);
	pSpecialTileAck->set_fan(pSpecialTileMsg->nSpecialTileFan);
	pSpecialTileAck->set_count(pSpecialTileMsg->nSpecialTileCount);

	// 在数据都添置好以后，来计算包体长度
	protoMsgAck.header.dwLength = protoMsgAck.protoAckMsg.ByteSize();
}

/************************************************************************/
/*    加番牌个数														*/
/************************************************************************/
void TKMahjongProtobufFactory::SerializeNetMsgSpecialCountAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg)
{
	// 组装消息头
	SerializeProtobufAckMsgHead( pMsg, protoMsgAck );

	TKACKMAHJONGSPECIALCOUNT* pSpecialCountMsg = (TKACKMAHJONGSPECIALCOUNT*)pMsg;

	// 获取真正的消息体
	MahJongJSPKAckMsg* pJSPKAckMsg = protoMsgAck.protoAckMsg.mutable_mahjongjspk_ack_msg();
	ASSERT(NULL != pJSPKAckMsg);

	// 设置MatchID
	SerializeProtobufSetMatchID( pJSPKAckMsg );

	MahJongJSPKSpecialCountAck * pSpecialCountAck = pJSPKAckMsg->mutable_mahjongjspkspecialcount_ack_msg();
	ASSERT(NULL != pSpecialCountAck);

	// 清空消息体 防止重入消息错误
	pSpecialCountAck->Clear();

	// 设置消息体中的所有参数
	pSpecialCountAck->set_count(pSpecialCountMsg->nSpecialCount);

	// 在数据都添置好以后，来计算包体长度
	protoMsgAck.header.dwLength = protoMsgAck.protoAckMsg.ByteSize();
}

/************************************************************************/
/*    奖花消息														*/
/************************************************************************/
void TKMahjongProtobufFactory::SerializeNetMsgLuckyTileAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg)
{
	// 组装消息头
	SerializeProtobufAckMsgHead( pMsg, protoMsgAck );

	TKACKMAHJONGLUCKYTILE* pLuckyTileMsg = (TKACKMAHJONGLUCKYTILE*)pMsg;

	// 获取真正的消息体
	MahJongJSPKAckMsg* pJSPKAckMsg = protoMsgAck.protoAckMsg.mutable_mahjongjspk_ack_msg();
	ASSERT(NULL != pJSPKAckMsg);

	// 设置MatchID
	SerializeProtobufSetMatchID( pJSPKAckMsg );

	MahJongJSPKLuckyTileAck * pLuckyTileAck = pJSPKAckMsg->mutable_mahjongjspkluckytile_ack_msg();
	ASSERT(NULL != pLuckyTileAck);

	// 清空消息体 防止重入消息错误
	pLuckyTileAck->Clear();

	// 设置消息体中的所有参数
	pLuckyTileAck->set_luckytilecount(pLuckyTileMsg->nLuckyTileCount);
	for (int i = 0; i < pLuckyTileMsg->nLuckyTileCount; i++ )
	{
		pLuckyTileAck->add_luckytileid(pLuckyTileMsg->anLuckyTileID[i]);
	}
	pLuckyTileAck->set_hittilecount(pLuckyTileMsg->nHitTileCount);
	for (int i = 0; i < pLuckyTileMsg->nHitTileCount; i++ )
	{
		pLuckyTileAck->add_hittileid(pLuckyTileMsg->anHitTileID[i]);
	}
	pLuckyTileAck->set_eachluckytilefan(pLuckyTileMsg->nEachLuckyTileFan);

	// 在数据都添置好以后，来计算包体长度
	protoMsgAck.header.dwLength = protoMsgAck.protoAckMsg.ByteSize();
}

/************************************************************************/
/*    超时检测													*/
/************************************************************************/
void TKMahjongProtobufFactory::SerializeNetMsgCheckTimeOutAck(TKACKMAHJONGPROTOBUF& protoMsgAck, TKHEADER* pMsg)
{
	// 组装消息头
	SerializeProtobufAckMsgHead( pMsg, protoMsgAck );

	TKREQCHECKTIMEOUT* pCheckTimeOutMsg = (TKREQCHECKTIMEOUT*)pMsg;

	// 获取真正的消息体
	MahJongJSPKAckMsg* pJSPKAckMsg = protoMsgAck.protoAckMsg.mutable_mahjongjspk_ack_msg();
	ASSERT(NULL != pJSPKAckMsg);

	// 设置MatchID
	SerializeProtobufSetMatchID( pJSPKAckMsg );

	MahJongCheckTimeOutAck * pCheckTimeOutAck = pJSPKAckMsg->mutable_mahjongchecktimeout_ack_msg();
	ASSERT(NULL != pCheckTimeOutAck);

	// 清空消息体 防止重入消息错误
	pCheckTimeOutAck->Clear();

	pCheckTimeOutAck->set_waittime(pCheckTimeOutMsg->nWaitSecond);
	pCheckTimeOutAck->set_userid(pCheckTimeOutMsg->m_dwUserID);

	// 在数据都添置好以后，来计算包体长度
	protoMsgAck.header.dwLength = protoMsgAck.protoAckMsg.ByteSize();
	//// for test
	//TKWriteLog("收到 checktimeout req 举报玩家为[%d] time = %u", 
	//	pCheckTimeOutAck->userid(),
	//	GetTickCount());
}

