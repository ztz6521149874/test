#include "TKMahjongGame.h"
#include "TKMahjongServerObj.h"
#include "TKMahjongDefine.h"
#include "GameMachine.h"
#include "ScoreCalculator.h"
#include "BloodyScoreCalculator.h"
#include "UptostandCalculator.h"
#include "RequestObj.h"
#include "tkgameplayer.h"
#include "TKMahjongProtocol.h"
#include "TKMahjongServerPlayer.h"
#include "TKMahjongGamePlayer.h"
#include <TKStat.h>

#ifndef ASSERT
#define ASSERT	assert
#endif

// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
CTKMahjongGame::CTKMahjongGame(void)
{
    m_pGameMachine = NULL;
    m_pMahjongObj = NULL;
    m_pCalculator = NULL;
    m_pRequestObj = NULL;
    m_pProtoFactory = NULL;
    m_nCashFlow = 0;
    m_nTax = 0;

    m_vecScoreSysCompensate.clear();
    for (int i = 0; i < MAX_PLAYER_COUNT; ++i)
    {
        m_vecScoreSysCompensate.push_back(0);   // 初始化系统赔付
    }
}

// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
CTKMahjongGame::~CTKMahjongGame(void)
{
	if ( NULL != m_pGameMachine )
	{
		delete m_pGameMachine;
		m_pGameMachine = NULL;
	}
	if ( NULL != m_pMahjongObj )
	{
		delete m_pMahjongObj;
		m_pMahjongObj = NULL;
	}
	if ( NULL != m_pCalculator )
	{
		delete m_pCalculator;
		m_pCalculator = NULL;
	}
	if ( NULL != m_pRequestObj )
	{
		delete m_pRequestObj;
		m_pRequestObj = NULL;
	}
	if ( NULL != m_pProtoFactory)
	{
		delete m_pProtoFactory;
		m_pProtoFactory = NULL;
	}

    m_vecScoreSysCompensate.clear();




	


#ifdef VCZH_CHECK_MEMORY_LEAKS
	_CrtDumpMemoryLeaks();

#endif
}



// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
BOOL CTKMahjongGame::OnInitialUpdate()
{
	// 调用基类函数，初始化基类数据
	if( !CTKGame::OnInitialUpdate() )
	{
		return FALSE;
	}

	// 初始化游戏规则对象
//	TCHAR szProperty[ 256 ] = "2,1,127,2,2,3,3,1,1,1,1,0,0,1,1,1,0,15,10,1,0,1,0,0,0,0,30,150,420744970,1";
	if ( !m_GameRule.InitInstance( m_pRuler->szPropertyEx ) )
	{
		return FALSE;
	} 

	// 中转类初始化
	if ( !m_GameProxy.InitInstance( this ) )
	{
		return FALSE;
	}

	// 初始化分数计算子
	int cnPlayer = CTKGame::GetGamePlayerCount();
	m_pCalculator = CreateCalculator( &m_GameRule, cnPlayer, m_nScoreBase );
	TKVerifyMemoryAlloc( m_pCalculator, __FILE__, __LINE__ );

	// 初始化请求信息对象
	m_pRequestObj = new CRequestObj;
	TKVerifyMemoryAlloc( m_pRequestObj, __FILE__, __LINE__ );
	m_pRequestObj->SetRequestWaitTime( m_GameRule.GetRule( RULE_REQUEST_WAITTIME ) );
	m_pRequestObj->ResetRequest( 0 , 0 ) ;


	// 初始化麻将对象
	m_pMahjongObj = new CTKMahjongServerObj( this ); 
	TKVerifyMemoryAlloc( m_pMahjongObj, __FILE__, __LINE__ );
    if (!m_pMahjongObj->InitInstance(this, &m_GameRule))
    {
        return FALSE;
    }

	// 添加玩家
    int nPlayerCount = CTKGame::GetGamePlayerCount();
	for ( int i = 0; i < nPlayerCount; i++ )
	{
		CTKMahjongGamePlayer* pPlayer = dynamic_cast<CTKMahjongGamePlayer*>( CTKGame::GetGamePlayerByIdx( i ) );
		if (NULL != pPlayer)
		{
			m_pMahjongObj->AddPlayer(pPlayer);
			pPlayer->ResetData();
		}
	}

	// 初始化状态机
	m_pGameMachine = new CGameMachine( this, m_pMahjongObj );
	TKVerifyMemoryAlloc( m_pGameMachine, __FILE__, __LINE__ );
	if ( !m_pGameMachine->OnInitalUpdate( &m_GameRule ) )
	{
		return FALSE;
	}

	// 初始化protobuf转换器
	m_pProtoFactory = new TKMahjongProtobufFactory( &m_GameProxy );
	TKVerifyMemoryAlloc( m_pProtoFactory, __FILE__, __LINE__ );
	return TRUE;
}

int  CTKMahjongGame::GetLastHandSaveInfoLen()
{
	return sizeof(int);
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CTKMahjongGame::OnPlayerTickCount(CTKGamePlayer *pGamePlayer)
{
	for ( int i = 0;  i < MAX_PLAYER_COUNT; ++i )
	{
		if ( m_pRequestObj->CheckOverTime( i ) )
		{
			CTKGamePlayer *pOverTimePlayer = GetGamePlayerBySeat(i);
			if (pOverTimePlayer)
			{
				// 断线状态
				SetPlayerBreak( pOverTimePlayer->m_nSeatOrder, true );
				
				// 设置这个玩家托管了
				TKREQTRUSTPLAY reqTrustPlay = { 0 } ;
				reqTrustPlay.header.dwType   = REQ_TYPE( TK_MSG_TRUSTPLAY ) ;
				reqTrustPlay.header.dwLength = MSG_LENGTH( TKREQTRUSTPLAY ) ;
				reqTrustPlay.dwUserID = pOverTimePlayer->m_dwUserID;
				reqTrustPlay.dwType = TRUST_OVERTIME;
				OnPlayerMsg( pOverTimePlayer->m_dwUserID, pOverTimePlayer->m_nSeatOrder, ( PTKHEADER )&reqTrustPlay );

				//用户超时，有可能断线了
				InvokeAddBreakUser(i);
			}
		}
	}

	// 上面只检查了有请求的情况，对于没有发出请求的情况（开牌）无能为力，这里让状态机再检查一下这种情况
	m_pGameMachine->OnTickCount();
}

//设置用户的游戏结果
void CTKMahjongGame::SetGameResult
(
    int in_nSeat,
    int in_nWinPlus,
    int in_nLossPlus,
    int in_nDrawPlus,
    int& io_nScorePlus, 
    BOOL in_bValideHand /*= TRUE*/, 
    BOOL in_bLevyTax /*= TRUE*/, 
    WORD in_wGameResult /*= TK_UNKNOWN*/
)
{
    if (Rule()->GetRule(RULE_SCORE_LOWER_LIMIT) == 1
        && (__super::m_nScoreType == TK_GAME_SCORETYPE_CHIP
        || __super::m_nScoreType == TK_GAME_SCORETYPE_LIFE))
    {
        int nScorePlus = io_nScorePlus;
        CTKGamePlayer* pGamePlayer = GetGamePlayerBySeat(in_nSeat);
        if (pGamePlayer != NULL)
        {
            if (nScorePlus > 0
                && m_vecScoreSysCompensate[in_nSeat] < 0)
            {// 玩家有分，但之前有系统欠分
                if (nScorePlus > -m_vecScoreSysCompensate[in_nSeat])
                {// 足够偿还
                    nScorePlus += m_vecScoreSysCompensate[in_nSeat];
                    m_vecScoreSysCompensate[in_nSeat] = 0;
                }
                else if (nScorePlus < -m_vecScoreSysCompensate[in_nSeat])
                {// 不够偿还
                    m_vecScoreSysCompensate[in_nSeat] += nScorePlus;
                    nScorePlus = 0;
                }
                else
                {// 正好够偿还
                    nScorePlus = 0;
                    m_vecScoreSysCompensate[in_nSeat] = 0;
                }
            }

            int nPlayerScoreLeft = pGamePlayer->m_nScore;
            int nScoreResultExcept = nPlayerScoreLeft + nScorePlus;

            if (nScoreResultExcept < 0)
            {// 输到0
                m_vecScoreSysCompensate[in_nSeat] += nScoreResultExcept;    // 累积玩家欠系统的积分

                nScorePlus = -nPlayerScoreLeft;
            }
        }

        //////////////////////////////////////////////////////////////////////////
        int nScorePlusOri = nScorePlus;
        __super::SetGameResult
            (
            in_nSeat, 
            in_nWinPlus, 
            in_nLossPlus, 
            in_nDrawPlus, 
            nScorePlus, 
            in_bValideHand, 
            in_bLevyTax, 
            in_wGameResult
            );
        io_nScorePlus -= nScorePlusOri - nScorePlus;
    }
    else
    {
        __super::SetGameResult
            (
            in_nSeat, 
            in_nWinPlus, 
            in_nLossPlus, 
            in_nDrawPlus, 
            io_nScorePlus, 
            in_bValideHand, 
            in_bLevyTax, 
            in_wGameResult
            );
    }
}

bool CTKMahjongGame::IsEndGameEarly(int nSeat)
{
	int curScore = m_pCalculator->GetPlayerLeftScoer(nSeat);

	return curScore <= 0;
}

// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CTKMahjongGame::InitAllGameData()
{
	m_dwGameBegin = GetTickCount();
	ResetGameData() ;	// 重置游戏数据
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CTKMahjongGame::ResetGameData()
{
	m_pRequestObj->ResetRequest( 0 , 0 ) ;

	m_pMahjongObj->ResetGameData() ;
	m_pMahjongObj->ResetDoubleData();

	// 重置算分子
	m_pCalculator->ResetData() ;

    std::vector<int>::iterator itr = m_vecScoreSysCompensate.begin();
    for (; itr!=m_vecScoreSysCompensate.end(); ++itr)
    {
        *itr = 0;
    }
}




// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
void CTKMahjongGame::OnPlayerArrived( CTKGamePlayer *pGamePlayer )
{
	// 调用基类函数，广播用户到达消息给已经进入的客户端
	CTKGame::OnPlayerArrived( pGamePlayer );
}



// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
BOOL CTKMahjongGame::OnGameBegin()
{
	//
	if (GetGamePlayerCount() != MAX_PLAYER_COUNT)
	{
		TKWriteLog("CTKMahjongGame::OnGameBegin PlayerCount=%d!=%d, MPID/T/M/S/R=%d/%u/%u/%d/%d",
			GetGamePlayerCount(), MAX_PLAYER_COUNT, m_nProductID, m_dwTourneyID, m_dwMatchID, m_wStageID, m_wRoundID);

		NotifyGameOver(TK_GAMEOVERTYPE_NORNAL);
		return TRUE;
	}

	// 设置游戏的初始信息
	InitAllGameData() ;
    _sendGameBeginAck();

	// 启动状态机
	CEvent event( EVENT_START_GAME );
	m_pGameMachine->HandleEvent( &event );

	if ( AllIsBotTrustPlay() )
	{
		// 全都是机器人代打的,每个人扣分直接结束游戏
		int nScore = -5 * m_nScoreBase;
		for ( int i = 0; i < MAX_PLAYER_COUNT; ++i )
		{
			m_pCalculator->AddPlayerScore( i, nScore );
		}
		m_pGameMachine->Stop();
	}
	else
	{
		// 启动时就被机器人托管的人切换到本服务器托管
		TKREQTRUSTPLAY reqTrustPlay = { 0 } ;
		reqTrustPlay.header.dwType   = REQ_TYPE( TK_MSG_TRUSTPLAY ) ;
		reqTrustPlay.header.dwLength = MSG_LENGTH( TKREQTRUSTPLAY ) ;
		reqTrustPlay.dwType = TRUST_OFFLINE;

		for ( int i = 0; i < MAX_PLAYER_COUNT; ++i )
		{
			if ( IsBotTrustPlay( i ) )
			{
				CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerBySeat( i );
				reqTrustPlay.dwUserID = pPlayer->UserID();
				OnPlayerMsg( reqTrustPlay.dwUserID, i, ( PTKHEADER )&reqTrustPlay );
			}
		}
	}

	return TRUE;
}

void CTKMahjongGame::_sendGameBeginAck()
{
    TKMobileAckMsg rAck;
    MahJongJSPKAckMsg *pMahJong = rAck.mutable_mahjongjspk_ack_msg();
    MahJongGameBeginAck *pMsg = pMahJong->mutable_mahjonggamebegin_ack_msg();
    pMsg->set_nullvalue(1);
    for (int i = 0;i < MAX_PLAYER_COUNT;++i)
    {
        CTKMahjongServerPlayer * pPlayer = (CTKMahjongServerPlayer*)(m_pMahjongObj->GetPlayerBySeat(i));
        if (NULL == pPlayer)
        {
            continue;
        }
        pMsg->add_wishcoin(0);
        if (TKGenRandom() % 10000 < m_GameRule.GetRule(RULE_WISH_COIN_RATE))
        {
            pPlayer->AddGrowValue(GV_WishCoin);
            pMsg->set_wishcoin(i, 1);
        }

        pMsg->add_slivercoin(0);
        if (TKGenRandom() % 10000 < m_GameRule.GetRule(RULE_WISH_SLIVER_RATE))
        {
            pPlayer->AddGrowValue(GV_SilverCoin);
            pMsg->set_slivercoin(i, 1);
        }

        pMsg->add_coppercoin(0);
        if (TKGenRandom() % 10000 < m_GameRule.GetRule(RULE_WISH_COPPER_RATE))
        {
            pPlayer->AddGrowValue(GV_CopperCoin);
            pMsg->set_coppercoin(i, 1);
        }
    }
    SendMsg(rAck);
}

BOOL CTKMahjongGame::OnPlayerMsg( DWORD dwUserID, int nSeatOrder, PTKHEADER pMsg )
{
	//先调用基类的消息处理函数，如果返回TRUE表示基类已经处理，那么本函数直接返回TRUE
	if ( CTKGame::OnPlayerMsg( dwUserID, nSeatOrder, pMsg ) ) 
	{
		return TRUE;
	}

	PTKHEADER pTransMsg = nullptr;
	BOOL bUseEndDel = FALSE;

	// 检测一下接收的数据包 是否protobuf协议
	if ( REQ_TYPE( TK_MSG_MAHJONG_PROTOBUF ) == pMsg->dwType )
	{
		if ( !m_pProtoFactory->DeserializeProtobuf( pMsg, &pTransMsg ) )
		{
			TKWriteLog( "[%s:%d] Msg Sender( %d ), ProtoFactory DeserializeNetCToSMsg Error!!", __FILE__ , __LINE__, dwUserID) ;
			return false;
		}

		// 转换成功  
		bUseEndDel = TRUE;
	}

	// 如果不是 protobuf 协议 默认处理原来协议
	if ( pTransMsg == nullptr )
	{
		pTransMsg = pMsg;
		bUseEndDel = FALSE;
	}

	if ( REQ_TYPE( TK_MSG_TRUSTPLAY ) == pTransMsg->dwType )
	{
		// 有人托管了,先做一些处理,因为当前状态有可能对这个消息不感兴趣
		PTKREQTRUSTPLAY pReqTrustPlay = ( PTKREQTRUSTPLAY )pTransMsg;
		if ( dwUserID != pReqTrustPlay->dwUserID )
		{
			TKWriteLog( "[%s:%d] Msg Sender( %d ), The seat player( %d )", __FILE__ , __LINE__, dwUserID, pReqTrustPlay->dwUserID ) ;

			// 释放
			if ( pTransMsg != nullptr && bUseEndDel)
			{
				delete pTransMsg;
				pTransMsg = nullptr;
			}

			return false;
		}

		// 发给托管\解除托管的玩家
		if (IsHaveplayerSeat(nSeatOrder))
		{
			if ( !GameSend2SeatPlayer( nSeatOrder, ( PTKHEADER )pTransMsg ) )
			{
				TKWriteLog( "%s : %d, Error! 发送消息错误 len( %d )", __FILE__, __LINE__, pTransMsg->dwLength );
			}
		}

		if ( UNTRUST == pReqTrustPlay->dwType )
		{
			// 解除托管,不用做什么操作
			CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerBySeat( nSeatOrder );
			pPlayer->ClearState( ST_TRUSTPLAY );

			// 录像记录玩家解除托管状态
			CTKBuffer bufGameAction;
			bufGameAction.AppendFormatText("<a id=\"%d\" rt=\"%d\" s=\"%d\" />", RECORD_CLEARTRUST, GetRtValue(), nSeatOrder);
			AddGameAction( bufGameAction.GetBufPtr() );

			//TKWriteLog("didnt delete %s : %d", __FUNCTION__, __LINE__);


			// 释放
			if ( pTransMsg != nullptr && bUseEndDel)
			{
				delete pTransMsg;
				pTransMsg = nullptr;
			}

			return true;
		}

		// 标记这个玩家托管了
		m_pMahjongObj->PlayerTrustPlay( nSeatOrder, pReqTrustPlay->dwType );
		
		// 录像记录玩家托管状态
		CTKBuffer bufGameAction;
		bufGameAction.AppendFormatText("<a id=\"%d\" rt=\"%d\" s=\"%d\" />", RECORD_TRUSTPLAY, GetRtValue(), nSeatOrder);
		AddGameAction( bufGameAction.GetBufPtr() );

		if ( AllPlayerTrustPlay() && !HasPlayerTing() )
		{
			// 所有玩家都托管了,结束本局
			m_pGameMachine->Stop();

			//TKWriteLog("didnt delete %s : %d", __FUNCTION__, __LINE__);

			// 释放
			if ( pTransMsg != nullptr && bUseEndDel)
			{
				delete pTransMsg;
				pTransMsg = nullptr;
			}

			return true;
		}
	}

	if (pTransMsg->dwType < EVENT_INTERNEL_END)
	{
		TKWriteLog( "!!! Warring !!!  CTKMahjongGame::OnPlayerMsg, Invalide MsgType=0x%08X, UID=%u", pTransMsg->dwType, dwUserID);
		
		// 释放
		if ( pTransMsg != nullptr && bUseEndDel)
		{
			delete pTransMsg;
			pTransMsg = nullptr;
		}

		return true;
	}

	// 交给状态机去处理
	CEvent event( pTransMsg->dwType, dwUserID, pTransMsg );
	m_pGameMachine->HandleEvent( &event );

	// 释放
	if ( pTransMsg != nullptr && bUseEndDel)
	{
		delete pTransMsg;
		pTransMsg = nullptr;
	}

	return true;
}



// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
void CTKMahjongGame::OnPlayerNetBreak( CTKGamePlayer *pGamePlayer )
{
	try
	{
		SetPlayerBreak( pGamePlayer->m_nSeatOrder, true );

		CTKBuffer bufGameAction;
		bufGameAction.AppendFormatText("<a id=\"%d\" rt=\"%d\" s=\"%d\" />", RECORD_NETBREAK, GetRtValue(), pGamePlayer->m_nSeatOrder);
		AddGameAction( bufGameAction.GetBufPtr() );
		// 设置玩家托管了
		if ( !m_pMahjongObj->PlayerTrustPlay( pGamePlayer->m_nSeatOrder, TRUST_OFFLINE ) )
		{
			return;
		}
		
		if ( AllPlayerTrustPlay()  && !HasPlayerTing())
		{
			// 所有玩家都托管了,结束本局
			m_pGameMachine->Stop();
			return;
		}

		// 让状态机处理玩家托管事件
		CEvent event( EVENT_TRUSTPLAY, pGamePlayer->m_dwUserID, NULL );
		m_pGameMachine->HandleEvent( &event );
	}
	catch (...)
	{
		TKWriteLog( "%s: %d, Exception, 玩家断线异常( %d )", __FILE__, __LINE__, pGamePlayer->m_dwUserID );
	}
}



// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
void CTKMahjongGame::OnPlayerNetResume( CTKGamePlayer *pGamePlayer )
{
	SetPlayerBreak( pGamePlayer->m_nSeatOrder, false );
	//m_pMahjongObj->CancelTrustPlay(pGamePlayer->m_nSeatOrder);
	
	CTKBuffer bufGameAction;
	bufGameAction.AppendFormatText("<a id=\"%d\" rt=\"%d\" s=\"%d\" />", RECORD_NETRESUME, GetRtValue(), pGamePlayer->m_nSeatOrder);
	AddGameAction( bufGameAction.GetBufPtr() );
}



// *********************************************************************************************************************
// 
// 
// 散桌
// 
// *********************************************************************************************************************
void CTKMahjongGame::SetPlayerBreak( int seat, bool isBreak )
{
	CTKMahjongPlayer* player = m_pMahjongObj->GetPlayerBySeat( seat );
	if ( NULL != player )
	{
		player->SetBreak( isBreak );
	}
}



// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
bool CTKMahjongGame::IsPlayerQuitOnOwn( int seatOrder )
{
	CTKGamePlayer* player = GetGamePlayerBySeat( seatOrder );
	if( !player ) return false;
	return (player->IsExitGame() != 0);
}



// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
bool CTKMahjongGame::IsPlayerPlaying( int seatOrder )
{
	if( IsPlayerQuitOnOwn(seatOrder) ) return false;
	if( __super::IsBotTrustPlay(seatOrder) ) return false;
	if( __super::IsNetBreak(seatOrder) ) return false;

	return true;
}



// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
IScoreCalculator * CTKMahjongGame::CreateCalculator( CGameRule * pGameRule, int nPlayerCount, int nMulti )
{
	IScoreCalculator * pCalculator = NULL;
	BOOL bGangAward = FALSE;
	switch( pGameRule->GetRule( RULE_SCORE_RULE ) )
	{
	case RULE_SCORE_PUBLIC:
		pCalculator = new CScoreCalculator;
		bGangAward = true;
		break;
	case RULE_SCORE_BLOODY:
		pCalculator = new CBloodyScoreCalculator;
		break;
	//FOR TWO PLAYER
	case RULE_SCORE_UPTOSTANDARD:
		{
			pCalculator = new CUptostandCalculator;
			bGangAward = true;
		}
		break;
	default:
		ASSERT( FALSE );
		break;
	}

	if ( NULL != pCalculator )
	{
		pCalculator->InitInstance( pGameRule, nPlayerCount, nMulti, this);
        int nBaseScore = pGameRule->GetRule(RULE_SCORE_RULE) == RULE_SCORE_PUBLIC ? 8 : 0; // 限番的国标麻将，底分是8分
		pCalculator->SetScore( SCORE_TYPE_BASE , nBaseScore ) ;					// 基本分
        pCalculator->SetScore(SCORE_TYPE_WIN_FAIL, 0);			                // 诈和扣分
		pCalculator->SetScore( SCORE_TYPE_ZHI_GANG , bGangAward ? 2 : 0 ) ;		// 直杠明杠得分
		pCalculator->SetScore( SCORE_TYPE_BU_GANG , bGangAward ? 1 : 0 ) ;		// 补杠明杠得分
		pCalculator->SetScore( SCORE_TYPE_AN_GANG , bGangAward ? 2 : 0 ) ;		// 暗杠得分
		pCalculator->SetScore( SCORE_TYPE_HUN_GANG , bGangAward ? 88 : 0 ) ;	// 混杠得分
	}

	return pCalculator;
}



// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
BOOL CTKMahjongGame::AllPlayerTrustPlay()
{
	for ( int i = 0; i < MAX_PLAYER_COUNT; ++i )
	{
		CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerBySeat( i );
		if ( NULL == pPlayer )
		{
			continue;
		}
		if ( !pPlayer->HasState( ST_TRUSTPLAY ) )
		{
			return FALSE;
		}
	}

	return true;
}


BOOL CTKMahjongGame::HasPlayerTing()
{
	for ( int i = 0; i < MAX_PLAYER_COUNT; ++i )
	{
		CTKMahjongPlayer * pPlayer = m_pMahjongObj->GetPlayerBySeat( i );
		if ( NULL == pPlayer )
		{
			continue;
		}
		if ( pPlayer->HasCalled() )
		{
			return TRUE;
		}
	}

	return FALSE;
}



// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
BOOL CTKMahjongGame::AllIsBotTrustPlay()
{
	for ( int i = 0; i < MAX_PLAYER_COUNT; ++i )
	{
		if ( !IsBotTrustPlay( i ) )
		{
			return FALSE;
		}
	}

	return TRUE;
}



// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
BOOL CTKMahjongGame::OnResetDump()
{
	// mahjongobj的
	if ( NULL != m_pMahjongObj )
	{
		m_pMahjongObj->OnResetDump();
	}
	else
	{
		TKWriteLog( "ResetDump, m_pMahjongObj = NULL" );
	}

	// 先打出requestobj的
	if ( NULL != m_pRequestObj )
	{
		m_pRequestObj->OnResetDump();		
	}
	else
	{
		TKWriteLog( "ResetDump, m_pRequestObj = NULL" );
	}

	// 状态机的
	if ( NULL != m_pGameMachine )
	{
		m_pGameMachine->OnResetDump();
	}
	else
	{
		TKWriteLog( "ResetDump, m_pGameMachine = NULL" );
	}

	return true;
}




// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
void CTKMahjongGame::AddCashFlow( int nCashFlow, int nTax )
{
	m_nCashFlow += nCashFlow;
	m_nTax += nTax;
}



// *********************************************************************************************************************
//
//	
//	
//
// *********************************************************************************************************************
void CTKMahjongGame::State()
{
	if ( tk_pStatMgr && m_nTax > 0 )
	{
		tk_pStatMgr->AddStat3( TK_STAT_TYPE_GAME_TAX, TK_GAMEID_MJT, m_nProductID, 1, m_nCashFlow, m_nTax );
	}
}

BOOL CTKMahjongGame::IsEnableLockdown()
{
	return TRUE;
}

BOOL CTKMahjongGame::IsHaveplayerSeat(const int& seat)
{
	if (seat>=0 && seat<MAX_PLAYER_COUNT)
	{
		CTKGamePlayer* pPlayer = GetGamePlayerBySeat(seat);
		if (NULL == pPlayer)
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	return FALSE;
}
DWORD CTKMahjongGame::GetRtValue()
{
	DWORD dwCurTime = GetTickCount();
	DWORD dwDif = dwCurTime - m_dwGameBegin;
	if ( dwCurTime < m_dwGameBegin )
	{
		dwDif = ( DWORD )0xFFFFFFFF - m_dwGameBegin + dwCurTime;
	}

	return dwDif; // 返回毫秒
}

void CTKMahjongGame::AddGameAction(char* szActionDescribe, bool bFirst)
{
	//先写到基类里
	CTKGame::AddGameAction(szActionDescribe);
}

void CTKMahjongGame::AddGameResult(char* szResultDescribe)
{
	//先写到基类里
	CTKGame::AddGameResult(szResultDescribe);
}

void CTKMahjongGame::SendChangeFlowerAck(int nFlowerCount, PTKREQMAHJONGCHANGEFLOWER pReq)
{
    TKMobileAckMsg rAck;
    MahJongJSPKAckMsg *pMahJong = rAck.mutable_mahjongjspk_ack_msg();
    MahJongChangeFlowerAck *pMsg = pMahJong->mutable_mahjongchangeflower_ack_msg();

    pMsg->set_seat(pReq->nSeat);
    pMsg->set_requestid(pReq->nRequestID);
    pMsg->set_tileid(pReq->nTileID);
    pMsg->set_count(nFlowerCount);
    int nScore = m_GameRule.GetRule(RULE_CF_SCORE);
	int aScores[MAX_PLAYER_COUNT];
	std::fill_n(aScores, MAX_PLAYER_COUNT, -nScore);
	aScores[pReq->nSeat] = nScore;
	m_pCalculator->ChangeRTScore(pReq->nSeat, aScores);

	CTKBuffer bufGameAction;
	bufGameAction.AppendFormatText("<a id=\"%d\" o=\"", RECORD_SCORECHANGE);
    for (int i = 0;i < MAX_PLAYER_COUNT;++i)
    {
        __super::SetGameResult(i, 0, 0, 0, aScores[i], FALSE, FALSE);
		pMsg->add_score(aScores[i]);
		bufGameAction.AppendFormatText((i == 0 ? "%d," : "%d"), aScores[i]);
    }
    SendMsg(rAck);
	bufGameAction.AppendFormatText("\" />");
	this->AddGameAction(bufGameAction.GetBufPtr());
}

BOOL	CTKMahjongGame::GameBroadcast(PTKHEADER pMsg)
{
    return GetProtobufFactory()->Broadcast(pMsg);

}
BOOL	CTKMahjongGame::GameSend2SeatPlayer(int nSeat,PTKHEADER pMsg)
{
    return GetProtobufFactory()->Send2SeatPlayer(nSeat, pMsg);

}
BOOL	CTKMahjongGame::GameSend2Black(PTKHEADER pMsg)
{
    return TRUE;
}
BOOL	CTKMahjongGame::GameSend2SeatBlack(int nSeat, PTKHEADER pMsg)
{
    return GetProtobufFactory()->Send2SeatBlack(nSeat, pMsg);

}
BOOL CTKMahjongGame::GameSend2Observer(PTKHEADER pMsg)
{
    return TRUE;
}

BOOL	CTKMahjongGame::GameSend2Player( DWORD dwUserID, PTKHEADER pMsg )
{
    return GetProtobufFactory()->Send2Player(dwUserID, pMsg);
}

void CTKMahjongGame::SendMsg(TKMobileAckMsg& rMsg, int nSeat)
{
    MahJongJSPKAckMsg * pAckMsg = rMsg.mutable_mahjongjspk_ack_msg();

    pAckMsg->set_matchid(m_dwMatchID);
    // 序列化
    std::string szSerializeMsg;
    rMsg.SerializeToString(&szSerializeMsg);

    // 发送
    BYTE* pszPack = new BYTE[sizeof(TKHEADER) + rMsg.ByteSize()];    // 分配空间
    ZeroMemory(pszPack, sizeof(TKHEADER) + rMsg.ByteSize());

    ((PTKHEADER)pszPack)->dwType = TK_ACK | TK_MSG_MAHJONG_PROTOBUF;
    ((PTKHEADER)pszPack)->dwLength = rMsg.ByteSize();   // size
    BYTE* pData = pszPack + sizeof(TKHEADER);
    memcpy(pData, szSerializeMsg.c_str(), rMsg.ByteSize());

    if (-1 != nSeat)
    {
        Send2SeatPlayer(nSeat, (PTKHEADER)pszPack);
    }
    else
    {
        Broadcast((PTKHEADER)pszPack);
    }
    delete[] pszPack;
    pszPack = NULL;
}

void CTKMahjongGame::OnGameTimer( DWORD dwID, ULONG_PTR ulUserData )
{
	switch(dwID)
	{
	case EVENT_1PLAYER_NOT_WIN:
		{
			int * pBuffer = (int *)ulUserData;
			int cnNotWin = *pBuffer;
			pBuffer++;
			int nNextSeat = *pBuffer;

			CEvent event( dwID + cnNotWin - 1, nNextSeat );
			m_pGameMachine->HandleEvent(&event);
		}
		break;
	case EVENT_OPENDOOR_END:
		{
			//// FOR TEST
			//TKWriteLog("On Open Door Timer! time = %u", GetTickCount());
			CEvent event(dwID, (DWORD)ulUserData);
			m_pGameMachine->HandleEvent(&event);
		}
		break;
	case EVENT_LUCKY_TILE:
		{
			CEvent event(dwID, 0, (void*)ulUserData);
			m_pGameMachine->HandleEvent(&event);
		}
		break;
	default:
		break;
	}
}
