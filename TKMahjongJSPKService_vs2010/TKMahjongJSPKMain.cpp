#include "TKServiceTpl.h"
#include "TKMahjongJSPKService.h"

int APIENTRY WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow )
{
	CTKServiceTpl<CTKMahjongJSPKService> service;
	return service.Execute("MahjongJSPK");
}
