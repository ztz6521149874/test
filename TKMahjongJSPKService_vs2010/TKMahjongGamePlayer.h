#pragma once

#include "TKGamePlayer.h"
#include "TKMatchSvrUser.h"
#include "TKHEBBrokerProtocol.h"
#include "TKConnectionPool.h"
#include "../ProtocolBuffer/TKMahJongJSPK.pb.h"
#include <boost/serialization/singleton.hpp>
#include "ServerGameConfig.h"

using namespace cn::jj::service::msg::protocol;

#define MAX_PICK_COUNT 5 //最大抽奖次数

#if 0
// ==============================================
// 推送原生事件（后端采用异步操作），永远返回成功
// ==============================================
#ifndef TK_MSG_WS2HEBBROKER_PUSHDATA
#define TK_MSG_WS2HEBBROKER_PUSHDATA (TKID_MSG_HEBBROKER_BASE + 0x00000040)
typedef struct tagTKReqWS2HEBBrokerPushData {
	TKHEADER	header;
	DWORD       dwPID;             // 用户ID
	DWORD       dwSID;             // 原生数据ID
	DWORD       dwDataOffset;      // 原生数据偏移量
	DWORD       dwDataSize;        // 原生数据大小
	DWORD       dwMPID;            // 产品ID
	char        szUserName[TK_BUFLEN_REALNAME];     // 后台系统用户名
	// JSON格式原生数据(不支持嵌套)  // {"field1":intValue, "field2":"text value", ...}
} TKREQWS2HEBBROKERPUSHDATA, *PTKREQWS2HEBBROKERPUSHDATA;
typedef struct tagTKAckWS2HEBBrokerPushData {
	TKHEADER    header;
} TKACKWS2HEBBROKERPUSHDATA, *PTKACKWS2HEBBROKERPUSHDATA;
#endif //TK_MSG_WS2HEBBROKER_PUSHDATA
#endif

//////////////////////////////////////////RateTool//////////////////////////////////////////////////
class RateTool
{
public:
	RateTool();
	~RateTool();

	void InitData(int rate0, int rate2, int rate5);
	int  Random(int& multiple) const;

private:
	std::vector<double>	m_vRates;
	std::vector<int>	m_vMultiples;
};

///////////////////////////////////////////////////////////////////////
class CTKMahjongMatchSvrUser : public CTKMatchSvrUser
{
public:
	CTKMahjongMatchSvrUser();
	~CTKMahjongMatchSvrUser();
protected:
	virtual BOOL OnSyncMsg(PTKHEADER pMsg);
};

///////////////////////////////////////////////////////////////////////

class CTKMahjongGamePlayer : public CTKGamePlayer
{
public:
	CTKMahjongGamePlayer();
	~CTKMahjongGamePlayer();

public:
	void ResetData(); //清理上一局数据
	void SetWinScore(int nScore); //宝藏牌可抽奖积分
	void addHematinic(int nHematinic);

	void SetTreasureRateKit(RateTool * pRateKit) { m_pRateKit = pRateKit; };  //宝藏牌的概率

	void SetTreasureContinueRule(int nContinue) { m_nContinueRule = nContinue; };  //规则设置：宝藏牌翻出不合格是否可以进行下一轮（0-继续，1-不继续）
	void SetLastTreasureIs0(bool bLastTreasureIs0) { m_bLastTreasureIs0 = bLastTreasureIs0; };  //设置上次翻出的宝藏牌是否为不合格

	// void Test();
protected:
	virtual BOOL OnBreak();
	virtual BOOL OnMsg(PTKHEADER pMsg);

	void UpdateScore(int nUptScore); //通知比赛服更新积分
	void SendPickResult( int nLevel, int nMultiple );
	void SendToClient(TKMobileAckMsg& rMsg);

	void GetPivotGrow(DWORD nGrowId);
	void SavePivotGrow(DWORD nGrowId, int nValue);

	void Statistic();
private:
	int		m_nWinScore;
	int		m_nPickCount;

	int		m_nUptScore;

	int		m_aRoundPay[MAX_PICK_COUNT];
	int		m_aRoundGain[MAX_PICK_COUNT];

	int     m_nContinueRule; //宝藏牌翻出不合格是否可以进行下一轮（0-继续，1-不继续）
	bool    m_bLastTreasureIs0; //上次开启宝藏牌为不合格：m_nLevel=0

	RateTool * m_pRateKit;
};

