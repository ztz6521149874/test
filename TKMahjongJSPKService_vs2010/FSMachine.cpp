#include ".\fsmachine.h"
#include <assert.h>
#include <GTKFunc.h>

// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
IState::IState() //: m_szName( szName )
{
	m_vecEventMap.clear();
	m_nLastEventID = 0;
	m_pFSMachine = NULL;
}



// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
bool IState::AddEvent( int nEventID, std::string szNextState )
{
	// 这个事件ID必须有相应的处理函数,搜索一下
	EventMapVector::iterator it;
	for ( it = m_vecEventMap.begin(); it != m_vecEventMap.end(); it++ )
	{
		if ( nEventID == ( *it ).nEventID )
		{
			break;
		}
	}
	//assert( it != m_vecEventMap.end() && "错误:这个事件ID没有相应的处理函数,请在状态类构造时添加处理函数" );
	if (it == m_vecEventMap.end())
	{
		return false;
	}

	// 保存下来
	( *it ).szNextState = szNextState;

	return true;
}



// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
void IState::RegistEventFunction( int nEventID, ONEVENT fnOnEvent )
{
	EventMap eventMap = { nEventID, fnOnEvent, "" };
	m_vecEventMap.push_back( eventMap );
}



// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
bool IState::OnEvent( CEvent * pEvent )
{
	assert( NULL != m_pFSMachine );

	// 搜索对应的处理函数
	EventMapVector::iterator it;
	for ( it = m_vecEventMap.begin(); it != m_vecEventMap.end(); it++ )
	{
		int nID = ( *it ).nEventID;
		if ( pEvent->ID() == ( *it ).nEventID )
		{
			break;
		}
	}
	if ( it == m_vecEventMap.end() )
	{
		// 没有注册对应的处理函数
//		TKWriteLog( "错误:当前状态(%s)不能处理的事件(%d), %s, %d", Name(), pEvent->ID(), __FILE__, __LINE__ );
		return false;
	}

	// 保存一下,刚发生过什么事件
	m_nLastEventID = pEvent->ID();

	bool bRet = ( this->* ( *it ).fnOnEvent )( pEvent );
	if ( bRet && ( *it ).szNextState != "" )
	{
		// 处理成功,转入下一状态
		IState * pNextState = m_pFSMachine->SetState( ( *it ).szNextState );
		if( NULL == pNextState )
		{
			return false;
		}

		// 本状态OnLeave
		OnLeave();

		// 下一状态OnEnter
		pNextState->OnEnter( pEvent );
	}

	// 不用转换状态
	return true;
}



// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
void IState::PostEvent( CEvent & event )
{
	assert( NULL != m_pFSMachine );

	m_pFSMachine->PostEvent( event );
}



// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
void IState::PostEvent( int nEventID )
{
	assert( NULL != m_pFSMachine );

	CEvent event( nEventID );
	m_pFSMachine->PostEvent( event );
}



// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
void IState::PostErrorMsg( const TCHAR * pErrorMsg )
{
	// 取得当前时间
	SYSTEMTIME timeNow = { 0 } ;
	GetLocalTime( &timeNow ) ;
	TCHAR szTime[ 256 ] = { 0 } ;
	wsprintf( szTime , TEXT( "[%04d-%02d-%02d %02d:%02d:%02d:%03d]" ) , timeNow.wYear , timeNow.wMonth , timeNow.wDay , timeNow.wHour , timeNow.wMinute , timeNow.wSecond , timeNow.wMilliseconds ) ;
	std::string szError = szTime;
	szError += "," + m_szName + "," + pErrorMsg;

	m_pFSMachine->PostErrorMsg( szError );
}

std::string IState::getNextState()
{
	for (auto iter = m_vecEventMap.begin(); iter != m_vecEventMap.end(); ++iter)
	{
		if (m_nLastEventID == iter->nEventID)
		{
			return iter->szNextState;
		}
	}
	return std::string();
}


// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
CFSMachine::CFSMachine(void)
{
	m_mapState.clear();
	m_pCurrentState = NULL;
	m_vszErrorMsg.clear();
}



// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
CFSMachine::~CFSMachine(void)
{
	m_pCurrentState = NULL;
	StateMap::iterator it;
	for ( it = m_mapState.begin(); it != m_mapState.end(); it++ )
	{
		delete ( *it ).second;
	}
	m_mapState.clear();
	m_vszErrorMsg.clear();
}



// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
bool CFSMachine::AddState(const std::string& szStateName, IState * pState )
{
	pState->m_pFSMachine = this;

	StateMap::value_type v( szStateName, pState );
	m_mapState.insert( v );

	return true;
}


// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
IState * CFSMachine::SetState(const std::string& szStateName)
{
	StateMap::iterator it = m_mapState.find( szStateName );
	if ( it == m_mapState.end() )
	{
		TKWriteLog( "错误:设置非法的状态(%s),当前状态(%s), %s, %d", szStateName, m_pCurrentState->Name(), __FILE__, __LINE__ );
		return NULL;
	}

	m_pCurrentState = it->second;

	return m_pCurrentState;
}



// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
void CFSMachine::HandleEvent( CEvent * pEvent )
{
	assert( NULL != m_pCurrentState && "错误:状态机没有正确的设置初始状态" );

	m_pCurrentState->OnEvent( pEvent );

	// 在State的OnEvent处理中可能产生了内部事件,在这里处理处理一下
	EVENTVECTOR::iterator it = m_vecInnerEvent.begin();
	assert( m_vecInnerEvent.size() <= 1 );
	while ( it != m_vecInnerEvent.end() )
	{
		// 先删除掉
		CEvent event( *it );
		m_vecInnerEvent.erase( it );

		// 处理
		m_pCurrentState->OnEvent( &event );

		// 下一个
		it = m_vecInnerEvent.begin();
	}
/*
	for ( it = m_vecInnerEvent.begin(); it != m_vecInnerEvent.end(); ++it )
	{
		m_pCurrentState->OnEvent( &( *it ) );
	}
*/
	assert( m_vecInnerEvent.size() == 0 );

	// 都处理完了,清理掉
//	m_vecInnerEvent.clear();
}



// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
void CFSMachine::PostEvent( CEvent & event )
{
	m_vecInnerEvent.push_back( event );
}




// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
void CFSMachine::OnTickCount()
{
	m_pCurrentState->OnTickCount();

	// 在State的OnEvent处理中可能产生了内部事件,在这里处理处理一下
	EVENTVECTOR::iterator it = m_vecInnerEvent.begin();
	assert( m_vecInnerEvent.size() <= 1 );
	while ( it != m_vecInnerEvent.end() )
	{
		// 先删除掉
		CEvent event( *it );
		m_vecInnerEvent.erase( it );

		// 处理
		m_pCurrentState->OnEvent( &event );

		// 下一个
		it = m_vecInnerEvent.begin();
	}
	assert( m_vecInnerEvent.size() == 0 );
}



// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
IState * CreateState( char * szStateName )
{
	IState * pState = GenericFactory<IState>::instance().Create( szStateName );
	pState->SetName( szStateName );
	return pState;
}



// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
void CFSMachine::PostErrorMsg( std::string & szErrorMsg )
{
	m_vszErrorMsg.push_back( szErrorMsg );
}
