#include ".\tkmahjongplayer.h"
#include "MahJongTile.h"		// CMahJongTile
#include "assert.h"
#include "tkmahjongprotocol.h"
#include "tchar.h"
#include "tkgame2pivotprotocol.h"
#include "TKGameService.h"

#ifndef ASSERT
#define ASSERT	assert
#endif

// *********************************************************************************************************************
// 
// 
// 
// *********************************************************************************************************************
CTKMahjongPlayer::CTKMahjongPlayer(void) :m_pPlayer(NULL)
{
    m_nGameSeat = MAX_PLAYER_COUNT - m_nSeatOrder - 1;
    m_nWind = -1;
    m_nRules = 0;
    m_nSex = 0;

    memset(m_apHandTile, 0, sizeof(m_apHandTile));
    m_nHandTileCount = 0;
    m_pDrawTile = NULL;

    memset(m_apShowGroup, 0, sizeof(m_apShowGroup));
    memset(m_anShowGroupType, 0, sizeof(m_anShowGroupType));
    m_nShowGroupCount = 0;

    memset(m_apDiscardTile, 0, sizeof(m_apDiscardTile));
    m_nDiscardTileCount = 0;

    memset(m_apFlowerTile, 0, sizeof(m_apFlowerTile));
    m_nFlowerCount = 0;

    m_nFrustaCount = 0;

    m_bRealHandTile = FALSE;

    m_nCallType = 0;
    m_bWinSelf = FALSE;
    m_bAutoGang = FALSE;
    m_isBreak = false;

    m_nContinueChiCount = 0;
    m_nContinuePengCount = 0;
    m_nContinueGangCount = 0;

    m_cnGang = 0;
    m_cnPao = 0;
    m_cnWin = 0;

    memset(&m_sWinInfo, 0, sizeof(m_sWinInfo));

    m_cnDiscardTile = 0;

    m_nTrustPlay = 0;

    m_nState = 0;

    m_cnShowGroupSeat = 0;

	m_nDelayCount = 0;

    ::InitializeCriticalSection(&m_cs);
}




// *********************************************************************************************************************
// 
// 
// 
// *********************************************************************************************************************
CTKMahjongPlayer::~CTKMahjongPlayer(void)
{
	::DeleteCriticalSection( &m_cs ) ;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CTKMahjongPlayer::ResetGameData()
{
	memset( m_apHandTile , 0 , sizeof( m_apHandTile ) ) ;
	m_nHandTileCount = 0 ;
	m_pDrawTile = NULL ;

	memset( m_apShowGroup , 0 , sizeof( m_apShowGroup ) ) ;
	memset( m_anShowGroupType , 0 , sizeof( m_anShowGroupType ) ) ;
	m_nShowGroupCount = 0 ;

	memset( m_apDiscardTile , 0 , sizeof( m_apDiscardTile ) ) ;
	m_nDiscardTileCount = 0 ;

	memset( m_apFlowerTile , 0 , sizeof( m_apFlowerTile ) ) ;
	m_nFlowerCount = 0 ;

	//	m_nFrustaCount = 0 ;	// 这里没有重置牌墙墩数，因为在Obj中需要用这个信息来清除牌墙

	m_nCallType      = 0 ;
	m_bWinSelf       = FALSE ;
	m_bAutoGang      = FALSE ;

	m_nContinueChiCount  = 0 ;
	m_nContinuePengCount = 0 ;
	m_nContinueGangCount = 0 ;

	m_cnDiscardTile = 0;

	m_cnGang = 0;
	m_cnPao = 0;

	m_cnShowGroupSeat = 0;

	m_cnWin = 0;

	m_nState = 0;
	m_nTrustPlay = 0;

	m_nDelayCount = 0;
}

void CTKMahjongPlayer::SetCallType( int nCallType , BOOL bWinSelf , BOOL bAutoGang )
{
	m_nCallType      = nCallType ;
	m_bWinSelf       = bWinSelf ;
	m_bAutoGang      = bAutoGang ;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CTKMahjongPlayer::SetState( int nState , int nParam /* = 0 */ )
{
	m_nState |= nState ;

	if( ST_TRUSTEXIT == nState )
	{ // 设置托管退出状态
		m_nTrustPlay = 4 ;
	}
	else if( ST_TRUSTPLAY == nState )
	{ // 设置托管状态
		m_nTrustPlay = nParam ;
	}
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CTKMahjongPlayer::ClearState( int nState )
{
	m_nState &= ~nState ;

	if( ST_TRUSTPLAY == nState )
	{ // 清除托管状态
		m_nTrustPlay = 0 ;
	}
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CTKMahjongPlayer::IsTrustPlayOrOffLine()
{
	return ( HasState( ST_OFFLINE ) || HasState( ST_TRUSTPLAY ) || HasState( ST_TRUSTEXIT ) ) ;
}

int CTKMahjongPlayer::GetFlowerCount()
{
    int nCount = 0;
    for (int i = 0; i < m_nHandTileCount; i++)
    { 
        if (m_apHandTile[i]->IsFlower())
        { 
            ++nCount;
        }
    }
    return nCount;
}

// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CMahJongTile* CTKMahjongPlayer::GetOneFlowerTile()
{
	for( int i = 0 ; i < m_nHandTileCount ; i ++ )
	{ // 遍历手中的牌
		ASSERT( NULL != m_apHandTile[ i ] ) ;
		if( m_apHandTile[ i ]->IsFlower() )
		{ // 花牌
			return m_apHandTile[ i ] ;
		}
	}

	return NULL ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CMahJongTile* CTKMahjongPlayer::FindHandTile( int nTileID , BOOL bGetLastTile )
{
	ASSERT( m_nHandTileCount > 0 ) ;	// 这时候手中必须要有牌

	CMahJongTile *pTile = NULL ;

	if( !RealHandTile() )
	{ // 如果手中牌都不是真实的牌
		if( bGetLastTile )
		{ // 取最后一张
			pTile = m_apHandTile[ m_nHandTileCount - 1 ] ;
		}
		else
		{ // 取第一张
			pTile = m_apHandTile[ 0 ] ;
		}
	}
	else
	{ // 手中牌都是真实的牌
		for( int i = 0 ; i < m_nHandTileCount ; i ++ )
		{ // 遍历手中的牌
			if( m_apHandTile[ i ]->IDIs( nTileID ) )
			{ // 就是这张牌
				pTile = m_apHandTile[ i ] ;
				break ;
			}
		}
	}

	ASSERT( NULL != pTile ) ;
	
	return pTile ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CMahJongTile* CTKMahjongPlayer::FindDiscardTile( int nTileID )
{
	CMahJongTile *pTile = NULL ;
	for( int i = 0 ; i < m_nDiscardTileCount ; i ++ )
	{ // 遍历出的牌
		pTile = m_apDiscardTile[ i ] ;
		ASSERT( NULL != pTile ) ;
		if( pTile->IDIs( nTileID ) )
		{ // 就是这张牌
			return pTile ;
		}
	}

	return NULL ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CMahJongTile* CTKMahjongPlayer::FindShowTile( int nTileID )
{
	CMahJongTile *pTile = NULL ;

	for( int i = 0 ; i < m_nShowGroupCount ; i ++ )
	{ // 遍历所有的吃碰杠分组
		for( int j = 0 ; j < 4 ; j ++ )
		{ // 变量分组中的牌
			pTile = m_apShowGroup[ i ][ j ] ;
			if( NULL != pTile && pTile->IDIs( nTileID ) )
			{ // 就是这张牌
				return pTile ;
			}
		}
	}

	return NULL ;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
int CTKMahjongPlayer::FillHandTileInfo( int *pnTile )
{
	if( NULL != pnTile )
	{ // 需要填充手中牌的信息
		for( int i = 0 ; i < m_nHandTileCount ; i ++ , pnTile ++ )
		{ // 遍历手中的牌
			*pnTile = m_apHandTile[ i ]->ID() ;
		}
	}
	
	return m_nHandTileCount ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
int CTKMahjongPlayer::FillShowTilesInfo( int *pnTile )
{
	int showCount = 0;

	if( NULL != pnTile )
	{
		if ( m_nShowGroupCount > 6 ) return 0;
		for ( int i=0; i<m_nShowGroupCount; i++ )
		{ 
			for ( int j=0; j<4; j++ )
			{
				if ( m_apShowGroup[i][j] == NULL )
					*pnTile = 0;
				else
					*pnTile = m_apShowGroup[i][j]->ID();	// 是否是暗杠

				// TKTRACE( "color = %d, what = %d \n", CMahJongTile::TileColor(*pnTile), CMahJongTile::TileWhat(*pnTile) );

				showCount++;
				pnTile++;
			}
		}
	}

	return showCount;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CTKMahjongPlayer::ResetHandTileID( int* pnTileID , int nTileCount , int nDrawTileID )
{
	if ( pnTileID == NULL )
	{
		for( int i = 0; i < m_nHandTileCount; i++ )
		{
			m_apHandTile[i]->SetID( 0 );
		}
		return;
	}
	
	ASSERT( nTileCount == m_nHandTileCount ) ;	// 和手中牌张数应该一致
	
	CMahJongTile *pTile = NULL ;
	for( int i = 0 ; i < nTileCount ; i ++ )
	{ // 遍历这些牌
		pTile = m_apHandTile[ i ] ;
		ASSERT( NULL != pTile ) ;	// 牌一定有效
		pTile->SetID( *pnTileID ++ ) ;	// 设置牌的ID号
	}

	if ( 0 == nDrawTileID )
	{
		return;
	}

	if( 2 == ( nTileCount % 3 ) )
	{ // 手中牌张数正好是抓牌后牌的张数
		m_pDrawTile = FindHandTile( nDrawTileID , TRUE ) ;
	}
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
BOOL CTKMahjongPlayer::HasTile( int nTileID )
{
	for( int i = 0 ; i < m_nHandTileCount ; i ++ )
	{ // 遍历手中牌的张数
		ASSERT( NULL != m_apHandTile[ i ] ) ;
		if( m_apHandTile[ i ]->IDIs( nTileID ) )
		{ // 就是这张牌
			return TRUE ;
		}
	}
	
	return FALSE ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
BOOL CTKMahjongPlayer::HasChi( int nColor , int nWhat )
{
	CMahJongTile *pTile = NULL ;

	for( int i = 0 ; i < m_nShowGroupCount ; i ++ )
	{ // 遍历吃碰杠的牌
		if( GROUP_STYLE_SHUN != m_anShowGroupType[ i ] ) continue ;	// 不是顺子

		for( int j = 0 ; j < 3 ; j ++ )
		{ // 遍历这三张牌
			pTile = m_apShowGroup[ i ][ j ] ;	// 取这张牌
			ASSERT( NULL != pTile ) ;
			if( !pTile->SameAs( nColor , nWhat ) ) continue ;	// 不是这张牌

			return TRUE ;
		}
	}

	return FALSE ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
BOOL CTKMahjongPlayer::HasPeng( int nColor , int nWhat )
{
	CMahJongTile *pTile = NULL ;

	for( int i = 0 ; i < m_nShowGroupCount ; i ++ )
	{ // 遍历吃碰杠的牌
		if( GROUP_STYLE_KE != m_anShowGroupType[ i ] ) continue ;	// 不是刻子

		pTile = m_apShowGroup[ i ][ 0 ] ;	// 取第一张牌
		ASSERT( NULL != pTile ) ;
		if( !pTile->SameAs( nColor , nWhat ) ) continue ;	// 不是这张牌

		return TRUE ;
	}
	
	return FALSE ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
BOOL CTKMahjongPlayer::HasGang( int nColor , int nWhat )
{
	CMahJongTile *pTile = NULL ;
	int nType = 0 ;

	for( int i = 0 ; i < m_nShowGroupCount ; i ++ )
	{ // 遍历吃碰杠的牌
		nType = m_anShowGroupType[ i ] ;	// 这个分组的类型
		if( GROUP_STYLE_MINGGANG != nType && GROUP_STYLE_ANGANG != nType ) continue ;	// 不是杠牌

		pTile = m_apShowGroup[ i ][ 0 ] ;	// 取第一张牌
		ASSERT( NULL != pTile ) ;
		if( !pTile->SameAs( nColor , nWhat ) ) continue ;	// 不是这张牌

		return TRUE ;
	}

	return FALSE ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
BOOL CTKMahjongPlayer::HasRequestPower( int nRequestType , int nSeat )
{
	int nRules = Rules() ;	// 这个玩家吃碰杠的规则

	switch( nRequestType )
	{
	case REQUEST_TYPE_CHI :		// 吃
		nRules &= 0xFF ;
		break ;

	case REQUEST_TYPE_PENG :	// 碰
		nRules = ( nRules >> 8 ) & 0xFF ;
		break ;

	case REQUEST_TYPE_GANG :	// 杠
		nRules = ( nRules >> 16 ) & 0xFF ;
		break ;

	case REQUEST_TYPE_WIN :		// 和
		nRules = ( nRules >> 24 ) & 0xFF ;
		break ;
	}

	return ( 0 != ( nRules & ( 1 << nSeat ) ) ) ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
BOOL CTKMahjongPlayer::HasCalledPower( int nRequestType , int nParam )
{
	BOOL bOK = FALSE ;

	switch( nRequestType )
	{
	case REQUEST_TYPE_GANG :	// 判断杠牌
		bOK = /*m_bAutoGang*/TRUE ;
		break ;

	case REQUEST_TYPE_WIN :		// 判断和牌
		bOK = TRUE ;
		// 这时nParam是放炮者的座位号
		if( m_bWinSelf )
		{ // 设置了只和自摸
			bOK = ( nParam == Seat() ) ;	// 必须是和自己
		}
		break ;
	}

	return bOK ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CTKMahjongPlayer::AddHandTile( CMahJongTile *pTile )
{
	ASSERT( NULL != pTile ) ;
	ASSERT( m_nHandTileCount < MAX_HAND_COUNT - 1 ) ;

	__try
	{
		Lock() ;

		m_apHandTile[ m_nHandTileCount ++ ] = pTile ;
		
		if( 2 == m_nHandTileCount % 3 )
		{ // 到了可以和牌的张数了（说明可能是游戏中的抓牌或者开牌时抓到某个数量的牌了）
			m_pDrawTile = pTile ;	// 设置最后抓的牌的信息
		}
		else
		{ // 没有到可以和牌的张数（也就是说这是开牌的时候的抓牌或者补花，而不是游戏中的抓牌）
			m_pDrawTile = NULL ;
		}
	}
	__finally
	{
		UnLock() ;
	}
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CTKMahjongPlayer::DeleteHandTile( CMahJongTile *pTile )
{
	ASSERT( NULL != pTile ) ;
	ASSERT( m_nHandTileCount > 0 ) ;

	__try
	{
		Lock() ;

		for( int i = 0 ; i < m_nHandTileCount ; i ++ )
		{ // 遍历手中的牌
			if( m_apHandTile[ i ] == pTile )
			{ // 就是这张牌
				// 把后面的牌移到这里来
				if( i != m_nHandTileCount - 1 )
				{ // 不是最后一张
					memmove( m_apHandTile + i , m_apHandTile + i + 1 , ( m_nHandTileCount - 1 - i ) * sizeof( CMahJongTile* ) ) ;
				}
				m_nHandTileCount-- ;
				return ;
			}
		}
	}
	__finally
	{
		UnLock() ;
	}

	ASSERT( FALSE ) ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CMahJongTile* CTKMahjongPlayer::DeleteDiscardTile( int nTileID )
{
	__try
	{
		Lock() ;

		if(m_nDiscardTileCount < 1)
		{
			return NULL;
		}

		// 这里是把这张牌删掉了，其实也可以设置标志来用其他方式表示这张牌
		CMahJongTile *pTile = m_apDiscardTile[ m_nDiscardTileCount - 1 ] ;	// 指向这张最后出的牌

		if ( pTile->IDIs( nTileID ) )
		{
			m_apDiscardTile[ --m_nDiscardTileCount ] = NULL ;

			return pTile ;
		}
		else
		{
			return NULL;
		}
	}
	__finally
	{
		UnLock() ;
	}

	return NULL ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CMahJongTile* CTKMahjongPlayer::DeleteGangTile( int nTileID )
{
	// 删除的这张牌一定是在明杠的牌中，而且一定是某个明杠的最后一张牌

	__try
	{
		Lock() ;
		
		CMahJongTile *pTile = NULL ;
		for( int i = 0 ; i < m_nShowGroupCount ; i ++ )
		{ // 遍历所有的分组
			if( GROUP_STYLE_MINGGANG == m_anShowGroupType[ i ] )
			{ // 明杠
				pTile = m_apShowGroup[ i ][ 3 ] ;	// 取这个杠牌的最后一张
				ASSERT( NULL != pTile ) ;
				if( pTile->IDIs( nTileID ) )
				{ // 就是这张牌
					return pTile ;
				}
			}
		}
	}
	__finally
	{
		UnLock() ;
	}

	return NULL ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CMahJongTile* CTKMahjongPlayer::SortHandTile()
{
	// 先对牌进行排序，排序后按照数组下标应该是每张牌的按照位置的索引
	int nTileCount = m_nHandTileCount ;	// 手中牌的张数
	if( RealHandTile() )
	{ // 手中是真实的牌
		qsort( m_apHandTile , nTileCount , sizeof( CMahJongTile* ) , CMahJongTile::Compare ) ;	// 排序
	}

	CMahJongTile *pTile = NULL ;
	if( 2 == ( nTileCount % 3 ) )
	{ // 正好是抓牌的张数
		pTile = m_apHandTile[ nTileCount - 1 ] ;	// 最后的一张牌
		m_pDrawTile = pTile ;
	}
	else
	{ // 不是抓牌的张数
		m_pDrawTile = NULL ;
	}

	return pTile ;
}


// ************************************************************************************************************************
// 
// 取得我的算番信息
// 
// ************************************************************************************************************************
void CTKMahjongPlayer::FillCheckParam( CHECKPARAM * pCheckParam )
{
	if( pCheckParam == NULL )
	{ // 参数无效
		ASSERT( FALSE ) ;
		return ;
	}

	// 手中的牌
	CMahJongTile * pDrawTile = NULL ;
	pCheckParam->cnHandStone = m_nHandTileCount ;

	if( m_pDrawTile != NULL && m_nHandTileCount % 3 == 2 )
	{ // 如果刚抓了一张牌
		// 放在最前面，否则第一个位置空着，Obj类会处理
		m_pDrawTile->FillTileInfo( pCheckParam->asHandStone ) ;
		pDrawTile = m_pDrawTile ;
		// 设为自摸
		pCheckParam->nWinMode |= WIN_MODE_ZIMO ;
	}
	int nTileIndex = 1 ; // 第一个元素放的是要和的那张牌，自已手上的牌从第二个位置开始
	for( int i = 0 ; i < m_nHandTileCount ; i++ )
	{ // 遍历手中的牌
		ASSERT( m_apHandTile[ i ] != NULL ) ;
		if( m_apHandTile[ i ] == pDrawTile )
		{ // 刚抓的那张牌，已放到数组里了
			continue ;
		}
		m_apHandTile[ i ]->FillTileInfo( pCheckParam->asHandStone + nTileIndex ) ;
		nTileIndex++ ;
	}

	// 吃碰杠的牌
	pCheckParam->cnShowGroups = m_nShowGroupCount ;
	pCheckParam->cnMinGang = 0 ;
	pCheckParam->cnAnGang  = 0 ;
	for( int i = 0 ; i < m_nShowGroupCount ; i++ )
	{ // 遍历所有的分组
		// 统计明暗杠个数
		if( m_anShowGroupType[ i ] == GROUP_STYLE_MINGGANG )
		{ // 明杠
			pCheckParam->cnMinGang++ ;
		}
		else if ( m_anShowGroupType[i] == GROUP_STYLE_ANGANG )
		{ // 暗杠
			pCheckParam->cnAnGang++ ;
		}

		int j = 0;
		for( j = 0 ; j < 4 ; j++ )
		{ // 遍历这一组的四张牌
			if ( m_apShowGroup[ i ][ j ] == NULL )
			{ // 这个位置没牌（比如吃碰的第四张牌）
				break ;
			}
			m_apShowGroup[ i ][ j ]->FillTileInfo( pCheckParam->asShowGroup[ i ].asStone + j ) ;
		}
		ASSERT( j >= 3 ) ;	// 这个分组中至少是3张牌
		pCheckParam->asShowGroup[ i ].nGroupStyle = m_anShowGroupType[ i ] ;
	}

	// 花牌，这里暂时没有，因为要改结构
	pCheckParam->cnFlower = m_nFlowerCount ;
	for( int i = 0 ; i < m_nFlowerCount ; i++ )
	{ // 遍历手中的花牌
		m_apFlowerTile[ i ]->FillTileInfo( pCheckParam->asFlower + i ) ;
	}

	// 和牌方式，暂时没有

	// 和牌所需最小番数由Obj类填充

	// 圈风门风,门风号由obj类填充
	pCheckParam->nMenWind = ( COLOR_WIND << 8 ) + ( m_nWind << 4 ) ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CTKMahjongPlayer::TraceInfo( int nType /* = 7 */ )
{
	char szText[ 1024 * 4 ] = { 0 } ;
	char szTemp[ 64 ] = { 0 } ;
	
	// 名字
	if( 0 != ( nType & ( 1 << 0 ) ) )
	{ // 要名字
        sprintf_s(szTemp, "%d[%s] ", m_pPlayer->m_dwUserID, m_pPlayer->m_szNickName);
		strcat_s( szText , szTemp ) ;
	}

	// 手中牌
	if( 0 != ( nType & ( 1 << 1 ) ) )
	{ // 要有手中牌
		strcat_s( szText , "手中牌:" ) ;
		for( int i = 0 ; i < m_nHandTileCount ; i ++ )
		{ // 遍历手中牌
			sprintf_s( szTemp , "%s," , m_apHandTile[ i ]->NameID() ) ;
			strcat_s( szText , szTemp ) ;
		}
		strcat_s( szText , " " ) ;
	}

	// 吃碰杠的牌
	if( 0 != ( nType & ( 1 << 2 ) ) )
	{ // 要有吃碰杠的牌
		strcat_s( szText , "吃碰杠的牌:" ) ;
		for( int i = 0 ; i < m_nShowGroupCount ; i ++ )
		{ // 遍历吃碰杠的分组
			sprintf_s( szTemp , "[%d:" , m_anShowGroupType[ i ] ) ;
			strcat_s( szText , szTemp ) ;
			for( int j = 0 ; j < 4 ; j ++ )
			{ // 遍历分组中的牌
				if( NULL != m_apShowGroup[ i ][ j ] )
				{ // 这个位置有牌
					sprintf_s( szTemp , "%s," , m_apShowGroup[ i ][ j ]->NameID() ) ;
					strcat_s( szText , szTemp ) ;
				}
			}
			strcat_s( szText , "]," ) ;

		}
		strcat_s( szText , " " ) ;
	}

	// 出的牌
	if( 0 != ( nType & ( 1 << 3 ) ) )
	{ // 要有出的牌
		strcat_s( szText , "出的牌:" ) ;
		for( int i = 0 ; i < m_nDiscardTileCount ; i ++ )
		{ // 遍历出的牌
			sprintf_s( szTemp , "%s," , m_apDiscardTile[ i ]->NameID() ) ;
			strcat_s( szText , szTemp ) ;
		}
		strcat_s( szText , " " ) ;
	}

	// 抓的牌
	if( 0 != ( nType & ( 1 << 4 ) ) )
	{ // 要有刚抓的牌
		strcat_s( szText , "刚抓的牌:" ) ;
		strcat_s( szText , ( NULL != m_pDrawTile ) ? m_pDrawTile->NameID() : "(无)" ) ;
		strcat_s( szText , " " ) ;
	}
	
//	TKWriteLog( TEXT( "%s" ) , szText ) ;
}




// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
BOOL CTKMahjongPlayer::IsAutoPlay()
{
	return FALSE ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CMahJongTile* CTKMahjongPlayer::GetAutoPlayTile()
{
	ASSERT( ( 2 == ( m_nHandTileCount % 3 ) ) ) ;	// 牌的张数一定要对
	ASSERT( RealHandTile() ) ;	// 一定可以看到手中的牌

	// 有两种情况会让玩家出牌，一种是玩家抓牌（包括杠牌和补花）之后，一种是玩家吃碰牌之后
	CMahJongTile *pTile = GetDrawTile() ;	// 取抓的这张牌
	if( NULL == pTile )
	{ // 没有抓牌
		pTile = m_apHandTile[ m_nHandTileCount - 1 ] ;	// 取最右边的一张牌
	}

	// 如果玩家已经听牌，就直接返回这张刚抓的牌
	if( HasCalled() )
	{ // 玩家已经听牌
		assert( NULL != GetDrawTile() ) ;
		return pTile ;
	}

	return pTile ;
}

CMahJongTile* CTKMahjongPlayer::GetAutoPlayTileEx()
{
	try
	{
		// 如果抓的牌形成了杠，就直接返回这张刚抓的牌
		CMahJongTile* drawTile = GetDrawGangTile();
		if ( NULL != drawTile )
		{
			return drawTile;
		}

		// 如果玩家已经听牌，就直接返回这张刚抓的牌
		if( HasCalled() )
		{ // 玩家已经听牌
			assert( NULL != GetDrawTile() ) ;
			return GetDrawTile();
		}

		int nTileCount = m_nHandTileCount ;	// 手中牌的张数

		assert( NULL == GetOneFlowerTile() ) ;	// 这时候手中肯定没有花牌
		qsort( m_apHandTile , nTileCount , sizeof( CMahJongTile* ) , CMahJongTile::Compare ) ;	// 排序	

		// 
		// 选牌的原则是：
		// 1.不是混牌
		// 2.一张离相同花色的其他牌中最远的牌
		// 

		// 先计算每张牌离其他牌的距离
		int pnDistance[ MAX_HAND_TILE_COUNT ] = { 0 } ;	// 各张牌离其他牌的距离
		CMahJongTile *pTile = NULL , *pTemp = NULL ;
		for( int i = 0 ; i < nTileCount ; i ++ )
		{ // 遍历这些牌
			pTile = m_apHandTile[ i ] ;	// 这个位置的牌
            pnDistance[i] = 0;

            // 看看前后的牌的值
            if (i > 0)
            { // 不是第一张
                pTemp = m_apHandTile[i - 1];	// 取前一张
                pnDistance[i] += CMahJongTile::Distance(pTile, pTemp);
            }
            else
            { // 第一张
                pnDistance[i] += 500;
            }
            if (i < nTileCount - 1)
            { // 不是最后一张
                pTemp = m_apHandTile[i + 1];	// 取后一张
                pnDistance[i] += CMahJongTile::Distance(pTile, pTemp);
            }
            else
            { // 最后一张
                pnDistance[i] += 500;
            }
		}

		// 从中选出最远的一张牌出来
		int nDistance = 1 ;	// 当前最大的距离
		int nIndex = -1 ;	// 当前最大距离这张牌的索引号
		for(int i = 0 ; i < nTileCount ; i ++ )
		{ // 遍历所有的牌
			if( pnDistance[ i ] >= nDistance )
			{ // 这张牌离别的牌的距离也很远
				nIndex = i ;
				nDistance = pnDistance[ i ] ;
			}
		}

		assert( -1 != nIndex ) ;	// 一定找到了这样的一张牌

		return m_apHandTile[ nIndex ] ;
	}
	catch (...)
	{
		TKWriteLog( "%s : %d, Exception", __FILE__, __LINE__ );
		return NULL;
	}
}

CMahJongTile* CTKMahjongPlayer::GetDrawGangTile()
{
	CMahJongTile *pTile = GetDrawTile() ;

	if ( NULL == pTile 
		|| pTile->ID()<= 0
		|| m_nHandTileCount <= 1 )
	{
		return NULL;
	}

	int cnDraw		   = 0;
	for ( int i=0; i<m_nHandTileCount; i++ )
	{
		if ( m_apHandTile[i]
		&& CMahJongTile::SameTile( pTile->ID(), m_apHandTile[i]->ID() ) )
		{
			cnDraw++;
		}
	}
	if ( 4 == cnDraw )
	{
		return pTile;
	}

	return NULL;
}

// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
int CTKMahjongPlayer::ChangeFlower( CMahJongTile* pTile )
{
	ASSERT( NULL != pTile ) ;
	ASSERT( pTile->IsFlower() ) ;
	
	DeleteHandTile( pTile ) ;
	AddFlowerTile( pTile ) ;
    return m_nFlowerCount;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CTKMahjongPlayer::DrawTile( CMahJongTile* pTile , int nDrawType )
{
	ASSERT( NULL != pTile ) ;
	
	AddHandTile( pTile ) ;	// 把牌加到手中

	// 重置连续吃碰杠牌的次数
	m_nContinueChiCount  = 0 ;
	m_nContinuePengCount = 0 ;
	if( DRAW_TILE_TYPE_GANG != nDrawType )
	{ // 不是杠后抓牌
		m_nContinueGangCount = 0 ;
	}
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CTKMahjongPlayer::DiscardTile( CMahJongTile* pTile, BOOL bHide )
{
	ASSERT( NULL != pTile ) ;
	
	DeleteHandTile( pTile ) ;	// 从手中删除这张牌
	AddDiscardTile( pTile ) ;	// 把这张牌放到出的牌中

	m_cnDiscardTile++;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
int CTKMahjongPlayer::ChiTile( CMahJongTile* pTile , CMahJongTile* apChiTile[ 3 ], int nDiscardSeat )
{
	ASSERT( NULL != pTile ) ;	// 吃的这张牌一定有效
	ASSERT( ( NULL != apChiTile[ 0 ] ) && ( NULL != apChiTile[ 1 ] ) && ( NULL != apChiTile[ 2 ] ) ) ;	// 吃的这三张牌一定有效
	ASSERT( ( pTile == apChiTile[ 0 ] ) || ( pTile == apChiTile[ 1 ] ) || ( pTile == apChiTile[ 2 ] ) ) ;	// 吃的这张牌一定在这三张牌中

	// 增加连续吃牌的次数
	m_nContinueChiCount ++ ;
	// 重置连续碰牌、杠牌的次数
	m_nContinuePengCount = 0 ;
	m_nContinueGangCount = 0 ;

	// 记下喂吃的这个人
	m_anShowGroupSeat[ m_cnShowGroupSeat ] = nDiscardSeat;
	m_cnShowGroupSeat++;

	// 把这组牌加入到吃碰杠的牌中
	return AddGroupTile( GROUP_STYLE_SHUN , apChiTile , 3 ) ;	
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
int CTKMahjongPlayer::PengTile( CMahJongTile* pTile , CMahJongTile* apPengTile[ 3 ], int nDiscardSeat )
{
	ASSERT( NULL != pTile ) ;	// 碰的这张牌一定有效
	ASSERT( ( NULL != apPengTile[ 0 ] ) && ( NULL != apPengTile[ 1 ] ) && ( NULL != apPengTile[ 2 ] ) ) ;	// 碰的这三张牌一定有效
	ASSERT( ( pTile == apPengTile[ 0 ] ) || ( pTile == apPengTile[ 1 ] ) || ( pTile == apPengTile[ 2 ] ) ) ;	// 碰的这张牌一定在这三张牌中

	// 增加连续碰牌的次数
	m_nContinuePengCount ++ ;
	// 重置连续吃牌、杠牌的次数
	m_nContinueChiCount = 0 ;
	m_nContinueGangCount = 0 ;

	// 记下喂的这个人
	m_anShowGroupSeat[ m_cnShowGroupSeat ] = nDiscardSeat;
	m_cnShowGroupSeat++;

	// 把这组牌加入到吃碰杠的牌中
	return AddGroupTile( GROUP_STYLE_KE , apPengTile , 3 ) ;	
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
int CTKMahjongPlayer::GangTile( CMahJongTile* pTile , CMahJongTile* apGangTile[ 4 ] , int nCount, int nDiscardSeat )
{
	// 增加连续杠牌的次数
	m_nContinueGangCount ++ ;
	// 重置连续吃牌、碰牌的次数
	m_nContinueChiCount = 0 ;
	m_nContinuePengCount = 0 ;

	if( NULL == pTile )
	{ // 暗杠
		ASSERT( 4 == nCount ) ;	// 一定是4张
		ASSERT( ( NULL != apGangTile[ 0 ] ) && ( NULL != apGangTile[ 1 ] ) && ( NULL != apGangTile[ 2 ] ) && ( NULL != apGangTile[ 3 ] ) ) ;	// 这四张牌一定有效

		// 把这组牌加入到吃碰杠的牌中
		return AddGroupTile( GROUP_STYLE_ANGANG , apGangTile , nCount ) ;
	}
	else
	{ // 明杠
		if( 1 == nCount )
		{ // 补杠
			ASSERT( NULL != apGangTile[ 0 ] ) ;	// 杠牌一定有效

			// 把这组牌加入到吃碰杠的牌中
			return AddGroupTile( GROUP_STYLE_MINGGANG , apGangTile , nCount ) ;
		}
		else
		{ // 直杠
			ASSERT( 4 == nCount ) ;	// 一定是4张
			ASSERT( ( NULL != apGangTile[ 0 ] ) && ( NULL != apGangTile[ 1 ] ) && ( NULL != apGangTile[ 2 ] ) && ( NULL != apGangTile[ 3 ] ) ) ;		// 这四张牌一定有效
			ASSERT( ( pTile == apGangTile[ 0 ] ) || ( pTile == apGangTile[ 1 ] ) || ( pTile == apGangTile[ 2 ] ) || ( pTile == apGangTile[ 3 ] ) ) ;	// 杠牌一定在这四张牌中
		
			// 记下喂吃的这个人
			m_anShowGroupSeat[ m_cnShowGroupSeat ] = nDiscardSeat;
			m_cnShowGroupSeat++;

			// 把这组牌加入到吃碰杠的牌中
			return AddGroupTile( GROUP_STYLE_MINGGANG , apGangTile , nCount ) ;
		}
	}

	m_cnGang++;

	ASSERT( FALSE ) ;
	return 0 ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
int CTKMahjongPlayer::ChangeHandTile( int anOldTileID[], int anNewTileID[], int cnChangeTile )
{
	for ( int i = 0; i < cnChangeTile; i++ ) 
	{
		int j = 0;
		for ( j = 0; j < m_nHandTileCount; j++ ) 
		{
			if ( m_apHandTile[j]->ID() == anOldTileID[i] ) 
			{
				m_apHandTile[j]->SetID( anNewTileID[i] );
				break;
			}
		}
		ASSERT( j != m_nHandTileCount );
	}

	return 0;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
int CTKMahjongPlayer::AddGroupTile( int nGroupStyle , CMahJongTile **ppTile , int nCount )
{
	if( GROUP_STYLE_ANGANG != nGroupStyle )
	{ // 不是暗杠（因为暗杠肯定是这次抓牌后才可能的操作）
		m_pDrawTile = NULL ;	// 重置抓的牌（重置抓的牌会影响到理牌的动画）
	}
	
	__try
	{
		Lock() ;

		if( ( GROUP_STYLE_MINGGANG == nGroupStyle ) && ( 1 == nCount ) )
		{ // 明杠，并且只有一张牌的信息（说明是补杠）
			CMahJongTile *pTile = *ppTile ;	// 指向这张牌
			ASSERT( HasPeng( pTile->Color() , pTile->What() ) ) ;	// 这时候一定碰了这张牌
			
			for( int i = 0 ; i < m_nShowGroupCount ; i ++ )
			{ // 遍历吃碰杠的分组
				if( GROUP_STYLE_KE == m_anShowGroupType[ i ] )
				{ // 刻
					CMahJongTile *pTileShow = m_apShowGroup[ i ][ 0 ] ;	// 指向分组的第一张牌
					if( pTileShow->SameAs( pTile->Color() , pTile->What() ) )
					{ // 就是碰的这张牌
						m_anShowGroupType[ i ] = GROUP_STYLE_MINGGANG ;	// 更改分组类型
						m_apShowGroup[ i ][ 3 ] = pTile ;
						return i ;
					}
				}
			}
			
			// 运行到这里说明没有找到这个分组
			ASSERT( FALSE ) ;
		}
		else
		{ // 其他类型（包括吃、碰、暗杠、明杠的直杠）
			m_anShowGroupType[ m_nShowGroupCount ] = nGroupStyle ;	// 保存吃碰杠的类型
			for( int i = 0 ; i < nCount ; i ++ )
			{ // 遍历这几张牌
				ASSERT( NULL != ppTile[ i ] ) ;
				m_apShowGroup[ m_nShowGroupCount ][ i ] = ppTile[ i ] ;
			}
			m_nShowGroupCount ++ ;	// 增加分组的个数
		}
	}
	__finally
	{
		UnLock() ;
	}
	
	return m_nShowGroupCount - 1 ;	// 因为m_nShowGroupCount已经增加了
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CTKMahjongPlayer::AddDiscardTile( CMahJongTile *pTile )
{
	ASSERT( NULL != pTile ) ;
	ASSERT( m_nDiscardTileCount < MAX_DISCARD_COUNT - 1 ) ;
	
	__try
	{
		Lock() ;

		m_apDiscardTile[ m_nDiscardTileCount ++ ] = pTile ;
	}
	__finally
	{
		UnLock() ;
	}
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CTKMahjongPlayer::AddFlowerTile( CMahJongTile *pTile )
{
	ASSERT( NULL != pTile ) ;
	ASSERT( pTile->IsFlower() ) ;
	ASSERT( m_nFlowerCount < MAX_FLOWER_COUNT - 1 ) ;
	
	__try
	{
		Lock() ;

		m_apFlowerTile[ m_nFlowerCount ++ ] = pTile ;
	}
	__finally
	{
		UnLock() ;
	}
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CTKMahjongPlayer::Lock()
{
	::EnterCriticalSection( &m_cs ) ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CTKMahjongPlayer::UnLock()
{
	::LeaveCriticalSection( &m_cs ) ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
BOOL CTKMahjongPlayer::OnResetDump()
{
	// 基本信息
	TCHAR szState[ 32 ];
	if ( HasCalled() )
	{
		_tcscpy_s( szState, "已听牌" );
	}
	else
	{
		_tcscpy_s( szState, "未听牌" );
	}
	if ( IsTrustPlayOrOffLine() )
	{
		_tcscat_s( szState, "已托管" );
	}
	else
	{
		_tcscat_s( szState, "未托管" );
	}
//	TKWriteLog( "ResetDump, %08x, %s, %d, %s", m_dwUserID, m_szNickName, m_nSeatOrder, szState );

	// 手中的牌
	TraceInfo();

	return true;
}

BOOL CTKMahjongPlayer::AwardWare(CTKGamePlayer* pPlayer, int nWareType, int nWareCount)
{
    if (0 >= nWareType || 0 >= nWareCount)
    {
        return FALSE;
    }
    msg_gs2pivot_req_award_ware req_award_ware;
    memset(&req_award_ware, 0, sizeof(req_award_ware));
    req_award_ware.dwType = TKID_GS2PIVOT_AWARD_WARE | TK_REQ;
    req_award_ware.dwLength = sizeof(req_award_ware) - sizeof(TKHEADER);
    req_award_ware.dwUserID = pPlayer->m_dwUserID;
    req_award_ware.dwMatchID = pPlayer->m_dwMatchID;
    req_award_ware.dwMPID = pPlayer->m_nProductID;
    req_award_ware.dwGameID = pPlayer->m_nGameID;
    req_award_ware.dwPlatType = pPlayer->m_wCntTID;
    req_award_ware.dwSrcTypeID = TKCOMMSRV_ENUM_DATA_COMMONWARE_RANDOM;
    req_award_ware.dwWareTypeID = nWareType;
    req_award_ware.dwCount = nWareCount;
    req_award_ware.dwTourneyID = pPlayer->m_dwTourneyID;
    req_award_ware.byOSType = pPlayer->m_byOSType;
    req_award_ware.dwAppID = pPlayer->m_dwAppID;
    req_award_ware.dwSiteID = pPlayer->m_dwSiteID;
    req_award_ware.dwMoneyAccType = pPlayer->m_dwMoneyAccType;

    return tk_pGameService->PostMsg2Pivot_GSBase(&req_award_ware, pPlayer->m_dwUserID, pPlayer->m_dwMatchID
        , pPlayer->m_wStageID, pPlayer->m_wRoundID, NULL);
}

BOOL CTKMahjongPlayer::AwardGold(CTKGamePlayer* pPlayer, int nCount)
{
    if (0 >= nCount)
    {
        return FALSE;
    }
    msg_gs2pivot_req_award_money req_award_money;
    memset(&req_award_money, 0, sizeof(req_award_money));
    req_award_money.dwType = TKID_GS2PIVOT_AWARD_MONEY | TK_REQ;
    req_award_money.dwLength = sizeof(req_award_money) - sizeof(TKHEADER);
    req_award_money.dwUserID = pPlayer->m_dwUserID;
    req_award_money.dwMatchID = pPlayer->m_dwMatchID;
    req_award_money.dwMPID = pPlayer->m_nProductID;
    req_award_money.dwGameID = pPlayer->m_nGameID;
    req_award_money.dwPlatType = pPlayer->m_wCntTID;
    req_award_money.dwCMTSID = TK_COMMSRV_ENUM_CMTSID_RESERVE;
    req_award_money.dwSrcTypeID = TKCOMMSRV_ENUM_DATA_COMMONMONEY_RANDOM;
    req_award_money.dwGold = nCount;
    req_award_money.dwTourneyID = pPlayer->m_dwTourneyID;
    req_award_money.byOSType = pPlayer->m_byOSType;
    req_award_money.dwAppID = pPlayer->m_dwAppID;
    req_award_money.dwSiteID = pPlayer->m_dwSiteID;
    req_award_money.dwMoneyAccType = pPlayer->m_dwMoneyAccType;

    return tk_pGameService->PostMsg2Pivot_GSBase(&req_award_money, pPlayer->m_dwUserID, pPlayer->m_dwMatchID
        , pPlayer->m_wStageID, pPlayer->m_wRoundID, NULL);
}

bool CTKMahjongPlayer::CheckLuckyTile(int nLuckyTileID)
{
    for (int i = 0; i < m_nHandTileCount; i++)
    {
        if (CMahJongTile::SameTile(nLuckyTileID, m_apHandTile[i]->ID()))
        {
            return TRUE;
        }
    }

    for (int i = 0; i < m_nShowGroupCount; i++)
    {
        for (int j = 0;j < 4;++j)
        {
            if (m_apShowGroup[i][j] == NULL)
            {
                break;
            }
            if (CMahJongTile::SameTile(nLuckyTileID, m_apShowGroup[i][j]->ID()))
            {
                return TRUE;
            }
        }
    }
    return CMahJongTile::TileColor(nLuckyTileID) > COLOR_JIAN;
}
