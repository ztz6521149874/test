#pragma once

#include "FSMachine.h"
#include "TKMahjongProtocol.h"

class CTKMahjongGame;
class CTKMahjongServerObj;
class IScoreCalculator;
class CGameRule;
class CGameMachine;
class CMahJongTile;
class IGameState : public IState
{
	friend class CGameMachine;
public:
	virtual ~IGameState(){};

protected:
	// 声明为保护类型,不允许构造
	IGameState();
	IGameState( IGameState & state );
	IGameState & operator =( IGameState state );

public:

	// 获得规则对象
	CGameRule * Rule();

	// 获得算分子对象
	IScoreCalculator * Calculator();

protected:
	BOOL Broadcast( PTKHEADER pMsg );
	BOOL Send2SeatPlayer( int nSeat, PTKHEADER pMsg );

	// 检查网络事件的合法性
	bool CheckMsg( DWORD dwUserID, int nSeat, int nMsgLen, PTKHEADER pHeader, int nRequestID, const char* pcszFile, int nLine );

	// 检查座位号和消息长度
	bool CheckMsgSeatAndLen( DWORD dwUserID, int nSeat, int nMsgLen, PTKHEADER pHeader, const char* pcszFile, int nLine );

	// 检查一下是否所有人都回复了,如果都回复了返回最高级别的回复,否则返回-1
	int CheckAckRequest();

	// 指定的座位上是否有人
	bool HasPlayer( int nSeat );

	// 发送给客户端的通知消息
	/// 如果nNotify是MAHJONG_NOTIFY_SHOW_CHAT_MSG，MAHJONG_NOTIFY_URL_XXX，那么pszText是这条要显示的信息
	/// 如果nNotify是MAHJONG_NOTIFY_DISABLE_RECEIVE，那么pszText其实是一个指向int的指针
	void Notify( int nNotify, const char* pszText = NULL ) ;

	// 设置是否检查消息的座位号是否合法
	void CheckMsgSeat( bool bCheck ) { m_bCheckSeat = bCheck ; }

protected:
	CTKMahjongGame * m_pMahjongGame;
	CTKMahjongServerObj * m_pMahjongObj;
	bool m_bCheckSeat;	// 是否检查座位号是否合法
};



// *********************************************************************************************************************
//
//	等待开始状态
//	
//
// *********************************************************************************************************************
class CPrepareState : public IGameState
{
public:
	// 构造函数
	CPrepareState( /*std::string & szName*/ );
	// 析构函数
	~CPrepareState();

protected:
	// 事件处理
	bool OnStartGame( CEvent * pEvent );

	// 发送游戏基本信息给客户端
	void BroadcastGameBaseMsg();

	// 确定各家的游戏座位号和庄家
	void DecideGameSeatAndBanker() ;

	// 定庄、定游戏座位、门风、圈风
	void SetPlace() ;
};



// *********************************************************************************************************************
//
//	洗牌状态
//	
//
// *********************************************************************************************************************
class CShuffleState : public IGameState
{
public:
	// 构造函数
	CShuffleState();
	// 析构函数
	~CShuffleState();

protected:
	// 处入时的处理
	virtual void OnEnter( CEvent * pEvent );

	// 洗牌
	void Shuffle();

	// 洗牌结束
	bool OnShuffleEnd( CEvent * pEvent );
};



// *********************************************************************************************************************
//
//	开始掷色子状态
//	
//
// *********************************************************************************************************************
class CCastDiceState : public IGameState
{
public:
	// 构造函数
	CCastDiceState();
	// 析构函数
	~CCastDiceState();

protected:
	// 处入时的处理
	virtual void OnEnter( CEvent * pEvent );

	// 掷色子
	void CastDice();

	// 掷色子结束
	bool OnCastDiceEnd( CEvent * pEvent );

private:
	int m_anOpenDoor[ 2 ];				// 开门参数
};



// *********************************************************************************************************************
//
//	开门状态
//	
//
// *********************************************************************************************************************
class COpenDoorState : public IGameState
{
public:
	// 构造函数
	COpenDoorState();
	// 析构函数
	~COpenDoorState();

	//// 定时检查
	//virtual void OnTickCount();

protected:
	// 进入时的处理
	virtual void OnEnter( CEvent * pEvent );

	// 开牌
	void OpenDoor( int nOpenDoorSeat , int nOpenDoorFrusta );

	// 事件处理
	bool OnAckOpenDoor( CEvent * pEvent );
	bool OnOpenDoorEnd( CEvent * pEvent );
	bool OnTrustPlay( CEvent * pEvent );

	// 是否所有的玩家都已经开牌结束了
	bool AllPlayerOpenDoor() ;

private:
	bool m_abOpenDoor[ MAX_PLAYER_COUNT ];	// 是否所有人都应答了
	DWORD m_dwStartTime;					// 进入时的时间
};

// *********************************************************************************************************************
//
// 补花状态( 开始打牌前补完所有手中的花 )
//	
//
// *********************************************************************************************************************
class CChangeAllFlowerState : public IGameState
{
public:
	// 构造函数
	CChangeAllFlowerState();
	// 析构函数
	~CChangeAllFlowerState(){};

protected:
	// 进入时的处理
	virtual void OnEnter( CEvent * pEvent );

	// 请求玩家补花
    void SendRequestChangeFlowerMsg(int nSeat, int nWaitSecond);

	// 自动给这个玩家补花
	void AutoChangeFlower( int nSeat );

	//////////////
	// 事件处理 //
	//////////////
	// 客户端请求补花
	bool OnReqChangeFlower( CEvent * pEvent );

	// 客户端放弃补花
	bool OnAckRequest( CEvent * pEvent );

	// 补花结束
	bool OnChangeAllFlowerEnd( CEvent * pEvent );

	// 玩家托管了
	bool OnTrustPlay( CEvent * pEvent );
private:
	// 客户端请求补花实际处理函数
	bool OnReqChangeFlower( PTKREQMAHJONGCHANGEFLOWER pReqChangeFlower );

	// 客户端放弃补花实际处理函数
	bool OnAckRequest( PTKACKMAHJONGREQUEST pAckMahJongRequest );

private:
	int m_nCurSeat;	// 当前正在请求这个玩家补花

	char m_buffer[ sizeof( TKREQMAHJONGCHANGEFLOWER ) ];// 公用缓冲区
};



// *********************************************************************************************************************
//
// 天听状态
//	
//
// *********************************************************************************************************************
class CTianTingState : public IGameState
{
public:
	// 构造函数
	CTianTingState();
	// 析构函数
	~CTianTingState(){};

protected:
	// 进入时的处理
	virtual void OnEnter( CEvent * pEvent );

	//////////////
	// 事件处理 //
	//////////////
	// 客户端放弃听牌
	bool OnAckRequest( CEvent * pEvent );

	// 客户端请求听牌
	bool OnReqCall( CEvent * pEvent );

	// 天听结束
	bool OnTianTingEnd( CEvent * pEvent );

	// 玩家托管了
	bool OnTrustPlay( CEvent * pEvent );

	// 玩家听牌牌张信息
	bool OnReqCallTile( CEvent * pEvent );

    // 玩家请求发起超时检测
    bool OnCheckTimeout( CEvent * pEvent );

private:
	// 请求客户端天听
	void SendRequestCallMsg( int nBanker );

	// 客户端放弃听牌实际处理函数
	bool OnAckRequest( PTKACKMAHJONGREQUEST pAckMahJongRequest );

	// 客户端请求听牌实际处理函数
	bool OnReqCall( PTKREQMAHJONGCALL pReqMahJongCall );

	// 检查是否所有人都回复了,如果都回复了做相应的处理
	void CheckAndDealAckRequest();

	// 玩家听牌牌张信息
	bool OnReqCallTile( PTKREQMAHJONGCALLTILE pReqMahjongCallTile );
};




// *********************************************************************************************************************
//
// 等待玩家出牌状态
// 这个状态下玩家可能的操作:
// 1.补花
// 2.杠牌
// 3.出牌
// 4.听牌并出牌
// 5.和牌
//	
//
// *********************************************************************************************************************
class CDiscardTileState : public IGameState
{
public:
	// 构造函数
	CDiscardTileState();
	// 析构函数
	~CDiscardTileState(){};

protected:
	// 进入时的处理
	virtual void OnEnter( CEvent * pEvent );

	//////////////
	// 事件处理 //
	//////////////
	// 玩家请求补花
	bool OnReqChangeFlower( CEvent * pEvent );

	// 玩家请求杠牌
	bool OnReqGang( CEvent * pEvent );

	// 玩家请求出牌
	bool OnReqDiscardTile( CEvent * pEvent );

	// 玩家请求听牌
	bool OnReqCall( CEvent * pEvent );

	// 玩家请求和牌
	bool OnReqWin( CEvent * pEvent );

	// 玩家暗杠
	bool OnAnGang( CEvent * pEvent );

	// 等待抢杠了
	bool OnBuGang( CEvent * pEvent );

	// 玩家请求加倍
	bool OnReqDouble( CEvent * pEvent );

	// 玩家自摸了
	bool OnPlayerWinSelf( CEvent * pEvent );

	// 玩家加倍（自摸）
	bool OnPlayerDouble( CEvent * pEvent );

	// 玩家托管了
	bool OnTrustPlay( CEvent * pEvent );

	// 玩家请求发起超时检测
	bool OnCheckTimeout( CEvent * pEvent );

	// 玩家听牌牌张信息
	bool OnReqCallTile( CEvent * pEvent );

private:
	// 玩家请求补花实际处理函数
	bool OnReqChangeFlower( PTKREQMAHJONGCHANGEFLOWER pReqChangeFlower );

	// 玩家请求杠牌实际处理函数
	bool OnReqGang( PTKREQMAHJONGGANG pReqMahJongGang );

	// 玩家请求出牌实际处理函数
	bool OnReqDiscardTile( PTKREQMAHJONGDISCARDTILE pReqMahJongDiscardTile );

	// 玩家请求加倍的实际处理函数
	bool OnReqDouble( PTKREQMAHJONGDBL pReqMahjongDbl );

	// 玩家请求听牌实际处理函数
	bool OnReqCall( PTKREQMAHJONGCALL pReqMahJongCall );

	// 玩家请求和牌实际处理函数
	bool OnReqWin( PTKREQMAHJONGWIN pReqMahJongWin );

	// 请求玩家出牌
	void SendRequestDiscardTileMsg( int nSeat );
	void SendRequestDiscardTileMsgEx( int nSeat );

	// 自动帮玩家出牌
	void AutoDiscardTile( int nSeat );

	// 重置加倍属性
	void ResetDbl() { m_canDbl = true; }

	// 自动胡牌
	void AutoWin( int nSeat, CMahJongTile* nTile);

	// 玩家听牌牌张信息
	bool OnReqCallTile( PTKREQMAHJONGCALLTILE pReqMahjongCallTile );

protected:
	char m_buffer[ sizeof( TKREQMAHJONGDISCARDTILE ) ];// 公用缓冲区
	bool m_canDbl;
	char m_winBuffer[ sizeof( TKREQMAHJONGWIN ) ];		// 自动胡牌缓冲区
};



// *********************************************************************************************************************
//
// 等待玩家抢杠状态
//	
//
// *********************************************************************************************************************
class CWaitWinGangState : public IGameState
{
public:
	// 构造函数
	CWaitWinGangState();
	// 析构函数
	~CWaitWinGangState(){};
protected:
	// 进入时的处理
	virtual void OnEnter( CEvent * pEvent );

	//////////////
	// 事件处理 //
	// 有人请求抢杠
	bool OnReqWin( CEvent * pEvent );

	// 客户端放弃抢杠
	bool OnAckRequest( CEvent * pEvent );

	// 没人请求抢杠
	bool OnNoReqWin( CEvent * pEvent );

	// 有人抢杠成功
	bool OnWinGang( CEvent * pEvent );

	// 有人加倍
	bool OnPlayerDouble( CEvent * pEvent );

	// 玩家托管了
	bool OnTrustPlay( CEvent * pEvent );

	// 玩家请求加倍
	bool OnReqDouble( CEvent * pEvent );

	// 玩家请求发起超时检测
	bool OnCheckTimeout( CEvent * pEvent );

private:
	// 有人请求抢杠的实际处理函数
	bool OnReqWin( PTKREQMAHJONGWIN pReqMahJongWin );

	// 客户端放弃抢杠实际处理函数
	bool OnAckRequest( PTKACKMAHJONGREQUEST pAckMahJongRequest );

	// 玩家请求加倍的实际处理函数
	bool OnReqDouble( PTKREQMAHJONGDBL pReqMahjongDbl );

	// 询问是否有人要抢杠
	void SendRequestWinGangMsg( int nGangSeat );
	void SendRequestWinGangMsgEx( int nGangSeat );

	// 检查是否所有人都回复了,如果都回复了做相应的处理
	void CheckAndDealAckRequest();

	// 自动胡牌
	void AutoWin( int nSeat, CMahJongTile* nTile);

protected:
	char m_winBuffer[ sizeof( TKREQMAHJONGWIN ) ];		// 自动胡牌缓冲区

private:
	bool m_canDbl;
};



// *********************************************************************************************************************
//
// 等待其它玩家吃碰杠和状态
//	
//
// *********************************************************************************************************************
class CWaitState : public IGameState
{
public:
	// 构造函数
	CWaitState();
	// 析构函数
	~CWaitState(){};

protected:
	// 进入时的处理
	virtual void OnEnter( CEvent * pEvent );

	//////////////
	// 事件处理 //
	// 玩家请求杠牌
	bool OnReqGang( CEvent * pEvent );

	// 玩家请求吃牌
	bool OnReqChi( CEvent * pEvent );

	// 玩家请求碰牌
	bool OnReqPeng( CEvent * pEvent );

	// 玩家请求加倍
	bool OnReqDouble( CEvent * pEvent );

	// 玩家请求和牌
	bool OnReqWin( CEvent * pEvent );

	// 玩家放弃
	bool OnAckRequest( CEvent * pEvent );

	// 有人杠牌
	bool OnPlayerGang( CEvent * pEvent );

	// 有人吃牌
	bool OnPlayerChi( CEvent * pEvent );

	// 有人加倍（点炮）
	bool OnPlayerDouble( CEvent * pEvent );

	// 有人碰牌
	bool OnPlayerPeng( CEvent * pEvent );

	// 有人和牌
	bool OnPlayerWin( CEvent * pEvent );

	// 所有人都放弃了
	bool OnAllAbort( CEvent * pEvent );

	// 玩家托管了
	bool OnTrustPlay( CEvent * pEvent );

	// 玩家请求发起超时检测
	bool OnCheckTimeout( CEvent * pEvent );

private:
	// 玩家请求吃牌的实际处理函数
	bool OnReqChi( PTKREQMAHJONGCHI pReqMahJongChi );

	// 玩家请求碰牌的实际处理函数
	bool OnReqPeng( PTKREQMAHJONGPENG pReqMahJongPeng );

	// 玩家请求杠牌实际处理函数
	bool OnReqGang( PTKREQMAHJONGGANG pReqMahJongGang );

	// 玩家请求加倍的实际处理函数
	bool OnReqDouble( PTKREQMAHJONGDBL pReqMahjongDbl );

	// 玩家请求和牌实际处理函数
	bool OnReqWin( PTKREQMAHJONGWIN pReqMahJongWin );

	// 玩家放弃实际处理函数
	bool OnAckRequest( PTKACKMAHJONGREQUEST pAckMahJongRequest );

	// 询问是否有人要吃碰杠和
	void SendRequestMsg();

	// 询问是否有人要吃碰杠和加倍
	void SendRequestMsgEx();

	// 检查是否所有人都回复了,如果都回复了做相应的处理
	void CheckAndDealAckRequest();

	// 自动胡牌
	void AutoWin( int nSeat, CMahJongTile* nTile);

protected:
	char m_winBuffer[ sizeof( TKREQMAHJONGWIN ) ];		// 自动胡牌缓冲区

private:
	bool m_canDbl; // 加倍控制
};



// *********************************************************************************************************************
//
// 玩家抓牌状态( 普通 )
//	
//
// *********************************************************************************************************************
class CDrawTileState : public IGameState
{
public:
	// 构造函数
	CDrawTileState();
	// 析构函数
	~CDrawTileState(){};

protected:
	// 进入时的处理
	virtual void OnEnter( CEvent * pEvent );

	//////////////
	// 事件处理 //
	// 抓牌成功
	bool OnDrawSucceed( CEvent * pEvent );

	// 没牌了
	bool OnNoTile( CEvent * pEvent );
};



// *********************************************************************************************************************
//
// 玩家抓牌状态( 杠后抓牌 )
//	
//
// *********************************************************************************************************************
class CDrawTileAfterGangState : public IGameState
{
public:
	// 构造函数
	CDrawTileAfterGangState();
	// 析构函数
	~CDrawTileAfterGangState(){};

protected:
	// 进入时的处理
	virtual void OnEnter( CEvent * pEvent );

	//////////////
	// 事件处理 //
	// 抓牌成功
	bool OnDrawSucceed( CEvent * pEvent );

	// 没牌了
	bool OnNoTile( CEvent * pEvent );
};



// *********************************************************************************************************************
//
// 玩家抓牌状态( 补花后抓牌 )
//	
//
// *********************************************************************************************************************
class CDrawTileAfterChangeFlowerState : public IGameState
{
public:
	// 构造函数
	CDrawTileAfterChangeFlowerState();
	// 析构函数
	~CDrawTileAfterChangeFlowerState(){};

protected:
	// 进入时的处理
	virtual void OnEnter( CEvent * pEvent );

	//////////////
	// 事件处理 //
	// 抓牌成功
	bool OnDrawSucceed( CEvent * pEvent );

	// 没牌了
	bool OnNoTile( CEvent * pEvent );
};



// *********************************************************************************************************************
//
// 等待玩家出牌状态
// 这个状态下玩家可能的操作:
// 1.出牌
// 2.听牌并出牌
//	
//
// *********************************************************************************************************************
class CDiscardAfterChiPengState : public IGameState
{
public:
	// 构造函数
	CDiscardAfterChiPengState();
	// 析构函数
	~CDiscardAfterChiPengState(){};

protected:
	// 进入时的处理
	virtual void OnEnter( CEvent * pEvent );

	//////////////
	// 事件处理 //
	//////////////
	// 玩家请求出牌
	bool OnReqDiscardTile( CEvent * pEvent );

	// 玩家请求听牌
	bool OnReqCall( CEvent * pEvent );

	// 玩家托管了
	bool OnTrustPlay( CEvent * pEvent );

	// 玩家请求发起超时检测
	bool OnCheckTimeout( CEvent * pEvent );

	// 玩家听牌牌张信息
	bool OnReqCallTile( CEvent * pEvent );

private:
	// 玩家请求出牌实际处理函数
	bool OnReqDiscardTile( PTKREQMAHJONGDISCARDTILE pReqMahJongDiscardTile );

	// 玩家请求听牌实际处理函数
	bool OnReqCall( PTKREQMAHJONGCALL pReqMahJongCall );

	// 请求玩家出牌
	void SendRequestDiscardTileMsg( int nSeat );

	// 自动帮玩家出牌
	void AutoDiscardTile( int nSeat );

	// 玩家听牌牌张信息
	bool OnReqCallTile( PTKREQMAHJONGCALLTILE pReqMahjongCallTile );

protected:
	char m_buffer[ sizeof( TKREQMAHJONGDISCARDTILE ) ];// 公用缓冲区
};







// *********************************************************************************************************************
//
//  吃后杠，碰后杠	
//
// *********************************************************************************************************************
class CDiscardGangAfterChiPengState : public IGameState
{
public:
	// 构造函数
	CDiscardGangAfterChiPengState();
	// 析构函数
	~CDiscardGangAfterChiPengState(){};

protected:
	// 进入时的处理
	virtual void OnEnter( CEvent * pEvent );

	//////////////
	// 事件处理 //
	//////////////
	// 玩家请求出牌
	bool OnReqDiscardTile( CEvent * pEvent );

	// 玩家请求听牌
	bool OnReqCall( CEvent * pEvent );

	// 玩家托管了
	bool OnTrustPlay( CEvent * pEvent );

	// 玩家请求发起超时检测
	bool OnCheckTimeout( CEvent * pEvent );

	// 玩家听牌牌张信息
	bool OnReqCallTile( CEvent * pEvent );

	// 玩家请求杠牌
	bool OnReqGang( CEvent * pEvent );

private:
	// 玩家请求出牌实际处理函数
	bool OnReqDiscardTile( PTKREQMAHJONGDISCARDTILE pReqMahJongDiscardTile );

	// 玩家请求听牌实际处理函数
	bool OnReqCall( PTKREQMAHJONGCALL pReqMahJongCall );

	// 请求玩家出牌
	void SendRequestDiscardTileMsg( int nSeat );

	// 自动帮玩家出牌
	void AutoDiscardTile( int nSeat );

	// 玩家听牌牌张信息
	bool OnReqCallTile( PTKREQMAHJONGCALLTILE pReqMahjongCallTile );

	// 玩家请求杠牌实际处理函数
	bool OnReqGang( PTKREQMAHJONGGANG pReqMahJongGang );

	bool OnPlayerGang(CEvent * pEvent);

	bool OnBuGang(CEvent * pEvent);

protected:
	char m_buffer[ sizeof( TKREQMAHJONGDISCARDTILE ) ];// 公用缓冲区
};




// *********************************************************************************************************************
//
// 和牌状态
//	
//
// *********************************************************************************************************************
class CWinState : public IGameState
{
public:
	// 构造函数
	CWinState();
	// 析构函数
	~CWinState(){};

protected:
	// 进入时的处理
	virtual void OnEnter( CEvent * pEvent );

	//////////////
	// 事件处理 //
	//////////////
	// 还剩1个玩家未和牌
	bool On1PlayerNotWin( CEvent * pEvent );

	// 还剩2个玩家未和牌
	bool On2PlayerNotWin( CEvent * pEvent );

	// 还剩3个玩家未和牌
	bool On3PlayerNotWin( CEvent * pEvent );

	// 奖花
	bool OnLuckyTile( CEvent * pEvent );


protected:
	// 让所有要和牌的玩家和牌
	bool Win( PTKREQMAHJONGWIN apReqMahJongWin[], int cnReqWin, bool bWinSelf );

	// 让指定的玩家和牌
	//bool Win( PTKREQMAHJONGWIN pReqMahJongWin );

private:
	int m_cnWinner;		// 已和牌玩家数
	int m_nLine;
	char m_szBuffer[128];
};

// *********************************************************************************************************************
//
// 奖花
//	
//
// *********************************************************************************************************************
class CLuckyTileState : public IGameState
{
public:
	// 构造函数
	CLuckyTileState();
	// 析构函数
	~CLuckyTileState(){};

protected:
	// 进入时的处理
	virtual void OnEnter( CEvent * pEvent );

	//////////////
	// 事件处理 //
	//////////////
	// 还剩1个玩家未和牌
	bool On1PlayerNotWin( CEvent * pEvent );

	// 还剩2个玩家未和牌
	bool On2PlayerNotWin( CEvent * pEvent );

	// 还剩3个玩家未和牌
	bool On3PlayerNotWin( CEvent * pEvent );


	// 分数计算
	bool CalcScore(int nLuckyTileHitCount);
	
};



// *********************************************************************************************************************
//
// 结果状态
//	
//
// *********************************************************************************************************************
class CResultState : public IGameState
{
public:
	// 构造函数
	CResultState();
	// 析构函数
	~CResultState(){};

protected:
	// 进入时的处理
	virtual void OnEnter( CEvent * pEvent );
};




// *********************************************************************************************************************
//
// 和牌状态
//	
//
// *********************************************************************************************************************
class CBloodyWinState : public IGameState
{
public:
	// 构造函数
	CBloodyWinState();
	// 析构函数
	~CBloodyWinState(){};

protected:
	// 进入时的处理
	virtual void OnEnter( CEvent * pEvent );

	//////////////
	// 事件处理 //
	//////////////
	// 还剩1个玩家未和牌
	bool On1PlayerNotWin( CEvent * pEvent );

	// 还剩2个玩家未和牌
	bool On2PlayerNotWin( CEvent * pEvent );

	// 还剩3个玩家未和牌
	bool On3PlayerNotWin( CEvent * pEvent );

	// 奖花
	bool OnLuckyTile( CEvent * pEvent );

protected:
	// 让所有要和牌的玩家和牌
	bool Win( PTKREQMAHJONGWIN apReqMahJongWin[], int cnReqWin, bool bWinSelf );

	// 让指定的玩家和牌
	//bool Win( PTKREQMAHJONGWIN pReqMahJongWin );

private:
	int m_cnWinner;		// 已和牌玩家数
};



// *********************************************************************************************************************
//
// 结果状态
//	
//
// *********************************************************************************************************************
class CBloodyResultState : public IGameState
{
public:
	// 构造函数
	CBloodyResultState(){};
	// 析构函数
	~CBloodyResultState(){};

protected:
	// 进入时的处理
	virtual void OnEnter( CEvent * pEvent );
};