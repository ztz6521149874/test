/*
* Copyright (c) 2018,JJWorld
* All rights reserved.
* 
* 文件名称：TKMahjongGameProxy.h
* 摘    要：用于包装MahjongGame类，使其不需要暴露太多信息
* 
* 当前版本：1.0
* 作    者：wangyb01
* 完成日期：2018年1月11日
*/

#pragma once

#include "tkgame.h"

class TKMahjongGameProxy
{
private:
	CTKGame* m_pMahjongGame;		// Game

public:
	TKMahjongGameProxy();
	virtual ~TKMahjongGameProxy();
	
	// 初始化接口
	BOOL InitInstance(CTKGame * pGame);

public: // 消息发送接口包装
	// 广播
	BOOL Broadcast(PTKHEADER pMsg);

	// 发送给observer型的旁观者
	BOOL Send2Observer(PTKHEADER pMsg);

	// 发送给指定玩家
	BOOL Send2SeatPlayer(int nSeat, PTKHEADER pMsg);

	// 发送给black型的旁观者
	BOOL Send2Black(PTKHEADER pMsg);

	// 发送指定black型的旁观者
	BOOL Send2SeatBlack(int nSeat, PTKHEADER pMsg);

	// 发送给玩家
	BOOL Send2Player( DWORD dwUserID, PTKHEADER pMsg );
public :
	// 获取比赛ID
	int GetMatchID();
};


