#pragma once

#include "tkgame.h"
#include "TKMahjongProtocol.h"
#include "TKMahjongGameProxy.h"
#include "TKMahjongProtobufFactory.h"
#include "GameRule.h"

// *********************************************************************************************************************
//
//
//
//
// *********************************************************************************************************************
typedef struct STaxList
{
	int minFans;
	int maxFans;
	int tax;
	STaxList()
	{
		memset( this, 0, sizeof(*this) );
	}
}TAXLIST;

class CGameMachine;
class CTKMahjongServerObj;
class IScoreCalculator;
class CRequestObj;
class CTKMahjongGame : public CTKGame
{
public:
	CTKMahjongGame(void);
	~CTKMahjongGame(void);

	// 初始化
	virtual BOOL OnInitialUpdate();
	virtual int  GetLastHandSaveInfoLen();
	virtual void OnPlayerTickCount(CTKGamePlayer *pGamePlayer);

public:
    //设置用户的游戏结果
    void SetGameResult
    ( 
        int in_nSeat,
        int in_nWinPlus,
        int in_nLossPlus,
        int in_nDrawPlus,
        int& io_nScorePlus, 
        BOOL in_bValideHand = TRUE, 
        BOOL in_bLevyTax = TRUE, 
        WORD in_wGameResult = TK_UNKNOWN
    );

	bool IsEndGameEarly(int nSeat);

	bool IsIslandMatch() { return m_nScoreType == TK_GAME_SCORETYPE_LIFE; }; //是否是岛屿赛

public:
	virtual void OnPlayerArrived( CTKGamePlayer *pGamePlayer );
	virtual BOOL OnGameBegin();
    virtual BOOL OnPlayerMsg(DWORD dwUserID, int nSeatOrder, PTKHEADER pMsg);
	virtual void OnPlayerNetBreak( CTKGamePlayer *pGamePlayer );
	virtual void OnPlayerNetResume( CTKGamePlayer *pGamePlayer );
		void SetPlayerBreak( int seat, bool isBreak );
		bool IsPlayerQuitOnOwn( int seatOrder );
		bool IsPlayerPlaying( int seatOrder );
	virtual	BOOL OnResetDump();
	virtual BOOL IsEnableLockdown();
	virtual void OnGameTimer(DWORD dwID, ULONG_PTR ulUserData) ;

	void AddCashFlow( int nCashFlow, int nTax );
	void State();

	DWORD GetRtValue();			// 获得与游戏开始的时差
	void AddGameAction(char* szActionDescribe, bool bFirst = false);
	void AddGameResult(char* szResultDescribe);
public:
	CGameRule * Rule() { return &m_GameRule; };
	IScoreCalculator * Calculator() { return m_pCalculator; };
	CRequestObj * RequestObj() { return m_pRequestObj; };
	BOOL IsHaveplayerSeat(const int& seat);		//判断这个座位上是否有玩家
	TKMahjongProtobufFactory * GetProtobufFactory() { return m_pProtoFactory; };
    void SendChangeFlowerAck(int nFlowerCount, PTKREQMAHJONGCHANGEFLOWER pReq);
public: // 实现基类发包接口
	/************************************************************************/
	/* 以下这些接口会重组消息包成为protobuf协议 并发送                        */
	/************************************************************************/
	BOOL	GameBroadcast(PTKHEADER pMsg);
	BOOL	GameSend2SeatPlayer(int nSeat,PTKHEADER pMsg);
	BOOL	GameSend2Black(PTKHEADER pMsg);
	BOOL	GameSend2SeatBlack(int nSeat, PTKHEADER pMsg);
	BOOL	GameSend2Observer(PTKHEADER pMsg);
	BOOL	GameSend2Player( DWORD dwUserID, PTKHEADER pMsg );

    void SendMsg(TKMobileAckMsg& rMsg, int nSeat = -1);
protected:
	// 初始化游戏数据（第一局游戏开始前调用这个函数）
	void InitAllGameData() ;
	// 重置游戏数据（每局游戏开始的时候调用这个函数）
	void ResetGameData() ;

	// 创建算分子
	IScoreCalculator * CreateCalculator( CGameRule * pGameRule, int nPlayerCount, int nMulti );

	// 是否所有人都托管了
	BOOL AllPlayerTrustPlay();
	BOOL AllIsBotTrustPlay();

	//是否有人听牌
	BOOL HasPlayerTing();

private:
    void _sendGameBeginAck();

private:
	CGameMachine *			m_pGameMachine;		// 麻将对象
	CTKMahjongServerObj	*	m_pMahjongObj;		// 麻将对象
	CGameRule				m_GameRule;			// 游戏规则对象
	IScoreCalculator *		m_pCalculator ;		// 分数计算子
	CRequestObj *			m_pRequestObj ;		// 请求信息对象
	TKMahjongGameProxy      m_GameProxy;		// 对Game进行必要的封装
	TKMahjongProtobufFactory*	m_pProtoFactory ;	// protobuf协议转换工厂

	int							m_nCashFlow;
	int							m_nTax;
	DWORD					m_dwGameBegin;		// 记录游戏开始时间

private:
    std::vector<int> m_vecScoreSysCompensate;   // 系统赔付
};
