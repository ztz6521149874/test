#pragma once

#include "TKMahjongProtocol.h"
#include <vector>
#include "assert.h"
#include "tkgameprotocol.h"
#include <queue>
#include "TKMahjongPlayer.h"

// *********************************************************************************************************************
//
//
//
//
// *********************************************************************************************************************
class CTKGameRule;
class CMahJongTile;
class IJudge;
class CGameRule;
class ITileWall;
class CTKMahjongGame;
class CTKMahjongObj
{
public:
	// 构造/析构
	CTKMahjongObj(void);
	virtual ~CTKMahjongObj(void);

	// 初始化
    virtual bool InitInstance(CTKMahjongGame* pGame, CGameRule * pRule);

    void _initTiles(CGameRule * pRule);

	// 每一局开始时，重置游戏数据
	virtual void ResetGameData() ;

    void _split(const string& s, const string& delim, vector<string>& ret);
    void _initTileManual(MAHJONGTILECOUNTINFO& rCardInfo, const string& strTile);
    void _initTilesManual(char * szIniFile);
public:
	// 添加一个玩家
	virtual CTKMahjongPlayer * AddPlayer(CTKGamePlayer * pPlayer);

	// 设置游戏座位号信息和庄家
	void SetGameSeatAndBanker( int* pnChangePlaceRule , int nBanker ) ;
	// 设置圈风和门风信息
	/// 在调用这个函数之前，各个玩家的游戏座位号信息和庄家信息已经确定
	bool SetWind( int nRoundWind ) ;

	// 获得总牌数量信息
	int AllTileCount() { return m_nTileCount; }

	// 获得玩家信息
	int PlayerCount() { return m_cnPlayer; }						// 获得玩家个数
	// 指定的座位上是否有人
	bool HasPlayer( int nSeat );
	CTKMahjongPlayer* GetPlayerBySeat( int nSeat ) ;				// 从座位号获得玩家信息
	CTKMahjongPlayer* GetPlayerByGameSeat( int nGameSeat ) ;		// 从游戏座位号号获得玩家信息
	CTKMahjongPlayer* GetPlayerByWind( int nWind ) ;				// 从门风号获得玩家信息
	CTKMahjongPlayer* GetPlayerByID( DWORD dwUserID ) ;				// 从用户ID获得玩家信息
	
	// 取得指定游戏座位号对应的座位号(这个座位号上可能没人)
	int GetSeatByGameSeat( int nGameSeat ){ nGameSeat %= MAX_PLAYER_COUNT; return m_anSeat[ nGameSeat ]; }

	// 庄家信息
	void Banker( int nSeat ) { m_nBanker = nSeat ; }	// 设置庄家
	int Banker() { return m_nBanker ; }					// 获得庄家

	// 游戏中的一些信息
	void CurDiscardSeat( int nSeat ) ;		// 设置当前出牌者
	int CurDiscardSeat() { return m_nCurDiscardSeat ; }	// 当前出牌者
	void LastDiscardSeat(int nSeat) { m_nLastDiscardSeat = nSeat; }		// 上轮出牌者
	int LastDiscardSeat() { return m_nLastDiscardSeat ; }	// 上轮出牌者
	int GetNextPlayerSeat( int nSeat, bool bIgnoreWinner = true ) ;	// 返回nSeat的下家的座位号
	int GetPrevPlayerSeat( int nSeat, bool bIgnoreWinner = true ) ;	// 返回nSeat的上家的座位号
	BOOL IsDiscardTile( int nTileID ) ;		// 是否是当前出的牌
	BOOL IsLastGangTile( int nTileID ) ;	// 是否是最后一个杠的牌
	int LastGangSeat() { return m_nLastGangSeat ; }	// 最后一个杠牌的玩家座位号
	CMahJongTile * CurDiscardTile() { return m_pCurDiscardTile ; }	// 最后出的那张牌
	int HunGangSeat() { return m_nHunGangSeat ; }	// 混杠玩家的座位号
	CMahJongTile * GetLastGangTile() { return m_pLastGangTile ; }	// 最后杠的那张牌

	// 设置开牌位置信息
	void SetOpenDoorPos( int nOpenDoorSeat , int nOpenDoorFrusta ) ;
	
	// 从牌墙中取一张牌
	CMahJongTile* GetOneTileFromWall( bool bFromWallHeader , int nOffset ) ;

	// 增加亮在桌面上的明牌张数
	bool AddShowTileCount( int nTileID, int nIncrement = 1 );
	
	// 判断某个玩家是否未出过牌，也没吃碰杠过牌
	bool IsTian( int nSeat ) { return m_abTian[nSeat]; }
	
	// 生成和牌方式组合	
	int	WinMode( int nWinSeat, int nPaoSeat, int nWinTileID );		
	
public:
	// 洗牌
	void Shuffle( int anFrustaOfSeat[] );

public :
	// 玩家补花
	/// 返回要补的这张花牌
    int PlayerChangeFlower(PTKREQMAHJONGCHANGEFLOWER pReqMahJongChangeFlower);
	// 玩家抓了一张牌
	/// 返回抓的这张牌
	CMahJongTile* PlayerDrawTile( PTKREQMAHJONGDRAWTILE pReqMahJongDrawTile ) ;
	// 玩家出了一张牌
	/// 返回出的这张牌
	CMahJongTile* PlayerDiscardTile( PTKREQMAHJONGDISCARDTILE pReqMahJongDiscardTile ) ;
	// 玩家吃牌
	/// apChiTile中返回吃的这三张牌（两张玩家手上的，一张玩家出的）
	/// 函数返回的是其他玩家出的那张牌
	CMahJongTile* PlayerChiTile( PTKREQMAHJONGCHI pReqMahJongChi , CMahJongTile* apChiTile[ 3 ] ) ;
	// 玩家碰牌
	/// apPengTile中返回碰的这三张牌（两张玩家手上的，一张玩家出的）
	/// 函数返回的是其他玩家出的那张牌
	CMahJongTile* PlayerPengTile( PTKREQMAHJONGPENG pReqMahJongPeng , CMahJongTile* apPengTile[ 3 ] ) ;
	// 玩家杠牌
	/// apGangTile中返回杠的这四张牌，nCount中返回apGangTile中的张数（如果是补杠，nCount为1，否则为4）
	/// 函数返回的是其他玩家出的那张牌（如果是暗杠或者补杠，返回值为NULL）
	CMahJongTile* PlayerGangTile( PTKREQMAHJONGGANG pReqMahJongGang , CMahJongTile* apGangTile[ 4 ] , int& nCount, bool bAfterPeng = false ) ;

protected :
	// 获得某张牌的张数信息
	PMAHJONGTILECOUNTINFO GetTileCountInfo( int nColor , int nWhat ) ;

	// 从出的牌中查找某张牌
	CMahJongTile* FindDiscardTile( int nTileID ) ;

	// 从吃碰杠的牌中查找某张牌
	CMahJongTile* FindShowTile( int nTileID ) ;

	// 取某张牌在牌列中的索引号
	int GetTileValue( int nTileID );

	// 是否绝张
	BOOL IsOnlyOne( int nTileID, BOOL bWinSelf = FALSE );

	// 重置最后一个杠牌的信息
	void ResetLastGangInfo() ;

	// 取得一个备用的牌
	CMahJongTile * GetOccasionalTile( int nTileID );

protected:
	// 初始化函数
	virtual bool OnInitInstance() ;

	// 创建麻将牌
	/// i是这张牌的序号
	virtual CMahJongTile* CreateTile( CGameRule * pRule ) ;

	// 创建玩家对象
	/// nSeat是这个玩家的座位号
    virtual CTKMahjongPlayer* CreatePlayer(int nSeat, CTKGamePlayer * pPlayer) = 0;

	// 创建Judge
	virtual IJudge * CreateJudge( int nMahjongType );

	// 创建牌墙
	virtual ITileWall * CreateTileWall( int nMahjongType );

	// 洗牌
	virtual void OnShuffle(){};

protected:
    CTKMahjongGame* m_pMahjongGame;                 //游戏对象
	CGameRule * m_pGameRule;						// 游戏规则

	CTKMahjongPlayer * m_apPlayer[ MAX_PLAYER_COUNT ] ;	// 玩家信息
	int m_cnPlayer;									// 玩家个数

	IJudge * m_pJudge ;								// 吃碰杠听和检验

	CMahJongTile *m_apAllTile[ MAX_TILE_COUNT ] ;	// 所有的麻将牌
    deque< MAHJONGTILECOUNTINFO> m_vMannulTiles;
	int m_nTileCount ;								// 总共的牌的张数
	MAHJONGTILECOUNTINFO m_apTileCountInfo[ 42 ] ;	// 42种牌（万1-9，条1-9，饼1-9，东南西北，中发白，梅兰竹菊，春夏秋冬）使用的张数信息（注意客户端和服务器端对“用”了的牌定义不同，客户端是指出的牌、自己的牌、大家吃碰杠的牌；服务器端还包括其他人手中的牌）
	int m_acnShowTile[ 34 ] ;						// 34种牌（万1-9，条1-9，饼1-9，东南西北，中发白）使用的张数信息(主要用于和绝张，只包括桌面上亮明了的牌)

	ITileWall * m_pTileWall;						// 牌墙

	int m_nCurDiscardSeat ;							// 当前出牌者座位号
													/// 玩家出牌前，这个是当前应该出牌的玩家；
													/// 玩家出牌后，询问大家是否吃碰杠和牌时，这个是已经出牌的玩家；
													/// 大家回复了吃碰杠和牌消息后，这个是下一个该出牌的玩家；
	CMahJongTile *m_pCurDiscardTile ;				// 最后一个出牌者出的牌
	int m_nLastDiscardSeat;							// 最后一个出牌者

	int m_nCurDrawSeat ;							// 当前抓牌者座位号
	int m_nCurDrawType ;							// 当前抓牌的类型（一直有效到下一次抓牌或者有吃碰杠的时候），取值为DRAW_TILE_TYPE_XXX，不包括补花后的抓牌

	int m_nLastGangSeat ;							// 最后一个杠牌的玩家座位号
	CMahJongTile *m_pLastGangTile ;					// 最后一个杠牌中的一张
	int m_nLastGangType ;							// 最后一个杠牌的类型
	int m_nLastKongSeat ;							// 最后一个杠牌是这个座位号的玩家出的
	int m_nHunGangSeat ;							// 混杠玩家的座位号

	int m_nBanker ;									// 庄家的座位号
	int m_nRoundWind ;								// 圈风

	bool m_abTian[/*FOR TWO PLAYER 4*/MAX_PLAYER_COUNT] ; // 玩家未出过牌，也没吃碰杠过牌

	typedef std::vector< CMahJongTile * > MAHJONGTILEVECTOR;
	MAHJONGTILEVECTOR m_vecOccasionalTile;			// 备用的牌(一炮多响时可能会用到)

	int m_anSeat[ MAX_PLAYER_COUNT ];				// 以GameSeat为索引,每个GameSeat上是哪个座位号的人 
};
