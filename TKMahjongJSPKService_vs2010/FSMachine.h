#pragma once

#include <map>
#include <string>
#include <vector>
#include <tkobject.h>
#include "tkfixlist.h"
#include "tkobjectfactory.h"

// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
class CEvent
{
public:
	CEvent( int nEventID, DWORD dwPoster = 0, void * pParam = NULL ) : m_nEventID( nEventID ), m_dwPoster( dwPoster ), m_pParam( pParam ) {};
	CEvent( const CEvent & event ) : m_nEventID( event.m_nEventID ), m_dwPoster( event.m_dwPoster ), m_pParam( event.m_pParam ){};
	int ID() { return m_nEventID; }

public:
	int m_nEventID;			// 事件ID
	DWORD m_dwPoster;		// 发送者,如果这是个网络事件,这个值应该是UserID,如果是本地事件,这个值有可能是个指针,依发送者
							// 和接收者之间的约定而定
	void * m_pParam;		// 参数
};



// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
#define REGIST_EVENT_FUNCTION( id, fn )\
{\
	EventMap eventMap = { id, ( ONEVENT )fn, "" };\
	m_vecEventMap.push_back( eventMap );\
}
class CFSMachine;
class IState : public CTKObject
{
	friend class CFSMachine;
public:
	virtual ~IState(){};

public:
	// 添加一个响应事件,该事件后转换的状态
	bool AddEvent( int nEventID, std::string szNextState = "" );

	// 事件处理
	bool OnEvent( CEvent * pEvent );

	// 状态名
	void SetName( std::string szName ) { m_szName = szName; };
	std::string & Name() { return m_szName; }

	// 进入这个状态时的预操作
	virtual void OnEnter( CEvent * pEvent ){};
	// 离开这个状态时的操作
	virtual void OnLeave(){};

	// 定时检查
	virtual void OnTickCount(){};

protected:
	// 声明为保护类型,不允许构造基类型的对象
	IState();
	IState( IState & state );
	IState & operator =( IState state );

protected:
	// 注册对应事件ID的处理函数这个函数应该在每个派生类的构造函数里调用
	typedef bool ( IState:: * ONEVENT )( CEvent * pEvent );
	void RegistEventFunction( int nEventID, ONEVENT fnOnEvent );

	// 发送消息到状态机
	void PostEvent( CEvent & event );
	void PostEvent( int nEventID );

	// 通知Machine有错误发生
	void PostErrorMsg( const TCHAR * pErrorMsg );

	std::string getNextState();
protected:
	struct EventMap 
	{
		int nEventID;					// 事件ID
		ONEVENT fnOnEvent;				// 事件处理
		std::string szNextState;		// 下一个状态
	};
	typedef std::vector<EventMap> EventMapVector;
	EventMapVector m_vecEventMap;		// 事件 - 处理函数 - 下一个状态

	std::string m_szName;				// 状态名

	CFSMachine * m_pFSMachine;			// 状态机
	int m_nLastEventID;					// 导致进入这个状态的事件ID
};
IState * CreateState( char * szStateName );



// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
class CFSMachine
{
	friend class IState;
public:
	CFSMachine(void);
	virtual ~CFSMachine(void);

public:
	// 添加一个状态
	virtual bool AddState(const std::string& szStateName, IState * pState );

	// 设置当前状态
	IState * SetState(const std::string& szStateName );

	// 事件处理
	void HandleEvent( CEvent * pEvent );

	// 定时检查
	virtual void OnTickCount();

protected:
	// 内部发送消息,不让外界调用这个函数是因为外界并不了解当前状态,发送的消息有可能是非法的
	void PostEvent( CEvent & event );
	// 通知Machine有错误发生
	void PostErrorMsg( std::string & szErrorMsg );

protected:
	typedef std::map< std::string, IState * > StateMap;
	StateMap	m_mapState;				// 状态列表
	IState	*	m_pCurrentState;		// 当前状态

	typedef std::vector< CEvent > EVENTVECTOR;
	EVENTVECTOR m_vecInnerEvent;		// 状态机内部事件

	std::vector< std::string > m_vszErrorMsg;// 错误消息
};
