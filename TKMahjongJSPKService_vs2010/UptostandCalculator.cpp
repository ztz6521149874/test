#include <assert.h>

#ifndef ASSERT
#define ASSERT	assert
#endif

#include ".\uptostandcalculator.h"
#include "GameRule.h"
//FOR TWO PLAYER

// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CUptostandCalculator::CUptostandCalculator()
{
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CUptostandCalculator::~CUptostandCalculator()
{
	ExitInstance() ;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CUptostandCalculator::OnResetData()
{
	m_OweScore.clear() ;	// 清除这一盘的欠分信息
}

// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
int CUptostandCalculator::GetLastAddScore( int nSeat )
{
	ASSERT( IsValidSeat( nSeat ) ) ;
	if( m_Score.empty() )
	{ // 如果得分为空
		return 0 ;
	}

	PONETIMESCORE pScore = &m_Score.back() ;
	return pScore->pnScore[ nSeat ] ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
int CUptostandCalculator::GetAwardScore( int nSeat )
{
	ASSERT( IsValidSeat( nSeat ) ) ;
	int nScore = 0 ;
	PONETIMESCORE pScore = NULL ;
	ScoreIterator iter ;
	for( iter = m_Score.begin() ; iter != m_Score.end() ; iter ++ )
	{ // 遍历所有的计分
		pScore = &( *iter ) ;
		if( pScore->nSeat != nSeat ) continue ;	// 不是这个座位号的玩家得分

		if( pScore->nStyle != 1 ) continue ;	// 不是只有这一个玩家加分

		nScore += pScore->pnScore[ nSeat ] ;
	}

	return nScore ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CUptostandCalculator::GetPlayerScore( TKREQMAHJONGRESULT & reqMahJongResult )
{
	for( int i = 0 ; i < MAX_PLAYER_COUNT ; i ++ )
	{ // 遍历所有的玩家
		if ( 0 == m_pnPlayerMulti[ i ] )
		{
			// 这个座位上没人
			continue;
		}

		reqMahJongResult.asPlayerResult[ i ].nScore = m_pnPlayerLeftScore[ i ] - m_pnPlayerScore[ i ] ;
		reqMahJongResult.asPlayerResult[ i ].nTotalScore = m_pnPlayerLeftScore[ i ];
	}
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CUptostandCalculator::AdjustScore( PONETIMESCORE pScore )
{
	OWESCORE owe = { 0 } ;	// 欠分信息

	if( 1 == pScore->nStyle )
	{ // 只有一个人加分
		ASSERT( IsValidSeat( pScore->nSeat ) ) ;

		int nSeat = pScore->nSeat ;	// 加分的玩家
		int nScore = pScore->pnScore[ nSeat ] ;	// 加的分
		ASSERT( nScore > 0 ) ;
		m_pnPlayerLeftScore[ nSeat ] += pScore->pnScore[ nSeat ] ;
	}
	else if( 2 == pScore->nStyle )
	{ // 只有一个人扣分
		ASSERT( IsValidSeat( pScore->nSeat ) ) ;

		int nSeat = pScore->nSeat ;	// 扣分的玩家
		int nScore = -pScore->pnScore[ nSeat ] ;
		ASSERT( nScore > 0 ) ;
		m_pnPlayerLeftScore[ nSeat ] -= nScore ;
	}
	else if( 3 == pScore->nStyle )
	{ // 这时，大家输赢和为0
		ASSERT( IsValidSeat( pScore->nSeat ) ) ;
		/*FOR TWO PLAYER*/
		ASSERT( pScore->pnScore[ 0 ] + pScore->pnScore[ 1 ] == 0 ) ;
		int nSeat = pScore->nSeat ;	// 加分的玩家
		int nScore = 0 ;
		for( int i = 0 ; i < MAX_PLAYER_COUNT ; i ++ )
		{ // 遍历所有的玩家
			if( i == nSeat ) continue ;	// 略过加分的玩家
			if( 0 == pScore->pnScore[ i ] ) continue ;	// 略过没有输分的玩家
			if ( 0 == m_pnPlayerMulti[ i ] )
			{
				// 这个座位上没人
				continue;
			}

			ASSERT( pScore->pnScore[ i ] < 0 ) ;	// 这个玩家应该是输分了（只有一个加分的玩家）
			nScore = -pScore->pnScore[ i ] ;	// 这个玩家输的分
			m_pnPlayerLeftScore[ i ] -= nScore ;
			m_pnPlayerLeftScore[ nSeat ] += nScore ;
		}
	}
}




// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CUptostandCalculator::AddScore( TKACKMAHJONGWINDETAILEX & reqMahjongWin, int anShowSeat[], int cnShowSeat )
{
	int nWinSeat = reqMahjongWin.nWinSeat;
	int nPaoSeat = reqMahjongWin.nPaoSeat;
	ASSERT( IsValidSeat( reqMahjongWin.nWinSeat ) ) ;
	ASSERT( IsValidSeat( reqMahjongWin.nPaoSeat ) ) ;

	// 必须要初始化
	reqMahjongWin.nBaseScore = m_nMulti;
	memset( reqMahjongWin.asScoreDetail, 0, sizeof( reqMahjongWin.asScoreDetail ) );

	if ( nWinSeat == nPaoSeat )
	{
		// 自摸和
		for ( int i = 0; i < MAX_PLAYER_COUNT; ++i )
		{
			if ( i == nWinSeat || 0 == m_pnPlayerMulti[ i ] )
			{
				continue;
			}

			// 每个人输 ( 番数+8 ) * 基数 的分数
			//FOR TWO PLAYER 去掉8番底
			int nScore = reqMahjongWin.sWinInfo.nScoreOfFan * m_nMulti * m_nDouble;
			reqMahjongWin.asScoreDetail[ i ].anScore[ 5 ] -= 0;
			reqMahjongWin.asScoreDetail[ nWinSeat ].anScore[ 5 ] += nScore;
		}
	}
	else
	{
		// 点炮和
		for ( int i = 0; i < MAX_PLAYER_COUNT; ++i )
		{
			if ( i == nWinSeat || 0 == m_pnPlayerMulti[ i ] )
			{
				continue;
			}

			// 先输一个基数
			//FOR TWO PLAYER 去掉8番底
			/*
			int nScore = 8 * m_nMulti;
			reqMahjongWin.asScoreDetail[ i ].anScore[ 5 ] -= nScore;
			reqMahjongWin.asScoreDetail[ nWinSeat ].anScore[ 5 ] += nScore;
			*/

			if ( i == nPaoSeat )
			{
				// 点炮者还要输一个番数
				int nScore = 0;
				nScore = reqMahjongWin.sWinInfo.nScoreOfFan * m_nMulti * m_nDouble;
				reqMahjongWin.asScoreDetail[ i ].anScore[ 5 ] -= 0;
				reqMahjongWin.asScoreDetail[ nWinSeat ].anScore[ 5 ] += nScore;
			}
		}
	}

	// 计算所有玩家实际分数
	for (int i=0 ; i<MAX_PLAYER_COUNT ; ++i)
	{
		reqMahjongWin.asScoreDetail[i].anScore[6] = reqMahjongWin.asScoreDetail[i].anScore[5];
	}
}

void CUptostandCalculator::ChangeRTScore(int nWinSeat, int aScores[])
{
	for (int i = 0; i < MAX_PLAYER_COUNT; ++i)
	{
		aScores[i] = std::max<int>(aScores[i], 0);
	}
	__super::ChangeRTScore(nWinSeat, aScores);
}
