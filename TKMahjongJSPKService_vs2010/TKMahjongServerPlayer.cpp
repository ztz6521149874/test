#include "TKMahjongServerPlayer.h"
#include "mahjongtile.h"
#include "tkgame2pivotprotocol.h"
#include "TKMahjongJSPKService.h"

CCallback::CCallback()
{

}

CCallback::~CCallback()
{
}

BOOL CCallback::Execute(PTKHEADER in_pAckMsg)
{
    if (in_pAckMsg->dwParam != TK_ACKRESULT_SUCCESS)
    {
        return FALSE;
    }
    if ((TKID_GS2PIVOT_GET_SINGLEGROW | TK_ACK) != in_pAckMsg->dwType)
    {
        return FALSE;
    }
    msg_pivot2gs_ack_get_singleGrow* pAckMsg = (msg_pivot2gs_ack_get_singleGrow*)(in_pAckMsg);
    CTKGamePlayer* pPlayer = gtk_pMahjongJSPKService->GetGamePlayer(pAckMsg->dwUserID);
    if (NULL == pPlayer)
    {
        return FALSE;
    }

    PTKMATCHSTRANGROW pGrow = (PTKMATCHSTRANGROW)((BYTE*)pAckMsg + pAckMsg->stSuffixIdx.dwSuffixOffSet);
    pPlayer->SetGrowValue(pGrow->dwGrowID, pGrow->dwValue, FALSE);
    RELREF_TKOBJECT(pPlayer);
    return TRUE;
}

CTKMahjongServerPlayer::CTKMahjongServerPlayer(void)
{
	m_nCallTileIndex = -1;
	memset( m_asShowGroupInfo, 0, sizeof( m_asShowGroupInfo ) );

	m_nCallTileCount = 0;
	for (int i=0; i<34; i++)
	{
		m_nCallTile[i] = 0;
	}
}

CTKMahjongServerPlayer::~CTKMahjongServerPlayer(void)
{
}

void CTKMahjongServerPlayer::InitPlayer(CTKGamePlayer* pPlayer)
{
    m_pPlayer = pPlayer;

    ResetGameData();
    m_nTotalScore = pPlayer->m_nScore;
    m_nSeatOrder = pPlayer->m_nSeatOrder;
    m_nScore = 0;
    RealHandTile(TRUE);	// 设置玩家手中的牌都是真实的牌

    _getPivotGrow(GV_WinStreak);
    _getPivotGrow(GV_LoseStreak);
    _getPivotGrow(GV_MostWin);
    _getPivotGrow(GV_MostLose);
}

void CTKMahjongServerPlayer::SetCallType( int nCallType , BOOL bWinSelf , BOOL bAutoGang )
{
	CTKMahjongPlayer::SetCallType( nCallType, bWinSelf, bAutoGang );
	m_nCallTileIndex = m_nDiscardTileCount - 1 ;
}

int CTKMahjongServerPlayer::ChiTile( CMahJongTile* pTile , CMahJongTile* apChiTile[ 3 ], int nDiscardSeat ) 
{
	int nGroupIndex = CTKMahjongPlayer::ChiTile( pTile , apChiTile, nDiscardSeat ) ;

	// 记下吃牌信息
	m_asShowGroupInfo[nGroupIndex].nSeat = nDiscardSeat;
	m_asShowGroupInfo[nGroupIndex].nTileID = pTile->ID();

	return nGroupIndex ;
}

int CTKMahjongServerPlayer::PengTile( CMahJongTile* pTile , CMahJongTile* apPengTile[ 3 ], int nDiscardSeat )
{
	int nGroupIndex = CTKMahjongPlayer::PengTile( pTile , apPengTile, nDiscardSeat ) ;

	// 记下碰牌信息
	m_asShowGroupInfo[nGroupIndex].nSeat = nDiscardSeat;
	return nGroupIndex ;
}

int CTKMahjongServerPlayer::GangTile( CMahJongTile* pTile , CMahJongTile* apGangTile[ 4 ] , int nCount, int nDiscardSeat )
{
	int nGroupIndex = CTKMahjongPlayer::GangTile( pTile , apGangTile , nCount, nDiscardSeat ) ;

	// 记下杠牌信息
	if ( m_anShowGroupType[nGroupIndex] == GROUP_STYLE_MINGGANG 
		&& m_asShowGroupInfo[nGroupIndex].nSeat == -1 ) 
	{
		// 别人打给我杠的
		m_asShowGroupInfo[nGroupIndex].nSeat = nDiscardSeat;
	}

	return nGroupIndex ;
}

BOOL CTKMahjongServerPlayer::FindTile( int nTileID, int & cnTile )
{
	for ( int i = 0; i < m_nHandTileCount; i++ ) 
	{
		if ( ( m_apHandTile[i]->ID() & 0x0ff0 ) == ( nTileID & 0x0ff0 ) ) 
		{
			cnTile++;
		}
	}

	return TRUE;
}

void CTKMahjongServerPlayer::CountHandTile( int acnTile[] )
{
	for( int i = 0; i < m_nHandTileCount; i++ )
	{
		int nIndex = GetTileValue( m_apHandTile[i]->ID() );
		if ( nIndex < 0 || nIndex >= 34 )
		{
			continue;
		}
		acnTile[nIndex]++;
	}
}

int CTKMahjongServerPlayer::GetTileValue( int nTileID )
{
	int nColor = ( nTileID & 0x0f00 ) >> 8;
	int nWhat = ( nTileID & 0x00f0 ) >> 4;

	if ( nColor < 4 )
	{
		return nColor + ( nColor << 3 ) + nWhat;//nColor * 9 + nWhat;
	}
	else if (nColor == COLOR_JIAN)
	{
		return 31 + nWhat;
	}
    return -1;
}

BOOL CTKMahjongServerPlayer::IsAutoPlay()
{
	return m_nTrustPlay > 1;
}

// 重置听牌信息
void CTKMahjongServerPlayer::ResetCallInfo()
{
	m_nCallTileCount = 0;
	for (int i=0; i<34; i++)
	{
		m_nCallTile[i] = 0;
	}
}

// 设置听牌信息
void CTKMahjongServerPlayer::SetCallInfo(int nCount, int CallTile[34])
{
	m_nCallTileCount = nCount;

	for (int i=0; i<m_nCallTileCount; i++)
	{
		m_nCallTile[i] = CallTile[i];
	}
}

// 查找是否是所听牌
bool CTKMahjongServerPlayer::IsCallTile(int nTile)
{
	for (int i=0; i<m_nCallTileCount; i++)
	{
		if ( CMahJongTile::SameTile( nTile, m_nCallTile[i] ) )
		{
			return true;
		}
	}

	return false;
}

// 删除非法听牌
void CTKMahjongServerPlayer::DeleteCallTile(int nTile)
{
	for (int i=0; i<m_nCallTileCount; i++)
	{
		if ( CMahJongTile::SameTile( nTile, m_nCallTile[i] ) )
		{
			m_nCallTile[i] = m_nCallTile[m_nCallTileCount-1];
			m_nCallTile[m_nCallTileCount-1] = 0;
			m_nCallTileCount--;
			break;
		}
	}
}

int CTKMahjongServerPlayer::FillCallTile( int pnTile[] )
{
	if( NULL != pnTile )
	{
		for( int i = 0 ; i < m_nCallTileCount ; i ++)
		{ // 遍历手中的牌
			int nIndex = GetTileValue( m_nCallTile[i] ) ;
			pnTile[nIndex] = 1 ;
		}
	}
	return m_nCallTileCount;
}

void CTKMahjongServerPlayer::SaveGrowValue(CTKGame *pGame, int nWinSeat)
{
    if (MATCH_RULE_FDTABLE == pGame->m_stStageRuler.nStageType)
    {
        return;
    }

    AddGrowValue(GV_GameCount);

    if (m_nSeatOrder == nWinSeat)
    {
        m_pPlayer->AddAtomGameEvent(EVT_Win, 1, TRUE);
        m_pPlayer->AddAtomGameEvent(EVT_GameResult, 1, TRUE);

        AddGrowValue(GV_WinCount);
        AddGrowValue(GV_WinStreak);

        int nLoseStreak = 0;
        if (m_pPlayer->GetGrowValue(GV_LoseStreak, nLoseStreak))
        {
            AddGrowValue(GV_LoseStreak, -nLoseStreak);
        }

        int nMostWin = 0;
        if (m_pPlayer->GetGrowValue(GV_MostWin, nMostWin) && GetGrowValue(GV_WinStreak) > nMostWin)
        {
            AddGrowValue(GV_MostWin);
        }
    }
    else
    {
        m_pPlayer->AddAtomGameEvent(EVT_GameResult, 0, TRUE);
        AddGrowValue(GV_LoseStreak);

        int nWinStreak = 0;
        if (m_pPlayer->GetGrowValue(GV_WinStreak, nWinStreak))
        {
            AddGrowValue(GV_WinStreak, -nWinStreak);
        }

        int nMostLose = 0;
        if (m_pPlayer->GetGrowValue(GV_MostWin, nMostLose) && GetGrowValue(GV_LoseStreak) > nMostLose)
        {
            AddGrowValue(GV_MostLose);
        }
    }
    _saveGrowValue();

    DealSingleGrow64ByGrowID(EG_BrushGameHand, 1);
    time_t nTimeLen = time(NULL) - pGame->m_timeGameBegin;
    DealSingleGrow64ByGrowID(EG_BrushGameTime, nTimeLen);
    m_pPlayer->AddAtomGameEvent(EVT_BrushPlayerJudge, 1, TRUE);
}

void CTKMahjongServerPlayer::AddGrowValue(DWORD nGrowId, int nValue)
{
    if (nValue != 0)
    {
        m_astGrowInfo[nGrowId] += nValue;
        m_pPlayer->AddGrowValue(nGrowId, nValue, FALSE);
    }
}

int CTKMahjongServerPlayer::GetGrowValue(DWORD nGrowId)
{
    int nValue = 0;
    m_pPlayer->GetGrowValue(nGrowId, nValue);
    return nValue;
}

void CTKMahjongServerPlayer::DealSingleGrow64ByGrowID(DWORD dwGrowID, __int64 nValue)
{
    if (0 >= nValue)
    {
        return;
    }

    msg_gs2pivot_req_deal_grow_64 rReqGrow;
    memset(&rReqGrow, 0, sizeof(rReqGrow));
    rReqGrow.dwType = TKID_GS2PIVOT_DEAL_GROW_64 | TK_REQ;
    rReqGrow.dwLength = sizeof(rReqGrow) - sizeof(TKHEADER);
    rReqGrow.dwUserID = m_pPlayer->m_dwUserID;
    rReqGrow.dwGrowID = dwGrowID;
    rReqGrow.llValue = nValue;
    rReqGrow.dwMPID = m_pPlayer->m_nProductID;
    rReqGrow.dwGameID = tk_pGameService->m_nGameID;
    rReqGrow.dwPlatType = m_pPlayer->m_wCntTID;
    rReqGrow.byOSType = m_pPlayer->m_byOSType;
    rReqGrow.dwAppID = m_pPlayer->m_dwAppID;
    rReqGrow.dwSiteID = m_pPlayer->m_dwSiteID;

    tk_pGameService->PostMsg2Pivot_GSBase(&rReqGrow, m_pPlayer->m_dwUserID, m_pPlayer->m_dwMatchID
        , m_pPlayer->m_wStageID, m_pPlayer->m_wRoundID, NULL);
}

void CTKMahjongServerPlayer::_getPivotGrow(DWORD wGrowId)
{
    msg_gs2pivot_req_get_singleGrow rReqGrow;
    memset(&rReqGrow, 0, sizeof(rReqGrow));
    rReqGrow.dwType = TKID_GS2PIVOT_GET_SINGLEGROW | TK_REQ;
    rReqGrow.dwLength = sizeof(rReqGrow) - sizeof(TKHEADER);
    rReqGrow.dwUserID = m_pPlayer->m_dwUserID;
    rReqGrow.dwGrowID = wGrowId;
    rReqGrow.dwMPID = m_pPlayer->m_nProductID;
    rReqGrow.dwGameID = tk_pGameService->m_nGameID;
    rReqGrow.dwPlatType = m_pPlayer->m_wCntTID;
    rReqGrow.byOSType = m_pPlayer->m_byOSType;
    rReqGrow.dwAppID = m_pPlayer->m_dwAppID;
    rReqGrow.dwSiteID = m_pPlayer->m_dwSiteID;

    CCallback* pCallBack = new CCallback();
    tk_pGameService->PostMsg2Pivot_GSBase(&rReqGrow, m_pPlayer->m_dwUserID, m_pPlayer->m_dwMatchID
        , m_pPlayer->m_wStageID, m_pPlayer->m_wRoundID, pCallBack);
}

void CTKMahjongServerPlayer::_saveGrowValue()
{
    if (m_astGrowInfo.empty())
    {
        return;
    }

    for (auto iter = m_astGrowInfo.begin(); iter != m_astGrowInfo.end(); ++iter)
    {
        DWORD dwGrowID = iter->first;
        DWORD dwValue = (DWORD)iter->second;
        msg_gs2pivot_req_deal_grow rReqGrow;
        memset(&rReqGrow, 0, sizeof(rReqGrow));
        rReqGrow.dwType = TKID_GS2PIVOT_DEAL_GROW | TK_REQ;
        rReqGrow.dwLength = sizeof(rReqGrow) - sizeof(TKHEADER);
        rReqGrow.dwUserID = m_pPlayer->m_dwUserID;
        rReqGrow.dwGrowID = iter->first;
        rReqGrow.nValue = iter->second;
        rReqGrow.dwMPID = m_pPlayer->m_nProductID;
        rReqGrow.dwGameID = tk_pGameService->m_nGameID;
        rReqGrow.dwPlatType = m_pPlayer->m_wCntTID;
        rReqGrow.byOSType = m_pPlayer->m_byOSType;
        rReqGrow.dwAppID = m_pPlayer->m_dwAppID;
        rReqGrow.dwSiteID = m_pPlayer->m_dwSiteID;

        tk_pGameService->PostMsg2Pivot_GSBase(&rReqGrow, m_pPlayer->m_dwUserID, m_pPlayer->m_dwMatchID
            , m_pPlayer->m_wStageID, m_pPlayer->m_wRoundID, NULL);
    }
    m_astGrowInfo.clear();
}

