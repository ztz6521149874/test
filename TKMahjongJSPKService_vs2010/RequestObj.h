#ifndef _REQUEST_OBJ_H
#define _REQUEST_OBJ_H

#include "time.h"
#include "tkmahjongprotocol.h"
#include "tkptrlist.h"		// 



// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
class CRequestObj
{
private :
	int m_nRequestID ;			// 请求消息的ID号
	int m_nRequestType ;		// 请求消息的类型
	int m_nRequestSeat ;		// 发送请求消息的座位号，这些座位号按位取值，若第N位为1，表示给座位号N的玩家发送了请求消息
	int m_nAckRequestSeat ;		// 回复了请求消息的座位号，这些座位号按位取值，若第N位为1，表示座位号为N的玩家回复了请求消息

	int m_pnAckRequestType[ MAX_PLAYER_COUNT ] ;	// 玩家回复的请求类型，这个是玩家的操作，如果放弃请求，值为0
	
	CTKPtrList<char> m_listMsg ;// 玩家针对这个请求所采取的操作（这里存放的是吃碰杠和的消息）

	int m_nOKRequestType ;		// 当前这个请求的回复应该处理的类型

	DWORD m_dwRequestWaitTime;  // 向客户端发送请求信息后的等待时间，超过这个时间认为客户端作弊
	int m_nWaitSeconds ;		// 请求需要等待的时间
	DWORD m_dwStartTime ;       // 请求时的时间

public :
	// 构造函数
	CRequestObj() ;
	// 虚构函数
	~CRequestObj() ;
	
	// 重置请求对象
	/// 这次重置是为了发送nRequestType的请求，等待时间是nWaitSeconds（单位：秒）
	void ResetRequest( int nRequestType , int nWaitSeconds ) ;
	
	// 填充请求消息类型
	void FillRequestMsg( PTKREQMAHJONGREQUEST pReqMahJongRequest , int nSeat ) ;

	// 设置回复了请求的座位号
	/// nRequestType是这个玩家想执行的操作，如果为0，表示这个玩家放弃了这次请求
	void SetAckQuestSeat( int nSeat , int nRequestType , PTKHEADER pHeader = NULL ) ;

	// 判断是否所有玩家都回复了
	BOOL AllAckRequest() { return ( m_nAckRequestSeat == m_nRequestSeat ) ; }
	// 判断是否是请求的ID号
	BOOL IsRequestID( int nRequestID ) { return ( nRequestID == m_nRequestID ) ; }
	// 判断这个玩家是否回复了请求消息
	BOOL IsAckRequestSeat( int nSeat ) { return ( 0 != ( m_nAckRequestSeat & ( 1 << nSeat ) ) ) ; }
	// 判断是否发送了请求的座位号
	BOOL IsRequestSeat( int nSeat ) { return ( 0 != ( m_nRequestSeat & ( 1 << nSeat ) ) ) ; }
	// 是否是请求了的类型
	BOOL IsRequestType( int nType ) { return ( 0 != ( m_nRequestType & nType ) ) ; }
	// 某个玩家是否回复了某个类型
	BOOL IsPlayerAckRequestType( int nSeat , int nType ) { return ( nType == m_pnAckRequestType[ nSeat ] ) ; }
	// 返回级别最高的那个回复类型
	int GetOKAckRequestType() { return m_nOKRequestType ; }

	// 返回玩家座位号
	/// 返回回复了nType这种类型的玩家座位号
	/// 【注意】
	/// 1.这个函数只会在调用了SetAckQuestSeat，并且pHeader不为NULL的那些玩家中查找
	/// 2.如果有多个这样的玩家，只会返回第一个
	int GetAckMsgSeat( int nType ) ;
	// 返回玩家回复时发送的消息
	/// 如果这个座位号的玩家放弃了操作，那么返回的就是NULL
	PTKHEADER GetPlayerAckMsg( int nSeat ) ;

	// 增加请求的ID号
	void IncRequestID() ;
	
	int RequestID() { return m_nRequestID ; }		// 请求的ID号

	// 检查是否有超时客户端
	int CheckTime() ; 
	// 设置客户端超时时间
	void SetRequestWaitTime(int nWaitTime) { m_dwRequestWaitTime = (DWORD)(nWaitTime*1000); }
	// 检查指定的客户端是否超时未回复
	BOOL CheckOverTime( int nSeat );
	// 检查指定的客户端是否超时指定的时间未回复
	BOOL CheckOverTime( int nSeat, int nWaitTime);

	BOOL OnResetDump();

	bool CheckDelayDiscard(int nDelayTime) { return (GetTickCount() - m_dwStartTime) > (DWORD)(nDelayTime * 1000); };
private :
	// 设置请求的座位号
	void SetRequestSeat( int nSeat ) ;
} ;


#endif
