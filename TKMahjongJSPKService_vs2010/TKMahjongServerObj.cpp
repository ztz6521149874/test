#include <winsock2.h>
#include <windows.h>

#include "tkmahjongdefine.h"
#include "MahJongTile.h"			// CMahJongTile
#include "PublicJudge.h"			// CPublicJudge
#include "tkMahJongServerPlayer.h"	// CMahJongServerPlayer
#include ".\tkmahjongserverobj.h"
#include "GameRule.h"
#include "TKMahjongGame.h"
#include "ScoreCalculator.h"
#include "RequestObj.h"
#include "PublicTileWall.h"

#include <math.h>

#include <assert.h>
#include "TKMahjongJSPKService.h"

#ifndef ASSERT
#define ASSERT assert
#endif

// *********************************************************************************************************************
// 
// 
// 
// *********************************************************************************************************************
CTKMahjongServerObj::CTKMahjongServerObj( CTKMahjongGame * pGame ) 
: m_pMahjongGame( pGame )
{
    m_vecPengCardsIDJust.clear();
	m_nSpecialTileID = 0;
}



// *********************************************************************************************************************
// 
// 
// 
// *********************************************************************************************************************
CTKMahjongServerObj::~CTKMahjongServerObj(void)
{
    m_vecPengCardsIDJust.clear();
}



void CTKMahjongServerObj::ResetGameData()
{
    CTKMahjongObj::ResetGameData();

    m_vecPengCardsIDJust.clear();
}

// *********************************************************************************************************************
// 
// 
// 
// *********************************************************************************************************************
CTKMahjongPlayer* CTKMahjongServerObj::CreatePlayer(int nSeat, CTKGamePlayer * pPlayer)
{
    CTKMahjongServerPlayer * pMahjongPlayer = new CTKMahjongServerPlayer;
    if (NULL != pMahjongPlayer)
    {
        pMahjongPlayer->InitPlayer(pPlayer);
    }
    return pMahjongPlayer;
}



// *********************************************************************************************************************
// 
// 
// 
// *********************************************************************************************************************
void CTKMahjongServerObj::FillOpenDoorMsg(int nSeat, int nAwardSeat, int nFlowerCount, PTKREQMAHJONGOPENDOOR pReqMahJongOpenDoor)
{
    int nTileCount = 4;	// 牌的张数
    if (nSeat == Banker())// 庄家
    { 
        nTileCount += 1;
    }

    pReqMahJongOpenDoor->cnHandTile = nTileCount;

    // 给这个玩家发牌
    int *pnTile = (int *)(pReqMahJongOpenDoor + 1);
    int nLeftTileCount = nTileCount;	// 剩下需要随机发的牌的张数
    CTKMahjongPlayer* pPlayer = GetPlayerBySeat(nSeat);
    ASSERT(NULL != pPlayer);

    CMahJongTile *pTile = NULL;
    PMAHJONGTILECOUNTINFO pTileInfo = NULL;

    // 1.发四张花牌
    if (nSeat == nAwardSeat)
    {
        for (int i = 0; i < nFlowerCount; i++)
        {
            pTileInfo = RandomSelectOneLeftTile(COLOR_FLOWER);
            if (pTileInfo == NULL)
            {
                TKWriteLog("%s : %d, Error! ywx 未找到牌张", __FILE__, __LINE__);
                continue;
            }

            *pnTile = TILE_ID(pTileInfo->nColor, pTileInfo->nWhat, pTileInfo->vUsed[pTileInfo->vUsed.size() - 1]);
            pTile = GetOneTileFromWall(TRUE, 0);	// 从牌墙头取一张牌
            pTile->SetID(*pnTile);
            pPlayer->AddHandTile(pTile);

            pnTile++;
            nLeftTileCount--;
        }
    }

    /// 2.随机的发牌
    int nAvoidColor = -1 == nAwardSeat ? -1 : COLOR_FLOWER;
    for (int i = 0; i < nLeftTileCount; i++)
    {
        pTileInfo = RandomSelectOneLeftTile(-1, nAvoidColor);
        if (pTileInfo == NULL)
        {
            TKWriteLog("%s : %d, Error! ywx 未找到牌张！RandomSelectOneLeftTile()", __FILE__, __LINE__);
            continue;
        }

        *pnTile = TILE_ID(pTileInfo->nColor, pTileInfo->nWhat, pTileInfo->vUsed[pTileInfo->vUsed.size() - 1]);
        pTile = GetOneTileFromWall(TRUE, 0);	// 从牌墙头取一张牌
        pTile->SetID(*pnTile);
        pPlayer->AddHandTile(pTile);

        pnTile++;
    }

    // 随机的更改一下这些牌的顺序（把前面得到的那些顺、刻打乱）
    pnTile = (int*)(pReqMahJongOpenDoor + 1);	// 指向牌的信息
    for (int i = 0;i < nTileCount;++i)
    {
        int j = TKGenRandom() % (nTileCount - i);
        std::swap(pnTile[j], pnTile[nTileCount - i - 1]);
    }
}

int CTKMahjongServerObj::GenerateRandomTileId(std::vector<int>& vUsedTileId, int nTotal)
{
	int nIndex = TKGenRandom() % nTotal + 1;
	int nTmp = nIndex;
	int nUp = TKGenRandom() % 2;
	do
	{
		if (!count(vUsedTileId.begin(), vUsedTileId.end(), nIndex))
		{
			return nIndex;
		}
		else
		{
			if (1 == nUp)
			{
				nIndex++;
				if (nIndex > nTotal)
				{
					nIndex = 1;
				}
			}
			else
			{
				nIndex--;
				if (nIndex <= 0)
				{
					nIndex = nTotal;
				}
			}
		}
	} while (nTmp != nIndex);

	TKWriteLog("%s : %d, Error! GenerateRandomTileId", __FILE__, __LINE__);
	
	return 0;
}

// *********************************************************************************************************************
// 
// 
// 
// *********************************************************************************************************************
PMAHJONGTILECOUNTINFO CTKMahjongServerObj::RandomSelectOneLeftTile( int nColor /* = -1 */, int avoidColor1 /* = -1 */, int avoidColor2 /* = -1 */)
{
	int nTotal = sizeof( m_apTileCountInfo ) / sizeof( m_apTileCountInfo[ 0 ] ) ;	// 总共有多少种牌
    int nIndex = TKGenRandom() % m_nTileCount;

	int nTemp = 0 ;
	PMAHJONGTILECOUNTINFO pTileInfo = NULL ;

    if (!m_vMannulTiles.empty())
    {
        MAHJONGTILECOUNTINFO& rInfo = m_vMannulTiles.front();
        for (int i = 0; i < nTotal; i++)
        {
            pTileInfo = m_apTileCountInfo + i;
            if (pTileInfo->nColor ==rInfo.nColor&&pTileInfo->nWhat==rInfo.nWhat)
            {
                // 就是这张牌
                pTileInfo->vUsed.push_back(pTileInfo->vUsed.size() + 1);	// 增加这张牌的使用张数
                m_vMannulTiles.pop_front();
                return pTileInfo;
            }
        }
    }

	// 从所有的牌中，选择没有用完的第nIndex张牌
	do
	{
		nTemp = nIndex ;
		for( int i = 0 ; i < nTotal ; i ++ )
		{ // 遍历所有的牌
			pTileInfo = m_apTileCountInfo + i ;
			if( pTileInfo->vUsed.size() >= pTileInfo->nTotal ) continue ;	// 这个牌已经用完
			if( ( -1 != nColor ) && ( nColor != pTileInfo->nColor ) ) continue ;	// 如果还要选择花色，并且花色不对

			if (-1 != avoidColor1 && pTileInfo->nColor == avoidColor1) continue; // 避开这个花色
			if (-1 != avoidColor2 && pTileInfo->nColor == avoidColor2) continue; // 避开这个花色

			if( pTileInfo->nTotal - pTileInfo->vUsed.size() >= nIndex )
			{ // 就是这张牌
				pTileInfo->vUsed.push_back(GenerateRandomTileId(pTileInfo->vUsed, pTileInfo->nTotal));	// 增加这张牌的使用张数
				return pTileInfo ;
			}
			nIndex -= ( pTileInfo->nTotal - pTileInfo->vUsed.size()) ;	// 略过这张牌剩下的几张
		}
	} while( nTemp != nIndex ) ;	// 只要找到了没有发完的牌，就接着找

	return NULL ;
}

// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
int CTKMahjongServerObj::PlayerDrawOneTile( int nSeat , int nDrawTileType , BOOL bFromWallHeader , int nOffset )
{
	if ( m_pTileWall->Empty() ) 
	{
		return 0;
	}

    //双方都没有能胡的牌 返回false 流局结束
    if (m_bCheckCallTile && !CheckCallTile())
    {
        return 0;
    }

	CTKMahjongServerPlayer *pPlayer = ( CTKMahjongServerPlayer * )GetPlayerBySeat( nSeat ) ;	// 这个玩家

	// 先看看这个位置是否已经先放好了一张牌了
	int nTileID = CheckWallTileID( bFromWallHeader , nOffset ) ;

	if( 0 == nTileID )
	{ 
		PMAHJONGTILECOUNTINFO pTileInfo = RandomSelectOneLeftTile() ;
		if( NULL == pTileInfo )
		{ // 所有的牌都用了
			return 0 ;
		}
		nTileID = TILE_ID( pTileInfo->nColor , pTileInfo->nWhat , pTileInfo->vUsed[pTileInfo->vUsed.size() - 1]) ;	// 这张牌的ID号
	}

	// 发送抓牌消息
	TKREQMAHJONGDRAWTILE reqMahJongDrawTile = { 0 } ;
	reqMahJongDrawTile.header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_DRAW_TILE ) ;
	reqMahJongDrawTile.header.dwLength = MSG_LENGTH( TKREQMAHJONGDRAWTILE ) ;
	reqMahJongDrawTile.nSeat           = nSeat ;
	reqMahJongDrawTile.nDrawTileType   = nDrawTileType ;
	reqMahJongDrawTile.bFromWallHeader = bFromWallHeader ;
	reqMahJongDrawTile.nTileOffset     = nOffset ;
	char szBuffer[ sizeof( TKREQMAHJONGDRAWTILE ) ] = { 0 } ;	// 假的牌的信息
	memcpy( szBuffer , &reqMahJongDrawTile , sizeof( TKREQMAHJONGDRAWTILE ) ) ;
	reqMahJongDrawTile.nTileID = nTileID ;	// 真的牌的信息中，设置牌的真的ID号
	SendSeatTileInfo( nSeat , ( PTKHEADER )&reqMahJongDrawTile , ( PTKHEADER )szBuffer ) ;

	// 玩家抓牌
	PlayerDrawTile( &reqMahJongDrawTile ) ;

	// 更新玩家手中牌
	CountHandTile( nSeat );

	return nTileID ;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
int CTKMahjongServerObj::CheckWallTileID( BOOL bFromWallHeader , int nOffset )
{
/*
	if( m_pTileWall->Empty() )
	{ // 牌墙中没牌了
		return 0 ;
	}

	CMahJongTile *pTile = m_apWallTile[ GetWallTileIndex( bFromWallHeader , nOffset ) ] ;
	ASSERT( NULL != pTile ) ;
	if( NULL == pTile || 0 == pTile->ID() )
	{ // 这个位置没有牌，或者牌无效
		return 0 ;
	}

	return pTile->ID() ;
*/
	return 0;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CTKMahjongServerObj::SendSeatTileInfo( int nSeat , PTKHEADER pvMsgTrue , PTKHEADER pvMsgFalse )
{
	PTKHEADER pTempMsgTrue = pvMsgTrue;
	PTKHEADER pTempMsgFalse = pvMsgFalse;

	for( int i = 0; i < MAX_PLAYER_COUNT; i++ )
	{ // 遍历所有座位号
		bool isCall = false;
		CTKMahjongServerPlayer *pPlayer = ( CTKMahjongServerPlayer * )GetPlayerBySeat( i ) ;	// 这个玩家
        if ( pPlayer && pPlayer->HasCalled())
        {
            isCall = true;
        }

		if( i != nSeat && !isCall)
		{ // 其他座位
		
			if ( !Send2SeatPlayer( i, ( PTKHEADER )pTempMsgFalse ) )
			{
				TKWriteLog( "%s : %d, Error! 发送消息错误 len( %d )", __FILE__, __LINE__, pTempMsgFalse->dwLength );
			}
		}
		else
		{ // 就是这个座位的牌的信息

			if ( !Send2SeatPlayer( i, ( PTKHEADER )pTempMsgTrue ) )
			{
				TKWriteLog( "%s : %d, Error! 发送消息错误 len( %d )", __FILE__, __LINE__, pTempMsgTrue->dwLength );
			}

		}
	}

	if ( !m_pMahjongGame->GameSend2Observer( ( PTKHEADER )pTempMsgTrue ) )
	{
		TKWriteLog( "%s : %d, Error, 发送消息错误， len( %d )", __FILE__, __LINE__, pTempMsgTrue->dwLength );
	}

	if ( !m_pMahjongGame->GameSend2Black( ( PTKHEADER )pTempMsgFalse ) )
	{
		TKWriteLog( "%s : %d, Error, 发送消息错误， len( %d )", __FILE__, __LINE__, pTempMsgFalse->dwLength );
	}
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
BOOL CTKMahjongServerObj::Send2SeatPlayer( int nSeat, PTKHEADER pMsg )
{
	if ( NULL == m_apPlayer[ nSeat ] )
	{
		// 这个座位上没人
		return false;
	}

	return m_pMahjongGame->GameSend2SeatPlayer( nSeat, pMsg );
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CTKMahjongServerObj::Send2Observer( PTKHEADER pMsg )
{
	m_pMahjongGame->GameSend2Observer( pMsg );
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
BOOL CTKMahjongServerObj::Broadcast( PTKHEADER pMsg )
{
	return m_pMahjongGame->GameBroadcast( pMsg );
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CTKMahjongServerObj::PlayerGang( PTKREQMAHJONGGANG pReqMahJongGang, bool bAfterPeng )
{
	int nSeat = pReqMahJongGang->nSeat;
	if( IS_GANG_TYPE_HIDDEN( pReqMahJongGang->nGangType ) )
	{ // 暗杠
		// 其他座位号或者这个座位号的旁观者都应该收到假的牌的信息
		TKREQMAHJONGGANG reqMahJongGang = { 0 } ;
		memcpy( &reqMahJongGang , pReqMahJongGang , sizeof( TKREQMAHJONGGANG ) ) ;
        bool setFalse = true;
        int opSeat = (pReqMahJongGang->nSeat + 1) % 2;
        if ((m_pGameRule->GetRule(RULE_DOUBLE_COUNT)) && GetPlayerBySeat(opSeat)->HasCalled())
        {  // 暗杠的话听牌玩家可看见对家牌
            setFalse = false;
        }

        if (setFalse)
        {
            reqMahJongGang.nTileID = 0;	// 假的消息中不知道要杠的这张牌
            memset(reqMahJongGang.anGangTileID, 0, sizeof(reqMahJongGang.anGangTileID));	// 假的消息中不知道要杠的这几张牌
        }

		for( int i = 0; i < MAX_PLAYER_COUNT; i ++ )
		{ // 遍历所有座位号
			if ( !HasPlayer( i ) )
			{
				// 这个座位没人
				continue;
			}

			if( i == nSeat )
			{ // 杠牌的这个座位号
				if ( !Send2SeatPlayer( i, ( PTKHEADER )pReqMahJongGang ) )
				{
					TKWriteLog( "%s : %d, Error! 发送消息错误 len( %d )", __FILE__, __LINE__, pReqMahJongGang->header.dwLength );
				}
			}
			else
			{ // 不是杠牌的座位号
				if ( !Send2SeatPlayer( i, ( PTKHEADER )&reqMahJongGang ) )
				{
					TKWriteLog( "%s : %d, Error! 发送消息错误 len( %d )", __FILE__, __LINE__, reqMahJongGang.header.dwLength );
				}
			}
		}

		// 发送给observer型的旁观者
		if ( !m_pMahjongGame->GameSend2Observer( ( PTKHEADER )pReqMahJongGang ) )
		{
			TKWriteLog( "%s : %d, Error, 发送消息错误， len( %d )", __FILE__, __LINE__, pReqMahJongGang->header.dwLength );
		}

		// 发送给black型的旁观者
		if ( !m_pMahjongGame->GameSend2Black( ( PTKHEADER )&reqMahJongGang ) )
		{
			TKWriteLog( "%s : %d, Error, 发送消息错误， len( %d )", __FILE__, __LINE__, reqMahJongGang.header.dwLength );
		}
	}
	else
	{ // 不是暗杠
		if ( !Broadcast( ( PTKHEADER )pReqMahJongGang ) )
		{
			TKWriteLog( "%s : %d, Error! 发送消息出错, len( %d )", __FILE__, __LINE__, pReqMahJongGang->header.dwLength );
		}
	}

	// 玩家杠牌
	CMahJongTile *apGangTile[ 4 ] = { 0 } ;
	int nCount = 0 ;
	PlayerGangTile( pReqMahJongGang , apGangTile , nCount, bAfterPeng ) ;
	// 统计牌张
	CountHandTile( nSeat );
	if (IsTwoTing())
	{
		m_bCheckCallTile = true;
	}
}

int CTKMahjongServerObj::CalcGangScore(int nSeat)
{
	TKREQSCORECHANGE reqScore = { 0 };


	// 杠牌，直接给玩家加分
	IScoreCalculator * pCalculator = m_pMahjongGame->Calculator();
	int nScore = m_pMahjongGame->Rule()->GetRule(RULE_GANG_SCORE) * m_pMahjongGame->m_nScoreBase;

	int aScores[MAX_PLAYER_COUNT];
	std::fill_n(aScores, MAX_PLAYER_COUNT, -nScore);
	aScores[nSeat] = nScore;
	pCalculator->ChangeRTScore(nSeat, aScores);

	CTKBuffer bufGameAction;
	bufGameAction.AppendFormatText("<a id=\"%d\" o=\"", RECORD_SCORECHANGE);
	for (int i = 0; i < MAX_PLAYER_COUNT; ++i)
	{
		m_pMahjongGame->SetGameResult(i, 0, 0, 0, aScores[i], FALSE, FALSE);
		reqScore.anIncremental[i] = aScores[i];
		bufGameAction.AppendFormatText((i == 0 ? "%d," : "%d"), aScores[i]);
	}
	bufGameAction.AppendFormatText("\" />");
	m_pMahjongGame->AddGameAction(bufGameAction.GetBufPtr());

	reqScore.header.dwType = REQ_TYPE(TK_MSG_SCORE_CHANGE);
	reqScore.header.dwLength = MSG_LENGTH(TKREQSCORECHANGE);
	reqScore.type = 1;
	if (!Broadcast((PTKHEADER)&reqScore))
	{
		TKWriteLog("%s : %d, Error! 发送消息错误", __FILE__, __LINE__);
	}

	return nScore;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CTKMahjongServerObj::PlayerReqDiscardTile( PTKREQMAHJONGDISCARDTILE pReqMahJongDiscardTile )
{
	int nSeat = pReqMahJongDiscardTile->nSeat;
	CTKMahjongPlayer * pPlayer = GetPlayerBySeat( nSeat ) ;	// 这个玩家
	int nTileID = pReqMahJongDiscardTile->nTileID ;	// 牌的ID号
	if( !pPlayer->HasTile( nTileID ) )
	{ // 玩家手中没有这张牌
		TKWriteLog( "[%s:%d]user(%d[%s]) no tile(0x%03X)" , __FILE__ , __LINE__ , pPlayer->UserID(), pPlayer->NickName(), nTileID ) ;
		pPlayer->TraceInfo() ;
		return false;
	}

	// 如果这个玩家已经听牌了，那么出的这张牌应该是抓的那张牌
	if( pPlayer->HasCalled() )
	{ // 玩家已经听牌了
		CMahJongTile *pDrawTile = pPlayer->GetDrawTile() ;
		ASSERT( NULL != pDrawTile ) ;
TKMARK_LASTEXECLINE;
		if( !pDrawTile->IDIs( nTileID ) )
		{ // 这张牌不是玩家抓的那张牌
			TKWriteLog( "[%s:%d]user(%d[%s]) discard tile(0x%03X),draw tile(0x%03X)" , __FILE__ , __LINE__ , pPlayer->UserID(), pPlayer->NickName(), nTileID , pDrawTile->ID() ) ;
			return false;
		}
	}

	// 转发这个消息给所有用户
	if ( !Broadcast( ( PTKHEADER )pReqMahJongDiscardTile ) )
	{
		TKWriteLog( "%s : %d, Error! 发送消息出错, len( %d )", __FILE__, __LINE__, pReqMahJongDiscardTile->header.dwLength );
	}

	// 设置这个玩家已经回复了,否则万一因为某种原因导致服务器陷入久等状态,这个玩家可能会被当作替罪羊
	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	pRequestObj->SetAckQuestSeat( nSeat, REQUEST_TYPE_GANG );

	// 玩家出了这张牌
	PlayerDiscardTile( pReqMahJongDiscardTile ) ;

	// 统计牌张
	CountHandTile( nSeat );

	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CTKMahjongServerObj::PlayerReqCallAndDiscardTile( PTKREQMAHJONGCALL pReqMahJongCall )
{
	int nSeat = pReqMahJongCall->nSeat;
	CTKMahjongPlayer * pPlayer = GetPlayerBySeat( nSeat ) ;

	// 如果这个玩家已经听牌了，发这条消息是非法的
	int nTileID = pReqMahJongCall->nTileID ;	// 牌的ID号
	if( pPlayer->HasCalled() )
	{ // 玩家已经听牌了
//		TKWriteLog( "[%s:%d]user(%d[%s]) discard tile(0x%03X),draw tile(0x%03X)" , __FILE__ , __LINE__ , pPlayer->UserID(), pPlayer->NickName(), nTileID , pDrawTile->ID() ) ;
		return false;
	}

	// 检查一下玩家手中有没有这张牌
	if( !pPlayer->HasTile( nTileID ) )
	{ // 玩家手中没有这张牌
		TKWriteLog( "[%s:%d]user(%d[%s]) no tile(0x%03X)" , __FILE__ , __LINE__ , pPlayer->UserID(), pPlayer->NickName(), nTileID ) ;
		pPlayer->TraceInfo() ;
		return false;
	}

	// 设置这个玩家已经回复了,否则万一因为某种原因导致服务器陷入久等状态,这个玩家可能会被当作替罪羊
	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	pRequestObj->SetAckQuestSeat( nSeat, REQUEST_TYPE_CALL );

	// 设置这个玩家听牌（如果这个人是庄家，并且大家都还还没有出过牌，那么就是庄家报天听）
	int nCallType = 1 ;		// 听牌类型，缺省为普通听牌
	if( nSeat == Banker() && IsTian( nSeat ) )
	{ // 这个人就是庄家，并且是没有出牌也没有吃碰杠牌
		nCallType = 2 ;	// 庄家报天听
	}
	pPlayer->SetCallType( nCallType , pReqMahJongCall->bWinSelf , pReqMahJongCall->bAutoGang ) ;

	// 填写听牌类型
	pReqMahJongCall->nCallType = nCallType ;

	// 发对家牌给这个玩家
    _sendOtherTile(nSeat);

	// 分发给所有玩家
	if ( !Broadcast( ( PTKHEADER )pReqMahJongCall ) )
	{
		TKWriteLog( "%s : %d, Error! 发送消息出错, len( %d )", __FILE__, __LINE__, pReqMahJongCall->header.dwLength );
	}

	// 让这个玩家出牌
	TKREQMAHJONGDISCARDTILE reqMahJongDiscardTile = { 0 } ;
	reqMahJongDiscardTile.header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_DISCARD_TILE ) ;
	reqMahJongDiscardTile.header.dwLength = MSG_LENGTH( TKREQMAHJONGDISCARDTILE ) ;
	reqMahJongDiscardTile.nSeat           = nSeat ;
	reqMahJongDiscardTile.nRequestID      = pRequestObj->RequestID() ;
	reqMahJongDiscardTile.nTileID         = pReqMahJongCall->nTileID ;
	PlayerDiscardTile( &reqMahJongDiscardTile ) ;

	CheckCall(nSeat); // 无需打牌，只有一种结果
	if (!SetCallInfo(nSeat, pReqMahJongCall->nTileID, nCallType))
	{
		ASSERT(false);
	}
	CountHandTile(nSeat);
	CountHandTile(1 - nSeat);

	// 出牌后加倍统计
    if (m_pMahjongGame->Rule()->GetRule(RULE_DOUBLE_COUNT))
    {
        // 数据统计(两玩家都可见)
        m_nLastCallDisID[nSeat] = pReqMahJongCall->nTileID;
    }

	if (IsTwoTing())
	{
		m_bCheckCallTile = true;
	}

	return true;
}

void CTKMahjongServerObj::_sendOtherTile(int nSeat)
{
    char playerTiles[1024] = { 0 };
    PTKACKTILES ackTiles = (PTKACKTILES)playerTiles;
    ackTiles->header.dwType = ACK_TYPE(TK_MSG_PLAYER_TILES);
    ackTiles->header.dwLength = MSG_LENGTH(TKACKTILES);
    ackTiles->seat = 1 - nSeat;
    CTKMahjongPlayer * pOPlayer = GetPlayerBySeat(1 - nSeat);
    if (NULL == pOPlayer)
    {
        TKWriteLog("%s : %d, Error! 非天听获取对家出错, seat( %d )", __FILE__, __LINE__, 1 - nSeat);
        return;
    }
    ackTiles->cnHandTile = pOPlayer->FillHandTileInfo(ackTiles->anHandTile);
    Send2SeatPlayer(nSeat, PTKHEADER(ackTiles));

    // 暗杠牌
    SendMsgShowTiles(1 - nSeat);
}

// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CTKMahjongServerObj::SendMsgShowTiles( int seat, bool toWatch /* = false */ )
{
	/*char playerTiles[1024] = { 0 };
	PTKACKTILES ackTiles = ( PTKACKTILES )playerTiles;*/

	TKACKSHOWTILES showTiles = { 0 };
	showTiles.header.dwType = ACK_TYPE( TK_MSG_PLAYER_SHOWTILES );
	showTiles.header.dwLength = MSG_LENGTH( TKACKSHOWTILES );
	showTiles.seat = seat;
	CTKMahjongPlayer * player = GetPlayerBySeat( seat ) ;
	if ( NULL == player )
	{
		TKWriteLog( "%s : %d, Error! 获取亮牌玩家出错, seat( %d )", __FILE__, __LINE__, seat );
		return false;
	}

	showTiles.cnShow = player->FillShowTilesInfo( showTiles.anShowTiles );
	if ( showTiles.cnShow == 0 || showTiles.cnShow % 4 != 0 )
	{
		return false;
	}
	
	Send2SeatPlayer( 1 - seat, PTKHEADER(&showTiles) );

	if ( toWatch )
	{
		m_pMahjongGame->GameSend2Black( PTKHEADER(&showTiles) );
		m_pMahjongGame->GameSend2Observer( PTKHEADER(&showTiles) );
	}

	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CTKMahjongServerObj::PlayerReqWin( PTKREQMAHJONGWIN pReqMahJongWin, bool setAck )
{
	// 看看玩家是否可以和这个玩家的牌
	int nSeat = pReqMahJongWin->nSeat;
	CTKMahjongServerPlayer * pPlayer = (CTKMahjongServerPlayer *)GetPlayerBySeat( nSeat ) ;	// 这个玩家
	int nPaoSeat = pReqMahJongWin->nPaoSeat ;	// 放炮的玩家（如果是自摸，那么这个值就是和牌的玩家自己）
	if( !pPlayer->HasRequestPower( REQUEST_TYPE_WIN , nPaoSeat ) )
	{ // 玩家不能和这个人的牌
		TKWriteLog( TEXT( "【CTKMahjongServerObj::OnReqWin】,seat(%d),pao seat(%d)" ) , nSeat , nPaoSeat ) ;
		return false;
	}

	// 相公不能和牌
	if( pPlayer->IsXiangGong() )
	{ // 相公
		TKWriteLog( "[%s:%d]user(%d[%s]) xianggong,cannot win" , __FILE__ , __LINE__ , pPlayer->UserID(), pPlayer->NickName() ) ;
		return false;
	}

	// 如果这个玩家已经听牌了，那么看看是否设置了只能自摸
	if( pPlayer->HasCalled() )
	{ // 已经听牌了
		if( !pPlayer->HasCalledPower( REQUEST_TYPE_WIN , nPaoSeat ) )
		{ // 不能和
			TKWriteLog( TEXT( "【CMahJongGame::OnReqWin】,seat(%d) has called, pao seat(%d)" ) , nSeat , nPaoSeat ) ;
			return false;
		}
	}

	// 检查这个玩家是否可以和牌
	if( !CheckWin( pReqMahJongWin ) )
	{ // 玩家不能和牌
		//TKWriteLog( "[%s:%d]user(%d[%s]) not win(0x%03X)" , __FILE__ , __LINE__, pPlayer->UserID(), pPlayer->NickName(), pReqMahJongWin->nTileID ) ;
		pPlayer->TraceInfo() ;
		pPlayer->DeleteCallTile(pReqMahJongWin->nTileID);

		return false;
	}

	if (setAck)
	{
		// 设置玩家回复要和牌
#if _DEBUG
		TKWriteLog("玩家(%d)请求和牌", nSeat);
#endif
		CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
		pRequestObj->SetAckQuestSeat( nSeat , REQUEST_TYPE_WIN , ( PTKHEADER ) pReqMahJongWin ) ;

		// 告诉其他人可以放弃一些操作了
		SendGiveUpRequestMsg( nSeat , REQUEST_TYPE_WIN , nPaoSeat ) ;
	}

	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CTKMahjongServerObj::SendGiveUpRequestMsg( int nSeat , int nRequestType , int nParam )
{
	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	TKACKMAHJONGREQUEST ackMahJongRequest = { 0 } ;
	ackMahJongRequest.header.dwType   = ACK_TYPE( TK_MSG_MAHJONG_REQUEST ) ;
	ackMahJongRequest.header.dwLength = MSG_LENGTH( TKACKMAHJONGREQUEST ) ;
	ackMahJongRequest.nRequestID      = pRequestObj->RequestID() ;

	int anGiveUpRequestType[ MAX_PLAYER_COUNT ] = { 0 } ;	// 各个座位号的玩家应该放弃的请求类型

	// 
	// 对于吃碰杠牌，发送相应的放弃信息给其他所有的玩家；
	// 对于和牌，发送不能吃碰杠的放弃信息给和牌的上手玩家，发送不能吃碰杠和的放弃信息给和牌的下手玩家
	// 
	if( REQUEST_TYPE_WIN != nRequestType )
	{ // 不是和牌（吃碰杠）
		switch( nRequestType )
		{
		case REQUEST_TYPE_CHI :		// 吃
			return ;
		case REQUEST_TYPE_PENG :	// 碰
			ackMahJongRequest.nGiveUpRequestType = REQUEST_TYPE_CHI ;	// 不用吃了
			break ;
		case REQUEST_TYPE_GANG :	// 杠
			ackMahJongRequest.nGiveUpRequestType = REQUEST_TYPE_CHI | REQUEST_TYPE_PENG ;	// 不用吃碰了
			break ;
		}

		// 所有玩家放弃的类型都是一样的
		for( int i = 0; i < MAX_PLAYER_COUNT; i ++ )
		{ // 遍历所有座位号的玩家
			anGiveUpRequestType[ i ] = ackMahJongRequest.nGiveUpRequestType ;
		}
	}
	else
	{ // 和牌
		int nPaoSeat = nParam ;	// 这时，参数是放炮玩家的座位号
		ackMahJongRequest.nGiveUpRequestType = REQUEST_TYPE_CHI | REQUEST_TYPE_PENG | REQUEST_TYPE_GANG ;	// 不用吃碰杠了
		if( nPaoSeat != nSeat )
		{ 
            // 不是自摸的, 这时，和牌玩家上手玩家和下手玩家放弃的操作不同：上手玩家不用放弃和牌，下手玩家连和牌也要放弃
            BOOL bPrevPlayer = TRUE;	// 是否是和牌玩家的上手玩家
            int nTempSeat = nPaoSeat;
            for (int i = 1; i < MAX_PLAYER_COUNT; i++)
            { // 遍历除了放炮玩家之外的其他玩家
                nTempSeat = GetNextPlayerSeat(nTempSeat);	// 下一家
                if (nTempSeat == nSeat)
                { // 就是和牌的玩家
                    bPrevPlayer = FALSE;
                    continue;
                }

                if (bPrevPlayer)
                { // 这个玩家是和牌玩家的上手玩家
                    anGiveUpRequestType[nTempSeat] = ackMahJongRequest.nGiveUpRequestType;
                }
                else
                { // 这个玩家是和牌玩家的下手玩家
                    anGiveUpRequestType[nTempSeat] = (ackMahJongRequest.nGiveUpRequestType | REQUEST_TYPE_WIN);
                }
            }
		}
		else
		{ // 自摸的
			// 所有玩家放弃的类型都是一样的
			for( int i = 0; i < MAX_PLAYER_COUNT; i ++ )
			{ // 遍历所有座位号的玩家
				anGiveUpRequestType[ i ] = ( ackMahJongRequest.nGiveUpRequestType | REQUEST_TYPE_WIN ) ;
			}
		}
	}

	for( int i = 0; i < MAX_PLAYER_COUNT; i ++ )
	{ // 遍历所有玩家
		if ( !HasPlayer( i ) )
		{
			// 这个座位上没人
			continue;
		}

		if( pRequestObj->IsRequestSeat( i ) && !pRequestObj->IsAckRequestSeat( i ) )
		{ // 请求了这个玩家，并且这个玩家没有回复
			if( i != nSeat )
			{ // 不是这个采取了操作的玩家
				ackMahJongRequest.nGiveUpRequestType = anGiveUpRequestType[ i ] ;
				ackMahJongRequest.nSeat = i ;
				if ( !Send2SeatPlayer( i, ( PTKHEADER )&ackMahJongRequest ) )
				{
					TKWriteLog( "%s : %d, Error! 发送消息错误 len( %d )", __FILE__, __LINE__, ackMahJongRequest.header.dwLength );
				}
			}
		}
	}
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
BOOL CTKMahjongServerObj::CheckWin( PTKREQMAHJONGWIN pReqMahJongWin )
{
	// 构造一个CHECKPARAM以交给Judge来判断
	CHECKPARAM sCheckParam = { 0 } ;

	// 由Player类填充一部分
	CTKMahjongPlayer * pPlayer = GetPlayerBySeat( pReqMahJongWin->nSeat ) ;	// 这个要和牌的玩家
	pPlayer->FillCheckParam( &sCheckParam ) ;
	// 还需要填充和牌方式，门风，所需最小番数
	sCheckParam.nWinMode = WinMode( pReqMahJongWin->nSeat, pReqMahJongWin->nPaoSeat, pReqMahJongWin->nTileID );
	sCheckParam.nQuanWind = ( COLOR_WIND << 8 ) + ( m_nRoundWind << 4 );
	sCheckParam.nMinFan   = 0;
	// 如果不是自摸的，还要将别人打的牌或杠的牌传进去
	if( pReqMahJongWin->nPaoSeat != pReqMahJongWin->nSeat )
	{ // 不是自摸
		int nID = pReqMahJongWin->nTileID ;	// 和的这张牌的ID号
		sCheckParam.asHandStone[ 0 ].nID    = nID ;
		sCheckParam.asHandStone[ 0 ].nColor = CMahJongTile::TileColor( nID ) ;
		sCheckParam.asHandStone[ 0 ].nWhat  = CMahJongTile::TileWhat( nID ) ;
		sCheckParam.cnHandStone++;
	}

	// 客户端发过来的分组信息校验过，是可和的，这里只需要算番就行了
	WININFO infoWin = { 0 } ;
	memcpy( infoWin.asGroup, pReqMahJongWin->asGroup, sizeof( infoWin.asGroup ) );
	memcpy( infoWin.asHandStone, pReqMahJongWin->asHandTile, sizeof( infoWin.asHandStone ) );
	infoWin.cnGroups = pReqMahJongWin->cnGroups;
	infoWin.nResultant = pReqMahJongWin->nResultant;
	// 下面几个变量主要用于番种分析，由服务器算完番后填充，再发到客户端
	infoWin.cnShowGroups = pReqMahJongWin->cnShowGroups;
	infoWin.nHuTileID = pReqMahJongWin->nTileID;
	infoWin.cnFlower = sCheckParam.cnFlower;
	memcpy( &infoWin.asFlower, &sCheckParam.asFlower, sizeof( MAHJONGTILE ) * infoWin.cnFlower );
	infoWin.nQuanWind = sCheckParam.nQuanWind;
	infoWin.nMenWind = sCheckParam.nMenWind;
	infoWin.nWinMode = sCheckParam.nWinMode;

	BOOL bCanWin = m_pJudge->CanWin( sCheckParam, infoWin );
	if( bCanWin )
	{ // 这个玩家的确和牌了
		int nSpecialTileCount = GetSpecialCount(sCheckParam);
		{
			TKACKMAHJONGSPECIALCOUNT ackMahJongSpecialCount = { 0 } ;
			ackMahJongSpecialCount.header.dwType   = ACK_TYPE( TK_MSG_MAHJONG_SPECIALCOUNT ) ;
			ackMahJongSpecialCount.header.dwLength = MSG_LENGTH( TKACKMAHJONGSPECIALCOUNT ) ;
			ackMahJongSpecialCount.nSpecialCount = nSpecialTileCount;

			// 分发给所有玩家
			if ( !Broadcast( ( PTKHEADER )&ackMahJongSpecialCount ) )
			{
				TKWriteLog( "%s : %d, Error! 发送消息出错, len( %d )", __FILE__, __LINE__, ackMahJongSpecialCount.header.dwLength );
			}
		}
		int nSpecialTileFan = nSpecialTileCount*m_pGameRule->GetRule(RULE_SPECIALTILE_FAN);
		infoWin.nMaxFans += nSpecialTileFan;
		infoWin.anFans[FAN_SPECIAL] = nSpecialTileFan; // 录像中的加番牌信息不通过这种方式取，因为会带来pc端bug
		( ( CTKMahjongServerPlayer * )pPlayer )->WinInfo( infoWin ) ;	// 设置这个玩家的和牌信息
	
#if _DEBUG
		TKWriteLog("######################   nMaxFans %d", infoWin.nMaxFans);
#endif
   
	}

	return bCanWin ;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CHECKRESULT CTKMahjongServerObj::CheckBotWin( int seat, bool ziMo, int huId, PTKREQMAHJONGWIN pReqMahJongWin )
{
	try
	{
		CHECKPARAM sCheckParam = { 0 } ;
		int paoSeat = ziMo ? seat : (1 - seat);
		// FillCheckParam( &sCheckParam, nPaoSeat ) ;

		// 由Player类填充一部分
		CTKMahjongPlayer *pPlayer = GetPlayerBySeat( seat ) ;	// 这个要和牌的玩家
		// 从这个玩家手中找花牌
		CMahJongTile* flowerTile = pPlayer->GetOneFlowerTile() ;
		if( NULL != flowerTile ) return F_NOTTING;
		pPlayer->FillCheckParam( &sCheckParam ) ;
		// 还需要填充和牌方式，门风，所需最小番数
		sCheckParam.nQuanWind = ( COLOR_WIND << 8 ) + ( m_nRoundWind << 4 );
		sCheckParam.nMinFan   = 0;
		// 如果不是自摸的，还要将别人打的牌或杠的牌传进去
		if( !ziMo )
		{ // 不是自摸
			assert( 0 != huId );
			int nID = huId ;	// 和的这张牌的ID号
			sCheckParam.asHandStone[ 0 ].nID    = nID ;
			sCheckParam.asHandStone[ 0 ].nColor = CMahJongTile::TileColor( nID ) ;
			sCheckParam.asHandStone[ 0 ].nWhat  = CMahJongTile::TileWhat( nID ) ;
			sCheckParam.cnHandStone++;
		}
		sCheckParam.nWinMode = WinMode( seat, paoSeat, sCheckParam.asHandStone[ 0 ].nID );

		WININFO sCheckWinInfo;
		CHECKRESULT eCheckWinResult = m_pJudge->CheckWin( sCheckParam , sCheckWinInfo ) ;

		CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
		// 填写和牌消息
		pReqMahJongWin->header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_WIN ) ;
		pReqMahJongWin->header.dwLength = MSG_LENGTH( TKREQMAHJONGWIN ) ;
		pReqMahJongWin->nRequestID      = pRequestObj->RequestID();
		pReqMahJongWin->nSeat           = seat ;
		pReqMahJongWin->nPaoSeat        = paoSeat ;
		pReqMahJongWin->nTileID         = sCheckParam.asHandStone[ 0 ].nID;
		if( eCheckWinResult == T_OK )
		{ // 能和
			pReqMahJongWin->nResultant = sCheckWinInfo.nResultant ;
			pReqMahJongWin->cnGroups   = sCheckWinInfo.cnGroups ;
			memcpy( pReqMahJongWin->asGroup , sCheckWinInfo.asGroup , sizeof( pReqMahJongWin->asGroup ) ) ;
			memcpy( pReqMahJongWin->asHandTile, sCheckWinInfo.asHandStone, sizeof( pReqMahJongWin->asHandTile ) ) ;
			pReqMahJongWin->cnShowGroups = sCheckWinInfo.cnShowGroups ;
		}
		else
		{ // 诈和
			pReqMahJongWin->nResultant = WIN_FAIL ;
		}

		return eCheckWinResult ;
	}
	catch (...)
	{
		TKWriteLog( "%s : %d, Exception", __FILE__, __LINE__ );
		return F_NOTTING;
	}
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CTKMahjongServerObj::PlayerReqChi( PTKREQMAHJONGCHI pReqMahJongChi )
{
	int nSeat = pReqMahJongChi->nSeat ;	// 这个玩家的座位号

	if ( !m_pGameRule->GetRule( RULE_CANEAT ) ) 
	{
		// 不能吃牌
		TKWriteLog( TEXT( "【CMahJongGame::OnReqChi】,seat(%d),can not eat" ) , nSeat , pReqMahJongChi->nTileID ) ;
		return false;
	}

	// 看看消息这张牌是否是出的那张牌
	if( !IsDiscardTile( pReqMahJongChi->nTileID ) )
	{ // 这张牌不是刚才玩家出的那张牌
		TKWriteLog( TEXT( "【CMahJongGame::OnReqChi】,seat(%d),not discard tile(0x%03X)" ) , nSeat , pReqMahJongChi->nTileID ) ;
		return false;
	}

	// 检查玩家是否可以吃这个出牌玩家的牌
	CTKMahjongPlayer * pPlayer = GetPlayerBySeat( nSeat ) ;	// 这个玩家
	int nDiscardSeat = CurDiscardSeat() ;	// 当前出牌的玩家
	if( !pPlayer->HasRequestPower( REQUEST_TYPE_CHI , nDiscardSeat ) )
	{ // 这个玩家不能吃当前出牌玩家的牌
		TKWriteLog( TEXT( "【CMahJongGame::OnReqChi】,seat(%d),discard seat(%d)" ) , nSeat , nDiscardSeat ) ;
		return false ;
	}

	// 如果玩家已经听牌，那么不能吃牌
	if( pPlayer->HasCalled() )
	{ // 已经听牌了
		TKWriteLog( TEXT( "【CMahJongGame::OnReqChi】,seat(%d) has called" ) , nSeat ) ;
		return false ;
	}

	// 看看这张牌是否是万、条、饼
	int nColor = TILE_COLOR( pReqMahJongChi->nTileID ) ;	// 花色
	int nWhat  = TILE_WHAT( pReqMahJongChi->nTileID ) ;	// 点数
	if( COLOR_WAN != nColor && COLOR_TIAO != nColor && COLOR_BING != nColor )
	{ // 不是万、条、饼
		TKWriteLog( "[%s:%d]user(%d[%s]) tile(0x%03X) not WAN/TIAO/BING", __FILE__, __LINE__, pPlayer->UserID(), pPlayer->NickName(), pReqMahJongChi->nTileID ) ;
		return false;
	}

	// 看看这个玩家手中是否有这几张牌，并且这三张牌是否是连续的三张同花色的牌
	int nTileID = 0 ;
	int nCheck = 0 ;	// 用于校验点数是否相邻的
	int nTemp = 0 ;
	BOOL bFind = FALSE ;	// 在消息中的这三张牌中，是否找到了玩家出的那张牌（不能是其他随便的一个顺子牌）
	for( int i = 0 ; i < 3 ; i ++ )
	{ // 遍历这三张牌
		nTileID = pReqMahJongChi->anChiTileID[ i ] ;	// 这张牌的ID号
		if( SAME_TILE_ID( pReqMahJongChi->nTileID , nTileID ) )
		{ // 这张是要吃的那张牌
			bFind = TRUE ;	// 找到了玩家出的那张牌
			continue ;
		}

		// 看看有没有这张牌
		if( !pPlayer->HasTile( nTileID ) )
		{ // 玩家手中没有这张牌
			TKWriteLog( "[%s:%d]user(%d[%s]) no %d tile(0x%03X)" , __FILE__ , __LINE__ , pPlayer->UserID() , pPlayer->NickName() , i , nTileID ) ;
			TKWriteLog( TEXT( "tileinfo(0x%03X)(0x%03X,0x%03X,0x%03X)" ) , pReqMahJongChi->nTileID , pReqMahJongChi->anChiTileID[ 0 ] , pReqMahJongChi->anChiTileID[ 1 ] , pReqMahJongChi->anChiTileID[ 2 ] ) ;
			pPlayer->TraceInfo() ;
			return false ;
		}

		// 看看花色是否相同，点数是否相连
		if( nColor != TILE_COLOR( nTileID ) )
		{ // 花色不同
			TKWriteLog( "[%s:%d]user(%d[%s]) tile(0x%03X,0x%03X) not same color" , __FILE__ , __LINE__ , pPlayer->UserID() , pPlayer->NickName() , nTileID , pReqMahJongChi->nTileID ) ;
			return false ;
		}
		nTemp = nWhat - TILE_WHAT( nTileID ) ;
		if( ( -2 != nTemp ) && ( -1 != nTemp ) && ( 1 != nTemp ) && ( 2 != nTemp ) )
		{ // 不是相连的牌
			TKWriteLog( "[%s:%d]user(%d[%s]) tile(0x%03X,0x%03X) not continue" , __FILE__ , __LINE__ , pPlayer->UserID() , pPlayer->NickName() , nTileID , pReqMahJongChi->nTileID ) ;
			return false ;
		}
		nCheck += nTemp ;
	}
	if( !bFind )
	{ // 这三张牌中根本没有玩家出的那张牌
		TKWriteLog( "[%s:%d]user(%d[%s]) tile(0x%03X,0x%03X,0x%03X) no tile(0x%03X)" , __FILE__ , __LINE__ , pPlayer->UserID() , pPlayer->NickName() , pReqMahJongChi->anChiTileID[ 0 ] , pReqMahJongChi->anChiTileID[ 1 ] , pReqMahJongChi->anChiTileID[ 2 ] , pReqMahJongChi->nTileID ) ;
		return false ;
	}
	if( ( -3 != nCheck ) && ( 0 != nCheck ) && ( 3 != nCheck ) )
	{ // 不是相连的三张牌
		TKWriteLog( "[%s:%d]user(%d[%s]) tile(0x%03X,0x%03X,0x%03X) not continue" , __FILE__ , __LINE__ , pPlayer->UserID() , pPlayer->NickName() , pReqMahJongChi->anChiTileID[ 0 ] , pReqMahJongChi->anChiTileID[ 1 ] , pReqMahJongChi->anChiTileID[ 2 ] ) ;
		return false ;
	}

	// 设置玩家回复要吃牌
	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	pRequestObj->SetAckQuestSeat( nSeat , REQUEST_TYPE_CHI , ( PTKHEADER ) pReqMahJongChi ) ;

	// 告诉其他人可以放弃一些操作了
	SendGiveUpRequestMsg( nSeat , REQUEST_TYPE_CHI , 0 ) ;

	return true ;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CTKMahjongServerObj::PlayerReqDbl( PTKREQMAHJONGDBL pReqMahjongDbl )
{
	// 是否选择了加倍
	if ( !pReqMahjongDbl->isDouble ) return false;

	int seat = pReqMahjongDbl->seat;
	// 加倍次数检测 
	bool canDouble = CheckCanDouble( seat );
	if ( !canDouble ) return false;

	// 设置玩家回复要加倍
	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	pRequestObj->SetAckQuestSeat( seat , REQUEST_TYPE_DOUBLE  , ( PTKHEADER ) pReqMahjongDbl ) ;

	// 告诉其他人可以放弃一些操作了
	SendGiveUpRequestMsg( seat , REQUEST_TYPE_DOUBLE | REQUEST_TYPE_WIN, 0 ) ;

	return true ;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CTKMahjongServerObj::PlayerReqPeng( PTKREQMAHJONGPENG pReqMahJongPeng )
{
	int nSeat = pReqMahJongPeng->nSeat ;	// 这个玩家的座位号

	if ( !m_pGameRule->GetRule( RULE_CANPONG ) ) 
	{
		// 不能碰牌
		TKWriteLog( TEXT( "【CMahJongGame::OnReqPeng】,seat(%d), can not peng" ) , nSeat ) ;
		return false;
	}

	// 看看消息这张牌是否是出的那张牌
	if( !IsDiscardTile( pReqMahJongPeng->nTileID ) )
	{ // 这张牌不是刚才玩家出的那张牌
		TKWriteLog( TEXT( "【CMahJongGame::OnReqPeng】,seat(%d),not discard tile(0x%03X)" ) , nSeat , pReqMahJongPeng->nTileID ) ;
		return false ;
	}

	// 检查玩家是否可以碰这个出牌玩家的牌
	CTKMahjongPlayer * pPlayer = GetPlayerBySeat( nSeat ) ;	// 这个玩家
	int nDiscardSeat = CurDiscardSeat() ;	// 当前出牌的玩家
	if( !pPlayer->HasRequestPower( REQUEST_TYPE_PENG , nDiscardSeat ) )
	{ // 这个玩家不能碰当前出牌玩家的牌
		TKWriteLog( TEXT( "【CMahJongGame::OnReqPeng】,seat(%d),discard seat(%d)" ) , nSeat , nDiscardSeat ) ;
		return false ;
	}

	// 如果玩家已经听牌，那么不能碰牌
	if( pPlayer->HasCalled() )
	{ // 已经听牌了
		TKWriteLog( TEXT( "【CMahJongGame::OnReqPeng】,seat(%d) has called" ) , nSeat ) ;
		return false ;
	}

	if ( !IsNoSameTileID( pReqMahJongPeng->anPengTileID, 3 ) ) 
	{
		TKWriteLog( "[%s:%d]user(%d[%s]) tile(0x%03X, 0x%03X, 0x%03X )" , __FILE__ , __LINE__ , pPlayer->UserID() , pPlayer->NickName() , pReqMahJongPeng->anPengTileID[0], pReqMahJongPeng->anPengTileID[1], pReqMahJongPeng->anPengTileID[2] );
		return false;
	}

	// 看看这个玩家手中是否有这几张牌，并且花色、点数是否相同
	int nTileID = 0 ;
	int nColor = TILE_COLOR( pReqMahJongPeng->nTileID ) ;	// 花色
	int nWhat  = TILE_WHAT( pReqMahJongPeng->nTileID ) ;	// 点数
	BOOL bFind = FALSE ;	// 在消息中的这三张牌中，是否找到了玩家出的那张牌（不能是其他随便的一个刻子牌）
	for( int i = 0 ; i < 3 ; i ++ )
	{ // 遍历这三张牌
		nTileID = pReqMahJongPeng->anPengTileID[ i ] ;	// 这张牌的ID号
		if( SAME_TILE_ID( pReqMahJongPeng->nTileID , nTileID ) )
		{ // 这张是要碰的那张牌
			bFind = TRUE ;
			continue ;
		}

		// 看看有没有这张牌
		if( !pPlayer->HasTile( nTileID ) )
		{ // 玩家手中没有这张牌
			TKWriteLog( "[%s:%d]user(%d[%s]) no %d tile(0x%03X)" , __FILE__ , __LINE__ , pPlayer->UserID() , pPlayer->NickName() , i , nTileID ) ;
			TKWriteLog( TEXT( "tileinfo(0x%03X)(0x%03X,0x%03X,0x%03X)" ) , pReqMahJongPeng->nTileID , pReqMahJongPeng->anPengTileID[ 0 ] , pReqMahJongPeng->anPengTileID[ 1 ] , pReqMahJongPeng->anPengTileID[ 2 ] ) ;
			pPlayer->TraceInfo() ;
			return false ;
		}

		// 看看花色、点数是否相同
		if( nColor != TILE_COLOR( nTileID ) || nWhat != TILE_WHAT( nTileID ) )
		{ // 花色或者点数不同
			TKWriteLog( "[%s:%d]user(%d[%s]) tile(0x%03X,0x%03X)" , __FILE__ , __LINE__ , pPlayer->UserID() , pPlayer->NickName() , nTileID , pReqMahJongPeng->nTileID ) ;
			return false ;
		}
	}
	if( !bFind )
	{ // 要碰的这三张牌中根本没有玩家出的那张牌
		TKWriteLog( "[%s:%d]user(%d[%s]) tile(0x%03X,0x%03X,0x%03X) no tile(0x%03X)" , __FILE__ , __LINE__ , pPlayer->UserID() , pPlayer->NickName() , pReqMahJongPeng->anPengTileID[ 0 ] , pReqMahJongPeng->anPengTileID[ 1 ] , pReqMahJongPeng->anPengTileID[ 2 ] , pReqMahJongPeng->nTileID ) ;
		return false ;
	}

	// 设置玩家回复要碰牌
	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	pRequestObj->SetAckQuestSeat( nSeat , REQUEST_TYPE_PENG , ( PTKHEADER ) pReqMahJongPeng ) ;

	// 告诉其他人可以放弃一些操作了
	SendGiveUpRequestMsg( nSeat , REQUEST_TYPE_PENG , 0 ) ;

    m_vecPengCardsIDJust.clear();
    for (int i=0; i<3; ++i)
    {// 记录刚刚碰到的牌，在碰后杠校验时使用
        m_vecPengCardsIDJust.push_back(pReqMahJongPeng->anPengTileID[i]);
    }

	return true ;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CTKMahjongServerObj::IsNoSameTileID( int anTileID[], int cnTile )
{
	for ( int i = 0; i < cnTile; i++ ) 
	{
		for ( int j = i + 1; j < cnTile; j++ ) 
		{
			if ( anTileID[i] == anTileID[j] ) 
			{
				return false;
			}
		}
	}

	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CTKMahjongServerObj::PlayerReqGang( PTKREQMAHJONGGANG pReqMahJongGang, bool bSelfGang, bool bAfterPeng )
{
    // 牌墙空，不允许杠牌，即不允许杠最后一张牌
    if (m_pTileWall->Empty())
    {
        return false;
    }

	// 检查玩家是否可以杠这个玩家出的牌，或者是自己摸的牌
	int nSeat = pReqMahJongGang->nSeat;
	CTKMahjongPlayer * pPlayer = GetPlayerBySeat( nSeat ) ;	// 这个玩家
	int nGangSeat = CurDiscardSeat() ;	// 要杠这个玩家的牌
	if( !pPlayer->HasRequestPower( REQUEST_TYPE_GANG , nGangSeat ) )
	{ // 不能杠这个玩家的牌
		TKWriteLog( TEXT( "【CMahJongGame::OnReqGang】,seat(%d),gang seat(%d)" ) , nSeat , nGangSeat ) ;
		return false ;
	}

	// 如果玩家已经听牌，那么看看是否在听牌时设置了可以杠牌
	if( pPlayer->HasCalled() )
	{ // 已经听牌了
		if( !pPlayer->HasCalledPower( REQUEST_TYPE_GANG , pReqMahJongGang->nGangType ) )
		{ // 不能杠牌
			TKWriteLog( TEXT( "【CMahJongGame::OnReqGang】,seat(%d) has called,gang type(%d)" ) , nSeat , pReqMahJongGang->nGangType ) ;
			return false ;
		}
	}

	// 有可能发过来的所有牌都是同一个ID
	if ( !IsNoSameTileID( pReqMahJongGang->anGangTileID, 4 ) ) 
	{
		TKWriteLog( "[%s:%d]user(%d[%s]) tile(0x%03X, 0x%03X, 0x%03X, 0x%03X )" , __FILE__ , __LINE__ , pPlayer->UserID() , pPlayer->NickName() , pReqMahJongGang->anGangTileID[0], pReqMahJongGang->anGangTileID[1], pReqMahJongGang->anGangTileID[2], pReqMahJongGang->anGangTileID[3] );
		return false;
	}


	// 检查杠牌的类型是否合法，以及玩家手中是否有这些牌
	int nTileID = 0 ;
	int nColor = TILE_COLOR( pReqMahJongGang->nTileID ) ;	// 牌的花色
	int nWhat  = TILE_WHAT( pReqMahJongGang->nTileID ) ;	// 牌的点数


	if (bAfterPeng && IS_GANG_TYPE_DIRECT(pReqMahJongGang->nGangType) && IS_GANG_TYPE_PUBLIC(pReqMahJongGang->nGangType))
	{
		// 这时有效的杠牌类型是：直杠＋明杠
		int nGangType = 0 ;
		GANG_TYPE_PUBLIC( nGangType ) ;
		GANG_TYPE_DIRECT( nGangType ) ;
		if( nGangType != pReqMahJongGang->nGangType )
		{ // 杠牌类型不对
			TKWriteLog( TEXT( "【CMahJongGame::OnReqGang】,seat(%d),wrong gang type(%d-%d)" ) , nSeat , pReqMahJongGang->nGangType , nGangType ) ;
			return false ;
		}

		nTileID = pReqMahJongGang->nTileID ;
		if( !pPlayer->HasTile( nTileID ) )
		{ // 手中没有这张牌
			TKWriteLog( "[%s:%d]user(%d[%s]) no tile(0x%03X)" , __FILE__ , __LINE__ , pPlayer->UserID() , pPlayer->NickName() , nTileID ) ;
			TKWriteLog( TEXT( "tileinfo(0x%03X)(0x%03X,0x%03X,0x%03X,0x%03X)" ) , pReqMahJongGang->nTileID , pReqMahJongGang->anGangTileID[ 0 ] , pReqMahJongGang->anGangTileID[ 1 ] , pReqMahJongGang->anGangTileID[ 2 ] , pReqMahJongGang->anGangTileID[ 3 ] ) ;
			pPlayer->TraceInfo() ;
			return false ;
		}
		if( !pPlayer->HasPeng( nColor , nWhat ) )
		{ // 没有碰这张牌
			TKWriteLog( "[%s:%d]user(%d[%s]) not peng(%d,%d)" , __FILE__ , __LINE__ , pPlayer->UserID() , pPlayer->NickName() , nColor , nWhat ) ;
			return false ;
		}
		//pReqMahJongGang.bBugen = false;

		m_pMahjongGame->RequestObj()->SetAckQuestSeat( nSeat , REQUEST_TYPE_GANG ) ;

	} 
	else if( bSelfGang )
	{ 
		// 自己摸牌之后的杠牌
		// 这时有效的杠牌类型是：直杠＋暗杠，补杠＋明杠
		int nGangType1 = 0 ,
			nGangType2 = 0 ;
		GANG_TYPE_HIDDEN( nGangType1 ) ;
		GANG_TYPE_DIRECT( nGangType1 ) ;
		GANG_TYPE_PATCH( nGangType2 ) ;
		GANG_TYPE_PUBLIC( nGangType2 ) ;
		if( nGangType1 != pReqMahJongGang->nGangType && nGangType2 != pReqMahJongGang->nGangType )
		{ // 杠牌类型不对
			TKWriteLog( TEXT( "【CMahJongGame::OnReqGang】,seat(%d),wrong gang type(%d-%d,%d)" ) , nSeat , pReqMahJongGang->nGangType , nGangType1 , nGangType2 ) ;
			return false ;
		}

		// 这时候，杠牌玩家手中有四张相同花色和点数的牌；或者手中有一张，并且碰了这张牌
		if( IS_GANG_TYPE_PUBLIC( pReqMahJongGang->nGangType ) )
		{ // 明杠（这时手中一张，并且碰了这张牌）
			nTileID = pReqMahJongGang->nTileID ;
			if( !pPlayer->HasTile( nTileID ) )
			{ // 手中没有这张牌
				TKWriteLog( "[%s:%d]user(%d[%s]) no tile(0x%03X)" , __FILE__ , __LINE__ , pPlayer->UserID() , pPlayer->NickName() , nTileID ) ;
				TKWriteLog( TEXT( "tileinfo(0x%03X)(0x%03X,0x%03X,0x%03X,0x%03X)" ) , pReqMahJongGang->nTileID , pReqMahJongGang->anGangTileID[ 0 ] , pReqMahJongGang->anGangTileID[ 1 ] , pReqMahJongGang->anGangTileID[ 2 ] , pReqMahJongGang->anGangTileID[ 3 ] ) ;
				pPlayer->TraceInfo() ;
				return false ;
			}
			if( !pPlayer->HasPeng( nColor , nWhat ) )
			{ // 没有碰这张牌
				TKWriteLog( "[%s:%d]user(%d[%s]) not peng(%d,%d)" , __FILE__ , __LINE__ , pPlayer->UserID() , pPlayer->NickName() , nColor , nWhat ) ;
				return false ;
			}
		}
		else if( IS_GANG_TYPE_HIDDEN( pReqMahJongGang->nGangType ) )
		{ // 暗杠（这时手中有四张这个牌）
			BOOL bFind = FALSE ;	// 这四张牌中是否有要杠的这张牌
			for( int i = 0 ; i < 4 ; i ++ )
			{ // 遍历这四张牌
				nTileID = pReqMahJongGang->anGangTileID[ i ] ;
				if( SAME_TILE_ID( nTileID , pReqMahJongGang->nTileID ) )
				{ // 就是这张牌
					bFind = TRUE ;
				}

				// 看看玩家手中是否有这张牌
				if( !pPlayer->HasTile( nTileID ) )
				{ // 没有这张牌
					TKWriteLog( "[%s:%d]user(%d[%s]) no %d tile(0x%03X)" , __FILE__ , __LINE__ , pPlayer->UserID() , pPlayer->NickName() , i , nTileID ) ;
					TKWriteLog( TEXT( "tileinfo(0x%03X)(0x%03X,0x%03X,0x%03X,0x%03X)" ) , pReqMahJongGang->nTileID , pReqMahJongGang->anGangTileID[ 0 ] , pReqMahJongGang->anGangTileID[ 1 ] , pReqMahJongGang->anGangTileID[ 2 ] , pReqMahJongGang->anGangTileID[ 3 ] ) ;
					pPlayer->TraceInfo() ;
					return false ;
				}

				// 看看花色、点数是否相同
				if( nColor != TILE_COLOR( nTileID ) || nWhat != TILE_WHAT( nTileID ) )
				{ // 花色或者点数不同
					TKWriteLog( "[%s:%d]user(%d[%s]) tile(0x%03X,0x%03X)" , __FILE__ , __LINE__ , pPlayer->UserID() , pPlayer->NickName() , nTileID , pReqMahJongGang->nTileID ) ;
					return false ;
				}
			}
			if( !bFind )
			{ // 这四张牌中没有要杠的这张牌
				TKWriteLog( "[%s:%d]user(%d[%s]) tile(0x%03X,0x%03X,0x%03X,0x%03X) no tile(0x%03X)" , __FILE__ , __LINE__ , pPlayer->UserID() , pPlayer->NickName() , pReqMahJongGang->anGangTileID[ 0 ] , pReqMahJongGang->anGangTileID[ 1 ] , pReqMahJongGang->anGangTileID[ 2 ] , pReqMahJongGang->anGangTileID[ 3 ] , pReqMahJongGang->nTileID ) ;
				return false ;
			}
		}

		// 设置这个玩家已经回复了,否则万一因为某种原因导致服务器陷入久等状态,这个玩家可能会被当作替罪羊
		m_pMahjongGame->RequestObj()->SetAckQuestSeat( nSeat , REQUEST_TYPE_GANG ) ;
	}
	else
	{ 
		// 别人出牌之后的杠牌
		// 这时有效的杠牌类型是：直杠＋明杠
		int nGangType = 0 ;
		GANG_TYPE_PUBLIC( nGangType ) ;
		GANG_TYPE_DIRECT( nGangType ) ;
		if( nGangType != pReqMahJongGang->nGangType )
		{ // 杠牌类型不对
			TKWriteLog( TEXT( "【CMahJongGame::OnReqGang】,seat(%d),wrong gang type(%d-%d)" ) , nSeat , pReqMahJongGang->nGangType , nGangType ) ;
			return false ;
		}

		// 看看消息这张牌是否是出的那张牌
		if( !IsDiscardTile( pReqMahJongGang->nTileID ) )
		{ // 这张牌不是刚才玩家出的那张牌
			TKWriteLog( TEXT( "【CMahJongGame::OnReqGang】,seat(%d),not discard tile(0x%03X)" ) , nSeat , pReqMahJongGang->nTileID ) ;
			return false ;
		}

		// 这时候，杠牌玩家手中应该有三张相同花色和点数的牌
		BOOL bFind = FALSE ;	// 这四张牌中是否有要杠的那张牌
		for( int i = 0 ; i < 4 ; i ++ )
		{ // 遍历这四张牌
			nTileID = pReqMahJongGang->anGangTileID[ i ] ;
			if( SAME_TILE_ID( nTileID , pReqMahJongGang->nTileID ) )
			{ // 就是这张牌
				bFind = TRUE ;
				continue ;
			}

			// 看看玩家手中是否有这张牌
			if( !pPlayer->HasTile( nTileID ) )
			{ // 没有这张牌
				TKWriteLog( "[%s:%d]user(%d[%s]) no %d tile(0x%03X)" , __FILE__ , __LINE__ , pPlayer->UserID() , pPlayer->NickName() , i , nTileID ) ;
				TKWriteLog( TEXT( "tileinfo(0x%03X)(0x%03X,0x%03X,0x%03X,0x%03X)" ) , pReqMahJongGang->nTileID , pReqMahJongGang->anGangTileID[ 0 ] , pReqMahJongGang->anGangTileID[ 1 ] , pReqMahJongGang->anGangTileID[ 2 ] , pReqMahJongGang->anGangTileID[ 3 ] ) ;
				pPlayer->TraceInfo() ;
				return false ;
			}

			// 看看花色、点数是否相同
			if( nColor != TILE_COLOR( nTileID ) || nWhat != TILE_WHAT( nTileID ) )
			{ // 花色或者点数不同
				TKWriteLog( "[%s:%d]user(%d[%s]) tile(0x%03X,0x%03X)" , __FILE__ , __LINE__ , pPlayer->UserID() , pPlayer->NickName() , nTileID , pReqMahJongGang->nTileID ) ;
				return false;
			}
		}
		if( !bFind )
		{ // 这四张牌中没有要杠的这张牌
			TKWriteLog( "[%s:%d]user(%d[%s]) tile(0x%03X,0x%03X,0x%03X,0x%03X) no tile(0x%03X)" , __FILE__ , __LINE__ , pPlayer->UserID() , pPlayer->NickName() , pReqMahJongGang->anGangTileID[ 0 ] , pReqMahJongGang->anGangTileID[ 1 ] , pReqMahJongGang->anGangTileID[ 2 ] , pReqMahJongGang->anGangTileID[ 3 ] , pReqMahJongGang->nTileID ) ;
			return false ;
		}

		// 设置玩家回复要杠牌
		m_pMahjongGame->RequestObj()->SetAckQuestSeat( nSeat , REQUEST_TYPE_GANG , ( PTKHEADER ) pReqMahJongGang ) ;

		// 告诉其他人可以放弃一些操作了
		SendGiveUpRequestMsg( nSeat , REQUEST_TYPE_GANG , 0 ) ;
	}


	return true;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CTKMahjongServerObj::PlayerChi( PTKREQMAHJONGCHI pReqMahJongChi )
{
	// 分发给所有玩家
	if ( !Broadcast( ( PTKHEADER )pReqMahJongChi ) )
	{
		TKWriteLog( "%s : %d, Error! 发送消息出错, len( %d )", __FILE__, __LINE__, pReqMahJongChi->header.dwLength );
	}

	// 玩家吃牌
	CMahJongTile *apChiTile[ 3 ] = { 0 } ;
	PlayerChiTile( pReqMahJongChi , apChiTile ) ;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CTKMahjongServerObj::PlayerPeng( PTKREQMAHJONGPENG pReqMahJongPeng )
{
	// 分发给所有玩家
	if ( !Broadcast( ( PTKHEADER )pReqMahJongPeng ) )
	{
		TKWriteLog( "%s : %d, Error! 发送消息出错, len( %d )", __FILE__, __LINE__, pReqMahJongPeng->header.dwLength );
	}

	// 玩家碰牌
	CMahJongTile* apPengTile[ 3 ] = { 0 } ;
	PlayerPengTile( pReqMahJongPeng , apPengTile ) ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CTKMahjongServerObj::PlayerWin( PTKREQMAHJONGWIN pReqMahJongWin )
{
	int nSeat = pReqMahJongWin->nSeat ;	// 这个和牌玩家的座位号
	CTKMahjongServerPlayer * pPlayer = NULL ;
	int nTileID = pReqMahJongWin->nTileID ;	// 和的这张牌
	CMahJongTile *pTile = NULL ;

	// 重置杠牌的信息
	ResetLastGangInfo() ;

	// 如果不是自摸，把这张牌从出牌玩家出的牌（如果是抢杠，那么是从杠的牌）中删掉，加入到和牌玩家手中
	if( nSeat != pReqMahJongWin->nPaoSeat )
	{ // 不是自摸
		pPlayer = ( CTKMahjongServerPlayer * )GetPlayerBySeat( pReqMahJongWin->nPaoSeat ) ;	// 放炮的玩家
		if( IsLastGangTile( nTileID ) )
		{ // 和的就是杠的那张牌（抢杠）
			pTile = pPlayer->DeleteGangTile( nTileID ) ;	// 把这张牌从杠的牌中删除
		}
		else
		{ // 和的是别人出的牌
			pTile = pPlayer->DeleteDiscardTile( nTileID ) ;	// 把这张牌从出的牌中删掉
		}

		if ( NULL == pTile )
		{
			// 没有,可能是一炮多响,被上一个和牌玩家拿走了,重新构造一个
			pTile = GetOccasionalTile( nTileID );
		}

		// 增加放炮玩家的放炮次数
		pPlayer->AddPaoCount();

		pPlayer = ( CTKMahjongServerPlayer * )GetPlayerBySeat( nSeat ) ;	// 和牌的玩家
		pPlayer->AddHandTile( pTile ) ;	// 把这张牌加入手中
//		pPlayer->AddWinCount();
	}

	pPlayer = ( CTKMahjongServerPlayer * )GetPlayerBySeat( nSeat ) ;	// 和牌的玩家
	pPlayer->AddWinCount();
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
bool CTKMahjongServerObj::PlayerTrustPlay( int nSeat, int nTrustType )
{
	CTKMahjongPlayer * pPlayer = GetPlayerBySeat( nSeat );
	if ( pPlayer->HasState( ST_TRUSTPLAY ) )
	{
		return false;
	}

	pPlayer->SetState( ST_TRUSTPLAY, nTrustType );

	return true;
}

void CTKMahjongServerObj::CancelTrustPlay(int nSeat)
{
	CTKMahjongPlayer * pPlayer = GetPlayerBySeat(nSeat);
	pPlayer->ClearState(ST_TRUSTPLAY);
}





// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
IJudge * CTKMahjongServerObj::CreateJudge( int nMahjongType )
{
	switch( nMahjongType )
	{
	case MAHJONG_TYPE_GB:
	case MAHJONG_TYPE_PUBLIC:
	case MAHJONG_TYPE_TWO: /*FOR TWO PLAYER*/
	case MAHJONG_TYPE_JSPK: /*FOR TWO PLAYER*/
		return new CPublicJudge;
	default:
		break;
	}

	return NULL;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
ITileWall * CTKMahjongServerObj::CreateTileWall( int nMahjongType )
{
	switch( nMahjongType )
	{
	case MAHJONG_TYPE_GB:
	case MAHJONG_TYPE_PUBLIC:
	case MAHJONG_TYPE_TWO: /*FOR TWO PLAYER*/
	case MAHJONG_TYPE_JSPK: /*FOR TWO PLAYER*/
		return new CPublicTileWall;
	default:
		break;
	}

	return NULL;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CTKMahjongServerObj::AddWinInfo( TKACKMAHJONGWINDETAILEX & win )
{
	m_vecWinInfo.push_back( win );
}




// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
int CTKMahjongServerObj::GetWinInfo( TKACKMAHJONGWINDETAILEX asWin[] )
{
	WININFOVECTOR::iterator it;
	int cnWin = 0;
	for ( it = m_vecWinInfo.begin(); it != m_vecWinInfo.end(); ++it )
	{
		asWin[ cnWin ] = *it;
		cnWin++;
	}

	return cnWin;
}

bool CTKMahjongServerObj::ClearWinInfo()
{
	m_vecWinInfo.clear();
	return true;
}


void CTKMahjongServerObj::CheckCall( int seat )
{
	if ( seat < 0 || seat > 1)
	{
		TKWriteLog( "%s : %d, Error! 验听时座位号不对, seat( %d )", __FILE__, __LINE__, seat );
		return;
	}
	m_vsCheckCallInfo[seat].clear();

	CHECKPARAM sCheckParam = { 0 };
	// 由Player类填充一部分
	CTKMahjongPlayer * pPlayer = GetPlayerBySeat( seat );
	pPlayer->FillCheckParam( &sCheckParam );

	// 还需要填充和牌方式，门风，所需最小番数
	sCheckParam.nQuanWind = ( COLOR_WIND << 8 ) + ( m_nRoundWind << 4 );
	sCheckParam.nMinFan = 0;

	// 听牌时不能计自摸的番
	// 如果是大众麻将，要加上天听和立直，否则有可能不够番而不能听
	if ( pPlayer->Seat() == Banker() && m_abTian[Banker()] ) // 庄家没出过牌，请求庄家天听
	{
		// 但要计天听的番和立直的番
		sCheckParam.nWinMode |= WIN_MODE_TIANTING;
		//		sCheckParam.nWinMode |= WIN_MODE_LIZHI;
	}
	sCheckParam.nWinMode &= ~WIN_MODE_ZIMO;
	m_pJudge->CheckTing( sCheckParam, m_vsCheckCallInfo[seat] );
}

bool CTKMahjongServerObj::SetCallInfo
(
    int in_nSeat, 
    int in_nTakeoutTileID, 
    int in_nCallType
)
{
    CTKMahjongServerPlayer * pPlayer = (CTKMahjongServerPlayer*)GetPlayerBySeat( in_nSeat );
    if (!pPlayer)
    {
        assert(false);
        return false;
    }

    if (m_vsCheckCallInfo[in_nSeat].empty())
    {
        ASSERT(false);
        TKWriteLog( "ywx %s : %d, Error! CTKMahjongServerObj::SetCallInfo, seat( %d )，听牌错误！", 
            __FILE__, __LINE__, in_nSeat );

        return false;
    }
    else
    {
        for (int i=0; i<m_vsCheckCallInfo[in_nSeat].size(); ++i)
        {
            CALLINFO sCallInfo = m_vsCheckCallInfo[in_nSeat][i];

            if (in_nTakeoutTileID == sCallInfo.nDiscardTileID ||
                in_nTakeoutTileID == 0 ||
                sCallInfo.nDiscardTileID == 0xcccc)
            {
                int nCallTile[34] = {0};
                for (int j=0; j<sCallInfo.cnCallTile; ++j)
                {
                    nCallTile[j] = sCallInfo.asCallTileInfo[j].nCallTileID;
                }

                pPlayer->SetCallInfo(sCallInfo.cnCallTile, nCallTile);

                return true;
            }
        }
    }

    return false;
}

// 听牌总张数：1.所听牌index（4 - m_show[]-m_player1[]-m_player2[] ） 2.相加
int CTKMahjongServerObj::GetLeftTileCount( int nTileID )
{
	ASSERT( nTileID != 0 );

	int nIndex = GetTileValue( nTileID );
	ASSERT( nIndex >= 0 && nIndex < 34 );

	int cnLeftTile = 4 - m_acnShowTile[nIndex] - m_acnHandTile[0][nIndex] - m_acnHandTile[1][nIndex];
	if ( cnLeftTile < 0 )
	{
		cnLeftTile = 0;
	}

	return cnLeftTile;
}

void CTKMahjongServerObj::CountHandTile( int seat )
{
	if ( seat < 0 || seat >= MAX_PLAYER_COUNT || !IsTwoTing() || (!m_pGameRule->GetRule( RULE_DOUBLE_COUNT )))
	{
		return;
	}

	CTKMahjongServerPlayer * pPlayer = ( CTKMahjongServerPlayer *) GetPlayerBySeat( seat );
	if ( NULL == pPlayer ) return;

	memset( m_acnHandTile[seat], 0, sizeof( m_acnHandTile[seat] ) );
	pPlayer->CountHandTile( m_acnHandTile[seat] );
}

bool CTKMahjongServerObj::IsTwoTing()
{
	CTKMahjongPlayer *player = NULL;
	for ( int i=0; i<MAX_PLAYER_COUNT; i++ )
	{
		player = GetPlayerBySeat( i ) ;
		if ( NULL == player || !player->HasCalled() ) return false;
	}

	return true;
}

void CTKMahjongServerObj::AckDouble( int seat )
{
	if ( seat < 0 || seat > 1 ) return;

	m_dblAllNum[seat]++;
	if ( IsTwoTing() )
	{
		int opSeat	= 1 - seat;
		int tingNum	= -1;
		GetTingNum( opSeat, tingNum );
		if ( -1 == tingNum )
		{
			TKWriteLog( "%s : %d, Error! 对家张数获取错误", __FILE__, __LINE__ );
		}
		else if ( 1 == tingNum )
		{
			m_dblNumOne[seat]++;
		}
		else if ( 0 == tingNum )
		{
			m_dblNumZero[seat]++;
		}
	}
}

bool CTKMahjongServerObj::CheckCanDouble( int seat )
{
	if ( seat < 0 || seat > 1 )
	{
		return false;
	}

	bool canDouble = true;
	if ( !m_pMahjongGame->Rule()->GetRule( RULE_DOUBLE_COUNT ) )
	{
		canDouble = false;
	}
	else
	{
		if ( IsTwoTing( ) )
		{ 
			int opSeat = 1 - seat;
			int tingNum = -1;
			bool calSuccess = GetTingNum( opSeat, tingNum );
			if ( !calSuccess ) return false;

			// seat 发来加倍消息
			{ // 当两家都报停(次数置0)，对家0张1次，1张2次，>1张64倍
				if ( tingNum <= 0 )
				{
					if ( m_dblNumZero[seat] >= 1 )
					{
						canDouble = false;
					}
				}
				else if ( tingNum == 1 )
				{
					if ( m_dblNumOne[seat] >= 2 )
					{
						canDouble = false;
					}
				}
			}
		}

        int maxDbl = m_pMahjongGame->Rule()->GetRule(RULE_DOUBLE_COUNT);
		if ( maxDbl > 0 && m_dblAllNum[seat] >= maxDbl )
		{
			canDouble = false;
		}
	}

	return canDouble;
}

bool CTKMahjongServerObj::GetTingNum( int seat, int &tingNum )
{
	if ( seat < 0 || seat > 1 ) return false;

	CALLINFOVECTOR * pvsCallInfo = NULL;
	CheckCallResult( seat, &pvsCallInfo );
	/*if( pvsCallInfo->size() != 1 ) return false;
	CALLINFO &sCallInfo = pvsCallInfo->front();*/
	if (pvsCallInfo->size() <= 0)
	{
		return false;
	}

	int i = 0;
	if( pvsCallInfo->size() == 1 )
	{ // 上家先听 或 和一种牌
		i = 0;
	}
	else
	{ // 自己先听
		if ( m_nLastCallDisID[seat] <= 0 ) return false;
		for( ; i < pvsCallInfo->size(); i++ )
		{
			if ( pvsCallInfo->at( i ).nDiscardTileID == m_nLastCallDisID[seat] )
			{
				// 就是这个
				break;
			}
		}
		if (i >= pvsCallInfo->size())
		{
			return false;
		}
	}

	CALLINFO &sCallInfo = pvsCallInfo->at( i );

	int allNum = 0;
	for( int i=0; i<sCallInfo.cnCallTile; i++ )
	{
		allNum += GetLeftTileCount( sCallInfo.asCallTileInfo[i].nCallTileID );
	}

	tingNum = allNum;

	return true;
}

void CTKMahjongServerObj::CheckCallResult( int seat, CALLINFOVECTOR ** pvsCheckCallInfo )
{
	if ( seat < 0 || seat > 1)	return;

	if ( pvsCheckCallInfo != NULL )
	{
		*pvsCheckCallInfo = &m_vsCheckCallInfo[seat];
	}
}

int CTKMahjongServerObj::GetAllTingCount( int seat )
{
	if ( seat < 0 || seat >= MAX_PLAYER_COUNT )
	{
		return 0;
	}

	return m_dblAllNum[seat];
}

void CTKMahjongServerObj::MemCallDisID( int seat, int id )
 {
	 if ( seat < 0 || seat >= MAX_PLAYER_COUNT || id <= 0 )  return;

	 m_nLastCallDisID[seat] = id;
 }

void CTKMahjongServerObj::SendDblTrip( int seat )
{
	// 该玩家刚加完倍
	CTKGamePlayer* player = m_pMahjongGame->GetGamePlayerBySeat(seat);
	if ( !player ) return;

	int opSeat = 1 - seat;

	if ( IsTwoTing() )
	{
		int tingNum = -1;
		bool calSuccess = GetTingNum( opSeat, tingNum );
		if ( !calSuccess ) return;
		
		if ( tingNum <= 0 )
		{
			if( player )
				player->SendTipMessage(TK_TIPMESSAGETYPE_TABLESCROLLMESSAGE, "对方死胡，您只可加倍一次，下次将自动和牌");
			return;
		}
	}
	
	if ( TK_GAME_SCORETYPE_CHIP == m_pMahjongGame->m_nScoreType
			||TK_GAME_SCORETYPE_LIFE ==  m_pMahjongGame->m_nScoreType )
	{
		int myDbl = 0;
		int scoreTotalMe = 0;
		int scoreTotalUp = 0;
		int maxWin		 = 0;

		CTKMahjongServerPlayer* playerMe = (CTKMahjongServerPlayer*) GetPlayerBySeat( seat );
		CTKMahjongServerPlayer* playerUp =  (CTKMahjongServerPlayer*) GetPlayerBySeat( opSeat );
		if ( playerMe && playerUp )
		{
			myDbl = GetAllTingCount( seat );
			scoreTotalMe = playerMe->TotalScore();
			scoreTotalUp = playerUp->TotalScore();

			maxWin = m_pMahjongGame->m_nScoreBase * (int)pow(2.0f, myDbl) * playerMe->WinInfo().nMaxFans;
			if ( maxWin >= scoreTotalMe )
			{
				player->SendTipMessage(TK_TIPMESSAGETYPE_TABLESCROLLMESSAGE, "您携带的筹码量不足，不能再次加倍");
			}
			else if ( maxWin >= scoreTotalUp )
			{
				player->SendTipMessage(TK_TIPMESSAGETYPE_TABLESCROLLMESSAGE, "对方携带的筹码量不足，不能再次加倍");
			}
		}
	}
}


void CTKMahjongServerObj::ResetDoubleData()
{
	memset( m_dblNumOne, 0, sizeof(m_dblNumOne) );
	memset( m_dblNumZero, 0, sizeof(m_dblNumZero) );
	memset( m_dblAllNum, 0, sizeof(m_dblAllNum) );

	memset( m_acnHandTile, 0, sizeof(m_acnHandTile) );
	memset( m_nLastCallDisID, 0, sizeof(m_nLastCallDisID) );
}

BOOL CTKMahjongServerObj::OnResetDump()
{
	// 四家的基本信息、牌、状态（听牌、托管）
	for ( int i = 0; i < MAX_PLAYER_COUNT; ++i )
	{
		if ( NULL != m_apPlayer[ i ] )
		{
			m_apPlayer[ i ]->OnResetDump();
		}
	}
	return TRUE;
}

bool CTKMahjongServerObj::istracedreshuchance() const
{
	if (m_pMahjongGame)
	{
		int chance = TKGenRandom() % 100;
		if ((chance<m_pMahjongGame->m_nTracedUserReShChance) && (m_pMahjongGame->m_nTracedUserReShChance>0))
		{
			return true;
		}
	}
	return false;
}

// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CTKMahjongServerObj::ResetCallInfo()
{
	for (int i=0; i<MAX_PLAYER_COUNT; i++)
	{
		CTKMahjongServerPlayer * pPlayer = (CTKMahjongServerPlayer*)GetPlayerBySeat( i );
		if (pPlayer)
			pPlayer->ResetCallInfo();
	}

	m_bCheckCallTile = false;
	m_nSpecialTileID = 0;
}


//判断玩家是否可以和牌
bool CTKMahjongServerObj::CheckCanWin( PTKREQMAHJONGWIN pReqMahJongWin, int nSeat, int nPaoSeat, CMahJongTile* nTile )
{
	CHECKPARAM pCheckParam = {0};
	CTKMahjongPlayer* pPlayer = GetPlayerBySeat(nSeat);
	if (pPlayer)
	{
		pPlayer->FillCheckParam(&pCheckParam);
		if (nSeat != nPaoSeat)
		{
			nTile->FillTileInfo(pCheckParam.asHandStone);
			pCheckParam.cnHandStone++;
		}
		// 还需要填充和牌方式，门风，所需最小番数
		pCheckParam.nQuanWind = (COLOR_WIND << 8) + (m_nRoundWind << 4);
		pCheckParam.nMinFan   = 0;
	}

	WININFO CheckWinInfo;
	CHECKRESULT CheckWinResult = m_pJudge->CheckWin(pCheckParam, CheckWinInfo);
	CheckWinInfo.nHuTileID = nTile->ID();
	//
	CRequestObj * pRequestObj = m_pMahjongGame->RequestObj();
	pReqMahJongWin->header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_WIN ) ;
	pReqMahJongWin->header.dwLength = MSG_LENGTH( TKREQMAHJONGWIN ) ;
	pReqMahJongWin->nRequestID      = pRequestObj->RequestID() ;
	pReqMahJongWin->nSeat           = nSeat ;
	pReqMahJongWin->nPaoSeat        = nPaoSeat ;
	pReqMahJongWin->nTileID         = nTile->ID() ;

	if( CheckWinResult == T_OK )
	{ // 能和
		pReqMahJongWin->nResultant = CheckWinInfo.nResultant;
		pReqMahJongWin->cnGroups   = CheckWinInfo.cnGroups ;
		memcpy( pReqMahJongWin->asGroup , CheckWinInfo.asGroup , sizeof( pReqMahJongWin->asGroup ) ) ;
		memcpy( pReqMahJongWin->asHandTile, CheckWinInfo.asHandStone, sizeof( pReqMahJongWin->asHandTile ) ) ;
		pReqMahJongWin->cnShowGroups = CheckWinInfo.cnShowGroups ;

		return true;
	}

	return false;
}

bool CTKMahjongServerObj::CheckCanZimo(const int& nSeat, const int& nTile)
{
	CHECKPARAM sCheckParam = {0};
	CTKMahjongPlayer* pPlayer = GetPlayerBySeat(nSeat);
	if (pPlayer)
	{
		pPlayer->FillCheckParam(&sCheckParam);
		// 还需要填充和牌方式，门风，所需最小番数
		sCheckParam.nQuanWind = (COLOR_WIND << 8) + (m_nRoundWind << 4);
		sCheckParam.nMinFan   = 0;
		sCheckParam.asHandStone[0].nID    = nTile;
		sCheckParam.asHandStone[0].nColor = TILE_COLOR(nTile);
		sCheckParam.asHandStone[0].nWhat  = TILE_WHAT(nTile);
		sCheckParam.cnHandStone++;
	}

	WININFO CheckWinInfo = {0};
	CHECKRESULT CheckWinResult = m_pJudge->CheckWin(sCheckParam, CheckWinInfo);
	if (CheckWinResult == T_OK)
	{ // 能和
		return true;
	}

	return false;
}
// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
// 检测用户是否还有可胡的牌
bool CTKMahjongServerObj::CheckCallTile()
{
	int nAllTile[34] = {0};
	for (int i=0; i<34; i++)
	{
		nAllTile[i] = m_acnShowTile[i] + m_acnHandTile[0][i] + m_acnHandTile[1][i];
	}
	int nAllPlayerCallTile[34] = {0};
	int nAllPlayerCallTileCount[2] = {0};
	for (int i=0; i<MAX_PLAYER_COUNT; i++)
	{
		CTKMahjongServerPlayer* pPlayer = (CTKMahjongServerPlayer *)GetPlayerBySeat(i);
		nAllPlayerCallTileCount[i] = pPlayer->FillCallTile(nAllPlayerCallTile);
	}

	if (nAllPlayerCallTileCount[0] == 0 || nAllPlayerCallTileCount[1] == 0)
	{
		return true;
	}

    bool bFound = false;
	for (int i=0; i<34; i++)
	{
		if (nAllPlayerCallTile[i] > 0 && nAllTile[i] < 4)
		{
			return true;

            bFound = true;

            if (i > 26)
            {// 风
				char temp[100] = { 0 };
                sprintf_s(temp, "ywx, CheckCallTile，what：%d风，剩余%d张\n", i-26, 4-nAllTile[i]);
                OutputDebugString(temp);
            }
            else if (i > 8)
            {
				char temp[100] = { 0 };
                sprintf_s(temp, "ywx, CheckCallTile，what：%d条，剩余%d张\n", i-8, 4-nAllTile[i]);
                OutputDebugString(temp);
            }
		}
	}

    if (bFound)
    {
        return true;
    }

	return false;
}

// 发送加番牌信息
void CTKMahjongServerObj::SendSpecialTileInfo()
{
	// 发送加番牌信息
	TKACKMAHJONGSPECIALTILE ackSpecialTile = { 0 };
	ackSpecialTile.header.dwType = ACK_TYPE( TK_MSG_MAHJONG_SPECIALTILE );
	ackSpecialTile.header.dwLength = MSG_LENGTH( TKACKMAHJONGSPECIALTILE );

	int nRndCount = 42, nRndOffset = 0;
	int rndRate = TKGenRandom() % nRndCount + nRndOffset;
	while (true)
	{
		if (m_apTileCountInfo[rndRate].nTotal == 4)
		{
			break;
		}
		rndRate = TKGenRandom() % nRndCount + nRndOffset;
	}

	MAHJONGTILECOUNTINFO mtc = m_apTileCountInfo[rndRate];
	m_nSpecialTileID = TILE_ID(mtc.nColor, mtc.nWhat, 1);

	ackSpecialTile.nSpecialTileID = m_nSpecialTileID;
	ackSpecialTile.nSpecialTileFan = m_pGameRule->GetRule(RULE_SPECIALTILE_FAN);
	ackSpecialTile.nSpecialTileCount = 4;

	if ( !Broadcast( ( PTKHEADER )&ackSpecialTile ) )
	{
		TKWriteLog( "%s : %d, Error! SetPlace() 发送消息出错 长度( %d )", __FILE__, __LINE__, ackSpecialTile.header.dwLength );
	}

	// 记录加番牌
	DWORD dwDifMs = m_pMahjongGame->GetRtValue();
	CTKBuffer bufGameAction;
	bufGameAction.AppendFormatText("<a id=\"%d\" rt=\"%d\" rtms=\"%d\" sti=\"%d\" stf=\"%d\" stc=\"%d\" />", 
		RECORD_SPECIALTILE,
		dwDifMs / 1000,
		dwDifMs,
		ackSpecialTile.nSpecialTileID, 
		ackSpecialTile.nSpecialTileFan, 
		ackSpecialTile.nSpecialTileCount);
	m_pMahjongGame->AddGameAction( bufGameAction.GetBufPtr() );
}


bool CTKMahjongServerObj::CheckBigFlowerAward(int& nWaitSecond)
{
	bool bBigFlag = false;
    if (0 == m_pGameRule->GetRule(RULE_CF_COUNT))
    {
        return bBigFlag;
    }

    for (int nSeat = 0;nSeat < MAX_PLAYER_COUNT;++nSeat)
    {
        CTKMahjongPlayer * pPlayer = GetPlayerBySeat(nSeat);
        if (NULL == pPlayer)
        {
            continue;
        }
        int nCount = pPlayer->GetFlowerCount();
        if (nCount != m_pGameRule->GetRule(RULE_CF_COUNT))
        {
            continue;
        }
        _sendBigFlowerAward(nSeat, m_pGameRule->GetRule(RULE_CF_GAMEOVER));
        _awardToPlayer(nSeat, m_pGameRule->GetRule(RULE_CF_WARETYPE), m_pGameRule->GetRule(RULE_CF_WARECOUNT));
        _awardToPlayer(1 - nSeat, m_pGameRule->GetRule(RULE_CFO_WARETYPE), m_pGameRule->GetRule(RULE_CFO_WARECOUNT));
        _sendBigFlowerLamp(nSeat);
        nWaitSecond = 15;
		bBigFlag = true;
    }
	return bBigFlag;
}

bool CTKMahjongServerObj::CheckFullBloomAward(int & nWaitSecond)
{
	bool bFullBloomFlag = false;
	if (0 == m_pGameRule->GetRule(RULE_FB_COUNT))
	{
		return bFullBloomFlag;
	}

	for (int nSeat = 0; nSeat < MAX_PLAYER_COUNT; ++nSeat)
	{
		CTKMahjongPlayer * pPlayer = GetPlayerBySeat(nSeat);
		if (NULL == pPlayer)
		{
			continue;
		}
		int nCount = pPlayer->GetFlowerCount();
		if (nCount != m_pGameRule->GetRule(RULE_FB_COUNT))
		{
			continue;
		}
		_sendFullBloomAward(nSeat);
		_awardToPlayer(nSeat, m_pGameRule->GetRule(RULE_FB_WARETYPE), m_pGameRule->GetRule(RULE_FB_WARECOUNT));
		_awardToPlayer(1 - nSeat, m_pGameRule->GetRule(RULE_FBO_WARETYPE), m_pGameRule->GetRule(RULE_FBO_WARECOUNT));
		_sendFullBloomLamp(nSeat);
		nWaitSecond = 15;
		bFullBloomFlag = true;
	}
	return bFullBloomFlag;
}

void CTKMahjongServerObj::_awardToPlayer(int nSeat, int nType, int nCount)
{
    CTKGamePlayer *pGamePlayer = m_pMahjongGame->GetGamePlayerBySeat(nSeat);
    CTKMahjongPlayer * pPlayer = GetPlayerBySeat(nSeat);
    if (GOLD == nType)
    {
        pPlayer->AwardGold(pGamePlayer, nCount);
    }
    else
    {
        pPlayer->AwardWare(pGamePlayer, nType, nCount);
    }
}

void CTKMahjongServerObj::_sendBigFlowerLamp(int nSeat)
{
    CTKGamePlayer *pGamePlayer = m_pMahjongGame->GetGamePlayerBySeat(nSeat);
    TKMobileAckMsg rAck;
    MahJongJSPKAckMsg *pMahJong = rAck.mutable_mahjongjspk_ack_msg();
    pMahJong->set_matchid(pGamePlayer->m_dwMatchID);
    BigFlowerLampAck *pMsg = pMahJong->mutable_bigflowerlamp_ack_msg();
    pMsg->set_nickname(GBKToUTF8(pGamePlayer->m_szNickName));
    pMsg->set_productid(pGamePlayer->m_nProductID);
    pMsg->set_awardtype(m_pGameRule->GetRule(RULE_CF_WARETYPE));
    pMsg->set_awardcount(m_pGameRule->GetRule(RULE_CF_WARECOUNT));
    gtk_pMahjongJSPKService->Broadcast(rAck);
}

void CTKMahjongServerObj::_sendBigFlowerAward(int nSeat, int nIsOver)
{
    TKMobileAckMsg rAck;
    MahJongJSPKAckMsg *pMahJong = rAck.mutable_mahjongjspk_ack_msg();
    BigFlowerAwardAck *pMsg = pMahJong->mutable_bigfloweraward_ack_msg();
    pMsg->set_seat(nSeat);
	pMsg->set_isover(nIsOver);
    m_pMahjongGame->SendMsg(rAck);
}

void CTKMahjongServerObj::_sendFullBloomLamp(int nSeat)
{
	CTKGamePlayer *pGamePlayer = m_pMahjongGame->GetGamePlayerBySeat(nSeat);
	TKMobileAckMsg rAck;
	MahJongJSPKAckMsg *pMahJong = rAck.mutable_mahjongjspk_ack_msg();
	pMahJong->set_matchid(pGamePlayer->m_dwMatchID);
	FullBloomLampAck *pMsg = pMahJong->mutable_fullbloomlamp_ack_msg();
	pMsg->set_nickname(GBKToUTF8(pGamePlayer->m_szNickName));
	pMsg->set_productid(pGamePlayer->m_nProductID);
	pMsg->set_awardtype(m_pGameRule->GetRule(RULE_FB_WARETYPE));
	pMsg->set_awardcount(m_pGameRule->GetRule(RULE_FB_WARECOUNT));
	gtk_pMahjongJSPKService->Broadcast(rAck);
}

void CTKMahjongServerObj::_sendFullBloomAward(int nSeat)
{
	TKMobileAckMsg rAck;
	MahJongJSPKAckMsg *pMahJong = rAck.mutable_mahjongjspk_ack_msg();
	FullBloomAwardAck *pMsg = pMahJong->mutable_fullbloomaward_ack_msg();
	pMsg->set_seat(nSeat);
	m_pMahjongGame->SendMsg(rAck);
}

// 获取加番牌个数
int CTKMahjongServerObj::GetSpecialCount(CHECKPARAM &sCheckParam)
{
    if (m_pGameRule->GetRule(RULE_SPECIALTILE_FAN) == 0)
    {
        return 0;
    }
    int nSpecialTileCount = 0;
    for (int i = 0; i < sCheckParam.cnHandStone; i++)
    {
        if (CMahJongTile::SameTile(m_nSpecialTileID, sCheckParam.asHandStone[i].nID))
        {
            nSpecialTileCount++;
        }
    }
    for (int i = 0; i < sCheckParam.cnShowGroups; i++)
    {
        STONEGROUP sg = sCheckParam.asShowGroup[i];
        for (int j = 0; j < 4; j++)
        {
            STONE st = sg.asStone[j];
            if (CMahJongTile::SameTile(m_nSpecialTileID, st.nID))
            {
                nSpecialTileCount++;
            }
        }
    }
    return nSpecialTileCount;
}

// 发送奖花消息
void CTKMahjongServerObj::SendLuckyTileInfo(PTKACKMAHJONGLUCKYTILE pAckMsg)
{
	if ( !Broadcast( ( PTKHEADER )pAckMsg ) )
	{
		TKWriteLog( "%s : %d, Error! SetPlace() 发送消息出错 长度( %d )", __FILE__, __LINE__, pAckMsg->header.dwLength );
	}
}


// 发送奖花消息
int CTKMahjongServerObj::GetLuckyTile(TKACKMAHJONGLUCKYTILE & stLuckyTile)
{
	int nLuckyTileCount = m_pGameRule->GetRule(RULE_LUCKY_TILE_COUNT);

	for (int i = 0; i < nLuckyTileCount; i++)
	{
		if (m_pTileWall->Empty())
		{
			break;
		}

        // 先随机的选一张牌
        PMAHJONGTILECOUNTINFO pTileInfo = RandomSelectOneLeftTile();
        if (NULL == pTileInfo)
        {
            break;
        }

        int nTileID = TILE_ID(pTileInfo->nColor, pTileInfo->nWhat, pTileInfo->vUsed[pTileInfo->vUsed.size() - 1]);	// 这张牌的ID号
		
		// 从牌墙中拿出这张牌
		CMahJongTile *pTile = GetOneTileFromWall( true , 0 ) ;
		if (NULL == pTile)
		{
			break;
		}
		pTile->SetID( nTileID ) ;	// 设置这张牌的ID号

		// 更新消息内容
		stLuckyTile.anLuckyTileID[i] = pTile->ID();
		stLuckyTile.nLuckyTileCount++;

		// 判断有没有中奖
		CTKMahjongPlayer *pPlayer = NULL;
		for ( int i=0; i<MAX_PLAYER_COUNT; i++ )
		{
			pPlayer = GetPlayerBySeat( i ) ;
			if (pPlayer && pPlayer->WinCount() > 0) // 胡牌玩家
			{
				if (pPlayer->CheckLuckyTile(pTile->ID())) // 命中
				{
					stLuckyTile.anHitTileID[stLuckyTile.nHitTileCount++] = pTile->ID();
					break;
				}				
			}
		}
	}

	// 奖花牌番数
    stLuckyTile.nEachLuckyTileFan = m_pGameRule->GetLuckTileFan(stLuckyTile.nLuckyTileCount);

	return stLuckyTile.nLuckyTileCount;
}

