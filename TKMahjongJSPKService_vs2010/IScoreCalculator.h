#pragma once


//#include <windows.h>
#include "tkMahJongProtocol.h"	// MAX_PLAYER_COUNT，GLREQMAHJONGRESULT
#include <TKGame.h>

// #pragma warning (disable: 4786)	// 禁止VC对类型名过长的警告
// #include <vector>
// using namespace std ;

// 得分类型
#define SCORE_TYPE_BASE				(  0 )	// 基本分
#define SCORE_TYPE_WIN				(  1 )	// 和牌的分
#define SCORE_TYPE_WIN_FAIL			(  2 )	// 诈和扣分
#define SCORE_TYPE_ZHI_GANG			(  3 )	// 直杠明杠
#define SCORE_TYPE_BU_GANG			(  4 )	// 补杠明杠
#define SCORE_TYPE_AN_GANG			(  5 )	// 暗杠
#define SCORE_TYPE_HUN_GANG			(  6 )	// 混杠
#define SCORE_TYPE_DICE_POINT		(  7 )	// 掷骰子相同点数
#define SCORE_TYPE_DICE_RED			(  8 )	// 掷骰子都是红色
#define SCORE_TYPE_DICE_SAME_RED	(  9 )	// 掷骰子相同点数的红色
#define SCORE_TYPE_PLAY_AWARD		( 10 )	// 这一盘的彩金
#define SCORE_TYPE_PLAY_TASK		( 11 )	// 这一盘的任务（吃碰杠某些组合或者连续吃碰杠）

// 一次得分信息
typedef struct tagOneTimeScore
{
	int nType ;							// 得分类型
	int nSeat;							// 加分(或减分)的玩家座位号
	int nStyle ;						// 1－只有一个玩家加分，2－只有一个玩家减分，3－有输有赢，总和为0
	int pnScore[ MAX_PLAYER_COUNT ] ;	// 这次各个玩家的得分（下标是玩家座位号）
} ONETIMESCORE , *PONETIMESCORE ;

class CGameRule;
class IScoreCalculator
{
public:
	// 构造函数
	IScoreCalculator();
	// 析构函数
	virtual ~IScoreCalculator();

public:
	// 初始化分数计算子
	BOOL InitInstance
    ( 
        CGameRule* pRule, 
        int nPlayerCount, 
        int nMulti,
        CTKGame* in_pTKGame
    );

	// 重置分数信息
	void ResetData() ;

	// 这时这一盘的玩家分数信息
	/// pnPlayerScore是这一盘开始时，各个玩家的总分；pnPlayerMulti是这一盘开始时，各个玩家的加倍
	/// 每一盘都应该重置分数的信息
	void SetPlayerScore( int *pnPlayerScore , int *pnPlayerMulti ) ;

	// 设置玩家剩下的分数
	int AddPlayerScore( int nSeat, int nScore );

	// 设置各个类型的得分
	/// nType是分数的类型，nScore是分数，这个分数总是一个正数
	void SetScore( int nType , int nScore ) ;
	// 获得各个类型的得分（包括倍分）
	int GetScore( int nType ) ;
	// 和牌时确定加倍基数
	void SetDouble( int cnDouble = 0 );
public:
	// 添加一个得分信息（不包括基本分和和牌分）
	/// nType是分数类型
	/// nAddScoreSeat是加分的玩家座位号
	/// nDecScoreSeat是减分的玩家座位号,如果为-1表示除加分者外所有人都减
	virtual void AddScore( int nType , int nAddScoreSeat , int nDecScoreSeat ) ;

	// 添加和牌分（基本分也在这个函数中添加）
	virtual void AddScore( TKACKMAHJONGWINDETAIL & reqMahjongWin, int anShowSeat[], int cnShowSeat );

	// 添加和牌分（基本分也在这个函数中添加）
	virtual void AddScore( TKACKMAHJONGWINDETAILEX & reqMahjongWin, int anShowSeat[], int cnShowSeat ){};

	// 获得这一盘各个玩家的得分
	virtual void GetPlayerScore( TKREQMAHJONGRESULT & reqMahJongResult ){};
	virtual int GetPlayerScore( int nSeat ){ return m_pnPlayerLeftScore[ nSeat ] - m_pnPlayerScore[ nSeat ]; }

	// 获得初始得分
	virtual int GetPlayerBeginScore(int nSeat) {return m_pnPlayerScore[nSeat];}
	// 获得剩余得分
	virtual int GetPlayerLeftScoer(int nSeat) {return m_pnPlayerLeftScore[nSeat];}
	// 实时修改玩家得分
	virtual void ChangeRTScore(int nWinSeat, int aScores[]);
protected:
	// 初始化,派生类可重载
	virtual BOOL OnInitInstance( CGameRule * pRule , int nPlayerCount , int nMulti ){ return true; }

	// 重置分数信息,派生类可重载
	virtual void OnResetData(){}

	// 删除某种类型的分数信息（必须是三项都要符合条件才删除）
	/// nType是分数类型，如果为-1，表示不考虑类型
	/// nSeat是加分( 或减分 )玩家座位号，如果为-1，表示不考虑这一项
	virtual void DelScore( int nType , int nSeat ) ;

#if defined(DEBUG)||defined(_DEBUG)
	void Trace() ;
#endif

protected:
	// 收尾函数
	BOOL ExitInstance() ;

	// 判断是否是一个合法的类型
	BOOL IsValidType( int nType ) ;
	// 判断是否是一个合法的座位号
	BOOL IsValidSeat( int nSeat ) ;

protected :
	int m_nPlayerCount ;	// 玩家个数
	int m_nMulti ;			// 倍分（这个倍分是有房间决定的，是一番对应多少个游戏豆或者分）
	int m_nDouble;			// 加倍基数(支持加倍功能)

	int m_pnScore[ 16 ] ;	// 各个类型的得分（不包括乘上m_nMulti的值）

	int m_pnPlayerScore[ MAX_PLAYER_COUNT ] ;		// 每盘开始时，玩家的得分
	int m_pnPlayerLeftScore[ MAX_PLAYER_COUNT ] ;	// 每盘计算得分时，各玩家剩下的得分
	int m_pnPlayerMulti[ MAX_PLAYER_COUNT ] ;		// 每盘开始时，玩家的倍数

	typedef vector<ONETIMESCORE> ScoreType ;
	typedef vector<ONETIMESCORE>::iterator ScoreIterator ;
	ScoreType m_Score ;			// 这一盘的得分信息

	CGameRule * m_pRule;
    CTKGame* m_pTKGame;
};