#ifndef _SCORE_CALCULATOR_H
#define _SCORE_CALCULATOR_H

#include "IScoreCalculator.h"

// 得分不够时，欠分的信息
typedef struct tagOweScore
{
	int nLoanee ;	// 债务人
	int nLoaner ;	// 债权人
	int nScore ;	// 欠的分数（这个数字总是一个正数）
} OWESCORE , *POWESCORE ;


// ************************************************************************************************************************
// 
// 分数计算器，这个类主要是用来记录一盘中的分数信息，并最终计算出各家应该各得多少分
// 
// ************************************************************************************************************************
class CScoreCalculator : public IScoreCalculator
{

public :
	// 构造函数
	CScoreCalculator() ;
	// 析构函数
	~CScoreCalculator() ;

	// 获得这一盘各个玩家的得分
	virtual void GetPlayerScore( TKREQMAHJONGRESULT & reqMahJongResult );

	// 添加和牌分（基本分也在这个函数中添加）
	virtual void AddScore( TKACKMAHJONGWINDETAILEX & reqMahjongWin, int anShowSeat[], int cnShowSeat );

protected:
	virtual void OnResetData();

protected :
	// 获得玩家最后一次加入的得分（不能用于获得和牌的得分）
	int GetLastAddScore( int nSeat ) ;
	// 获得玩家这一盘被赠送的得分
	int GetAwardScore( int nSeat ) ;

	// 调整得分
	void AdjustScore( PONETIMESCORE pScore ) ;

private:
	typedef vector<OWESCORE> OweScoreType ;
	typedef vector<OWESCORE>::iterator OweScoreIterator ;
	OweScoreType m_OweScore ;	// 这一盘依次的欠债信息（依次的意思是按照游戏过程中的输赢顺序）
} ;

#endif
