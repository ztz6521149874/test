#include <winsock2.h>
#include <windows.h>

#include "RequestObj.h"	// CRequestObj
#include "assert.h"

#ifndef ASSERT
#define ASSERT	assert
#endif

// *********************************************************************************************************************
// 成员函数的定义
// 


// *********************************************************************************************************************
// 
// 
// 
// *********************************************************************************************************************
CRequestObj::CRequestObj()
{
	m_nRequestID      = 0 ;
	m_nRequestType    = 0 ;
	m_nRequestSeat    = 0 ;
	m_nAckRequestSeat = 0 ;
	m_nWaitSeconds    = 0 ;
	m_dwStartTime     = 0 ;
	m_dwRequestWaitTime = 0;

	memset( m_pnAckRequestType , 0 , sizeof( m_pnAckRequestType ) ) ;

	m_nOKRequestType = 0 ;
}


// *********************************************************************************************************************
// 
// 
// 
// *********************************************************************************************************************
CRequestObj::~CRequestObj()
{
	// 清除所有的消息
	m_listMsg.FreeAll() ;
}


// *********************************************************************************************************************
// 
// 
// 
// *********************************************************************************************************************
void CRequestObj::ResetRequest( int nRequestType , int nWaitSeconds )
{
	m_nRequestType = nRequestType ;
	m_nWaitSeconds = nWaitSeconds ;

	IncRequestID() ;
}


// *********************************************************************************************************************
// 
// 
// 
// *********************************************************************************************************************
void CRequestObj::FillRequestMsg( PTKREQMAHJONGREQUEST pReqMahJongRequest , int nSeat )
{
	pReqMahJongRequest->header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_REQUEST ) ;
	pReqMahJongRequest->header.dwLength = MSG_LENGTH( TKREQMAHJONGREQUEST ) ;
	pReqMahJongRequest->nSeat           = nSeat ;
	pReqMahJongRequest->nRequestID      = m_nRequestID ;
	pReqMahJongRequest->nRequestType    = m_nRequestType ;
	pReqMahJongRequest->nWaitSecond     = m_nWaitSeconds ;

	SetRequestSeat( nSeat ) ;
}



// *********************************************************************************************************************
// 
// 
// 
// *********************************************************************************************************************
void CRequestObj::SetAckQuestSeat( int nSeat , int nRequestType , PTKHEADER pHeader /* = NULL */ )
{
	m_nAckRequestSeat |= ( 1 << nSeat ) ;
	m_pnAckRequestType[ nSeat ] = nRequestType ;

	// 根据当前采取操作的请求类型和这个玩家设置的请求类型，来确定最新的采取操作的请求类型
	switch( m_nOKRequestType )
	{
	case REQUEST_TYPE_CHI :				// 吃
		if( REQUEST_TYPE_PENG == nRequestType || REQUEST_TYPE_GANG == nRequestType || REQUEST_TYPE_WIN == nRequestType )
		{ // 碰、杠、和
			m_nOKRequestType = nRequestType ;
		}
		break ;

	case REQUEST_TYPE_PENG :			// 碰
	case REQUEST_TYPE_GANG :			// 杠
		if( REQUEST_TYPE_WIN == nRequestType )
		{ // 和
			m_nOKRequestType = nRequestType ;
		}
		break ;

	case REQUEST_TYPE_CALL :			// 听牌
        TKWriteLog( TEXT( "[%s:%d]seat=%d,type=%d" ) , __FILE__ , __LINE__ , nSeat , nRequestType ) ;
		break ;

	case REQUEST_TYPE_WIN :				// 和牌
		break ;

	case REQUEST_TYPE_DISCARD_TILE :	// 出牌
		if( REQUEST_TYPE_GANG == nRequestType || REQUEST_TYPE_CALL == nRequestType || REQUEST_TYPE_WIN == nRequestType || REQUEST_TYPE_CHANGE_FLOWER == nRequestType )
		{ // 杠、听牌、和牌、补花
			m_nOKRequestType = nRequestType ;
		}
		break ;

	case REQUEST_TYPE_CHANGE_FLOWER :	// 补花
        TKWriteLog( TEXT( "[%s:%d]seat=%d,type=%d" ) , __FILE__ , __LINE__ , nSeat , nRequestType ) ;
		break ;

	default :
		m_nOKRequestType = nRequestType ;
		break ;
	}

	if( NULL == pHeader )
	{ // 没有消息
		ASSERT( REQUEST_TYPE_WIN != nRequestType );
		return ;
	}

	int nMsgLen = pHeader->dwLength + sizeof( TKHEADER ) ;	// 消息的长度

	char* szTemp = ( char* ) malloc( nMsgLen + sizeof( int ) ) ;	// 还有座位号信息
	(*(int*)szTemp) = nSeat ;	// 先把座位号放在消息前
	memcpy( szTemp + sizeof( int ) , pHeader , nMsgLen ) ;
	m_listMsg.AddTail( szTemp ) ;
}



// *********************************************************************************************************************
// 
// 
// 
// *********************************************************************************************************************
int CRequestObj::GetAckMsgSeat( int nType )
{
	int nSeat = -1 ;
	char *szTemp = NULL ;
	
	for( int i = 0 ; i < m_listMsg.GetCount() ; i ++ )
	{ // 遍历这些保存的消息
		szTemp = m_listMsg.GetAt( i ) ;	// 取这个保存的信息
		// 信息先保存的是座位号（int）然后是具体的消息
		nSeat = (*(int*)szTemp ) ;	// 这个玩家的座位号
		if( IsPlayerAckRequestType( nSeat , nType ) )
		{ // 就是这个座位号
			return nSeat ;
		}
	}
	
	return -1 ;
}


// *********************************************************************************************************************
// 
// 
// 
// *********************************************************************************************************************
PTKHEADER CRequestObj::GetPlayerAckMsg( int nSeat )
{
	char *szTemp = NULL ;

	for( int i = 0 ; i < m_listMsg.GetCount() ; i ++ )
	{ // 遍历这些保存的消息
		szTemp = m_listMsg.GetAt( i ) ;	// 取这个保存的信息
		// 信息先保存的是座位号（int）然后是具体的消息
#if _DEBUG
		TKWriteLog("Seat = %d", (*(int*)szTemp));
#endif
        
		if( nSeat == (*(int*)szTemp ) )
		{ // 就是这个座位号
			return ( PTKHEADER ) ( szTemp + sizeof( int ) ) ;	// 返回消息
		}
	}

	return NULL ;
}


// *********************************************************************************************************************
// 
// 
// 
// *********************************************************************************************************************
void CRequestObj::IncRequestID()
{
	m_nRequestID ++ ;

	// 重置请求的一些信息
	m_nRequestSeat    = 0 ;	// 请求的玩家座位号
	m_nAckRequestSeat = 0 ;	// 回复的玩家座位号
	memset( m_pnAckRequestType , 0 , sizeof( m_pnAckRequestType ) ) ;	// 玩家回复的类型
	
	// 
	// 需要处理的回复类型
	// 
	// 可能的请求类型组合为：
	// 1.补花                            补花阶段    要某人补花
	// 2.听牌                            游戏阶段    询问是否天听
	// 3.出牌＋补花＋听牌＋杠牌＋和牌    游戏阶段    某人抓牌后
	// 4.吃牌＋碰牌＋杠牌＋和牌          游戏阶段    某人出牌后
	// 5.和牌                            游戏阶段    某人要杠牌时
	// 6.出牌＋听牌                      游戏阶段    有人吃碰牌后，请他出牌和听牌
	// 
	if( IsRequestType( REQUEST_TYPE_DISCARD_TILE ) )
	{ // 有请求出牌类型
		m_nOKRequestType = REQUEST_TYPE_DISCARD_TILE ;
	}
	else
	{ // 其他请求
		m_nOKRequestType  = REQUEST_TYPE_NULL ;
	}

	// 清除所有的消息
	m_listMsg.FreeAll() ;

	// 获取请求的开始时间
	m_dwStartTime = GetTickCount();
}



// *********************************************************************************************************************
// 
// 
// 
// *********************************************************************************************************************
void CRequestObj::SetRequestSeat( int nSeat )
{
	int n = ( 1 << nSeat ) ;

	m_nRequestSeat |= n ;
	m_nAckRequestSeat &= ~n ;			// 这个玩家还没有回复请求
	m_pnAckRequestType[ nSeat ] = 0 ;	// 这个玩家返回的类型为空
}



// ************************************************************************************************************************
// 检查是否有超时的客户端，如果没有返回-1，有则返回第一个超时的座位号
// 
// 
// ************************************************************************************************************************
int CRequestObj::CheckTime()
{
	if(m_dwRequestWaitTime <= 0)
		return -1;

	DWORD dwCurTime = GetTickCount();

	// 服务器发送请求后的等待时间间隔大于设置的时间上限
	if(dwCurTime - m_dwStartTime >= m_dwRequestWaitTime)
	{
		// 如果此时所有玩家都已回复,返回
		if(AllAckRequest())
			return -1;

		// 查询所有的玩家看是谁没有回复
		for(int i = 0 ; i < MAX_PLAYER_COUNT ; i ++)
		{
			// 如果不是请求的座位号则跳过
			if(!IsRequestSeat(i))
				continue;

			if(!IsAckRequestSeat(i))
			{ // 这个（被请求的）玩家没有回复请求
				return i;
			}
		}
	}
	return -1;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
BOOL CRequestObj::CheckOverTime( int nSeat )
{
	if( m_dwRequestWaitTime <= 0 )
	{
		return FALSE;
	}

	if (IsAckRequestSeat(nSeat))
	{
		return FALSE;
	}

	DWORD dwCurTime = GetTickCount();
	if ( dwCurTime - m_dwStartTime >= m_dwRequestWaitTime )
	{
		// 已经超时了
		if ( IsRequestSeat( nSeat ) )
		{
			// 而且请求这个人了
			return true;
		}
	}

	return FALSE;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
BOOL CRequestObj::CheckOverTime( int nSeat, int nWaitTime)
{
	if( nWaitTime <= 0 )
	{
		return FALSE;
	}

	if (IsAckRequestSeat(nSeat))
	{
		return FALSE;
	}

	DWORD dwCurTime = GetTickCount();
	if ( (int)(dwCurTime - m_dwStartTime) >= (nWaitTime * 1000))
	{
		// 已经超时了
		if ( IsRequestSeat( nSeat ) )
		{
			// 而且请求这个人了
			return true;
		}
	}

	return FALSE;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
BOOL CRequestObj::OnResetDump()
{
	TKWriteLog( "ResetDump : RequestID( %08x ), RequestSeat( %08x ), RequestType( %08x ), AckSeat( %08x ), OKRequestType( %08x ), StartTime( %d ), ResetTime( %d )",
		m_nRequestID, m_nRequestSeat, m_nRequestType, m_nAckRequestSeat, m_nOKRequestType, m_dwStartTime, GetTickCount() );
	for ( int i = 0; i < MAX_PLAYER_COUNT; ++i )
	{
		if ( IsRequestSeat( i ) )
		{
			TKWriteLog( "ResetDump : 请求了Seat( %d )的玩家", i );
			if ( IsAckRequestSeat( i ) )
			{
				TKWriteLog( "ResetDump : Seat( %d )的玩家回复类型( %08x )", i, m_pnAckRequestType[ i ] );
			}
			else
			{
				TKWriteLog( "ResetDump : Seat( %d )的玩家未回复", i );
				char *szTemp = NULL ;
				for( int j = 0 ; j < m_listMsg.GetCount() ; j ++ )
				{ // 遍历这些保存的消息
					szTemp = m_listMsg.GetAt( j ) ;	// 取这个保存的信息
					// 信息先保存的是座位号（int）然后是具体的消息
					if( i == (*(int*)szTemp ) )
					{ // 就是这个座位号
						PTKHEADER pHeader = ( PTKHEADER ) ( szTemp + sizeof( int ) );
						if ( NULL == pHeader )
						{
							TKWriteLog( "ResetDump : Seat( %d )的玩家被标记为未回复，但他确实发送过回复消息，但是消息指针为空", i );
						}
						else
						{
							TKWriteLog( "ResetDump : Seat( %d )的玩家被标记为未回复，但他确实发送过回复消息Type( %08x ), Length( %d )", 
								i, pHeader->dwType, pHeader->dwLength );
						}
						break;
					}
				}
			}
		}
	}

	return TRUE;
}


// 
// 成员函数的定义
// ************************************************************************************************************************


