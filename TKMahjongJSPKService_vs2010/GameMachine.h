#pragma once
#include "fsmachine.h"
#include "TKFixList.h"


// *********************************************************************************************************************
//
//	麻将对象类
//	麻将对象类是一个状态机,它要负责装配状态,设定各状态之间的依存关系
//
// *********************************************************************************************************************
class CTKMahjongGame;
class CTKMahjongServerObj;
class CGameRule;
class CGameMachine : public CFSMachine
{
	friend class IGameState;
public:
	CGameMachine( CTKMahjongGame * pGame, CTKMahjongServerObj * pObj );
	~CGameMachine(void);

public:
	// 初始化
	BOOL OnInitalUpdate( CGameRule * pRule );

	// 停止,直接进入最后一个状态
	BOOL Stop();

	// 添加一个状态
	virtual bool AddState( std::string szStateName, IState * pState );

	BOOL OnResetDump();

public:
	BOOL OnPlayerMsg( DWORD dwUserID, PTKHEADER pMsg );

protected:
	BOOL AssembleState( char * szFSMFile );
	BOOL ParseProperty( char * szProperty );
	int EventString2Int( TCHAR szEvent[] );

protected:
	// 保存一下,有可能用不着
	CTKMahjongGame * m_pMahjongGame;
	CTKMahjongServerObj *	m_pMahjongObj;

	// 游戏规则
	CGameRule * m_pRule;

	IState * m_pFinalState;		// 最终状态,默认AssembleState读入的最后一个状态就是最终状态
};
