#pragma once
#pragma warning (disable: 4267)	// 从“size_t”转换到“int”，可能丢失数据

#include "tkgameplayer.h"
#include "tkmahjongdefine.h"
#include "tkgameprotocol.h"
#include "TKMahJongJSPK.pb.h"

using namespace cn::jj::service::msg::protocol;
enum
{ 
	ST_TRUSTPLAY = 1,
	ST_OFFLINE = 2,
	ST_TRUSTEXIT = 4,
	ST_EXITMATCH = 8
};

// *********************************************************************************************************************
//
//
//
//
// *********************************************************************************************************************
class CMahJongTile;
class CTKMahjongPlayer
{
public:
	CTKMahjongPlayer(void);
	virtual ~CTKMahjongPlayer(void);

	// 重置游戏数据
	virtual void ResetGameData() ;

public:
	int Seat() { return m_nSeatOrder ; }								// 座位号
	int GameSeat() { return m_nGameSeat ; }								// 游戏座位号
	void GameSeat( int n ) { m_nGameSeat = n ; }						// 设置游戏座位号
	int Sex() { return m_nSex; }										// 性别
	int Wind() { return m_nWind ; }										// 门风
	void Wind( int n ) { m_nWind = n ; }								// 设置门风
	int Rules() { return m_nRules ; }									// 行牌规则
	void Rules( int n ) { m_nRules = n ; }								// 设置行牌规则
	int FrustaCount() { return m_nFrustaCount ; }						// 砌牌的墩数
	void FrustaCount( int nCount ){ m_nFrustaCount = nCount ; }			// 设置砌牌的墩数
	BOOL RealHandTile() { return m_bRealHandTile ; }					// 手中牌是否是真实的
	void RealHandTile( BOOL b ) { m_bRealHandTile = b ; }				// 设置手中牌是否是真实的
	int DiscardTileCount() { return m_nDiscardTileCount ; }				// 出牌张数
	CMahJongTile** GetDiscardTile () { return m_apDiscardTile ; }		// 返回出的牌
	DWORD UserID() { return m_pPlayer->m_dwUserID ; }					// 返回用户名
	TCHAR* NickName() { return m_pPlayer->m_szNickName ; }				// 返回用户昵称
	int Score() { return m_nScore; }									// 返回用户积分
	int FigureID() { return m_pPlayer->m_nFigureID; }					// 返回头象ID
	int TotalScore() { return m_nTotalScore; }							// 返回总分
	void Score( int nScore ){ m_nScore = nScore; }						// 设置本局得分
	void TotalScore( int nTotalScore ){ m_nTotalScore = nTotalScore; }	// 设置累计得分

	// 设置听牌类型（1－普通听牌，2－天听，3－相公）
	virtual void SetCallType( int nCallType , BOOL bWinSelf , BOOL bAutoGang ) ;
	BOOL HasCalled() { return ( 1 == m_nCallType ) || ( 2 == m_nCallType ) ; }// 是否听牌
	BOOL IsTianCall() { return ( 2 == m_nCallType ) ; }					// 是否是天听
	BOOL IsXiangGong() { return ( 3 == m_nCallType ) ; }				// 是否是相公
	BOOL IsAutoGang() { return m_bAutoGang; }							// 是否设置了自动杠

	// 设置玩家状态
	/// nState是设置的状态，nParam是和状态相关的参数
	/// nState为ST_START时，nParam为玩家发送的开始消息PGLACKMAHJONGCONTINUE
	/// nState为ST_TRUSTPLAY时，nParam为玩家的托管类型
	void SetState( int nState , int nParam = 0 ) ;
	// 清除玩家状态
	void ClearState( int nState ) ;
	// 查询是否有某个状态
	bool HasState( int nState ) { return ( 0 != ( nState & m_nState ) ) ; }
	void SetBreak( bool isBreak ) { m_isBreak = isBreak; }
	bool IsBreak() { return m_isBreak; }

	bool IsTrustPlayOrOffLine() ;	// 这个玩家是否托管或者断线了

    int GetFlowerCount();
	// 取一张花牌
	/// 如果有花牌，那么返回一张花牌；否则返回NULL
	CMahJongTile* GetOneFlowerTile() ;
	// 从手中牌里面找一张牌
	/// nTileID是这张牌的ID号；如果玩家手中的牌不是真实的，那么就从牌尾（bGetLastTile为TRUE）或者牌头（bGetLastTile为FALSE）取一张牌
	CMahJongTile* FindHandTile( int nTileID , BOOL bGetLastTile ) ;
	// 从出的牌中找一张牌
	CMahJongTile* FindDiscardTile( int nTileID ) ;
	// 从吃碰杠的牌中找一张牌
	CMahJongTile* FindShowTile( int nTileID ) ;
	// 取抓的那张牌
	CMahJongTile* GetDrawTile() { return m_pDrawTile ; }

	// 填充手中牌的信息
	/// pnTile指向的内存块足够填充手中的牌，每张牌是一个int；如果pnTile为NULL，就直接返回手中牌的张数
	/// 返回值是手中牌的张数
	int FillHandTileInfo( int *pnTile ) ;
	// 重置手中牌的ID号
	/// 把手中牌的ID号重置为pnTileID，一共有nTileCount张，抓的那张牌ID号为nDrawTileID
	void ResetHandTileID( int* pnTileID = NULL, int nTileCount = 0, int nDrawTileID = 0 ) ;

	// 填充吃碰杠牌信息
	int FillShowTilesInfo( int *pnTile );


	// 判断手中是否有某张牌
	/// 这个和FindHandTile不同之处是这里必须是要相同ID号才行
	BOOL HasTile( int nTileID ) ;
	// 判断是否已经吃了某个牌
	BOOL HasChi( int nColor , int nWhat ) ;
	// 判断是否已经碰了某个牌
	BOOL HasPeng( int nColor , int nWhat ) ;
	// 判断是否已经杠了某个牌
	BOOL HasGang( int nColor , int nWhat ) ;
	// 判断是否可以吃碰杠和某个玩家的牌
	/// nRequestType取值为REQUEST_TYPE_CHI/REQUEST_TYPE_PENG/REQUEST_TYPE_GANG/REQUEST_TYPE_WIN
	/// nSeat是出牌玩家座位号，判断杠牌或者和牌时，还可能就是这个玩家自己的座位号
	BOOL HasRequestPower( int nRequestType , int nSeat ) ;
	// 判断听牌之后是否可以杠、和牌
	/// nRequestType取值为REQUEST_TYPE_GANG/REQUEST_TYPE_WIN
	/// nParam在判断杠牌时，是杠牌的类型（此参数现在没用到）；在判断和牌时，是放炮的玩家座位号
	BOOL HasCalledPower( int nRequestType , int nParam ) ;

	// 往手中加一张牌
	void AddHandTile( CMahJongTile *pTile ) ;
	// 删除手中的一张牌
	void DeleteHandTile( CMahJongTile *pTile ) ;
	// 从出的牌中删一张牌
	/// 返回这张牌的值
	CMahJongTile* DeleteDiscardTile( int nTileID ) ;
	// 从杠的牌中删除一张牌（删除的这张牌一定是在明杠的牌中，而且一定是某个明杠的最后一张牌）
	/// 返回这张牌的值
	CMahJongTile* DeleteGangTile( int nTileID ) ;

	// 排序手中的牌，返回抓牌位置的牌（如果牌的张数正好是抓牌张数的话）
	CMahJongTile* SortHandTile() ;
	
	// 取得我的算番信息
	void FillCheckParam( CHECKPARAM * pCheckParam ) ;

	// 输出信息到日志文件
	/// nType按位取值（bit0－名字，bit1－手中牌，bit2－吃碰杠的牌，bit3－出的牌，bit4－抓的牌）
	void TraceInfo( int nType = 7 ) ;

	// 检查玩家是否已经和牌了
	/// 如果玩家和牌了，那么返回和牌的WININFO信息，否则返回NULL
	WININFO & WinInfo() { return m_sWinInfo; }
	// 设置玩家的和牌信息
	void WinInfo( WININFO & sWinInfo ) { m_sWinInfo = sWinInfo; }

	// 和牌次数
	int WinCount(){ return m_cnWin; };

	// 杠牌次数
	int GangCount() { return m_cnGang; }

	// 放炮次数
	int PaoCount() { return m_cnPao; }

	// 增加放炮次数
	void AddPaoCount( int nAdd = 1 ) { m_cnPao += nAdd; }
	// 增加和牌次数
	void AddWinCount( int nAdd = 1 ) { m_cnWin += nAdd; }

	// 取得喂吃\碰\杠的玩家
	int * GetShowSeat( int & cnSeat ){ cnSeat = m_cnShowGroupSeat; return m_anShowGroupSeat; }

	BOOL OnResetDump();

    BOOL AwardWare(CTKGamePlayer* pPlayer, int nWareType, int nWareCount);
    BOOL AwardGold(CTKGamePlayer* pPlayer, int nCount);

public:
		// 是否替这个玩家自动玩牌
	/// 对于客户端：主动托管的时候，客户端帮玩家出牌；对于服务器端：超时托管、断线托管、托管退出时，服务器帮玩家出牌
	virtual BOOL IsAutoPlay() ;
	// 取一张自动打出的牌
	virtual CMahJongTile* GetAutoPlayTile() ;
	CMahJongTile* GetAutoPlayTileEx(); // 智能选牌
		CMahJongTile* GetDrawGangTile(); // 若抓的牌成杠则出抓的牌
	
	// 玩家补花
	/// pTile是要补的这张花牌
	/// 【注意】调用基类的这个函数，才会把相应的牌从玩家手中删除
    virtual int ChangeFlower(CMahJongTile* pTile);
	// 玩家抓牌
	/// pTile是要抓的这张牌，nDrawType是抓牌类型（取值为DRAW_TILE_TYPE_XXX）
	/// 【注意】调用基类的这个函数，才会把相应的牌加到玩家手中
	virtual void DrawTile( CMahJongTile* pTile , int nDrawType ) ;
	// 玩家出牌
	/// pTile是要出的这张牌, bHide是否隐藏这张牌的牌面
	/// 【注意】调用基类的这个函数，才会把相应的牌从玩家手中删除
	virtual void DiscardTile( CMahJongTile* pTile, BOOL bHide = FALSE ) ;
	// 玩家吃牌
	/// pTile是吃的这张牌（别人出的那张牌），apChiTile是这吃的三张牌
	/// 基类的这个函数返回这个吃牌所在的分组索引号
	/// 【注意】调用这个函数的时候，相应的牌已经从玩家手中或者出牌队列中删除了
	///			调用基类的这个函数，才会把相应的牌加入到玩家吃碰杠的分组中
	virtual int ChiTile( CMahJongTile* pTile , CMahJongTile* apChiTile[ 3 ], int nDiscardSeat ) ;
	// 玩家碰牌
	/// pTile是碰的这张牌（别人出的那张牌），apPengTile是这碰的三张牌
	/// 基类的这个函数返回这个碰牌所在的分组索引号
	/// 【注意】调用这个函数的时候，相应的牌已经从玩家手中或者出牌队列中删除了
	///			调用基类的这个函数，才会把相应的牌加入到玩家吃碰杠的分组中
	virtual int PengTile( CMahJongTile* pTile , CMahJongTile* apPengTile[ 3 ], int nDiscardSeat ) ;
	// 玩家杠牌
	/// 直杠的明杠：pTile是别人出的那张牌，apGangTile是这要杠的四张牌，nCount为4
	/// 直杠的暗杠：pTile为NULL，apGangTile是这要杠的四张牌，nCount为4
	/// 补杠的明杠：pTile为自己要补杠的那张牌，apGangTile也是这张要补杠的牌，nCount为1
	/// 基类的这个函数返回这个杠牌所在的分组索引号
	/// 【注意】调用这个函数的时候，相应的牌已经从玩家手中或者出牌队列中删除了
	///			调用基类的这个函数，才会把相应的牌加入到玩家吃碰杠的分组中
	virtual int GangTile( CMahJongTile* pTile , CMahJongTile* apGangTile[ 4 ] , int nCount, int nDiscardSeat ) ;

	// 改变手中的牌
	virtual int ChangeHandTile( int anOldTileID[], int anNewTileID[], int cnChangeTile );

	// 是否已经打过牌了
	int AllDiscardTileCount() { return m_cnDiscardTile; }

protected :
	// 加入一组吃碰杠的牌
	/// nGroupStyle是组的类型，取值为GROUP_STYLE_XXX
	/// 返回这个分组的索引号
	int AddGroupTile( int nGroupStyle , CMahJongTile **ppTile , int nCount ) ;

	// 往出的牌中加一张牌
	void AddDiscardTile( CMahJongTile *pTile ) ;

	// 增加一张花牌（并不是指往手中加一张花牌，而是增加玩家花牌的信息）
	void AddFlowerTile( CMahJongTile *pTile ) ;

	// 进入临界区
	void Lock() ;
	// 退出临界区
	void UnLock() ;

public:
	bool CheckLuckyTile(int nLuckyTileID);
		
	int GetDelayCount() { return m_nDelayCount; };
	void ResetDelayCount() { m_nDelayCount = 0; };
	void AutoAddDelayCount() { m_nDelayCount++; }; //出牌延迟次数

protected:
    CTKGamePlayer * m_pPlayer;
	int m_nSeatOrder;							// 座位号
	int m_nSex;									// 性别

	int m_nScore;								// 得分
	int m_nTotalScore;							// 累计得分

	bool m_isBreak;								// 是否断线
	int m_nState ;								// 玩家的状态，取值为ST_XXX
	int m_nTrustPlay ;							// 托管的类型，0－没有托管，1－手中托管，2－超时托管，3－断线托管，4－托管退出

	int m_nGameSeat ;							// 游戏中的座位号。这个座位号是在游戏中看到的各玩家的座位，游戏中的上下家关系由这个
												/// 座位号确定，下家的座位号＋1，上家的座位号－1
												/// 比赛中还通过更改这个值来实现随机换位如果要随机换位，那么游戏中的座位号可能每一
												/// 圈会更改一次
												/// 取值从0开始
	int m_nWind ;								// 游戏中的门风号。游戏座位号确定之后，各个玩家还可能有不同的门风
												/// 每一盘各玩家的门风号可能就会改变一次
												/// 取值从0开始
	
	int m_nRules ;								// 行牌规则
												/// 最低字节：吃牌规则，标识可以吃哪些座位号的玩家打的牌
												/// 次低字节：碰牌规则，标识可以碰哪些座位号的玩家打的牌
												/// 次高字节：杠牌规则，标识可以杠哪些座位号的玩家打的牌
												/// 最高字节：胡牌规则，标识可以胡哪些座位号的玩家打的牌
												/// 每个字节的8位（bit）分别标识各个座位号的玩家，如果该位（bit）为1，标识可以吃（碰、杠、胡）
												/// 该玩家出的牌

	// 手中牌的信息
	CMahJongTile* m_apHandTile[ MAX_HAND_COUNT ] ;			// 手中的牌（排序之后，是玩家手中牌从左到右排列）
	int m_nHandTileCount ;									// 手中牌的张数
	CMahJongTile *m_pDrawTile ;								// 玩家最后抓的一张牌，一直有效到下一次轮到自己操作时（包括下一次抓牌或者吃碰杠牌）

	// 吃碰杠牌的信息
	CMahJongTile* m_apShowGroup[ MAX_SHOW_COUNT ][ 4 ] ;	// 吃碰杠的牌
	int m_anShowGroupType[ MAX_SHOW_COUNT ] ;				// 吃碰杠牌的类型
	int m_nShowGroupCount ;									// 吃碰杠牌的组数
	int m_anShowGroupSeat[ MAX_SHOW_COUNT ];				// 吃碰杠的这几组都是谁给的
	int m_cnShowGroupSeat;									// 有几组( 因为暗杠不算,所以要和m_nShowGroupCount区别开来)

	// 出的牌的信息
	CMahJongTile* m_apDiscardTile[ MAX_DISCARD_COUNT ] ;	// 出的牌
	int m_nDiscardTileCount ;								// 出牌的张数

	// 花牌的信息
	CMahJongTile* m_apFlowerTile[ MAX_FLOWER_COUNT ] ;		// 花牌
	int m_nFlowerCount ;									// 花牌的个数

	int m_nFrustaCount ;									// 砌牌时，应该砌的牌的墩数

	BOOL m_bRealHandTile ;									// 手中牌是否是真实的（对于服务器端而言，都是真实的；对于客户端而言，其他玩家的牌都不是真实的，并且如果自己旁观一个不允许旁观的玩家，那么这个座位的玩家的牌也不是真实的）

	int m_nCallType ;							// 听牌类型，0－不听牌，1－普通的听牌，2－天听，3－相公（诈和之后）
	BOOL m_bWinSelf ;							// 听牌后，是否只和自摸
	BOOL m_bAutoGang ;							// 听牌后，是否自动开杠

	int m_nContinueChiCount ;					// 连续吃牌的次数，当玩家抓牌或者碰、杠牌时重置
	int m_nContinuePengCount ;					// 连续碰牌的次数，当玩家抓牌或者吃、杠牌时重置
	int m_nContinueGangCount ;					// 连续杠牌的次数，当玩家抓牌（非杠后抓牌）或者吃、碰牌时重置

	CRITICAL_SECTION m_cs ;						// 用于保护玩家牌信息的临界区

	int m_cnDiscardTile;						// 打过几牌了，这个变量跟m_nDiscardTileCount不同，玩家打的牌不论是否在牌池里都会记录

	int m_cnGang;								// 杠牌次数
	int m_cnPao;								// 放炮次数

	WININFO m_sWinInfo;							// 临时暂存的和牌信息

	int m_cnWin;								// 和牌次数

	int m_nDelayCount; //出牌延迟次数
};
