#include "stdafx.h"
#include "TKMahjongProtocol.h"
#include "TKMahjongGameProxy.h"

#ifndef ASSERT
#define ASSERT	assert
#endif

TKMahjongGameProxy::TKMahjongGameProxy()	
{
}

// 析构函数
TKMahjongGameProxy::~TKMahjongGameProxy()
{
}

BOOL TKMahjongGameProxy::InitInstance(CTKGame * pGame)
{
	if ( pGame == NULL )
	{
		return FALSE;
	}
	m_pMahjongGame = pGame;
	return TRUE;
}


// 获取比赛ID
int TKMahjongGameProxy::GetMatchID()
{
	return m_pMahjongGame->m_dwMatchID;
}

// 发送组装好的protobuf消息包
BOOL TKMahjongGameProxy::Broadcast(TKHEADER* pMsg)
{
	if (pMsg == NULL)
	{
		TKWriteLog( "%s : %d, Error! Input pMsg Is NULL", __FILE__, __LINE__);
		return FALSE;
	}

	// 发送消息
	if ( !m_pMahjongGame->Broadcast(pMsg) )
	{
		ASSERT(FALSE);
		TKWriteLog( "%s : %d, Error! Protobuf() 发送消息出错 len( %d ) ", __FILE__, __LINE__, pMsg->dwLength );


		return FALSE;
	}

	return TRUE;
}


// 发送给observer型的旁观者
BOOL TKMahjongGameProxy::Send2Observer(TKHEADER* pMsg)
{
	if (pMsg == NULL)
	{
		TKWriteLog( "%s : %d, Error! Input pMsg Is NULL", __FILE__, __LINE__);
		return FALSE;
	}

	// 发送消息
	if ( !m_pMahjongGame->Send2Observer(pMsg) )
	{
		ASSERT(FALSE);
		TKWriteLog( "%s : %d, Error! Protobuf() 发送消息出错 len( %d ) ", __FILE__, __LINE__, pMsg->dwLength );

		return FALSE;
	}
	return TRUE;
}

// 发送给玩家
BOOL TKMahjongGameProxy::Send2SeatPlayer(int nSeat, TKHEADER* pMsg)
{
	if (pMsg == NULL)
	{
		TKWriteLog( "%s : %d, Error! Input pMsg Is NULL", __FILE__, __LINE__);
		return FALSE;
	}

	// 发送消息
	if ( !m_pMahjongGame->Send2SeatPlayer(nSeat, pMsg) )
	{
		ASSERT(FALSE);
		TKWriteLog( "%s : %d, Error! Protobuf() 发送消息出错 len( %d ) ", __FILE__, __LINE__, pMsg->dwLength );

		return FALSE;
	}

	return TRUE;
}

// 发送给black型的旁观者
BOOL TKMahjongGameProxy::Send2Black(TKHEADER* pMsg)
{
	if (pMsg == NULL)
	{
		TKWriteLog( "%s : %d, Error! Input pMsg Is NULL", __FILE__, __LINE__);
		return FALSE;
	}

	// 发送消息
	if ( !m_pMahjongGame->Send2Black(pMsg) )
	{
		ASSERT(FALSE);
		TKWriteLog( "%s : %d, Error! Protobuf() 发送消息出错 len( %d ) ", __FILE__, __LINE__, pMsg->dwLength );

		return FALSE;
	}

	return TRUE;
}

BOOL	TKMahjongGameProxy::Send2SeatBlack(int nSeat, PTKHEADER pMsg)
{
	if (pMsg == NULL)
	{
		TKWriteLog( "%s : %d, Error! Input pMsg Is NULL", __FILE__, __LINE__);
		return FALSE;
	}

	// 发送消息
	if ( !m_pMahjongGame->Send2SeatBlack(nSeat, pMsg) )
	{
		ASSERT(FALSE);
		TKWriteLog( "%s : %d, Error! Protobuf() 发送消息出错 len( %d ) ", __FILE__, __LINE__, pMsg->dwLength );

		return FALSE;
	}

	return TRUE;
}
BOOL	TKMahjongGameProxy::Send2Player( DWORD dwUserID, PTKHEADER pMsg )
{
	if (pMsg == NULL)
	{
		TKWriteLog( "%s : %d, Error! Input pMsg Is NULL", __FILE__, __LINE__);
		return FALSE;
	}

	// 发送消息
	if ( !m_pMahjongGame->Send2Player(dwUserID, pMsg) )
	{
		ASSERT(FALSE);
		TKWriteLog( "%s : %d, Error! Protobuf() 发送消息出错 len( %d ) ", __FILE__, __LINE__, pMsg->dwLength );

		return FALSE;
	}

	return TRUE;
}