// PublicJudge.h: interface for the CPublicJudge class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PUBLICJUDGE_H__8946C47D_A29C_4245_B54A_B0C5EF19EE89__INCLUDED_)
#define AFX_PUBLICJUDGE_H__8946C47D_A29C_4245_B54A_B0C5EF19EE89__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "tkmahjongdefine.h"
#include "judge.h"

class CPublicJudge;
typedef	vector<int>	FANVECTOR;
typedef	int ( CPublicJudge::* Check )( HURESULT &sHuResult );
typedef	int ( CPublicJudge::* Parse )( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );
struct FAN
{
	int					nScore;				// 分数
	Check				fCheck;				// 检查该番种的函数指针
	Parse				fParse;				// 分析该番种的函数指针
};

typedef struct tagTileInfo
{
	int					nGroupIndex;		// 所在分组索引号
	int					nIndex;				// 在分组里的索引号
	int					nColor;				// 花色
	int					nWhat;				// 点数
} TILEINFO, PTILEINFO;

class CPublicJudge : public IJudge
{
public:
	CPublicJudge();
	virtual ~CPublicJudge();

	// 初始化
	virtual BOOL OnInit();

public:
	// 验和，默认为国标规则
	virtual CHECKRESULT CheckWin( CHECKPARAM &sCheckParam, HURESULT &sHuResult );

	// 验听
	virtual CHECKRESULT CheckTing( CHECKPARAM &sCheckParam, CALLINFOVECTOR &vsCallInfo );

	// 验证和牌是否有效
	virtual BOOL CanWin( CHECKPARAM &sCheckParam, WININFO &sWinInfo );

	// 取番种信息
	virtual BOOL GetFanInfo( int nFanID, char szFanName[], char szFanScore[] );

	// 是不是刻
	BOOL IsKe( MAHJONGGROUPTILE &sGroup );

	// 是不是幺九刻
	BOOL Is19Ke( MAHJONGGROUPTILE &sGroup );

	// 将一个番种信息插入到正确的位置
	void InsertFanInfo( FANINFOVECTOR &vsFanInfo, FANINFO &sFanInfo );

	//xinxy 07.8.16 add   为了在CMahJongServerOgj中使用这4个成员函数所以将它们从protected改变为public
	// 是不是顺
	BOOL IsShun( MAHJONGTILE asTile[] );

	// 是不是刻
	BOOL IsKe( MAHJONGTILE asTile[] );

	// 是不是将
	BOOL IsJong( MAHJONGTILE asTile[] );

	//将手中的牌排序(混牌在最后，其它牌按从小到大排列),返回混牌个数
	virtual void Sort( STONE asStone[], int cnStone );
	//xinxy 07.8.16 addend

protected:
	// 是否能吃,返回可供选择的组数
	virtual int CheckChi( CHECKPARAM &sCheckParam, MAHJONGGROUPTILE asShowGroup[] );

	// 是否能碰,返回可供选择的组数
	virtual int CheckPeng( CHECKPARAM &sCheckParam, MAHJONGGROUPTILE asShowGroup[] );

	// 是否能杠,返回可供选择的组数,必须在sCheckParam的nWinMode中设置是否自摸的牌
	virtual int CheckGang( CHECKPARAM &sCheckParam, MAHJONGGROUPTILE asShowGroup[] );

protected:
	// 算番
	virtual CHECKRESULT EnumerateFans( HURESULT &sHuResult );

	BOOL SetFanTree( char szFileName[] );

	// 对带混的分组算番
	CHECKRESULT EnumerateHunFans( int nHunGroupIndex, HURESULT &sHuResult );

	// 检验十三幺和法
	CHECKRESULT CheckThirteenUnios( STONE asStone[], int cnNormalStone, int cnHun, 
		HURESULT &sHuResult );
	
	// 检验全不靠和法
	CHECKRESULT CheckAllLonely( STONE asStone[], int cnNormalStone, int cnHun, 
		HURESULT &sHuResult );

	// 检验七对和法
	CHECKRESULT CheckSevenPairs( STONE asStone[], int cnNormalStone, int cnHun, 
		HURESULT &sHuResult );

	// 检验是否可和普通牌型
	CHECKRESULT CheckNormal( STONE asStone[], int cnNormalStone, int cnHun, 
		int cnGrouped, HURESULT &sHuResult );

	// 取得牌型信息
	virtual void GetStonesRelation( STONE asHandStone[], int cnHandStone, 
		STONESRELATION &sStonesRelation );

	// 取得分组信息
	virtual void GetGroupsInfo( STONEGROUP asStoneGroup[],
		GROUPSRELATION &sGroupsRelation, STONESRELATION &sStonesRelation );

	// 是否有效的和牌信息
	BOOL ValidWinInfo( CHECKPARAM &sCheckParam, WININFO &sWinInfo );

	// 是不是13幺
	BOOL IsThirteenUnios( MAHJONGTILE asTile[] );

	// 是不是全不靠
	BOOL IsAllLonely( MAHJONGTILE asTile[] );
	
	// 是不是杠
	BOOL IsGang( MAHJONGTILE asTile[] );
	
	// 是不是龙
	BOOL IsLong( MAHJONGTILE asTile[] );
	
	// 是不是有效的牌(ID号和花色点数是否一致，手上有没有，是否有重复)
	BOOL ValidTile( MAHJONGTILE &sTile, CHECKPARAM & sCheckParam, 
		int anAppearedTileID[], int &cnAppearedTile );

	// 看看哪些牌可做将
	int EnumerateJong( STONE asStone[], int cnNormalStone, int cnHun, int anJongIndex[][2] );

	// 设置混为指定的牌,并修改m_sStonesRelation结构
	void UseHun( STONE &sHunStone, int nStoneValue, bool bAmend = false );

	// 设置混为指定的牌
	void UseHun( STONE &sHunStone, int nColor, int nWhat );

	// 指定若干张混为“龙”组中缺的几张牌
	void UseHunForLong( STONE asHun[], int cnHun, int anFirstWhat[] );

	// 给定一个ID号，在分组信息里搜索这张牌，得到它的花色点数、分组索引号、分组内索引号
	BOOL GetTileInfo( int nTileID, const STONEGROUP asGroup[], TILEINFO& sTileInfo  );

protected:	// check
	//88番
	//1 大四喜 由4副风刻(杠)组成的和牌。不计圈风刻、门风刻、三风刻、碰碰和、小四喜
	int CheckDa4Xi( HURESULT &sHuResult );

	//2 大三元 和牌中，有中发白3副刻子。不计箭刻
	int CheckDa3Yuan( HURESULT &sHuResult );

	//3 绿一色 由23468条及发字中的任何牌组成的顺子、刻子、将的和牌。不计混一色。如无“发”字组成的各牌，可计清一色
	int CheckLv1She( HURESULT &sHuResult );

	//4 九莲宝灯 由一种花色序数牌子按1112345678999组成的特定牌型，见同花色任何1张序数牌即成和牌。不计清一色
	int Check9LianBaoDen( HURESULT &sHuResult );

	//5 四杠 4个杠
	int Check4Gang( HURESULT &sHuResult );

	//6 连七对 由一种花色序数牌组成序数相连的7个对子的和牌。不计清一色、不求人、单钓 
	int CheckLian7Dui( HURESULT &sHuResult );

	//7 十三幺 由3种序数牌的一、九牌，7种字牌及其中一对作将组成的和牌。不计五门齐、不求人、单钓
	int Check131( HURESULT &sHuResult );

	//
	//64番
	//8 清幺九 由序数牌一、九刻子组成的和牌。不计碰碰和、同刻、元字 
	int CheckQing19( HURESULT &sHuResult );

	//9 小四喜 和牌时有风牌的3副刻子及将牌。不计三风刻
	int CheckXiao4Xi( HURESULT &sHuResult );

	//10 小三元 和牌时有箭牌的两副刻子及将牌。不计箭刻
	int CheckXiao3Yuan( HURESULT &sHuResult );

	//11 字一色 由字牌的刻子(杠)、将组成的和牌。不计碰碰和
	int CheckZi1She( HURESULT &sHuResult );

	//12 四暗刻 4个暗刻(暗杠)。不计门前清、碰碰和
	int Check4AnKe( HURESULT &sHuResult );

	//13 一色双龙会 一种花色的两个老少副，5为将牌。不计平和、七对、清一色
	int Check1She2Long( HURESULT &sHuResult );

	//
	//48番
	//14 一色四同顺 一种花色4副序数相同的顺子，不计一色三节高、一般高、四归一 
	int Check1She4TongShun( HURESULT &sHuResult );

	//15 一色四节高 一种花色4副依次递增一位数的刻子不计一色三同顺、碰碰和 
	int Check1She4JieGao( HURESULT &sHuResult );

	//
	//32番
	//16 一色四步高 一种花色4副依次递增一位数或依次递增二位数的顺子 
	int Check1She4BuGao( HURESULT &sHuResult );

	//17 三杠 3个杠
	int Check3Gang( HURESULT &sHuResult );

	//18 混幺九 由字牌和序数牌一、九的刻子用将牌组成的各牌。不计碰碰和 
	int CheckHun19( HURESULT &sHuResult );

	//
	//24番
	//19 七对 由7个对子组成和牌。不计不求人、单钓
	int Check7Dui( HURESULT &sHuResult );

	//20 七星不靠 必须有7个单张的东西南北中发白，加上3种花色，数位按147、258、369中的7张序数牌组成没有将牌的和牌。不计五门齐、不求人、单钓
	int Check7XinBuKao( HURESULT &sHuResult );

	//21 全双刻 由2、4、6、8序数牌的刻子、将牌组成的和牌。不计碰碰和、断幺
	int CheckQuanShuangKe( HURESULT &sHuResult );

	//22 清一色 由一种花色的序数牌组成和牌。不无字
	int CheckQing1She( HURESULT &sHuResult );

	//23 一色三同顺 和牌时有一种花色3副序数相同的顺子。不计一色三节高
	int Check1She3TongShun( HURESULT &sHuResult );

	//24 一色三节高 和牌时有一种花色3副依次递增一位数字的刻子。不计一色三同顺
	int Check1She3JieGao( HURESULT &sHuResult );

	//25 全大 由序数牌789组成的顺子、刻子(杠)、将牌的和牌。不计无字
	int CheckQuanDa( HURESULT &sHuResult );

	//26 全中 由序数牌456组成的顺子、刻子(杠)、将牌的和牌。不计断幺
	BOOL CheckQuanZhong( HURESULT &sHuResult );

	//27 全小 由序数牌123组成的顺子、刻子(杠)将牌的的和牌。不计无字
	int CheckQuanXiao( HURESULT &sHuResult );

	//
	//16番
	//28 清龙 和牌时，有一种花色1-9相连接的序数牌
	int CheckQingLong( HURESULT &sHuResult );

	//29 三色双龙会 2种花色2个老少副、另一种花色5作将的和牌。不计喜相逢、老少副、无字、平和
	int Check3She2Long( HURESULT &sHuResult );

	//30 一色三步高 和牌时，有一种花色3副依次递增一位或依次递增二位数字的顺子
	int Check1She3BuGao( HURESULT &sHuResult );

	//31 全带五 每副牌及将牌必须有5的序数牌。不计断幺
	int CheckQuanDai5( HURESULT &sHuResult );

	//32 三同刻 3个序数相同的刻子(杠)
	int Check3TongKe( HURESULT &sHuResult );

	//33 三暗刻 3个暗刻 
	int Check3AnKe( HURESULT &sHuResult );

	//
	//12番
	//34 全不靠 由单张3种花色147、258、369不能错位的序数牌及东南西北中发白中的任何14张牌组成的和牌。不计五门齐、不求人、单钓
	int CheckQuanBuKao( HURESULT &sHuResult );

	//35 组合龙 3种花色的147、258、369不能错位的序数牌
	int CheckZhuHeLong( HURESULT &sHuResult );

	//36 大于五 由序数牌6-9的顺子、刻子、将牌组成的和牌。不计无字
	int CheckDaYu5( HURESULT &sHuResult );

	//37 小于五 由序数牌1-4的顺子、刻子、将牌组成的和牌。不计无字
	int CheckXiaoYu5( HURESULT &sHuResult );

	//38 三风刻 3个风刻
	int Check3FengKe( HURESULT &sHuResult );

	//
	//8 番
	//39 花龙 3种花色的3副顺子连接成1-9的序数牌
	int CheckHuaLong( HURESULT &sHuResult );

	//40 推不倒 由牌面图形没有上下区别的牌组成的和牌，包括1234589饼、245689条、白板。不计缺一门
	int CheckTuiBuDao( HURESULT &sHuResult );

	//41 三色三同顺 和牌时，有3种花色3副序数相同的顺子
	int Check3She3TongShun( HURESULT &sHuResult );

	//42 三色三节高 和牌时，有3种花色3副依次递增一位数的刻子
	int Check3She3JieGao( HURESULT &sHuResult );

	//43 无番和 和牌后，数不出任何番种分(花牌不计算在内)
	int CheckWuFan( HURESULT &sHuResult );

	//44 妙手回春 自摸牌墙上最后一张牌和牌。不计自摸
	int CheckMiaoShou( HURESULT &sHuResult );

	//45 海底捞月 和打出的最后一张牌
	int CheckHaiDi( HURESULT &sHuResult );

	//46 杠上开花 开杠抓进的牌成和牌(不包括补花)不计自摸
	int CheckGangHu( HURESULT &sHuResult );

	//47 抢杠和 和别人自抓开明杠的牌。不计和绝张
	int CheckQiangGang( HURESULT &sHuResult );

	//
	//6 番
	//48 碰碰和 由4副刻子(或杠)、将牌组成的和牌
	int CheckPenPenHu( HURESULT &sHuResult );

	//51 五门齐 和牌时3种序数牌、风、箭牌齐全
	int Check5MenQi( HURESULT &sHuResult );

	//52 全求人 全靠吃牌、碰牌、单钓别人批出的牌和牌。不计单钓 
	int CheckQuanQiuRen( HURESULT &sHuResult );

	//53 双暗杠 2个暗杠
	int Check2AnGang( HURESULT &sHuResult );

	//54 双箭刻 2副箭刻(或杠)
	int Check2JianKe( HURESULT &sHuResult );

	//
	//4 番
	//55 全带幺 和牌时，每副牌、将牌都有幺牌
	int CheckQuanDai1( HURESULT &sHuResult );

	//56 不求人 4副牌及将中没有吃牌、碰牌(包括明杠)，自摸和牌
	int CheckBuQiuRen( HURESULT &sHuResult );

	//57 双明杠 2个明杠
	int Check2MinGang( HURESULT &sHuResult );

	//58 和绝张 和牌池、桌面已亮明的3张牌所剩的第4张牌(抢杠和不计和绝张)
	int CheckHuJueZhang( HURESULT &sHuResult );

	//
	//2 番
	//59 箭刻 由中、发、白3张相同的牌组成的刻子
	int CheckJianKe( HURESULT &sHuResult );

	//60 圈风刻 与圈风相同的风刻
	int CheckQuanFeng( HURESULT &sHuResult );

	//61 门风刻 与本门风相同的风刻
	int CheckMenFeng( HURESULT &sHuResult );

	//62 门前清 没有吃、碰、明杠，和别人打出的牌
	int CheckMenQing( HURESULT &sHuResult );

	//63 平和 由4副顺子及序数牌作将组成的和牌，边、坎、钓不影响平和
	int CheckPinHu( HURESULT &sHuResult );

	//64 四归一 和牌中，有4张相同的牌归于一家的顺、刻子、对、将牌中(不包括杠牌)
	int Check4Gui1( HURESULT &sHuResult );

	//65 双同刻 2副序数相同的刻子 
	int Check2TongKe( HURESULT &sHuResult );

	//66 双暗刻 2个暗刻
	int Check2AnKe( HURESULT &sHuResult );

	//67 暗杠 自抓4张相同的牌开杠
	int CheckAnGang( HURESULT &sHuResult );

	//68 断幺 和牌中没有一、九及字牌 
	int CheckDuan19( HURESULT &sHuResult );

	// 1 番
	//69 一般高 由一种花色2副相同的顺子组成的牌 
	int CheckYiBanGao( HURESULT &sHuResult );

	//70 喜相逢 2种花色2副序数相同的顺子
	int CheckXiXiangFeng( HURESULT &sHuResult );

	//71 连六 一种花色6张相连接的序数牌
	int CheckLian6( HURESULT &sHuResult );

	//72 老少副 一种花色牌的123、789两副顺子
	int CheckLaoShaoFu( HURESULT &sHuResult );

	//73 幺九刻 3张相同的一、九序数牌及字牌组成的刻子(或杠)
	int Check19Ke( HURESULT &sHuResult );

	//74 明杠 自己有暗刻，碰别人打出的一张相同的牌开杠：或自己抓进一张与碰的明刻相同的牌开杠
	int CheckMinGang( HURESULT &sHuResult );

	//75 缺一门 和牌中缺少一种花色序数牌
	int CheckQue1Men( HURESULT &sHuResult );

	//76 无字 和牌中没有风、箭牌
	int CheckWuZi( HURESULT &sHuResult );

	//77 边张 单和123的3及789的7或1233和3、77879和7都为边张。手中有12345和3，56789和7不算边张
	int CheckBianZang( HURESULT &sHuResult );

	//78 坎张 和2张牌之间的牌。4556和5也为坎张，手中有45567和6不算坎张
	int CheckKanZang( HURESULT &sHuResult );

	//79 单钓将 钓单张牌作将成和
	int CheckDanDiao( HURESULT &sHuResult );

	//80 自摸 自己抓进牌成和牌
	int CheckZiMo( HURESULT &sHuResult );

	// 81 花牌 即春夏秋冬，梅兰竹菊，每花计一分。不计在起和分内，和牌后才能计分。
	// 花牌补花成和计自摸分，不计杠上开花

	// 以下是大众麻将的番种
	// 168番
	// 82 四方大发财
	int Check4FangDaFa( HURESULT &sHuResult );

	// 87 八仙过海，手上有八张花牌，不计春夏秋冬，梅兰竹菊
	int Check8Xian( HURESULT &sHuResult );
	
	// 32番
	// 88 七抢一，手上有七张花牌和牌，另一家手上有一张花牌，不计春夏秋冬，梅兰竹菊
	int Check7Qiang1( HURESULT &sHuResult );
	
	// 96 立直
	int CheckLiZhi( HURESULT &sHuResult );
	
	// 1番
	// 97 二五八将
	int Check258Jong( HURESULT &sHuResult );
	
	// 98 梅兰竹菊，集齐这四张花牌之后额外加分
	int Check4Flower( HURESULT &sHuResult );
	
	// 99 春夏秋冬，集齐这四张花牌之后额外加分
	int Check4Season( HURESULT &sHuResult );
	
	// 100 季花
	int CheckSeasonFlower( HURESULT &sHuResult );
	
	// 101 么九头 由序数牌的19做将牌
	int Check19Jong( HURESULT &sHuResult );

	// 102 极速二麻胡
	int CheckJSPKHu( HURESULT &sHuResult );

protected:	// 分析
		//88番
	//1 大四喜 由4副风刻(杠)组成的和牌。不计圈风刻、门风刻、三风刻、碰碰和、小四喜
	int ParseDa4Xi( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//2 大三元 和牌中，有中发白3副刻子。不计箭刻
	int ParseDa3Yuan( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//3 绿一色 由23468条及发字中的任何牌组成的顺子、刻子、将的和牌。不计混一色。如无“发”字组成的各牌，可计清一色
	int ParseLv1She( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//4 九莲宝灯 由一种花色序数牌子按1112345678999组成的特定牌型，见同花色任何1张序数牌即成和牌。不计清一色
	int Parse9LianBaoDeng( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//5 四杠 4个杠
	int Parse4Gang( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//6 连七对 由一种花色序数牌组成序数相连的7个对子的和牌。不计清一色、不求人、单钓 
	int ParseLian7Dui( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//7 十三幺 由3种序数牌的一、九牌，7种字牌及其中一对作将组成的和牌。不计五门齐、不求人、单钓
	int Parse13Yao( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//
	//64番
	//8 清幺九 由序数牌一、九刻子组成的和牌。不计碰碰和、同刻、元字 
	int ParseQing19( HURESULT &sHuResul, FANINFOVECTOR &vsFanInfot );

	//9 小四喜 和牌时有风牌的3副刻子及将牌。不计三风刻
	int ParseXiao4Xi( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//10 小三元 和牌时有箭牌的两副刻子及将牌。不计箭刻
	int ParseXiao3Yuan( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//11 字一色 由字牌的刻子(杠)、将组成的和牌。不计碰碰和
	int ParseZi1She( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//12 四暗刻 4个暗刻(暗杠)。不计门前清、碰碰和
	int Parse4AnKe( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//13 一色双龙会 一种花色的两个老少副，5为将牌。不计平和、七对、清一色
	int Parse1She2Long( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//
	//48番
	//14 一色四同顺 一种花色4副序数相同的顺子，不计一色三节高、一般高、四归一 
	int Parse1She4TongShun( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//15 一色四节高 一种花色4副依次递增一位数的刻子不计一色三同顺、碰碰和 
	int Parse1She4JieGao( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//
	//32番
	//16 一色四步高 一种花色4副依次递增一位数或依次递增二位数的顺子 
	int Parse1She4BuGao( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//17 三杠 3个杠
	int Parse3Gang( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//18 混幺九 由字牌和序数牌一、九的刻子用将牌组成的各牌。不计碰碰和 
	int ParseHun19( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//
	//24番
	//19 七对 由7个对子组成和牌。不计不求人、单钓
	int Parse7Dui( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//20 七星不靠 必须有7个单张的东西南北中发白，加上3种花色，数位按147、258、369中的7张序数牌组成没有将牌的和牌。不计五门齐、不求人、单钓
	int Parse7XinBuKao( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//21 全双刻 由2、4、6、8序数牌的刻子、将牌组成的和牌。不计碰碰和、断幺
	int ParseQuan2Ke( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//22 清一色 由一种花色的序数牌组成和牌。不无字
	int ParseQing1She( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//23 一色三同顺 和牌时有一种花色3副序数相同的顺子。不计一色三节高
	int Parse1She3TongShun( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//24 一色三节高 和牌时有一种花色3副依次递增一位数字的刻子。不计一色三同顺
	int Parse1She3JieGao( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//25 全大 由序数牌789组成的顺子、刻子(杠)、将牌的和牌。不计无字
	int ParseQuanDa( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//26 全中 由序数牌456组成的顺子、刻子(杠)、将牌的和牌。不计断幺
	int ParseQuanZhong( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//27 全小 由序数牌123组成的顺子、刻子(杠)将牌的的和牌。不计无字
	int ParseQuanXiao( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//
	//16番
	//28 清龙 和牌时，有一种花色1-9相连接的序数牌
	int ParseQingLong( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//29 三色双龙会 2种花色2个老少副、另一种花色5作将的和牌。不计喜相逢、老少副、无字、平和
	int Parse3She2Long( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//30 一色三步高 和牌时，有一种花色3副依次递增一位或依次递增二位数字的顺子
	int Parse1She3BuGao( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//31 全带五 每副牌及将牌必须有5的序数牌。不计断幺
	int ParseQuanDai5( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//32 三同刻 3个序数相同的刻子(杠)
	int Parse3TongKe( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//33 三暗刻 3个暗刻 
	int Parse3AnKe( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//
	//12番
	//34 全不靠 由单张3种花色147、258、369不能错位的序数牌及东南西北中发白中的任何14张牌组成的和牌。不计五门齐、不求人、单钓
	int ParseQuanBuKao( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//35 组合龙 3种花色的147、258、369不能错位的序数牌
	int ParseZhuHeLong( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//36 大于五 由序数牌6-9的顺子、刻子、将牌组成的和牌。不计无字
	int ParseDaYu5( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//37 小于五 由序数牌1-4的顺子、刻子、将牌组成的和牌。不计无字
	int ParseXiaoYu5( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//38 三风刻 3个风刻
	int Parse3FengKe( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//
	//8 番
	//39 花龙 3种花色的3副顺子连接成1-9的序数牌
	int ParseHuaLong( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//40 推不倒 由牌面图形没有上下区别的牌组成的和牌，包括1234589饼、245689条、白板。不计缺一门
	int ParseTuiBuDao( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//41 三色三同顺 和牌时，有3种花色3副序数相同的顺子
	int Parse3She3TongShun( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//42 三色三节高 和牌时，有3种花色3副依次递增一位数的刻子
	int Parse3She3JieGao( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//43 无番和 和牌后，数不出任何番种分(花牌不计算在内)
	int ParseWuFan( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//44 妙手回春 自摸牌墙上最后一张牌和牌。不计自摸
	int ParseMiaoShou( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//45 海底捞月 和打出的最后一张牌
	int ParseHaiDi( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//46 杠上开花 开杠抓进的牌成和牌(不包括补花)不计自摸
	int ParseGangKai( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//47 抢杠和 和别人自抓开明杠的牌。不计和绝张
	int ParseQiangGang( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//
	//6 番
	//48 碰碰和 由4副刻子(或杠)、将牌组成的和牌
	int ParsePenPenHu( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//49 混一色 由一种花色序数牌及字牌组成的和牌 
	int ParseHun1She( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//50 三色三步高 3种花色3副依次递增一位序数的顺子
	int Parse3She3BuGao( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//51 五门齐 和牌时3种序数牌、风、箭牌齐全
	int Parse5MenQi( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//52 全求人 全靠吃牌、碰牌、单钓别人批出的牌和牌。不计单钓 
	int ParseQuanQiuRen( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//53 双暗杠 2个暗杠
	int Parse2AnGang( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//54 双箭刻 2副箭刻(或杠)
	int Parse2JianKe( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//
	//4 番
	//55 全带幺 和牌时，每副牌、将牌都有幺牌
	int ParseQuan1( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//56 不求人 4副牌及将中没有吃牌、碰牌(包括明杠)，自摸和牌
	int ParseBuQiuRen( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//57 双明杠 2个明杠
	int Parse2MinGang( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//58 和绝张 和牌池、桌面已亮明的3张牌所剩的第4张牌(抢杠和不计和绝张)
	int ParseHuJueZhang( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//
	//2 番
	//59 箭刻 由中、发、白3张相同的牌组成的刻子
	int ParseJianKe( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//60 圈风刻 与圈风相同的风刻
	int ParseQuanFeng( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//61 门风刻 与本门风相同的风刻
	int ParseMenFeng( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//62 门前清 没有吃、碰、明杠，和别人打出的牌
	int ParseMenQing( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//63 平和 由4副顺子及序数牌作将组成的和牌，边、坎、钓不影响平和
	int ParsePinHu( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//64 四归一 和牌中，有4张相同的牌归于一家的顺、刻子、对、将牌中(不包括杠牌)
	int Parse4Gui1( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//65 双同刻 2副序数相同的刻子 
	int Parse2TongKe( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//66 双暗刻 2个暗刻
	int Parse2AnKe( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//67 暗杠 自抓4张相同的牌开杠
	int ParseAnGang( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//68 断幺 和牌中没有一、九及字牌 
	int ParseDuan19( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	// 1 番
	//69 一般高 由一种花色2副相同的顺子组成的牌 
	int ParseYiBanGao( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//70 喜相逢 2种花色2副序数相同的顺子
	int ParseXiXiangFeng( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//71 连六 一种花色6张相连接的序数牌
	int ParseLian6( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//72 老少副 一种花色牌的123、789两副顺子
	int ParseLaoShaoFu( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//73 幺九刻 3张相同的一、九序数牌及字牌组成的刻子(或杠)
	int Parse19Ke( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//74 明杠 自己有暗刻，碰别人打出的一张相同的牌开杠：或自己抓进一张与碰的明刻相同的牌开杠
	int ParseMinGang( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//75 缺一门 和牌中缺少一种花色序数牌
	int ParseQue1Meng( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//76 无字 和牌中没有风、箭牌
	int ParseWuZi( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//77 边张 单和123的3及789的7或1233和3、77879和7都为边张。手中有12345和3，56789和7不算边张
	int ParseBianZang( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//78 坎张 和2张牌之间的牌。4556和5也为坎张，手中有45567和6不算坎张
	int ParseKanZang( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//79 单钓将 钓单张牌作将成和
	int ParseDanDiao( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	//80 自摸 自己抓进牌成和牌
	int ParseZiMo( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	// 81 花牌 即春夏秋冬，梅兰竹菊，每花计一分。不计在起和分内，和牌后才能计分。
	// 花牌补花成和计自摸分，不计杠上开花
	int ParseHua( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	// 以下是大众麻将的番种
	// 168番
	// 82 四方大发财
	int Parse4FangDaFa( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	// 83 天和
	int ParseTianHu( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );
	
	// 158番
	// 84 地和 
	int ParseDiHu( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	// 108番
	// 85 人和 
	int ParseRenHu( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	// 87 八仙过海，手上有八张花牌，不计春夏秋冬，梅兰竹菊
	int Parse8Xian( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	// 32番
	// 88 七抢一，手上有七张花牌和牌，另一家手上有一张花牌，不计春夏秋冬，梅兰竹菊
	int Parse7Qiang1( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	// 89 天听
	int ParseTianTing( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );
	
	// 96 立直
	int ParseLiZhi( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );
	
	// 1番
	// 97 二五八将
	int Parse258Jong( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );
	
	// 98 梅兰竹菊，集齐这四张花牌之后额外加分
	int Parse4Flower( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	// 99 春夏秋冬，集齐这四张花牌之后额外加分
	int Parse4Season( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	// 100 季花
	int ParseJiHua( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );

	// 101 么九头 由序数牌的19做将牌
	int Parse19Jong( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo );
	
	// ***********//
	// 基类的函数 //
	// ***********//
	// 是否花牌
	BOOL IsFlower( int nStoneID);
	
	// 检验传入的参数合法性
	BOOL InvalidParam( const CHECKPARAM &sCheckParam );
	
	// 是否相同的牌
	BOOL IsSameStone( STONE & sStone1, STONE & sStone2 );
	
	// 读取番种
	char * GetPrivateInt( char * pChar , int & nData );
	
	// 从花色点数得到一维数组的索引号
	int GetValue( int nColor, int nWhat );
	
	// 得到一张牌的一维数组索引号
	int GetValue( STONE &sStone );
	
	// 获得算番入口
	void GetEntry( HURESULT &sHuResult, int &nRow, int &nCol );

	// 获得一组牌的ID
	int GetID( MAHJONGGROUPTILE &sGroup, int anTileID[] );

private:
	//
	STONESRELATION m_sOriginalStoneRelation;
//	int m_cnHun;

	// ***********//
	// 基类的变量 //
	// ***********//
	// 本规则最番种个数
	int m_cnMaxFans;
	
	// 手中最多能有几张牌
	int m_cnMaxHandStone;
	
	// 最多能分为几个分组
	int m_cnMaxGroups;
	
	// 算番入口集
	FANVECTOR	m_avFansEntry[9][6];
	
	// 番种列表
	FAN			m_asFanInfo[MAXFANS];
	
	// 下面是一些临时变量，保存在这里是为了避免大量函数调用时频繁的压栈与退栈
	// 另外，每个算番的函数要求的参数都不同，为了统一处理，这些函数都没有入口参数，所需
	// 的参数都保存在这里
	// Doc类传过来的参数
	CHECKPARAM * m_pCheckParam;
	
	// 算番结果
	HURESULT m_sHuResult;
	
	// 手中牌的统计信息
	STONESRELATION m_sStonesRelation;
	
	// 所有分组的统计信息
	GROUPSRELATION m_sGroupsRelation;
	
	// 算番屏蔽位
	bool m_abEnableRule[MAXFANS];
	
	// 保存34种牌，用于验听
	STONE m_asAllStone[34];
};

#endif // !defined(AFX_PUBLICJUDGE_H__8946C47D_A29C_4245_B54A_B0C5EF19EE89__INCLUDED_)
