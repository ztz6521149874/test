#pragma once
#include <assert.h>

class CTKAutoDelete
{
public:
	CTKAutoDelete( void * pToDelete )
	{
		assert( NULL != pToDelete );
		m_pToDelete = pToDelete;
	}

	~CTKAutoDelete()
	{
		delete m_pToDelete;
		m_pToDelete = NULL;
	}

private:
	void * m_pToDelete;
};

