#include <winsock2.h>
#include <windows.h>
#include <tchar.h>

#include "gamerule.h"		// CGameRule
#include "MahJongTile.h"	// CMahJongTile
#include "stdio.h"
#include <assert.h>

#ifndef ASSERT
#define ASSERT	assert
#endif

// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
int CMahJongTile::Distance( CMahJongTile *pTile1 , CMahJongTile *pTile2 )
{
	int nDistance = 0 ;
	
	// 如果花色不同，或者是不同的字牌，那么距离为500
	// 如果两张牌相同，那么距离为1
	// 否则距离是两张牌的点数的差*10
	if( pTile1->Color() != pTile2->Color() )
	{ // 花色不同
		nDistance = 500 ;
	}
	else
	{ // 花色相同
		nDistance = pTile1->What() - pTile2->What() ;
		if( 0 != nDistance )
		{ // 点数不同
			int nColor = pTile1->Color() ;	// 两张牌的花色
			if( COLOR_WIND == nColor || COLOR_JIAN == nColor )
			{ // 字牌（风牌或者箭牌）
				nDistance = 500 ;
			}
			else
			{ // 普通的牌
				if( nDistance < 0 )
				{ // 为负
					nDistance = -nDistance ;
				}
				nDistance *= 10 ;
			}
		}
		else
		{ // 点数相同（相同的牌）
			nDistance = 1 ;
		}
	}
	
	return nDistance ;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
int CMahJongTile::Compare( const void* pData1 , const void* pData2 )
{
	/// 大小次序，从小到大依次为：混牌、万（1-9）、条（1-9）、饼（1-9）、东南西北、中发白、梅兰竹菊、春夏秋冬
	CMahJongTile *pTile1 = *( CMahJongTile** ) pData1 ;
	CMahJongTile *pTile2 = *( CMahJongTile** ) pData2 ;
	
	return pTile1->ID() - pTile2->ID() ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CMahJongTile::CMahJongTile( CGameRule * pRule )
{
	ASSERT( NULL != pRule ) ;
	m_pRule = pRule;
	
	m_nID    = 0 ;
	m_nColor = 0 ;
	m_nWhat  = 0 ;

	memset( m_szName , 0 , sizeof( m_szName ) ) ;
	memset( m_szNameID , 0 , sizeof( m_szNameID ) ) ;
	
	m_nHunTileID = 0 ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CMahJongTile::~CMahJongTile()
{
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
BOOL CMahJongTile::SetID( int nID )
{
	m_nID = nID ;
	if( 0 == nID )
	{ // 设为无效的牌
		m_nColor = m_nWhat = 0 ;
		memset( m_szName , 0 , sizeof( m_szName ) ) ;
		memset( m_szNameID , 0 , sizeof( m_szNameID ) ) ;
		return TRUE ;
	}
	
	// 把这个ID号转换为牌的花色、点数
	if( !ID2ColorWhat( nID , m_nColor , m_nWhat ) )
	{ // 无效的ID号
		TKWriteLog( TEXT( "【CMahJongTile::SetID】id(0x%03X)" ) , nID ) ;
		m_nID = m_nColor = m_nWhat = 0 ;
		memset( m_szName , 0 , sizeof( m_szName ) ) ;
		memset( m_szNameID , 0 , sizeof( m_szNameID ) ) ;
		return FALSE ;
	}
	UpdateTileName() ;	// 更新牌的名字

	return TRUE ;
}

// ************************************************************************************************************************
// 
// 这个函数主要用于已变为其它牌的混牌
// 
// ************************************************************************************************************************
BOOL CMahJongTile::SetTile( MAHJONGTILE &sTile , int nHunTileID /* = 0 */ )
{
	m_nID = sTile.nID ;
	if( 0 == m_nID )
	{ // 设为无效的牌
		m_nColor = m_nWhat = 0 ;
		memset( m_szName , 0 , sizeof( m_szName ) ) ;
		memset( m_szNameID , 0 , sizeof( m_szNameID ) ) ;
		return FALSE ;
	}

	m_nColor = sTile.nColor ;
	m_nWhat  = sTile.nWhat ;
	UpdateTileName() ;	// 更新牌的名字

	// 看看是否需要指定混牌
	if( 0 != nHunTileID )
	{ // 需要指定混牌的ID号
		m_nHunTileID = nHunTileID ;
	}

	return TRUE;
}

// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
BOOL CMahJongTile::IsFlower()
{
	int nColor = Color() ;	// 牌的花色
	
	return ( ( COLOR_FLOWER == nColor ) || ( COLOR_SEASON == nColor ) ) ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
BOOL CMahJongTile::IDIs( int nID )
{
	return SameID( ID() , nID ) ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
BOOL CMahJongTile::SameAs( int nColor , int nWhat )
{
	return ( Color() == nColor ) && ( What() == nWhat ) ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CMahJongTile::FillTileInfo( PMAHJONGTILE pMahJongTile )
{
	if( NULL == this )
	{ // 对象无效
		return ;
	}

	pMahJongTile->nID    = ID() ;
	pMahJongTile->nColor = Color() ;
	pMahJongTile->nWhat  = What() ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CMahJongTile::ResetGameData()
{
	m_nID    = 0 ;
	m_nColor = 0 ;
	m_nWhat  = 0 ;

	memset( m_szName , 0 , sizeof( m_szName ) ) ;
	memset( m_szNameID , 0 , sizeof( m_szNameID ) ) ;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
BOOL CMahJongTile::ID2ColorWhat( int nID , int& nColor , int& nWhat )
{
	if( 0 == ( nID & 0xF ) )
	{ // 无效的ID号（最低的四位是牌的张数）
		return FALSE ;
	}
	
	nColor = TileColor( nID ) ;
	nWhat  = TileWhat( nID ) ;
	
	// 看看是否是一个合法的花色、点数
	switch( nColor )
	{
	case COLOR_WAN :	// 万
	case COLOR_TIAO :	// 条
	case COLOR_BING :	// 饼
		if( nWhat > 8 ) return FALSE ;
		break ;

	case COLOR_WIND :	// 风牌（东南西北）
		if( nWhat > 3 ) return FALSE ;
		break ;

	case COLOR_JIAN :	// 箭牌（中发白）
		if( nWhat > 2 ) return FALSE ;
		break ;

	case COLOR_FLOWER :	// 梅兰竹菊
	case COLOR_SEASON :	// 春夏秋冬
		if( nWhat > 3 ) return FALSE ;
		break ;

	default :
        TKWriteLog( TEXT( "【CMahJongTile::ID2ColorWhat】id(0x%03X),color(%d),what(%d)" ) , nID , nColor , nWhat ) ;
		ASSERT( FALSE ) ;
		return FALSE ;
	}
	
	return TRUE ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CMahJongTile::UpdateTileName()
{
	char* pszType[ 3 ] = { "万" , "条" , "饼" } ;
	char* pszWind[ 4 ] = { "东风", "南风", "西风", "北风" } ;
	char* pszJian[ 3 ] = { "红中", "绿发", "白板" } ;
	char* pszFlower[ 4 ] = { "梅" , "兰" , "竹" , "菊" } ;
	char* pszSeason[ 4 ] = { "春" , "夏" , "秋" , "冬" } ;
//	char* szFlower = "花牌" ;
	
	int nColor = m_nColor ;
	int nWhat  = m_nWhat ;
	int nID    = m_nID ;

	// 根据花色、点数、ID号构造牌的名字
	switch( nColor )
	{
	case COLOR_WAN :	// 万
	case COLOR_TIAO :	// 条
	case COLOR_BING :	// 饼
		_stprintf_s( m_szName , TEXT( "%d%s" ) , nWhat + 1 , pszType[ nColor ] ) ;
		_stprintf_s( m_szNameID , TEXT( "%d%s(0x%03X)" ) , nWhat + 1 , pszType[ nColor ] , nID ) ;
		break ;
		
	case COLOR_WIND :	// 风牌（东南西北）
		_stprintf_s( m_szName , TEXT( "%s" ) , pszWind[ nWhat ] ) ;
		_stprintf_s( m_szNameID , TEXT( "%s(0x%03X)" ) , pszWind[ nWhat ] , nID ) ;
		break ;
		
	case COLOR_JIAN :	// 箭牌（中发白）
		_stprintf_s( m_szName , TEXT( "%s" ) , pszJian[ nWhat ] ) ;
		_stprintf_s( m_szNameID , TEXT( "%s(0x%03X)" ) , pszJian[ nWhat ] , nID ) ;
		break ;
		
	case COLOR_FLOWER :	// 梅兰竹菊
		_stprintf_s( m_szName , TEXT( "%s" ) , pszFlower[ nWhat ] ) ;
		_stprintf_s( m_szNameID , TEXT( "%s(0x%03X)" ) , pszFlower[ nWhat ] , nID ) ;
		break ;

	case COLOR_SEASON :	// 春夏秋冬
		_stprintf_s( m_szName , TEXT( "%s" ) , pszSeason[ nWhat ] ) ;
		_stprintf_s( m_szNameID , TEXT( "%s(0x%03X)" ) , pszSeason[ nWhat ] , nID ) ;
		break ;
		
	default :
		ASSERT( FALSE ) ;
	}
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CMahJongTile::GetTileName( int nTileID, TCHAR szName[], int nBufferLength )
{
	int nColor, nWhat;
	if ( !ID2ColorWhat( nTileID, nColor, nWhat ) )
	{
		_tcscpy_s( szName, nBufferLength, "无效牌" );
		return;
	}
	
	char* pszType[ 3 ] = { "万" , "条" , "饼" } ;
	char* pszWind[ 4 ] = { "东风", "南风", "西风", "北风" } ;
	char* pszJian[ 3 ] = { "红中", "绿发", "白板" } ;
	char* pszFlower[ 4 ] = { "梅" , "兰" , "竹" , "菊" } ;
	char* pszSeason[ 4 ] = { "春" , "夏" , "秋" , "冬" } ;
	// 根据花色、点数、ID号构造牌的名字
	switch( nColor )
	{
	case COLOR_WAN :	// 万
	case COLOR_TIAO :	// 条
	case COLOR_BING :	// 饼
		_stprintf_s( szName , nBufferLength, TEXT( "%d%s" ) , nWhat + 1 , pszType[ nColor ] ) ;
		break ;

	case COLOR_WIND :	// 风牌（东南西北）
		_stprintf_s( szName , nBufferLength, TEXT( "%s" ) , pszWind[ nWhat ] ) ;
		break ;

	case COLOR_JIAN :	// 箭牌（中发白）
		_stprintf_s( szName , nBufferLength, TEXT( "%s" ) , pszJian[ nWhat ] ) ;
		break ;

	case COLOR_FLOWER :	// 梅兰竹菊
		_stprintf_s( szName , nBufferLength, TEXT( "%s" ) , pszFlower[ nWhat ] ) ;
		break ;

	case COLOR_SEASON :	// 春夏秋冬
		_stprintf_s( szName , nBufferLength, TEXT( "%s" ) , pszSeason[ nWhat ] ) ;
		break ;

	default :
		ASSERT( FALSE ) ;
	}
}

