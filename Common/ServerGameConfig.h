﻿/*
	服务器端的游戏配置
*/
#pragma once

#include <string>
#include <vector>

enum RuleItem
{
    RULE_TILETYPE = 0,                         //每副牌用哪几门牌(万条饼风箭)
    RULE_SCORE_RULE = 1,                       //计分规则
    RULE_CANEAT = 2,                           //是否能吃
    RULE_CANPONG = 3,                          //是否能碰
    RULE_DISCARDTIME = 4,                      //出牌时间
    RULE_WAITTIME = 5,                         //吃碰杠等待时间
    RULE_WISH_COIN_RATE = 6,                   //产生祝福币的概率（1/10000）
    RULE_WISH_SLIVER_RATE = 7,                 //产生祝福银币的概率（1/10000）
    RULE_WISH_COPPER_RATE = 8,                 //产生祝福铜币的概率（1/10000）
    RULE_REQUEST_WAITTIME = 9,                 //向客户端发送请求信息后的等待时间，超过这个时间认为客户端作弊，单位是秒
    RULE_SCORE_LIMIT = 10,                     //是否有上下封顶限制
    RULE_DOUBLE_COUNT = 11,                    //过胡翻倍次数，0代表不加倍
    RULE_BIG_FLOWER_RATE = 12,                 //出花开富贵的概率（1/10000）
    RULE_LUCKY_TILE_COUNT = 13,                //奖花牌数量
    RULE_LUCKY_TILE_FAN1 = 14,                 //中1张花牌番数
    RULE_LUCKY_TILE_FAN2 = 15,                 //中2张花牌番数
    RULE_LUCKY_TILE_FAN3 = 16,                 //中3张花牌番数
    RULE_LUCKY_TILE_FAN4 = 17,                 //中4张花牌番数
    RULE_LUCKY_TILE_FAN5 = 18,                 //中5张花牌番数
    RULE_SCORE_LOWER_LIMIT = 19,               //积分下限限制（即输家score不允许为负数）
    RULE_HAND_WIN_SCORE = 20,                  //赢家当局得分须≥指定积分才能得到赢家奖金。
    RULE_SPECIALTILE_FAN = 21,                 //加番牌所加番数
    RULE_CF_COUNT = 22,                        //花开富贵牌数量
    RULE_CF_SCORE = 23,	                       //补花牌得分
    RULE_CF_WARETYPE = 24,                     //花开富贵物品类型
    RULE_CF_WARECOUNT = 25,                    //花开富贵物品数量
    RULE_CFO_WARETYPE = 26,                    //花开富贵对家物品奖励类型：2-金币，889秋卡
    RULE_CFO_WARECOUNT = 27,                   //花开富贵对家物品奖励奖物品数量
	RULE_CF_GAMEOVER = 28,                     //花开富贵是否立即结束游戏
	RULE_FULL_BLOOM_RATE = 29,				   //出花开满园的概率（1/10000）
	RULE_FB_COUNT = 30,				           //花开满园牌数量
	RULE_FB_WARETYPE = 31,					   //花开满园奖品类型：2-金币，889秋卡
	RULE_FB_WARECOUNT = 32,					   //花开满园奖品数量
	RULE_FBO_WARETYPE = 33,					   //花开满园对家奖品类型：2-金币，889秋卡
	RULE_FBO_WARECOUNT = 34,				   //花开满园对家奖品数量
	RULE_TREASURE_CARD = 35,				   //是否开启宝藏牌
	RULE_GANG_SCORE = 36,					   //杠牌收取玩家倍数(*游戏基数)
	RULE_TREASURE_PICK = 37,                   //宝藏牌翻出不合格是否可以进行下一轮（0-继续，1-不继续）
	RULE_TREASURE_LEVEL0 = 38,                 //宝藏牌翻出不合格[0等级]的概率（‱）
	RULE_TREASURE_LEVEL2 = 39,                 //宝藏牌翻出优秀[2等级]的概率（‱）
	RULE_TREASURE_LEVEL5 = 40,                 //宝藏牌翻出完美[5等级]的概率（‱）
	RULE_WAIT_DISCARDTIME = 41,                //出牌等待超时时长。（当玩家连续2次出牌等待时长超过RULE_WAIT_DISCARDTIME秒，后续出牌等待时长缩短为RULE_REDUCED_DISCARDTIME秒。）
	RULE_REDUCED_DISCARDTIME = 42,             //连续2次出牌后的出牌时间
    RULE_ITEM_COUNT // 总共有多少个单项	
};

const int g_nDefaultValue[RULE_ITEM_COUNT] = { 17,0,0,1,10,8,0,0,0,30,0,0,0,5,10,20,30,40,50,1,0,10,4,2,2,2,2,2,1,0,3,2,2,2,2,1,50,0,516,474,10,8,5 };


//游戏配置
class CConfig
{
public:
	CConfig();
public:
    void Load(const std::string& configStr);

	int GetRule( int nRuleMask );

	void SetRule( int nRuleMask, int nRuleValue);

private:
    int m_nCfgValue[RULE_ITEM_COUNT];
};
