#ifndef _MAHJONG_TILE_H
#define _MAHJONG_TILE_H

#include "tkmahjongdefine.h"


class CMahJongObj ;	// 麻将对象



// *********************************************************************************************************************
//
// 
// 麻将牌基类，包含牌的最基本的信息
// 
//
// *********************************************************************************************************************
class CGameRule;
class CMahJongTile
{
private:
	// 牌的基本信息
	int	m_nID ;			// 牌的ID号
	int	m_nColor ;		// 牌的花色
	int	m_nWhat ;		// 牌的点数

	char m_szName[ 8 ] ;	// 牌的名字
	char m_szNameID[ 16 ] ;	// 牌的名字和ID号

	int m_nHunTileID ;	// 指定的混牌ID号（如果没有使用这个值，那么就用麻将对象的判断混牌的方法，否则就判断this这张牌的id号是否和这个牌相同）
	
	CGameRule  * m_pRule ;	// 规则对象

public :
	// 构造函数
	CMahJongTile( CGameRule * pRule ) ;
	// 析构函数
	virtual ~CMahJongTile() ;

	int	ID()	 const { return m_nID ; }		// 返回牌的ID号
	int	Color()  const { return m_nColor ; }	// 返回牌的花色
	int	What()	 const { return m_nWhat ; }		// 返回牌的点数

	const char* Name() const { return m_szName ; }		// 返回牌的名字
	const char* NameID() const { return m_szNameID ; }	// 返回牌的名字和ID号
	
	// 设置这张牌的ID号
	/// 如果nID为0，表示把这张牌设置为空
	/// 如果ID号无效，返回FALSE
	BOOL SetID( int nID ) ;
	// 设置这张牌的牌值和另外一张牌相同（这个函数主要用于已变为其它牌的混牌）
	/// 需要注意的是：这另外一张牌的id，color，what不一定是一致的（符合ID2ColorWhat），比如id是一个混牌，而color和what是
	/// 这个混牌变成的其他牌
	/// nHunTileID是混牌的ID号，一般来说不应该使用这个参数，如果使用了这个参数指定了某个有效的混牌ID号，那么以后判断这张牌
	/// （指this对象）是否是混牌时，就仅仅看ID号是否和这个混牌ID号相同来判断
	BOOL SetTile( MAHJONGTILE &sTile , int nHunTileID = 0 ) ;
	
	// 判断是否是花牌
	BOOL IsFlower() ;
	
	// 判断是否是这个ID号的牌
	BOOL IDIs( int nID ) ;
	// 判断是否花色点数相同
	BOOL SameAs( int nColor , int nWhat ) ;

	// 填充麻将结构信息
	void FillTileInfo( PMAHJONGTILE pMahJongTile ) ;

public :
	// 重置牌的游戏数据
	virtual void ResetGameData() ;

private :
	// 从牌的ID号、花色、点数构造牌的名字
	/// 调用这个函数之前，必须保证ID、Color、What都是有效的值
	void UpdateTileName() ;

public :
	// 获得一个ID号的花色
	static int TileColor( int nID ) { return ( ( nID >> 8 ) & 0x0F ) ; }
	// 获得一个ID号的点数
	static int TileWhat( int nID ) { return ( ( nID >> 4 ) & 0x0F ) ; }
	// 判断两个ID号是否是同一张牌
	static BOOL SameID( int nID1 , int nID2 ) { return ( ( 0xFFFF & nID1 ) == ( 0xFFFF & nID2 ) ) ; }
	// 判断两张牌是否是同一张牌
	static BOOL SameTile( int nID1 , int nID2 ) { return ( ( TileColor( nID1 ) == TileColor( nID2 ) ) && ( TileWhat( nID1 ) == TileWhat( nID2 ) ) ) ; }
	// 返回两张牌之间的距离
	static int Distance( CMahJongTile *pTile1 , CMahJongTile *pTile2 ) ;
	// 比较两张牌的大小
	/// pData1和pData2是CMahJongTile**类型
	/// 大小次序，从小到大依次为：混牌、万（1-9）、条（1-9）、饼（1-9）、东南西北、中发白、梅兰竹菊、春夏秋冬
	/// 如果*pData1比*pData2大，那么返回正数；如果两者一样大，返回0；其他，返回负数
	static int Compare( const void* pData1 , const void* pData2 ) ;
	// 从牌的ID号、花色、点数构造牌的名字
	/// 调用这个函数之前，必须保证ID、Color、What都是有效的值
	static void GetTileName( int nTileID, TCHAR szName[], int nBufferLength ) ;
	// 从牌的ID号转换到花色、点数
	/// 如果返回FALSE，表示ID号无效
	static BOOL ID2ColorWhat( int nID , int& nColor , int& nWhat ) ;
};

#endif
