#pragma once

#include "TKMahjongDefine.h"
#include "tkprotocol.h"
#include "TKMahJongJSPK.pb.h"

#define REQ_TYPE( type )				( TK_REQ | ( type ) )
#define ACK_TYPE( type )				( TK_ACK | ( type ) )
#define MSG_LENGTH( type )				( sizeof( type ) - sizeof( TKHEADER ) )

// 
// ＝普通牌＝
// 牌的ID号：0x0000ABCD，A－第几副牌（从0开始）；B－牌的花色（从0开始）；C－牌的点数（从0开始）；D－第几张牌（1－4）
// ＝道具牌＝
// 牌的ID号：0xEFGHABCD，ABCD的含义还是同普通牌，EFGH为道具牌的ID号
// 

// 
// 游戏的各个阶段
// 
// 
//      一局结束
//         |
//         |   开始阶段
//         |
// 所有玩家都点击开始
//         |
//         |   准备阶段（定庄、定游戏座位号、询问玩家加倍、洗牌、砌牌、掷骰子）
//         |
// 所有玩家都设置了加倍
//         |
//         |   开牌阶段（通知加倍、通知彩金、开牌）
//         |
// 所有玩家开牌结束
//         |
//         |   补花阶段（翻混、补花）
//         |
//      补花结束
//         |
//         |   游戏阶段（出牌、摸牌等）
//         |
// 有玩家和牌或者荒庄
//         |
//         |   显示结果阶段（发送结果信息）
//         |
// 玩家发送收到结果消息（玩家显示结果）
//     一局结束
// 
// 
#define MAHJONG_SCHEDULE_CONTINUE		( 0 )	// 开始阶段
#define MAHJONG_SCHEDULE_PREPARE		( 1 )	// 准备阶段
#define MAHJONG_SCHEDULE_OPEN_DOOR		( 2 )	// 开牌阶段
#define MAHJONG_SCHEDULE_CHANGE_FLOWER	( 3 )	// 补花阶段
#define MAHJONG_SCHEDULE_PLAY			( 4 )	// 出牌阶段
#define MAHJONG_SCHEDULE_RESULT			( 5 )	// 显示结果阶段


// =====================================================================================================================
//
// 开始消息定义了
//
// =====================================================================================================================
// =====================================================================================================================
// 
// 【游戏开始消息】
// 
// 
// =====================================================================================================================
#define TK_MSG_MAHJONG_GAME_BEGIN		( TK_MSG_GAME_MJJSPK + 0x01 )


// =====================================================================================================================
// 
// 【游戏基本信息消息】
// 在玩家正常进入游戏之后，服务器发送给玩家的第一个游戏是这个消息
// 在旁观或者断线续玩的时候，服务器发送给旁观的客户端或者断线回来的玩家的第一个消息也是这个消息
// 
// =====================================================================================================================
#define TK_MSG_MAHJONG_BASE_INFO		( TK_MSG_GAME_MJJSPK + 0x01 )
// server->client
/// 麻将规则类型
#define MAHJONG_TYPE_NULL				( 0 )	// 空
#define MAHJONG_TYPE_GB					( 1 )	// 国标
#define MAHJONG_TYPE_PUBLIC				( 2 )	// 大众
#define MAHJONG_TYPE_CD_XZ				( 3 )	// 成都血战到底
#define MAHJONG_TYPE_NC					( 4 )	// 南昌麻将
#define MAHJONG_TYPE_NOFANS				( 5 )	// 推倒和
#define MAHJONG_TYPE_TWO				( 7 )	// 二人麻将 /*FOR TWO PLAYER*/
#define MAHJONG_TYPE_JSPK             ( 80 ) // 极速二麻

/// 麻将牌类型
#define TILE_TYPE_BIT_WAN				( 1 << 0 )	// 万
#define TILE_TYPE_BIT_TIAO				( 1 << 1 )	// 条
#define TILE_TYPE_BIT_BING				( 1 << 2 )	// 饼
#define TILE_TYPE_BIT_WIND				( 1 << 3 )	// 东南西北
#define TILE_TYPE_BIT_JIAN				( 1 << 4 )	// 中发白
#define TILE_TYPE_BIT_FLOWER			( 1 << 5 )	// 梅兰竹菊
#define TILE_TYPE_BIT_SEASON			( 1 << 6 )	// 春夏秋冬
typedef struct tagTKReqMahJongBaseInfo
{
	TKHEADER header ;
	int nMahJongType ;					// 麻将类型，取值为MAHJONG_TYPE_XXX，客户端可以根据这个类型来加载相应的规则信息
	int nSetCount ;						// 用几副牌
	int nPlayerCount ;					// 几个玩家
	int nTileType ;						// 每副牌用哪些牌，取值为TILE_TYPE_BIT_XXX的并
	int nDiceCount ;					// 骰子数目
	int nDrawFrustaEachTime ;			// 开牌时，每次抓几墩牌
	int nDrawFrustaTimes ;				// 开牌时，抓几次牌（不包括最后的跳牌和扫底）
	int nBankerSkipTileCount ;			// 开牌时，庄家跳牌是摸接下来的第一张，然后跳过这么多张，跳第二张牌；闲家扫底都是摸接下来的第一张牌
} TKREQMAHJONGBASEINFO , *PTKREQMAHJONGBASEINFO ;


// =================================================================================================
// 
// 【游戏规则消息】
// 服务器发送了游戏的基本信息之后，发送游戏规则信息给客户端
// 这个消息从游戏的基本信息中独立出来，是因为一来同一类型的麻将可能会支持不同类型的规则，二来如果要
// 不断支持不同的规则，那么这部分的信息是会不断更改，而且不同类型的地方麻将可能规则信息完全不一致。
// 
// =================================================================================================
#define TK_MSG_MAHJONG_RULE_INFO		( TK_MSG_GAME_MJJSPK + 0x02 )
// server->client
#define RULE_BIT_USE_TOOL				( 1 << 0 )		// 是否使用道具，1表示使用道具，0表示不使用道具
#define RULE_BIT_TIP_CALL				( 1 << 1 )		// 是否提示听牌，1表示提示听牌，0表示不提示听牌
#define RULE_BIT_TIP_WIN				( 1 << 2 )		// 是否提示和牌，1表示提示和牌，0表示不提示和牌
#define RULE_BIT_HUN					( 1 << 3 )		// 是否有混牌，1表示有混牌，0表示没有混牌
#define RULE_BIT_SHOW_DOUBLE			( 1 << 4 )		// 是否显示玩家的加倍信息，1表示显示，0表示不显示
#define RULE_BIT_GANG_AWARD				( 1 << 5 )		// 杠牌是否有得分，1表示有得分，0表示没有得分
#define RULE_BIT_DOUBLE					( 1 << 6 )		// 是否允许加倍，1表示可加倍，0表示不可加倍
typedef struct tagTKReqMahJongRuleInfo
{
	TKHEADER header ;
	int nRule ;							// 规则信息（这些信息是可以按位来存取的），相应的位的含义见RULE_BIT_XXX
	bool bWinSelfOnly ;					// 是否只能和自摸
	int nDiscardTime ;					// 出牌等待时间（单位：秒）
	int nWaitTime ;						// 吃碰杠听等待时间（单位：秒）
	int nWinNeedMinFan ;				// 和牌所需的最小番数
} TKREQMAHJONGRULEINFO , *PTKREQMAHJONGRULEINFO ;


// =================================================================================================
// 
// 【版本消息】
// 每一盘玩家先发送REQ的这个消息给服务器端,服务器针对客户端版本会有相应处理
// 
// =================================================================================================
#define TK_MSG_MAHJONG_VERSION			( TK_MSG_GAME_MJJSPK + 0x03 )
// client->server
typedef struct tagTKReqMahJongVersion
{
	TKHEADER header ;
	int nSeat ;					// 座位号
	DWORD dwFanTreeVersion ;	// 算法中算番树的版本号
	DWORD dwFanTreeCheckSum ;	// 算法中算番树的校验和
	DWORD dwMagicItemVersion ;	// 支持的道具版本号（服务器会根据这个版本号给这个客户端发送他能够理解的道具）
} TKREQMAHJONGVERSION , *PTKREQMAHJONGVERSION ;



// =================================================================================================
// 
// 【算番树消息】
// 如果客户端的算番树不是最新的，服务器端发送最新的算番树信息给玩家
// 这个消息一般来说是服务器收到客户端的版本消息后发送给客户端的
// 
// =================================================================================================
#define TK_MSG_MAHJONG_FAN_TREE	( TK_MSG_GAME_MJJSPK + 0x04 )
// server->client
typedef struct tagTKReqMahJongFanTree
{
	TKHEADER header ;
	DWORD dwFanTreeVersion ;	// 最新的算番树的版本
	DWORD dwFanTreeCheckSum ;	// 最新的算番树的校验和
	// 消息后跟最新的算番树结构信息
} TKREQMAHJONGFANTREE , *PTKREQMAHJONGFANTREE ;



// =================================================================================================
// 
// 【游戏座位、庄家消息】
// 第一盘大家都开始游戏后，服务器端发送这个消息给所有客户端
// 在每次更改了玩家的游戏座位或者行牌规则之后，服务器端发送这个消息给所有客户端
// 断线续玩的时候或者旁观的时候，如果已经过了定庄、定游戏座位阶段，那么也会发送这个消息
// 
// =================================================================================================
#define TK_MSG_MAHJONG_PLACE		( TK_MSG_GAME_MJJSPK + 0x05 )
// server->client
typedef struct tagTKReqMahJongPlace
{
	TKHEADER header ;
	int anGameSeat[ MAX_PLAYER_COUNT ] ;	// 各座位的游戏座位号（从0开始计数），pnGameSeat[i]是座位号为i的玩家的游戏座位号
	/// 游戏中的行牌顺序就是按照这个游戏座位号来的，下一家的游戏座位号＋1，上一家的游
	/// 戏座位号－1。
	/// 注意：游戏座位号为0并不表示就是庄家或者门风为东，这个是行牌的顺序。
	int nBanker ;							// 庄家的座位号
	int nRules ;							// 行牌规则
											/// 最低字节：吃牌规则，标识可以吃哪些座位号的玩家打的牌
											/// 次低字节：碰牌规则，标识可以碰哪些座位号的玩家打的牌
											/// 次高字节：杠牌规则，标识可以杠哪些座位号的玩家打的牌
											/// 最高字节：和牌规则，标识可以和哪些座位号的玩家打的牌
											/// 每个字节的8位（bit）分别标识各个座位号的玩家（第几位标识座位号为几的玩家），
											/// 如果该位（bit）为1，标识可以吃（碰、杠、和）该玩家出的牌
} TKREQMAHJONGPLACE , *PTKREQMAHJONGPLACE ;



// =================================================================================================
// 
// 【门风、圈风消息】
// 当玩家的游戏座位号确定之后，服务器发送各玩家的门风、圈风信息给客户端。
// 
// =================================================================================================
#define TK_MSG_MAHJONG_WIND_PLACE		( TK_MSG_GAME_MJJSPK + 0x06 )
// server->client
/*FOR TWO PLAYER*/
//#define WIND_PLACE_EAST		( 0 )	// 东
#define WIND_PLACE_SOUTH	( 0 )	// 南
//#define WIND_PLACE_WEST		( 2 )	// 西
#define WIND_PLACE_NORTH	( 1 )	// 北
typedef struct tagTKReqMahJongWindPlace
{
	TKHEADER header ;
	int nRoundWind ;						// 圈风，取值为WIND_PLACE_XXX
} TKREQMAHJONGWINDPLACE , *PTKREQMAHJONGWINDPLACE ;



// =================================================================================================
// 
// 【结果消息】
//     一局游戏结束时，服务器发送这个消息给所有客户端，报告这一局的结果信息；客户端收到这个消息后
// 
// =================================================================================================
#define TK_MSG_MAHJONG_RESULT			( TK_MSG_GAME_MJJSPK + 0x08 )
#define TK_MSG_MAHJONG_RESULTEX			( TK_MSG_GAME_MJJSPK + 0x09 )
// server->client
typedef struct tagPlayerResult 
{
	int	cnWin;										// 和牌次数
	int	cnPao;										// 点炮次数
	int	cnGang;										// 杠牌次数( 包括混杠 )
	int	nScore;										// 本局得分
	int nDouble;									// 本局加倍次数（ 以赢家为准，默认0 ）
	int nTotalScore;								// 累计得分
	int cnHandTile;									// 手上剩下的牌张数
	int	anHandTile[ MAX_HAND_TILE_COUNT ];			// 手上剩下的牌
} PLAYERRESULT, *PPLAYERRESULT;
typedef struct tagTKReqMahJongResult
{
	TKHEADER header ;
	int  cnWinInfo;									// 0 == cnWinInfo表示荒庄（流局）
	PLAYERRESULT asPlayerResult[ MAX_PLAYER_COUNT ];// 每个玩家的结果信息
	int nTime;
	int nResultType;//结算类型：玩家正常结算resulttype==0,积分输光resulttype==1,流局resulttype==2
	// 如果没有流局,后跟cnWinInfo个和牌信息
} TKREQMAHJONGRESULT , *PTKREQMAHJONGRESULT ;



// =================================================================================================
// 
// 【洗牌、砌牌消息】
// 在确定了游戏座位、庄家之后，服务器发送这个消息给所有客户端
// 
// =================================================================================================
#define TK_MSG_MAHJONG_SHUFFLE	( TK_MSG_GAME_MJJSPK + 0x10 )
// server->client
typedef struct tagTKReqMahJongShuffle
{
	TKHEADER header;
	int cnTile;									// 洗牌时麻将牌的总共张数（包括道具牌的张数）
	int cnMagicItem;							// 道具牌的个数（这个信息用来明确告诉玩家这局牌中有多少道具牌）
	int anFrustaOfSeat[ MAX_PLAYER_COUNT ] ;	// 砌牌时，各个座位号的玩家需要砌牌的墩数（每一墩是上下两张牌）
} TKREQMAHJONGSHUFFLE , *PTKREQMAHJONGSHUFFLE ;



// =================================================================================================
// 
// 【掷骰子消息】
// 当有掷骰子操作时，服务器发送这个消息给客户端
// 
// =================================================================================================
#define TK_MSG_MAHJONG_CAST_DICE	( TK_MSG_GAME_MJJSPK + 0x11 )
// server->client
#define CAST_DICE_TYPE_NULL			( 0 )	// 空
#define CAST_DICE_TYPE_OPEN_DOOR	( 1 )	// 为了开牌掷骰子
#define CAST_DICE_TYPE_OPEN_DOOR_2	( 2 )	// 为了开牌第二次掷骰子（有些麻将中，开牌时需要掷两次骰子，第一次确定开牌的玩家，第二次确定开牌的牌的位置）
#define CAST_DICE_TYPE_BANKER		( 3 )	// 为了定庄掷骰子
#define CAST_DICE_TYPE_GAME_SEAT	( 4 )	// 为了定游戏座位号掷骰子
#define CAST_DICE_TYPE_HUN			( 5 )	// 为了翻混掷骰子
#define CAST_DICE_TYPE_GANG			( 6 )	// 为了杠牌掷骰子
typedef struct tagTKReqMahJongCastDice
{
	TKHEADER header ;
	int nSeat ;				// 掷骰子的玩家座位号
	int nCastDiceType ;		// 掷骰子的类型，取值为CAST_DICE_TYPE_XXX，如果首位为1，表示这次掷骰子会获得一定的奖励
	int cnDice;				// 骰子数目
	// 消息后跟 cnDice 个骰子的点数信息，每个点数是一个int
} TKREQMAHJONGCASTDICE , *PTKREQMAHJONGCASTDICE ;



// =================================================================================================
// 
// 【开牌消息】
// 掷骰子之后，庄家开始摸牌，其他玩家按照游戏座位依次抓牌；玩家在抓完自己的牌之后，发送ACK的消息给服务器端
// 
// =================================================================================================
#define TK_MSG_MAHJONG_OPEN_DOOR	( TK_MSG_GAME_MJJSPK + 0x12 )
// server->client
typedef struct tagTKReqMahJongOpenDoor
{
	TKHEADER header ;
	int nSeat;					// 这是nSeat这个位置的玩家的数据
	int nOpenDoorSeat ;			// 从这个座位号的玩家手中的牌开始开牌
	int nOpenDoorFrusta ;		// 从nOpenDoorSeat玩家手中从右开始的这墩牌开始开牌（从0开始计数）
	int cnHandTile;				// 要抓的手中牌的张数
	// 消息后跟的是 cnHandTile 张这个玩家抓的牌的ID号信息，每张牌是一个int
	// 抓牌时，应该参考 TK_MSG_MAHJONG_BASE_INFO 消息的 TKREQMAHJONGBASEINFO 结构中的 nDrawFrustaEachTime、
	// nDrawFrustaTimes
	// 如果nSeat是庄家，那么牌的张数是：nDrawFrustaEachTime * nDrawFrustaTimes + 2
	// 如果nSeat是闲家，那么牌的张数是：nDrawFrustaEachTime * nDrawFrustaTimes + 1
} TKREQMAHJONGOPENDOOR , *PTKREQMAHJONGOPENDOOR ;
// client->server
typedef struct tagTKAckMahJongOpenDoor
{
	TKHEADER header ;			// header.dwResult无意义
	int nSeat ;					// 座位号
} TKACKMAHJONGOPENDOOR , *PTKACKMAHJONGOPENDOOR ;

typedef struct tagTKReqMahJongOpenDoorEx
{
	TKHEADER header ;
	int nSeat;					// 这是nSeat这个位置的玩家的数据
	int nOpenDoorSeat ;			// 从这个座位号的玩家手中的牌开始开牌
	int nOpenDoorFrusta ;		// 从nOpenDoorSeat玩家手中从右开始的这墩牌开始开牌（从0开始计数）
	int acnHandTile[ MAX_PLAYER_COUNT ]; // 每个人手中牌张数
	int apnHandTile[ MAX_PLAYER_COUNT ][ MAX_HAND_COUNT ];	// 每个人手中的牌
} TKREQMAHJONGOPENDOOREX , *PTKREQMAHJONGOPENDOOREX ;


// =================================================================================================
// 
// 【翻混消息】
// 开牌之后，有些麻将中需要翻混。一般都是翻开一张牌，然后根据这张牌来指定一张（或者若干张）牌为混牌
// 
// =================================================================================================
#define TK_MSG_MAHJONG_HUN			( TK_MSG_GAME_MJJSPK + 0x13 )
// server->client
typedef struct tagTKReqMahJongHun
{
	TKHEADER header ;
	BOOL bFromWallHeader ;	// 是否从牌墙的前头开始翻混（否则从牌尾位置翻混）
	int nTileOffset ;		// 从牌墙前头或者牌尾开始翻第几张牌（从0开始计数）
	int nTileID ;			// 翻开的牌的ID号（这张牌不一定是混牌）
	int cnHunTile ;			// 混牌的张数
	// 消息后跟 cnHunTile 个混牌的ID号信息，每张牌是一个int。注意的是，因为这个混牌并不是真正发的牌，
	// 所以这个ID号中的的低4位都为1
} TKREQMAHJONGHUN , *PTKREQMAHJONGHUN ;



// =================================================================================================
// 
// 【请求操作消息】
// 当服务器请求客户端执行某个操作（比如出牌、询问是否吃碰杠和牌等）时发送REQ的消息给所有客户端
// 如果玩家可以执行该操作，则直接发送该操作的消息，否则（玩家不能执行该操作）玩家向服务器返回ACK
// 的消息
// 在有人出牌之后，如果服务器在收到了玩家的吃碰杠和消息，会给其他玩家发送ACK的这个消息，其中
// nGiveUpRequestType表示的是玩家可以放弃的请求。比如有人出牌后，玩家A要碰牌，那么服务器会发送
// nGiveUpRequestType为吃牌的请求，
// 如果玩家B当前只可以吃牌，在收到这个消息后，就可以直接取消而不用等待超时或者玩家的操作了。
// 
// 可能的请求类型组合为：
// 1.补花                            补花阶段    要某人补花，询问这个玩家
// 2.听牌                            游戏阶段    一开始游戏，庄家出第一张牌之前，询问其他几家是否要听牌（天听）
// 3.补花＋听牌＋出牌＋杠牌＋和牌    游戏阶段    某人抓牌后，询问这个抓牌的玩家
// 4.吃牌＋碰牌＋杠牌＋和牌          游戏阶段    某人出牌后，询问其他几个玩家
// 5.和牌                            游戏阶段    某人要杠牌时，询问其他几个玩家
// 6.出牌＋听牌                      游戏阶段    某人吃碰牌之后，请求这个玩家出牌
// 
// 客户端在判断这些请求类型时，应该先判断补花，然后才判断其他的类型。
// 
// =================================================================================================
#define TK_MSG_MAHJONG_REQUEST		( TK_MSG_GAME_MJJSPK + 0x14 )
// server->client
#define REQUEST_TYPE_NULL				( 0 )		// 空
#define REQUEST_TYPE_CHI				( 1 << 0 )	// 请求吃牌
#define REQUEST_TYPE_PENG				( 1 << 1 )	// 请求碰牌
#define REQUEST_TYPE_GANG				( 1 << 2 )	// 请求杠牌
#define REQUEST_TYPE_CALL				( 1 << 3 )	// 请求听牌
#define REQUEST_TYPE_WIN				( 1 << 4 )	// 请求和牌
#define REQUEST_TYPE_DISCARD_TILE		( 1 << 5 )	// 请求出牌
#define REQUEST_TYPE_CHANGE_FLOWER		( 1 << 6 )	// 请求补花
#define REQUEST_TYPE_DOUBLE				( 1 << 7 )	// 请求加倍
// server->client
typedef struct tagTKReqMahJongRequest
{
	TKHEADER header ;
	int nSeat ;					// 被请求玩家的座位号
	int nRequestID ;			// 标识这次请求的ID号
	int nRequestType ;			// 请求的类型（为上述REQUEST_TYPE_XXX的并）
	int nWaitSecond ;			// 等待的时间（单位：秒）
} TKREQMAHJONGREQUEST , *PTKREQMAHJONGREQUEST ;
// client->server
typedef struct tagTKAckMahJongRequest
{
	TKHEADER header ;			// header.dwResult无意义
	int nSeat ;					// (client->server)返回这个请求信息的玩家座位号 (server->client)通知的这个玩家的座位号
	int nRequestID ;			// 请求的ID号（这个ID号应该和 TKREQMAHJONGREQUEST 中的 nRequestID 相同）
	int nGiveUpRequestType ;	// (client->server)玩家放弃的请求类型（指如果玩家可以执行某个操作，不过玩家选择了取消）
	/// 取值为0或者上述REQUEST_TYPE_XXX的并
	// (server->client)标识可以取消的操作类型，如果玩家只能进行这些操作，那么可以直接放弃
	/// 取值为REQUEST_TYPE_XXX的并
} TKACKMAHJONGREQUEST , *PTKACKMAHJONGREQUEST ;



// =================================================================================================
// 
// 【补花消息】
// 翻混之后，进入补花阶段，从庄家开始，依次补花。首先是服务器发送请求补花操作（TK_MSG_MAHJONG_REQUEST
// 消息），如果玩家要补花则发送这个消息给服务器端，然后服务器转发这个消息给所有客户端。
// 行牌过程中，如果玩家抓到一张花牌，服务器也会先请求该玩家补花，然后客户端发送这个消息给服务器，服务
// 器转发这个消息给所有客户端。
// 服务器转发这个消息之后，再发送一个抓牌消息给客户端。
// 补花阶段，如果玩家有多张花牌，那么是一张一张的补花，直到玩家手中没有花牌为止。
// 
// =================================================================================================
#define TK_MSG_MAHJONG_CHANGE_FLOWER	( TK_MSG_GAME_MJJSPK + 0x15 )
// client->server->client
typedef struct tagTKReqMahJongChangeFlower
{
	TKHEADER header ;
	int nSeat ;			// 补花玩家的座位号
	int nRequestID ;	// 先前请求消息中的请求ID号
	int nTileID ;		// 花牌的ID号
} TKREQMAHJONGCHANGEFLOWER , *PTKREQMAHJONGCHANGEFLOWER ;



// =================================================================================================
// 
// 【出牌消息】
// 客户端先发送REQ的出牌消息给服务器端，服务器转发这个REQ的消息给所有的用户
// ** 注意 **
// 客户端出牌除了这个消息之外，在听牌消息中，也可能会有一个出牌的动作。
// 
// =================================================================================================
#define TK_MSG_MAHJONG_DISCARD_TILE	( TK_MSG_GAME_MJJSPK + 0x16 )
// client->server->client
typedef struct tagTKReqMahJongDiscardTile
{
	TKHEADER header ;
	int nSeat ;			// 出牌的玩家座位号
	int nRequestID ;	// 先前请求消息中的请求ID号
	int nTileID ;		// 出牌的ID号
	BOOL bHide;			// 是否隐藏这张牌 
} TKREQMAHJONGDISCARDTILE , *PTKREQMAHJONGDISCARDTILE ;



// =================================================================================================
// 
// 【抓牌消息】
// 从牌墙中抓一张牌
// 玩家抓牌后服务器还会发送一个请求操作消息，请求这个抓牌的玩家进行相应的操作（比如出牌、补花、使用
// 道具、杠牌、和牌等）
// 
// =================================================================================================
#define TK_MSG_MAHJONG_DRAW_TILE		( TK_MSG_GAME_MJJSPK + 0x17 )
// server->client
#define DRAW_TILE_TYPE_NULL				( 0 )	// 空
#define DRAW_TILE_TYPE_CHANGE_FLOWER_1	( 1 )	// 补花阶段，补花后抓牌
#define DRAW_TILE_TYPE_NORMAL			( 2 )	// 普通的抓牌（上家出牌之后自己抓牌）
#define DRAW_TILE_TYPE_CHANGE_FLOWER_2	( 3 )	// 游戏阶段，补花后抓牌
#define DRAW_TILE_TYPE_GANG				( 4 )	// 杠后抓牌
typedef struct tagTKReqMahJongDrawTile
{
	TKHEADER header ;
	int nSeat ;				// 抓牌玩家座位号
	int nDrawTileType ;		// 抓牌类型，取值为DRAW_TILE_TYPE_XXX
	BOOL bFromWallHeader ;	// 是否从牌墙的前头开始抓牌（否则从牌尾位置抓牌）
	int nTileOffset ;		// 从牌墙前头或者牌尾开始抓第几张牌（从0开始计数）
	int nTileID ;			// 牌的ID号
} TKREQMAHJONGDRAWTILE , *PTKREQMAHJONGDRAWTILE ;



// =================================================================================================
// 
// 【听牌消息】
// 玩家听牌时，发送这个消息给服务器端，服务器端认可后转发给所有客户端
// ** 注意 **
// 1.听牌的时候，服务器并不对听牌的合法性进行检查（是否真的可以听牌），而总是认可玩家发送的这个听牌消息。
// 
// =================================================================================================
#define TK_MSG_MAHJONG_CALL			( TK_MSG_GAME_MJJSPK + 0x18 )
// client->server->client
typedef struct tagTKReqMahJongCall
{
	TKHEADER header ;
	int nSeat ;				// 听牌玩家的座位号
	int nRequestID ;		// 先前请求消息中的请求ID号
	int nTileID ;			// 听牌玩家听牌后打出的牌的ID号，如果是判断天听（庄家出牌之前询问各客户端是否听牌），
							/// 那么返回的消息中，这个值为0
	BOOL bWinSelf ;			// 是否只和自摸
	BOOL bAutoGang ;		// 是否自动杠牌（包括明杠、暗杠，所有自己可能的杠牌。从而有可能杠牌之后反而变得不能听牌了）
	int  nCallType ;		// 听牌类型，取值为1－普通听牌，2－天听；这项由服务器填写，分发给客户端
	BOOL bHide;				// 是否隐藏这张牌 
} TKREQMAHJONGCALL , *PTKREQMAHJONGCALL ;



// =================================================================================================
// 
// 【吃牌消息】
// 玩家要吃牌时，玩家先发送这个消息给服务器端，服务器端认可后转发这个消息给所有客户端
// 
// =================================================================================================
#define TK_MSG_MAHJONG_CHI			( TK_MSG_GAME_MJJSPK + 0x19 )
// client->server->client
typedef struct tagTKReqMahJongChi
{
	TKHEADER header ;
	int nSeat ;				// 要吃牌的玩家座位号
	int nRequestID ;		// 先前请求消息中的请求ID号
	int nTileID ;			// 要吃的这张牌的ID号
	int anChiTileID[ 3 ] ;	// 这个顺子中三张牌的ID号
} TKREQMAHJONGCHI , *PTKREQMAHJONGCHI ;



// =================================================================================================
// 
// 【碰牌消息】
// 玩家要碰牌时，发送这个消息给服务器端，服务器端认可后转发这个消息给所有客户端
// 
// =================================================================================================
#define TK_MSG_MAHJONG_PENG			( TK_MSG_GAME_MJJSPK + 0x1A )
// client->server->client
typedef struct tagTKReqMahJongPeng
{
	TKHEADER header ;
	int nSeat ;				// 要碰牌的玩家座位号
	int nRequestID ;		// 先前请求消息中的请求ID号
	int nTileID ;			// 要碰的这张牌的ID号
	int anPengTileID[ 3 ] ;	// 这个刻子中三张牌的ID号
} TKREQMAHJONGPENG , *PTKREQMAHJONGPENG ;



// =================================================================================================
// 
// 【杠牌消息】
// 客户端要杠牌，发送这个消息给服务器端，服务器端认可后分发这个消息给所有的客户端。
// 
// =================================================================================================
#define TK_MSG_MAHJONG_GANG			( TK_MSG_GAME_MJJSPK + 0x1B )
// client->server->client
#define GANG_TYPE_PUBLIC(type)		((type)&=(~1))				// 设置明杠
#define GANG_TYPE_HIDDEN(type)		((type)|=1)					// 设置暗杠
#define GANG_TYPE_DIRECT(type)		((type)&=(~2))				// 设置直杠
#define GANG_TYPE_PATCH(type)		((type)|=2)					// 设置补杠
#define IS_GANG_TYPE_PUBLIC(type)	(0==((type)&0x01))			// 判断是否是明杠
#define IS_GANG_TYPE_HIDDEN(type)	(1==((type)&0x01))			// 判断是否是暗杠
#define IS_GANG_TYPE_DIRECT(type)	(0==(((type)>>1)&0x01))		// 判断是否是直杠
#define IS_GANG_TYPE_PATCH(type)	(1==(((type)>>1)&0x01))		// 判断是否是补杠
typedef struct tagTKReqMahJongGang
{
	TKHEADER header ;
	int nSeat ;				// 要杠牌的玩家座位号
	int nRequestID ;		// 先前请求消息中的请求ID号
	int nTileID ;			// 要杠的这张牌的ID号
	int nGangType ;			// 杠牌的类型，按位取值
	///        |
	///      值|   0         1
	///        |
	/// bit位  |
	/// -------|---------------------------------------------------
	///   0    |  明杠      暗杠
	///   1    |  直杠      补杠
	///        |
	int anGangTileID[ 4 ] ;	// 要杠的这四张牌的ID号
} TKREQMAHJONGGANG , *PTKREQMAHJONGGANG ;



// =================================================================================================
// 
// 【和牌消息】
// 玩家要和牌时，发送这个REQ消息给服务器端，服务器端认可后，发送ACK消息给所有客户端。
// 如果是诈和，那么诈和的玩家成为相公，可以继续游戏并吃碰杠牌，但是不能再听牌、和牌。
// 
// =================================================================================================
typedef struct tagScoreDetail 
{
	int anScore[ 7 ];	// 5手得分 + 理论得分 + 实际得分
	int nLeftScore;		// 剩余分数
}SCOREDETAIL, *PSCOREDETAIL;
typedef struct tagScoreDetailEx 
{
	int anScore[ 8 ];	// 5手得分 + 理论得分 + 实际得分 + 底分
	int nLeftScore;		// 剩余分数
}SCOREDETAILEX, *PSCOREDETAILEX;
#define TK_MSG_MAHJONG_WIN			( TK_MSG_GAME_MJJSPK + 0x1C )
// client->server
typedef struct tagTKReqMahJongWin
{
	TKHEADER header ;
	int nSeat ;					// 和牌玩家的座位号
	int nRequestID ;			// 先前请求消息中的请求ID号
	int nPaoSeat ;				// 放炮玩家的座位号，如果是自摸，那么这个座位号和上述的nSeat相同
	int nTileID ;				// 和的哪张牌
	int					nResultant;			// 和牌牌型，取值为define.h文件中的NORMAL、PENPENHU等
	int					cnGroups;			// 总共的分组数（包括吃碰杠的分组）
	int					cnShowGroups;		// 吃碰杠的分组数
	MAHJONGGROUPTILE	asGroup[6];			// 牌型组合方式，最多6组
	MAHJONGTILE			asHandTile[MAX_HAND_COUNT];// 手中牌，对不能分组的牌型，这里保存手中牌的最终信息
} TKREQMAHJONGWIN , *PTKREQMAHJONGWIN ;
// server->client
typedef struct tagTKAckMahJongWin
{
	TKHEADER header ;			
	int anWinSeat[ MAX_PLAYER_COUNT - 1 ];// 和牌玩家
	int cnWinSeat;				// 和牌玩家个数
	int	nPaoSeat;				// 点炮玩家(如果等于-1说明是自摸)
	int nWinTile;				// 和的那张牌
	int cnDouble[ MAX_PLAYER_COUNT - 1 ];	// 和牌玩家加倍次数(默认为0)
	int anWinMode[ MAX_PLAYER_COUNT - 1 ];// 和牌方式
} TKACKMAHJONGWIN , *PTKACKMAHJONGWIN ;



// =================================================================================================
// 
// 【和牌详细消息】
// 
// =================================================================================================
#define TK_MSG_MAHJONG_WINDETAIL				( TK_MSG_GAME_MJJSPK + 0x1D )
typedef struct tagTKAckMahjongWinDetail 
{
	TKHEADER header ;			
	int nWinSeat;				// 和牌玩家
	int	nPaoSeat;				// 点炮玩家(如果等于nWinSeat说明是自摸)
	int nWinTile;				// 和的那张牌
	WININFO sWinInfo;			// 和牌详细信息
	int nBaseScore;				// 基数
	SCOREDETAIL asScoreDetail[ MAX_PLAYER_COUNT ];	// 这一次和牌得分详细信息
	int nTime;
} TKACKMAHJONGWINDETAIL, *PTKACKMAHJONGWINDETAIL;

#define TK_MSG_MAHJONG_WINDETAILEX				( TK_MSG_GAME_MJJSPK + 0x1E )
typedef struct tagTKAckMahjongWinDetailEx 
{
	TKHEADER header ;			
	int nWinSeat;				// 和牌玩家
	int winDouble;				// 和牌玩家加倍次数
	int	nPaoSeat;				// 点炮玩家(如果等于nWinSeat说明是自摸)
	int nWinTile;				// 和的那张牌
	WININFO sWinInfo;			// 和牌详细信息
	int nBaseScore;				// 基数
	SCOREDETAILEX asScoreDetail[ MAX_PLAYER_COUNT ];	// 这一次和牌得分详细信息
	int nTime;
} TKACKMAHJONGWINDETAILEX, *PTKACKMAHJONGWINDETAILEX;

#define TK_MSG_MAHJONG_SPECIALCOUNT				( TK_MSG_GAME_MJJSPK + 0x1F )
typedef struct tagTKAckMahjongSpecialCount
{
	TKHEADER header ;	
	int nSpecialCount;		// 加番牌个数
} TKACKMAHJONGSPECIALCOUNT, *PTKACKMAHJONGSPECIALCOUNT;


// =================================================================================================
// 
// 【通知消息】
// 为了在多种麻将中，统一处理某些事件，这里对一些时刻进行通知。
// 
// =================================================================================================
#define TK_MSG_MAHJONG_NOTIFY					( TK_MSG_GAME_MJJSPK + 0x21 )
// server->client
#define MAHJONG_NOTIFY_START_SHUFFLE	( 1 )	// 开始洗牌（用于通知客户端可以播放洗牌的声音了）
#define MAHJONG_NOTIFY_DEAL_FINISH		( 2 )	// 发牌完毕（用于在不同麻将中通知客户端可以第一次整理手中的牌了）
#define MAHJONG_NOTIFY_NO_MORE_TILE		( 3 )	// 牌墙中没牌了（荒庄了，这个通知消息是在玩家抓牌时牌墙中没牌了才发送的）
#define MAHJONG_NOTIFY_DISCARD_START	( 4 )	// 开始出牌了（让庄家出第一张牌了）
#define MAHJONG_NOTIFY_DISCARD_FINISH	( 5 )	// 出牌结束了（这盘牌结束了）
#define MAHJONG_NOTIFY_DISABLE_RECEIVE	( 6 )	// 暂停接收网络消息，消息后跟一个int，表示暂停接收的时间（单位：毫秒）
#define	MAHJONG_NOTIFY_ERROR			( 7 )	// 错误提示
#define MAHJONG_NOTIFY_PLAYER_NOCHIP	( 8 )	// 玩家筹码输完

typedef struct tagTKReqMahJongNotify
{
	TKHEADER header ;
	int nNotify ;
	// 当nNotify为MAHJONG_NOTIFY_ERROR时，消息后跟一个以0结尾的char*字符串。
	// 当nNotify为MAHJONG_NOTIFY_DISABLE_RECEIVE时，消息后跟一个int。
} TKREQMAHJONGNOTIFY , *PTKREQMAHJONGNOTIFY ;


// =================================================================================================
// 
// 【托管消息】
// 
// 
// =================================================================================================
#define TK_MSG_TRUSTPLAY						( TK_MSG_GAME_MJJSPK + 0x22 )
enum{ UNTRUST, TRUST_SMART, TRUST_OVERTIME, TRUST_OFFLINE, TRUST_EXIT };
typedef struct tagTKReqTrustPlay 
{
	TKHEADER header;
	DWORD dwUserID;
	DWORD dwType;
} TKREQTRUSTPLAY, *PTKREQTRUSTPLAY;


// =================================================================================================
// 
// 【分数变化消息】
// 
// 
// =================================================================================================
#define TK_MSG_SCORE_CHANGE						( TK_MSG_GAME_MJJSPK + 0x23 )
typedef struct tagTKReqScoreChange
{
	TKHEADER header;
	int anIncremental[ MAX_PLAYER_COUNT ];
	int type;
} TKREQSCORECHANGE, *PTKREQSCORECHANGE;


//
// 亮某玩家牌消息
//
#define TK_MSG_PLAYER_TILES						( TK_MSG_GAME_MJJSPK + 0x24)
// server->client
typedef struct tagTKAckPlayerTiles
{
	TKHEADER header;
	
	int seat;
	int cnHandTile;									// 手上剩下的牌张数
	int	anHandTile[ MAX_HAND_TILE_COUNT ];			// 手上剩下的牌
}TKACKTILES, *PTKACKTILES;

//
// 某玩家加倍消息
//
#define TK_MSG_DOUBLING						( TK_MSG_GAME_MJJSPK + 0x25)
// client->server
typedef struct tagTKReqMahjongDbl
{
	TKHEADER header;

	int seat;
	int nRequestID;
	bool isDouble;
	
	int  dblType;				// 加倍类型
	TKREQMAHJONGWIN dblMsg;		// 检测
}TKREQMAHJONGDBL, *PTKREQMAHJONGDBL;
// server->client
typedef struct tagTKAckDoubling
{
	TKHEADER header;

	int seat;
	int count;
	int tileId; // 和牌的ID
}TKACKDOUBLING, *PTKACKDOUBLING;

//
// 某玩家吃碰杠的牌消息（暗杠）
//
#define TK_MSG_PLAYER_SHOWTILES						( TK_MSG_GAME_MJJSPK + 0x26)
// server->client
typedef struct tagTKAckPlayerShowTiles
{
	TKHEADER header;

	int seat;
	int cnShow;
	int	anShowTiles[24];			// 吃碰杠组数
}TKACKSHOWTILES, *PTKACKSHOWTILES;


//C-->S 客户端请求服务器发起超时检测
//S-->C 服务器通知客户端发起超时检测
#define TK_MSG_CHECKTIMEOUT						(TK_MSG_GAME_MJJSPK + 0x27)
typedef struct tagTKREQCHECKTIMEOUT
{
	TKHEADER header;
	DWORD m_dwUserID;							// 断线超时玩家ID
	int nWaitSecond ;							// 等待的时间（单位：秒）
}TKREQCHECKTIMEOUT, *PTKREQCHECKTIMEOUT;

// 听牌信息
#define TK_MSG_MAHJONG_CALLTILE				( TK_MSG_GAME_MJJSPK + 0x28 )
// client->server
typedef struct tagTkReqMahjongCallTile
{
	TKHEADER header ;
	int nSeat ;				// 听牌玩家的座位号
	int nRequestID ;		// 先前请求消息中的请求ID号
	int	cnCallTileCount;	// 可和的牌张数
	int nCallTileID[34];	// 听牌后所能胡的牌
}TKREQMAHJONGCALLTILE, *PTKREQMAHJONGCALLTILE;

// 加番牌信息
#define TK_MSG_MAHJONG_SPECIALTILE				( TK_MSG_GAME_MJJSPK + 0x29 )
// server->client
typedef struct tagTkAckMahjongSpecialTile
{
	TKHEADER header ;
	int nSpecialTileID;		// 加番牌ID号
	int	nSpecialTileFan;	// 加番牌番数
	int nSpecialTileCount;	// 加番牌个数
}TKACKMAHJONGSPECIALTILE, *PTKACKMAHJONGSPECIALTILE;

// 奖花消息
#define TK_MSG_MAHJONG_LUCKYTILE				( TK_MSG_GAME_MJJSPK + 0x2A )
// server->client
typedef struct tagTkAckMahjongLuckyTile
{
	TKHEADER header ;
	int nLuckyTileCount; // 奖花牌数量
	int anLuckyTileID[20]; // 奖花牌ID
	int nHitTileCount; // 中奖的牌数量
	int anHitTileID[20];	// 中奖的牌ID
	int nEachLuckyTileFan; // 每张中奖牌的番数
}TKACKMAHJONGLUCKYTILE, *PTKACKMAHJONGLUCKYTILE;

//
// 二人麻将 protobuf 协议
// TKHEADER+MsgBody(protobuf)，通过TKHEADER中的dwType确定是否是protobuf的协议，游戏内部消息通过protobuf属性（消息）是否存在判断
//
#define TK_MSG_MAHJONG_PROTOBUF									( TK_MSG_GAME_MJJSPK ) //128 = 0x80
// server->client
typedef struct tagTkAckMahjongProtobufAck
{
	TKHEADER header ;
	cn::jj::service::msg::protocol::TKMobileAckMsg protoAckMsg;
}TKACKMAHJONGPROTOBUF, *PTKACKMAHJONGPROTOBUF;

typedef struct tagTkAckMahjongProtobufReq
{
	TKHEADER header ;
	cn::jj::service::msg::protocol::TKMobileReqMsg protoReqMsg;
}TKREQMAHJONGPROTOBUF, *PTKREQMAHJONGPROTOBUF;


// 暂时放在这里
enum EventID
{
	//
	EVENT_INTERNEL_BEGIN = 1,	// 内部事件开始ID

	// 内部事件
	EVENT_START_GAME,			// 开始游戏
	EVENT_SHUFFLE_END,			// 洗牌结束
	EVENT_CASTDICE_END,			// 掷色子结束
	EVENT_OPENDOOR_END,			// 开门结束
	EVENT_DECIDEHUN_END,		// 定混结束
	EVENT_NOTILE,				// 没牌了
	EVENT_CHANGEALLFLOWER_END,	// 开始打牌之前的补花结束
	EVENT_TIANTING_END,			// 询问天听结束
	EVENT_BUGANG,				// 补杠
	EVENT_WINGANG,				// 抢杠
	EVENT_NOWINGANG,			// 没人抢杠
	EVENT_ALLABORT,				// 所有人都放弃吃碰杠和
	EVENT_CHI,					// 有人吃
	EVENT_PENG,					// 有人碰
	EVENT_GANG,					// 有人杠
	EVENT_DOUBLE,				// 有人加倍
	EVENT_WIN_PAO,				// 有人和牌( 点炮 )
	EVENT_WIN_SELF,				// 有人和牌( 自摸 )
	EVENT_DRAWSUCCEED,			// 抓牌成功
	EVENT_1PLAYER_NOT_WIN,		// 还剩1个玩家未和牌
	EVENT_2PLAYER_NOT_WIN,		// 还剩2个玩家未和牌
	EVENT_3PLAYER_NOT_WIN,		// 还剩3个玩家未和牌
	EVENT_LUCKY_TILE,		// 奖花

	//
	EVENT_INTERNEL_END = TK_MSG_GAME_MJJSPK,// 内部事件结束ID



	// 与网络有关的事件
	EVENT_BASEINFO = REQ_TYPE( TK_MSG_MAHJONG_BASE_INFO ),
	EVENT_VERSION = REQ_TYPE( TK_MSG_MAHJONG_VERSION ),
	EVENT_FAN_TREE = REQ_TYPE( TK_MSG_MAHJONG_FAN_TREE ),
	EVENT_PLACE = REQ_TYPE( TK_MSG_MAHJONG_PLACE ),
	EVENT_WIND_PLACE = REQ_TYPE( TK_MSG_MAHJONG_WIND_PLACE ),
	EVENT_REQ_RESULT = REQ_TYPE( TK_MSG_MAHJONG_RESULTEX ),
	EVENT_ACK_RESULT = ACK_TYPE( TK_MSG_MAHJONG_RESULTEX ),
	EVENT_SHUFFLE = REQ_TYPE( TK_MSG_MAHJONG_SHUFFLE ),
	EVENT_CAST_DICE = REQ_TYPE( TK_MSG_MAHJONG_CAST_DICE ),
	EVENT_REQ_OPEN_DOOR = REQ_TYPE( TK_MSG_MAHJONG_OPEN_DOOR ),
	EVENT_ACK_OPEN_DOOR = ACK_TYPE( TK_MSG_MAHJONG_OPEN_DOOR ),
	EVENT_HUN = REQ_TYPE( TK_MSG_MAHJONG_HUN ),
	EVENT_REQ_REQUEST = REQ_TYPE( TK_MSG_MAHJONG_REQUEST ),
	EVENT_ACK_REQUEST = ACK_TYPE( TK_MSG_MAHJONG_REQUEST ),
	EVENT_CHANGE_FLOWER = REQ_TYPE( TK_MSG_MAHJONG_CHANGE_FLOWER ),
	EVENT_DISCARD_TILE = REQ_TYPE( TK_MSG_MAHJONG_DISCARD_TILE ),
	EVENT_DRAW_TILE = REQ_TYPE( TK_MSG_MAHJONG_DRAW_TILE ),
	EVENT_CALL = REQ_TYPE( TK_MSG_MAHJONG_CALL ),
	EVENT_REQ_CHI = REQ_TYPE( TK_MSG_MAHJONG_CHI ),
	EVENT_REQ_DOUBLE = REQ_TYPE( TK_MSG_DOUBLING ),
	EVENT_REQ_PENG = REQ_TYPE( TK_MSG_MAHJONG_PENG ),
	EVENT_REQ_GANG = REQ_TYPE( TK_MSG_MAHJONG_GANG ),
	EVENT_REQ_WIN = REQ_TYPE( TK_MSG_MAHJONG_WIN ), 
	EVENT_ACK_WIN = ACK_TYPE( TK_MSG_MAHJONG_WIN ),
	EVENT_NOTIFY = REQ_TYPE( TK_MSG_MAHJONG_NOTIFY ),
	EVENT_TRUSTPLAY = REQ_TYPE( TK_MSG_TRUSTPLAY ),
	EVENT_CHECKTIMEOUT = REQ_TYPE( TK_MSG_CHECKTIMEOUT ),
	EVENT_CALLTILE = REQ_TYPE( TK_MSG_MAHJONG_CALLTILE ),

	EVENT_COUNT
};


