// QuadrantTree.cpp: implementation of the CQuadrantTree class.
//
//////////////////////////////////////////////////////////////////////

#include <winsock2.h>
#include <windows.h>
#include <assert.h>
#include "QuadrantTree.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#ifndef ASSERT
#define ASSERT	assert
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CQuadrantTree::CQuadrantTree()
{
}

CQuadrantTree::~CQuadrantTree()
{

}

// **************************************************************************************
// 
// 初始化四叉树
// 
// **************************************************************************************
void CQuadrantTree::Init()
{
	memset( m_asNode, 0, sizeof( m_asNode ) );
	memset( m_anLeafIndex, 0, sizeof( m_anLeafIndex ) );
	m_cnPath = 0;
}

// **************************************************************************************
// 
// 构造四叉树
// 传进来的麻将牌数组默认普通牌在前，混牌在后
// 
// **************************************************************************************
BOOL CQuadrantTree::Create( STONEGROUP &sRoot, STONE asStone[], int cnNormalStone, int cnHun )
{
	//ASSERT( cnNormalStone + cnHun <= 14 );
	
	Init();

	// 根结点
	m_asNode[0] = sRoot;

	if ( cnNormalStone + cnHun == 0 )
	{
		// 单钓，只有一对将
		m_cnPath = 1;

		return TRUE;
	}

	// 为避免出现重复的情况，在剩下的牌全是混时在本层下面构造一个左叶子结点，立即返回
	if ( cnNormalStone == 0 )
	{
		// 最多只会有5个混，在已有将的情况下，混的个数只能是3的整数倍
		ASSERT ( cnHun == 3 );
		CreateHunLeaf( 1, asStone );
		return TRUE;
	}

	BOOL bSuccess = CreateLeftChild( 0, LEFT1CHILD, asStone, cnNormalStone, cnHun );
	bSuccess |= CreateLeftChild( 0, LEFT2CHILD, asStone, cnNormalStone, cnHun );
	bSuccess |= CreateLeftChild( 0, LEFT3CHILD, asStone, cnNormalStone, cnHun );
	bSuccess |= CreateRightChild( 0, asStone, cnNormalStone, cnHun );

	return bSuccess;
}

// **************************************************************************************
// 
// 获得一条从根结点到叶节点的路径
// 
// **************************************************************************************
BOOL CQuadrantTree::GetPath( int nPathIndex, STONEGROUP asGroup[] )
{
	ASSERT( nPathIndex < m_cnPath && nPathIndex >= 0 );

	int cnGroup = 0;
	int nIndex = m_anLeafIndex[nPathIndex];
	while ( nIndex > 0 )
	{
		ASSERT( m_asNode[nIndex].asStone[0].nID != 0 );
		asGroup[cnGroup] = m_asNode[nIndex];
		nIndex = ( nIndex - 1 ) >> 2;
		cnGroup++;
	}

	// 再将根结点拷进去
	asGroup[cnGroup] = m_asNode[0];

	return TRUE;
}

// **************************************************************************************
// 
// 构造左边的三棵子树( 顺 )，nChild指明构造第几棵子树
// 第n棵子树取第一个牌作为顺的第n张
// 
// **************************************************************************************
BOOL CQuadrantTree::CreateLeftChild( int nParentIndex, int nChild, STONE asStone[], 
									int cnNormalStone, int cnHun )
{
	ASSERT( cnNormalStone != 0 && ( cnNormalStone + cnHun ) % 3 == 0 );

	if ( asStone[0].nColor >= 3 ) 
	{
		// 风牌箭牌肯定不能配成顺
		return FALSE;
	}

	// 当前结点下标
	int nIndex = ( nParentIndex << 2 ) + nChild + 1;
	// 初始化当前要用到的结点
	memset( m_asNode + nIndex, 0, sizeof( STONEGROUP ) );
	
	// 顺余牌数组
	STONE asSpareStone[ MAX_HAND_COUNT - 3 ] = { 0 };
	int cnSpareStone = cnNormalStone;
	int cnSpareHun = cnHun;	// 剩余的混
	if ( !CreateShun( asStone, cnSpareStone, cnSpareHun, nChild, m_asNode[nIndex], asSpareStone ) )
	{
		return FALSE;
	}

	// 本层成功
	ASSERT( ( cnSpareStone + cnSpareHun ) % 3 == 0 );
	if ( cnSpareStone + cnSpareHun >= 3 )
	{
		// 为避免出现重复的情况，在剩下的牌全是混时在本层下面构造一个左叶子结点，立即返回
		if ( cnSpareStone == 0 )
		{
			// 最多只会有5个混，在已有将的情况下，混的个数只能是3的整数倍
			ASSERT ( cnSpareHun == 3 );
			CreateHunLeaf( ( nIndex << 2 ) + 1, asStone + cnNormalStone );
			return TRUE;
		}
		
		BOOL bSuccess = CreateLeftChild( nIndex, LEFT1CHILD, 
			asSpareStone, cnSpareStone, cnSpareHun );
		bSuccess |= CreateLeftChild( nIndex, LEFT2CHILD, 
			asSpareStone, cnSpareStone, cnSpareHun );
		bSuccess |= CreateLeftChild( nIndex, LEFT3CHILD, 
			asSpareStone, cnSpareStone, cnSpareHun );
		bSuccess |= CreateRightChild( nIndex, asSpareStone, cnSpareStone, cnSpareHun );
		return bSuccess;
	}
	else
	{
		// 没牌了，本条路径可行
		m_anLeafIndex[m_cnPath] = nIndex;
		m_cnPath++;
	}
	
	return TRUE;
}

// **************************************************************************************
// 
// 构造右子树(刻)
// 
// **************************************************************************************
BOOL CQuadrantTree::CreateRightChild( int nParentIndex, STONE asStone[], 
									 int cnNormalStone, int cnHun )
{
	ASSERT( cnNormalStone != 0 && ( cnNormalStone + cnHun ) % 3 == 0 );

	// 当前结点下标
	int nIndex = ( nParentIndex << 2 ) + 4;
	
	// 顺余牌数组
	STONE asSpareStone[ MAX_HAND_COUNT - 3 ] = { 0 };
	int cnSpareStone = cnNormalStone;
	int cnSpareHun = cnHun;	// 剩余的混
	
	// 初始化当前要用到的结点
	memset( m_asNode + nIndex, 0, sizeof( STONEGROUP ) );
	m_asNode[nIndex].nGroupStyle = GROUP_STYLE_KE;
	m_asNode[nIndex].asStone[0] = asStone[0];

	int cnSameStone = 1;
	int i = 0;
	for(i = 1; i < cnNormalStone; i++ )
	{
		if ( asStone[0].nColor != asStone[i].nColor || asStone[0].nWhat != asStone[i].nWhat )
		{
			// 都是不相干的牌了
			break;
		}
		m_asNode[nIndex].asStone[cnSameStone] = asStone[i];
		cnSameStone++;
		if ( cnSameStone == 3 )
		{
			// 已找到3张了
			i++;// 指向下一张，方便统一处理，因为在上面的break出口，i 是指向下一张的
			break;
		}
	}

	if ( !PerfectGroup( m_asNode[nIndex], 0, asStone + cnNormalStone, cnSpareHun ) )
	{
		return FALSE;
	}

	// 本层成功，剩下的牌应该是中间的几张
	cnSpareStone -= cnSameStone;
	ASSERT( ( cnSpareStone + cnSpareHun ) % 3 == 0 );
	if ( cnSpareStone + cnSpareHun >= 3 )
	{
		// 为避免出现重复的情况，在剩下的牌全是混时在本层下面构造一个左叶子结点，立即返回
		if ( cnSpareStone == 0 )
		{
			// 最多只会有5个混，在已有将的情况下，混的个数只能是3的整数倍
			ASSERT ( cnSpareHun == 3 );
			CreateHunLeaf( ( nIndex << 2 ) + 1, asStone + cnNormalStone );
			return TRUE;
		}
		
		memcpy( asSpareStone, asStone + i, ( cnSpareStone + cnSpareHun ) * sizeof( STONE ) );
		
		BOOL bSuccess = CreateLeftChild( nIndex, LEFT1CHILD, 
			asSpareStone, cnSpareStone, cnSpareHun );
		bSuccess |= CreateLeftChild( nIndex, LEFT2CHILD, 
			asSpareStone, cnSpareStone, cnSpareHun );
		bSuccess |= CreateLeftChild( nIndex, LEFT3CHILD, 
			asSpareStone, cnSpareStone, cnSpareHun );
		bSuccess |= CreateRightChild( nIndex, asSpareStone, cnSpareStone, cnSpareHun );
		return bSuccess;
	}
	else
	{
		// 没牌了，本条路径可行
		m_anLeafIndex[m_cnPath] = nIndex;
		m_cnPath++;
	}
	
	return TRUE;
}

// **************************************************************************************
// 
// 构造一个全是混的叶子结点
// 
// **************************************************************************************
void CQuadrantTree::CreateHunLeaf( int nLeafIndex, STONE asHun[] )
{
	m_asNode[nLeafIndex].nGroupStyle = GROUP_STYLE_HUN;
	memcpy( m_asNode[nLeafIndex].asStone, asHun, 3 * sizeof( STONE ) );
	m_anLeafIndex[m_cnPath] = nLeafIndex;
	m_cnPath++;
}

// **************************************************************************************
// 
// 给定一个牌数组，以这个牌数组的第一张牌为基础，构造一个顺牌分组，nBaseIndex指明第一张牌
// 在这个分组里要放置的位置
// 
// **************************************************************************************
BOOL CQuadrantTree::CreateShun( STONE asStone[], int& cnNormalStone, int& cnHun, 
							   int nBaseIndex, STONEGROUP &sGroup, STONE asSpareStone[] )
{
	ASSERT( cnNormalStone > 0 && nBaseIndex >= 0 && nBaseIndex < 3 );
	
	// 因为asStone里的牌是从小到大排列的，如果取第一张作为顺的第二或第三张，必然要用混来补充
	if ( nBaseIndex > cnHun )
	{
		// nBaseIndex == 2 则要求 cnHun >= 2，nBaseIndex == 1 则要求 cnHun >= 1
		return FALSE;
	}
	
	if ( asStone[0].nWhat == STONE_NO8 && nBaseIndex == 0 // 第一张牌是8，不能作顺的第一张
		|| asStone[0].nWhat == STONE_NO9 && nBaseIndex < 2// 第一张牌是9，只能作顺的第三张 
		|| asStone[0].nWhat == STONE_NO1 && nBaseIndex > 0// 第一张牌是1，只能作顺的第一张 
		|| asStone[0].nWhat == STONE_NO2 && nBaseIndex == 2 )// 第一张牌是2，不能作顺的第三张
	{
		return FALSE;
	}

	sGroup.nGroupStyle = GROUP_STYLE_SHUN;
	sGroup.asStone[nBaseIndex] = asStone[0];
	int cnScrapStone = 0;
	
	// 如果取第一张作顺的第三张，不用找了，直接用混填充
	int i = 1; // 循环结束后i指向剩下的第一张牌
	if ( nBaseIndex != 2 )
	{
		for( i = 1; i < cnNormalStone; i++ )
		{
			if ( asStone[i].nColor != asStone[0].nColor )
			{
				// 后面都是不相干的牌了
				// 这个时候i指向的这个牌是剩下的第一个无用的牌
				break;
			}
			int nDiff = asStone[i].nWhat - asStone[0].nWhat;
			if ( nDiff > ( 2 - nBaseIndex ) )
			{
				// 后面都是不相干的牌了
				// 这个时候i指向的这个牌是剩下的第一个无用的牌
				break;
			}
			
			// 这张牌满足要求
			int nIndex = nBaseIndex + nDiff;
			if ( sGroup.asStone[nIndex].nID == 0 )
			{
				// 拷到分组里
				sGroup.asStone[nIndex] = asStone[i];
			}
			else
			{
				// 已经有这张牌了，拷到剩余牌数组里
				asSpareStone[cnScrapStone] = asStone[i];
				cnScrapStone++;
			}
			
			if ( nIndex == 2 )
			{
				// 最后一张牌也找到了，跳出
				// 这个时候i+1指向的牌才是剩下的第一个无用的牌
				i++;
				break;
			}
		}
	}
	else
	{
		// 没经过循环，i指向第二张牌（第一张牌已被用掉了），下面会将用不到的牌拷出去
		//i = 0;
		i = 1;
	}
	
	// 如果这个分组缺牌，用混填充
	int cnScrapHun = cnHun;	// 剩下的混数
	if ( !PerfectGroup( sGroup, nBaseIndex, asStone + cnNormalStone, cnScrapHun ) )
	{
		// 失败，这个分组无法填充完整
		return FALSE;
	}
	
	// 终于分好了，剩下的牌应该是中间的几张
	memcpy( asSpareStone + cnScrapStone, asStone + i /*+ 1*/, 
		( cnNormalStone + cnHun - i/* - 1 */) * sizeof( STONE ) );
	// 剩下的普通牌数 = 原普通牌数 - 3 + 用掉的混数
	cnNormalStone = cnNormalStone -  3 + cnHun - cnScrapHun;
	cnHun = cnScrapHun;
	
	return TRUE;

/*	下面的代码在for( i = 1; i < cnNormalStone; i++ )这个循环有三个跳出点，前两个跳出点和
	第三个跳出点i所指向的牌不同，前两个跳出点i指向剩下的第一个无用的牌，而第三个跳出点i+1
	指向的才是第一个无用的牌，而且，if ( nBaseIndex != 2 )的else分支为i赋值跟第三个跳出
	点相同。
	memcpy( asSpareStone + cnScrapStone, asStone + i + 1, ( cnNormalStone + cnHun - i - 1 ) * sizeof( STONE ) );
	这条语句是相对于第三个跳出点的处理，所以出错

	ASSERT( cnNormalStone > 0 && nBaseIndex >= 0 && nBaseIndex < 3 );

	// 因为asStone里的牌是从小到大排列的，如果取第一张作为顺的第二或第三张，必然要用混来补充
	if ( nBaseIndex > cnHun )
	{
		// nBaseIndex == 2 则要求 cnHun >= 2，nBaseIndex == 1 则要求 cnHun >= 1
		return FALSE;
	}

	sGroup.nGroupStyle = GROUP_STYLE_SHUN;
	sGroup.asStone[nBaseIndex] = asStone[0];
	int cnScrapStone = 0;

	// 如果取第一张作顺的第三张，不用找了，直接用混填充
	int i = 1; // 循环结束后i指向找到的最后一张牌
	if ( nBaseIndex != 2 )
	{
		for( i = 1; i < cnNormalStone; i++ )
		{
			if ( asStone[i].nColor != asStone[0].nColor )
			{
				// 后面都是不相干的牌了
				break;
			}
			int nDiff = asStone[i].nWhat - asStone[0].nWhat;
			if ( nDiff > ( 2 - nBaseIndex ) )
			{
				// 后面都是不相干的牌了
				break;
			}

			// 这张牌满足要求
			int nIndex = nBaseIndex + nDiff;
			if ( sGroup.asStone[nIndex].nID == 0 )
			{
				// 拷到分组里
				sGroup.asStone[nIndex] = asStone[i];
			}
			else
			{
				// 已经有这张牌了，拷到剩余牌数组里
				asSpareStone[cnScrapStone] = asStone[i];
				cnScrapStone++;
			}

			if ( nIndex == 2 )
			{
				// 最后一张牌也找到了，跳出
				break;
			}
		}
	}
	else
	{
		// 没经过循环，i指向第一张牌，下面会将用不到的牌拷出去
		i = 0;
	}
	
	// 如果这个分组缺牌，用混填充
	int cnScrapHun = cnHun;	// 剩下的混数
	if ( !PerfectGroup( sGroup, nBaseIndex, asStone + cnNormalStone, cnScrapHun ) )
	{
		// 失败，这个分组无法填充完整
		return FALSE;
	}

	// 终于分好了，剩下的牌应该是中间的几张
	memcpy( asSpareStone + cnScrapStone, asStone + i + 1, 
		( cnNormalStone + cnHun - i - 1 ) * sizeof( STONE ) );
	// 剩下的普通牌数 = 原普通牌数 - 3 + 用掉的混数
	cnNormalStone = cnNormalStone -  3 + cnHun - cnScrapHun;
	cnHun = cnScrapHun;

	return TRUE;
*/
}


// **************************************************************************************
// 
// 是否一个完备的分组，如果不完备，在需要的地方补充混
// 
// **************************************************************************************
BOOL CQuadrantTree::PerfectGroup( STONEGROUP &sGroup, int nBaseIndex, STONE asHun[], int &cnHun )
{
	for( int i = 0; i < 3; i++ )
	{
		if ( i == nBaseIndex )
		{
			continue;
		}
		if ( sGroup.asStone[i].nID == 0 )
		{
			if ( cnHun == 0 )
			{
				return FALSE;
			}
			// 在最后面取一张混
			cnHun--;
			sGroup.asStone[i] = asHun[cnHun];
			// 将这张混变成需要的牌
			sGroup.asStone[i].nColor = sGroup.asStone[nBaseIndex].nColor;
			ASSERT( sGroup.nGroupStyle == GROUP_STYLE_SHUN 
				|| sGroup.nGroupStyle == GROUP_STYLE_KE );
			if ( sGroup.nGroupStyle == GROUP_STYLE_SHUN )
			{
				sGroup.asStone[i].nWhat = sGroup.asStone[nBaseIndex].nWhat + i - nBaseIndex;
			}
			else
			{
				sGroup.asStone[i].nWhat = sGroup.asStone[nBaseIndex].nWhat ;
			}
		}
	}

	return TRUE;
}
