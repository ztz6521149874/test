#include ".\gamerule.h"
#include <assert.h>
#include <GTKFunc.h>
#include "TKMahjongProtocol.h"
#include "MahJongTile.h"
#include "string.h"
#include "tkmahjongdefine.h"

#ifndef ASSERT
#define ASSERT	assert
#endif

CGameRule::CGameRule(void)
{
}



// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
CGameRule::~CGameRule(void)
{
}



// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
char * CGameRule::GetIntFromString(char *s,int *pVar)
{
	if (s==NULL) return NULL;

	while ((*s==' ')||(*s==',')) s++;
	if (*s) *pVar=atoi(s);
	while ((*s!=' ') &&(*s!=',') && *s) s++;
	return s;
}




// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
bool CGameRule::InitInstance( TCHAR szProperty[] )
{
	// �Ƚ���property�ַ���
	m_MahJongConfig.Load(std::string(szProperty));

	for (int i = 0; i < MAX_HUN_COUNT ; i++ )
	{
		m_anHunTileID[ i ] = 0xFFFF;
	}

	return true;
}

// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
int CGameRule::GetRule( int nRuleMask )
{
	if ( nRuleMask >= RULE_ITEM_COUNT )
	{
		return 0;
	}

	return m_MahJongConfig.GetRule( nRuleMask );
}

int CGameRule::GetLuckTileFan(int nLuckCount)
{
    if (nLuckCount <= 0 || nLuckCount > m_MahJongConfig.GetRule(RULE_LUCKY_TILE_COUNT))
    {
        return 0;
    }
    int nRuleItem = RULE_LUCKY_TILE_FAN1 + nLuckCount - 1;
    return GetRule(nRuleItem);
}

// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
void CGameRule::SetRule( int nRuleMask, int nRule )
{
	if ( nRuleMask >= RULE_ITEM_COUNT )
	{
		return;
	}

	m_MahJongConfig.SetRule( nRuleMask, nRule );
}

// *********************************************************************************************************************
// 
// 
// 
// 
// *********************************************************************************************************************
void CGameRule::Sort(STONE asStone[], int cnStone)
{
    int k = 0;
    STONE sStone = { 0 };

    for (int i = cnStone - 1; i >= 0; i--)
    {
        k = i;
        for (int j = i - 1; j >= 0; j--)
        {
            if ((asStone[k].nColor << 4) + asStone[k].nWhat
                < (asStone[j].nColor << 4) + asStone[j].nWhat)
            {
                k = j;
            }
        }
        if (k != i)
        {
            sStone = asStone[k];
            asStone[k] = asStone[i];
            asStone[i] = sStone;
        }
    }
}






