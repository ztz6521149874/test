#include ".\publictilewall.h"
#include "MahJongTile.h"
#include "assert.h"

#ifndef ASSERT
#define ASSERT	assert
#endif

CPublicTileWall::CPublicTileWall(void)
{
	memset( m_apWallTile, 0, sizeof( m_apWallTile ) );
	memset( m_anFrustaCount, 0, sizeof( m_anFrustaCount ) );
	m_nCurWallTileIndex = -1;
	m_nLastWallTileIndex = -1;
	m_nHaidiTileIndex = -1;
	m_bNoTile = true;
}

CPublicTileWall::~CPublicTileWall(void)
{
}


void CPublicTileWall::ResetGameData()
{
	memset( m_apWallTile, 0, sizeof( m_apWallTile ) );
	memset( m_anFrustaCount, 0, sizeof( m_anFrustaCount ) );
	m_cnTile = 0;
	m_nCurWallTileIndex = -1;
	m_nLastWallTileIndex = -1;
	m_nHaidiTileIndex = -1;
	m_bNoTile = true;
}


void CPublicTileWall::Shuffle( ShuffleParam * pShuffleParam, int anFrustaOfSeat[] )
{
	for( int i = 0 ; i < pShuffleParam->cnTile ; i ++ )
	{ // 遍历所有的牌
		m_apWallTile[ i ] = pShuffleParam->ppWallTile[ i ] ;
	}

	m_cnTile = pShuffleParam->cnTile;

	m_nRealTile = m_cnTile;
	// 平分成四方,每方18墩牌
	/*FOR TWO PLAYER*/
	int nTileTotalCount = pShuffleParam->cnTile;
	int nFrusta = nTileTotalCount / 2;
	int nPerFrusta = nFrusta / MAX_PLAYER_COUNT;
	int nIndex = nFrusta % MAX_PLAYER_COUNT;
	m_anFrustaCount[0] = nPerFrusta + ((nIndex-- > 0) ? 1 : 0);
	m_anFrustaCount[1] = nPerFrusta + ((nIndex-- > 0) ? 1 : 0);
	memcpy( anFrustaOfSeat, m_anFrustaCount, sizeof( m_anFrustaCount ) );

	m_bNoTile = false;
}



CMahJongTile * CPublicTileWall::GetOneTile( bool bFromWallHeader , int nOffset )
{
	int nCurIndex = GetWallTileIndex( bFromWallHeader , nOffset ) ;

	CMahJongTile *pTile = m_apWallTile[ nCurIndex ] ;	// 取这个位置的这张牌
	m_apWallTile[ nCurIndex ] = NULL ; // 清除这个位置的牌

	/*if( 1 == ( nCurIndex % 2 ) )
	{ // 下层的牌被抓走了
		ASSERT( NULL == m_apWallTile[ nCurIndex - 1 ] ) ;	// 上层的牌也应该被抓走了
	}*/

	// 
	// 更改牌头和牌尾的索引号（因为抓牌可能把这两个位置的牌抓走了）
	// 
	int nTempIndex = -1 ;
	if( nCurIndex == m_nCurWallTileIndex )
	{ // 牌头位置的牌被抓走了
		if( nCurIndex == m_nHaidiTileIndex )
		{ // 如果这张牌是海底牌
			m_bNoTile = TRUE ;	// 设置牌墙中没牌了
		}

		nTempIndex = nCurIndex ;
		do
		{
			nTempIndex ++ ;
			if( nTempIndex >= m_cnTile )
			{ // 越界了
				nTempIndex = 0 ;
			}
		} while( NULL == m_apWallTile[ nTempIndex ] && nTempIndex != m_nCurWallTileIndex ) ;	// 如果这个位置有牌或者已经绕牌墙一圈了，那么退出循环
		m_nCurWallTileIndex = nTempIndex ;
	}
	if( nCurIndex == m_nLastWallTileIndex )
	{ // 牌尾位置的牌被抓走了
		nTempIndex = nCurIndex ;
		do
		{
			if( 0 == ( nTempIndex % 2 ) )
			{ // 上层的牌
				nTempIndex ++ ;
			}
			else
			{ // 下层的牌
				if( nTempIndex > 1 )
				{ // 不是第一墩
					nTempIndex -= 3 ; // 到前一墩的上层的牌
				}
				else
				{ // 第一墩下层的那张牌
					nTempIndex = m_cnTile - 2 ;	// 到牌尾那一墩的上层的牌
				}
			}
		} while( NULL == m_apWallTile[ nTempIndex ] && nTempIndex != m_nLastWallTileIndex ) ;	// 如果这个位置有牌或者已经绕牌墙一圈了，那么退出循环
		m_nLastWallTileIndex = nTempIndex ;

		// 修改海底牌的位置
		m_nHaidiTileIndex = m_nLastWallTileIndex ;
		if( m_nLastWallTileIndex % 2 == 0 )
		{ // 如果最后一墩的上层牌未抓走
			m_nHaidiTileIndex++ ;
		}
		ASSERT( m_bNoTile || m_apWallTile[ m_nHaidiTileIndex ] != NULL ) ;	// 要么牌墙中没牌了，要么海底位置还有牌
	}
	m_nRealTile--;
	return pTile ;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
int CPublicTileWall::GetWallTileIndex( bool bFromWallHeader , int nOffset )
{
	int nCurIndex = -1 ;

	if( bFromWallHeader )
	{ // 从牌头摸牌
		nCurIndex = m_nCurWallTileIndex ;	// 从牌头位置开始找
		while( nOffset > 0 )
		{ // 还没有找到这张牌
			if( NULL != m_apWallTile[ nCurIndex ] )
			{ // 这个位置有牌
				nOffset -- ;
			}

			// 移到下一张牌
			nCurIndex ++ ;	// 下一张就是索引号＋1的那张牌
			if( nCurIndex >= m_cnTile )
			{ // 到最后了
				nCurIndex = 0 ;	// 从第一张牌开始找
			}
		}
	}
	else
	{ // 从牌尾摸牌
		nCurIndex = m_nLastWallTileIndex ;	// 从牌尾位置开始找
		while( nOffset > 0 )
		{ // 还没有找到这张牌
			if( NULL != m_apWallTile[ nCurIndex ] )
			{ // 这个位置有牌
				nOffset -- ;
			}

			// 移到下一张牌
			if( 0 == ( nCurIndex % 2 ) )
			{ // 上层的牌
				nCurIndex ++ ;	// 这时，下一张牌就是索引号＋1的那张牌
			}
			else
			{ // 下层的牌
				if( nCurIndex > 1 )
				{ // 不是第一墩
					nCurIndex -= 3 ; // 到前一墩的上层的牌
				}
				else
				{ // 第一墩下层的那张牌
					nCurIndex = m_cnTile - 2 ;	// 从牌尾那一墩上层的牌开始
				}
			}
		}
	}

	return nCurIndex ;
}



void CPublicTileWall::SetOpenDoorPos( int nOpenDoorSeat , int nOpenDoorFrusta )
{
	ASSERT( nOpenDoorSeat >= 0 && nOpenDoorSeat < MAX_PLAYER_COUNT );

	// 调整一下开牌位置
	if( nOpenDoorFrusta >= m_anFrustaCount[ nOpenDoorSeat ] )
	{ // 点数过大，超过了这个人手中牌的墩数了

		nOpenDoorFrusta -= m_anFrustaCount[ nOpenDoorSeat ] ;	// 从上一家那里开牌，开牌的墩数就要少一些了

		nOpenDoorSeat++;		// 从上一家那里开牌
		if ( nOpenDoorSeat == MAX_PLAYER_COUNT )
		{
			nOpenDoorSeat = 0;
		}		
	}

	// 确定开牌位置的索引号

#if _DEBUG
	for (int i = 0; i < MAX_PLAYER_COUNT; i++)
	{
		TKWriteLog("当前座位：%d,墩数:%d", i, m_anFrustaCount[i]);
	}
	TKWriteLog("nOpenDoorSeat = %d", nOpenDoorSeat);
	TKWriteLog("nOpenDoorFrusta = %d", nOpenDoorFrusta);
	TKWriteLog("m_cnTile = %d", m_cnTile);
#endif

	m_nCurWallTileIndex = ( m_anFrustaCount[ nOpenDoorSeat ] * nOpenDoorSeat + nOpenDoorFrusta ) * 2 ;

#if _DEBUG
	TKWriteLog("当前牌索引：m_nCurWallTileIndex:%d", m_nCurWallTileIndex);
#endif

	if( m_nCurWallTileIndex >= m_cnTile )
	{ // 越界了
		TKWriteLog( "越界了m_nCurWallTileIndex >= m_cnTile，总张数：m_cnTile：%d，当前牌索引：m_nCurWallTileIndex：%d,",m_cnTile, m_nCurWallTileIndex );
		ASSERT( m_cnTile == m_nCurWallTileIndex ) ;
		m_nCurWallTileIndex = 0 ;
	}

	// 确定最后一张牌的索引号
	m_nLastWallTileIndex = m_nCurWallTileIndex - 2 ;
	if( m_nLastWallTileIndex < 0 )
	{ // 越界了
		TKWriteLog( "总张数：m_cnTile：%d，当前牌索引：m_nCurWallTileIndex：%d,最后一张牌索引：m_nLastWallTileIndex：%d",m_cnTile, m_nCurWallTileIndex ,m_nLastWallTileIndex);
		ASSERT( 0 == m_nCurWallTileIndex ) ;
		m_nLastWallTileIndex = m_cnTile - 2 ;
	}

	// 确定海底位置牌的索引号
	m_nHaidiTileIndex = m_nLastWallTileIndex + 1 ;
}

