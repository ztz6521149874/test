﻿#include "ServerGameConfig.h"
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;
//////////////////////////////////////////////////////////////////////////
// 内联实现
//////////////////////////////////////////////////////////////////////////

CConfig::CConfig()
{
}

void CConfig::Load(const std::string& configStr)
{
    vector<string> vStrCfg;
    boost::split(vStrCfg, configStr, boost::is_any_of(","));
    for (unsigned int i = 0;i < RULE_ITEM_COUNT;++i)
    {
        if (i < vStrCfg.size() && !vStrCfg[i].empty())
        {
            m_nCfgValue[i] = boost::lexical_cast<int>(vStrCfg[i]);
        }
        else
        {
            m_nCfgValue[i] = g_nDefaultValue[i];
        }
    }
}

int CConfig::GetRule(int nRuleMask)
{
    int nValue = 0;
    if (nRuleMask >= 0 && nRuleMask < RULE_ITEM_COUNT)
    {
        nValue = m_nCfgValue[nRuleMask];
    }
    return nValue;
}

void CConfig::SetRule(int nRuleMask, int nRuleValue)
{
    if (nRuleMask >= 0 && nRuleMask < RULE_ITEM_COUNT)
    {
        m_nCfgValue[nRuleMask] = nRuleValue;
    }
}
