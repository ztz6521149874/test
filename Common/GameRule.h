#pragma once

#include <tchar.h>
#include "TKMahjongDefine.h"
#include "ServerGameConfig.h"


enum RuleScore
{
	RULE_SCORE_PUBLIC,			// 普通计分规则,杠牌得分等( 如联众QQ )
	RULE_SCORE_MATCH,			// 比赛计分规则,杠牌不得分,和牌分平摊( 如英雄麻将 )
	RULE_SCORE_BLOODY,			// 血战计分规则
	RULE_SCORE_UPTOSTANDARD		// FOR TWO PLAYER 达标赛算分 已番数为分，没有负分   
};

#define MAX_HUN_COUNT		4	// 最多四种混牌 

// *********************************************************************************************************************
// 
// 游戏规则基类
// 
// 
// *********************************************************************************************************************
class CGameRule
{
public:
	// 构造函数
	CGameRule();
	~CGameRule(void);

	// 初始化
	bool InitInstance( TCHAR szProperty[] );

public:
	// 取规则信息,nRuleMask指定要取哪个单项信息
	int GetRule( int nRuleMask );

    int GetLuckTileFan(int nLuckCount);

	// 设置规则信息,nRuleMask指定要设置哪个单项信息
	void SetRule( int nRuleMask, int nRule );

	// 将手中的牌排序(混牌在最后，其它牌按从小到大排列),返回混牌个数
	virtual void Sort( STONE asStone[], int cnStone );

private:
	char * GetIntFromString( char *s,int *pVar );

protected:
	CConfig m_MahJongConfig;    // 所有规则
	int m_anHunTileID[ MAX_HUN_COUNT ];	// 所有混牌
};
