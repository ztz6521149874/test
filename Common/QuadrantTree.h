
#if !defined(AFX_QUADRANTTREE_H__63E84FA6_E632_468F_AA6D_676C4FDA459B__INCLUDED_)
#define AFX_QUADRANTTREE_H__63E84FA6_E632_468F_AA6D_676C4FDA459B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "tkmahjongdefine.h"

#define MAXNODES					( 341 )
#define LEFT1CHILD					( 0 )
#define LEFT2CHILD					( 1 )
#define LEFT3CHILD					( 2 )

class CQuadrantTree  
{
public:
	CQuadrantTree();
	virtual ~CQuadrantTree();

public:
	// 构造四叉树
	BOOL Create( STONEGROUP &sRoot, STONE asStone[], int cnNormalStone, int cnHun );

	// 获得一条从根结点到叶节点的路径
	BOOL GetPath( int nPathIndex, STONEGROUP asGroup[] );

	// 取路径条数
	int GetPathCount() { return m_cnPath; }
	
private:
	// 初始化四叉树
	void Init();

	// 构造左边的三棵子树( 顺 )，nChild指明构造第几棵子树
	// 第n棵子树取第一个牌作为顺的第n张
	BOOL CreateLeftChild( int nParentIndex, int nChild, STONE asStone[],
		int cnNormalStone, int cnHun );

	// 构造右子树(刻)
	BOOL CreateRightChild( int nParentIndex, STONE asStone[], int cnNormalStone, int cnHun );

	// 构造一个全是混的叶子结点
	void CreateHunLeaf( int nLeafIndex, STONE asHun[] );

	// 给定一个牌数组，以第一张牌为基础，构造一个顺牌分组，nBaseIndex指明第一张牌要放置的位置
	BOOL CreateShun( STONE asStone[], int& cnNormalStone, int& cnHun, int nBaseIndex,
		STONEGROUP &sGroup, STONE asSpareStone[] );

	// 在一个分组里需要的地方补充混，nBaseIndex指明本分组不需要补充的位置
	BOOL PerfectGroup( STONEGROUP &sGroup, int nBaseIndex, STONE asHun[], int &cnHun );

private:
	STONEGROUP		m_asNode[MAXNODES];
	int				m_anLeafIndex[256];		// 最终成功的叶结点数组
	int				m_cnPath;				// 从根结点到叶节点的路径数

};

#endif // !defined(AFX_QUADRANTTREE_H__63E84FA6_E632_468F_AA6D_676C4FDA459B__INCLUDED_)
