#pragma once

#include "TKMahjongProtocol.h"

// *********************************************************************************************************************
// 
// 算番接口
// 
// 
// *********************************************************************************************************************
class CGameRule;
class IJudge
{
public:
	// 构造与析构
	IJudge(){} 
	virtual ~IJudge(){}

	// 初始化，构造之后必须调用本函数
	BOOL Init( CGameRule * pRule )
	{ 
		m_pRule = pRule;
		return OnInit();
	}

public:
	// 初始化
	virtual BOOL OnInit(){ return true; }

	// 算番接口方法
	// 验和，默认为国标规则
	virtual CHECKRESULT CheckWin( CHECKPARAM &sCheckParam, HURESULT &sHuResult ) = 0;

	// 验听
	virtual CHECKRESULT CheckTing( CHECKPARAM &sCheckParam, CALLINFOVECTOR &vsCallInfo ) = 0;

	// 验证和牌是否有效
	virtual BOOL CanWin( CHECKPARAM &sCheckParam, WININFO &sWinInfo ) = 0;

	// 取番种信息
	virtual BOOL GetFanInfo( int nFanID, char szFanName[], char szFanScore[] ){ return true; };

	// 吃碰杠检验,返回可供选择的组数
	virtual int CheckShowTile( CHECKPARAM &sCheckParam, int nCheckMask, MAHJONGGROUPTILE asShowGroup[] )
	{
		int cnShowGroup = 0;
		if ( nCheckMask & REQUEST_TYPE_CHI )
		{
			cnShowGroup += CheckChi( sCheckParam, asShowGroup );
		}
		if ( nCheckMask & REQUEST_TYPE_PENG )
		{
			cnShowGroup += CheckPeng( sCheckParam, asShowGroup + cnShowGroup );
		}
		if ( nCheckMask & REQUEST_TYPE_GANG )
		{
			if (nCheckMask & REQUEST_TYPE_DISCARD_TILE)
				sCheckParam.nWinMode |= WIN_MODE_ZIMO;
			cnShowGroup += CheckGang( sCheckParam, asShowGroup + cnShowGroup );
			if (nCheckMask & REQUEST_TYPE_DISCARD_TILE)
				sCheckParam.nWinMode &= ~WIN_MODE_ZIMO;
			//cnShowGroup += CheckGang( sCheckParam, asShowGroup + cnShowGroup );
		}

		return cnShowGroup;
	}

protected:
	// 是否能吃,返回可供选择的组数
	virtual int CheckChi( CHECKPARAM &sCheckParam, MAHJONGGROUPTILE asShowGroup[] ) = 0;

	// 是否能碰,返回可供选择的组数
	virtual int CheckPeng( CHECKPARAM &sCheckParam, MAHJONGGROUPTILE asShowGroup[] ) = 0;

	// 是否能杠,返回可供选择的组数,必须在sCheckParam的nWinMode中设置是否自摸的牌
	virtual int CheckGang( CHECKPARAM &sCheckParam, MAHJONGGROUPTILE asShowGroup[] ) = 0;

protected:
	CGameRule * m_pRule;
};