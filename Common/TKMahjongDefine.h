#pragma once

#include <VECTOR>
using namespace std ;
#include "GTKFUNC.H"

// *********************************************************************************************************************
//
//	服务器和客户端通用的标识符定义
//
//
// *********************************************************************************************************************
#define TKVerifyMemoryAlloc( point, file, line )\
	if ( NULL == point )\
	{\
		TKWriteLog( "Memory Alloc Failed, %s, %d", file, line );\
		return FALSE;\
	}
#define GOLD 2
#define MAX_PLAYER_COUNT				(  2 )	// 一桌游戏的最多玩家数/*FOR TWO PLAYER*/
#define MAX_HAND_TILE_COUNT				( 20 )	// 玩家手中牌的最多张数

// 客户端看到的方位号
#define SIDE_LEFT						( 2 )	/*FOR TWO PLAYER*/
#define SIDE_OPPOSITE					( 0 )	/*FOR TWO PLAYER*/
#define SIDE_RIGHT						( 3 )	/*FOR TWO PLAYER*/
#define SIDE_ME							( 1 )	/*FOR TWO PLAYER*/


// 有关牌的一些宏
#define MAX_HAND_COUNT					(  17 )	// 手上最多只可能有17张牌
#define MAX_SHOW_COUNT					(   6 )	// 吃碰杠的牌组数
#define MAX_DICE_COUNT					(   4 )	// 最多的骰子数目
#define MAX_TILE_COUNT					( 144 )	// 最多的牌的张数
#define MAX_SET_COUNT					(   1 )	// 最多一副牌
#define MAX_FRUSTA_COUNT				(  18 )	// 牌墙最多墩数
#define MAX_DISCARD_COUNT				(  48 )	// 一局中最多出这么多张牌
#define MAX_FLOWER_COUNT				(   8 )	// 最多的花牌个数

#define COLOR_WAN						( 0 )	// 万
#define COLOR_TIAO						( 1 )	// 条
#define COLOR_BING						( 2 )	// 饼
#define COLOR_WIND						( 3 )	// 风
#define COLOR_JIAN						( 4 )	// 箭
#define COLOR_FLOWER					( 5 )	// 梅兰竹菊
#define COLOR_SEASON					( 6 )	// 春夏秋冬
#define STONE_NO1						( 0 )	// 1
#define STONE_NO2						( 1 )	// 1
#define STONE_NO3						( 2 )	// 1
#define STONE_NO4						( 3 )	// 1
#define STONE_NO5						( 4 )	// 1
#define STONE_NO6						( 5 )	// 1
#define STONE_NO7						( 6 )	// 1
#define STONE_NO8						( 7 )	// 1
#define STONE_NO9						( 8 )	// 1
#define TILE_NO1						( STONE_NO1 )
#define TILE_NO2						( STONE_NO2 )
#define TILE_NO3						( STONE_NO3 )
#define TILE_NO4						( STONE_NO4 )
#define TILE_NO5						( STONE_NO5 )
#define TILE_NO6						( STONE_NO6 )
#define TILE_NO7						( STONE_NO7 )
#define TILE_NO8						( STONE_NO8 )
#define TILE_NO9						( STONE_NO9 )


#define TILE_ID(color,what,count)		(((color)<<8)|((what)<<4)|(count))
#define TILE_COLOR(id)					(((id)>>8)&0x0F)
#define TILE_WHAT(id)					(((id)>>4)&0x0F)
#define SAME_TILE_ID(a,b)				(((a)&0xFFFF)==((b)&0xFFFF))

// 胡牌方式
#define WIN_MODE_GANGSHANGHU			( 1 <<  0 )	// 杠上开花
#define	WIN_MODE_QIANGGANG				( 1 <<  1 )	// 抢杠
#define WIN_MODE_HUJUEZHANG				( 1 <<  2 )	// 和绝张
#define WIN_MODE_HAIDI					( 1 <<  3 )	// 海底
#define WIN_MODE_ZIMO					( 1 <<  4 )	// 自摸
#define	WIN_MODE_TIANHU					( 1 <<  5 )	// 天和
#define WIN_MODE_DIHU					( 1 <<  6 )	// 地和
#define WIN_MODE_RENHU					( 1 <<  7 )	// 人和
#define WIN_MODE_TIANTING				( 1 <<  8 )	// 天听
#define WIN_MODE_7QIANG1				( 1 <<  9 )	// 七抢一
#define WIN_MODE_LIZHI					( 1 << 10 ) // 立直
#define WIN_MODE_GANGPAO				( 1 << 11 )	// 杠上炮

// 吃碰杠检验掩码
#define CHECK_MASK_EAT					( 1 )	// 吃
#define CHECK_MASK_PENG					( 2 )	// 碰
#define CHECK_MASK_GANG 				( 4 )	// 杠

// 每个分组的组合形式
#define GROUP_STYLE_SHUN				( 1 )	// 顺
#define GROUP_STYLE_KE					( 2 )	// 刻
#define GROUP_STYLE_JONG				( 3 )	// 将
#define GROUP_STYLE_MINGGANG			( 4 )	// 明杠
#define GROUP_STYLE_ANGANG				( 5 )	// 暗杠
#define GROUP_STYLE_LONG				( 6 )	// 组合龙
#define GROUP_STYLE_HUN					( 7 )	// 全混

// 和牌基本番种
//88番
//1 大四喜 由4副风刻(杠)组成的和牌。不计圈风刻、门风刻、三风刻、碰碰和
#define FAN_DA4XI						( 0  )  
//2 大三元 和牌中，有中发白3副刻子。不计箭刻
#define FAN_DA3YUAN						( 1  )  
//3 绿一色 由23468条及发字中的任何牌组成的顺子、刻子、将的和牌。不计混一色。如无“发”字组成的各牌，可计清一色
#define FAN_LVYISHE						( 2  )  
//4 九莲宝灯 由一种花色序数牌子按1112345678999组成的特定牌型，见同花色任何1张序数牌即成和牌。不计清一色
#define FAN_9LIANBAODEN					( 3  )  
//5 四杠 4个杠
#define FAN_4GANG						( 4  )  
//6 连七对 由一种花色序数牌组成序数相连的7个对子的和牌。不计清一色、不求人、单钓 
#define FAN_LIAN7DUI					( 5  )  
//7 十三幺 由3种序数牌的一、九牌，7种字牌及其中一对作将组成的和牌。不计五门齐、不求人、单钓
#define FAN_131							( 6  )  

//64番
//8 清幺九 由序数牌一、九刻子组成的和牌。不计碰碰和、同刻、元字 
#define FAN_QING19						( 7  )  
//9 小四喜 和牌时有风牌的3副刻子及将牌。不计三风刻
#define FAN_XIAO4XI						( 8  )  
//10 小三元 和牌时有箭牌的两副刻子及将牌。不计箭刻
#define FAN_XIAO3YUAN					( 9 )  
//11 字一色 由字牌的刻子(杠)、将组成的和牌。不计碰碰和
#define FAN_ZI1SHE						( 10 )  
//12 四暗刻 4个暗刻(暗杠)。不计门前清、碰碰和
#define FAN_4ANKE						( 11 ) 
//13 一色双龙会 一种花色的两个老少副，5为将牌。不计平和、七对、清一色
#define FAN_1SHE2GLONG					( 12 )  

//48番
//14 一色四同顺 一种花色4副序数相同的顺子，不计一色三节高、一般高、四归一 
#define FAN_1SHE4TONGSHUN				( 13 ) 
//15 一色四节高 一种花色4副依次递增一位数的刻子不计一色三同顺、碰碰和 
#define FAN_1SHE4JIEGAO					( 14 )  

//32番
//16 一色四步高 一种花色4副依次递增一位数或依次递增二位数的顺子 
#define FAN_1SHE4BUGAO					( 15 ) 
//17 三杠 3个杠
#define FAN_3GANG						( 16 )  
//18 混幺九 由字牌和序数牌一、九的刻子用将牌组成的各牌。不计碰碰和 
#define FAN_HUN19						( 17 )  

//24番
//19 七对 由7个对子组成和牌。不计不求人、单钓
#define FAN_7DUI						( 18 )  
//20 七星不靠 必须有7个单张的东西南北中发白，加上3种花色，数位按147、258、369中的7张序数牌组成没有将牌的和牌。不计五门齐、不求人、单钓
#define FAN_7XINBUKAO					( 19 )  
//21 全双刻 由2、4、6、8序数牌的刻子、将牌组成的和牌。不计碰碰和、断幺
#define FAN_QUANSHUANGKE					( 20 ) 
//22 清一色 由一种花色的序数牌组成和牌。不无字
#define FAN_QING1SHE					( 21 )  
//23 一色三同顺 和牌时有一种花色3副序数相同的顺子。不计一色三节高
#define FAN_1SHE3TONGSHUN				( 22 )  
//24 一色三节高 和牌时有一种花色3副依次递增一位数字的刻子。不计一色三同顺
#define FAN_1SHE3JIEGAO					( 23 )  
//25 全大 由序数牌789组成的顺子、刻子(杠)、将牌的和牌。不计无字
#define FAN_QUANDA						( 24 )  
//26 全中 由序数牌456组成的顺子、刻子(杠)、将牌的和牌。不计断幺
#define FAN_QUANZHONG					( 25 )  
//27 全小 由序数牌123组成的顺子、刻子(杠)将牌的的和牌。不计无字
#define FAN_QUANXIAO					( 26 )  

//16番
//28 清龙 和牌时，有一种花色1-9相连接的序数牌
#define FAN_QINGLONG					( 27 )  
//29 三色双龙会 2种花色2个老少副、另一种花色5作将的和牌。不计喜相逢、老少副、无字、平和
#define FAN_3SHE2LONG					( 28 )  
//30 一色三步高 和牌时，有一种花色3副依次递增一位或依次递增二位数字的顺子
#define FAN_1SHE3BUGAO					( 29 )  
//31 全带五 每副牌及将牌必须有5的序数牌。不计断幺
#define FAN_QUANDAI5					( 30 )  
//32 三同刻 3个序数相同的刻子(杠)
#define FAN_3TONGKE						( 31 )  
//33 三暗刻 3个暗刻 
#define FAN_3ANKE						( 32 )  

//12番
//34 全不靠 由单张3种花色147、258、369不能错位的序数牌及东南西北中发白中的任何14张牌组成的和牌。不计五门齐、不求人、单钓
#define FAN_QUANBUKAO					( 33 )  
//35 组合龙 3种花色的147、258、369不能错位的序数牌
#define FAN_ZHUHELONG					( 34 )  
//36 大于五 由序数牌6-9的顺子、刻子、将牌组成的和牌。不计无字
#define FAN_DAYU5						( 35 )  
//37 小于五 由序数牌1-4的顺子、刻子、将牌组成的和牌。不计无字
#define FAN_XIAOYU5						( 36 )  
//38 三风刻 3个风刻
#define FAN_3FENGKE						( 37 )  

//8 番
//39 花龙 3种花色的3副顺子连接成1-9的序数牌
#define FAN_HUALONG						( 38 )   
//40 推不倒 由牌面图形没有上下区别的牌组成的和牌，包括1234589饼、245689条、白板。不计缺一门
#define FAN_TUIBUDAO					( 39 )  
//41 三色三同顺 和牌时，有3种花色3副序数相同的顺子
#define FAN_3SHE3TONGSHUN				( 40 )  
//42 三色三节高 和牌时，有3种花色3副依次递增一位数的刻子
#define FAN_3SHEJIEJIEGAO				( 41 )  
//43 无番和 和牌后，数不出任何番种分(花牌不计算在内)
#define FAN_WUFAN						( 42 )  
//44 妙手回春 自摸牌墙上最后一张牌和牌。不计自摸
#define FAN_MIAOSHOU					( 43 )  
//45 海底捞月 和打出的最后一张牌
#define FAN_HAIDI						( 44 )  
//46 杠上开花 开杠抓进的牌成和牌(不包括补花)不计自摸
#define FAN_GANGHU						( 45 )  
//47 抢杠和 和别人自抓开明杠的牌。不计和绝张
#define FAN_QIANGGANG					( 46 )  

//6 番
//48 碰碰和 由4副刻子(或杠)、将牌组成的和牌
#define FAN_PENPENHU					( 47 )  
//49 混一色 由一种花色序数牌及字牌组成的和牌 
#define FAN_HUN1SHE						( 48 ) 
//50 三色三步高 3种花色3副依次递增一位序数的顺子
#define FAN_3SHE3BUGAO					( 49 )  
//51 五门齐 和牌时3种序数牌、风、箭牌齐全
#define FAN_5MENQI						( 50 )  
//52 全求人 全靠吃牌、碰牌、单钓别人批出的牌和牌。不计单钓 
#define FAN_QUANQIUREN					( 51 )  
//53 双暗杠 2个暗杠
#define FAN_2ANGANG						( 52 ) 
//54 双箭刻 2副箭刻(或杠)
#define FAN_2JIANKE						( 53 )  

//4 番
//55 全带幺 和牌时，每副牌、将牌都有幺牌
#define FAN_QUANDAIYAO					( 54 )  
//56 不求人 4副牌及将中没有吃牌、碰牌(包括明杠)，自摸和牌
#define FAN_BUQIUREN					( 55 )  
//57 双明杠 2个明杠
#define FAN_2MINGANG					( 56 )   
//58 和绝张 和牌池、桌面已亮明的3张牌所剩的第4张牌(抢杠和不计和绝张)
#define FAN_HUJUEZHANG					( 57 ) 

//2 番
//59 箭刻 由中、发、白3张相同的牌组成的刻子
#define FAN_JIANKE						( 58 )  
//60 圈风刻 与圈风相同的风刻
#define FAN_QUANFENG					( 59 )  
//61 门风刻 与本门风相同的风刻
#define FAN_MENGFENG					( 60 ) 
//62 门前清 没有吃、碰、明杠，和别人打出的牌
#define FAN_MENGQING					( 61 )  
//63 平和 由4副顺子及序数牌作将组成的和牌，边、坎、钓不影响平和
#define FAN_PINHU						( 62 )  
//64 四归一 和牌中，有4张相同的牌归于一家的顺、刻子、对、将牌中(不包括杠牌)
#define FAN_4GUI1						( 63 )  
//65 双同刻 2副序数相同的刻子
#define FAN_2TONGKE						( 64 ) 
//66 双暗刻 2个暗刻
#define FAN_2ANKE						( 65 )  
//67 暗杠 自抓4张相同的牌开杠
#define FAN_ANGANG						( 66 )  
//68 断幺九 和牌中没有一、九及字牌 1 番
#define FAN_DUAN19						( 67 )  

// 1番
//69 一般高 由一种花色2副相同的顺子组成的牌 
#define FAN_YIBANGAO					( 68 )  
//70 喜相逢 2种花色2副序数相同的顺子
#define FAN_XIXIANGFENG					( 69 ) 
//71 连六 一种花色6张相连接的序数牌
#define FAN_LIAN6						( 70 ) 
//72 老少副 一种花色牌的123、789两副顺子
#define FAN_LAOSHAOFU					( 71 ) 
//73 幺九刻 3张相同的一、九序数牌及字牌组成的刻子(或杠)
#define FAN_19KE						( 72 )  
//74 明杠 自己有暗刻，碰别人打出的一张相同的牌开杠：或自己抓进一张与碰的明刻相同的牌开杠
#define FAN_MINGANG						( 73 )  
//75 缺一门 和牌中缺少一种花色序数牌
#define FAN_QUE1MEN						( 74 )  
//76 无字 和牌中没有风、箭牌
#define FAN_WUZI						( 75 )  
//77 边张 单和123的3及789的7或1233和3、77879和7都为边张。手中有12345和3，56789和7不算边张
#define FAN_BIANZANG					( 76 )  
//78 坎张 和2张牌之间的牌。4556和5也为坎张，手中有45567和6不算坎张
#define FAN_KANZANG						( 77 )  
//79 单钓将 钓单张牌作将成和
#define FAN_DANDIAO						( 78 ) 
//80 自摸 自己抓进牌成和牌
#define FAN_ZIMO						( 79 )	
//81 花牌 即春夏秋冬，梅兰竹菊，每花计一分。不计在起和分内，和牌后才能计分。花牌补花成和计自摸分，不计杠上开花
#define FAN_FLOWER						( 80  )  

//以下是大众规则的番种
//#define FAN_ZHUHELONG_Q					( 86  )  // "全不靠之组合龙" 
// 168番
#define FAN_4FANGDAFA					( 81  )  // "四方大发财" 
#define FAN_TIANHU						( 82  )  // "天和" 
// 158番
#define FAN_DIHU						( 83  )  // "地和" 
// 108番
#define FAN_RENHU						( 84  )  // "人和" 
// 88番
#define FAN_HUNGANG						( 85  )  // "混杠" 
#define FAN_8XIANGUOHAI					( 86 )  // "八仙过海" 
// 32番
#define FAN_7QIANG1						( 87 )  // "七抢一" 
#define FAN_TIANTING					( 88 )  // "天听" 
// 6番
#define FAN_HUN4JIE						( 89  )  // "混四节" 
// 4番
#define FAN_HUN4BU						( 90  )  // "混四步" 
#define FAN_HUN3JIE						( 91  )  // "混三节" 
// 2番
#define FAN_WUHUN						( 92  )  // "无混" 
#define FAN_HUNLONG						( 93  )  // "混龙" 
#define FAN_HUN3BU						( 94  )  // "混三步" 
#define FAN_LIZHI						( 95 )   // "立直" 
// 1番
#define FAN_258JONG						( 96  )  // "二五八将" 
#define FAN_4FLOWER						( 97  )  // "梅兰竹菊" 
#define FAN_4SEASON						( 98  )  // "春夏秋冬" 
#define FAN_SEASONFLOWER				( 99  )  // "季花" 
#define FAN_19JONG						( 100 )  // "么九头" 由序数牌的19做将牌

// 自定义番
#define FAN_SPECIAL						( 101 )	 // 加番牌

// 急速二麻追加番种
#define FAN_JSPKHU						( 102 )	 // 极速二麻胡
#define FAN_JSPKLUCKYTILE				( 103 )	 // 极速二麻奖花

// 验和验听函数返回值
enum CHECKRESULT { T_OK, F_NOENOUGHFANS, F_NOTTING };

struct STONE
{
	int					nID;		// 麻将牌的ID号（唯一）
	int					nColor;		// 麻将牌的花色
	int					nWhat;		// 麻将牌的点数
};

struct STONEGROUP
{
	STONE				asStone[4];
	int					nGroupStyle;
};

typedef STONE MAHJONGTILE ;
typedef STONE* PMAHJONGTILE ;
typedef STONEGROUP MAHJONGGROUPTILE ;
typedef STONEGROUP* PMAHJONGGROUPTILE ;

#define MAXFANNAMELEN	( 32 )
#define MAXFANS			( 128 )		// 最大番种数
#define WIN_FAIL		( -1 )		// 诈和
#define NORMAL			(  0 )		// 普通和牌
#define PENPENHU		(  4 )		// 碰碰和
#define QIDUI			(  5 )		// 七对
#define SHISHANYAO		(  6 )		// 十三幺
#define QUANBUKAO		(  7 )		// 全不靠
#define ZUHELONG		(  8 )		// 组合龙
#define JSPKHU            ( 9 )    // 急速二麻胡牌规则

struct CHECKPARAM
{
	// 以下数据调用方填写
	STONE	 			asHandStone[MAX_HAND_COUNT];// 手中牌
	int					cnHandStone;		// 手中牌的张数
	STONEGROUP 			asShowGroup[6];		// 吃碰杠的牌
	int					cnShowGroups;		// 吃碰杠的数量
	STONE				asFlower[8];		// 花牌
	int					cnFlower;			// 花牌个数
	int					cnMinGang;			// 明杠个数
	int					cnAnGang;			// 暗杠个数
	int					nWinMode;			// 和牌方式组合
	int					nMinFan;			// 和牌所需的最低番数
	int					nQuanWind;			// 圈风
	int					nMenWind;			// 门风
#ifdef _TEST_
	int					nRule;				// 规则
	int					nHunID;				// 混牌ID
#endif
};

// 和牌返回值
struct HURESULT 
{
	int					nMaxFans;			// 能算出的最高番数
	int					anFans[MAXFANS];	// 番种列表
	int					nResultant;			// 和牌牌型
	STONEGROUP			asGroup[6];			// 牌型组合方式，最多6组
	STONE				asHandStone[MAX_HAND_COUNT];// 手中牌，对不能分组的牌型，这里保存手中牌的最终信息
	int					cnGroups;			// 分组数
	// 下面几个变量主要用于番种分析，由服务器算完番后填充，再发到客户端
	int					nHuTileID;			// 和的那张牌ID
	STONE				asFlower[8];		// 花牌
	int					cnFlower;			// 花牌个数
	int					nQuanWind;			// 圈风
	int					nMenWind;			// 门风
	int					cnShowGroups;		// 吃碰杠的分组数
	int					nWinMode;			// 和牌方式
	int					nScoreOfFan;		// 番数对应的分数,不包括基本分
};

typedef HURESULT WININFO;					// 和牌详细信息


// 听牌返回值
struct WAITING
{
	int					nWaitingStoneID;
	int					nFans;
};
struct WAITRESULT
{
	int					nPutStoneID;		// 打哪张
	vector<WAITING>		vsWaiting;			// 和牌番数	
};
typedef vector<WAITRESULT> WAITRESULTVECTOR;

// 听的那张牌的信息
typedef struct tagCallTileInfo
{
	int					nCallTileID;		// 听哪张牌
	int					nFans;				// 和这张牌有几番
} CALLTILEINFO, *PCALLTILEINFO;
// 打掉某张牌后能听的所有牌的信息
typedef struct tagCallInfo
{
	int					nDiscardTileID;		// 打哪张
	int					cnCallTile;			// 可和的牌张数
	CALLTILEINFO		asCallTileInfo[34];	// 最多可和34张牌
} CALLINFO, PCALLINFO;
typedef vector<CALLINFO> CALLINFOVECTOR;	// 所有可听的详细信息


typedef struct tagFanInfo
{
	int					nID;
	int					nScore;
	int					anTileID[16];		// 涉及到的牌ID
	int					cnTile;				// 涉及到的牌数
} FANINFO, PFANINFO;
typedef vector<FANINFO> FANINFOVECTOR;		// 所有番的详细信息

typedef struct tagStonesRelation
{
	int					cnHun;				// 混牌个数
	int					cnAllColors;		// 所有牌(包括吃碰杠的牌)花色数
	int					acnAllColors[5];	// 所有牌，每花色牌个数
	int					acnAllStones[35];	// 所有牌，每种牌个数
	int					cnHandColors;		// 手中牌花色数
	int					acnHandColors[5];	// 手中牌(不包括吃碰杠的牌)每花色牌个数，依次为万条饼风箭
	int					cnStoneTypes;		// 手中有多少种牌(不包括吃碰杠的牌)
	int					acnHandStones[35];  // 手中每种牌个数(不包括吃碰杠的牌)，最后一个
	// 元素无用，只是为了方便将风牌和箭牌统一处理
	BOOL				bNoEat;				// 没有吃过牌
} STONESRELATION;

typedef struct tagGroupsRelation
{
	int					cnShunGroups;		// 共有多少个顺
	int					cnKeGroups;			// 共有多少个刻
	int					acnShunGroups[3][7];// 各种顺的数量:123.234.456.567....
	int					acnKeGroups[5][9];	// 各种刻的数量
	//	int					cnHunGroups;		// 全是混的分组数量(不包括将牌分组)
	//	BOOL				bHunJong;			// 是否全是混做将
}GROUPSRELATION;

typedef struct tagConfig
{
	BOOL bWatchable ;						// 是否允许旁观
	BOOL bSound ;							// 是否选择了静音
	BOOL bAnimate ;							// 是否显示动画
	char szSnapshotPath[ _MAX_PATH ] ;		// 截图的路径
	BOOL bThemeSound ;						// 是否听到场景语音
	int  nLanguage ;						// 自己选择的报牌、行牌的语言
	int  nListenMode ;						// 对于别人的报牌、行牌，自己听到的效果
	/// 0－别人选的什么方言，自己听到的就是什么方言
	/// 1－不管别人选的什么方言，自己听到的都是自己所选择的那种方言
	BOOL bShowItemTip ;						// 当道具可用时，是否在桌面显示道具提示
	BOOL bShowFlash ;                       // 用户是否播放道具FLASH
} CONFIG, * PCONFIG;

// 
// 麻将每张牌的张数信息
// 
typedef struct tagMahJongTileCountInfo
{
	int nColor ;	// 花色
	int nWhat ;		// 点数
	vector<int> vUsed;//使用index数组
	int nTotal ;	// 总共应该有几张
	tagMahJongTileCountInfo()
	{
		nColor = 0;
		nWhat = 0;
		vUsed.clear();
		nTotal = 0;
	}
} MAHJONGTILECOUNTINFO , *PMAHJONGTILECOUNTINFO ;


typedef struct tagShowGroupInfo
{
	int nTileID;		// 吃的哪张牌，如果是碰和杠这个值为-1（因为不需要）
	int nSeat;			// 吃碰杠的是哪家的
} SHOWGROUPINFO, *PSHOWGROUPINFO;


// 
// 不同动画的用户自定义类型
// 
#define	CARTOON_USER_TYPE_UP_DOWN		(  1 )	// 牌抬起放下的动画
#define CARTOON_USER_TYPE_SORT   		(  2 )	// 理牌动画
#define CARTOON_USER_TYPE_MOVE			(  3 )	// 水平移动牌的动画
#define CARTOON_USER_TYPE_CHANGE_FLOWER	( 11 )	// 补花动画
#define CARTOON_USER_TYPE_DISCARD		( 12 )	// 出牌动画
#define CARTOON_USER_TYPE_DRAW			( 13 )	// 抓牌动画
#define CARTOON_USER_TYPE_SHOW			( 14 )	// 吃碰杠牌动画
#define CARTOON_USER_TYPE_HUN			( 15 )	// 翻混动画
#define CARTOON_USER_TYPE_FLASH			( 19 )	// 牌闪烁动画
#define CARTOON_USER_TYPE_SHUFFLE		( 21 )	// 砌牌动画
#define CARTOON_USER_TYPE_SHUFFLE_2		( 22 )	// 砌牌之后把牌推出去动画
#define CARTOON_USER_TYPE_DEAL_FINISH	( 24 )	// 发牌结束动画（把牌倒下，再把牌抬起来）
#define CARTOON_USER_TYPE_DICE			( 31 )	// 骰子动画
#define CARTOON_USER_TYPE_PREPARE_SORT	( 41 )	// 准备理牌动画（这种动画仅仅起一个承前启后的作用）
#define CARTOON_USER_TYPE_STATE			( 42 )	// 玩家吃碰杠等动画（显示一个提示的字，然后一段时间后消失）
#define CARTOON_USER_TYPE_TIMER			( 51 )	// 时钟动画
#define CARTOON_USER_TYPE_SANDGLASS		( 52 )	// 沙漏动画
#define CARTOON_USER_TYPE_WINFAIL		( 53 )	// 诈和动画
#define CARTOON_USER_TYPE_CALL			( 54 )	// 听牌动画
#define CARTOON_USER_TYPE_WIN			( 55 )	// 和牌动画
#define CARTOON_USER_TYPE_THEME_SOUND	( 61 )	// 场景语音动画
#define CARTOON_USER_TYPE_AWARD			( 71 )	// 获得奖励动画
#define CARTOON_USER_TYPE_AWARD_GANG	( 72 )	// 杠牌得分的动画
#define CARTOON_USER_TYPE_AWARD_MISSION	( 73 )	// 完成部分任务的动画
#define	CARTOON_ITEM_TYPE_LIGHTBALL		( 74 )	// 光球动画

// 控件消息
#define WM_CONTROLMSG					( WM_USER + 101 )
#define WM_MAGICITEM_REQUESTUSE			( 1 )	// 请求使用道具，开始等待用户选择
#define WM_MAGICITEM_REQUESTEND			( 2 )	// 请求结束，用户已做出选择
#define WM_SCROLLBAR_SCROLL				( WM_USER + 102 )

// 供主窗口使用的禁用、可用套接字的消息（因为套接字必须在有窗口的线程中才能被禁用、可用）
#define WM_MAINFRAME_ENABLE_SOCKET		( WM_USER + 1000 )

// 让主窗口播放声音消息
#define WM_MAINFRAME_PLAY_MUSIC			( WM_USER + 1001 )

//吃碰杠等动画对应的资源下标
#define  TKMAJONG_STATEINDEX_CHI			( 1)	//吃牌动画位图的下标
#define	 TKMAJONG_STATEINDEX_PENG			( 2)	//碰牌动画位图的下标
#define	 TKMAJONG_STATEINDEX_GANG			( 3)	//杠牌动画的位图下标
#define	 TKMAJONG_STATEINDEX_CALL			( 4)	//听牌动画的位图下标
#define  TKMAJONG_STATEINDEX_ZIMO			( 5)	//自摸动画的位图下标
#define  TKMAJONG_STATEINDEX_DIANPAO		( 6)	//点炮动画的位图下标
#define  TKMAJONG_STATEINDEX_DOUBLE			( 7)    //加倍动画的位图下标
#define  TKMAJONG_STATEINDEX_DBLCOIN		( 8)	//加倍金币的位图下标

//动画资源在资源的ID
#define ID_SPRITES_CHI						( 1)	//吃动画
#define ID_SPRITES_PENG						( 2)	//碰动画
#define ID_SPRITES_Gang						( 3)	//杠动画
#define ID_SPRITES_CALL						( 4)	//听牌动画
#define ID_SPRITES_ZIMO						( 5)	//自摸动画
#define ID_SPRITES_DIANPAO					( 6)	//点炮动画
#define ID_SPRITES_CLOCK					( 7)	//倒计时动画
#define ID_SPRITES_SANDGLASS				( 8)	//沙漏动画
#define ID_SPRITES_DICE						( 9)	//骰子动画
#define ID_SPRITES_TILE						(10)	//牌动画
#define ID_SPRITES_DOUBLE					(11)    //加倍动画
#define ID_SPRITES_DBLCOIN					(12)    //加倍金币


enum EnumRecordNameID
{
	RECORD_BANKER		=  0,	// 定庄
	RECORD_SHUFFLE		=  1,	// 洗牌
	RECORD_OPEN			=  2,	// 开牌
	RECORD_FLOWER		=  3,	// 补花
	RECORD_DRAW			=  4,	// 抓牌
	RECORD_TING			=  5,	// 听牌
	RECORD_GANG			=  7,	// 杠牌
	RECORD_DISCARD		=  8,	// 打牌
	RECORD_CHI			= 11,	// 吃牌
	RECORD_PENG			= 12,	// 碰牌
	RECORD_WIN			= 13,	// 胡牌
	RECORD_RESULTS		= 14,	// 结果
	RECORD_SCORECHANGE  = 15,	// 分数变化
	RECORD_HUN			= 17,	// 定混
	RECORD_NETBREAK		= 18,	// 断线
	RECORD_NETRESUME	= 19,	// 断线恢复
	RECORD_TRUSTPLAY	= 23,	// 托管
	RECORD_CLEARTRUST	= 24,	// 解除托管
	RECORD_MAIMA		= 25,	// 买马
	RECORD_SHAOZHUANG	= 26,	// 分猪肉
	RECORD_SPECIALTILE	= 27,	// 加番牌
	RECORD_LUCKYTILE	= 31,	// 奖花牌



};