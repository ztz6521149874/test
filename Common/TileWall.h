#pragma once

//#include <string>
#define   WIN32_LEAN_AND_MEAN   
#include <Windows.h>

// *********************************************************************************************************************
//
// 牌墙接口
// 因为各种规则的麻将在牌墙码放上存在较大的差异,因些独立出来做为一个类
//
//
// *********************************************************************************************************************
class CMahJongTile;
struct ShuffleParam 
{
	CMahJongTile ** ppWallTile;						// 指向所有牌墙中的牌的信息
	int cnTile;										// 共有多少张牌
};
class ITileWall
{
public:
	virtual ~ITileWall(){};

protected:
	// 声明为保护类型,不允许构造基类
	ITileWall(){};
	ITileWall( ITileWall & wall );
	ITileWall & operator =( ITileWall wall );

public:
	// 洗牌,砌牌墙
	virtual void Shuffle( ShuffleParam * pShuffleParam, int anFrustaOfSeat[] ) = 0;

	// 开牌
	virtual void SetOpenDoorPos( int nOpenDoorSeat , int nOpenDoorFrusta ){};

	// 从牌墙里抓一张牌
	virtual CMahJongTile * GetOneTile( bool bFromWallHeader , int nOffset ){ return NULL; };

	// 是否还有牌
	virtual bool Empty() = 0;

	// 重置
	virtual void ResetGameData() = 0;

	// 绘制
	virtual void Render( HDC hdc ){};
};