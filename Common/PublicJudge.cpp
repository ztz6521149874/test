// PublicJudge.cpp: implementation of the CPublicJudge class.
//
//////////////////////////////////////////////////////////////////////

//#include "stdafx.h"
//#include "Mahjong.h"
#include <winsock2.h>
#include <windows.h>
#include <assert.h>
//#include "logfileobj.h"
#include "MahJongTile.h"
#include "PublicJudge.h"
#include "QuadrantTree.h"
#include "tkmahjongprotocol.h"
#include "GameRule.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#ifndef ASSERT
#define ASSERT	assert
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPublicJudge::CPublicJudge()
{
//	Init();
}

CPublicJudge::~CPublicJudge()
{

}

// 初始化，构造之后必须调用本函数
BOOL CPublicJudge::OnInit()
{
	m_cnMaxHandStone = 5;
	m_cnMaxGroups = 2;
	m_cnMaxFans = 81;

	// 初始化番种列表
	int i = 0;
	for(i = 0; i < 81; i++ )
	{
		// 国标的番种
		if ( i < 7 )
		{
			m_asFanInfo[i].nScore = 88;
		}
		else if ( i < 13 )
		{
			m_asFanInfo[i].nScore = 64;
		}
		else if ( i < 15 )
		{
			m_asFanInfo[i].nScore = 48;
		}
		else if ( i < 18 )
		{
			m_asFanInfo[i].nScore = 32;
		}
		else if ( i < 27 )
		{
			m_asFanInfo[i].nScore = 24;
		}
		else if ( i < 33 )
		{
			m_asFanInfo[i].nScore = 16;
		}
		else if (i < 38 )
		{
			m_asFanInfo[i].nScore = 12;
		}
		else if ( i < 47 )
		{
			m_asFanInfo[i].nScore = 8;
		}
		else if ( i < 54 )
		{
			m_asFanInfo[i].nScore = 6;
		}
		else if ( i < 58 )
		{
			m_asFanInfo[i].nScore = 4;
		}
		else if ( i < 68 ) 
		{
			m_asFanInfo[i].nScore = 2;
		}
		else
		{
			if (FAN_ZIMO == i)
			{
				m_asFanInfo[i].nScore = 8;
			}
			else
			{
				m_asFanInfo[i].nScore = 1;
			}
		}
	}

	// 大众添加的番种
	m_asFanInfo[FAN_4FANGDAFA].nScore = 168;
	m_asFanInfo[FAN_TIANHU].nScore = 48;
	m_asFanInfo[FAN_DIHU].nScore = 32;
	m_asFanInfo[FAN_RENHU].nScore = 24;
	m_asFanInfo[FAN_HUNGANG].nScore = 88;
	m_asFanInfo[FAN_8XIANGUOHAI].nScore = 88;
	m_asFanInfo[FAN_7QIANG1].nScore = 32;
	m_asFanInfo[FAN_TIANTING].nScore = 16;
	m_asFanInfo[FAN_HUN4JIE].nScore = 6;
	m_asFanInfo[FAN_HUN4BU].nScore = 4;
	m_asFanInfo[FAN_HUN3JIE].nScore = 4;
	m_asFanInfo[FAN_WUHUN].nScore = 2;
	m_asFanInfo[FAN_HUNLONG].nScore = 2;
	m_asFanInfo[FAN_HUN3BU].nScore = 2;
	m_asFanInfo[FAN_LIZHI].nScore = 4;
	m_asFanInfo[FAN_258JONG].nScore = 1;
	m_asFanInfo[FAN_4FLOWER].nScore = 1;
	m_asFanInfo[FAN_4SEASON].nScore = 1;
	m_asFanInfo[FAN_SEASONFLOWER].nScore = 1;
	m_asFanInfo[FAN_19JONG].nScore = 1;

	// 极速二麻番种
	m_asFanInfo[FAN_JSPKHU].nScore = 1;
	m_asFanInfo[FAN_JSPKLUCKYTILE].nScore = 1;
    m_asFanInfo[FAN_QING1SHE].nScore = 4;
    m_asFanInfo[FAN_ZI1SHE].nScore = 10;
    m_asFanInfo[FAN_PINHU].nScore = 2;
    m_asFanInfo[FAN_PENPENHU].nScore = 4;

	// 回调函数
	m_asFanInfo[FAN_DA4XI].fCheck = &CPublicJudge::CheckDa4Xi;
	m_asFanInfo[FAN_DA3YUAN].fCheck = &CPublicJudge::CheckDa3Yuan;
	m_asFanInfo[FAN_LVYISHE].fCheck = &CPublicJudge::CheckLv1She;
	m_asFanInfo[FAN_9LIANBAODEN].fCheck = &CPublicJudge::Check9LianBaoDen;
	m_asFanInfo[FAN_4GANG].fCheck = &CPublicJudge::Check4Gang;
	m_asFanInfo[FAN_LIAN7DUI].fCheck = &CPublicJudge::CheckLian7Dui;
	m_asFanInfo[FAN_131].fCheck = &CPublicJudge::Check131;
	m_asFanInfo[FAN_QING19].fCheck = &CPublicJudge::CheckQing19;
	m_asFanInfo[FAN_XIAO4XI].fCheck = &CPublicJudge::CheckXiao4Xi;
	m_asFanInfo[FAN_XIAO3YUAN].fCheck = &CPublicJudge::CheckXiao3Yuan;
	m_asFanInfo[FAN_ZI1SHE].fCheck = &CPublicJudge::CheckZi1She;
	m_asFanInfo[FAN_4ANKE].fCheck = &CPublicJudge::Check4AnKe;
	m_asFanInfo[FAN_1SHE2GLONG].fCheck = &CPublicJudge::Check1She2Long;
	m_asFanInfo[FAN_1SHE4TONGSHUN].fCheck = &CPublicJudge::Check1She4TongShun;
	m_asFanInfo[FAN_1SHE4JIEGAO].fCheck = &CPublicJudge::Check1She4JieGao;
	m_asFanInfo[FAN_1SHE4BUGAO].fCheck = &CPublicJudge::Check1She4BuGao;
	m_asFanInfo[FAN_3GANG].fCheck = &CPublicJudge::Check3Gang;
	m_asFanInfo[FAN_HUN19].fCheck = &CPublicJudge::CheckHun19;
	m_asFanInfo[FAN_7DUI].fCheck = &CPublicJudge::Check7Dui;
	m_asFanInfo[FAN_7XINBUKAO].fCheck = &CPublicJudge::Check7XinBuKao;
	m_asFanInfo[FAN_QUANSHUANGKE].fCheck = &CPublicJudge::CheckQuanShuangKe;
	m_asFanInfo[FAN_QING1SHE].fCheck = &CPublicJudge::CheckQing1She;
	m_asFanInfo[FAN_1SHE3TONGSHUN].fCheck = &CPublicJudge::Check1She3TongShun;
	m_asFanInfo[FAN_1SHE3JIEGAO].fCheck = &CPublicJudge::Check1She3JieGao;
	m_asFanInfo[FAN_QUANDA].fCheck = &CPublicJudge::CheckQuanDa;
	m_asFanInfo[FAN_QUANZHONG].fCheck = &CPublicJudge::CheckQuanZhong;
	m_asFanInfo[FAN_QUANXIAO].fCheck = &CPublicJudge::CheckQuanXiao;
	m_asFanInfo[FAN_QINGLONG].fCheck = &CPublicJudge::CheckQingLong;
	m_asFanInfo[FAN_3SHE2LONG].fCheck = &CPublicJudge::Check3She2Long;
	m_asFanInfo[FAN_1SHE3BUGAO].fCheck = &CPublicJudge::Check1She3BuGao;
	m_asFanInfo[FAN_QUANDAI5].fCheck = &CPublicJudge::CheckQuanDai5;
	m_asFanInfo[FAN_3TONGKE].fCheck = &CPublicJudge::Check3TongKe;
	m_asFanInfo[FAN_3ANKE].fCheck = &CPublicJudge::Check3AnKe;
	m_asFanInfo[FAN_QUANBUKAO].fCheck = &CPublicJudge::CheckQuanBuKao;
	m_asFanInfo[FAN_ZHUHELONG].fCheck = &CPublicJudge::CheckZhuHeLong;
	m_asFanInfo[FAN_DAYU5].fCheck = &CPublicJudge::CheckDaYu5;
	m_asFanInfo[FAN_XIAOYU5].fCheck = &CPublicJudge::CheckXiaoYu5;
	m_asFanInfo[FAN_3FENGKE].fCheck = &CPublicJudge::Check3FengKe;
	m_asFanInfo[FAN_HUALONG].fCheck = &CPublicJudge::CheckHuaLong;
	m_asFanInfo[FAN_TUIBUDAO].fCheck = &CPublicJudge::CheckTuiBuDao;
	m_asFanInfo[FAN_3SHE3TONGSHUN].fCheck = &CPublicJudge::Check3She3TongShun;
	m_asFanInfo[FAN_3SHEJIEJIEGAO].fCheck = &CPublicJudge::Check3She3JieGao;
	m_asFanInfo[FAN_WUFAN].fCheck = &CPublicJudge::CheckWuFan;
	m_asFanInfo[FAN_MIAOSHOU].fCheck = &CPublicJudge::CheckMiaoShou;
	m_asFanInfo[FAN_HAIDI].fCheck = &CPublicJudge::CheckHaiDi;
	m_asFanInfo[FAN_GANGHU].fCheck = &CPublicJudge::CheckGangHu;
	m_asFanInfo[FAN_QIANGGANG].fCheck = &CPublicJudge::CheckQiangGang;
	m_asFanInfo[FAN_PENPENHU].fCheck = &CPublicJudge::CheckPenPenHu;
	m_asFanInfo[FAN_5MENQI].fCheck = &CPublicJudge::Check5MenQi;
	m_asFanInfo[FAN_QUANQIUREN].fCheck = &CPublicJudge::CheckQuanQiuRen;
	m_asFanInfo[FAN_2ANGANG].fCheck = &CPublicJudge::Check2AnGang;
	m_asFanInfo[FAN_2JIANKE].fCheck = &CPublicJudge::Check2JianKe;
	m_asFanInfo[FAN_QUANDAIYAO].fCheck = &CPublicJudge::CheckQuanDai1;
	m_asFanInfo[FAN_BUQIUREN].fCheck = &CPublicJudge::CheckBuQiuRen;
	m_asFanInfo[FAN_2MINGANG].fCheck = &CPublicJudge::Check2MinGang;
	m_asFanInfo[FAN_HUJUEZHANG].fCheck = &CPublicJudge::CheckHuJueZhang;
	m_asFanInfo[FAN_JIANKE].fCheck = &CPublicJudge::CheckJianKe;
	m_asFanInfo[FAN_QUANFENG].fCheck = &CPublicJudge::CheckQuanFeng;
	m_asFanInfo[FAN_MENGFENG].fCheck = &CPublicJudge::CheckMenFeng;
	m_asFanInfo[FAN_MENGQING].fCheck = &CPublicJudge::CheckMenQing;
	m_asFanInfo[FAN_PINHU].fCheck = &CPublicJudge::CheckPinHu;
	m_asFanInfo[FAN_4GUI1].fCheck = &CPublicJudge::Check4Gui1;
	m_asFanInfo[FAN_2TONGKE].fCheck = &CPublicJudge::Check2TongKe;
	m_asFanInfo[FAN_2ANKE].fCheck = &CPublicJudge::Check2AnKe;
	m_asFanInfo[FAN_ANGANG].fCheck = &CPublicJudge::CheckAnGang;
	m_asFanInfo[FAN_DUAN19].fCheck = &CPublicJudge::CheckDuan19;
	m_asFanInfo[FAN_YIBANGAO].fCheck = &CPublicJudge::CheckYiBanGao;
	m_asFanInfo[FAN_XIXIANGFENG].fCheck = &CPublicJudge::CheckXiXiangFeng;
	m_asFanInfo[FAN_LIAN6].fCheck = &CPublicJudge::CheckLian6;
	m_asFanInfo[FAN_LAOSHAOFU].fCheck = &CPublicJudge::CheckLaoShaoFu;
	m_asFanInfo[FAN_19KE].fCheck = &CPublicJudge::Check19Ke;
	m_asFanInfo[FAN_MINGANG].fCheck = &CPublicJudge::CheckMinGang;
	m_asFanInfo[FAN_QUE1MEN].fCheck = &CPublicJudge::CheckQue1Men;
	m_asFanInfo[FAN_WUZI].fCheck = &CPublicJudge::CheckWuZi;
	m_asFanInfo[FAN_BIANZANG].fCheck = &CPublicJudge::CheckBianZang;
	m_asFanInfo[FAN_KANZANG].fCheck = &CPublicJudge::CheckKanZang;
	m_asFanInfo[FAN_DANDIAO].fCheck = &CPublicJudge::CheckDanDiao;
	m_asFanInfo[FAN_ZIMO].fCheck = &CPublicJudge::CheckZiMo;
	m_asFanInfo[FAN_FLOWER].fCheck = NULL;

	// 大众添加的番种
	m_asFanInfo[FAN_4FANGDAFA].fCheck = &CPublicJudge::Check4FangDaFa;
	m_asFanInfo[FAN_TIANHU].fCheck = NULL;
	m_asFanInfo[FAN_DIHU].fCheck = NULL;
	m_asFanInfo[FAN_RENHU].fCheck = NULL;
	m_asFanInfo[FAN_8XIANGUOHAI].fCheck = &CPublicJudge::Check8Xian;
	m_asFanInfo[FAN_7QIANG1].fCheck = &CPublicJudge::Check7Qiang1;
	m_asFanInfo[FAN_TIANTING].fCheck = NULL;
	m_asFanInfo[FAN_LIZHI].fCheck = &CPublicJudge::CheckLiZhi;
	m_asFanInfo[FAN_258JONG].fCheck = &CPublicJudge::Check258Jong;
	m_asFanInfo[FAN_4FLOWER].fCheck = &CPublicJudge::Check4Flower;
	m_asFanInfo[FAN_4SEASON].fCheck = &CPublicJudge::Check4Season;
	m_asFanInfo[FAN_SEASONFLOWER].fCheck = &CPublicJudge::CheckSeasonFlower;
	m_asFanInfo[FAN_19JONG].fCheck = &CPublicJudge::Check19Jong;
	
	// 极速二麻番种
	m_asFanInfo[FAN_JSPKHU].fCheck = &CPublicJudge::CheckJSPKHu;

	// 番种分析的回调函数
	m_asFanInfo[FAN_DA4XI].fParse = &CPublicJudge::ParseDa4Xi;
	m_asFanInfo[FAN_DA3YUAN].fParse = &CPublicJudge::ParseDa3Yuan;
	m_asFanInfo[FAN_LVYISHE].fParse = &CPublicJudge::ParseLv1She;
	m_asFanInfo[FAN_9LIANBAODEN].fParse = &CPublicJudge::Parse9LianBaoDeng;
	m_asFanInfo[FAN_4GANG].fParse = &CPublicJudge::Parse4Gang;
	m_asFanInfo[FAN_LIAN7DUI].fParse = &CPublicJudge::ParseLian7Dui;
	m_asFanInfo[FAN_131].fParse = &CPublicJudge::Parse13Yao;
	m_asFanInfo[FAN_QING19].fParse = &CPublicJudge::ParseQing19;
	m_asFanInfo[FAN_XIAO4XI].fParse = &CPublicJudge::ParseXiao4Xi;
	m_asFanInfo[FAN_XIAO3YUAN].fParse = &CPublicJudge::ParseXiao3Yuan;
	m_asFanInfo[FAN_ZI1SHE].fParse = &CPublicJudge::ParseZi1She;
	m_asFanInfo[FAN_4ANKE].fParse = &CPublicJudge::Parse4AnKe;
	m_asFanInfo[FAN_1SHE2GLONG].fParse = &CPublicJudge::Parse1She2Long;
	m_asFanInfo[FAN_1SHE4TONGSHUN].fParse = &CPublicJudge::Parse1She4TongShun;
	m_asFanInfo[FAN_1SHE4JIEGAO].fParse = &CPublicJudge::Parse1She4JieGao;
	m_asFanInfo[FAN_1SHE4BUGAO].fParse = &CPublicJudge::Parse1She4BuGao;
	m_asFanInfo[FAN_3GANG].fParse = &CPublicJudge::Parse3Gang;
	m_asFanInfo[FAN_HUN19].fParse = &CPublicJudge::ParseHun19;
	m_asFanInfo[FAN_7DUI].fParse = &CPublicJudge::Parse7Dui;
	m_asFanInfo[FAN_7XINBUKAO].fParse = &CPublicJudge::Parse7XinBuKao;
	m_asFanInfo[FAN_QUANSHUANGKE].fParse = &CPublicJudge::ParseQuan2Ke;
	m_asFanInfo[FAN_QING1SHE].fParse = &CPublicJudge::ParseQing1She;
	m_asFanInfo[FAN_1SHE3TONGSHUN].fParse = &CPublicJudge::Parse1She3TongShun;
	m_asFanInfo[FAN_1SHE3JIEGAO].fParse = &CPublicJudge::Parse1She3JieGao;
	m_asFanInfo[FAN_QUANDA].fParse = &CPublicJudge::ParseQuanDa;
	m_asFanInfo[FAN_QUANZHONG].fParse = &CPublicJudge::ParseQuanZhong;
	m_asFanInfo[FAN_QUANXIAO].fParse = &CPublicJudge::ParseQuanXiao;
	m_asFanInfo[FAN_QINGLONG].fParse = &CPublicJudge::ParseQingLong;
	m_asFanInfo[FAN_3SHE2LONG].fParse = &CPublicJudge::Parse3She2Long;
	m_asFanInfo[FAN_1SHE3BUGAO].fParse = &CPublicJudge::Parse1She3BuGao;
	m_asFanInfo[FAN_QUANDAI5].fParse =&CPublicJudge:: ParseQuanDai5;
	m_asFanInfo[FAN_3TONGKE].fParse = &CPublicJudge::Parse3TongKe;
	m_asFanInfo[FAN_3ANKE].fParse = &CPublicJudge::Parse3AnKe;
	m_asFanInfo[FAN_QUANBUKAO].fParse = &CPublicJudge::ParseQuanBuKao;
	m_asFanInfo[FAN_ZHUHELONG].fParse = &CPublicJudge::ParseZhuHeLong;
	m_asFanInfo[FAN_DAYU5].fParse = &CPublicJudge::ParseDaYu5;
	m_asFanInfo[FAN_XIAOYU5].fParse = &CPublicJudge::ParseXiaoYu5;
	m_asFanInfo[FAN_3FENGKE].fParse = &CPublicJudge::Parse3FengKe;
	m_asFanInfo[FAN_HUALONG].fParse = &CPublicJudge::ParseHuaLong;
	m_asFanInfo[FAN_TUIBUDAO].fParse = &CPublicJudge::ParseTuiBuDao;
	m_asFanInfo[FAN_3SHE3TONGSHUN].fParse = &CPublicJudge::Parse3She3TongShun;
	m_asFanInfo[FAN_3SHEJIEJIEGAO].fParse = &CPublicJudge::Parse3She3JieGao;
	m_asFanInfo[FAN_WUFAN].fParse = &CPublicJudge::ParseWuFan;
	m_asFanInfo[FAN_MIAOSHOU].fParse = &CPublicJudge::ParseMiaoShou;
	m_asFanInfo[FAN_HAIDI].fParse = &CPublicJudge::ParseHaiDi;
	m_asFanInfo[FAN_GANGHU].fParse = &CPublicJudge::ParseGangKai;
	m_asFanInfo[FAN_QIANGGANG].fParse = &CPublicJudge::ParseQiangGang;
	m_asFanInfo[FAN_PENPENHU].fParse = &CPublicJudge::ParsePenPenHu;
	m_asFanInfo[FAN_HUN1SHE].fParse = &CPublicJudge::ParseHun1She;
	m_asFanInfo[FAN_3SHE3BUGAO].fParse = &CPublicJudge::Parse3She3BuGao;
	m_asFanInfo[FAN_5MENQI].fParse = &CPublicJudge::Parse5MenQi;
	m_asFanInfo[FAN_QUANQIUREN].fParse = &CPublicJudge::ParseQuanQiuRen;
	m_asFanInfo[FAN_2ANGANG].fParse = &CPublicJudge::Parse2AnGang;
	m_asFanInfo[FAN_2JIANKE].fParse = &CPublicJudge::Parse2JianKe;
	m_asFanInfo[FAN_QUANDAIYAO].fParse = &CPublicJudge::ParseQuan1;
	m_asFanInfo[FAN_BUQIUREN].fParse = &CPublicJudge::ParseBuQiuRen;
	m_asFanInfo[FAN_2MINGANG].fParse = &CPublicJudge::Parse2MinGang;
	m_asFanInfo[FAN_HUJUEZHANG].fParse = &CPublicJudge::ParseHuJueZhang;
	m_asFanInfo[FAN_JIANKE].fParse = &CPublicJudge::ParseJianKe;
	m_asFanInfo[FAN_QUANFENG].fParse = &CPublicJudge::ParseQuanFeng;
	m_asFanInfo[FAN_MENGFENG].fParse = &CPublicJudge::ParseMenFeng;
	m_asFanInfo[FAN_MENGQING].fParse = &CPublicJudge::ParseMenQing;
	m_asFanInfo[FAN_PINHU].fParse = &CPublicJudge::ParsePinHu;
	m_asFanInfo[FAN_4GUI1].fParse = &CPublicJudge::Parse4Gui1;
	m_asFanInfo[FAN_2TONGKE].fParse = &CPublicJudge::Parse2TongKe;
	m_asFanInfo[FAN_2ANKE].fParse = &CPublicJudge::Parse2AnKe;
	m_asFanInfo[FAN_ANGANG].fParse = &CPublicJudge::ParseAnGang;
	m_asFanInfo[FAN_DUAN19].fParse = &CPublicJudge::ParseDuan19;
	m_asFanInfo[FAN_YIBANGAO].fParse = &CPublicJudge::ParseYiBanGao;
	m_asFanInfo[FAN_XIXIANGFENG].fParse = &CPublicJudge::ParseXiXiangFeng;
	m_asFanInfo[FAN_LIAN6].fParse = &CPublicJudge::ParseLian6;
	m_asFanInfo[FAN_LAOSHAOFU].fParse = &CPublicJudge::ParseLaoShaoFu;
	m_asFanInfo[FAN_19KE].fParse = &CPublicJudge::Parse19Ke;
	m_asFanInfo[FAN_MINGANG].fParse = &CPublicJudge::ParseMinGang;
	m_asFanInfo[FAN_QUE1MEN].fParse = &CPublicJudge::ParseQue1Meng;
	m_asFanInfo[FAN_WUZI].fParse = &CPublicJudge::ParseWuZi;
	m_asFanInfo[FAN_BIANZANG].fParse = &CPublicJudge::ParseBianZang;
	m_asFanInfo[FAN_KANZANG].fParse = &CPublicJudge::ParseKanZang;
	m_asFanInfo[FAN_DANDIAO].fParse = &CPublicJudge::ParseDanDiao;
	m_asFanInfo[FAN_ZIMO].fParse = &CPublicJudge::ParseZiMo;
	m_asFanInfo[FAN_FLOWER].fParse = &CPublicJudge::ParseHua;
	m_asFanInfo[FAN_4FANGDAFA].fParse = &CPublicJudge::Parse4FangDaFa;
	m_asFanInfo[FAN_TIANHU].fParse = &CPublicJudge::ParseTianHu;
	m_asFanInfo[FAN_DIHU].fParse = &CPublicJudge::ParseDiHu;
	m_asFanInfo[FAN_RENHU].fParse = &CPublicJudge::ParseRenHu;
	m_asFanInfo[FAN_8XIANGUOHAI].fParse = &CPublicJudge::Parse8Xian;
	m_asFanInfo[FAN_7QIANG1].fParse = &CPublicJudge::Parse7Qiang1;
	m_asFanInfo[FAN_TIANTING].fParse = &CPublicJudge::ParseTianTing;
	m_asFanInfo[FAN_LIZHI].fParse = &CPublicJudge::ParseLiZhi;
	m_asFanInfo[FAN_258JONG].fParse = &CPublicJudge::Parse258Jong;
	m_asFanInfo[FAN_4FLOWER].fParse = &CPublicJudge::Parse4Flower;
	m_asFanInfo[FAN_4SEASON].fParse = &CPublicJudge::Parse4Season;
	m_asFanInfo[FAN_SEASONFLOWER].fParse = &CPublicJudge::ParseJiHua;
	m_asFanInfo[FAN_19JONG].fParse = &CPublicJudge::Parse19Jong;

	// 生成34张牌, 这34张牌的编号
	for( i = 0; i < 3; i++ )
	{
		for( int j = 0; j < 9; j++ )
		{
			m_asAllStone[i * 9 + j].nColor = i;
			m_asAllStone[i * 9 + j].nWhat = j;
			m_asAllStone[i * 9 + j].nID = ( i << 8 ) + ( j << 4 ) + 0x0f;
		}
	}
	for( i = 27; i < 31; i++ )
	{
		m_asAllStone[i].nColor = COLOR_WIND;
		m_asAllStone[i].nWhat = i - 27;
		m_asAllStone[i].nID = ( COLOR_WIND << 8 ) + ( ( i - 27 ) << 4 ) + 0x0f;
	}
	for( i = 31; i < 34; i++ )
	{
		m_asAllStone[i].nColor = COLOR_JIAN;
		m_asAllStone[i].nWhat = i - 31;
		m_asAllStone[i].nID = ( COLOR_JIAN << 8 ) + ( ( i - 31 ) << 4 ) + 0x0f;
	}

	// 加载番种树
	TCHAR szPath[ _MAX_PATH ];
	GetCurrentDirectory( _MAX_PATH, szPath );
	_tcscat_s( szPath,"\\fans.dat" );
	return SetFanTree( szPath );
}

// **************************************************************************************
// 
// 设置算番树
// 
// **************************************************************************************
BOOL CPublicJudge::SetFanTree( char szFileName[] )
{
	static TCHAR aszFans[ 9 ][ 6 ][ 256 ] = 
	{
		"2 12 13 15 21 22 24 25 26 27 29 30 35 36 39 51 55 54 61 62 63 67 68 70 71 76 77 78",
		"",
		"2 13 15 22 27 29 39 48 51 54 55 61 63 68 70 71 76 77 78",
		"22 27 29 38 39 40 49 51 54 55 61 63 68 69 70 71 74 76 77 78 90 93 94",
		"13 15 22 24 25 26 27 28 29 30 35 36 38 39 40 49 51 54 55 61 62 63 67 68 69 70 71 74 75 76 77 78 90 93 94",
		"",

		"2 3 21 22 24 25 26 27 29 30 35 36 39 51 54 55 61 63 66 67 68 70 71 72 73 76 77 78",
		"",
		"2 22 27 29 39 48 51 54 55 58 59 60 61 63 66 68 70 71 72 73 76 77 78",
		"22 27 29 38 39 40 49 51 54 55 58 59 60 61 63 66 68 69 70 71 72 73 74 75 76 77 78 93 94",
		"22  24 25 26 27 29 30 35 36 38 39 40 49 51 54 55 61 63 66 67 68 69 70 71 72 73 74 75 76 77 78 93 94",
		"38 40 49 50 51 54 55 58 59 60 61 63 66 69 71 72 73 76 77 78 ",

		"2 3 21 24 25 26 30 35 36 39 51 52 54 55 56 61 63 65 66 67 68 70 71 72 73 76 77 78",
		"",
		"2 9 39 48 51 52 53 54 55 56 58 59 60 61 63 65 66 68 70 71 72 73 76 77 78",
		"9 39 51 52 53 54 55 56 58 59 60 61 63 64 65 66 68 69 70 71 72 73 74 76 77 78",
		"24 25 26 30 35 36 39 51 52 54 55 56 61 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78",
		"50 51 52 54 55 56 58 59 60 61 63 64 65 66 69 71 72 73 76 77 78",

		"2 16 21 24 25 26 23 32 35 36 39 51 52 55 56 61 63 65 66 67 72 73 76 77 78",
		"",
		"1 2 8 9 16 23 32 37 39 48 51 52 53 54 55 56 58 59 60 61 63 65 66 72 73 76 77 78",
		"1 8 9 16 23 31 32 37 39 41 51 52 53 54 55 56 58 59 60 61 63 64 65 66 71 72 73 74 76 77 78 91",
		"16 23 24 25 26 30 31 32 35 36 39 41 51 52 54 55 56 61 63 64 65 66 67 71 72 73 74 75 76 77 78 91",
		"16 31 32 41 50 51 52 54 55 56 58 59 60 61 63 64 65 66 71 72 73 76 77 78",

		"2 4 11 14 16 20 21 23 24 25 26 35 36 39 47 51 52 55 61 63 65 66 67 32 72 78 56 73",
		"81 0 1 4 8 9 10 11 16 32 37 47 51 52 53 54 55 56 58 59 60 61 63 65 66 73 78",
		"0 1 2 4 8 9 11 14 16 17 23 32 37 39 47 48 51 52 53 55 56 58 59 60 61 63 65 66 72 73 78",
		"0 1 4 8 9 11 16 17 23 31 32 37 39 41 47 51 52 53 54 55 56 58 59 60 61 63 64 65 66 71 72 73 74 78 89 91",
		"4 7 11 14 16 20 23 24 25 26 30 31 32 35 36 39 41 47 51 52 54 55 56 61 63 64 65 66 67 71 72 73 74 75 78 89 91",
		"4 11 16 17 31 32 41 47 50 51 52 54 55 56 58 59 60 61 63 64 65 66 71 72 73 78 89",

		"2 5 12 18 21 24 25 26 30 35 36 39 63 67",
		"10 18 63",
		"2 17 18 39 48 63",
		"17 18 39 63 74",
		"7 18 24 25 26 35 36 39 63 67 74 75",
		"17 18 50 63",

		"",
		"",
		"",
		"",
		"",
		"6",

		"",
		"",
		"",
		"",
		"",
		"19 34 33",

		"",
		"",
		"",
		"34 55 58 59 60 61 63 66 72 73 76 77 78",
		"34 55 61 62 63 66 72 73 75 76 77 78",
		"34 50 55 58 59 60 61 63 66 72 73 76 77 78"
	};
	for( int i = 0; i < 9; i++ )
	{
		for( int j = 0; j < 6; j++ )
		{
			// 先清空
			m_avFansEntry[i][j].clear();
			if ( aszFans[ i ][ j ][ 0 ] != 0 )
			{
				TCHAR * pStream = aszFans[ i ][ j ];
				while ( pStream && pStream[0] != 0 )
				{
					int nFanID;
					pStream = GetPrivateInt( pStream, nFanID );
					m_avFansEntry[i][j].push_back( nFanID );
				}
			}
		}
	}

	return true;


/*
	// 从文件初始化算番入口集
	char szStream[256] = { 0 };
	char szSection[ 32 ] = { 0 } ;
	char szKeyName[ 32 ] = { 0 } ;
	char * pStream;
	int nFanID;
	int cnRead = 0;
	for( int i = 0; i < 9; i++ )
	{
		sprintf( szSection , "mode%d", i );
		for( int j = 0; j < 6; j++ )
		{
			// 先清空
			m_avFansEntry[i][j].clear();

			// 读取对应的条目
			sprintf( szKeyName , "color%d", j );
			cnRead += GetPrivateProfileString(szSection, szKeyName, "" , szStream, 
				sizeof( szStream ), szFileName );
			
			// 从读入的string里取得一个int流，逐个填充到入口集里
			if ( szStream[0] != 0 )
			{
				pStream = szStream;
				while ( pStream && pStream[0] != 0 )
				{
					pStream = GetPrivateInt( pStream, nFanID );
					m_avFansEntry[i][j].push_back( nFanID );
				}
			}
		}
	}

	return cnRead != 0;
*/
}


// **************************************************************************************
// 
// 设置混为指定的牌,并修改m_sStonesRelation结构
// 只修改算番会用到的成员:acnAllStones,acnAllColors,cnAllColors
// 
// **************************************************************************************
void CPublicJudge::UseHun( STONE &sHunStone, int nStoneValue, bool bAmend )
{
	sHunStone.nColor = m_asAllStone[nStoneValue].nColor;
	sHunStone.nWhat = m_asAllStone[nStoneValue].nWhat;

	if ( bAmend )
	{
		// 要修改m_sStonesRelation
		m_sStonesRelation.acnAllStones[nStoneValue]++;
		if ( m_sStonesRelation.acnAllColors[sHunStone.nColor] == 0 )
		{
			m_sStonesRelation.cnAllColors++;
		}
		m_sStonesRelation.acnAllColors[sHunStone.nColor]++;
		m_sStonesRelation.cnHun--;
		ASSERT( m_sStonesRelation.cnHun >= 0 );
	}
}

// **************************************************************************************
// 
// 设置混为指定的牌
// 
// **************************************************************************************
void CPublicJudge::UseHun( STONE &sHunStone, int nColor, int nWhat )
{
	sHunStone.nColor = nColor;
	sHunStone.nWhat = nWhat;
}


// **************************************************************************************
// 
// 将手中的牌排序(混牌在最后，其它牌按从小到大排列)
// 和的那张牌如果是混牌，排在所有混的最后面，目的是让它有机会和另一混牌组成将牌
// 
// **************************************************************************************
void CPublicJudge::Sort( STONE asStone[], int cnStone )
{
	ASSERT( cnStone != 0 );

	int k = 0;
	STONE sStone = { 0 };
	for( int i = cnStone - 1; i >= 0; i-- )
	{
		k = i;
		for( int j = i - 1; j >= 0; j-- )
		{
			if ( ( asStone[k].nColor << 4 ) + asStone[k].nWhat 
				< ( asStone[j].nColor << 4 ) + asStone[j].nWhat )
			{
				k = j;
			}
		}
		if ( k != i )
		{
			sStone = asStone[k];
			asStone[k] = asStone[i];
			asStone[i] = sStone;
		}
	}
}

// **************************************************************************************
// 
// 验和
// 
// **************************************************************************************
CHECKRESULT CPublicJudge::CheckWin( CHECKPARAM &sCheckParam, HURESULT &sHuResult )
{
	if ( InvalidParam( sCheckParam ) )
	{
		return F_NOTTING;
	}

	// 初始化数据
	memset( &sHuResult, 0, sizeof( HURESULT ) );
	// 将吃碰杠的分组拷到返回结构里
	memcpy( sHuResult.asGroup, sCheckParam.asShowGroup, 
		sizeof( STONEGROUP ) * sCheckParam.cnShowGroups );
	sHuResult.cnShowGroups = sCheckParam.cnShowGroups;
	sHuResult.nMenWind = sCheckParam.nMenWind;
	sHuResult.nQuanWind = sCheckParam.nQuanWind;
	sHuResult.nWinMode = sCheckParam.nWinMode;

	// 将传进来的参数暂存一下
	m_pCheckParam = &sCheckParam;

	// 不能破坏传进来的参数，后面算番还要用到,保存手中的牌
	STONE asHandStone[MAX_HAND_COUNT] = { 0 };
	memcpy( asHandStone, sCheckParam.asHandStone, sizeof( STONE ) * sCheckParam.cnHandStone );

	//将手中的牌排序(混牌在最后，其它牌按从小到大排列)
	Sort( asHandStone, sCheckParam.cnHandStone );

	// 扫描一下牌型
	memset( &m_sOriginalStoneRelation, 0, sizeof( m_sOriginalStoneRelation ) );
	GetStonesRelation( asHandStone, sCheckParam.cnHandStone, m_sOriginalStoneRelation );
//	m_sStonesRelation = m_sOriginalStoneRelation;
	int cnNormalStone = sCheckParam.cnHandStone - m_sOriginalStoneRelation.cnHun;

	CHECKRESULT eRet = F_NOTTING;
	HURESULT sHuResultTmp = sHuResult;

    sHuResultTmp.nResultant = JSPKHU;
	CHECKRESULT eRetTmp = CheckNormal( asHandStone, cnNormalStone, m_sOriginalStoneRelation.cnHun, 
							sCheckParam.cnShowGroups, sHuResultTmp );
	if ( sHuResultTmp.nMaxFans > sHuResult.nMaxFans )
	{
		sHuResult = sHuResultTmp;
	}
	if ( eRetTmp == T_OK ) 
	{
		// 至少有一种和法
		eRet = T_OK;
	}
	else if ( eRetTmp == F_NOENOUGHFANS && eRet == F_NOTTING )
	{
		// 至少有一种和法，只是番不够
		eRet = F_NOENOUGHFANS;
	}

	return eRet;
}

// 验听
CHECKRESULT CPublicJudge::CheckTing( CHECKPARAM &sCheckParam, CALLINFOVECTOR &vsCallInfo )
{
	// 初始化
	vsCallInfo.clear();
	
	int cnTryStone = sCheckParam.cnHandStone;	// 尝试打掉的牌张数

	// 将手中的牌排序
	if ( sCheckParam.cnHandStone % 3 == 1 )
	{
		// 只有13(或10、7、4、1)张牌时的听牌,传进来的牌少一张，第一个位置是空着的
		Sort( sCheckParam.asHandStone + 1, sCheckParam.cnHandStone );
		// 在所有牌的前面加一张特殊的牌进去验和
		sCheckParam.asHandStone[0].nID = 0xCCCC;
		sCheckParam.asHandStone[0].nColor = 0xCCCC;
		sCheckParam.asHandStone[0].nWhat = 0xCCCC;
		sCheckParam.cnHandStone++;
		cnTryStone = 1; // 只要尝试打掉第一张就行了，其它的牌不能打
	}
	else
	{
		Sort( sCheckParam.asHandStone, sCheckParam.cnHandStone );
	}
	
	CALLINFO sCallInfo = { 0 };
	CHECKRESULT eRet = F_NOTTING;
	
	for( int i = 0; i < cnTryStone; i++ )
	{
		// 用来保存本次结果，清零
		memset( &sCallInfo, 0, sizeof( sCallInfo ) );

		// 将准备替换的那张牌移到第一张的位置
		MAHJONGTILE sTileBak = sCheckParam.asHandStone[i];
		sCallInfo.nDiscardTileID = sTileBak.nID;
		// 这里只要将第一张牌移动到准备替换的那张牌所在的位置就行了
		memcpy( sCheckParam.asHandStone + i, sCheckParam.asHandStone, sizeof( MAHJONGTILE )  );
			
		HURESULT sHuResult;
		for( int j = 0; j < 34; j++ )
		{
			// 依次替换成其它的33张牌验和
			memset( &sHuResult, 0, sizeof( HURESULT ) );
			sCheckParam.asHandStone[0] = m_asAllStone[j];
			
			CHECKRESULT eResult = CheckWin( sCheckParam, sHuResult );
			if ( eResult == T_OK )
			{
				// 如果有无混这一番，还要看看不计无混是否够，不然有可能出现自摸混牌不能和的情况
				if ( sHuResult.nMaxFans - m_asFanInfo[FAN_WUHUN].nScore * sHuResult.anFans[FAN_WUHUN] 
					- sCheckParam.cnFlower < sCheckParam.nMinFan )
				{
					// 基本番不够
					eRet = F_NOENOUGHFANS;
				}
				else
				{
					eRet = eResult;
					// 这张牌也可和
					sCallInfo.asCallTileInfo[sCallInfo.cnCallTile].nCallTileID = m_asAllStone[j].nID;
					sCallInfo.asCallTileInfo[sCallInfo.cnCallTile].nFans = sHuResult.nMaxFans;
					sCallInfo.cnCallTile++;
				}
			}
			else if ( eResult == F_NOENOUGHFANS && eRet == F_NOTTING )
			{
				eRet = F_NOENOUGHFANS;
			}
		}
		
		// 看看打掉本张后是否能听
		if ( sCallInfo.cnCallTile != 0 )
		{
			// 能听，加到返回的vector里
			vsCallInfo.push_back( sCallInfo );
		}
		
		// 本张检验完了，还原
		memcpy( sCheckParam.asHandStone, sCheckParam.asHandStone + i, sizeof( MAHJONGTILE ) );
		memcpy( sCheckParam.asHandStone + i, &sTileBak, sizeof( MAHJONGTILE ) );
	}
	
	return eRet;
}

// **************************************************************************************
// 
// 对带混的分组算番
// 
// **************************************************************************************
CHECKRESULT CPublicJudge::EnumerateHunFans( int nHunGroupIndex, HURESULT &sHuResult )
{
	// 需要在这里变混，而不是在算番的函数里变混
	HURESULT sHuResultTmp = sHuResult;
	CHECKRESULT eRet = F_NOENOUGHFANS;
	int i = 0;
	int j = 0;
	BOOL bHunGroup = FALSE;
	if ( sHuResultTmp.asGroup[nHunGroupIndex].nGroupStyle == GROUP_STYLE_HUN )
	{
		// 这个分组确实全部是混牌
		bHunGroup = TRUE;
	}
	do 
	{
		// 最后一个分组必然是将
        if (1 > sHuResultTmp.cnGroups)
        {
            return eRet;
        }

        // 只循环一次
        i = 34;
		j = 0;
		do 
		{
			if ( bHunGroup )
			{
				if ( j < 21 )
				{
					// 把这个分组变成顺
					int nColor = j / 7;
					int nWhat = j % 7;
					UseHun( sHuResultTmp.asGroup[nHunGroupIndex].asStone[0], nColor, nWhat );
					UseHun( sHuResultTmp.asGroup[nHunGroupIndex].asStone[1], nColor, nWhat + 1 );
					UseHun( sHuResultTmp.asGroup[nHunGroupIndex].asStone[2], nColor, nWhat + 2 );
					sHuResultTmp.asGroup[nHunGroupIndex].nGroupStyle = GROUP_STYLE_SHUN;
				}
				else
				{
					// 把这个分组变成刻
					UseHun( sHuResultTmp.asGroup[nHunGroupIndex].asStone[0], j - 21 );
					UseHun( sHuResultTmp.asGroup[nHunGroupIndex].asStone[1], j - 21 );
					UseHun( sHuResultTmp.asGroup[nHunGroupIndex].asStone[2], j - 21 );
					sHuResultTmp.asGroup[nHunGroupIndex].nGroupStyle = GROUP_STYLE_KE;
				}
				j++;
			}
			else
			{
				// 只循环一次
				j = 55;
			}
			if ( EnumerateFans( sHuResultTmp ) == T_OK )
			{
				eRet = T_OK;
			} 
			if ( sHuResultTmp.nMaxFans > sHuResult.nMaxFans )
			{
				sHuResult = sHuResultTmp;
			}
		} while( j < 55 );
	} while( i < 34 );
	
	return eRet;
}

// **************************************************************************************
// 
// 算番
// 
// **************************************************************************************
CHECKRESULT CPublicJudge::EnumerateFans( HURESULT &sHuResult )
{
	// 初始化sHuResult
	sHuResult.nMaxFans = 0;
	memset( sHuResult.anFans, 0, sizeof( sHuResult.anFans ) );
	memset( &m_abEnableRule, 1, sizeof( m_abEnableRule ) );
    GetGroupsInfo(sHuResult.asGroup, m_sGroupsRelation, m_sStonesRelation);

    //碰碰胡
    if (CheckPenPenHu(sHuResult) != 0)
    {
        sHuResult.anFans[FAN_PENPENHU] = 1;
        sHuResult.nMaxFans += 20;
    }

    // 清一色
    if (CheckQing1She(sHuResult) != 0)
    {
        sHuResult.anFans[FAN_QING1SHE] = 1;
        sHuResult.nMaxFans += 10;
    }

    // 字一色
    if (CheckZi1She(sHuResult) != 0)
    {
        sHuResult.anFans[FAN_ZI1SHE] = 1;
        sHuResult.nMaxFans += 30;
    }

    //明杠
    if (CheckMinGang(sHuResult) != 0)
    {
        sHuResult.anFans[FAN_MINGANG] = 1;
        sHuResult.nMaxFans += 0;
    }

    //暗杠
    if (CheckAnGang(sHuResult) != 0)
    {
        sHuResult.anFans[FAN_ANGANG] = 1;
        sHuResult.nMaxFans += 0;
    }

	// 杠开
	if ( m_pCheckParam->nWinMode & WIN_MODE_GANGSHANGHU )
	{
		sHuResult.anFans[FAN_GANGHU] = 1;
        sHuResult.nMaxFans += 30;
		// 不计自摸 
		m_abEnableRule[FAN_ZIMO] = false;
	}

    // 天和
    if ((m_pCheckParam->nWinMode & WIN_MODE_TIANHU) == WIN_MODE_TIANHU)
    {
        sHuResult.anFans[FAN_TIANHU] = 1;
        sHuResult.nMaxFans += 80;
        // 天和不计天听，不计自摸
        m_abEnableRule[FAN_TIANTING] = FALSE;
        m_abEnableRule[FAN_ZIMO] = false;
    }

    // 地和/人和、均算地胡
    if ((m_pCheckParam->nWinMode & WIN_MODE_DIHU) == WIN_MODE_DIHU 
        || (m_pCheckParam->nWinMode & WIN_MODE_RENHU) == WIN_MODE_RENHU)
    {
        sHuResult.anFans[FAN_DIHU] = 1;
        sHuResult.nMaxFans += 40;
    }

    // 立直，即听牌
    if (CheckLiZhi(sHuResult) != 0)
    {
        sHuResult.anFans[FAN_LIZHI] = 1;
        sHuResult.nMaxFans += 10;
    }

    // 天听
    if (m_abEnableRule[FAN_TIANTING]
        && (m_pCheckParam->nWinMode & WIN_MODE_TIANTING) == WIN_MODE_TIANTING)
    {
        sHuResult.anFans[FAN_TIANTING] = 1;
        sHuResult.nMaxFans += 20;
    }

    // 自摸
    if (m_abEnableRule[FAN_ZIMO])
    {
        if (m_pCheckParam->nWinMode & WIN_MODE_ZIMO)
        {
            sHuResult.anFans[FAN_ZIMO] = 1;
            sHuResult.nMaxFans += 10;
        }
    }

    // 极速麻将算番
    int cnFans = CheckJSPKHu(sHuResult);
    if (cnFans != 0)
    {
        sHuResult.anFans[FAN_JSPKHU] = cnFans;
        sHuResult.nMaxFans += cnFans;
    }

    // 算出番数对应的分数
    sHuResult.nScoreOfFan = sHuResult.nMaxFans;
    return T_OK;
}

// **************************************************************************************
// 
// 验证和牌是否有效
// 
// **************************************************************************************
BOOL CPublicJudge::CanWin( CHECKPARAM &sCheckParam, WININFO &sWinInfo )
{
	// 将传进来的参数暂存一下(这个参数是绝对可靠的)
	m_pCheckParam = &sCheckParam;

	// 验证sWinInfo的正确性
	if ( !ValidWinInfo( sCheckParam, sWinInfo ) )
	{
		return FALSE;
	}

	return EnumerateFans( sWinInfo ) == T_OK;
}

// **************************************************************************************
// 
// 是否有效的和牌信息
// 
// **************************************************************************************
BOOL CPublicJudge::ValidWinInfo( CHECKPARAM &sCheckParam, WININFO &sWinInfo )
{
	// 1.首先要看和牌牌型是否正确
	// 2.其次要看牌数对不对，该玩家有没有这些牌,发过来的牌里有没有重复的
	// 3.再次要看分组是否合法
	int anAppearedTileID[20];	// 已出现过的ID
	int cnAppearedTile = 0;			// 已出现过的ID个数
	memset( anAppearedTileID, 0, sizeof( anAppearedTileID ) );

    // 分组数量
    int nGroupsCountByRule = 2;
    // 分组数校验
    if (sWinInfo.cnGroups != nGroupsCountByRule)
    {
        ASSERT(FALSE);
        return FALSE;
    }

    for (int i = 0; i < sWinInfo.cnGroups; i++)
    {
        // 分组是否合法
        int cnGroupTile = 3;
        switch (sWinInfo.asGroup[i].nGroupStyle)
        {
            case GROUP_STYLE_SHUN:
                if (!IsShun(sWinInfo.asGroup[i].asStone))
                {
                    ASSERT(FALSE);
                    return FALSE;
                }
                break;
            case GROUP_STYLE_KE:
                if (!IsKe(sWinInfo.asGroup[i].asStone))
                {
                    ASSERT(FALSE);
                    return FALSE;
                }
                break;
            case GROUP_STYLE_JONG:
                if (!IsJong(sWinInfo.asGroup[i].asStone))
                {
                    ASSERT(FALSE);
                    return FALSE;
                }
                cnGroupTile = 2;
                break;
            case GROUP_STYLE_ANGANG:
            case GROUP_STYLE_MINGGANG:
                if (!IsGang(sWinInfo.asGroup[i].asStone))
                {
                    ASSERT(FALSE);
                    return FALSE;
                }
                cnGroupTile = 4;
                break;
            default:
                return FALSE;
        }

        // 所有牌必须得有效
        for (int j = 0; j < cnGroupTile; j++)
        {
            if (!ValidTile(sWinInfo.asGroup[i].asStone[j], sCheckParam,
                anAppearedTileID, cnAppearedTile))
            {
                ASSERT(FALSE);
                return FALSE;
            }
        }
    }
	return TRUE;
}

// **************************************************************************************
// 
// 检验七对和法
// 
// **************************************************************************************
CHECKRESULT CPublicJudge::CheckSevenPairs( STONE asStone[], int cnNormalStone, int cnHun, 
											HURESULT &sHuResult )
{
	// 首先看看是否能和
	if ( cnNormalStone + cnHun != 14 )
	{
		return F_NOTTING;
	}
	int cnPairs = 0;
	int i = 0;
	for(i = 0; i < 34; i++ )
	{
		if ( m_sOriginalStoneRelation.acnHandStones[i] < 2 )
		{
			continue;
		}
		
		if ( m_sOriginalStoneRelation.acnHandStones[i] == 4 )
		{
			cnPairs += 2;
		}
		else
		{
			cnPairs++;
		}
	}
	//和七对要求对子数加上混牌数 >= 7
	if ( cnPairs + cnHun < 7 )
	{
		return F_NOTTING;
	}

	// 再将混牌变成需要的牌
	memcpy( sHuResult.asHandStone, asStone, sizeof( STONE ) * 14 );
	int nHunIndex = cnNormalStone;
	m_sStonesRelation = m_sOriginalStoneRelation;
	if ( cnHun != 0 )
	{
		for( i = 0; i < 34; i++ )
		{
			if ( m_sOriginalStoneRelation.acnHandStones[i] % 2 != 0 )
			{
				ASSERT( nHunIndex < cnNormalStone + cnHun );
				UseHun( sHuResult.asHandStone[nHunIndex], i, true );
				nHunIndex++;
			}
			if ( nHunIndex == cnNormalStone + cnHun )
			{
				// 没混了
				break;
			}
		}
	}
//	sHuResult.nResultant = QIDUI;
//	Sort( sHuResult.asHandStone, 14 );
//	return EnumerateFans( sHuResult );

	// 需要在这里变混，而不是在算番的函数里变混
	HURESULT sHuResultTmp = sHuResult;
	sHuResultTmp.nResultant = QIDUI;
	CHECKRESULT eRet = F_NOENOUGHFANS;
	int j = 0;
	i = 0;
	do 
	{
		// 在下面会修改m_sStonesRelation,保存一下
		STONESRELATION sStonesRelation1 = m_sStonesRelation;
		if ( nHunIndex != cnNormalStone + cnHun )
		{
			// 第一对混
			ASSERT( ( cnNormalStone + cnHun - nHunIndex ) % 2 == 0 );
			UseHun( sHuResultTmp.asHandStone[nHunIndex], i, true );
			UseHun( sHuResultTmp.asHandStone[nHunIndex + 1], i, true );
			i++;
		}
		else
		{
			// 只循环一次
			i = 34;
		}
		j = 0;
		do 
		{
			// 在下面会修改m_sStonesRelation,保存一下
			STONESRELATION sStonesRelation2 = m_sStonesRelation;
			if ( nHunIndex == 10 )
			{
				// 还有一对混
				UseHun( sHuResultTmp.asHandStone[12], j, true );
				UseHun( sHuResultTmp.asHandStone[13], j, true );
				j++;
			}
			else
			{
				// 只循环一次
				j = 34;
			}
			Sort( sHuResult.asHandStone, 14 );
			if ( EnumerateFans( sHuResultTmp ) == T_OK )
			{
				eRet = T_OK;
			} 
			if ( sHuResultTmp.nMaxFans > sHuResult.nMaxFans )
			{
				sHuResult = sHuResultTmp;
			}
			// 在下一次循环前恢复
			m_sStonesRelation = sStonesRelation2;
		} while( j < 34 );
		// 在下一次循环前恢复
		m_sStonesRelation = sStonesRelation1;
	} while( i < 34 );

	sHuResult.cnGroups = 14;
	return eRet;
}

// **************************************************************************************
// 
// 检验十三幺和法
// 
// **************************************************************************************
CHECKRESULT CPublicJudge::CheckThirteenUnios( STONE asStone[], int cnNormalStone, 
											 int cnHun, HURESULT &sHuResult )
{
	int acnMax[34] = { 2, 0, 0, 0, 0, 0, 0, 0, 2,// 万
					   2, 0, 0, 0, 0, 0, 0, 0, 2,// 条
					   2, 0, 0, 0, 0, 0, 0, 0, 2,// 饼
					   2, 2, 2, 2, 2, 2, 2 };
	int nHunIndex = cnNormalStone;
	for( int i = 0; i < 34; i++ )
	{
		if ( m_sOriginalStoneRelation.acnHandStones[i] > acnMax[i] )
		{
			return F_NOTTING;
		}
		
		if ( acnMax[i] != 0 && m_sOriginalStoneRelation.acnHandStones[i] == 0 )
		{
			// 缺一张，用混补
			if ( nHunIndex == 14 )
			{
				// 没混了
				return F_NOTTING;
			}
			UseHun( asStone[nHunIndex], i );
			nHunIndex++;
		}
	}
	if ( nHunIndex != 14 )
	{
		// 混还没用完，变成一万
		ASSERT( nHunIndex == 13 );// 最多只剩一张混了
		UseHun( asStone[nHunIndex], 0 );
	}

/*
	for( int i = 0; i < cnNormalStone; i++ )
	{
		if ( asStone[i].nColor < COLOR_WIND )
		{
			// 序数牌，必须1或9
			if ( asStone[i].nWhat != STONE_NO1 && asStone[i].nWhat != STONE_NO9 )
			{
				break;
			}
		}
	}
	if ( i != cnNormalStone )
	{
		return F_NOTTING;
	} 

*/
	sHuResult.nResultant = SHISHANYAO;
	sHuResult.cnGroups = 14;
	memcpy( sHuResult.asHandStone, asStone, sizeof( STONE ) * 14 );
	return EnumerateFans( sHuResult );
}

// **************************************************************************************
// 
// 检验全不靠和法
// 
// **************************************************************************************
CHECKRESULT CPublicJudge::CheckAllLonely( STONE asStone[], int cnNormalStone, int cnHun, 
										HURESULT &sHuResult )
{
	// 全不靠
	if ( m_sOriginalStoneRelation.acnHandColors[COLOR_WAN] > 3 
		|| m_sOriginalStoneRelation.acnHandColors[COLOR_TIAO] > 3 
		|| m_sOriginalStoneRelation.acnHandColors[COLOR_BING] > 3 )
	{
		// 全不靠要求每张牌都不相同，序数牌每门不能超过3张
		return F_NOTTING;
	}

	BOOL abHadStone[3] = { 0 };
	int anFirstWhat[3] = { -1, -1, -1 };	// 每个龙组的第一张牌数字
	int i = 0;
	for(i = 0; i < 3; i++ )
	{
		int nColorFirstIndx = ( i << 3 ) + i;//i * 9;
		for( int j = 0; j < 9; j++ )
		{
			if ( m_sOriginalStoneRelation.acnHandStones[nColorFirstIndx + j] == 0 )
			{
				continue;
			}
			if ( anFirstWhat[i] == -1 )
			{
				// 记下本花色(也即本龙组)第一张牌应该是什么数字
				anFirstWhat[i] = j % 3;

				if ( abHadStone[anFirstWhat[i]] )
				{
					// 其它花色已有这个系列的牌了
					return F_NOTTING;
				}
				abHadStone[anFirstWhat[i]] = TRUE;				
			}

			if ( j != anFirstWhat[i] && ( j - anFirstWhat[i] ) % 3 != 0 ) 
			{
				// 数字差必须是3的倍数
				return F_NOTTING;
			}
		}
	}
	int cnWind = 0;
	for( i = 27; i < 34; i++ )
	{
		if ( m_sOriginalStoneRelation.acnHandStones[i] > 1 )
		{
			return F_NOTTING;
		}
		cnWind += m_sOriginalStoneRelation.acnHandStones[i];
	}

	memcpy( sHuResult.asHandStone, asStone, sizeof( STONE ) * 14 );
	m_sStonesRelation = m_sOriginalStoneRelation;// 分组后将出现变化
	if ( cnHun > 0 )
	{
		int nHunIndex = cnNormalStone;
		if ( cnWind + cnHun >= 7 )
		{
			// 设置混，成为七星不靠和牌牌型
			for( i = 27; i < 34; i++ )
			{
				if ( 0 == m_sOriginalStoneRelation.acnHandStones[i] )
				{
					UseHun( sHuResult.asHandStone[nHunIndex], i );
					nHunIndex++;
				}
			}
		}
		
		// 剩下的混牌以序数牌，字牌的顺序一一设定
		UseHunForLong( sHuResult.asHandStone + nHunIndex, cnNormalStone + cnHun - nHunIndex, anFirstWhat );
	}
	
	sHuResult.nResultant = QUANBUKAO;
	sHuResult.cnGroups = 14;
	return EnumerateFans( sHuResult );
}


// **************************************************************************************
// 
// 是不是13幺
// 
// **************************************************************************************
BOOL CPublicJudge::IsThirteenUnios( MAHJONGTILE asTile[] )
{
	// 1.相同的牌最多只能有两张
	// 2.只能有19风箭牌
	BOOL bMoreTile = FALSE;
	int acnTile[5][9] = { 0	};
	for( int i = 0; i < 14; i++ )
	{
		acnTile[asTile[i].nColor][asTile[i].nWhat]++;
		if ( acnTile[asTile[i].nColor][asTile[i].nWhat] > 1 )
		{
			if ( bMoreTile )
			{
				return FALSE;
			}
			bMoreTile = TRUE;
		}
		if ( asTile[i].nColor == COLOR_WIND || asTile[i].nColor == COLOR_JIAN )
		{
			continue;
		}
		if ( asTile[i].nWhat != STONE_NO1 && asTile[i].nWhat != STONE_NO9 )
		{
			return FALSE;
		}
	}

	return TRUE;
}

// **************************************************************************************
// 
// 是不是全不靠
// 
// **************************************************************************************
BOOL CPublicJudge::IsAllLonely( MAHJONGTILE asTile[] )
{
	// 先将据有牌按正常的顺序排列
	Sort( asTile, 14 );

	int nLastColor = 0;			// 上一张牌的花色
	int cnSameColorTile = 0;	// 同一花色的牌的张数
	int nLastWhat = asTile[0].nWhat;// 同一花色的上一张牌的点数
	BOOL abHad[9] = { 0 };		// 每个数字只能出现一次
	abHad[nLastWhat] = TRUE;
	for( int i = 1; i < 14; i++ )
	{
		if ( asTile[i].nColor >= COLOR_WIND )
		{
			// 风牌箭牌，在下面检验
			if ( asTile[i].nColor == nLastColor && asTile[i].nWhat == nLastWhat )
			{
				// 不能重复
				return FALSE;
			}
			continue;;
		}
		
		if ( asTile[i].nColor != nLastColor )
		{
			// 新的花色了
			nLastColor = asTile[i].nColor;
			nLastWhat = asTile[i].nWhat;
			cnSameColorTile = 0;
			if ( abHad[nLastWhat] )
			{
				// 这个数字已出现过了
				return FALSE;
			}
			abHad[nLastWhat] = TRUE;
			continue;
		}

		// 这一张牌跟上一张牌花色相同
		cnSameColorTile++;
		if ( cnSameColorTile > 3 )
		{
			// 一个花色最多只能有3张
			return FALSE;
		}
		if ( ( asTile[i].nWhat - nLastWhat ) % 3 != 0 )
		{
			// 牌差得是3的倍数
			return FALSE;
		}
		nLastWhat = asTile[i].nWhat;
		if ( abHad[nLastWhat] )
		{
			// 这个数字已出现过了
			return FALSE;
		}
		abHad[nLastWhat] = TRUE;
	}

	return TRUE;
}

// **************************************************************************************
// 
// 检验是否可和普通牌型，算出最大番数
// 
// **************************************************************************************
CHECKRESULT CPublicJudge::CheckNormal( STONE asStone[], int cnNormalStone, int cnHun, 
										int cnGrouped, HURESULT &sHuResult )
{
	// 选将
	int anJongIndex[13][2];
	int cnPossibleJongs = EnumerateJong( asStone, cnNormalStone, cnHun, anJongIndex );
	if ( 0 == cnPossibleJongs ) 
	{
		return F_NOTTING;
	}

	STONEGROUP sJongGroup = { 0	};
	sJongGroup.nGroupStyle = GROUP_STYLE_JONG;
	STONE asLeftStone[15] = { 0 };	// 除掉将牌后还剩下的牌
	CQuadrantTree  cWinTree;
	HURESULT sHuResultTmp = sHuResult;
//	sHuResultTmp.nResultant = NORMAL;
	sHuResultTmp.cnGroups = m_cnMaxGroups;
	int cnLeftStone;
	int cnLeftHun;
	CHECKRESULT sRet = F_NOTTING;
	for( int i = 0; i < cnPossibleJongs; i++ )
	{
		sJongGroup.asStone[0] = asStone[anJongIndex[i][0]];
		sJongGroup.asStone[1] = asStone[anJongIndex[i][1]];
		memcpy( asLeftStone, asStone, sizeof( STONE ) * anJongIndex[i][0] );//第一张之前的

        cnLeftStone = cnNormalStone - 2;
        cnLeftHun = cnHun;
        // 第二张之后的的牌拷入剩余牌数组
        memcpy(asLeftStone + anJongIndex[i][1] - 1, asStone + anJongIndex[i][1] + 1,
            sizeof(STONE) * (cnNormalStone + cnHun - anJongIndex[i][1] - 1));

		if ( cWinTree.Create( sJongGroup, asLeftStone, cnLeftStone, cnLeftHun ) ) 
		{
			// 创建成功,说明至少有一种和法
			int cnWinPath = cWinTree.GetPathCount();
			for( int j = 0; j < cnWinPath; j++ )
			{
				// 遍历所有和法，算出所有番种
				cWinTree.GetPath( j, sHuResultTmp.asGroup + cnGrouped );
				memset( &m_sStonesRelation, 0, sizeof( STONESRELATION ) );
//				if ( EnumerateFans( sHuResultTmp ) == F_NOENOUGHFANS )
				CHECKRESULT eRetTmp = EnumerateHunFans( cnGrouped, sHuResultTmp );
				// 比较一下，是否比以前算出的番数大
				if ( sHuResultTmp.nMaxFans > sHuResult.nMaxFans ) 
				{
					sHuResult = sHuResultTmp;
				}
				if ( eRetTmp == T_OK ) 
				{
					// 至少有一种和法
					sRet = T_OK;
				}
				else if ( eRetTmp == F_NOENOUGHFANS && sRet == F_NOTTING )
				{
					// 至少有一种和法，只是番不够
					sRet = F_NOENOUGHFANS;
				}
			}
		}
	}

	sHuResult.cnGroups = m_cnMaxGroups;
	return sRet;
}

// **************************************************************************************
// 
// 取得牌型信息
// 
// **************************************************************************************
void CPublicJudge::GetStonesRelation( STONE asHandStone[], int cnHandStone, 
							   STONESRELATION &sStonesRelation )
{
	sStonesRelation.bNoEat = TRUE;
	int nColor, nWhat;
	//	BOOL abAppearedColor[5] = { 0 };		// 已出现过的花色
	int i = 0;
	for(i = 0; i < cnHandStone; i++ )
	{
		nColor = asHandStone[i].nColor;
		nWhat = asHandStone[i].nWhat;
		if ( sStonesRelation.acnHandStones[GetValue( nColor, nWhat )] == 0 )
		{
			sStonesRelation.cnStoneTypes++;
		}
		sStonesRelation.acnHandStones[GetValue( nColor, nWhat )]++;
		if ( sStonesRelation.acnHandColors[nColor] == 0 )
		{
			sStonesRelation.cnHandColors++;
		}
		sStonesRelation.acnHandColors[nColor]++;
	}
	
	// 计算一下所有牌的花色个数和每种牌个数
	sStonesRelation.cnAllColors = sStonesRelation.cnHandColors;
	memcpy( sStonesRelation.acnAllColors, sStonesRelation.acnHandColors, 
		sizeof( sStonesRelation.acnHandColors ) );
	memcpy( sStonesRelation.acnAllStones, sStonesRelation.acnHandStones, 
		sizeof( sStonesRelation.acnHandStones ) );
	if ( cnHandStone < m_cnMaxHandStone )
	{
		// 吃碰杠的牌
		for( i = 0; i < m_pCheckParam->cnShowGroups; i++ )
		{
			nColor = m_pCheckParam->asShowGroup[i].asStone[0].nColor;
			nWhat = m_pCheckParam->asShowGroup[i].asStone[0].nWhat;
			int nIndex = GetValue( nColor, nWhat );
			if ( sStonesRelation.acnAllColors[nColor] == 0 )
			{
				// 这种花色前面未出现过，花色数加1
				sStonesRelation.cnAllColors++;
			}
			sStonesRelation.acnAllColors[nColor] += 3;
			if ( m_pCheckParam->asShowGroup[i].nGroupStyle == GROUP_STYLE_SHUN )
			{
				// 肯定是吃的牌
				sStonesRelation.bNoEat = FALSE;
				sStonesRelation.acnAllStones[nIndex]++;
				sStonesRelation.acnAllStones[nIndex + 1]++;
				sStonesRelation.acnAllStones[nIndex + 2]++;
			}
			else if ( m_pCheckParam->asShowGroup[i].nGroupStyle == GROUP_STYLE_KE )
			{
				// 刻
				sStonesRelation.acnAllStones[nIndex] += 3;
			}
			else
			{
				// 杠
				sStonesRelation.acnAllStones[nIndex] += 4;
			}
		}
	}
}

// **************************************************************************************
// 
// 看看哪些牌可做将
// 
// **************************************************************************************
int CPublicJudge::EnumerateJong( STONE asStone[], int cnNormalStone, int cnHun, 
								int anJongIndex[][2] )
{
	// 手中牌张数信息，如果第i张牌有n张，那么acnStone[i]为n，i之后的n-1个数组元素为0
	int acnStone[MAX_HAND_COUNT] = { 0 };
	int i = 0;
	while( i < cnNormalStone )
	{
		acnStone[i] = 1;
		for( int j = i + 1; j < cnNormalStone; j++ )
		{
			if ( IsSameStone( asStone[j], asStone[i] ) )
			{
				acnStone[i]++;
			}
			else
			{
				break;
			}
		}
		i += acnStone[i];
	}

	int cnJong = 0;
	for ( i = 0; i < cnNormalStone; i++ )
	{
		if ( 0 == acnStone[i] )
		{
			continue;
		}

		if ( 1 == acnStone[i] )
		{
			// 这张牌只有一张,只能拿混补
			if ( cnHun == 0 )
			{
				// 没有混，不能做将
				continue;
			}
			anJongIndex[cnJong][0] = i;
			anJongIndex[cnJong][1] = cnNormalStone + cnHun - 1;
		}
		else
		{
			// 多于一张
			anJongIndex[cnJong][0] = i;
			anJongIndex[cnJong][1] = i + 1;
		}
		cnJong++;
	}

	if ( cnHun > 1 )
	{
		// 有1张以上的混，可以用两张混做将
		anJongIndex[cnJong][0] = cnNormalStone + cnHun - 2;
		anJongIndex[cnJong][1] = cnNormalStone + cnHun - 1;
		cnJong++;
	}

	return cnJong;
}

// **************************************************************************************
// 
// 指定若干张混为“龙”组中缺的几张牌
// 
// **************************************************************************************
void CPublicJudge::UseHunForLong( STONE asHun[], int cnHun, int anFirstWhat[] )
{
	if ( cnHun <= 0 )
	{
		return;
	}
	
	int cnHunUsed = 0;
	int i = 0;
	for(i = 0; i < 3; i++ )
	{
		// 三个花色
		int nColorFirstIndex = ( i << 8 ) + i;
		if ( anFirstWhat[i] == -1 )
		{
			// 这个花色没牌,确定该补哪个系列的牌
			int j = 0;
			for(j = 0; j < 3; j++ )
			{
				// 看看哪个数字没被用过
				if ( anFirstWhat[0] != j && anFirstWhat[1] != j && anFirstWhat[2] != j )
				{
					anFirstWhat[i] = j;
					break;
				}
			}
			ASSERT( j < 3 );
		}
			
		for( int k = anFirstWhat[i]; k < 9; k += 3 )
		{
			int nStoneValue = GetValue( i, k );
			if ( m_sStonesRelation.acnHandStones[nStoneValue] != 0 )
			{
				continue;
			}
			UseHun( asHun[cnHunUsed], nStoneValue );
			cnHunUsed++;
			if ( cnHunUsed == cnHun )
			{
				return;
			}
		}
	}

	// 还有多余的混，加到字牌里
	ASSERT( cnHunUsed < cnHun );
	for( i = 27; i < 34; i++ )
	{
		if ( m_sStonesRelation.acnHandStones[i] != 0 )
		{
			continue;
		}
		UseHun( asHun[cnHunUsed], i );
		cnHunUsed++;
		if ( cnHunUsed == cnHun )
		{
			return;
		}
	}

	// 在上面就应该返回了
	ASSERT( FALSE );

/*
	int cnHunUsed = 0;
	int nPennilessColor = -1;	// 一张牌也没有的花色
//	int anFirstWhat[3] = { -1, -1, -1 };	// 每个龙组的第一张牌数字
	for( int i = 0; i < 3; i++ )
	{
		// 三个花色
		int nFirstIndex = ( i << 8 ) + i;
		for( int j = 0; j < 9; j++ )
		{
			// 找到本花色的第一张牌
			if ( acnStone[nFirstIndex + j] != 0 )
			{
				break;
			}
		}
		if ( j == 9 )
		{
			// 本花色一张牌也没有，全要用混补
			nPennilessColor = i;
			continue;
		}

		anFirstWhat[i] = j % 3;
		for( int k = anFirstWhat[i]; k < 9; k += 3 )
		{
			if ( k == j || acnStone[k] != 0 )
			{
				continue;
			}
			asHun[cnHunUsed].nColor = i;
			asHun[cnHunUsed].nWhat = k;
			cnHunUsed++;
		}
	}

	// 对一张也没的花色加混
	if ( nPennilessColor != -1 )
	{
		// 每个花色的第一张只能是1、2、3
		for( i = 0; i < 3; i++ )
		{
			// 看看哪个数字没被用过
			if ( anFirstWhat[0] != i && anFirstWhat[1] != i && anFirstWhat[2] != i )
			{
				break;
			}
		}
		ASSERT( i < 3 );

		for( int j = 0; j < 3; j++ )
		{
			asHun[cnHunUsed].nColor = nPennilessColor;
			asHun[cnHunUsed].nWhat = i + ( j << 1 ) + j;//j * 3;
		}
	}
*/

}

// **************************************************************************************
// 
// 给定一个ID号，在分组信息里搜索这张牌，得到它的花色点数,并返回所在分组索引号
//
// **************************************************************************************
BOOL CPublicJudge::GetTileInfo( int nTileID, const STONEGROUP asGroup[], TILEINFO& sTileInfo )
{
	for( int i = 0; i < m_cnMaxGroups; i++ )
	{
		int cnGroupStones = 3;
		if ( asGroup[i].nGroupStyle == GROUP_STYLE_JONG ) 
		{
			cnGroupStones = 2;
		}
		for( int j = 0; j < cnGroupStones; j++ )
		{
			if ( nTileID == asGroup[i].asStone[j].nID )
			{
				sTileInfo.nColor = asGroup[i].asStone[j].nColor;
				sTileInfo.nWhat = asGroup[i].asStone[j].nWhat;
				sTileInfo.nGroupIndex = i;
				sTileInfo.nIndex = j;
				return TRUE;
			}
		}
	}

	// 必须成功
	ASSERT( FALSE );
	return FALSE;
}


// **************************************************************************************
// 
// 取得分组信息
// 填写GROUPSRELATION结构，同时根据分好组的牌填写STONESRELATION结构的部份信息
// STONESRELATION结构在算番时有用的成员：cnAllColors, acnAllColors, acnAllStones, bNoEat
// cnStoneTypes
//
// **************************************************************************************
void CPublicJudge::GetGroupsInfo( STONEGROUP asStoneGroup[],
								GROUPSRELATION &sGroupsRelation, 
								STONESRELATION &sStonesRelation )
{
	int nColor, nWhat;
	memset( &sGroupsRelation, 0, sizeof( GROUPSRELATION ) );
	memset( & sStonesRelation, 0, sizeof( STONESRELATION ) );
	sStonesRelation.bNoEat = m_sOriginalStoneRelation.bNoEat;

	for( int i = 0; i < m_cnMaxGroups; i++ )
	{
		nColor = asStoneGroup[i].asStone[0].nColor;
		nWhat = asStoneGroup[i].asStone[0].nWhat;
		if ( sStonesRelation.acnAllColors[nColor] == 0 )
		{
			// 前面的牌里没出现过这种花色
			sStonesRelation.cnAllColors++;
		}
		switch( asStoneGroup[i].nGroupStyle )
		{
		case GROUP_STYLE_SHUN:
			// 顺
			sGroupsRelation.cnShunGroups++;
			sGroupsRelation.acnShunGroups[nColor][nWhat]++;
			sStonesRelation.acnAllColors[nColor] += 3;
			sStonesRelation.acnAllStones[GetValue( nColor, nWhat )]++;
			sStonesRelation.acnAllStones[GetValue( nColor, nWhat + 1 )]++;
			sStonesRelation.acnAllStones[GetValue( nColor, nWhat + 2 )]++;
			break;
		case GROUP_STYLE_KE:
			sGroupsRelation.cnKeGroups++;
			sGroupsRelation.acnKeGroups[nColor][nWhat]++;
			sStonesRelation.acnAllColors[nColor] += 3;
			sStonesRelation.acnAllStones[GetValue( nColor, nWhat )] += 3;
			break;
		case GROUP_STYLE_JONG:
			sStonesRelation.acnAllColors[nColor] += 2;
			sStonesRelation.acnAllStones[GetValue( nColor, nWhat )] += 2;
			break;
		case GROUP_STYLE_MINGGANG:
		case GROUP_STYLE_ANGANG:
			sGroupsRelation.cnKeGroups++;
			sGroupsRelation.acnKeGroups[nColor][nWhat]++;			
			sStonesRelation.acnAllColors[nColor] += 4;
			sStonesRelation.acnAllStones[GetValue( nColor, nWhat )] += 4;
			break;
		case GROUP_STYLE_LONG:
			sStonesRelation.acnAllColors[nColor] += 3;
			sStonesRelation.acnAllStones[GetValue( nColor, nWhat )]++;
			sStonesRelation.acnAllStones[GetValue( nColor, nWhat + 3 )]++;
			sStonesRelation.acnAllStones[GetValue( nColor, nWhat + 6 )]++;
			break;
		default:
			ASSERT( FALSE );
		}
	}
}

// **************************************************************************************
// 
// 是否花牌
// 
// **************************************************************************************
BOOL CPublicJudge::IsFlower( int nStoneID )
{
	if ( ( nStoneID & 0x0F00 ) == COLOR_FLOWER )
	{
		return TRUE;
	}
	
	return FALSE;
}

// **************************************************************************************
// 
// 检验传入的参数合法性
// 
// **************************************************************************************
BOOL CPublicJudge::InvalidParam( const CHECKPARAM &sCheckParam )
{
	if ( sCheckParam.cnHandStone % 3 != 2 )
	{
		// 所有未公开的牌减去将牌牌数应能被3整除否则必然不能和
		return TRUE;
	}
	
	for( int i = 0; i < sCheckParam.cnHandStone; i++ )
	{
		if ( IsFlower( sCheckParam.asHandStone[i].nID ) )
		{
			return TRUE;
		}
	}
	
	return FALSE;
}

// **************************************************************************************
// 
// 是否相同的牌
// 
// **************************************************************************************
BOOL CPublicJudge::IsSameStone( STONE & sStone1, STONE & sStone2 )
{
	return  sStone1.nColor == sStone2.nColor && sStone1.nWhat == sStone2.nWhat;  
}

// **************************************************************************************
// 
// 从一字符串里读取第一个数字，返回值指向下一数字
// 
// **************************************************************************************
char * CPublicJudge::GetPrivateInt( char * pChar , int & nData )
{
	while ( *pChar == ' ' )
	{
		pChar++;
	}
	if ( *pChar )
	{
		nData = atoi( pChar );
	}
	while ( *pChar != ' ' && *pChar )
	{
		pChar++;
	}
	return pChar;
}

// **************************************************************************************
// 
// 从花色点数得到一维数组的索引号
// 
// **************************************************************************************
int CPublicJudge::GetValue( int nColor, int nWhat )
{
	// ASSERT( nColor >= 0 && nColor <= 4 );
	
	if ( nColor < 4 )
	{
		return nColor + ( nColor << 3 ) + nWhat;//nColor * 9 + nWhat;
	}
	else 
	{
		return 31 + nWhat;
	}
}

// **************************************************************************************
// 
// 得到一张牌的一维数组索引号
// 
// **************************************************************************************
int CPublicJudge::GetValue( STONE &sStone )
{
	// ASSERT( sStone.nID != 0 );
	return GetValue( sStone.nColor, sStone.nWhat );
}

// **************************************************************************************
// 
// 获得算番入口
// 
// **************************************************************************************
void CPublicJudge::GetEntry( HURESULT &sHuResult, int &nRow, int &nCol )
{
	if ( sHuResult.nResultant > PENPENHU )
	{
		// 根据和牌方式决定行下标
		nRow = sHuResult.nResultant;
		if ( sHuResult.nResultant == SHISHANYAO )
		{
			nCol = 5;
			return;
		}
	}
	else
	{
		// 根据刻的数量决定行下标
		nRow = m_sGroupsRelation.cnKeGroups;
	}
	if ( m_sStonesRelation.cnAllColors == 1 )
	{
		if ( m_sStonesRelation.acnAllColors[COLOR_WIND] != 0 )
		{
			// 全是风牌,字一色
			nCol = 1;
		}
		else
		{
			// 清一色
			nCol = 0;
		}
	}
	else if ( m_sStonesRelation.cnAllColors == 2 )
	{
		if ( m_sStonesRelation.acnAllColors[COLOR_WIND] != 0 
			&& m_sStonesRelation.acnAllColors[COLOR_JIAN] != 0 ) 
		{
			// 字一色
			nCol = 1;
		}
		else if ( m_sStonesRelation.acnAllColors[COLOR_WIND] != 0 
			|| m_sStonesRelation.acnAllColors[COLOR_JIAN] != 0 ) 
		{
			// 混一色
			nCol = 2;
		}
		else
		{
			// 无字
			nCol = 4;
		}
	}
	else if ( m_sStonesRelation.cnAllColors == 3 )
	{
		if ( m_sStonesRelation.acnAllColors[COLOR_WIND] != 0 
			&& m_sStonesRelation.acnAllColors[COLOR_JIAN] != 0 ) 
		{
			// 混一色
			nCol = 2;
		}
		else if ( m_sStonesRelation.acnAllColors[COLOR_WIND] != 0 
			|| m_sStonesRelation.acnAllColors[COLOR_JIAN] != 0 ) 
		{
			// 有字
			nCol = 3;
		}
		else
		{
			// 无字
			nCol = 4;
		}
		
	}
	else if ( m_sStonesRelation.cnAllColors == 4 )
	{
		// 有字
		nCol = 3;
	}
	else 
	{
		// 五门齐
		nCol = 5;
	}
}

//88番
// **************************************************************************************
// 
//1 大四喜 由4副风刻(杠)组成的和牌。不计圈风刻、门风刻、三风刻、碰碰和、小四喜、幺九刻
// 
// **************************************************************************************
int CPublicJudge::CheckDa4Xi( HURESULT &sHuResult )
{
	for( int i = 0; i < 4; i++ )
	{
		if ( m_sGroupsRelation.acnKeGroups[COLOR_WIND][i] != 1 ) 
		{
			return 0;
		}
	}

	// 屏蔽掉不计的番种
	m_abEnableRule[FAN_QUANFENG] = false;
	m_abEnableRule[FAN_MENGFENG] = false;
	m_abEnableRule[FAN_3FENGKE] = false;
	m_abEnableRule[FAN_PENPENHU] = false;
	m_abEnableRule[FAN_XIAO4XI] = false;
	m_abEnableRule[FAN_19KE] = false;

	return 1;
}

// **************************************************************************************
// 
//2 大三元 和牌中，有中发白3副刻子。不计箭刻、双箭刻、小三元
// 
// **************************************************************************************
int CPublicJudge::CheckDa3Yuan( HURESULT &sHuResult )
{
	for( int i = 0; i < 3; i++ )
	{
		if ( m_sGroupsRelation.acnKeGroups[COLOR_JIAN][i] != 1 ) 
		{
			return 0;
		}
	}

	// 屏蔽掉不计的番种
	m_abEnableRule[FAN_JIANKE] = false;
	m_abEnableRule[FAN_2JIANKE] = false;
	m_abEnableRule[FAN_XIAO3YUAN] = false;

	return 1;
}

// **************************************************************************************
// 
//3 绿一色 由23468条及发字中的任何牌组成的顺子、刻子、将的和牌。不计混一色。如无“发”字
// 组成的各牌，可计清一色
// 在算此番之前必须已确定是清一色或混一色
// 
// **************************************************************************************
int CPublicJudge::CheckLv1She( HURESULT &sHuResult )
{
	int acnMaxStones[34] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, //万
							 0, 8, 8, 8, 0, 8, 0, 8, 0, //条
							 0, 0, 0, 0, 0, 0, 0, 0, 0, //饼
							 0, 0, 0, 0,				// 风
							 0, 8, 0 };					// 箭
	for( int i = 0; i < 34; i++ )
	{
		if ( m_sStonesRelation.acnAllStones[i] > acnMaxStones[i] )
		{
			return 0;
		}
	}

	// 屏蔽掉不计的番种
	m_abEnableRule[FAN_HUN1SHE] = false;

	return 1;
}

// **************************************************************************************
// 
//4 九莲宝灯 由一种花色序数牌子按1112345678999组成的特定牌型，见同花色任何1张序数牌即成和
// 牌。不计清一色、无字
// 在算此番之前必须已确定是清一色
// 不计门前清 缺一门、幺九刻、不求人
// 
// **************************************************************************************
int CPublicJudge::Check9LianBaoDen( HURESULT &sHuResult )
{
	// 去掉最后摸的那张牌后，1和9必须得是3张，其它每样一张
	if ( m_pCheckParam->cnShowGroups > 0 )//|| m_sStonesRelation.cnStoneTypes != 9 )
	{
		// 不能有吃碰杠的牌
		// 九张牌都必须得有
		return 0;
	}

	// 先搜索到最后摸的那张牌的花色和数字
	TILEINFO sWinTileInfo = { 0 };
    sWinTileInfo.nColor = m_pCheckParam->asHandStone[0].nColor;
    sWinTileInfo.nWhat = m_pCheckParam->asHandStone[0].nWhat;

	// 遍历一下，是否满足要求
	int cnCorrectCount = 0;// 正确张数
	for( int i = 0; i < 9; i++ )
	{
		if ( i == 0 || i == 8 )
		{
			cnCorrectCount = 3;
		}
		else
		{
			cnCorrectCount = 1;
		}
		if ( i == sWinTileInfo.nWhat )
		{
			cnCorrectCount++;
		}
		if ( m_sStonesRelation.acnAllStones[sWinTileInfo.nColor * 9 + i] != cnCorrectCount )
		{
			return 0;
		}
	}

	// 屏蔽掉不计的番种
	m_abEnableRule[FAN_QING1SHE] = false;
	m_abEnableRule[FAN_WUZI] = false;
	m_abEnableRule[FAN_BUQIUREN] = false;
	m_abEnableRule[FAN_MENGQING] = false;
	m_abEnableRule[FAN_QUE1MEN] = false;
	m_abEnableRule[FAN_19KE] = false;

	return 1;
}

// **************************************************************************************
// 
//5 四杠 4个杠,不计双暗杠、双明杠、明杠、暗杠、三杠、单钓、碰碰和
// 
// **************************************************************************************
int CPublicJudge::Check4Gang( HURESULT &sHuResult )
{
	if ( m_pCheckParam->cnAnGang + m_pCheckParam->cnMinGang == 4 )
	{
		// 屏蔽掉不计的番种
		m_abEnableRule[FAN_3GANG] = false;
		m_abEnableRule[FAN_2MINGANG] = false;
		m_abEnableRule[FAN_2ANGANG] = false;
		m_abEnableRule[FAN_ANGANG] = false;
		m_abEnableRule[FAN_MINGANG] = false;
		m_abEnableRule[FAN_DANDIAO] = false;
		m_abEnableRule[FAN_PENPENHU] = false;
		
		return 1;
	}

	return 0;
}

// **************************************************************************************
// 
//6 连七对 由一种花色序数牌组成序数相连的7个对子的和牌。不计清一色、不求人、单钓 
// 在算此番之前必须已确定是清一色、七对
// 不计七对、清一色、不求人、单钓、无字、门清 
// 
// **************************************************************************************
int CPublicJudge::CheckLian7Dui( HURESULT &sHuResult )
{
	// 找到有牌的花色
	int i = 0;
	for(i = 0; i < 3; i++ )
	{
		if ( m_sStonesRelation.acnAllColors[i] != 0 )
		{
			break;
		}
	}
	if ( i >= 3 ) 
	{
		return 0;
	}

	// 找到第一对
	int nFirstIndex = i + ( i << 3 );
	for( i = 0; i < STONE_NO4; i++ )
	{
		if ( m_sStonesRelation.acnAllStones[nFirstIndex] == 2 )
		{
			break;
		}
		nFirstIndex++;
	}
	if ( i == STONE_NO4 )
	{
		return 0;
	}

	// 从第一对开始，必须连续有对
	for( i = 1; i < 7; i++ )
	{
		if ( m_sStonesRelation.acnAllStones[nFirstIndex + i] != 2 )
		{
			return 0;
		}
	}

//	for( i = 0; i < STONE_NO4; i++ )
//	{
//		if ( acnStone[i] != 2 )
//		{
//			continue;
//		}
//		for( int j = i; j < i + 7; j++ )
//		{
//			if ( acnStone[j] != 2 )
//			{
//				return 0;
//			}
//		}
//		break;
//	}

	// 屏蔽掉不计的番种
	m_abEnableRule[FAN_QING1SHE] = false;
	m_abEnableRule[FAN_7DUI] = false;
	m_abEnableRule[FAN_BUQIUREN] = false;
	m_abEnableRule[FAN_DANDIAO] = false;
	m_abEnableRule[FAN_MENGQING] = false;

	return 1;
}

// **************************************************************************************
// 
//7 十三幺 由3种序数牌的一、九牌，7种字牌及其中一对作将组成的和牌。不计五门齐、不求人、单钓
//	不计门前清
// 
// **************************************************************************************
int CPublicJudge::Check131( HURESULT &sHuResult )
{
	// 屏蔽掉不计的番种
	m_abEnableRule[FAN_5MENQI] = false;
	m_abEnableRule[FAN_BUQIUREN] = false;
	m_abEnableRule[FAN_DANDIAO] = false;
	m_abEnableRule[FAN_MENGQING] = false;

	return 1;
}

//
//64番
// **************************************************************************************
// 
//8 清幺九 由序数牌一、九刻子组成的和牌。不计碰碰和、两同刻、无字、全带幺、幺九刻 
//  不计混幺九、幺九头
// 
// **************************************************************************************
int CPublicJudge::CheckQing19( HURESULT &sHuResult )
{
//	for( int i = 0; i < m_pCheckParam->cnHandStone; i++ )
//	{
//		if ( m_pCheckParam->asHandStone[i].nColor >= COLOR_WIND 
//			|| m_pCheckParam->asHandStone[i].nWhat != STONE_NO1 
//			&& m_pCheckParam->asHandStone[i].nWhat != STONE_NO9 )
//		{
//			return 0;
//		}
//	}
	int i = 0;
	for(i = 0; i < 27; i++ )
	{
		if ( i % 9 == 0 || i % 9 == 8 )
		{
			continue;
		}
		if ( m_sStonesRelation.acnAllStones[i] != 0 )
		{
			return 0;
		}
	}
	for( i = 27; i < 34; i++ )
	{
		if ( m_sStonesRelation.acnAllStones[i] != 0 )
		{
			return 0;
		}
	}

	// 屏蔽掉不计的番种
	m_abEnableRule[FAN_PENPENHU] = false;
	m_abEnableRule[FAN_2TONGKE] = false;
	m_abEnableRule[FAN_WUZI] = false;
	m_abEnableRule[FAN_HUN19] = false;
	m_abEnableRule[FAN_QUANDAIYAO] = false;
	m_abEnableRule[FAN_19KE] = false;
	m_abEnableRule[FAN_19JONG] = false;

	return 1;
}

// **************************************************************************************
// 
//9 小四喜 和牌时有风牌的3副刻子及将牌。不计三风刻
// 
// **************************************************************************************
int CPublicJudge::CheckXiao4Xi( HURESULT &sHuResult )
{
	int cnWindGroup = 0;
	for( int i = 27; i < 31; i++ )
	{
		if ( m_sStonesRelation.acnAllStones[i] != 0 )
		{
			cnWindGroup++;
		}
	}
	// 最后一组是将牌，将牌必须是风牌并且不能是已成刻的风牌
	if ( cnWindGroup == 4 && sHuResult.asGroup[4].asStone[0].nColor == COLOR_WIND )
	{
		// 屏蔽掉不计的番种
		m_abEnableRule[FAN_3FENGKE] = false;
		return 1;
	}
	
	return 0;

/*
	int cnWindGroup = 0;
	for( int i = 0; i < m_cnMaxGroups - 1; i++ )
	{
		if ( sHuResult.asGroup[i].asStone[0].nColor == COLOR_WIND )
		{
			cnWindGroup++;
		}
	}
	// 最后一组是将牌，将牌必须是风牌并且不能是已成刻的风牌
	if ( cnWindGroup == 3 && sHuResult.asGroup[i].asStone[0].nColor == COLOR_WIND 
		&& m_sGroupsRelation.acnKeGroups[COLOR_WIND][sHuResult.asGroup[i].asStone[0].nWhat] == 0 )
	{
		// 屏蔽掉不计的番种
		m_abEnableRule[FAN_3FENGKE] = false;
		return 1;
	}

	return 0;
*/
}

// **************************************************************************************
// 
//10 小三元 和牌时有箭牌的两副刻子及将牌。不计箭刻、双箭刻
// 
// **************************************************************************************
int CPublicJudge::CheckXiao3Yuan( HURESULT &sHuResult )
{
	int cnJianKe = 0;
	for( int i = 0; i < 3; i++ )
	{
		if ( m_sGroupsRelation.acnKeGroups[COLOR_JIAN][i] != 0 )
		{
			cnJianKe++;
		}
	}
	if ( cnJianKe == 2 )
	{
		// 将牌要是箭牌并且不能是已成刻的箭牌
		if ( sHuResult.asGroup[m_cnMaxGroups - 1].asStone[0].nColor == COLOR_JIAN 
			&& m_sGroupsRelation.acnKeGroups[COLOR_JIAN][sHuResult.asGroup[m_cnMaxGroups - 1].asStone[0].nWhat] == 0 )
		{
			// 屏蔽掉不计的番种
			m_abEnableRule[FAN_JIANKE] = false;
			m_abEnableRule[FAN_2JIANKE] = false;
			return 1;
		}
	}

	return 0;
}

// **************************************************************************************
// 
//11 字一色 由字牌的刻子(杠)、将组成的和牌。
// 不计碰碰和 混幺九 全带幺  幺九刻  缺一门
// 
// **************************************************************************************
int CPublicJudge::CheckZi1She( HURESULT &sHuResult )
{
    if (m_sStonesRelation.cnAllColors == 1 && (m_sStonesRelation.acnAllColors[COLOR_JIAN] != 0))
    {
        // 屏蔽掉不计的番种
        m_abEnableRule[FAN_PENPENHU] = false;
        m_abEnableRule[FAN_HUN19] = false;
        m_abEnableRule[FAN_QUANDAIYAO] = false;
        m_abEnableRule[FAN_19KE] = false;
        m_abEnableRule[FAN_QUE1MEN] = false;
        return 1;
    }
    return 0;
}

// **************************************************************************************
// 
//12 四暗刻 4个暗刻(暗杠)。不计门前清、碰碰和、三暗刻、双暗刻、不求人
// 如果不是自摸和牌，和的那一刻不能算暗刻
// 不是自摸的不计单钓将
// 
// **************************************************************************************
int CPublicJudge::Check4AnKe( HURESULT &sHuResult )
{
	// 手中刻的数量
	int cnAnKe = 0;
	if ( m_sGroupsRelation.cnKeGroups == 4 
		&& m_pCheckParam->cnAnGang == m_pCheckParam->cnShowGroups )
	{
		// 只有暗杠，没有吃碰明杠
		if ( ( m_pCheckParam->nWinMode & WIN_MODE_ZIMO ) == 0 )
		{
			// 如果不是自摸和牌，和的那一刻不能算暗刻,因此，点炮和牌时只有单钓才能算四暗刻
			ASSERT( sHuResult.asGroup[m_cnMaxGroups - 1].nGroupStyle == GROUP_STYLE_JONG );
//			if ( !IsSameStone( m_pCheckParam->asHandStone[0], 
//				sHuResult.asGroup[m_cnMaxGroups - 1].asStone[0] ) )
			if ( m_pCheckParam->asHandStone[0].nID != sHuResult.asGroup[4].asStone[0].nID 
				&& m_pCheckParam->asHandStone[0].nID != sHuResult.asGroup[4].asStone[1].nID )
			{
				return 0;
			}
		}

		// 屏蔽掉不计的番种
		m_abEnableRule[FAN_MENGQING] = false;
		m_abEnableRule[FAN_PENPENHU] = false;
		m_abEnableRule[FAN_BUQIUREN] = false;
		m_abEnableRule[FAN_3ANKE] = false;
		m_abEnableRule[FAN_2ANKE] = false;
		if ( ( m_pCheckParam->nWinMode & WIN_MODE_ZIMO ) == 0 )
		{
			// 不是自摸的不计单钓将
			m_abEnableRule[FAN_DANDIAO] = false;
		}
		return 1;
	}

	return 0;
}

// **************************************************************************************
// 
//13 一色双龙会 一种花色的两个老少副，5为将牌。不计平和、七对、清一色
// 必然是七对、清一色
// 不计无字、缺一门、一般高，老少副 
// 
// **************************************************************************************
int CPublicJudge::Check1She2Long( HURESULT &sHuResult )
{
	if ( m_sStonesRelation.cnAllColors != 1 )//|| sHuResult.nResultant != QIDUI)
	{
		// 必须是清一色
		return 0;
	}

	// 找到这个花色
	int i = 0;
	for(i = 0; i < 3; i++ )
	{
		if ( m_sStonesRelation.acnAllColors[i] != 0 )
		{
			break;
		}
	}
	ASSERT( i != 3 );

	// 这个花色里，除4和6外，每样都要有两张
	i += ( i << 3 );
	for( int j = 0; j < 9; j++ )
	{
		if ( j == STONE_NO4 || j == STONE_NO6 ) 
		{
			if ( m_sStonesRelation.acnAllStones[i + j] != 0 )
			{
				return 0;
			}
		}
		else if ( m_sStonesRelation.acnAllStones[i + j] != 2 )
		{
			return 0;
		}
	}

//	for( int i = 0; i < 14; i++ )
//	{
//		if ( m_pCheckParam->asHandStone[i].nWhat == STONE_NO4
//			|| m_pCheckParam->asHandStone[i].nWhat == STONE_NO6 ) 
//		{
//			return 0;
//		}
//	}
	
	// 屏蔽掉不计的番种
	m_abEnableRule[FAN_7DUI] = false;
	m_abEnableRule[FAN_QING1SHE] = false;
	m_abEnableRule[FAN_WUZI] = false;
	m_abEnableRule[FAN_QUE1MEN] = false;
	m_abEnableRule[FAN_PINHU] = false;
	m_abEnableRule[FAN_YIBANGAO] = false;
	m_abEnableRule[FAN_LAOSHAOFU] = false;
	
	return 1;
}

//
//48番
// **************************************************************************************
// 
//14 一色四同顺 一种花色4副序数相同的顺子，不计一色三节高、一般高、四归一
// 
// **************************************************************************************
int CPublicJudge::Check1She4TongShun( HURESULT &sHuResult )
{
	if ( m_sGroupsRelation.cnShunGroups != 4 )
	{
		return 0;
	}

	ASSERT( sHuResult.asGroup[0].nGroupStyle == GROUP_STYLE_SHUN );

	int nColor = sHuResult.asGroup[0].asStone[0].nColor;
	int nWhat = sHuResult.asGroup[0].asStone[0].nWhat;
	if ( m_sGroupsRelation.acnShunGroups[nColor][nWhat] != 4 )
	{
		return 0;
	}

	// 屏蔽掉不计的番种
	m_abEnableRule[FAN_1SHE3JIEGAO] = false;
	m_abEnableRule[FAN_YIBANGAO] = false;
	m_abEnableRule[FAN_4GUI1] = false;
	return 1;
}

// **************************************************************************************
// 
//15 一色四节高 一种花色4副依次递增一位数的刻子
// 不计一色三同顺、一色三节高、碰碰和
// 不计混四节
// 
// **************************************************************************************
int CPublicJudge::Check1She4JieGao( HURESULT &sHuResult )
{
	if ( m_sGroupsRelation.cnKeGroups != 4 )
	{
		return 0;
	}

	ASSERT( sHuResult.asGroup[0].nGroupStyle == GROUP_STYLE_KE 
		|| sHuResult.asGroup[0].nGroupStyle == GROUP_STYLE_MINGGANG 
		|| sHuResult.asGroup[0].nGroupStyle == GROUP_STYLE_ANGANG );

	int nColor = sHuResult.asGroup[0].asStone[0].nColor;
	if ( nColor > COLOR_BING )
	{
		// 不是序数牌
		return 0;
	}
	int i = 0;
	for(i = 0; i < 9; i++ )
	{
		if ( m_sGroupsRelation.acnKeGroups[nColor][i] != 0 )
		{
			break;
		}
	}
	ASSERT( i != 9 );
	if( i > 5 )
	{ // 最小的那个刻子，最多是6
		return 0 ;
	}
	for( int j = i; j < i + 4; j++ )
	{
		if ( m_sGroupsRelation.acnKeGroups[nColor][j] == 0 ) 
		{
			return 0;
		}
	}

//	BOOL bFindFirst = 0;
//	for( int i = 0; i < 9; i++ )
//	{
//		if ( m_sGroupsRelation.acnKeGroups[nColor][i] == 0 )
//		{
//			if ( bFindFirst )
//			{
//				return 0;
//			}
//		}
//		else if ( !bFindFirst )
//		{
//			bFindFirst = TRUE;
//		}
//	}

	// 屏蔽掉不计的番种
	m_abEnableRule[FAN_1SHE3JIEGAO] = false;
	m_abEnableRule[FAN_1SHE3TONGSHUN] = false;
	m_abEnableRule[FAN_PENPENHU] = false;
	return 1;
}

//
//32番
// **************************************************************************************
// 
//16 一色四步高 一种花色4副依次递增一位数或依次递增二位数的顺子 
// 不计一色三步高，连六
// 
// **************************************************************************************
int CPublicJudge::Check1She4BuGao( HURESULT &sHuResult )
{
	if ( m_sGroupsRelation.cnShunGroups != 4 )
	{
		return 0;
	}

	ASSERT( sHuResult.asGroup[0].nGroupStyle == GROUP_STYLE_SHUN );

	int nColor = sHuResult.asGroup[0].asStone[0].nColor;
	int i = 0;
	for(i = 0; i < STONE_NO5; i++ )
	{
		// 第一个顺最大只能是456
		if ( m_sGroupsRelation.acnShunGroups[nColor][i] != 0 )
		{
			break;
		}
	}
	if ( i == STONE_NO5 )
	{
		return 0;
	}
	if ( m_sGroupsRelation.acnShunGroups[nColor][i + 1] != 1
		|| m_sGroupsRelation.acnShunGroups[nColor][i + 2] != 1
		|| m_sGroupsRelation.acnShunGroups[nColor][i + 3] != 1 )
	{
		// 递增1没有符合要求的顺子
		if ( i == 0 )
		{
			// 如果第一顺是123，那么看看递增2行不行
			if ( m_sGroupsRelation.acnShunGroups[nColor][i + 2] != 1
				|| m_sGroupsRelation.acnShunGroups[nColor][i + 4] != 1
				|| m_sGroupsRelation.acnShunGroups[nColor][i + 6] != 1 )
			{
				return 0;
			}
		}
		else
		{
			// 没指望了
			return 0;
		}
	}

	// 屏蔽掉不计的番种
	m_abEnableRule[FAN_1SHE3BUGAO] = false;
	m_abEnableRule[FAN_LIAN6] = false;
	return 1;
}

// **************************************************************************************
// 
//17 三杠 3个杠
// 不计双明杠、双暗杠、明杠、暗杠
// 
// **************************************************************************************
int CPublicJudge::Check3Gang( HURESULT &sHuResult )
{
	if ( m_pCheckParam->cnMinGang + m_pCheckParam->cnAnGang == 3 )
	{
		// 屏蔽掉不计的番种
		m_abEnableRule[FAN_2MINGANG] = false;
		m_abEnableRule[FAN_2ANGANG] = false;
		m_abEnableRule[FAN_MINGANG] = false;
		m_abEnableRule[FAN_ANGANG] = false;
		return 1;
	}
	return 0;
}

// **************************************************************************************
// 
//18 混幺九 由字牌和序数牌一、九的刻子用将牌组成的各牌。不计碰碰和、幺九刻、全带幺 
// 
// **************************************************************************************
int CPublicJudge::CheckHun19( HURESULT &sHuResult )
{
//	for( int i = 0; i < m_cnMaxHandStone; i++ )
//	{
//		if ( m_pCheckParam->asHandStone[i].nColor < COLOR_WIND )
//		{
//			// 序数牌，必须全是幺九
//			if ( m_pCheckParam->asHandStone[i].nWhat != STONE_NO1 
//				&& m_pCheckParam->asHandStone[i].nWhat != STONE_NO9 )
//			{
//				return 0;
//			}
//		}
//	}
	for( int i = 0; i < 27; i++ )
	{
		if ( i % 9 == 0 || i % 9 == 8 )
		{
			continue;
		}
		if ( m_sStonesRelation.acnAllStones[i] != 0 )
		{
			return 0;
		}
	}

	// 屏蔽掉不计的番种
	m_abEnableRule[FAN_PENPENHU] = false;
	m_abEnableRule[FAN_19KE] = false;
	m_abEnableRule[FAN_QUANDAIYAO] = false;
	return 1;
}

//
//24番
// **************************************************************************************
// 
//19 七对 由7个对子组成和牌。不计不求人、单钓、门清
// 
// **************************************************************************************
int CPublicJudge::Check7Dui( HURESULT &sHuResult )
{
	// 屏蔽掉不计的番种
	m_abEnableRule[FAN_MENGQING] = false;
	m_abEnableRule[FAN_BUQIUREN] = false;
	m_abEnableRule[FAN_DANDIAO] = false;
	return 1;
}

// **************************************************************************************
// 
//20 七星不靠 必须有7个单张的东西南北中发白，加上3种花色，数位按147、258、369中的7张序数
// 牌组成没有将牌的和牌。不计五门齐、不求人、单钓、门清、全不靠
// 
// **************************************************************************************
int CPublicJudge::Check7XinBuKao( HURESULT &sHuResult )
{
	if ( sHuResult.nResultant != QUANBUKAO )
	{
		return 0;
	}

	int cnWind = 0;
	for( int i = 0; i < m_cnMaxHandStone; i++ )
	{
		if ( sHuResult.asHandStone[i].nColor >= COLOR_WIND )
		{
			cnWind++;
		}
	}
	if ( cnWind != 7 )
	{
		return 0;
	}
	
	// 屏蔽掉不计的番种
	m_abEnableRule[FAN_MENGQING] = false;
	m_abEnableRule[FAN_BUQIUREN] = false;
	m_abEnableRule[FAN_DANDIAO] = false;
	m_abEnableRule[FAN_5MENQI] = false;
	m_abEnableRule[FAN_QUANBUKAO] = false;
	return 1;
}

// **************************************************************************************
// 
//21 全双刻 由2、4、6、8序数牌的刻子、将牌组成的和牌。不计碰碰和、断幺、无字
// 
// **************************************************************************************
int CPublicJudge::CheckQuanShuangKe( HURESULT &sHuResult )
{
	if ( m_sGroupsRelation.cnKeGroups != 4 )
	{
		return 0;
	}

	for( int i = 0; i < m_cnMaxGroups; i++ )
	{
		if ( ( sHuResult.asGroup[i].asStone[0].nWhat + 1 ) % 2 != 0 )
		{
			return 0;
		}
	}

	// 屏蔽掉不计的番种
	m_abEnableRule[FAN_PENPENHU] = false;
	m_abEnableRule[FAN_DUAN19] = false;
	m_abEnableRule[FAN_WUZI] = false;
	return 1;
}

// **************************************************************************************
// 
//22 清一色 由一种花色的序数牌组成和牌。不计无字、缺一门
// 
// **************************************************************************************
int CPublicJudge::CheckQing1She(HURESULT &sHuResult)
{
    if (m_sStonesRelation.cnAllColors == 1 && (m_sStonesRelation.acnAllColors[COLOR_JIAN] == 0))
    {
        // 屏蔽掉不计的番种
        m_abEnableRule[FAN_QUE1MEN] = false;
        m_abEnableRule[FAN_WUZI] = false;
        return 1;
    }
    return 0;
}

// **************************************************************************************
// 
//23 一色三同顺 和牌时有一种花色3副序数相同的顺子。不计一色三节高
// 
// **************************************************************************************
int CPublicJudge::Check1She3TongShun( HURESULT &sHuResult )
{
	int i = 0;
	for(i = 0; i < 3; i++ )
	{
		if ( m_sStonesRelation.acnAllColors[i] >= 9 )
		{
			break;
		}
	}
	if ( i == 3 )
	{
		// 没有一门有9张牌
		return 0;
	}
	for( int j = 0; j < 7; j++ )
	{
		if ( m_sGroupsRelation.acnShunGroups[i][j] == 3 )
		{
			return 1;
		}
	}
	return 0;
}

// **************************************************************************************
// 
//24 一色三节高 和牌时有一种花色3副依次递增一位数字的刻子。不计一色三同顺
// 
// **************************************************************************************
int CPublicJudge::Check1She3JieGao( HURESULT &sHuResult )
{
	int i = 0;
	for(i = 0; i < 3; i++ )
	{
		if ( m_sStonesRelation.acnAllColors[i] >= 9 )
		{
			break;
		}
	}
	if ( i == 3 )
	{
		// 没有一门有9张牌
		return 0;
	}
	for( int j = 0; j < STONE_NO8; j++ )
	{
		if ( m_sGroupsRelation.acnKeGroups[i][j] != 0
			&& m_sGroupsRelation.acnKeGroups[i][j + 1] != 0
			&& m_sGroupsRelation.acnKeGroups[i][j + 2] != 0 )
		{
			return 1;
		}
	}
	return 0;
}

// **************************************************************************************
// 
//25 全大 由序数牌789组成的顺子、刻子(杠)、将牌的和牌。不计无字、大于5
// 
// **************************************************************************************
int CPublicJudge::CheckQuanDa( HURESULT &sHuResult )
{
	int i = 0;
	for(i = 0; i < 3; i++ )
	{
		for( int j = 0; j < STONE_NO7; j++ )
		{
			if ( m_sStonesRelation.acnAllStones[i * 9 + j] != 0 )
			{
				return 0;
			}
		}
	}
	for( i = 27; i < 34; i++ )
	{
		// 不能有字
		if ( m_sStonesRelation.acnAllStones[i] != 0 )
		{
			return 0;
		}
	}

	// 屏蔽掉不计的番种
	m_abEnableRule[FAN_DAYU5] = false;
	m_abEnableRule[FAN_WUZI] = false;
	return 1;
}

// **************************************************************************************
// 
//26 全中 由序数牌456组成的顺子、刻子(杠)、将牌的和牌。不计断幺、无字
// 
// **************************************************************************************
int CPublicJudge::CheckQuanZhong( HURESULT &sHuResult )
{
	int i = 0;
	for(i = 0; i < 3; i++ )
	{
		for( int j = 0; j < 9; j++ )
		{
			if ( j == STONE_NO4 || j == STONE_NO5 || j == STONE_NO6 )
			{
				continue;
			}
			if ( m_sStonesRelation.acnAllStones[i * 9 + j] != 0 )
			{
				return 0;
			}
		}
	}
	for( i = 27; i < 34; i++ )
	{
		// 不能有字
		if ( m_sStonesRelation.acnAllStones[i] != 0 )
		{
			return 0;
		}
	}

	// 屏蔽掉不计的番种
	m_abEnableRule[FAN_DUAN19] = false;
	m_abEnableRule[FAN_WUZI] = false;
	return 1;
}

// **************************************************************************************
// 
//27 全小 由序数牌123组成的顺子、刻子(杠)将牌的的和牌。不计无字、小于5
// 
// **************************************************************************************
int CPublicJudge::CheckQuanXiao( HURESULT &sHuResult )
{
	int i = 0;
	for(i = 0; i < 3; i++ )
	{
		for( int j = STONE_NO4; j <= STONE_NO9; j++ )
		{
			if ( m_sStonesRelation.acnAllStones[i * 9 + j] != 0 )
			{
				return 0;
			}
		}
	}
	for( i = 27; i < 34; i++ )
	{
		// 不能有字
		if ( m_sStonesRelation.acnAllStones[i] != 0 )
		{
			return 0;
		}
	}

	// 屏蔽掉不计的番种
	m_abEnableRule[FAN_XIAOYU5] = false;
	m_abEnableRule[FAN_WUZI] = false;
	return 1;
}

//
//16番
// **************************************************************************************
// 
//28 清龙 和牌时，有一种花色1-9相连接的序数牌
// 不计六连张.老少配
// 不计混龙
// 
// **************************************************************************************
int CPublicJudge::CheckQingLong( HURESULT &sHuResult )
{
	int i = 0;
	for(i = 0; i < 3; i++ )
	{
		if ( m_sStonesRelation.acnAllColors[i] >= 9 )
		{
			break;
		}
	}
	if ( i == 3 )
	{
		// 没有一门有9张牌
		return 0;
	}
	if ( m_sGroupsRelation.acnShunGroups[i][0] >= 1
		&& m_sGroupsRelation.acnShunGroups[i][3] >= 1
		&& m_sGroupsRelation.acnShunGroups[i][6] >= 1 )
	{		
		// 屏蔽掉不计的番种
		m_abEnableRule[FAN_LIAN6] = false;
		m_abEnableRule[FAN_LAOSHAOFU] = false;
		m_abEnableRule[FAN_HUNLONG] = false;
		return 1;
	}

	return 0;
}

// **************************************************************************************
// 
//29 三色双龙会 2种花色2个老少副、另一种花色5作将的和牌。不计喜相逢、老少副、无字、平和
// 
// **************************************************************************************
int CPublicJudge::Check3She2Long( HURESULT &sHuResult )
{
	ASSERT( sHuResult.asGroup[m_cnMaxGroups - 1].nGroupStyle == GROUP_STYLE_JONG );

	// 将牌必须是5
	int nJongColor = sHuResult.asGroup[m_cnMaxGroups - 1].asStone[0].nColor;
	if ( m_sGroupsRelation.cnShunGroups != 4
		|| sHuResult.asGroup[m_cnMaxGroups - 1].asStone[0].nWhat != STONE_NO5
		|| m_sStonesRelation.acnAllColors[nJongColor] != 2 ) 
	{
		return 0;
	}

	for( int i = 0; i < 3; i++ )
	{
		if ( i == nJongColor )
		{
			continue;
		}

		if ( m_sGroupsRelation.acnShunGroups[i][0] != 1
			|| m_sGroupsRelation.acnShunGroups[i][6] != 1 )
		{
			return 0;
		}
	}

	// 屏蔽掉不计的番种
	m_abEnableRule[FAN_XIXIANGFENG] = false;
	m_abEnableRule[FAN_LAOSHAOFU] = false;
	m_abEnableRule[FAN_WUZI] = false;
	m_abEnableRule[FAN_PINHU] = false;
	return 1;
}

// **************************************************************************************
// 
//30 一色三步高 和牌时，有一种花色3副依次递增一位或依次递增二位数字的顺子
// 
// **************************************************************************************
int CPublicJudge::Check1She3BuGao( HURESULT &sHuResult )
{
	int i = 0;
	for(i = 0; i < 3; i++ )
	{
		if ( m_sStonesRelation.acnAllColors[i] >= 9 )
		{
			break;
		}
	}
	if ( i == 3 )
	{
		// 没有一门有9张牌
		return 0;
	}

	int j = 0;
	for(j = 0; j < STONE_NO6; j++ )
	{
		// 第一个顺最大只能是567
		if ( m_sGroupsRelation.acnShunGroups[i][j] == 0 )
		{
			continue;
		}

		if ( m_sGroupsRelation.acnShunGroups[i][j + 1] != 0 
			&& m_sGroupsRelation.acnShunGroups[i][j + 2] != 0 )
		{
			// 步进一成功
			break;
		}

		if ( j < STONE_NO4 )
		{
			// 如果第一顺小于456看看步进2有没有满足要求的顺
			if ( m_sGroupsRelation.acnShunGroups[i][j + 2] != 0 
				&& m_sGroupsRelation.acnShunGroups[i][j + 4] != 0 )
			{
				break;
			}
		}	
	}
	if ( j == STONE_NO6 )
	{
		return 0;
	}
	
	return 1;
}

// **************************************************************************************
// 
//31 全带五 每副牌及将牌必须有5的序数牌。不计断幺,无字
// 
// **************************************************************************************
int CPublicJudge::CheckQuanDai5( HURESULT &sHuResult )
{
	for( int i = 0; i < m_cnMaxGroups; i++ )
	{
		int j = 0;
		for(j = 0; j < 4; j++ )
		{
			if ( sHuResult.asGroup[i].asStone[j].nWhat == STONE_NO5 )
			{
				// 这一组里有个5，其它牌不用看了
				break;
			}
		}
		if ( j == 4 )
		{
			return 0;
		}
	}

	// 屏蔽掉不计的番种
	m_abEnableRule[FAN_DUAN19] = false;
	m_abEnableRule[FAN_WUZI] = false;
	return 1;
}

// **************************************************************************************
// 
//32 三同刻 3个序数相同的刻子(杠)
// 不计双同刻
// 
// **************************************************************************************
int CPublicJudge::Check3TongKe( HURESULT &sHuResult )
{
	for( int i = 0; i <= STONE_NO9; i++ )
	{
		if ( m_sGroupsRelation.acnKeGroups[COLOR_WAN][i] != 0
			&& m_sGroupsRelation.acnKeGroups[COLOR_BING][i] != 0 
			&& m_sGroupsRelation.acnKeGroups[COLOR_TIAO][i] != 0 ) 
		{
			// 屏蔽掉不计的番种
			m_abEnableRule[FAN_2TONGKE] = false;
			return 1;
		}
	}
	return 0;
}

// **************************************************************************************
// 
//33 三暗刻 3个暗刻 
// 不计双暗刻
// 
// **************************************************************************************
int CPublicJudge::Check3AnKe( HURESULT &sHuResult )
{
	// 暗刻数 
	int cnAnKe = 0;
	for( int i = m_pCheckParam->cnShowGroups; i < m_cnMaxGroups; i++ )
	{
		if ( sHuResult.asGroup[i].nGroupStyle == GROUP_STYLE_KE )
		{
			// 如果不是自摸和牌，成和的那一刻不能算暗刻
			if ( ( m_pCheckParam->nWinMode & WIN_MODE_ZIMO ) == 0 
				&& ( sHuResult.asGroup[i].asStone[0].nID == m_pCheckParam->asHandStone[0].nID 
				|| sHuResult.asGroup[i].asStone[1].nID == m_pCheckParam->asHandStone[0].nID
				|| sHuResult.asGroup[i].asStone[2].nID == m_pCheckParam->asHandStone[0].nID	) )
			{
				continue;
			}
			cnAnKe++;
		}
	}
	 
	if ( cnAnKe + m_pCheckParam->cnAnGang >= 3 )
	{
		// 屏蔽掉不计的番种
		m_abEnableRule[FAN_2ANKE] = false;
		return 1;
	}

	return 0;
}

//
//12番
// **************************************************************************************
// 
//34 全不靠 由单张3种花色147、258、369不能错位的序数牌及东南西北中发白中的任何14张牌组成
// 的和牌。不计五门齐、不求人、单钓、门清
// 
// **************************************************************************************
int CPublicJudge::CheckQuanBuKao( HURESULT &sHuResult )
{
	// 屏蔽掉不计的番种
	m_abEnableRule[FAN_5MENQI] = false;
	m_abEnableRule[FAN_BUQIUREN] = false;
	m_abEnableRule[FAN_DANDIAO] = false;
	m_abEnableRule[FAN_MENGQING] = false;
	return 1;
}

// **************************************************************************************
// 
//35 组合龙 3种花色的147、258、369不能错位的序数牌
// 
// **************************************************************************************
int CPublicJudge::CheckZhuHeLong( HURESULT &sHuResult )
{
	if ( sHuResult.nResultant == QUANBUKAO )
	{
		// 全不靠和牌，字牌数最多只能有5张
		int cnWind = 0;
		for( int i = 0; i < m_cnMaxHandStone; i++ )
		{
			if ( sHuResult.asHandStone[i].nColor >= COLOR_WIND )
//				if ( m_pCheckParam->asHandStone[i].nColor >= COLOR_WIND )
			{
				cnWind++;
			}
		}
		if( cnWind > 5 )
		{
			return 0;
		}
	}
	return 1;
}

// **************************************************************************************
// 
//36 大于五 由序数牌6-9的顺子、刻子、将牌组成的和牌。不计无字
// 
// **************************************************************************************
int CPublicJudge::CheckDaYu5( HURESULT &sHuResult )
{
	int i = 0;
	for(i = 0; i < 3; i++ )
	{
		for( int j = 0; j < STONE_NO6; j++ )
		{
			if ( m_sStonesRelation.acnAllStones[i * 9 + j] != 0 )
			{
				return 0;
			}
		}
	}
	for( i = 27; i < 34; i++ )
	{
		// 不能有字
		if ( m_sStonesRelation.acnAllStones[i] != 0 )
		{
			return 0;
		}
	}

	// 屏蔽掉不计的番种
	m_abEnableRule[FAN_WUZI] = false;
	return 1;
}

// **************************************************************************************
// 
//37 小于五 由序数牌1-4的顺子、刻子、将牌组成的和牌。不计无字
// 
// **************************************************************************************
int CPublicJudge::CheckXiaoYu5( HURESULT &sHuResult )
{
	int i = 0;
	for(i = 0; i < 3; i++ )
	{
		for( int j = STONE_NO5; j <= STONE_NO9; j++ )
		{
			if ( m_sStonesRelation.acnAllStones[i * 9 + j] != 0 )
			{
				return 0;
			}
		}
	}
	for( i = 27; i < 34; i++ )
	{
		// 不能有字
		if ( m_sStonesRelation.acnAllStones[i] != 0 )
		{
			return 0;
		}
	}

	// 屏蔽掉不计的番种
	m_abEnableRule[FAN_XIAOYU5] = false;
	m_abEnableRule[FAN_WUZI] = false;
	return 1;
}

// **************************************************************************************
// 
//38 三风刻 3个风刻 不计缺一门
// 
// **************************************************************************************
int CPublicJudge::Check3FengKe( HURESULT &sHuResult )
{
	int cnWind = 0;
	for( int i = 0; i < m_cnMaxGroups - 1; i++ )
	{// 最后一组是将牌
		if ( sHuResult.asGroup[i].asStone[0].nColor == COLOR_WIND )
		{
			cnWind++;
		}
	}

	if ( cnWind == 3 )
	{
		return 1;
	}

	return 0;
}

//
//8 番
// **************************************************************************************
// 
//39 花龙 3种花色的3副顺子连接成1-9的序数牌
// 不计混龙
// 
// **************************************************************************************
int CPublicJudge::CheckHuaLong( HURESULT &sHuResult )
{
	for( int i = 0; i < 3; i++ )
	{
		if ( m_sGroupsRelation.acnShunGroups[i][STONE_NO1] != 0 )
		{
			// 本花色有123，看看其它两个花色有没有456和789
			int nColor1 = ( i + 1 ) % 3;
			int nColor2 = ( i + 2 ) % 3;
			if ( m_sGroupsRelation.acnShunGroups[nColor1][STONE_NO4] != 0
				&& m_sGroupsRelation.acnShunGroups[nColor2][STONE_NO7] != 0 
				|| m_sGroupsRelation.acnShunGroups[nColor2][STONE_NO4] != 0
				&& m_sGroupsRelation.acnShunGroups[nColor1][STONE_NO7] != 0 )
			{
				m_abEnableRule[FAN_HUNLONG] = false;
				return 1;
			}
		}
	}
	
	return 0;
}

// **************************************************************************************
// 
//40 推不倒 由牌面图形没有上下区别的牌组成的和牌，包括1234589饼、245689条、白板。
// 不计缺一门
// 
// **************************************************************************************
int CPublicJudge::CheckTuiBuDao( HURESULT &sHuResult )
{
	int acnMaxStones[34] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, //万
							 0, 8, 0, 8, 8, 8, 0, 8, 8, //条
							 8, 8, 8, 8, 8, 0, 0, 8, 8, //饼
							 0, 0, 0, 0,				// 风
							 0, 0, 4 };					// 箭
	for( int i = 0; i < 34; i++ )
	{
		if ( m_sStonesRelation.acnAllStones[i] > acnMaxStones[i] )
		{
			return 0;
		}
	}

	// 屏蔽掉不计的番种
	m_abEnableRule[FAN_QUE1MEN] = false;
	return 1;
}

// **************************************************************************************
// 
//41 三色三同顺 和牌时，有3种花色3副序数相同的顺子
// 
// **************************************************************************************
int CPublicJudge::Check3She3TongShun( HURESULT &sHuResult )
{
	for( int i = 0; i < 7; i++ )
	{
		if ( m_sGroupsRelation.acnShunGroups[COLOR_WAN][i] != 0
			&& m_sGroupsRelation.acnShunGroups[COLOR_TIAO][i] != 0 
			&& m_sGroupsRelation.acnShunGroups[COLOR_BING][i] != 0 )
		{
			return 1;
		}
	}

	return 0;
}

// **************************************************************************************
// 
//42 三色三节高 和牌时，有3种花色3副依次递增一位数的刻子
// 不计混三节
// 
// **************************************************************************************
int CPublicJudge::Check3She3JieGao( HURESULT &sHuResult )
{
	// 找出只有一个刻子的花色
	int i = 0;
	for( i = 0; i < 3; i++ )
	{
	if ( m_sStonesRelation.acnAllColors[i] == 3 || m_sStonesRelation.acnAllColors[i] == 4 )
		{
			// 要找的就是它
			break;
		}
		else if ( m_sStonesRelation.acnAllColors[i] == 0 )
		{
			// 居然缺门
			return 0;
		}
	}
	if( i == 3 )
	{
		// 没有哪个花色只有3张，肯定是没有三色三节高的
		return 0;
	}

	// 另外两个花色
	int nColor1 = ( i + 1 ) % 3;
	int nColor2 = ( i + 2 ) % 3;
	
	// 找出这个刻子
	int j = 0;
	for(j = 0; j <= STONE_NO9; j++ )
	{
		if ( m_sGroupsRelation.acnKeGroups[i][j] != 0 )
		{
			break;
		}
	}
	if ( j > STONE_NO9 )
	{
		// 没刻子
		return 0;
	}

	// 共6种组合方式(数字表示刚找到的那个刻)
	// 1二三
	// 1三二
	// 一2三
	// 三2一
	// 一二3
	// 二一3
	int anIndex[3][2];
	anIndex[0][0] = j + 2 <= STONE_NO9 ? j + 1 : 0xff;
	anIndex[0][1] = j + 2 <= STONE_NO9 ? j + 2 : 0xff;
	anIndex[1][0] = ( j - 1 >= STONE_NO1 && j + 1 <= STONE_NO9 ) ? j - 1 : 0xff;
	anIndex[1][1] = ( j - 1 >= STONE_NO1 && j + 1 <= STONE_NO9 ) ? j + 1 : 0xff;
	anIndex[2][0] = j - 2 >= STONE_NO1 ? j - 2 : 0xff;
	anIndex[2][1] = j - 2 >= STONE_NO1 ? j - 1 : 0xff;

	for( i = 0; i < 3; i++ )
	{
		if ( anIndex[i][0] != 0xff )
		{
			if ( m_sGroupsRelation.acnKeGroups[ nColor1 ][ anIndex[i][0] ] != 0
					&& m_sGroupsRelation.acnKeGroups[ nColor2 ][ anIndex[i][1] ] != 0 
				|| m_sGroupsRelation.acnKeGroups[ nColor2 ][ anIndex[i][0] ] != 0
					&& m_sGroupsRelation.acnKeGroups[ nColor1 ][ anIndex[i][1] ] != 0 )
			{	
				m_abEnableRule[FAN_HUN3JIE] = false;
				return 1;
			}
		}
	}

	return 0;
}

// **************************************************************************************
// 
//43 无番和 和牌后，数不出任何番种分(花牌不计算在内)
// 
// **************************************************************************************
int CPublicJudge::CheckWuFan( HURESULT &sHuResult )
{
	return 0;
}

// **************************************************************************************
// 
//44 妙手回春 自摸牌墙上最后一张牌和牌。不计自摸
// 
// **************************************************************************************
int CPublicJudge::CheckMiaoShou( HURESULT &sHuResult )
{
	return 0;
}

// **************************************************************************************
// 
//45 海底捞月 和打出的最后一张牌
// 
// **************************************************************************************
int CPublicJudge::CheckHaiDi( HURESULT &sHuResult )
{
	return 0;
}

// **************************************************************************************
// 
//46 杠上开花 开杠抓进的牌成和牌(不包括补花)不计自摸
// 
// **************************************************************************************
int CPublicJudge::CheckGangHu( HURESULT &sHuResult )
{
	if ( m_pCheckParam->nWinMode & WIN_MODE_GANGSHANGHU ) 
	{
		return 1;
	}
	return 0;
}

// **************************************************************************************
// 
//47 抢杠和 和别人自抓开明杠的牌。不计和绝张
// 
// **************************************************************************************
int CPublicJudge::CheckQiangGang( HURESULT &sHuResult )
{
	if ( m_pCheckParam->nWinMode & WIN_MODE_QIANGGANG ) 
	{
		// 屏蔽掉不计的番种
		m_abEnableRule[FAN_HUJUEZHANG] = false;
		return 1;
	}
	
	return 0;
}

//
//6 番
// **************************************************************************************
// 
//48 碰碰和 由4副刻子(或杠)、将牌组成的和牌
// 
// **************************************************************************************
int CPublicJudge::CheckPenPenHu( HURESULT &sHuResult )
{
    //有顺子为平胡
    for (int i = 0; i < sHuResult.cnGroups; i++)
    {
        if (sHuResult.asGroup[i].nGroupStyle == GROUP_STYLE_SHUN)
        {
            return 0;
        }
    }
    return 1;
}

// **************************************************************************************
// 
//51 五门齐 和牌时3种序数牌、风、箭牌齐全
// 
// **************************************************************************************
int CPublicJudge::Check5MenQi( HURESULT &sHuResult )
{
	return 1;
}

// **************************************************************************************
// 
//52 全求人 全靠吃牌、碰牌、单钓别人批出的牌和牌。不计单钓 
// 
// **************************************************************************************
int CPublicJudge::CheckQuanQiuRen( HURESULT &sHuResult )
{
	if ( m_pCheckParam->cnShowGroups == m_cnMaxGroups - 1 
		&& m_pCheckParam->cnAnGang == 0 
		&& ( m_pCheckParam->nWinMode & WIN_MODE_ZIMO ) == 0 )
	{
		// 屏蔽掉不计的番种
		m_abEnableRule[FAN_DANDIAO] = false;
		return 1;
	}

	return 0;
}

// **************************************************************************************
// 
//53 双暗杠 2个暗杠,不计双暗刻
// 
// **************************************************************************************
int CPublicJudge::Check2AnGang( HURESULT &sHuResult )
{
	if ( m_pCheckParam->cnAnGang == 2 )
	{
		// 屏蔽掉不计的番种
		m_abEnableRule[FAN_2ANKE] = false;
		return 1;
	}
	return 0;
}

// **************************************************************************************
// 
//54 双箭刻 2副箭刻(或杠)
// 不计箭刻
// 
// **************************************************************************************
int CPublicJudge::Check2JianKe( HURESULT &sHuResult )
{
	if ( m_sGroupsRelation.acnKeGroups[COLOR_JIAN][0] 
		+ m_sGroupsRelation.acnKeGroups[COLOR_JIAN][1]
		+ m_sGroupsRelation.acnKeGroups[COLOR_JIAN][2] == 2 )
	{
		// 屏蔽掉不计的番种
		m_abEnableRule[FAN_JIANKE] = false;
		return 1;
	}
	
	return 0;
}

//
//4 番
// **************************************************************************************
// 
//55 全带幺 和牌时，每副牌、将牌都有幺牌
// 
// **************************************************************************************
int CPublicJudge::CheckQuanDai1( HURESULT &sHuResult )
{
	for( int i = 0; i < m_cnMaxGroups; i++ )
	{
		int cnStones = 1;
		if ( sHuResult.asGroup[i].nGroupStyle == GROUP_STYLE_SHUN )
		{
			cnStones = 3;
		}
		int j = 0;
		for(j = 0; j < cnStones; j++ )
		{
			if ( sHuResult.asGroup[i].asStone[j].nColor >= COLOR_WIND 
				|| sHuResult.asGroup[i].asStone[j].nWhat == STONE_NO1 
				|| sHuResult.asGroup[i].asStone[j].nWhat == STONE_NO9 )
			{
				break;
			}
		}
		if ( j == cnStones )
		{
			return 0;
		}
	}

	return 1;
}

// **************************************************************************************
// 
//56 不求人 4副牌及将中没有吃牌、碰牌(包括明杠)，自摸和牌
// 不计门清、自摸
// 
// **************************************************************************************
int CPublicJudge::CheckBuQiuRen( HURESULT &sHuResult )
{
	if ( m_pCheckParam->cnShowGroups == m_pCheckParam->cnAnGang 
		&& ( m_pCheckParam->nWinMode & WIN_MODE_ZIMO ) )
	{
		// 屏蔽掉不计的番种
		m_abEnableRule[FAN_MENGQING] = false;
//		m_abEnableRule[FAN_ZIMO] = false;
		return 1;
	}
	return 0;
}

// **************************************************************************************
// 
//57 双明杠 2个明杠
// 不计明杠
// 
// **************************************************************************************
int CPublicJudge::Check2MinGang( HURESULT &sHuResult )
{
	if ( m_pCheckParam->cnMinGang == 2 )
	{
		// 屏蔽掉不计的番种
		m_abEnableRule[FAN_MINGANG] = false;
		return 1;
	}
	return 0;
}

// **************************************************************************************
// 
//58 和绝张 和牌池、桌面已亮明的3张牌所剩的第4张牌(抢杠和不计和绝张)
// 
// **************************************************************************************
int CPublicJudge::CheckHuJueZhang( HURESULT &sHuResult )
{
	if ( m_pCheckParam->nWinMode & WIN_MODE_HUJUEZHANG )
	{
		return 1;
	}
	return 0;
}

//
//2 番
// **************************************************************************************
// 
//59 箭刻 由中、发、白3张相同的牌组成的刻子
// 
// **************************************************************************************
int CPublicJudge::CheckJianKe( HURESULT &sHuResult )
{
	if ( m_sGroupsRelation.acnKeGroups[COLOR_JIAN][0] != 0
		|| m_sGroupsRelation.acnKeGroups[COLOR_JIAN][1] != 0
		|| m_sGroupsRelation.acnKeGroups[COLOR_JIAN][2] != 0 )
	{
		return 1;
	}
	
	return 0;
}

// **************************************************************************************
// 
//60 圈风刻 与圈风相同的风刻
// 
// **************************************************************************************
int CPublicJudge::CheckQuanFeng( HURESULT &sHuResult )
{
	int nQuanWhat = ( m_pCheckParam->nQuanWind & 0x00f0 ) >> 4;
	if ( m_sGroupsRelation.acnKeGroups[COLOR_WIND][nQuanWhat] != 0 )
	{
		return 1;
	}
	return 0;
}

// **************************************************************************************
// 
//61 门风刻 与本门风相同的风刻
// 
// **************************************************************************************
int CPublicJudge::CheckMenFeng( HURESULT &sHuResult )
{
	int nMenWhat = ( m_pCheckParam->nMenWind & 0x00f0 ) >> 4;
	if ( m_sGroupsRelation.acnKeGroups[COLOR_WIND][nMenWhat] != 0 )
	{
		return 1;
	}
	return 0;
}

// **************************************************************************************
// 
//62 门前清 没有吃、碰、明杠，和别人打出的牌
// 
// **************************************************************************************
int CPublicJudge::CheckMenQing( HURESULT &sHuResult )
{
	if ( m_pCheckParam->cnShowGroups == m_pCheckParam->cnAnGang )
	{
		return 1;
	}
	return 0;
}

int CPublicJudge::CheckPinHu( HURESULT &sHuResult )
{
	//有顺子为平胡
	for (int i = 0; i < sHuResult.cnGroups; i++ ) 
	{
		if ( sHuResult.asGroup[i].nGroupStyle == GROUP_STYLE_SHUN)
		{
			return 1;
		}
	}
	return 0;
}

// **************************************************************************************
// 
//64 四归一 和牌中，有4张相同的牌归于一家的顺、刻子、对、将牌中(不包括杠牌)
// 
// **************************************************************************************
int CPublicJudge::Check4Gui1( HURESULT &sHuResult )
{
	int cnSame4Stone = 0;
	for( int i = 0; i < 34; i++ )
	{
		if ( m_sStonesRelation.acnAllStones[i] >= 4 )
		{
			cnSame4Stone++;
		}
	}

	return cnSame4Stone - m_pCheckParam->cnAnGang - m_pCheckParam->cnMinGang;
}

// **************************************************************************************
// 
//65 双同刻 2副序数相同的刻子 
// 
// **************************************************************************************
int CPublicJudge::Check2TongKe( HURESULT &sHuResult )
{
	int cnTongKe = 0;
	for( int i = 0; i <= STONE_NO9; i++ )
	{
		if ( m_sGroupsRelation.acnKeGroups[COLOR_WAN][i] 
			+ m_sGroupsRelation.acnKeGroups[COLOR_BING][i] 
			+ m_sGroupsRelation.acnKeGroups[COLOR_TIAO][i] >= 2 ) 
		{
			cnTongKe++;
		}
	}

	if ( cnTongKe > 0 )
	{
		// 屏蔽掉不计的番种
		m_abEnableRule[FAN_2TONGKE] = false;
	}
			
	return cnTongKe;
}

// **************************************************************************************
// 
//66 双暗刻 2个暗刻
// 
// **************************************************************************************
int CPublicJudge::Check2AnKe( HURESULT &sHuResult )
{
	// 暗刻数 
	int cnAnKe = 0;
	for( int i = m_pCheckParam->cnShowGroups; i < m_cnMaxGroups; i++ )
	{
		if ( sHuResult.asGroup[i].nGroupStyle == GROUP_STYLE_KE )
		{
			// 如果不是自摸和牌，成和的那一刻不能算暗刻
			if ( ( m_pCheckParam->nWinMode & WIN_MODE_ZIMO ) == 0 
				&& ( sHuResult.asGroup[i].asStone[0].nID == m_pCheckParam->asHandStone[0].nID 
				|| sHuResult.asGroup[i].asStone[1].nID == m_pCheckParam->asHandStone[0].nID
				|| sHuResult.asGroup[i].asStone[2].nID == m_pCheckParam->asHandStone[0].nID	) )
			{
				continue;
			}
			cnAnKe++;
		}
	}
	 
	if ( cnAnKe + m_pCheckParam->cnAnGang == 2 )
	{
		return 1;
	}

	return 0;
}

// **************************************************************************************
// 
//67 暗杠 自抓4张相同的牌开杠
// 
// **************************************************************************************
int CPublicJudge::CheckAnGang( HURESULT &sHuResult )
{
	if ( m_pCheckParam->cnAnGang == 1 )
	{
		return 1;
	}
	
	return 0;
}

// **************************************************************************************
// 
//68 断幺 和牌中没有一、九及字牌 1 番
// 不计无字
// 
// **************************************************************************************
int CPublicJudge::CheckDuan19( HURESULT &sHuResult )
{
	int i = 0;
	for(i = 0; i < 3; i++ )
	{
		if ( m_sStonesRelation.acnAllStones[i * 9] != 0
			|| m_sStonesRelation.acnAllStones[i * 9 + STONE_NO9] != 0 )
		{
			return 0;
		}
	}
	for( i = 3; i < 5; i++ )
	{
		if ( m_sStonesRelation.acnAllColors[i] != 0 )
		{
			return 0;
		}
	}

	// 屏蔽掉不计的番种
	m_abEnableRule[FAN_WUZI] = false;
	return 1;
}

// **************************************************************************************
// 
//69 一般高 由一种花色2副相同的顺子组成的牌 
// 
// **************************************************************************************
int CPublicJudge::CheckYiBanGao( HURESULT &sHuResult )
{
	int cnSame2Shun = 0;
	for( int i = 0; i < 3; i++ )
	{
		for( int j = 0; j < 7; j++ )
		{
			if ( m_sGroupsRelation.acnShunGroups[i][j] == 2 )
			{
				cnSame2Shun++;
			}
		}
	}
	return cnSame2Shun;
}

// **************************************************************************************
// 
//70 喜相逢 2种花色2副序数相同的顺子
// 
// **************************************************************************************
int CPublicJudge::CheckXiXiangFeng( HURESULT &sHuResult )
{
	int cnXiXiangFeng = 0;
	for( int i = 0; i <= STONE_NO7; i++ )
	{
		if ( ( ( m_sGroupsRelation.acnShunGroups[COLOR_WAN][i] != 0 ? 1 : 0 )
			+ ( m_sGroupsRelation.acnShunGroups[COLOR_TIAO][i] != 0 ? 1 : 0 )
			+ ( m_sGroupsRelation.acnShunGroups[COLOR_BING][i] != 0 ? 1 : 0 ) ) == 2 ) 
		{
			cnXiXiangFeng++;
		}
	}
	assert( cnXiXiangFeng <= 2 );
	return cnXiXiangFeng;
}

// **************************************************************************************
// 
//71 连六 一种花色6张相连接的序数牌
// 
// **************************************************************************************
int CPublicJudge::CheckLian6( HURESULT &sHuResult )
{
	int cnLian6 = 0;
	for( int i = 0; i < 3; i++ )
	{
		for( int j = 0; j <= STONE_NO4; j++ )
		{
			// 第一顺最大只能是456
			if ( m_sGroupsRelation.acnShunGroups[i][j] != 0
				&& m_sGroupsRelation.acnShunGroups[i][j + 3] != 0 )
			{
				cnLian6++;
			}
		}
	}
	return cnLian6;
}

// **************************************************************************************
// 
//72 老少副 一种花色牌的123、789两副顺子
// 
// **************************************************************************************
int CPublicJudge::CheckLaoShaoFu( HURESULT &sHuResult )
{
	int cnLaoShaoFu = 0;
	for( int i = 0; i < 3; i++ )
	{
		if ( m_sGroupsRelation.acnShunGroups[i][STONE_NO1] != 0
			&& m_sGroupsRelation.acnShunGroups[i][STONE_NO7] != 0 )
		{
			cnLaoShaoFu++;
		}
	}
	return cnLaoShaoFu;
}

// **************************************************************************************
// 
//73 幺九刻 3张相同的一、九序数牌及字牌组成的刻子(或杠)
// 风刻有三个或以上以则所有风刻不计幺九刻
// 与圈风或门风相同的风刻不计幺九刻
// 箭牌不计幺九刻
// 
// **************************************************************************************
int CPublicJudge::Check19Ke( HURESULT &sHuResult )
{
	int cn19Ke = 0;
	int i = 0;
	for(i = 0; i < 3; i++ )
	{
		cn19Ke += m_sGroupsRelation.acnKeGroups[i][STONE_NO1] + m_sGroupsRelation.acnKeGroups[i][STONE_NO9];
	}

	int cnWind = 0;
	for( i = 0; i < m_cnMaxGroups - 1; i++ )
	{
		if ( sHuResult.asGroup[i].asStone[0].nColor == COLOR_WIND )
		{
			cnWind++;
		}
	}
	if ( cnWind >= 3 )
	{
		// 风刻有三个或以上以则所有风刻不计幺九刻
		return cn19Ke;
	}

	for( i = 0; i < 4; i++ )
	{
		if ( i != ( m_pCheckParam->nQuanWind & 0x00f0 ) >> 4
			&& i != ( m_pCheckParam->nMenWind & 0x00f0 ) >> 4 ) 
		{
			cn19Ke += m_sGroupsRelation.acnKeGroups[COLOR_WIND][i];
		}
	}
	return cn19Ke;
}

// **************************************************************************************
// 
//74 明杠 自己有暗刻，碰别人打出的一张相同的牌开杠：或自己抓进一张与碰的明刻相同的牌开杠
// 
// **************************************************************************************
int CPublicJudge::CheckMinGang( HURESULT &sHuResult )
{
	if ( m_pCheckParam->cnMinGang == 1 )
	{
		return 1;
	}
	return 0;
}

// **************************************************************************************
// 
//75 缺一门 和牌中缺少一种花色序数牌
// 
// **************************************************************************************
int CPublicJudge::CheckQue1Men( HURESULT &sHuResult )
{
	int cnQueMeng = 0;
	for( int i = 0; i < 3; i++ )
	{
		if ( m_sStonesRelation.acnAllColors[i] == 0 )
		{
			cnQueMeng++;
		}
	}
	if ( cnQueMeng == 1 )
	{
		return 1;
	}
	return 0;
}

// **************************************************************************************
// 
//76 无字 和牌中没有风、箭牌
// 
// **************************************************************************************
int CPublicJudge::CheckWuZi( HURESULT &sHuResult )
{
	if ( m_sStonesRelation.acnAllColors[COLOR_WIND] 
		+ m_sStonesRelation.acnAllColors[COLOR_JIAN] == 0 )
	{
		return 1;
	}
	return 0;
}

// **************************************************************************************
// 
//77 边张 单和123的3及789的7或1233和3、77879和7都为边张。手中有12345和3，56789和7不算边张
// 
// **************************************************************************************
int CPublicJudge::CheckBianZang( HURESULT &sHuResult )
{
	if ( ( m_pCheckParam->nWinMode & WIN_MODE_TIANHU ) == WIN_MODE_TIANHU ) 
	{
		// 天和不计边坎钓
		return 0;
	}

	// 先搜索到最后摸的那张牌的花色、数字和所在分组号
	TILEINFO sWinTileInfo = { 0	};
	if ( !GetTileInfo( m_pCheckParam->asHandStone[0].nID, sHuResult.asGroup, sWinTileInfo ) )
	{
		// 居然没找到
		ASSERT( FALSE );
		return 0;
	}

	if ( sHuResult.asGroup[sWinTileInfo.nGroupIndex].nGroupStyle != GROUP_STYLE_SHUN )
	{
		// 不是顺
		return 0;
	}

	int nNoWhat;	// 不能有这张牌作顺的第一张的顺子
	if ( sWinTileInfo.nWhat == STONE_NO3 )
	{
		if ( sWinTileInfo.nIndex != 2 )
		{
			// 不是边顺
			return 0;
		}
		nNoWhat = STONE_NO3;
	}
	else if ( sWinTileInfo.nWhat == STONE_NO7 )
	{
		if ( sWinTileInfo.nIndex != 0 )
		{
			// 不是边顺
			return 0;
		}
		nNoWhat = STONE_NO5;
	}
	else
	{
		return 0;
	}

	for( int i = sHuResult.cnShowGroups; i < sHuResult.cnGroups - 1; i++ )
	{// 最后一对是将牌
		if ( sHuResult.asGroup[i].nGroupStyle == GROUP_STYLE_SHUN 
			&& sHuResult.asGroup[i].asStone[0].nColor == sWinTileInfo.nColor
			&& sHuResult.asGroup[i].asStone[0].nWhat == nNoWhat )
		{
			// 不能有其它用本张作为头一张(3)或最后一张(7)组成的顺
			return 0;
		}
	}

	// 不计坎张和单钓将
	m_abEnableRule[FAN_KANZANG] = false;
	m_abEnableRule[FAN_DANDIAO] = false;
	return 1;
}

// **************************************************************************************
// 
//78 坎张 和2张牌之间的牌。4556和5也为坎张，手中有45567和6不算坎张
// 
// **************************************************************************************
int CPublicJudge::CheckKanZang( HURESULT &sHuResult )
{
	if ( ( m_pCheckParam->nWinMode & WIN_MODE_TIANHU ) == WIN_MODE_TIANHU ) 
	{
		// 天和不计边坎钓
		return 0;
	}

	// 先搜索到最后摸的那张牌的花色和数字
	TILEINFO sWinTileInfo = { 0	};
	if ( !GetTileInfo( m_pCheckParam->asHandStone[0].nID, sHuResult.asGroup, sWinTileInfo ) )
	{
		// 居然没找到
		ASSERT( FALSE );
		return 0;
	}
	
	if ( sHuResult.asGroup[sWinTileInfo.nGroupIndex].nGroupStyle != GROUP_STYLE_SHUN
		|| sWinTileInfo.nIndex != 1 )
	{
		// 不是顺或者不是顺的第二张
		return 0;
	}

	int nNoWhat1, nNoWhat2;// 不能有这两种顺子
	if ( sWinTileInfo.nWhat >= STONE_NO3 && sWinTileInfo.nWhat <= STONE_NO7 )
	{
		nNoWhat1 = sWinTileInfo.nWhat - 2;	// 本张作为顺的最后一张
		nNoWhat2 = sWinTileInfo.nWhat;		// 本张作为顺的第一张
	}
	else if ( sWinTileInfo.nWhat == STONE_NO2 )
	{
		nNoWhat1 = nNoWhat2 = STONE_NO2;//只要没有234就行了
	}
	else if ( sWinTileInfo.nWhat == STONE_NO8 )
	{
		nNoWhat1 = nNoWhat2 = STONE_NO6;//只要没有678就行了
	}
	else
	{
		return 0;
	}

	for( int i = sHuResult.cnShowGroups; i < sHuResult.cnGroups - 1; i++ )
	{// 最后一对是将牌
		if ( sHuResult.asGroup[i].nGroupStyle == GROUP_STYLE_SHUN 
			&& sHuResult.asGroup[i].asStone[0].nColor == sWinTileInfo.nColor
			&& ( sHuResult.asGroup[i].asStone[0].nWhat == nNoWhat1 
			|| sHuResult.asGroup[i].asStone[0].nWhat == nNoWhat2 ) )
		{
			// 不能有将本张作为第一张或最后一张的顺
			return 0;
		}
	}

	// 不计边张和单钓将
	m_abEnableRule[FAN_BIANZANG] = false;
	m_abEnableRule[FAN_DANDIAO] = false;
	return 1;

}

// **************************************************************************************
// 
//79 单钓将 钓单张牌作将成和
// 
// **************************************************************************************
int CPublicJudge::CheckDanDiao( HURESULT &sHuResult )
{
	if ( ( m_pCheckParam->nWinMode & WIN_MODE_TIANHU ) == WIN_MODE_TIANHU ) 
	{
		// 天和不计边坎钓
		return 0;
	}

	// 最后一组必是将牌
	ASSERT( sHuResult.asGroup[m_cnMaxGroups - 1].nGroupStyle == GROUP_STYLE_JONG );

	if ( m_pCheckParam->asHandStone[0].nID == sHuResult.asGroup[4].asStone[0].nID 
		|| m_pCheckParam->asHandStone[0].nID == sHuResult.asGroup[4].asStone[1].nID )
	{
		return 1;
	}

	return 0;
}

// **************************************************************************************
// 
//80 自摸 自己抓进牌成和牌
// 
// **************************************************************************************
int CPublicJudge::CheckZiMo( HURESULT &sHuResult )
{
	return 0;
}

// **************************************************************************************
// 
// 以下是大众麻将的番种
// 
// **************************************************************************************

// 168番
// **************************************************************************************
// 
// 82 四方大发财
// 不计大四喜，小四喜，字一色，门风刻，圈风刻，3风刻，碰碰和，全带幺，混幺九 幺九刻
// 
// **************************************************************************************
int CPublicJudge::Check4FangDaFa( HURESULT &sHuResult )
{
	for( int i = 0; i < 4; i++ )
	{
		if ( m_sGroupsRelation.acnKeGroups[COLOR_WIND][i] != 1 ) 
		{
			return 0;
		}
	}
	if ( m_sStonesRelation.acnAllStones[32] != 2 )
	{
		// 发财两个
		return 0;
	}
	
	// 屏蔽掉不计的番种
	m_abEnableRule[FAN_DA4XI] = false;
	m_abEnableRule[FAN_XIAO4XI] = false;
	m_abEnableRule[FAN_ZI1SHE] = false;
	m_abEnableRule[FAN_QUANFENG] = false;
	m_abEnableRule[FAN_MENGFENG] = false;
	m_abEnableRule[FAN_3FENGKE] = false;
	m_abEnableRule[FAN_PENPENHU] = false;
	m_abEnableRule[FAN_HUN19] = false;
	m_abEnableRule[FAN_QUANDAIYAO] = false;
	m_abEnableRule[FAN_19KE] = false;

	return 1;
}

// **************************************************************************************
// 
// 87 八仙过海，手上有八张花牌，不计春夏秋冬，梅兰竹菊
// 
// **************************************************************************************
int CPublicJudge::Check8Xian( HURESULT &sHuResult )
{
	if ( m_pCheckParam->cnFlower == 8 )
	{
		return 1;
	}
	return 0;
}

// 32番
// **************************************************************************************
// 
// 88 七抢一，手上有七张花牌和牌，另一家手上有一张花牌，不计春夏秋冬，梅兰竹菊
// 
// **************************************************************************************
int CPublicJudge::Check7Qiang1( HURESULT &sHuResult )
{
	return 0;
}

int CPublicJudge::CheckLiZhi( HURESULT &sHuResult )
{
	if (m_pCheckParam->nWinMode & WIN_MODE_LIZHI)
	{
		return 1;
	}
	return 0;
}

// 1番
// **************************************************************************************
// 
// 97 二五八将
// 
// **************************************************************************************
int CPublicJudge::Check258Jong( HURESULT &sHuResult )
{
	ASSERT( sHuResult.asGroup[4].nGroupStyle == GROUP_STYLE_JONG );

	if ( sHuResult.asGroup[4].asStone[0].nColor < 3
		&& ( sHuResult.asGroup[4].asStone[0].nWhat == STONE_NO2 
			|| sHuResult.asGroup[4].asStone[0].nWhat == STONE_NO5 
			|| sHuResult.asGroup[4].asStone[0].nWhat == STONE_NO8 ) )
	{
		return 1;
	}

	return 0;
}

// **************************************************************************************
// 
// 98 梅兰竹菊，集齐这四张花牌之后额外加分
// 
// **************************************************************************************
int CPublicJudge::Check4Flower( HURESULT &sHuResult )
{
//	if ( m_pCheckParam->pFlower == NULL )
//	{
//		return 0;
//	}

	int cnFlower = 0;
	for( int i = 0; i < m_pCheckParam->cnFlower; i++ )
	{
		if ( m_pCheckParam->asFlower[i].nColor == COLOR_FLOWER )
		{
			cnFlower++;
		}
	}
	if ( cnFlower == 4 )
	{
		return 1;
	}

	return 0;
}

// **************************************************************************************
// 
// 99 春夏秋冬，集齐这四张花牌之后额外加分
// 
// **************************************************************************************
int CPublicJudge::Check4Season( HURESULT &sHuResult )
{
//	if ( m_pCheckParam->pFlower == NULL )
//	{
//		return 0;
//	}
	
	int cnSeason = 0;
	for( int i = 0; i < m_pCheckParam->cnFlower; i++ )
	{
		if ( m_pCheckParam->asFlower[i].nColor == COLOR_SEASON )
		{
			cnSeason++;
		}
	}
	if ( cnSeason == 4 )
	{
		return 1;
	}
	
	return 0;
}

// **************************************************************************************
// 
// 100 季花
// 
// **************************************************************************************
int CPublicJudge::CheckSeasonFlower( HURESULT &sHuResult )
{
	int cnSeasonFlower = 0;
	for( int i = 0; i < m_pCheckParam->cnFlower; i++ )
	{
		if ( m_pCheckParam->asFlower[i].nWhat == ( ( m_pCheckParam->nMenWind & 0x00f0 ) >> 4 ) )
		{
			cnSeasonFlower++;
		}
	}

	return cnSeasonFlower;
}

// **************************************************************************************
// 
// 101 么九头 由序数牌的19做将牌
// 
// **************************************************************************************
int CPublicJudge::Check19Jong( HURESULT &sHuResult )
{
	ASSERT( sHuResult.asGroup[4].nGroupStyle == GROUP_STYLE_JONG );
	
	if ( sHuResult.asGroup[4].asStone[0].nColor < 3
		&& ( sHuResult.asGroup[4].asStone[0].nWhat == STONE_NO1 
		|| sHuResult.asGroup[4].asStone[0].nWhat == STONE_NO9 ) )
	{
		return 1;
	}
	
	return 0;
}

// **************************************************************************************
// 
// 102 极速二麻胡
// 
// **************************************************************************************
int CPublicJudge::CheckJSPKHu( HURESULT &sHuResult )
{
	int nCount = 0;

	for (int i = 0 ; i < 34; i++)
	{
		if (m_sStonesRelation.acnAllStones[i] > 0)
		{
			int nOneTileCount = m_asAllStone[i].nWhat + 1; // 万条筒按照点数计算
			if (m_asAllStone[i].nColor >= COLOR_WIND) // 风箭花统一按照10点计算
			{
				nOneTileCount = 10;
			}
			nCount += nOneTileCount * m_sStonesRelation.acnAllStones[i];
		}
	}
	
	return nCount;
}


// **************************************************************************************
// 
// 取番种信息
// 
// **************************************************************************************
BOOL CPublicJudge::GetFanInfo( int nFanID, char szFanName[], char szFanScore[] )
{
	if ( nFanID < 0 || nFanID > 127 ) 
	{
		return FALSE;
	}

	// 所有番种的名字，这个顺序是和nFanID相对应的
	static char* stc_pszFanName[] =
	{
		"大四喜",
		"大三元",
		"绿一色",
		"九莲宝灯",
		"四杠",
		"连七对",
		"十三幺",
		"清幺九",
		"小四喜",
		"小三元",
		"字一色",
		"四暗刻",
		"一色双龙会",
		"一色四同顺",
		"一色四节高",
		"一色四步高",
		"三杠",
		"混幺九",
		"七小对",
		"七星不靠",
		"全双刻",
		"清一色",
		"一色三同顺",
		"一色三节高",
		"全大",
		"全中",
		"全小",
		"青龙",
		"三色双龙会",
		"一色三步高",
		"全带五",
		"三同刻",
		"三暗刻",
		"全不靠",
		"组合龙",
		"大于五",
		"小于五",
		"三风刻",
		"花龙",
		"推不倒",
		"三色三同顺",
		"三色三节高",
		"无番和",
		"妙手回春",
		"海底捞月",
		"杠上开花",
		"抢杠和",
		"碰碰和",
		"混一色",
		"三色三步高",
		"五门齐",
		"全求人",
		"双暗杠",
		"双箭刻",
		"全带幺",
		"不求人",
		"双明杠",
		"和绝张",
		"箭刻",
		"圈风刻",
		"门风刻",
		"门前清",
		"平和",
		"四归一",
		"双同刻",
		"双暗刻",
		"暗杠",
		"断幺九",
		"一般高",
		"喜相逢",
		"连六",
		"老少配",
		"幺九刻",
		"明杠",
		"缺一门",
		"无字",
		"边张",
		"坎张",
		"单钓将",
		"自摸",
		"花牌",
		"四方大发财",
		"天和",
		"地和",
		"人和",
		"混杠",
		"八仙过海",
		"七抢一",
		"天听",
		"混四节",
		"混四步",
		"混三节",
		"无混",
		"混龙",
		"混三步",
		"立直",
		"二五八将",
		"梅兰竹菊",
		"春夏秋冬",
		"季花",
		"么九头",
		"加番",
		"极速二麻胡",
		"极速二麻奖花",
	} ;

	_tcscpy_s( szFanName , sizeof(szFanName), stc_pszFanName[ nFanID ] ) ;
	wsprintf( szFanScore, "%d番", m_asFanInfo[nFanID].nScore );
	
	return TRUE;
}

// **************************************************************************************
// 
//88番
//1 大四喜 由4副风刻(杠)组成的和牌。不计圈风刻、门风刻、三风刻、碰碰和、小四喜
// 
// **************************************************************************************
int CPublicJudge::ParseDa4Xi( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_DA4XI] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_DA4XI;
	sFanInfo.nScore = m_asFanInfo[FAN_DA4XI].nScore;
	for( int i = 0; i < sHuResult.cnGroups; i++ )
	{
		if ( sHuResult.asGroup[i].nGroupStyle != GROUP_STYLE_JONG ) 
		{
			sFanInfo.cnTile += GetID( sHuResult.asGroup[i], sFanInfo.anTileID + sFanInfo.cnTile );
		}
	}

	vsFanInfo.push_back( sFanInfo );

	return 1;
}

// **************************************************************************************
// 
//2 大三元 和牌中，有中发白3副刻子。不计箭刻
// 
// **************************************************************************************
int CPublicJudge::ParseDa3Yuan( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_DA3YUAN] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_DA3YUAN;
	sFanInfo.nScore = m_asFanInfo[FAN_DA3YUAN].nScore;
	for( int i = 0; i < sHuResult.cnGroups; i++ )
	{
		if ( sHuResult.asGroup[i].asStone[0].nColor == COLOR_JIAN ) 
		{
			sFanInfo.cnTile += GetID( sHuResult.asGroup[i], sFanInfo.anTileID + sFanInfo.cnTile );
		}
	}

	vsFanInfo.push_back( sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
//3 绿一色 由23468条及发字中的任何牌组成的顺子、刻子、将的和牌。不计混一色。如无“发”字组成的各牌，可计清一色
// 
// **************************************************************************************
int CPublicJudge::ParseLv1She( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_LVYISHE] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_LVYISHE;
	sFanInfo.nScore = m_asFanInfo[FAN_LVYISHE].nScore;
	vsFanInfo.push_back( sFanInfo );

	return 1;
}

// **************************************************************************************
// 
//4 九莲宝灯 由一种花色序数牌子按1112345678999组成的特定牌型，见同花色任何1张序数牌即成和牌。不计清一色
// 
// **************************************************************************************
int CPublicJudge::Parse9LianBaoDeng( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_9LIANBAODEN] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_9LIANBAODEN;
	sFanInfo.nScore = m_asFanInfo[FAN_9LIANBAODEN].nScore;
	vsFanInfo.push_back( sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
//5 四杠 4个杠
// 
// **************************************************************************************
int CPublicJudge::Parse4Gang( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_4GANG] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_4GANG;
	sFanInfo.nScore = m_asFanInfo[FAN_4GANG].nScore;
	for( int i = 0; i < sHuResult.cnGroups; i++ )
	{
		if ( sHuResult.asGroup[i].nGroupStyle == GROUP_STYLE_MINGGANG 
			|| sHuResult.asGroup[i].nGroupStyle == GROUP_STYLE_ANGANG )
		{
			sFanInfo.anTileID[sFanInfo.cnTile] = sHuResult.asGroup[i].asStone[0].nID;
			sFanInfo.anTileID[sFanInfo.cnTile + 1] = sHuResult.asGroup[i].asStone[1].nID;
			sFanInfo.anTileID[sFanInfo.cnTile + 2] = sHuResult.asGroup[i].asStone[2].nID;
			sFanInfo.anTileID[sFanInfo.cnTile + 3] = sHuResult.asGroup[i].asStone[3].nID;
		}
		sFanInfo.cnTile += 4;
	}
	
	vsFanInfo.push_back( sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
//6 连七对 由一种花色序数牌组成序数相连的7个对子的和牌。不计清一色、不求人、单钓 
// 
// **************************************************************************************
int CPublicJudge::ParseLian7Dui( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_LIAN7DUI] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_LIAN7DUI;
	sFanInfo.nScore = m_asFanInfo[FAN_LIAN7DUI].nScore;
	vsFanInfo.push_back( sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
//7 十三幺 由3种序数牌的一、九牌，7种字牌及其中一对作将组成的和牌。不计五门齐、不求人、单钓
// 
// **************************************************************************************
int CPublicJudge::Parse13Yao( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_131] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_131;
	sFanInfo.nScore = m_asFanInfo[FAN_131].nScore;
	vsFanInfo.push_back( sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
//
//64番
//8 清幺九 由序数牌一、九刻子组成的和牌。不计碰碰和、同刻、元字 
// 
// **************************************************************************************
int CPublicJudge::ParseQing19( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_QING19] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_QING19;
	sFanInfo.nScore = m_asFanInfo[FAN_QING19].nScore;
	vsFanInfo.push_back( sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
//9 小四喜 和牌时有风牌的3副刻子及将牌。不计三风刻
// 
// **************************************************************************************
int CPublicJudge::ParseXiao4Xi( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_XIAO4XI] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_XIAO4XI;
	sFanInfo.nScore = m_asFanInfo[FAN_XIAO4XI].nScore;
	for( int i = 0; i < sHuResult.cnGroups; i++ )
	{
		if ( sHuResult.asGroup[i].asStone[0].nColor == COLOR_WIND )
		{
			sFanInfo.cnTile += GetID( sHuResult.asGroup[i], sFanInfo.anTileID + sFanInfo.cnTile );
		}
	}

	vsFanInfo.push_back( sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
//10 小三元 和牌时有箭牌的两副刻子及将牌。不计箭刻
// 
// **************************************************************************************
int CPublicJudge::ParseXiao3Yuan( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_XIAO3YUAN] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_XIAO3YUAN;
	sFanInfo.nScore = m_asFanInfo[FAN_XIAO3YUAN].nScore;
	for( int i = 0; i < sHuResult.cnGroups; i++ )
	{
		if ( sHuResult.asGroup[i].asStone[0].nColor != COLOR_JIAN )
		{
			continue;
		}
		sFanInfo.cnTile += GetID( sHuResult.asGroup[i], sFanInfo.anTileID + sFanInfo.cnTile );
	}
	vsFanInfo.push_back( sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
//11 字一色 由字牌的刻子(杠)、将组成的和牌。不计碰碰和
// 
// **************************************************************************************
int CPublicJudge::ParseZi1She( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_ZI1SHE] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_ZI1SHE;
	sFanInfo.nScore = m_asFanInfo[FAN_ZI1SHE].nScore;
	vsFanInfo.push_back( sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
//12 四暗刻 4个暗刻(暗杠)。不计门前清、碰碰和
// 
// **************************************************************************************
int CPublicJudge::Parse4AnKe( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_4ANKE] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_4ANKE;
	sFanInfo.nScore = m_asFanInfo[FAN_4ANKE].nScore;
	for( int i = 0; i < sHuResult.cnGroups; i++ )
	{
		if ( sHuResult.asGroup[i].nGroupStyle == GROUP_STYLE_KE 
			|| sHuResult.asGroup[i].nGroupStyle == GROUP_STYLE_MINGGANG 
			|| sHuResult.asGroup[i].nGroupStyle == GROUP_STYLE_ANGANG )
		{
			sFanInfo.cnTile += GetID( sHuResult.asGroup[i], sFanInfo.anTileID + sFanInfo.cnTile );
		}
	}
	vsFanInfo.push_back( sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
//13 一色双龙会 一种花色的两个老少副，5为将牌。不计平和、七对、清一色
// 
// **************************************************************************************
int CPublicJudge::Parse1She2Long( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_1SHE2GLONG] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_1SHE2GLONG;
	sFanInfo.nScore = m_asFanInfo[FAN_1SHE2GLONG].nScore;
	vsFanInfo.push_back( sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
//
//48番
//14 一色四同顺 一种花色4副序数相同的顺子，不计一色三节高、一般高、四归一 
// 
// **************************************************************************************
int CPublicJudge::Parse1She4TongShun( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_1SHE4TONGSHUN] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_1SHE4TONGSHUN;
	sFanInfo.nScore = m_asFanInfo[FAN_1SHE4TONGSHUN].nScore;
	for( int i = 0; i < sHuResult.cnGroups; i++ )
	{
		if ( sHuResult.asGroup[i].nGroupStyle != GROUP_STYLE_JONG )
		{
			sFanInfo.cnTile += GetID( sHuResult.asGroup[i], sFanInfo.anTileID + sFanInfo.cnTile );
		}
	}
	vsFanInfo.push_back( sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
//15 一色四节高 一种花色4副依次递增一位数的刻子不计一色三同顺、碰碰和 
// 
// **************************************************************************************
int CPublicJudge::Parse1She4JieGao( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_1SHE4JIEGAO] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_1SHE4JIEGAO;
	sFanInfo.nScore = m_asFanInfo[FAN_1SHE4JIEGAO].nScore;
	for( int i = 0; i < sHuResult.cnGroups; i++ )
	{
		if ( sHuResult.asGroup[i].nGroupStyle != GROUP_STYLE_JONG )
		{
			sFanInfo.cnTile += GetID( sHuResult.asGroup[i], sFanInfo.anTileID + sFanInfo.cnTile );
		}
	}
	vsFanInfo.push_back( sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
//
//32番
//16 一色四步高 一种花色4副依次递增一位数或依次递增二位数的顺子 
// 
// **************************************************************************************
int CPublicJudge::Parse1She4BuGao( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_1SHE4BUGAO] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_1SHE4BUGAO;
	sFanInfo.nScore = m_asFanInfo[FAN_1SHE4BUGAO].nScore;
	for( int i = 0; i < sHuResult.cnGroups; i++ )
	{
		if ( sHuResult.asGroup[i].nGroupStyle != GROUP_STYLE_JONG )
		{
			sFanInfo.cnTile += GetID( sHuResult.asGroup[i], sFanInfo.anTileID + sFanInfo.cnTile );
		}
	}
	vsFanInfo.push_back( sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
//17 三杠 3个杠
// 
// **************************************************************************************
int CPublicJudge::Parse3Gang( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_3GANG] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_3GANG;
	sFanInfo.nScore = m_asFanInfo[FAN_3GANG].nScore;
	for( int i = 0; i < sHuResult.cnGroups; i++ )
	{
		if ( sHuResult.asGroup[i].nGroupStyle == GROUP_STYLE_MINGGANG
			|| sHuResult.asGroup[i].nGroupStyle == GROUP_STYLE_ANGANG )
		{
			sFanInfo.cnTile += GetID( sHuResult.asGroup[i], sFanInfo.anTileID + sFanInfo.cnTile );
		}
	}
	vsFanInfo.push_back( sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
//18 混幺九 由字牌和序数牌一、九的刻子用将牌组成的各牌。不计碰碰和 
// 
// **************************************************************************************
int CPublicJudge::ParseHun19( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_HUN19] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_HUN19;
	sFanInfo.nScore = m_asFanInfo[FAN_HUN19].nScore;
	vsFanInfo.push_back( sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
//
//24番
//19 七对 由7个对子组成和牌。不计不求人、单钓
// 
// **************************************************************************************
int CPublicJudge::Parse7Dui( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_7DUI] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_7DUI;
	sFanInfo.nScore = m_asFanInfo[FAN_7DUI].nScore;
	vsFanInfo.push_back( sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
//20 七星不靠 必须有7个单张的东西南北中发白，加上3种花色，数位按147、258、369中的7张序数牌组成没有将牌的和牌。不计五门齐、不求人、单钓
// 
// **************************************************************************************
int CPublicJudge::Parse7XinBuKao( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_7XINBUKAO] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_7XINBUKAO;
	sFanInfo.nScore = m_asFanInfo[FAN_7XINBUKAO].nScore;
	vsFanInfo.push_back( sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
//21 全双刻 由2、4、6、8序数牌的刻子、将牌组成的和牌。不计碰碰和、断幺
// 
// **************************************************************************************
int CPublicJudge::ParseQuan2Ke( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_QUANSHUANGKE] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_QUANSHUANGKE;
	sFanInfo.nScore = m_asFanInfo[FAN_QUANSHUANGKE].nScore;
	vsFanInfo.push_back( sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
//22 清一色 由一种花色的序数牌组成和牌。不无字
// 
// **************************************************************************************
int CPublicJudge::ParseQing1She( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_QING1SHE] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_QING1SHE;
	sFanInfo.nScore = m_asFanInfo[FAN_QING1SHE].nScore;
	vsFanInfo.push_back( sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
//23 一色三同顺 和牌时有一种花色3副序数相同的顺子。不计一色三节高
// 
// **************************************************************************************
int CPublicJudge::Parse1She3TongShun( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_1SHE3TONGSHUN] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_1SHE3TONGSHUN;
	sFanInfo.nScore = m_asFanInfo[FAN_1SHE3TONGSHUN].nScore;

	// 花色
	int i = 0;
	for(i = 0; i < 3; i++ )
	{
		if ( m_sStonesRelation.acnAllColors[i] >= 9 )
		{
			break;
		}
	}
	ASSERT( i != 3 );

	// 点数
	int j = 0;
	for(j = 0; j < 7; j++ )
	{
		if ( m_sGroupsRelation.acnShunGroups[i][j] == 3 )
		{
			break;
		}
	}
	ASSERT( j != 7 );

	for( int k = 0; k < sHuResult.cnGroups; k++ )
	{
		if ( sHuResult.asGroup[k].nGroupStyle == GROUP_STYLE_SHUN 
			&& sHuResult.asGroup[k].asStone[0].nColor == i
			&& sHuResult.asGroup[k].asStone[0].nWhat == j )
		{
			sFanInfo.cnTile += GetID( sHuResult.asGroup[k], sFanInfo.anTileID + sFanInfo.cnTile );
		}
	}

	vsFanInfo.push_back( sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
//24 一色三节高 和牌时有一种花色3副依次递增一位数字的刻子。不计一色三同顺
// 
// **************************************************************************************
int CPublicJudge::Parse1She3JieGao( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_1SHE3JIEGAO] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_1SHE3JIEGAO;
	sFanInfo.nScore = m_asFanInfo[FAN_1SHE3JIEGAO].nScore;

	// 花色
	int i = 0;
	for(i = 0; i < 3; i++ )
	{
		if ( m_sStonesRelation.acnAllColors[i] >= 9 )
		{
			break;
		}
	}
	ASSERT( i != 3 );
	// 点数
	int j = 0;
	for(j = 0; j < STONE_NO8; j++ )
	{
		if ( m_sGroupsRelation.acnKeGroups[i][j] != 0
			&& m_sGroupsRelation.acnKeGroups[i][j + 1] != 0
			&& m_sGroupsRelation.acnKeGroups[i][j + 2] != 0 )
		{
			break;
		}
	}
	ASSERT( j != STONE_NO8 );

	for( int k = 0; k < sHuResult.cnGroups; k++ )
	{
		if ( IsKe( sHuResult.asGroup[k] ) && sHuResult.asGroup[k].asStone[0].nColor == i 
			&& sHuResult.asGroup[k].asStone[0].nWhat >= j && sHuResult.asGroup[k].asStone[0].nWhat <= j + 2 )
		{
			sFanInfo.cnTile += GetID( sHuResult.asGroup[k], sFanInfo.anTileID + sFanInfo.cnTile );
		}
	}

	vsFanInfo.push_back( sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
//25 全大 由序数牌789组成的顺子、刻子(杠)、将牌的和牌。不计无字
// 
// **************************************************************************************
int CPublicJudge::ParseQuanDa( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_QUANDA] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_QUANDA;
	sFanInfo.nScore = m_asFanInfo[FAN_QUANDA].nScore;
	vsFanInfo.push_back( sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
//26 全中 由序数牌456组成的顺子、刻子(杠)、将牌的和牌。不计断幺
// 
// **************************************************************************************
int CPublicJudge::ParseQuanZhong( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_QUANZHONG] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_QUANZHONG;
	sFanInfo.nScore = m_asFanInfo[FAN_QUANZHONG].nScore;
	vsFanInfo.push_back( sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
//27 全小 由序数牌123组成的顺子、刻子(杠)将牌的的和牌。不计无字
// 
// **************************************************************************************
int CPublicJudge::ParseQuanXiao( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_QUANXIAO] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_QUANXIAO;
	sFanInfo.nScore = m_asFanInfo[FAN_QUANXIAO].nScore;
	vsFanInfo.push_back( sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
//
//16番
//28 清龙 和牌时，有一种花色1-9相连接的序数牌
// 
// **************************************************************************************
int CPublicJudge::ParseQingLong( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_QINGLONG] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_QINGLONG;
	sFanInfo.nScore = m_asFanInfo[FAN_QINGLONG].nScore;
	int i = 0;
	for(i = 0; i < 3; i++ )
	{
		if ( m_sStonesRelation.acnAllColors[i] >= 9 )
		{
			break;
		}
	}
	ASSERT( i != 3 );
	ASSERT( m_sGroupsRelation.acnShunGroups[i][0] >= 1
		&& m_sGroupsRelation.acnShunGroups[i][3] >= 1
		&& m_sGroupsRelation.acnShunGroups[i][6] >= 1 );

	BOOL abFound[3] = { 0 };
	for( int j = 0; j < sHuResult.cnGroups; j++ )
	{
		if ( sHuResult.asGroup[j].nGroupStyle == GROUP_STYLE_SHUN  
			&& sHuResult.asGroup[j].asStone[0].nColor == i )
		{
			int nIndex = sHuResult.asGroup[j].asStone[0].nWhat / 3;
			int nMod = sHuResult.asGroup[j].asStone[0].nWhat % 3;
			if ( nMod == 0 && !abFound[nIndex] )
			{
				abFound[nIndex] = TRUE;
				sFanInfo.cnTile += GetID( sHuResult.asGroup[j], sFanInfo.anTileID + sFanInfo.cnTile );
			}
		}
	}

	vsFanInfo.push_back( sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
//29 三色双龙会 2种花色2个老少副、另一种花色5作将的和牌。不计喜相逢、老少副、无字、平和
// 
// **************************************************************************************
int CPublicJudge::Parse3She2Long( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_3SHE2LONG] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_3SHE2LONG;
	sFanInfo.nScore = m_asFanInfo[FAN_3SHE2LONG].nScore;
	vsFanInfo.push_back( sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
//30 一色三步高 和牌时，有一种花色3副依次递增一位或依次递增二位数字的顺子
// 
// **************************************************************************************
int CPublicJudge::Parse1She3BuGao( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_1SHE3BUGAO] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_1SHE3BUGAO;
	sFanInfo.nScore = m_asFanInfo[FAN_1SHE3BUGAO].nScore;

	// 花色
	int i = 0;
	for(i = 0; i < 3; i++ )
	{
		if ( m_sStonesRelation.acnAllColors[i] >= 9 )
		{
			break;
		}
	}
	ASSERT( i != 3 );
	// 点数
	int anWhat[3];
	int j = 0;
	for(j = 0; j < STONE_NO6; j++ )
	{
		// 第一个顺最大只能是567
		if ( m_sGroupsRelation.acnShunGroups[i][j] == 0 )
		{
			continue;
		}
		
		if ( m_sGroupsRelation.acnShunGroups[i][j + 1] != 0 
			&& m_sGroupsRelation.acnShunGroups[i][j + 2] != 0 )
		{
			// 步进一成功
			anWhat[0] = j;
			anWhat[1] = j + 1;
			anWhat[2] = j + 2;
			break;
		}
		
		if ( j < STONE_NO4 )
		{
			// 如果第一顺小于456看看步进2有没有满足要求的顺
			if ( m_sGroupsRelation.acnShunGroups[i][j + 2] != 0 
				&& m_sGroupsRelation.acnShunGroups[i][j + 4] != 0 )
			{
				anWhat[0] = j;
				anWhat[1] = j + 2;
				anWhat[2] = j + 4;
				break;
			}
		}	
	}
	ASSERT( j != STONE_NO6 );

	BOOL abFound[3] = { 0 };
	for( int k = 0; k < sHuResult.cnGroups; k++ )
	{
		if ( sHuResult.asGroup[k].nGroupStyle == GROUP_STYLE_SHUN
			 && sHuResult.asGroup[k].asStone[0].nColor == i )
		{
			for( int m = 0; m < 3; m++ )
			{
				if ( sHuResult.asGroup[k].asStone[0].nWhat == anWhat[m] && !abFound[m] )
				{
					abFound[m] = TRUE;
					sFanInfo.cnTile += GetID( sHuResult.asGroup[k], sFanInfo.anTileID + sFanInfo.cnTile );
				}
			}
		}
	}

	vsFanInfo.push_back( sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
//31 全带五 每副牌及将牌必须有5的序数牌。不计断幺
// 
// **************************************************************************************
int CPublicJudge::ParseQuanDai5( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_QUANDAI5] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_QUANDAI5;
	sFanInfo.nScore = m_asFanInfo[FAN_QUANDAI5].nScore;
	vsFanInfo.push_back( sFanInfo );

	return 1;
}

// **************************************************************************************
// 
//32 三同刻 3个序数相同的刻子(杠)
// 
// **************************************************************************************
int CPublicJudge::Parse3TongKe( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_3TONGKE] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_3TONGKE;
	sFanInfo.nScore = m_asFanInfo[FAN_3TONGKE].nScore;
	int nWhat = 0;
	for(nWhat = 0; nWhat <= STONE_NO9; nWhat++ )
	{
		if ( m_sGroupsRelation.acnKeGroups[COLOR_WAN][nWhat] != 0
			&& m_sGroupsRelation.acnKeGroups[COLOR_BING][nWhat] != 0 
			&& m_sGroupsRelation.acnKeGroups[COLOR_TIAO][nWhat] != 0 ) 
		{
			break;
		}
	}
	ASSERT( nWhat <= STONE_NO9 );

	for( int nGroupIndex = 0; nGroupIndex < sHuResult.cnGroups; nGroupIndex++ )
	{
		if ( IsKe( sHuResult.asGroup[nGroupIndex] )
			&& sHuResult.asGroup[nGroupIndex].asStone[0].nWhat == nWhat )
		{
			sFanInfo.cnTile += GetID( sHuResult.asGroup[nGroupIndex], sFanInfo.anTileID + sFanInfo.cnTile );
		}
	}

	vsFanInfo.push_back( sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
//33 三暗刻 3个暗刻 
// 
// **************************************************************************************
int CPublicJudge::Parse3AnKe( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_3ANKE] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_3ANKE;
	sFanInfo.nScore = m_asFanInfo[FAN_3ANKE].nScore;

	// 分组的排列顺序是吃碰杠的牌在前，将牌在最后
	for( int nGroupIndex = 0; nGroupIndex < sHuResult.cnGroups; nGroupIndex++ )
	{
		if ( nGroupIndex < sHuResult.cnShowGroups )
		{
			// 在吃碰杠的分组里只找暗杠
			if ( sHuResult.asGroup[nGroupIndex].nGroupStyle == GROUP_STYLE_ANGANG )
			{
				sFanInfo.cnTile += GetID( sHuResult.asGroup[nGroupIndex], sFanInfo.anTileID + sFanInfo.cnTile );
			}
		}
		else if ( sHuResult.asGroup[nGroupIndex].nGroupStyle == GROUP_STYLE_KE )
		{
			// 如果不是自摸的，除了和的张牌所在的刻，其余的刻都拷进去
			if ( ( sHuResult.nWinMode & WIN_MODE_ZIMO ) == 0
				&& ( sHuResult.asGroup[nGroupIndex].asStone[0].nID == sHuResult.nHuTileID 
				|| sHuResult.asGroup[nGroupIndex].asStone[1].nID == sHuResult.nHuTileID
				|| sHuResult.asGroup[nGroupIndex].asStone[2].nID == sHuResult.nHuTileID ) )
			{
				continue;
			}
			sFanInfo.cnTile += GetID( sHuResult.asGroup[nGroupIndex], sFanInfo.anTileID + sFanInfo.cnTile );
		}
	}
	vsFanInfo.push_back( sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
//
//12番
//34 全不靠 由单张3种花色147、258、369不能错位的序数牌及东南西北中发白中的任何14张牌组成的和牌。不计五门齐、不求人、单钓
// 
// **************************************************************************************
int CPublicJudge::ParseQuanBuKao( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_QUANBUKAO] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_QUANBUKAO;
	sFanInfo.nScore = m_asFanInfo[FAN_QUANBUKAO].nScore;
	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
//35 组合龙 3种花色的147、258、369不能错位的序数牌
// 
// **************************************************************************************
int CPublicJudge::ParseZhuHeLong( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_ZHUHELONG] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_ZHUHELONG;
	sFanInfo.nScore = m_asFanInfo[FAN_ZHUHELONG].nScore;

	// 和组合龙有两种可能的和法，一是和全不靠之组合龙，二是和组合龙和法
	if ( sHuResult.nResultant == QUANBUKAO )
	{
		// 全不靠和牌，没有分组
		for( int nTileIndx = 0; nTileIndx < m_cnMaxHandStone; nTileIndx++ )
		{
			ASSERT( sHuResult.asHandStone[nTileIndx].nID != 0 );
			if ( sHuResult.asHandStone[nTileIndx].nColor != COLOR_WIND 
				&& sHuResult.asHandStone[nTileIndx].nColor != COLOR_JIAN )
			{
				sFanInfo.anTileID[sFanInfo.cnTile] = sHuResult.asHandStone[nTileIndx].nID;
				sFanInfo.cnTile++;
			}
		}
	}
	else
	{
		// 组合龙和法
		for( int nGroupIndex = 0; nGroupIndex < sHuResult.cnGroups; nGroupIndex++ )
		{
			if ( sHuResult.asGroup[nGroupIndex].nGroupStyle == GROUP_STYLE_LONG )
			{
				sFanInfo.cnTile += GetID( sHuResult.asGroup[nGroupIndex], sFanInfo.anTileID + sFanInfo.cnTile );
			}
		}
	}

	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
//36 大于五 由序数牌6-9的顺子、刻子、将牌组成的和牌。不计无字
// 
// **************************************************************************************
int CPublicJudge::ParseDaYu5( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_DAYU5] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_DAYU5;
	sFanInfo.nScore = m_asFanInfo[FAN_DAYU5].nScore;
	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
//37 小于五 由序数牌1-4的顺子、刻子、将牌组成的和牌。不计无字
// 
// **************************************************************************************
int CPublicJudge::ParseXiaoYu5( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_XIAOYU5] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_XIAOYU5;
	sFanInfo.nScore = m_asFanInfo[FAN_XIAOYU5].nScore;
	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
//38 三风刻 3个风刻
// 
// **************************************************************************************
int CPublicJudge::Parse3FengKe( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_3FENGKE] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_3FENGKE;
	sFanInfo.nScore = m_asFanInfo[FAN_3FENGKE].nScore;

	for( int nGroupIndex = 0; nGroupIndex < sHuResult.cnGroups; nGroupIndex++ )
	{
		if ( ( sHuResult.asGroup[nGroupIndex].asStone[0].nColor == COLOR_WIND
			|| sHuResult.asGroup[nGroupIndex].asStone[0].nColor == COLOR_JIAN )
			&& IsKe( sHuResult.asGroup[nGroupIndex] ) )
		{
			sFanInfo.cnTile += GetID( sHuResult.asGroup[nGroupIndex], sFanInfo.anTileID + sFanInfo.cnTile );
		}
	}

	vsFanInfo.push_back( sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
//
//8 番
//39 花龙 3种花色的3副顺子连接成1-9的序数牌
// 
// **************************************************************************************
int CPublicJudge::ParseHuaLong( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_HUALONG] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_HUALONG;
	sFanInfo.nScore = m_asFanInfo[FAN_HUALONG].nScore;

	int anColor[3] = { 0 };
	int anWhat[3] = { 0 };
	BOOL abFound[3] = { 0 };
	for( anColor[0] = 0; anColor[0] < 3; anColor[0]++ )
	{
		if ( m_sGroupsRelation.acnShunGroups[anColor[0]][STONE_NO1] != 0 )
		{
			// 本花色有123，看看其它两个花色有没有456和789
			anColor[1] = ( anColor[0] + 1 ) % 3;
			anColor[2] = ( anColor[0] + 2 ) % 3;
			if ( m_sGroupsRelation.acnShunGroups[anColor[1]][STONE_NO4] != 0
				&& m_sGroupsRelation.acnShunGroups[anColor[2]][STONE_NO7] != 0 )
			{
				anWhat[1] = STONE_NO4;
				anWhat[2] = STONE_NO7;
				break;
			}
			else if ( m_sGroupsRelation.acnShunGroups[anColor[2]][STONE_NO4] != 0
				&& m_sGroupsRelation.acnShunGroups[anColor[1]][STONE_NO7] != 0 ) 
			{
				anWhat[1] = STONE_NO7;
				anWhat[2] = STONE_NO4;
				break;
			}
		}
	}
	ASSERT( anColor[0] != 3 );

	for( int nGroupIndex = 0; nGroupIndex < sHuResult.cnGroups; nGroupIndex++ )
	{
		if ( sHuResult.asGroup[nGroupIndex].nGroupStyle == GROUP_STYLE_SHUN )
		{
			for( int i = 0; i < 3; i++ )
			{
				if ( sHuResult.asGroup[nGroupIndex].asStone[0].nColor == anColor[i]
					&& sHuResult.asGroup[nGroupIndex].asStone[0].nWhat == anWhat[i] && !abFound[i] )
				{
					abFound[i] = TRUE;
					sFanInfo.cnTile += GetID( sHuResult.asGroup[nGroupIndex], sFanInfo.anTileID + sFanInfo.cnTile );
				}
			}
		}
	}

	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
//40 推不倒 由牌面图形没有上下区别的牌组成的和牌，包括1234589饼、245689条、白板。不计缺一门
// 
// **************************************************************************************
int CPublicJudge::ParseTuiBuDao( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_TUIBUDAO] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_TUIBUDAO;
	sFanInfo.nScore = m_asFanInfo[FAN_TUIBUDAO].nScore;
	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
//41 三色三同顺 和牌时，有3种花色3副序数相同的顺子
// 
// **************************************************************************************
int CPublicJudge::Parse3She3TongShun( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_3SHE3TONGSHUN] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_3SHE3TONGSHUN;
	sFanInfo.nScore = m_asFanInfo[FAN_3SHE3TONGSHUN].nScore;
	int nWhat = 0;
	for(nWhat = 0; nWhat < 7; nWhat++ )
	{
		if ( m_sGroupsRelation.acnShunGroups[COLOR_WAN][nWhat] != 0
			&& m_sGroupsRelation.acnShunGroups[COLOR_TIAO][nWhat] != 0 
			&& m_sGroupsRelation.acnShunGroups[COLOR_BING][nWhat] != 0 )
		{
			break;
		}
	}
	ASSERT( nWhat != 7 );

	BOOL abFound[3] = { 0 };
	for( int nGroupIndex = 0; nGroupIndex < sHuResult.cnGroups; nGroupIndex++ )
	{
		if ( sHuResult.asGroup[nGroupIndex].nGroupStyle == GROUP_STYLE_SHUN )
		{
			int nColor = sHuResult.asGroup[nGroupIndex].asStone[0].nColor;
			if ( !abFound[nColor] && sHuResult.asGroup[nGroupIndex].asStone[0].nWhat == nWhat )
			{
				abFound[nColor] = TRUE;
				sFanInfo.cnTile += GetID( sHuResult.asGroup[nGroupIndex], sFanInfo.anTileID + sFanInfo.cnTile );
			}
		}
	}

	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
//42 三色三节高 和牌时，有3种花色3副依次递增一位数的刻子
// 
// **************************************************************************************
int CPublicJudge::Parse3She3JieGao( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_3SHEJIEJIEGAO] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_3SHEJIEJIEGAO;
	sFanInfo.nScore = m_asFanInfo[FAN_3SHEJIEJIEGAO].nScore;

	int anColor[3];
	int anWhat[3];
	// 找出只有一个刻子的花色
	for( anColor[0] = 0; anColor[0] < 3; anColor[0]++ )
	{
		if ( m_sStonesRelation.acnAllColors[anColor[0]] == 3 || m_sStonesRelation.acnAllColors[anColor[0]] == 4 )
		{
			// 要找的就是它
			break;
		}
	}
	ASSERT( anColor[0] != 3 );

	// 找出这个刻子
	for( anWhat[0] = 0; anWhat[0] <= STONE_NO9; anWhat[0]++ )
	{
		if ( m_sGroupsRelation.acnKeGroups[anColor[0]][anWhat[0]] != 0 )
		{
			break;
		}
	}
	ASSERT( anWhat[0] <= STONE_NO9 );

	// 另外两个花色
	anColor[1] = ( anColor[0] + 1 ) % 3;
	anColor[2] = ( anColor[0] + 2 ) % 3;

	// 共6种组合方式(数字表示刚找到的那个刻)
	// 1二三
	// 1三二
	// 一2三
	// 三2一
	// 一二3
	// 二一3
	int anTryWhat[3][2];
	anTryWhat[0][0] = anWhat[0] + 2 <= STONE_NO9 ? anWhat[0] + 1 : 0xff;
	anTryWhat[0][1] = anWhat[0] + 2 <= STONE_NO9 ? anWhat[0] + 2 : 0xff;
	anTryWhat[1][0] = ( anWhat[0] - 1 >= STONE_NO1 && anWhat[0] + 1 <= STONE_NO9 ) ? anWhat[0] - 1 : 0xff;
	anTryWhat[1][1] = ( anWhat[0] - 1 >= STONE_NO1 && anWhat[0] + 1 <= STONE_NO9 ) ? anWhat[0] + 1 : 0xff;
	anTryWhat[2][0] = anWhat[0] - 2 >= STONE_NO1 ? anWhat[0] - 2 : 0xff;
	anTryWhat[2][1] = anWhat[0] - 2 >= STONE_NO1 ? anWhat[0] - 1 : 0xff;
	int i = 0;
	for(i = 0; i < 3; i++ )
	{
		if ( anTryWhat[i][0] != 0xff )
		{
			if ( m_sGroupsRelation.acnKeGroups[ anColor[1] ][ anTryWhat[i][0] ] != 0
				&& m_sGroupsRelation.acnKeGroups[ anColor[2] ][ anTryWhat[i][1] ] != 0 )
			{
				anWhat[1] = anTryWhat[i][0];
				anWhat[2] = anTryWhat[i][1];
				break;
			}
			else if ( m_sGroupsRelation.acnKeGroups[ anColor[2] ][ anTryWhat[i][0] ] != 0
				&& m_sGroupsRelation.acnKeGroups[ anColor[1] ][ anTryWhat[i][1] ] != 0 )
			{	
				anWhat[1] = anTryWhat[i][1];
				anWhat[2] = anTryWhat[i][0];
				break;
			}
		}
	}
	ASSERT( i != 3 );

	// 搜索这几组牌
	BOOL abFound[3] = { 0 };
	for( int nGroupIndex = 0; nGroupIndex < sHuResult.cnGroups; nGroupIndex++ )
	{
//		if ( sHuResult.asGroup[nGroupIndex].nGroupStyle == GROUP_STYLE_KE )
		if ( IsKe( sHuResult.asGroup[nGroupIndex] ) )
		{
			for( i = 0; i < 3; i++ )
			{
				if ( !abFound[i] && sHuResult.asGroup[nGroupIndex].asStone[0].nColor == anColor[i]
					&& sHuResult.asGroup[nGroupIndex].asStone[0].nWhat == anWhat[i] )
				{
					abFound[i] = TRUE;
					sFanInfo.cnTile += GetID( sHuResult.asGroup[nGroupIndex], 
						sFanInfo.anTileID + sFanInfo.cnTile );
				}
			}
		}
	}

	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
//43 无番和 和牌后，数不出任何番种分(花牌不计算在内)
// 
// **************************************************************************************
int CPublicJudge::ParseWuFan( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_WUFAN] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_WUFAN;
	sFanInfo.nScore = m_asFanInfo[FAN_WUFAN].nScore;
	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
//44 妙手回春 自摸牌墙上最后一张牌和牌。不计自摸
// 
// **************************************************************************************
int CPublicJudge::ParseMiaoShou( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_MIAOSHOU] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_MIAOSHOU;
	sFanInfo.nScore = m_asFanInfo[FAN_MIAOSHOU].nScore;
	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
//45 海底捞月 和打出的最后一张牌
// 
// **************************************************************************************
int CPublicJudge::ParseHaiDi( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_HAIDI] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_HAIDI;
	sFanInfo.nScore = m_asFanInfo[FAN_HAIDI].nScore;
	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
//46 杠上开花 开杠抓进的牌成和牌(不包括补花)不计自摸
// 
// **************************************************************************************
int CPublicJudge::ParseGangKai( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_GANGHU] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_GANGHU;
	sFanInfo.nScore = m_asFanInfo[FAN_GANGHU].nScore;
	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
//47 抢杠和 和别人自抓开明杠的牌。不计和绝张
// 
// **************************************************************************************
int CPublicJudge::ParseQiangGang( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_QIANGGANG] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_QIANGGANG;
	sFanInfo.nScore = m_asFanInfo[FAN_QIANGGANG].nScore;
	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
//
//6 番
//48 碰碰和 由4副刻子(或杠)、将牌组成的和牌
// 
// **************************************************************************************
int CPublicJudge::ParsePenPenHu( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_PENPENHU] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_PENPENHU;
	sFanInfo.nScore = m_asFanInfo[FAN_PENPENHU].nScore;
	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
//49 混一色 由一种花色序数牌及字牌组成的和牌 
// 
// **************************************************************************************
int CPublicJudge::ParseHun1She( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_HUN1SHE] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_HUN1SHE;
	sFanInfo.nScore = m_asFanInfo[FAN_HUN1SHE].nScore;
	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
//50 三色三步高 3种花色3副依次递增一位序数的顺子
// 
// **************************************************************************************
int CPublicJudge::Parse3She3BuGao( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_3SHE3BUGAO] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_3SHE3BUGAO;
	sFanInfo.nScore = m_asFanInfo[FAN_3SHE3BUGAO].nScore;

	int anColor[3];
	int anWhat[3];
	// 找出只有一个刻子的花色
	for( anColor[0] = 0; anColor[0] < 3; anColor[0]++ )
	{
		if ( m_sStonesRelation.acnAllColors[anColor[0]] == 3 )
		{
			// 要找的就是它
			break;
		}
	}
	ASSERT( anColor[0] != 3 );
	
	// 找出这个顺子
	for( anWhat[0] = 0; anWhat[0] <= STONE_NO7; anWhat[0]++ )
	{
		if ( m_sGroupsRelation.acnShunGroups[anColor[0]][anWhat[0]] != 0 )
		{
			break;
		}
	}
	ASSERT( anWhat[0] <= STONE_NO7 );
	
	// 另外两个花色
	anColor[1] = ( anColor[0] + 1 ) % 3;
	anColor[2] = ( anColor[0] + 2 ) % 3;
	
	// 共6种组合方式(数字表示刚找到的那个刻)
	// 1二三
	// 1三二
	// 一2三
	// 三2一
	// 一二3
	// 二一3
	int anTryWhat[3][2];
	anTryWhat[0][0] = anWhat[0] + 2 <= STONE_NO7 ? anWhat[0] + 1 : 0xff;
	anTryWhat[0][1] = anWhat[0] + 2 <= STONE_NO7 ? anWhat[0] + 2 : 0xff;
	anTryWhat[1][0] = ( anWhat[0] - 1 >= STONE_NO1 && anWhat[0] + 1 <= STONE_NO7 ) ? anWhat[0] - 1 : 0xff;
	anTryWhat[1][1] = ( anWhat[0] - 1 >= STONE_NO1 && anWhat[0] + 1 <= STONE_NO7 ) ? anWhat[0] + 1 : 0xff;
	anTryWhat[2][0] = anWhat[0] - 2 >= STONE_NO1 ? anWhat[0] - 2 : 0xff;
	anTryWhat[2][1] = anWhat[0] - 2 >= STONE_NO1 ? anWhat[0] - 1 : 0xff;
	int i = 0;
	for(i = 0; i < 3; i++ )
	{
		if ( anTryWhat[i][0] != 0xff )
		{
			if ( m_sGroupsRelation.acnShunGroups[ anColor[1] ][ anTryWhat[i][0] ] != 0
				&& m_sGroupsRelation.acnShunGroups[ anColor[2] ][ anTryWhat[i][1] ] != 0 )
			{
				anWhat[1] = anTryWhat[i][0];
				anWhat[2] = anTryWhat[i][1];
				break;
			}
			else if ( m_sGroupsRelation.acnShunGroups[ anColor[2] ][ anTryWhat[i][0] ] != 0
				&& m_sGroupsRelation.acnShunGroups[ anColor[1] ][ anTryWhat[i][1] ] != 0 )
			{	
				anWhat[1] = anTryWhat[i][1];
				anWhat[2] = anTryWhat[i][0];
				break;
			}
		}
	}
	ASSERT( i != 3 );
	
	// 搜索这几组牌
	BOOL abFound[3] = { 0 };
	for( int nGroupIndex = 0; nGroupIndex < sHuResult.cnGroups; nGroupIndex++ )
	{
		if ( sHuResult.asGroup[nGroupIndex].nGroupStyle == GROUP_STYLE_SHUN )
		{
			for( i = 0; i < 3; i++ )
			{
				if ( !abFound[i] && sHuResult.asGroup[nGroupIndex].asStone[0].nColor == anColor[i]
					&& sHuResult.asGroup[nGroupIndex].asStone[0].nWhat == anWhat[i] )
				{
					abFound[i] = TRUE;
					sFanInfo.cnTile += GetID( sHuResult.asGroup[nGroupIndex], 
						sFanInfo.anTileID + sFanInfo.cnTile );
				}
			}
		}
	}


	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
//51 五门齐 和牌时3种序数牌、风、箭牌齐全
// 
// **************************************************************************************
int CPublicJudge::Parse5MenQi( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_5MENQI] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_5MENQI;
	sFanInfo.nScore = m_asFanInfo[FAN_5MENQI].nScore;
	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
//52 全求人 全靠吃牌、碰牌、单钓别人批出的牌和牌。不计单钓 
// 
// **************************************************************************************
int CPublicJudge::ParseQuanQiuRen( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_QUANQIUREN] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_QUANQIUREN;
	sFanInfo.nScore = m_asFanInfo[FAN_QUANQIUREN].nScore;
	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
//53 双暗杠 2个暗杠
// 
// **************************************************************************************
int CPublicJudge::Parse2AnGang( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_2ANGANG] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_2ANGANG;
	sFanInfo.nScore = m_asFanInfo[FAN_2ANGANG].nScore;

	for( int nGroupIndex = 0; nGroupIndex < sHuResult.cnGroups; nGroupIndex++ )
	{
		if ( sHuResult.asGroup[nGroupIndex].nGroupStyle == GROUP_STYLE_ANGANG )
		{
			sFanInfo.cnTile += GetID( sHuResult.asGroup[nGroupIndex],
				sFanInfo.anTileID + sFanInfo.cnTile );
		}
	}

	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
//54 双箭刻 2副箭刻(或杠)
// 
// **************************************************************************************
int CPublicJudge::Parse2JianKe( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_2JIANKE] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_2JIANKE;
	sFanInfo.nScore = m_asFanInfo[FAN_2JIANKE].nScore;

	for( int nGroupIndex = 0; nGroupIndex < sHuResult.cnGroups; nGroupIndex++ )
	{
		if ( IsKe( sHuResult.asGroup[nGroupIndex] ) 
			&& sHuResult.asGroup[nGroupIndex].asStone[0].nColor == COLOR_JIAN )
		{
			sFanInfo.cnTile += GetID( sHuResult.asGroup[nGroupIndex], 
				sFanInfo.anTileID + sFanInfo.cnTile );
		}
	}

	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
//
//4 番
//55 全带幺 和牌时，每副牌、将牌都有幺牌
// 
// **************************************************************************************
int CPublicJudge::ParseQuan1( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_QUANDAIYAO] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_QUANDAIYAO;
	sFanInfo.nScore = m_asFanInfo[FAN_QUANDAIYAO].nScore;
	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
//56 不求人 4副牌及将中没有吃牌、碰牌(包括明杠)，自摸和牌
// 
// **************************************************************************************
int CPublicJudge::ParseBuQiuRen( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_BUQIUREN] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_BUQIUREN;
	sFanInfo.nScore = m_asFanInfo[FAN_BUQIUREN].nScore;
	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
//57 双明杠 2个明杠
// 
// **************************************************************************************
int CPublicJudge::Parse2MinGang( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_2MINGANG] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_2MINGANG;
	sFanInfo.nScore = m_asFanInfo[FAN_2MINGANG].nScore;

	for( int nGroupIndex = 0; nGroupIndex < sHuResult.cnGroups; nGroupIndex++ )
	{
		if ( sHuResult.asGroup[nGroupIndex].nGroupStyle == GROUP_STYLE_MINGGANG )
		{
			sFanInfo.cnTile += GetID( sHuResult.asGroup[nGroupIndex],
				sFanInfo.anTileID + sFanInfo.cnTile );
		}
	}

	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
//58 和绝张 和牌池、桌面已亮明的3张牌所剩的第4张牌(抢杠和不计和绝张)
// 
// **************************************************************************************
int CPublicJudge::ParseHuJueZhang( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_HUJUEZHANG] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_HUJUEZHANG;
	sFanInfo.nScore = m_asFanInfo[FAN_HUJUEZHANG].nScore;
	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
//
//2 番
//59 箭刻 由中、发、白3张相同的牌组成的刻子
// 
// **************************************************************************************
int CPublicJudge::ParseJianKe( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_JIANKE] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_JIANKE;
	sFanInfo.nScore = m_asFanInfo[FAN_JIANKE].nScore;

	for( int nGroupIndex = 0; nGroupIndex < sHuResult.cnGroups; nGroupIndex++ )
	{
		if ( IsKe( sHuResult.asGroup[nGroupIndex] ) 
			&& sHuResult.asGroup[nGroupIndex].asStone[0].nColor == COLOR_JIAN )
		{
			sFanInfo.cnTile += GetID( sHuResult.asGroup[nGroupIndex], 
				sFanInfo.anTileID + sFanInfo.cnTile );
		}
	}

	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
//60 圈风刻 与圈风相同的风刻
// 
// **************************************************************************************
int CPublicJudge::ParseQuanFeng( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_QUANFENG] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_QUANFENG;
	sFanInfo.nScore = m_asFanInfo[FAN_QUANFENG].nScore;

	int nQuanWhat = ( sHuResult.nQuanWind & 0x00f0 ) >> 4;
	for( int nGroupIndex = 0; nGroupIndex < sHuResult.cnGroups; nGroupIndex++ )
	{
		if ( IsKe( sHuResult.asGroup[nGroupIndex] )//.nGroupStyle == GROUP_STYLE_KE 
			&& sHuResult.asGroup[nGroupIndex].asStone[0].nColor == COLOR_WIND
			&& sHuResult.asGroup[nGroupIndex].asStone[0].nWhat == nQuanWhat )
		{
			sFanInfo.cnTile += GetID( sHuResult.asGroup[nGroupIndex], 
				sFanInfo.anTileID + sFanInfo.cnTile );
		}
	}

	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
//61 门风刻 与本门风相同的风刻
// 
// **************************************************************************************
int CPublicJudge::ParseMenFeng( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_MENGFENG] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_MENGFENG;
	sFanInfo.nScore = m_asFanInfo[FAN_MENGFENG].nScore;

	int nMenWhat = ( sHuResult.nMenWind & 0x00f0 ) >> 4;
	for( int nGroupIndex = 0; nGroupIndex < sHuResult.cnGroups; nGroupIndex++ )
	{
		if ( IsKe( sHuResult.asGroup[nGroupIndex] )//.nGroupStyle == GROUP_STYLE_KE 
			&& sHuResult.asGroup[nGroupIndex].asStone[0].nColor == COLOR_WIND
			&& sHuResult.asGroup[nGroupIndex].asStone[0].nWhat == nMenWhat )
		{
			sFanInfo.cnTile += GetID( sHuResult.asGroup[nGroupIndex], 
				sFanInfo.anTileID + sFanInfo.cnTile );
		}
	}

	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
//62 门前清 没有吃、碰、明杠，和别人打出的牌
// 
// **************************************************************************************
int CPublicJudge::ParseMenQing( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_MENGQING] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_MENGQING;
	sFanInfo.nScore = m_asFanInfo[FAN_MENGQING].nScore;
	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
//63 平和 由4副顺子及序数牌作将组成的和牌，边、坎、钓不影响平和
// 
// **************************************************************************************
int CPublicJudge::ParsePinHu( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_PINHU] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_PINHU;
	sFanInfo.nScore = m_asFanInfo[FAN_PINHU].nScore;
	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
//64 四归一 和牌中，有4张相同的牌归于一家的顺、刻子、对、将牌中(不包括杠牌)
// 
// **************************************************************************************
int CPublicJudge::Parse4Gui1( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_4GUI1] > 0 );
	FANINFO sFanInfo = { 0 };

	// 可能会有多个四归一
	// 先找出所有有相同四张的牌
	int anLineIndex[4];	// 最多只可能有4杠
	int cnSame4Tile = 0;
	int i = 0;
	for(i = 0; i < 34; i++ )
	{
		if ( m_sStonesRelation.acnAllStones[i] >= 4 )
		{
			ASSERT( cnSame4Tile < 4 );
			anLineIndex[cnSame4Tile] = i;
			cnSame4Tile++;
		}
	}

	if ( sHuResult.cnGroups == 14 )
	{
		// 无分组的牌型
		for( i = 0; i < cnSame4Tile; i++ )
		{
			memset( &sFanInfo, 0, sizeof( FANINFO ) );
			sFanInfo.nID = FAN_4GUI1;
			sFanInfo.nScore = m_asFanInfo[FAN_4GUI1].nScore;

			for( int j = 0; j < 14; j++ )
			{
				if ( GetValue( sHuResult.asHandStone[j] ) == anLineIndex[i] ) 
				{
					sFanInfo.anTileID[sFanInfo.cnTile] = sHuResult.asHandStone[j].nID;
					sFanInfo.cnTile++;
				}
			}

			ASSERT( sFanInfo.cnTile >= 4 );
			vsFanInfo.push_back( sFanInfo );
		}
	}
	else
	{
		for( i = 0; i < cnSame4Tile; i++ )
		{
			memset( &sFanInfo, 0, sizeof( FANINFO ) );
			sFanInfo.nID = FAN_4GUI1;
			sFanInfo.nScore = m_asFanInfo[FAN_4GUI1].nScore;

			for( int nGroupIndex = 0; nGroupIndex < sHuResult.cnGroups; nGroupIndex++ )
			{
				if ( sHuResult.asGroup[nGroupIndex].nGroupStyle == GROUP_STYLE_MINGGANG
					|| sHuResult.asGroup[nGroupIndex].nGroupStyle == GROUP_STYLE_ANGANG )
				{
					// 杠牌是不能算四归一的
					continue;
				}

				int cnTile = 3;
				if ( sHuResult.asGroup[nGroupIndex].nGroupStyle == GROUP_STYLE_JONG )
				{
					cnTile = 2;
				}
				for( int nTileIndex = 0; nTileIndex < cnTile; nTileIndex++ )
				{
					if ( GetValue( sHuResult.asGroup[nGroupIndex].asStone[nTileIndex] )
						== anLineIndex[i] )
					{
						sFanInfo.anTileID[sFanInfo.cnTile] = sHuResult.asGroup[nGroupIndex].asStone[nTileIndex].nID;
						sFanInfo.cnTile++;
					}
				}
			}

			if ( sFanInfo.cnTile != 0 )
			{
				vsFanInfo.push_back( sFanInfo );
			}
		}
	}

	return sHuResult.anFans[FAN_4GUI1];
}

// **************************************************************************************
// 
//65 双同刻 2副序数相同的刻子 
// 
// **************************************************************************************
int CPublicJudge::Parse2TongKe( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_2TONGKE] > 0 );
	FANINFO sFanInfo = { 0 };

	// 可能会有多个双同刻
	int anWhat[2];// 最多两个
	int cnTongKe = 0;
	int i = 0;
	for(i = 0; i <= STONE_NO9; i++ )
	{
		if ( m_sGroupsRelation.acnKeGroups[COLOR_WAN][i] 
			+ m_sGroupsRelation.acnKeGroups[COLOR_BING][i] 
			+ m_sGroupsRelation.acnKeGroups[COLOR_TIAO][i] >= 2 ) 
		{
			ASSERT( cnTongKe < 2 );
			anWhat[cnTongKe] = i;
			cnTongKe++;
		}
	}
	ASSERT( cnTongKe == sHuResult.anFans[FAN_2TONGKE] );

	for( i = 0; i < cnTongKe; i++ )
	{
		memset( &sFanInfo, 0, sizeof( FANINFO ) );
		sFanInfo.nID = FAN_2TONGKE;
		sFanInfo.nScore = m_asFanInfo[FAN_2TONGKE].nScore;

		for( int nGroupIndex = 0; nGroupIndex < sHuResult.cnGroups; nGroupIndex++ )
		{
			if ( IsKe( sHuResult.asGroup[nGroupIndex] )// .nGroupStyle == GROUP_STYLE_KE
				&& sHuResult.asGroup[nGroupIndex].asStone[0].nColor < COLOR_WIND
				&& sHuResult.asGroup[nGroupIndex].asStone[0].nWhat == anWhat[i] )
			{
				sFanInfo.cnTile += GetID( sHuResult.asGroup[nGroupIndex],
					sFanInfo.anTileID + sFanInfo.cnTile );
			}
		}
		vsFanInfo.push_back( sFanInfo );
	}
	
	return sHuResult.anFans[FAN_2TONGKE];
}

// **************************************************************************************
// 
//66 双暗刻 2个暗刻
// 
// **************************************************************************************
int CPublicJudge::Parse2AnKe( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_2ANKE] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_2ANKE;
	sFanInfo.nScore = m_asFanInfo[FAN_2ANKE].nScore;

	// 分组的排列顺序是吃碰杠的牌在前，将牌在最后
	for( int nGroupIndex = 0; nGroupIndex < sHuResult.cnGroups; nGroupIndex++ )
	{
		if ( nGroupIndex < sHuResult.cnShowGroups )
		{
			// 在吃碰杠的分组里只找暗杠
			if ( sHuResult.asGroup[nGroupIndex].nGroupStyle == GROUP_STYLE_ANGANG )
			{
				sFanInfo.cnTile += GetID( sHuResult.asGroup[nGroupIndex], sFanInfo.anTileID + sFanInfo.cnTile );
			}
		}
		else if ( sHuResult.asGroup[nGroupIndex].nGroupStyle == GROUP_STYLE_KE )
		{
			// 如果不是自摸的，除了和的张牌所在的刻，其余的刻都拷进去
			if ( ( sHuResult.nWinMode & WIN_MODE_ZIMO ) == 0
				&& ( sHuResult.asGroup[nGroupIndex].asStone[0].nID == sHuResult.nHuTileID 
				|| sHuResult.asGroup[nGroupIndex].asStone[1].nID == sHuResult.nHuTileID
				|| sHuResult.asGroup[nGroupIndex].asStone[2].nID == sHuResult.nHuTileID ) )
			{
				continue;
			}
			sFanInfo.cnTile += GetID( sHuResult.asGroup[nGroupIndex], sFanInfo.anTileID + sFanInfo.cnTile );
		}
	}
	vsFanInfo.push_back( sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
//67 暗杠 自抓4张相同的牌开杠
// 
// **************************************************************************************
int CPublicJudge::ParseAnGang( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_ANGANG] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_ANGANG;
	sFanInfo.nScore = m_asFanInfo[FAN_ANGANG].nScore;

	for( int nGroupIndex = 0; nGroupIndex < sHuResult.cnShowGroups; nGroupIndex++ )
	{
		if ( sHuResult.asGroup[nGroupIndex].nGroupStyle == GROUP_STYLE_ANGANG )
		{
			sFanInfo.cnTile += GetID( sHuResult.asGroup[nGroupIndex], 
				sFanInfo.anTileID + sFanInfo.cnTile );
		}
	}
	vsFanInfo.push_back( sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
//68 断幺 和牌中没有一、九及字牌 
// 
// **************************************************************************************
int CPublicJudge::ParseDuan19( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_DUAN19] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_DUAN19;
	sFanInfo.nScore = m_asFanInfo[FAN_DUAN19].nScore;
	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
// 1 番
//69 一般高 由一种花色2副相同的顺子组成的牌 
// 
// **************************************************************************************
int CPublicJudge::ParseYiBanGao( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_YIBANGAO] > 0 );
	FANINFO sFanInfo = { 0 };

	// 可能会有多个一般高
	int cnSame2Shun = 0;
	int anColor[2];
	int anWhat[2];		// 最多两个
	int i = 0;
	for(i = 0; i < 3; i++ )
	{
		for( int j = 0; j < 7; j++ )
		{
			if ( m_sGroupsRelation.acnShunGroups[i][j] == 2 )
			{
				ASSERT( cnSame2Shun < 2 );
				anColor[cnSame2Shun] = i;
				anWhat[cnSame2Shun] = j;
				cnSame2Shun++;
			}
		}
	}
	ASSERT( cnSame2Shun == sHuResult.anFans[FAN_YIBANGAO] );

	for( i = 0; i < cnSame2Shun; i++ )
	{
		memset( &sFanInfo, 0, sizeof( FANINFO ) );
		sFanInfo.nID = FAN_YIBANGAO;
		sFanInfo.nScore = m_asFanInfo[FAN_YIBANGAO].nScore;
		
		for( int nGroupIndex = 0; nGroupIndex < sHuResult.cnGroups; nGroupIndex++ )
		{
			if ( sHuResult.asGroup[nGroupIndex].nGroupStyle == GROUP_STYLE_SHUN 
				&& sHuResult.asGroup[nGroupIndex].asStone[0].nColor == anColor[i]
				&& sHuResult.asGroup[nGroupIndex].asStone[0].nWhat == anWhat[i] )
			{
				sFanInfo.cnTile += GetID( sHuResult.asGroup[nGroupIndex], 
					sFanInfo.anTileID + sFanInfo.cnTile );
			}
		}

		vsFanInfo.push_back( sFanInfo );
	}

	return sHuResult.anFans[FAN_YIBANGAO];
}

// **************************************************************************************
// 
//70 喜相逢 2种花色2副序数相同的顺子
// 
// **************************************************************************************
int CPublicJudge::ParseXiXiangFeng( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_XIXIANGFENG] > 0 );
	FANINFO sFanInfo = { 0 };

	// 可能会有多个喜相逢
	int cnXiXiangFeng = 0;
	int anWhat[2];	// 最多两个喜相逢
	int i = 0;
	for(i = 0; i <= STONE_NO7; i++ )
	{
		if ( ( ( m_sGroupsRelation.acnShunGroups[COLOR_WAN][i] != 0 ? 1 : 0 )
			+ ( m_sGroupsRelation.acnShunGroups[COLOR_TIAO][i] != 0 ? 1 : 0 )
			+ ( m_sGroupsRelation.acnShunGroups[COLOR_BING][i] != 0 ? 1 : 0 ) ) == 2 ) 
		{
			ASSERT( cnXiXiangFeng < 2 );
			anWhat[cnXiXiangFeng] = i;
			cnXiXiangFeng++;
		}
	}
	ASSERT( cnXiXiangFeng == sHuResult.anFans[FAN_XIXIANGFENG] );

	for( i = 0; i < cnXiXiangFeng; i++ )
	{
		memset( &sFanInfo, 0, sizeof( FANINFO ) );
		sFanInfo.nID = FAN_XIXIANGFENG;
		sFanInfo.nScore = m_asFanInfo[FAN_XIXIANGFENG].nScore;
		
		BOOL abFound[3] = { 0 };
		for( int nGroupIndex = 0; nGroupIndex < sHuResult.cnGroups; nGroupIndex++ )
		{
			if ( sHuResult.asGroup[nGroupIndex].nGroupStyle == GROUP_STYLE_SHUN 
				&& sHuResult.asGroup[nGroupIndex].asStone[0].nWhat == anWhat[i] )
			{
				int nColor = sHuResult.asGroup[nGroupIndex].asStone[0].nColor;
				if ( abFound[nColor] )
				{
					// 这种花色已经有一顺了
					continue;
				}
				abFound[nColor] = TRUE;
				sFanInfo.cnTile += GetID( sHuResult.asGroup[nGroupIndex], 
					sFanInfo.anTileID + sFanInfo.cnTile );
			}
		}
		
		vsFanInfo.push_back( sFanInfo );
	}

	
	return sHuResult.anFans[FAN_XIXIANGFENG];
}

// **************************************************************************************
// 
//71 连六 一种花色6张相连接的序数牌
// 
// **************************************************************************************
int CPublicJudge::ParseLian6( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_LIAN6] > 0 );
	FANINFO sFanInfo = { 0 };

	// 可能会有2个连6
	int cnLian6 = 0;
	int anColor[2];
	int anWhat[2];
	int i = 0;
	for(i = 0; i < 3; i++ )
	{
		for( int j = 0; j <= STONE_NO4; j++ )
		{
			// 第一顺最大只能是456
			if ( m_sGroupsRelation.acnShunGroups[i][j] != 0
				&& m_sGroupsRelation.acnShunGroups[i][j + 3] != 0 )
			{
				ASSERT( cnLian6 < 2 );
				anColor[cnLian6] = i;
				anWhat[cnLian6] = j;
				cnLian6++;
			}
		}
	}
	ASSERT( cnLian6 == sHuResult.anFans[FAN_LIAN6] );

	for( i = 0; i < cnLian6; i++ )
	{
		memset( &sFanInfo, 0, sizeof( FANINFO ) );
		sFanInfo.nID = FAN_LIAN6;
		sFanInfo.nScore = m_asFanInfo[FAN_LIAN6].nScore;

		BOOL abFound[2];
		abFound[0] = abFound[1] = FALSE;
		for( int nGroupIndex = 0; nGroupIndex < sHuResult.cnGroups; nGroupIndex++ )
		{
			if ( sHuResult.asGroup[nGroupIndex].nGroupStyle == GROUP_STYLE_SHUN 
				&& sHuResult.asGroup[nGroupIndex].asStone[0].nColor == anColor[i] )
			{
				if ( sHuResult.asGroup[nGroupIndex].asStone[0].nWhat == anWhat[i]
					&& !abFound[0] )
				{
					abFound[0] = TRUE;
					sFanInfo.cnTile += GetID( sHuResult.asGroup[nGroupIndex], 
						sFanInfo.anTileID + sFanInfo.cnTile );
				}
				else if ( sHuResult.asGroup[nGroupIndex].asStone[0].nWhat == anWhat[i] + 3
					&& !abFound[1] ) 
				{
					abFound[1] = TRUE;
					sFanInfo.cnTile += GetID( sHuResult.asGroup[nGroupIndex], 
						sFanInfo.anTileID + sFanInfo.cnTile );
				}
			}
		}
		ASSERT( abFound[0] == TRUE && abFound[1] == TRUE );

		vsFanInfo.push_back( sFanInfo );
	}
	
	return sHuResult.anFans[FAN_LIAN6];
}

// **************************************************************************************
// 
//72 老少副 一种花色牌的123、789两副顺子
// 
// **************************************************************************************
int CPublicJudge::ParseLaoShaoFu( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_LAOSHAOFU] > 0 );
	FANINFO sFanInfo = { 0 };

	// 可能会有两个老少副
	int cnLaoShaoFu = 0;
	int anColor[2];
	int i = 0;
	for(i = 0; i < 3; i++ )
	{
		if ( m_sGroupsRelation.acnShunGroups[i][STONE_NO1] != 0
			&& m_sGroupsRelation.acnShunGroups[i][STONE_NO7] != 0 )
		{
			ASSERT( cnLaoShaoFu < 2 );
			anColor[cnLaoShaoFu] = i;
			cnLaoShaoFu++;
		}
	}
	ASSERT( cnLaoShaoFu == sHuResult.anFans[FAN_LAOSHAOFU] );
	
	for( i = 0; i < cnLaoShaoFu; i++ )
	{
		memset( &sFanInfo, 0, sizeof( FANINFO ) );
		sFanInfo.nID = FAN_LAOSHAOFU;
		sFanInfo.nScore = m_asFanInfo[FAN_LAOSHAOFU].nScore;

		BOOL abFound[2];
		abFound[0] = abFound[1] = FALSE;
		for( int nGroupIndex = 0; nGroupIndex < sHuResult.cnGroups; nGroupIndex++ )
		{
			if ( sHuResult.asGroup[nGroupIndex].nGroupStyle == GROUP_STYLE_SHUN 
				&& sHuResult.asGroup[nGroupIndex].asStone[0].nColor == anColor[i] )
			{
				if ( sHuResult.asGroup[nGroupIndex].asStone[0].nWhat == STONE_NO1
					&& !abFound[0] )		
				{
					abFound[0] = TRUE;
					sFanInfo.cnTile += GetID( sHuResult.asGroup[nGroupIndex], 
						sFanInfo.anTileID + sFanInfo.cnTile );
				}
				else if ( sHuResult.asGroup[nGroupIndex].asStone[0].nWhat == STONE_NO7
					&& !abFound[1] ) 
				{
					abFound[1] = TRUE;
					sFanInfo.cnTile += GetID( sHuResult.asGroup[nGroupIndex], 
						sFanInfo.anTileID + sFanInfo.cnTile );
				}
			}
		}
		ASSERT( abFound[0] == TRUE && abFound[1] == TRUE );

		vsFanInfo.push_back( sFanInfo );
	}

	return sHuResult.anFans[FAN_LAOSHAOFU];
}

// **************************************************************************************
// 
//73 幺九刻 3张相同的一、九序数牌及字牌组成的刻子(或杠)
// 风刻有三个或以上以则所有风刻不计幺九刻
// 与圈风或门风相同的风刻不计幺九刻
// 箭牌不计幺九刻
// 
// **************************************************************************************
int CPublicJudge::Parse19Ke( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_19KE] > 0 );
	FANINFO sFanInfo = { 0 };

	// 可能会有多个幺九刻
	for( int nGroupIndex = 0; nGroupIndex < sHuResult.cnGroups; nGroupIndex++ )
	{
		if ( !Is19Ke( sHuResult.asGroup[nGroupIndex] ) )
		{
			continue;
		}

		// 是19刻，看看其它的限制条件
		if ( sHuResult.asGroup[nGroupIndex].asStone[0].nColor == COLOR_WIND )
		{
			// 是风牌
			if ( sHuResult.anFans[FAN_4FANGDAFA] != 0 || sHuResult.anFans[FAN_DA4XI] != 0 
				|| sHuResult.anFans[FAN_XIAO4XI] != 0 || sHuResult.anFans[FAN_3FENGKE] != 0 ) 
			{
				// 有三个以上的风刻，该刻不计幺九刻
				continue;
			}
			int nWhat = sHuResult.asGroup[nGroupIndex].asStone[0].nWhat;
			if ( nWhat == ( sHuResult.nQuanWind & 0x00f0 ) >> 4
				|| nWhat == ( sHuResult.nMenWind & 0x00f0 ) >> 4 ) 
			{
				// 这个刻已计了圈风刻或门风刻，也不计幺九刻
				continue;
			}
		}

		memset( &sFanInfo, 0, sizeof( FANINFO ) );
		sFanInfo.nID = FAN_19KE;
		sFanInfo.nScore = m_asFanInfo[FAN_19KE].nScore;
		sFanInfo.cnTile += GetID( sHuResult.asGroup[nGroupIndex], sFanInfo.anTileID + sFanInfo.cnTile );

		vsFanInfo.push_back( sFanInfo );
	}

	return sHuResult.anFans[FAN_19KE];
}

// **************************************************************************************
// 
//74 明杠 自己有暗刻，碰别人打出的一张相同的牌开杠：或自己抓进一张与碰的明刻相同的牌开杠
// 
// **************************************************************************************
int CPublicJudge::ParseMinGang( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_MINGANG] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_MINGANG; 
	sFanInfo.nScore = m_asFanInfo[FAN_MINGANG].nScore;

	for( int nGroupIndex = 0; nGroupIndex < sHuResult.cnGroups; nGroupIndex++ )
	{
		if ( sHuResult.asGroup[nGroupIndex].nGroupStyle == GROUP_STYLE_MINGGANG )
		{
			sFanInfo.cnTile += GetID( sHuResult.asGroup[nGroupIndex], 
				sFanInfo.anTileID + sFanInfo.cnTile );
			break;	// 只能有一个明杠
		}
	}

	vsFanInfo.push_back( sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
//75 缺一门 和牌中缺少一种花色序数牌
// 
// **************************************************************************************
int CPublicJudge::ParseQue1Meng( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_QUE1MEN] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_QUE1MEN;
	sFanInfo.nScore = m_asFanInfo[FAN_QUE1MEN].nScore;
	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
//76 无字 和牌中没有风、箭牌
// 
// **************************************************************************************
int CPublicJudge::ParseWuZi( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_WUZI] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_WUZI;
	sFanInfo.nScore = m_asFanInfo[FAN_WUZI].nScore;
	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
//77 边张 单和123的3及789的7或1233和3、77879和7都为边张。手中有12345和3，56789和7不算边张
// 
// **************************************************************************************
int CPublicJudge::ParseBianZang( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_BIANZANG] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nScore = m_asFanInfo[FAN_BIANZANG].nScore;
	sFanInfo.nID = FAN_BIANZANG;
	sFanInfo.anTileID[0] = sHuResult.nHuTileID;
	sFanInfo.cnTile = 1;	
	vsFanInfo.push_back( sFanInfo );
	
	return 1;

/*
	// 和的那张牌有可能被用作别的用途了，所以这里要找到一张与和的那张牌花色点数相同的牌,
	// 且这张牌是被用作边张了
	
	// 先找到和的那张牌
	int nHuColor;// = m_pCheckParam->asHandStone[0].nColor;
	int nHuWhat;// = m_pCheckParam->asHandStone[0].nWhat;
	
	if ( m_pRule->IsHun( sHuResult.nHuTileID ) ) 
	{
		// 如果最后和的那张是混牌，还要到分组里去搜索，看它变成什么牌了
		GetUsedHunInfo( sHuResult.nHuTileID, sHuResult.asGroup, nHuColor, nHuWhat );
	}
	else
	{
		nHuColor = ( sHuResult.nHuTileID & 0x0f00 ) >> 8;
		nHuWhat = ( sHuResult.nHuTileID & 0x00f0 ) >> 4;
	}
	int nCheckTile;
	if ( nHuWhat == STONE_NO3 )
	{
		// 第3张
		nCheckTile = 2;
	}
	else if ( nHuWhat == STONE_NO7 )
	{
		// 第1张
		nCheckTile = 0;
	}
	else
	{
		ASSERT( FALSE );
	}

	// 看看那张牌是被用作边张了
	for( int nGroupIndex = 0; nGroupIndex < sHuResult.cnGroups; nGroupIndex++ )
	{
		if ( sHuResult.asGroup[nGroupIndex].nGroupStyle == GROUP_STYLE_SHUN 
			&& sHuResult.asGroup[nGroupIndex].asStone[nCheckTile].nColor == nHuColor
			&& sHuResult.asGroup[nGroupIndex].asStone[nCheckTile].nWhat == nHuWhat )
		{
			sFanInfo.anTileID[0] = sHuResult.asGroup[nGroupIndex].asStone[nCheckTile].nID;
			break;
		}
	}
	ASSERT( nGroupIndex != sHuResult.cnGroups );
	sFanInfo.cnTile = 1;	
	vsFanInfo.push_back( sFanInfo );

	return 1;
*/
}

// **************************************************************************************
// 
//78 坎张 和2张牌之间的牌。4556和5也为坎张，手中有45567和6不算坎张
// 
// **************************************************************************************
int CPublicJudge::ParseKanZang( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_KANZANG] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_KANZANG;
	sFanInfo.nScore = m_asFanInfo[FAN_KANZANG].nScore;
	sFanInfo.anTileID[0] = sHuResult.nHuTileID;
	sFanInfo.cnTile = 1;	
	vsFanInfo.push_back( sFanInfo );
	
	return 1;

/*
	// 和的那张牌有可能被用作别的用途了，所以这里要找到一张与和的那张牌花色点数相同的牌,
	// 且这张牌是被用作坎张了
	// 先找到和的那张牌
	int nHuColor;// = m_pCheckParam->asHandStone[0].nColor;
	int nHuWhat;// = m_pCheckParam->asHandStone[0].nWhat;
	if ( m_pRule->IsHun( sHuResult.nHuTileID ) ) 
	{
		// 如果最后和的那张是混牌，还要到分组里去搜索，看它变成什么牌了
		GetUsedHunInfo( sHuResult.nHuTileID, sHuResult.asGroup, nHuColor, nHuWhat );
	}
	else
	{
		nHuColor = ( sHuResult.nHuTileID & 0x0f00 ) >> 8;
		nHuWhat = ( sHuResult.nHuTileID & 0x00f0 ) >> 4;
	}
	// 看看那张牌是被用作坎张了
	for( int nGroupIndex = 0; nGroupIndex < sHuResult.cnGroups; nGroupIndex++ )
	{
		if ( sHuResult.asGroup[nGroupIndex].nGroupStyle == GROUP_STYLE_SHUN 
			&& sHuResult.asGroup[nGroupIndex].asStone[1].nColor == nHuColor
			&& sHuResult.asGroup[nGroupIndex].asStone[1].nWhat == nHuWhat )
		{
			sFanInfo.anTileID[0] = sHuResult.asGroup[nGroupIndex].asStone[1].nID;
			break;
		}
	}
	ASSERT( nGroupIndex != sHuResult.cnGroups );

	sFanInfo.cnTile = 1;
	vsFanInfo.push_back( sFanInfo );
	
	return 1;
*/
}

// **************************************************************************************
// 
//79 单钓将 钓单张牌作将成和
// 
// **************************************************************************************
int CPublicJudge::ParseDanDiao( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_DANDIAO] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_DANDIAO;
	sFanInfo.nScore = m_asFanInfo[FAN_DANDIAO].nScore;
	sFanInfo.anTileID[0] = sHuResult.nHuTileID;
	sFanInfo.cnTile = 1;	
	vsFanInfo.push_back( sFanInfo );
	
	return 1;

/*
	// 最后一组是将牌
	sFanInfo.anTileID[0] = sHuResult.asGroup[sHuResult.cnGroups - 1].asStone[0].nID;
	sFanInfo.cnTile = 1;
	vsFanInfo.push_back( sFanInfo );
	
	return 1;
	*/
}

// **************************************************************************************
// 
//80 自摸 自己抓进牌成和牌
// 
// **************************************************************************************
int CPublicJudge::ParseZiMo( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_ZIMO] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_ZIMO;
	sFanInfo.nScore = m_asFanInfo[FAN_ZIMO].nScore;
	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
// 81 花牌 即春夏秋冬，梅兰竹菊，每花计一分。不计在起和分内，和牌后才能计分。
// 花牌补花成和计自摸分，不计杠上开花
// 
// **************************************************************************************
int CPublicJudge::ParseHua( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_FLOWER] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_FLOWER;
	sFanInfo.nScore = m_asFanInfo[FAN_FLOWER].nScore * sHuResult.cnFlower;
	for( int i = 0; i < sHuResult.cnFlower; i++ )
	{
		sFanInfo.anTileID[i] = sHuResult.asFlower[i].nID;
	}
	sFanInfo.cnTile = sHuResult.cnFlower;
	//vsFanInfo.push_back( sFanInfo );
	InsertFanInfo( vsFanInfo, sFanInfo );

	return sHuResult.anFans[FAN_FLOWER];
}

// 以下是大众麻将的番种
// 对大众麻将的番种要使用插入排序插入到正确的位置
// 168番
// **************************************************************************************
// 
// 82 四方大发财
// 
// **************************************************************************************
int CPublicJudge::Parse4FangDaFa( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_4FANGDAFA] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_4FANGDAFA;
	sFanInfo.nScore = m_asFanInfo[FAN_4FANGDAFA].nScore;

	InsertFanInfo( vsFanInfo, sFanInfo );

	return 1;
}

// **************************************************************************************
// 
// 83 天和
// 
// **************************************************************************************
int CPublicJudge::ParseTianHu( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_TIANHU] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_TIANHU;
	sFanInfo.nScore = m_asFanInfo[FAN_TIANHU].nScore;

	InsertFanInfo( vsFanInfo, sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
// 158番
// 84 地和 
// 
// **************************************************************************************
int CPublicJudge::ParseDiHu( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_DIHU] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_DIHU;
	sFanInfo.nScore = m_asFanInfo[FAN_DIHU].nScore;

	InsertFanInfo( vsFanInfo, sFanInfo );

	return 1;
}

// 108番
// **************************************************************************************
// 
// 85 人和 
// 
// **************************************************************************************
int CPublicJudge::ParseRenHu( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_RENHU] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_RENHU;
	sFanInfo.nScore = m_asFanInfo[FAN_RENHU].nScore;

	InsertFanInfo( vsFanInfo, sFanInfo );

	return 1;
}

// **************************************************************************************
// 
// 87 八仙过海，手上有八张花牌，不计春夏秋冬，梅兰竹菊
// 
// **************************************************************************************
int CPublicJudge::Parse8Xian( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_8XIANGUOHAI] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_8XIANGUOHAI;
	sFanInfo.nScore = m_asFanInfo[FAN_8XIANGUOHAI].nScore;

	InsertFanInfo( vsFanInfo, sFanInfo );

	return 1;
}

// 32番
// **************************************************************************************
// 
// 88 七抢一，手上有七张花牌和牌，另一家手上有一张花牌，不计春夏秋冬，梅兰竹菊
// 
// **************************************************************************************
int CPublicJudge::Parse7Qiang1( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_7QIANG1] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_7QIANG1;
	sFanInfo.nScore = m_asFanInfo[FAN_7QIANG1].nScore;
	
	InsertFanInfo( vsFanInfo, sFanInfo );

	return 1;
}

// **************************************************************************************
// 
// 89 天听
// 
// **************************************************************************************
int CPublicJudge::ParseTianTing( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_TIANTING] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_TIANTING;
	sFanInfo.nScore = m_asFanInfo[FAN_TIANTING].nScore;
	
	InsertFanInfo( vsFanInfo, sFanInfo );

	return 1;
}

// **************************************************************************************
// 
// 96 立直
// 
// **************************************************************************************
int CPublicJudge::ParseLiZhi( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_LIZHI] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_LIZHI;
	sFanInfo.nScore = m_asFanInfo[FAN_LIZHI].nScore;
	InsertFanInfo( vsFanInfo, sFanInfo );

	return 1;
}

// 1番
// **************************************************************************************
// 
// 97 二五八将
// 
// **************************************************************************************
int CPublicJudge::Parse258Jong( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_258JONG] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_258JONG;
	sFanInfo.nScore = m_asFanInfo[FAN_258JONG].nScore;

	for( int nGroupIndex = 0; nGroupIndex < sHuResult.cnGroups; nGroupIndex++ )
	{
		if ( sHuResult.asGroup[nGroupIndex].nGroupStyle == GROUP_STYLE_JONG )
		{
			sFanInfo.cnTile += GetID( sHuResult.asGroup[nGroupIndex], 
				sFanInfo.anTileID + sFanInfo.cnTile );
			break;
		}
	}

	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
// 98 梅兰竹菊，集齐这四张花牌之后额外加分
// 
// **************************************************************************************
int CPublicJudge::Parse4Flower( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_4FLOWER] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_4FLOWER;
	sFanInfo.nScore = m_asFanInfo[FAN_4FLOWER].nScore;

	ASSERT( sHuResult.cnFlower >= 4 );
	for( int i = 0; i < sHuResult.cnFlower; i++ )
	{
		if ( sHuResult.asFlower[i].nColor == COLOR_FLOWER )
		{
			sFanInfo.anTileID[sFanInfo.cnTile] = sHuResult.asFlower[i].nID;
			sFanInfo.cnTile++;
		}
	}
	ASSERT( sFanInfo.cnTile == 4 );

	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
// 99 春夏秋冬，集齐这四张花牌之后额外加分
// 
// **************************************************************************************
int CPublicJudge::Parse4Season( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_4SEASON] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_4SEASON;
	sFanInfo.nScore = m_asFanInfo[FAN_4SEASON].nScore;
	
	ASSERT( sHuResult.cnFlower >= 4 );
	for( int i = 0; i < sHuResult.cnFlower; i++ )
	{
		if ( sHuResult.asFlower[i].nColor == COLOR_SEASON )
		{
			sFanInfo.anTileID[sFanInfo.cnTile] = sHuResult.asFlower[i].nID;
			sFanInfo.cnTile++;
		}
	}
	ASSERT( sFanInfo.cnTile == 4 );
	
	vsFanInfo.push_back( sFanInfo );
	return 1;
}

// **************************************************************************************
// 
// 100 季花
// 
// **************************************************************************************
int CPublicJudge::ParseJiHua( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_SEASONFLOWER] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_SEASONFLOWER;
	sFanInfo.nScore = m_asFanInfo[FAN_SEASONFLOWER].nScore * sHuResult.anFans[FAN_SEASONFLOWER];

	for( int i = 0; i < sHuResult.cnFlower; i++ )
	{
		if ( sHuResult.asFlower[i].nWhat == ( ( sHuResult.nMenWind & 0x00f0 ) >> 4 ) )
		{
			sFanInfo.anTileID[sFanInfo.cnTile] = sHuResult.asFlower[i].nID;
			sFanInfo.cnTile++;
		}
	}
	ASSERT( sFanInfo.cnTile == sHuResult.anFans[FAN_SEASONFLOWER] );

	InsertFanInfo( vsFanInfo, sFanInfo );

	return sHuResult.anFans[FAN_SEASONFLOWER];
}

// **************************************************************************************
// 
// 101 么九头 由序数牌的19做将牌
// 
// **************************************************************************************
int CPublicJudge::Parse19Jong( HURESULT &sHuResult, FANINFOVECTOR &vsFanInfo )
{
	ASSERT( sHuResult.anFans[FAN_19JONG] > 0 );
	FANINFO sFanInfo = { 0 };
	sFanInfo.nID = FAN_19JONG;
	sFanInfo.nScore = m_asFanInfo[FAN_19JONG].nScore;

	for( int nGroupIndex = 0; nGroupIndex < sHuResult.cnGroups; nGroupIndex++ )
	{
		if ( sHuResult.asGroup[nGroupIndex].nGroupStyle == GROUP_STYLE_JONG )
		{
			sFanInfo.cnTile += GetID( sHuResult.asGroup[nGroupIndex], 
				sFanInfo.anTileID + sFanInfo.cnTile );
			break;
		}
	}

	vsFanInfo.push_back( sFanInfo );
	
	return 1;
}

// **************************************************************************************
// 
// 获得一组牌的ID
// 
// **************************************************************************************
int CPublicJudge::GetID( MAHJONGGROUPTILE &sGroup, int anTileID[] )
{
	int i = 0;
	for(i = 0; i < 4; i++ )
	{
		if ( sGroup.asStone[i].nID != 0 )
		{
			anTileID[i] = sGroup.asStone[i].nID;
		}
		else
		{
			break;
		}
	}

	return i;
}

// **************************************************************************************
// 
// 是不是刻
// 
// **************************************************************************************
BOOL CPublicJudge::IsKe( MAHJONGGROUPTILE &sGroup )
{
	if ( sGroup.nGroupStyle == GROUP_STYLE_KE 
		|| sGroup.nGroupStyle == GROUP_STYLE_MINGGANG 
		|| sGroup.nGroupStyle == GROUP_STYLE_ANGANG )
	{
		return TRUE;
	}

	return FALSE;
}

// **************************************************************************************
// 
// 是不是幺九刻
// 
// **************************************************************************************
BOOL CPublicJudge::Is19Ke( MAHJONGGROUPTILE &sGroup )
{
	if ( !IsKe( sGroup ) || sGroup.asStone[0].nColor == COLOR_JIAN )
	{
		return FALSE;
	}
	
	if ( sGroup.asStone[0].nColor == COLOR_WIND 
		|| sGroup.asStone[0].nColor >= COLOR_WAN && sGroup.asStone[0].nColor <= COLOR_BING 
		&& ( sGroup.asStone[0].nWhat == STONE_NO1 || sGroup.asStone[0].nWhat == STONE_NO9 ) )
	{
		return TRUE;
	}

	return FALSE;
}

// **************************************************************************************
// 
// 将一个番种信息插入到正确的位置
// 
// **************************************************************************************
void CPublicJudge::InsertFanInfo( FANINFOVECTOR &vsFanInfo, FANINFO &sFanInfo )
{
	FANINFOVECTOR::iterator Iter;
	for( Iter = vsFanInfo.begin(); Iter != vsFanInfo.end(); Iter++ )
	{
		if ( ( *Iter ).nScore < sFanInfo.nScore )
		{
			vsFanInfo.insert( Iter, sFanInfo );
			break;
		}
	}
	if ( Iter == vsFanInfo.end() )
	{
		// 一直到最后都没找到合适的位置，加到最后
		vsFanInfo.push_back( sFanInfo );
	}
}

// **************************************************************************************
// 
// 是不是顺
// 
// **************************************************************************************
BOOL CPublicJudge::IsShun( MAHJONGTILE asTile[] )
{

	for( int i = 0; i < 3; i++ )
	{
		if ( asTile[i].nColor < 0 || asTile[i].nColor >= COLOR_WIND )
		{
			return FALSE;
		}
		if ( asTile[i].nColor != asTile[0].nColor || asTile[i].nWhat != asTile[0].nWhat + i ) 
		{
			return FALSE;
		}
	}
	
	return TRUE;
}

// **************************************************************************************
// 
// 是不是刻
// 
// **************************************************************************************
BOOL CPublicJudge::IsKe( MAHJONGTILE asTile[] )
{

	for( int i = 0; i < 3; i++ )
	{
		if ( asTile[i].nColor < 0 || asTile[i].nColor > COLOR_JIAN )
		{
			return FALSE;
		}
		if ( asTile[i].nColor != asTile[0].nColor || asTile[i].nWhat != asTile[0].nWhat ) 
		{
			return FALSE;
		}
	}
	
	return TRUE;
}

// **************************************************************************************
// 
// 是不是杠
// 
// **************************************************************************************
BOOL CPublicJudge::IsGang( MAHJONGTILE asTile[] )
{
	for( int i = 0; i < 4; i++ )
	{
		if ( asTile[i].nColor < 0 || asTile[i].nColor > COLOR_JIAN )
		{
			return FALSE;
		}
		if ( asTile[i].nColor != asTile[0].nColor || asTile[i].nWhat != asTile[0].nWhat ) 
		{
			return FALSE;
		}
	}
	
	return TRUE;
}

// **************************************************************************************
// 
// 是不是龙
// 
// **************************************************************************************
BOOL CPublicJudge::IsLong( MAHJONGTILE asTile[] )
{
	for( int i = 0; i < 3; i++ )
	{
		if ( asTile[i].nColor < 0 || asTile[i].nColor >= COLOR_WIND )
		{
			return FALSE;
		}
		if ( asTile[i].nColor != asTile[0].nColor || asTile[i].nWhat != asTile[0].nWhat + i * 3 ) 
		{
			return FALSE;
		}
	}
	
	return TRUE;
}

// **************************************************************************************
// 
// 是不是将
// 
// **************************************************************************************
BOOL CPublicJudge::IsJong( MAHJONGTILE asTile[] )
{
	for( int i = 0; i < 2; i++ )
	{
		if ( asTile[i].nColor < 0 || asTile[i].nColor > COLOR_JIAN )
		{
			return FALSE;
		}
		if ( asTile[i].nColor != asTile[0].nColor || asTile[i].nWhat != asTile[0].nWhat ) 
		{
			return FALSE;
		}
	}
	
	return TRUE;
}

// **************************************************************************************
// 
// 是不是有效的牌
// 
// **************************************************************************************
BOOL CPublicJudge::ValidTile( MAHJONGTILE &sTile, CHECKPARAM & sCheckParam, 
		int anAppearedTileID[], int &cnAppearedTile )
{
	// ID号不能是0
	if ( sTile.nID == 0 )
	{
		return FALSE;
	}
	
	// 花色点数必须得和ID号一致
	if (( sTile.nID & 0x0ff0 ) != ( sTile.nColor << 8 ) + ( sTile.nWhat << 4 ) ) 
	{
		return FALSE;
	}

	// 手上有没有这张牌
	int i = 0;
	for(i = 0; i < sCheckParam.cnHandStone; i++ )
	{
		if ( sCheckParam.asHandStone[i].nID == sTile.nID )
		{
			break;
		}
	}
	if ( i == sCheckParam.cnHandStone )
	{
		// 没找到，看看吃碰杠的牌里有没有
		int j = 0;
		for(j = 0; j < sCheckParam.cnShowGroups; j++ )
		{
			int k = 0;
			for(k = 0; k < 4; k++ )
			{
				if ( sTile.nID == sCheckParam.asShowGroup[j].asStone[k].nID )
				{
					break;
				}
			}
			if ( k != 4 )
			{
				break;
			}
		}
		if ( j == sCheckParam.cnShowGroups )
		{
			// 也没找到这张牌
			return FALSE;
		}
	}

	// 这张牌以前有没有出现过
	for( i = 0; i < cnAppearedTile; i++ )
	{
		if ( anAppearedTileID[i] == sTile.nID )
		{
			return FALSE;
		}
	}
	// 这张牌没出现过
	anAppearedTileID[cnAppearedTile] = sTile.nID;
	cnAppearedTile++;

	return TRUE; 
}



// **************************************************************************************
// 
// 是否能吃
// 
// **************************************************************************************
int CPublicJudge::CheckChi( CHECKPARAM &sCheckParam, STONEGROUP asShowGroup[] )
{
    if (sCheckParam.asHandStone[0].nColor == COLOR_WIND || sCheckParam.asHandStone[0].nColor == COLOR_JIAN)
    {
        // 对混牌既不能吃不也不能碰
        return 0;
    }

	int cnEatGroup = 0;
	int anRelateIndex[5] = { -1, -1, 0, -1, -1 };	// 跟第一张牌相关联的牌的索引
	for( int i = 1; i < sCheckParam.cnHandStone; i++ )
	{
        if (sCheckParam.asHandStone[i].nColor != sCheckParam.asHandStone[0].nColor)// 混牌不能参与吃牌
        {
            continue;
        }
		int nMinus = sCheckParam.asHandStone[i].nWhat - sCheckParam.asHandStone[0].nWhat;
		if ( nMinus <= 2 && nMinus >= -2 && nMinus != 0 && anRelateIndex[nMinus + 2] == -1 )
		{
			anRelateIndex[nMinus + 2] = i;
		}
	}
	for( int i = 0; i <= 2; i++ )
	{
		if ( anRelateIndex[i] != -1 && anRelateIndex[i + 1] != -1 && anRelateIndex[i + 2] != -1 )
		{
			asShowGroup[cnEatGroup].nGroupStyle = GROUP_STYLE_SHUN;
			asShowGroup[cnEatGroup].asStone[0] = sCheckParam.asHandStone[anRelateIndex[i]];
			asShowGroup[cnEatGroup].asStone[1] = sCheckParam.asHandStone[anRelateIndex[i + 1]];
			asShowGroup[cnEatGroup].asStone[2] = sCheckParam.asHandStone[anRelateIndex[i + 2]];
			assert( asShowGroup[cnEatGroup].asStone[0].nID != 0 
				&& asShowGroup[cnEatGroup].asStone[1].nID != 0 
				&& asShowGroup[cnEatGroup].asStone[2].nID != 0 );
			cnEatGroup++;
		}
	}

	return cnEatGroup;
}

// **************************************************************************************
// 
// 是否能碰
// 
// **************************************************************************************
int CPublicJudge::CheckPeng( CHECKPARAM &sCheckParam, STONEGROUP asShowGroup[] )
{
	int cnSameStone = 1;
	for( int i = 1; i < sCheckParam.cnHandStone; i++ )
	{
		if ( CMahJongTile::SameTile( sCheckParam.asHandStone[i].nID, sCheckParam.asHandStone[0].nID ) )
		{
			asShowGroup[0].nGroupStyle = GROUP_STYLE_KE;
			asShowGroup[0].asStone[cnSameStone] = sCheckParam.asHandStone[i];
			assert( asShowGroup[0].asStone[cnSameStone].nID != 0 );
			cnSameStone++;
			if ( cnSameStone == 3 )
			{
				asShowGroup[0].asStone[0] = sCheckParam.asHandStone[0];
				assert( asShowGroup[0].asStone[0].nID != 0 );
				return 1;
			}
		}
	}

	return 0;
}

// **************************************************************************************
// 
// 是否能杠,必须在sCheckParam的nWinMode中设置是否自摸的牌
// 
// **************************************************************************************
int CPublicJudge::CheckGang( CHECKPARAM &sCheckParam, STONEGROUP asShowGroup[] )
{
	if (!( sCheckParam.nWinMode & WIN_MODE_ZIMO ) )
	{
		// 对混牌除非是自已摸的，否则不能杠
		return 0;
	}

	int cnCheck = 1;			// 检查几张牌
	int cnGang = 0;
	if ( sCheckParam.nWinMode & WIN_MODE_ZIMO )
	{
		// 如果是自已抓牌后的请求杠牌,每张牌都要看看能不能杠
		cnCheck = sCheckParam.cnHandStone;
	}

	for( int nCheckIndex = 0; nCheckIndex < cnCheck; nCheckIndex++ )
	{
		int cnSameStone = 1;
		// 手中牌
		for( int i = nCheckIndex + 1; i < sCheckParam.cnHandStone; i++ )
		{
			if ( CMahJongTile::SameTile( sCheckParam.asHandStone[i].nID, sCheckParam.asHandStone[nCheckIndex].nID ) )
			{
				assert( cnSameStone <= 3 );
				asShowGroup[cnGang].nGroupStyle = GROUP_STYLE_MINGGANG;
				if ( sCheckParam.nWinMode & WIN_MODE_ZIMO )
				{
					// 自已摸的牌成杠，暗杠
					asShowGroup[cnGang].nGroupStyle = GROUP_STYLE_ANGANG;
				}
				asShowGroup[cnGang].asStone[cnSameStone] = sCheckParam.asHandStone[i];
				assert( asShowGroup[cnGang].asStone[cnSameStone].nID != 0 );
				cnSameStone++;
				if ( cnSameStone == 4 )
				{
					asShowGroup[cnGang].asStone[0] = sCheckParam.asHandStone[nCheckIndex];
					assert( asShowGroup[cnGang].asStone[0].nID != 0 );
					cnGang++;
					break;
				}
			}
		}
		if ( cnSameStone == 4 )
		{
			continue;
		}

		// 如果是自已摸的牌，还要看看碰的牌
		if ( sCheckParam.nWinMode & WIN_MODE_ZIMO )
		{
			// 碰的牌
			for( int i = 0; i < sCheckParam.cnShowGroups; i++ )
			{
				if ( sCheckParam.asShowGroup[i].nGroupStyle != GROUP_STYLE_KE 
					|| !CMahJongTile::SameTile( sCheckParam.asShowGroup[i].asStone[0].nID, sCheckParam.asHandStone[nCheckIndex].nID ) )
				{
					continue;
				}
				// 肯定可以杠
				asShowGroup[cnGang].nGroupStyle = GROUP_STYLE_MINGGANG;
				asShowGroup[cnGang].asStone[0] = sCheckParam.asHandStone[nCheckIndex];
				memcpy( asShowGroup[cnGang].asStone + 1, sCheckParam.asShowGroup[i].asStone, 
					sizeof( STONE ) * 3 );
				cnGang++;
				break;
			}
		}
	}

	return cnGang;
}


