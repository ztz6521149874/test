#pragma once
#include "tilewall.h"
#include "TKMahjongDefine.h"

class CPublicTileWall : public ITileWall
{
public:
	CPublicTileWall(void);
	virtual ~CPublicTileWall(void);

public:
	// 洗牌,砌牌墙
	virtual void Shuffle( ShuffleParam * pShuffleParam, int anFrustaOfSeat[] );

	// 开牌
	virtual void SetOpenDoorPos( int nOpenDoorSeat , int nOpenDoorFrusta );

	// 从牌墙里抓一张牌
	virtual CMahJongTile * GetOneTile( bool bFromWallHeader , int nOffset );

	// 是否还有牌
	virtual bool Empty() { return m_bNoTile; }

	// 重置游戏数据
	virtual void ResetGameData();

protected:
	int GetWallTileIndex( bool bFromWallHeader , int nOffset );

protected:
	CMahJongTile* m_apWallTile[ MAX_TILE_COUNT ] ;	// 牌墙中的牌的信息，从0号游戏座位玩家的右手方第一墩牌的上层那张牌开始，一张上层的牌、一张下层的牌，依次遍历所有的牌
	int	m_anFrustaCount[ MAX_PLAYER_COUNT ];		// 每方各有几墩牌
	int m_cnTile;									// 总共有几张牌

	int m_nCurWallTileIndex ;						// 当前从牌头抓牌的牌的索引号（这时候，下一张牌就是索引号＋1的那张牌）
	int m_nLastWallTileIndex ;						// 当前从牌尾抓牌的牌的索引号（这时候，下一张牌可能是索引号＋1的那张牌，也可能是索引号－3的那张牌）
	int	m_nHaidiTileIndex ;							// 海底牌的索引号

	int m_nOpenDoorSeat ;							// 开牌座位号
	int m_nOpenDoorFrusta ;							// 开牌的墩数索引号

	bool m_bNoTile ;								// 是否已经没牌了
	int m_nRealTile;								// 牌墙中真实的牌墙数量
};
