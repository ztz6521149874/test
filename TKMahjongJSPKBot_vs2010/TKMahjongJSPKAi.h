#pragma once
#include "typedef.h"
#include <functional>
#include <json.h>
#include <string>
#include <sstream>
#include <memory>
#include "../ProtocolBuffer/TKMahJongJSPK.pb.h"
#include "TKMahjongJSPKAiUtil.h"
#include "TKMahjongJSPKAiDefine.h"
#include "TKBotPlayer.h"

using namespace std;
using namespace google::protobuf;
using namespace cn::jj::service::msg::protocol;

//////////////////////////////////////////////////////////////////////////////
class CTKMahjongJSPKAi
{
public:
	CTKMahjongJSPKAi();
	~CTKMahjongJSPKAi();

public:
	void InitData();
    void ClearData();
	void SetPostData();

	void SetAIServerAddr(string addr);

	void SetAILevel(int aiLevel);
	void SetMySeat(UINT mySeat);
	void SetDealerSeat(UINT dealerSeat);
	void SetDealType(UINT dealType);
	void UpdateLeftNum();
	void SetShowCount(UINT tileId);
	
	void AddHandTiles(UINT tileId);
	void DeleteHandTiles(UINT tileId);

	void HandleOpenDoor();
	void HandleSpecialTile(UINT spTile);
	void HandleDrawTile(UINT tileId);
	void HandleDiscardTile(int seat, UINT tileId);
	void HandleChowTile(UINT seat, vector<UINT> chiTiles);
	void HandlePonTile(UINT seat, vector<UINT> ponTiles);
	void HandleKonTile(UINT seat, UINT konType, vector<UINT> konTiles);
	void HandleListenTile(int seat, UINT tileId);

	//typedef void(*callback)(string);
	typedef std::function<void(string)> callback;
	void run(callback p_callBack);


	//HTTP应答的消息内容转变成数组指针返回
	void HttpRespStrToValues();
	int GetRespValueArray(int index);

	int FindHandTileIdValue(int value, int pos);

    //检查要杠的这张牌是补杠
    bool IsBuGang();

	void ClearRespValueArray();

public:
	MahjongJSPKAiUtil * m_aiUtil;

private:
	std::vector<int>   m_respValueArray;

	Json::Value        m_jsonReq;
	int                mySeat;
	vector<UINT>       m_handTiles;

	string             m_strAIServerAddr;

	HTTP_POST          m_asHttpPost;
	HTTP_RETURN        m_respResult;
};
