#pragma once
#include "typedef.h"
#include <string>
#include <sstream>
#include <memory>
#include <iterator>
#include <algorithm> 
#include "../ProtocolBuffer/TKMahjongJSPK.pb.h"
#include "TKMahjongJSPKAi.h"
#include "HttpClient.h"

using namespace std;

CTKMahjongJSPKAi::CTKMahjongJSPKAi() {
	m_aiUtil = new MahjongJSPKAiUtil();
}

CTKMahjongJSPKAi::~CTKMahjongJSPKAi() {
}

void CTKMahjongJSPKAi::InitData() {
	//游戏选手进入座位已初始化，在游戏开局之前
	//mySeat = -1;
	m_handTiles.swap(vector<UINT>());
	
	memset(&m_asHttpPost, 0, sizeof(m_asHttpPost));

	m_asHttpPost.nGameId = GAME_ID;
	m_asHttpPost.asRespMess.nLeftNum = 39;

	for (int i = 0; i < PLAYER_NUMBER; i++)
	{
		m_asHttpPost.asRespMess.asGameMess[i].nSeat = i;
	}

	memset(&m_respResult, 0, sizeof(m_respResult));
}

void CTKMahjongJSPKAi::ClearData() {
	//游戏选手进入座位已初始化，在游戏开局之前
	//mySeat = -1;
	m_handTiles.swap(vector<UINT>());
	memset(&m_asHttpPost, 0, sizeof(m_asHttpPost));
	m_asHttpPost.nGameId = GAME_ID;
	m_asHttpPost.asRespMess.nLeftNum = 39;
	for (int i = 0; i < PLAYER_NUMBER; i++)
	{
		m_asHttpPost.asRespMess.asGameMess[i].nSeat = i;
	}

	memset(&m_respResult, 0, sizeof(m_respResult));
}

void CTKMahjongJSPKAi::SetPostData() {
	string myTilesStr = "";
	for (int i=0;i<m_handTiles.size();i++)
	{
		myTilesStr = myTilesStr + m_aiUtil->GetTileIdStr(m_handTiles[i]);
	}
	m_asHttpPost.asRespMess.asGameMess[mySeat].sHandCard = myTilesStr;

	m_jsonReq.clear();
	m_jsonReq["game_id"] = m_asHttpPost.nGameId;
	m_jsonReq["respond_type"] = m_asHttpPost.nRespTpye;
	m_jsonReq["respond_mess"]["ai_level"] = m_asHttpPost.asRespMess.nAiLevel;
	m_jsonReq["respond_mess"]["deal_seat"] = m_asHttpPost.asRespMess.nDealSeat;
	m_jsonReq["respond_mess"]["deal_type"] = m_asHttpPost.asRespMess.nDealType;
	m_jsonReq["respond_mess"]["banker"] = m_asHttpPost.asRespMess.nBanker;
	m_jsonReq["respond_mess"]["special"] = m_asHttpPost.asRespMess.sSpeacial;
	m_jsonReq["respond_mess"]["cardwall"] = m_asHttpPost.asRespMess.sCardWall;
	m_jsonReq["respond_mess"]["left_num"] = m_asHttpPost.asRespMess.nLeftNum;
	m_jsonReq["respond_mess"]["last_history"] = m_asHttpPost.asRespMess.sLastHistory;
	for (int i = 0; i < TIIL_TYPE_NUMBER; i++)
	{
		m_jsonReq["respond_mess"]["show_count"].append(m_asHttpPost.asRespMess.nsShowCount[i]);
	}
	for (int i = 0; i < PLAYER_NUMBER; i++)
	{
		Json::Value player;
		player["seat"] = m_asHttpPost.asRespMess.asGameMess[i].nSeat;
		player["gamestate"] = m_asHttpPost.asRespMess.asGameMess[i].nGameState;
		player["hand_card"] = m_asHttpPost.asRespMess.asGameMess[i].sHandCard;
		player["chow_card"] = m_asHttpPost.asRespMess.asGameMess[i].sChowCard;
		player["pong_card"] = m_asHttpPost.asRespMess.asGameMess[i].sPongCard;
		player["akon_card"] = m_asHttpPost.asRespMess.asGameMess[i].saKonCard;
		player["mkon_card"] = m_asHttpPost.asRespMess.asGameMess[i].smKonCard;
		player["aout_card"] = m_asHttpPost.asRespMess.asGameMess[i].saOutCard;
		player["bout_card"] = m_asHttpPost.asRespMess.asGameMess[i].sbOutCard;
		for (int j = 0; j < m_asHttpPost.asRespMess.asGameMess[i].sHasHuCard.size(); j++)
		{
			player["has_hu_card"].append(m_asHttpPost.asRespMess.asGameMess[i].sHasHuCard[j]);
		}

		if (m_asHttpPost.asRespMess.asGameMess[i].sHasHuCard.size() == 0)
		{
			player["has_hu_card"].append("");
		}
		
		m_jsonReq["respond_mess"]["gamemess"].append(player);
	}
}

void CTKMahjongJSPKAi::SetAIServerAddr(string addr) {
	m_strAIServerAddr = addr;
}

void CTKMahjongJSPKAi::SetAILevel(int aiLevel) {
	m_asHttpPost.asRespMess.nAiLevel = aiLevel;
}

void CTKMahjongJSPKAi::SetMySeat(UINT seat) {
	mySeat = seat;
	m_asHttpPost.asRespMess.nDealSeat = mySeat;
}

void CTKMahjongJSPKAi::SetDealerSeat(UINT dealerSeat) {
	m_asHttpPost.asRespMess.nBanker = dealerSeat;
}

void CTKMahjongJSPKAi::SetDealType(UINT dealType) {
	m_asHttpPost.asRespMess.nDealType = dealType;
}

void CTKMahjongJSPKAi::UpdateLeftNum() {
	m_asHttpPost.asRespMess.nLeftNum--;
}

//设置post.respond_mess数据 明牌计数
void CTKMahjongJSPKAi::SetShowCount(UINT tileId) {
	int idx = m_aiUtil->GetShowTileIndex(tileId);
	if (idx >= 34 || m_asHttpPost.asRespMess.nsShowCount[idx] >= 4) {
		TKWriteLog("SetShowCount error: %d",  tileId);
	}
	if (idx < 34) {
		m_asHttpPost.asRespMess.nsShowCount[idx]++;
	}
}

void CTKMahjongJSPKAi::AddHandTiles(UINT tileId) {
	m_handTiles.push_back(tileId);
}

void CTKMahjongJSPKAi::DeleteHandTiles(UINT tileId) {

	std::vector<UINT>::iterator iter = std::find(m_handTiles.begin(), m_handTiles.end(), tileId);
	if (iter != m_handTiles.end()) {
		m_handTiles.erase(iter);
	}
	else {
	}
}

void CTKMahjongJSPKAi::HandleOpenDoor() {

}

void CTKMahjongJSPKAi::HandleSpecialTile(UINT spTile) {
	m_asHttpPost.asRespMess.sSpeacial = m_aiUtil->GetTileIdStr(spTile);
}

void CTKMahjongJSPKAi::HandleDrawTile(UINT tileId) {
	AddHandTiles(tileId);
	SetShowCount(tileId);
}

void CTKMahjongJSPKAi::HandleDiscardTile(int seat, UINT tileId) {
	string tileIdStr = m_aiUtil->GetTileIdStr(tileId);

	string actionStr = to_string(long long(seat)) + ",";
	actionStr = actionStr + ACTION_DISCARD + ",";
	actionStr = actionStr + tileIdStr;
	
	m_asHttpPost.asRespMess.sLastHistory = actionStr;

	//更新自己手牌
	if(seat == mySeat){
		DeleteHandTiles(tileId);
	}
	else 
	{
		SetShowCount(tileId);
	}

	if (m_asHttpPost.asRespMess.asGameMess[seat].nGameState >= STATE_CALL)
	{
		//已经听牌出牌
		m_asHttpPost.asRespMess.asGameMess[seat].sbOutCard = m_asHttpPost.asRespMess.asGameMess[seat].sbOutCard + tileIdStr;
	}
	else 
	{
		//未听牌
		m_asHttpPost.asRespMess.asGameMess[seat].saOutCard = m_asHttpPost.asRespMess.asGameMess[seat].saOutCard + tileIdStr;
	}
}

void CTKMahjongJSPKAi::HandleChowTile(UINT seat, vector<UINT> chiTiles) {
}

void CTKMahjongJSPKAi::HandlePonTile(UINT seat, vector<UINT> ponTiles) {
	string pon = "";
	for (int i = 0; i < ponTiles.size(); ++i) {
		pon = pon + m_aiUtil->GetTileIdStr(ponTiles[i]);

		if (mySeat == seat) {
			DeleteHandTiles(ponTiles[i]);
		}
	}

	if (mySeat != seat) {
		for (int i = 0; i < ponTiles.size() - 1; ++i) {
			SetShowCount(ponTiles[i]);
		}
	}
	
	m_asHttpPost.asRespMess.asGameMess[seat].sPongCard = m_asHttpPost.asRespMess.asGameMess[seat].sPongCard + pon + ",";

	string actionStr = to_string(long long(seat)) + ",";
	actionStr = actionStr + ACTION_PON + ",";
	actionStr = actionStr + pon;

	m_asHttpPost.asRespMess.sLastHistory = actionStr;
}

void CTKMahjongJSPKAi::HandleKonTile(UINT seat, UINT konType, vector<UINT> konTiles) {
	string kon = "";
	for (int i = 0; i < konTiles.size(); ++i) {
		kon = kon + m_aiUtil->GetTileIdStr(konTiles[i]);
		if (mySeat == seat) {
			DeleteHandTiles(konTiles[i]);
		}
	}

	string actionStr = to_string(long long(seat)) + ",";
	actionStr = actionStr + ACTION_KON + ",";

	//对手玩家暗杠时
	if(kon == ""){
		kon = "UNUNUNUN";
	}

	actionStr = actionStr + kon;

	m_asHttpPost.asRespMess.sLastHistory = actionStr;

	if (konType == MING_GANG)
	{
		if (mySeat != seat) {
			for (int i = 0; i < konTiles.size() - 1; ++i) {
				SetShowCount(konTiles[i]);
			}
		}
		m_asHttpPost.asRespMess.asGameMess[seat].smKonCard = m_asHttpPost.asRespMess.asGameMess[seat].smKonCard + kon + ",";
	}
	else if (konType == AN_GANG)
	{
		m_asHttpPost.asRespMess.asGameMess[seat].saKonCard = m_asHttpPost.asRespMess.asGameMess[seat].saKonCard + kon + ",";
	}
	else if (konType == BU_GANG) 
	{

		if (mySeat != seat && konTiles.size() > 0) {
			SetShowCount(konTiles[0]);
		}

		m_asHttpPost.asRespMess.asGameMess[seat].smKonCard = m_asHttpPost.asRespMess.asGameMess[seat].smKonCard + kon + ",";
		
		//删除碰牌牌组
		string pon(kon, 0, 6);
		pon = pon + ",";
		int flag = 0;
		while ((flag = m_asHttpPost.asRespMess.asGameMess[seat].sPongCard.find(pon)) != -1) {
			m_asHttpPost.asRespMess.asGameMess[seat].sPongCard.erase(flag, pon.size());
		}
	}
}


void CTKMahjongJSPKAi::HandleListenTile(int seat, UINT tileId) {
	m_asHttpPost.asRespMess.asGameMess[seat].nGameState = STATE_CALL;

	if (tileId == 0)
	{
		//闲家天听
		return;
	}

	string tileIdStr = m_aiUtil->GetTileIdStr(tileId);

	string actionStr = to_string(long long(seat)) + ",";
	actionStr = actionStr + ACTION_LISTEN + ",";
	actionStr = actionStr + m_aiUtil->GetTileIdStr(tileId);
	
	m_asHttpPost.asRespMess.sLastHistory = actionStr;

	//更新自己手牌
	if (mySeat == seat) {
		DeleteHandTiles(tileId);
	}

	if (m_asHttpPost.asRespMess.asGameMess[seat].nGameState >= STATE_CALL)
	{
		//已经听牌出牌
		m_asHttpPost.asRespMess.asGameMess[seat].sbOutCard = m_asHttpPost.asRespMess.asGameMess[seat].sbOutCard + tileIdStr;
	}
	else
	{
		//未听牌
		m_asHttpPost.asRespMess.asGameMess[seat].saOutCard = m_asHttpPost.asRespMess.asGameMess[seat].saOutCard + tileIdStr;
	}
}

void CTKMahjongJSPKAi::run(callback p_callBack)
{
	string strRequest;
	string strResponse;

	SetPostData();
	strRequest = m_jsonReq.toStyledString();

	

	string url = "http://" + m_strAIServerAddr + "/JiShu_MahJong";

#ifdef _DEBUG
	TKWriteLog("http send url: %s", url.c_str());
	TKWriteLog("http send: %s", strRequest.c_str());
#endif
	
	int nRet = HttpPost(url, strRequest, strResponse);

	if (nRet == SUCCESS)
	{
		Json::Reader reader;
		Json::Value jsonRes;
		if (reader.parse(strResponse, jsonRes))
		{
			m_respResult.asReqMess.sActionType = jsonRes["action_type"].asString();
			m_respResult.asReqMess.sActionContent = jsonRes["action_content"].asString();

#ifdef _DEBUG
			TKWriteLog("http response: %s", strResponse.c_str());
#endif
			
			p_callBack(m_respResult.asReqMess.sActionType);
		}
		else
		{
			TKWriteLog("AI request failed, reason: response is not json's format.");
		}
	}
	else
	{
		TKWriteLog("AI request failed, reason: %d", nRet);
	}
}

void CTKMahjongJSPKAi::HttpRespStrToValues() {

	string s1 = "";
	for(int i=0;i<m_respValueArray.size();i++){
		s1= s1+" "+to_string(long long (m_respValueArray[i]));
	}
	
	m_respValueArray.clear();

	string content = m_respResult.asReqMess.sActionContent;

	int valColor = 0;
	int valWhat = 0;
	for (int i = 0; i < content.length(); i += 2) {
		switch (content[i])
		{
		case 'B':
			valColor = TILECOLOR_TIAO;
			valWhat = int(content[i + 1] - '0') - 1;
			break;
		case 'C':
			valColor = TILECOLOR_CHARACTER;
			valWhat = int(content[i + 1]- '0') - 1;
			break;
		case 'W':
			valColor = TILECOLOR_WIND;
			switch (content[i + 1])
			{
			case 'E':
				valWhat = 0;
				break;
			case 'S':
				valWhat = 1;
				break;
			case 'W':
				valWhat = 2;
				break;
			case 'N':
				valWhat = 3;
				break;
			default:
				break;
			}
			break;
		case 'A':
			valColor = TILECOLOR_ARROW;
			switch (content[i + 1])
			{
			case 'C':
				valWhat = 0;
				break;
			case 'F':
				valWhat = 1;
				break;
			case 'B':
				valWhat = 2;
				break;
			default:
				break;
			}
			break;
		default:
			break;
		}
		int value = (valColor << 4) | valWhat;
		m_respValueArray.push_back(value);
	}
	
	string s2 = "";
	for (int i = 0; i < m_respValueArray.size(); i++) {
		s2 = s2 + " " + to_string(long long(m_respValueArray[i]));
	}
}

int CTKMahjongJSPKAi::GetRespValueArray(int index) {
	string s = "";
	for (int i = 0; i < m_respValueArray.size(); i++) {
		s = s + " " + to_string(long long(m_respValueArray[i]));
	}
	
	if (index >= m_respValueArray.size()) {
		return INVALID_TILE_ID;
	}

	return m_respValueArray[index];
}

void CTKMahjongJSPKAi::ClearRespValueArray() {
	m_respValueArray.clear();
}

int CTKMahjongJSPKAi::FindHandTileIdValue(int value, int pos) {

	if (value == INVALID_TILE_ID)
	{
		return 0;
	}

	string s = "";
	for (int i = 0; i < m_handTiles.size(); i++) {
		s = s + " " + to_string(long long(m_handTiles[i]));
	}
	
	int pTemp = 0;

	for (int i = 0; i < m_handTiles.size(); i++) {
		int curVal = m_aiUtil->GetValue(m_handTiles[i]);
		if (value == curVal) {
			if (pTemp == pos)
			{
				return m_handTiles[i];
			}
			pTemp++;
		}
	}

	return 0;
}

bool CTKMahjongJSPKAi::IsBuGang(){
	 string kon = m_respResult.asReqMess.sActionContent;

	 string gang = kon + kon + kon + kon;
	 int n = gang.size();

	 if (n!=8)
	 {
		 return false;
	 }

     string subGang = gang.erase(n-3,2);  

	 if(m_asHttpPost.asRespMess.asGameMess[m_asHttpPost.asRespMess.nDealSeat].sPongCard.find(subGang) == string::npos){
		 return false;
	 }
	 return true;
}