/*
* Copyright (c) 2018,JJWorld
* All rights reserved.
* 
* 文件名称：ProtoFactory.h
* 摘    要：用于原始的C++协议与新协议protobuf 转换
* 
* 当前版本：1.0
* 作    者：wangyb01
* 完成日期：2018年1月11日
*/

#pragma once

#include "TKMahjongProtocol.h"
#include "TKProtocol.h"
#include "TKMahJong.pb.h"
#include "TKMahJongJSPK.pb.h"
#include "TKMahJongJSPKDefine.pb.h"

using namespace cn::jj::service::msg::protocol;

#define PROTOLOG	WRITELOG
#define ASSERT		assert

// protobuf 协议转换工厂
class ProtobufFactory
{
private:
	bool	m_bUseProtobuf;	     // 是否使用protobuf协议
	int		m_iMatchID;          // 当前比赛 MatchID

public:
	ProtobufFactory();
	virtual ~ProtobufFactory();
		
	// 获取单例静态接口
	static ProtobufFactory& GetInstance();

	// 是否支持Proto协议
	bool IsUseProtobuf()	{ return m_bUseProtobuf; }

public:

	/************************************************************************/
	/* 把普通协议重新填充到proto协议中										*/
	// 参数1 原始的C++ 协议
	// 参数2 返回的proto协议，有可能与参数1 相等
	// 参数3 返回的proto协议使用之后是否要delete
	// 返回值 是否成功
	/************************************************************************/
	bool SerializeMsg2Protobuf( TKHEADER* pMsg, TKHEADER** pRetMsg, bool & bUseEndDel );

private:

	// 把需要发送的protobuf消息进行重组，使地址连续
	// 注：此函数返回的指针用完之后需要释放
	TKHEADER* CreateProtoReqMsgBuff(TKREQMAHJONGPROTOBUF* pReqMsg);

	// 对发送出去的消息包进行封装成protobuf格式
	void SerializeProtobufMsg( TKHEADER* pMsg, TKREQMAHJONGPROTOBUF& protoMsgReq );

	// 组装消息头
	void SerializeCToSMsgHead(TKREQMAHJONGPROTOBUF& protoMsgReq, TKHEADER* pMsg);

	/************************************************************************/
	/*  开牌结束消息 玩家抓完自己的牌后，发送此消息到服务器                 */
	/************************************************************************/
	void SerializeCToSMsgOpenDoorOver(TKREQMAHJONGPROTOBUF& protoMsgReq, TKHEADER* pMsg);

	/************************************************************************/
	/*  玩家向服务器返回放弃的操作类型                                      */
	/************************************************************************/
	void SerializeCToSMsgRequestAck(TKREQMAHJONGPROTOBUF& protoMsgReq, TKHEADER* pMsg);

	/************************************************************************/
	/*  补花请求消息                                                       */
	/************************************************************************/
	void SerializeCToSMsgChangeFlowerReq(TKREQMAHJONGPROTOBUF& protoMsgReq, TKHEADER* pMsg);

	/************************************************************************/
	/*  出牌请求消息                                                        */
	/************************************************************************/
	void SerializeCToSMsgDiscardTileReq(TKREQMAHJONGPROTOBUF& protoMsgReq, TKHEADER* pMsg);

	/************************************************************************/
	/*  听牌请求消息                                                        */
	/************************************************************************/
	void SerializeCToSMsgCallReq(TKREQMAHJONGPROTOBUF& protoMsgReq, TKHEADER* pMsg);

	/************************************************************************/
	/*  吃牌请求消息                                                        */
	/************************************************************************/
	void SerializeCToSMsgChiReq(TKREQMAHJONGPROTOBUF& protoMsgReq, TKHEADER* pMsg);

	/************************************************************************/
	/*  碰牌请求消息                                                        */
	/************************************************************************/
	void SerializeCToSMsgPengReq( TKREQMAHJONGPROTOBUF& protoMsgReq, TKHEADER* pMsg );

	/************************************************************************/
	/*  杠牌请求消息                                                        */
	/************************************************************************/
	void SerializeCToSMsgGangReq( TKREQMAHJONGPROTOBUF& protoMsgReq, TKHEADER* pMsg );

	/************************************************************************/
	/*  和牌请求消息                                                        */
	/************************************************************************/
	void SerializeCToSMsgWinReq( TKREQMAHJONGPROTOBUF& protoMsgReq, TKHEADER* pMsg );

	/************************************************************************/
	/*  托管消息                                                            */
	/************************************************************************/
	void SerializeCToSMsgTrustPlayReq( TKREQMAHJONGPROTOBUF& protoMsgReq, TKHEADER* pMsg );

	/************************************************************************/
	/*  加倍消息                                                            */
	/************************************************************************/
	void SerializeCToSMsgDoublingReq( TKREQMAHJONGPROTOBUF& protoMsgReq, TKHEADER* pMsg );

	/************************************************************************/
	/*  超时检测消息                                                        */
	/************************************************************************/
	void SerializeCToSMsgCheckTimeOutReq( TKREQMAHJONGPROTOBUF& protoMsgReq, TKHEADER* pMsg );

	/************************************************************************/
	/*  听牌信息                                                            */
	/************************************************************************/
	void SerializeCToSMsgCallTileReq( TKREQMAHJONGPROTOBUF& protoMsgReq, TKHEADER* pMsg );

public:

	// 收到的消息包进行转换成C++协议格式
	BOOL DeserializeProtobufMsg(const TKHEADER* pMsg,TKHEADER ** ppRetMsg );

private:
	// 获取protobuf消息体
	bool GetAckMsg(const TKACKMAHJONGPROTOBUF* pMsg, MahJongTPAckMsg& ackMsg);

	/************************************************************************/
	/* 游戏规则消息                                                         */
	/************************************************************************/
	TKHEADER* DeserializeMsgRulerInfoAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg);

	/************************************************************************/
	/* 游戏坐位、庄家消息                                                   */
	/************************************************************************/
	TKHEADER* DeserializeMsgPlaceAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg);

	/************************************************************************/
	/* 门风、圈风消息                                                       */
	/************************************************************************/
	TKHEADER* DeserializeMsgWindPlaceAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg);

	/************************************************************************/
	/* 结果消息，一局游戏结束时，服务器发送此消息给所有客户端                */
	/************************************************************************/
	TKHEADER* DeserializeMsgResultTPAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg);

	/************************************************************************/
	/* 洗牌、砌牌消息                                                       */
	/************************************************************************/
	TKHEADER* DeserializeMsgShuffleAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg);

	/************************************************************************/
	/* 掷骰子消息                                                           */
	/************************************************************************/
	TKHEADER* DeserializeMsgCastDice(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg);

	/************************************************************************/
	/* 开牌消息                                                           */
	/************************************************************************/
	TKHEADER* DeserializeMsgOpenDoorAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg);

	/************************************************************************/
	/* 通知消息                                                           */
	/************************************************************************/
	TKHEADER* DeserializeMsgNotifyAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg);

	/************************************************************************/
	/* 通知补花消息                                                          */
	/************************************************************************/
	TKHEADER* DeserializeMsgChangeFlower(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg);

	/************************************************************************/
	/* 通知抓牌消息                                                           */
	/************************************************************************/
	TKHEADER* DeserializeMsgDrawTile(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg);

	/************************************************************************/
	/* 玩家向服务器返回放弃的操作类型                                      */
	/************************************************************************/
	TKHEADER* DeserializeMsgActionAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg);

	/************************************************************************/
	/* 服务器向客户端广播其他玩家的操作消息                                 */
	/************************************************************************/
	TKHEADER* DeserializeMsgRequestAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg);

	/************************************************************************/
	/* 通知出牌消息                                 */
	/************************************************************************/
	TKHEADER* DeserializeMsgDiscardTile(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg);

	/************************************************************************/
	/*  翻混消息                                                            */
	/************************************************************************/
	TKHEADER* DeserializeMsgHunAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg);

	/************************************************************************/
	/*  通知听牌消息                                                            */
	/************************************************************************/
	TKHEADER* DeserializeMsgCallAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg);

	/************************************************************************/
	/*  通知吃牌消息                                                            */
	/************************************************************************/
	TKHEADER* DeserializeMsgChiAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg);

	/************************************************************************/
	/*  通知碰牌消息                                                            */
	/************************************************************************/
	TKHEADER* DeserializeMsgPengAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg);

	/************************************************************************/
	/*  通知杠牌消息                                                            */
	/************************************************************************/
	TKHEADER* DeserializeMsgGangAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg);

	/************************************************************************/
	/*  通知和牌消息                                                            */
	/************************************************************************/
	TKHEADER* DeserializeMsgWinTPAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg);

	/************************************************************************/
	/*  赢牌详细信息                                                            */
	/************************************************************************/
	TKHEADER* DeserializeMsgWinDetailAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg);

	/************************************************************************/
	/*  赢牌详细信息                                                            */
	/************************************************************************/
	TKHEADER* DeserializeMsgWinDetailExTPAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg);

	/************************************************************************/
	/*  托管消息                                                            */
	/************************************************************************/
	TKHEADER* DeserializeMsgTrustPlayAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg);

	/************************************************************************/
	/*  分数改变消息                                                            */
	/************************************************************************/
	TKHEADER* DeserializeMsgScoreChangeAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg);

	/************************************************************************/
	/* 通知亮牌消息                                 */
	/************************************************************************/
	TKHEADER* DeserializeMsgPlayerTilesAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg);

	/************************************************************************/
	/* 加倍消息                                                            */
	/************************************************************************/
	TKHEADER* DeserializeMsgDoublingAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg);

	/************************************************************************/
	/* 玩家吃碰杠的牌信息                                                    */
	/************************************************************************/
	TKHEADER* DeserializeMsgShowTiles(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg);

	/************************************************************************/
	/* 加番牌信息                                                           */
	/************************************************************************/
	TKHEADER* DeserializeMsgSpecialTileAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg);

	/************************************************************************/
	/* 加番牌个数                                                           */
	/************************************************************************/
	TKHEADER* DeserializeMsgSpecialCountAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg);
};


