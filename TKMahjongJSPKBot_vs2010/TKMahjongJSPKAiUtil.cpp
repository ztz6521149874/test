#include "TKMahjongJSPKAiUtil.h"
#include "TKMahjongJSPKAiDefine.h"
#include <map>
#include <string>

#include <iostream>  
#include <iomanip>  
#include <fstream>  
#include <time.h>

using namespace std;

map<UINT, string> colorMap;
map<UINT, string> windMap;
map<UINT, string> arrowMap;

void MahjongJSPKAiUtil::InitData() {
	colorMap.insert(std::pair<UINT, string>(TILECOLOR_CHARACTER, "C"));
	colorMap.insert(std::pair<UINT, string>(TILECOLOR_WIND, "W"));
	colorMap.insert(std::pair<UINT, string>(TILECOLOR_ARROW, "A"));
	colorMap.insert(std::pair<UINT, string>(TILECOLOR_TIAO, "B"));

	windMap.insert(std::pair<UINT, string>(0, "E"));
	windMap.insert(std::pair<UINT, string>(1, "S"));
	windMap.insert(std::pair<UINT, string>(2, "W"));
	windMap.insert(std::pair<UINT, string>(3, "N"));

	arrowMap.insert(std::pair<UINT, string>(0, "C"));
	arrowMap.insert(std::pair<UINT, string>(1, "F"));
	arrowMap.insert(std::pair<UINT, string>(2, "B"));
}

UINT MahjongJSPKAiUtil::GetColor(UINT tileID){
	return (tileID >> 8) & 0x0F;
}

UINT MahjongJSPKAiUtil::GetWhat(UINT tileID){
	return (tileID >> 4) & 0x0F;
}

UINT MahjongJSPKAiUtil::GetValue(UINT tileID){
	return tileID >> 4;
}

string MahjongJSPKAiUtil::GetTileIdStr(UINT tileId) {
	InitData();
	int colorValue = GetColor(tileId);
	int whatValue = GetWhat(tileId);
	string res = colorMap[colorValue];
	switch (colorValue)
	{
	case TILECOLOR_TIAO:
		res = res + to_string(long long(whatValue + 1));
		break;
	case TILECOLOR_CHARACTER:
		res = res + to_string(long long (whatValue + 1));
		break;
	case TILECOLOR_ARROW:
		res = res + arrowMap[whatValue];
		break;
	case TILECOLOR_WIND:
		res = res + windMap[whatValue];
		break;
	default:
		break;
	}
	return res;
} 
  

//log输出; 
void  MahjongJSPKAiUtil::WriteLog(string tag, string content) {
	ofstream outfile;  
    outfile.open("D:\\AIbot\\log\\botLog.txt", ios::app);  
    if(outfile.is_open())  
    {
		time_t timep;
        time (&timep);
	    char tmp[64];
	    strftime(tmp, sizeof(tmp), "%Y-%m-%d %H:%M:%S",localtime(&timep));

        outfile << tmp << "  " << tag << content << endl;  
        outfile.close();   
    }   
}

//得到Post (JSON)字段show_count的序号 条万筒（27）东西南北中发白（7）
int  MahjongJSPKAiUtil::GetShowTileIndex(UINT tileId) {

	string tileStr = GetTileIdStr(tileId);

	if (tileStr.size() != 2) {
		return 34;
	}

	int what = GetWhat(tileId) + 1;
	string color = tileStr.substr(0, 1);

	int idx = 0;
	if (color == "B") {
		idx = what - 1;
	}
	else if (color == "C") {
		idx = 8 + what;
	}
	else if (color == "D") {
		idx = 18 + what;
	}
	else if (color == "W") {
		idx = 27 + what;
	}
	else if (color == "A") {
		idx = 30 + what;
	}
	else {
		return 34;
	}
	return idx;
}