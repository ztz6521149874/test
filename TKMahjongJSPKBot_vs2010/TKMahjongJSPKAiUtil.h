#pragma once
#include "typedef.h"
#include <string>

using namespace std;

class MahjongJSPKAiUtil
{
public:
	void InitData();
	UINT GetColor(UINT tileId);
	UINT GetWhat(UINT tileId);
	UINT GetValue(UINT tileId);
	string GetTileIdStr(UINT tileId);

	int GetShowTileIndex(UINT tileId);

	void WriteLog(string tag, string content);
private:
	
};
