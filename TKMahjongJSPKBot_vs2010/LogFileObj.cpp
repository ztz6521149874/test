#include <winsock2.h>
#include <windows.h>
#include <tchar.h>		// _vsntprintf

#include "LogFileObj.h"	// CLogFileObj,CBlockLogObj


// ************************************************************************************************************************
// 宏定义
// 

#define MEM_BYTE_PER_LINE	( 8 )			// 输出内存块时一行显示的字节数目
#define TEXT_SPACE			TEXT( "                                   " )	// 对齐所需要的空格

// 
// 宏定义
// ************************************************************************************************************************





// ************************************************************************************************************************
// 全局变量定义
// 

extern LPCTSTR g_szLogFileName ;			// 日志文件名
CLogFileObj __objLogFile__ ;				// 全局的日志文件对象
//CListLogObj __objLogList__ ;				// 全局的日志列表对象

// 
// 全局变量定义
// ************************************************************************************************************************




// ************************************************************************************************************************
// 静态变量定义
// 

int CLogFileObj::m_nIndent = 0 ;	// 缩近尺寸最初为0

// 
// 静态变量定义
// ************************************************************************************************************************



// ************************************************************************************************************************
// 成员函数的定义
// 


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CLogFileObj::CLogFileObj( LPCTSTR pcszLogFileName /* = NULL */ )
{
	memset( m_szLogFileName , 0 , sizeof( m_szLogFileName ) ) ;
	if( NULL != pcszLogFileName && 0 != *pcszLogFileName )
	{ // 文件名有效
		_tcscpy_s( m_szLogFileName , pcszLogFileName ) ;
	}
	else
	{ // 文件名无效
		_tcscpy_s( m_szLogFileName , g_szLogFileName ) ;
	}
	
	m_pLogFile = NULL ;

	m_bEnableLog = FALSE;

	::InitializeCriticalSection( &m_CriticalSection ) ;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CLogFileObj::~CLogFileObj()
{
	::DeleteCriticalSection( &m_CriticalSection ) ;

	if( NULL != m_pLogFile )
	{ // 文件被打开
		fclose( m_pLogFile ) ;
		m_pLogFile = NULL ;
	}
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CLogFileObj::EnableLog( BOOL bEnable )
{
	m_bEnableLog = bEnable;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
BOOL CLogFileObj::WriteLog( LPCTSTR pcszFormat , ... )
{
	if ( !m_bEnableLog )
	{
		return TRUE;
	}

	// 获得缩近的文本
	TCHAR szIndent[ 64 ] = { 0 } ;
	if( m_nIndent > 0 )
	{ // 有缩近的时候
		TCHAR szIndentFormat[ 16 ] = { 0 } ;
		_stprintf_s( szIndentFormat , TEXT( "%%%ds" ) , m_nIndent ) ;
		_stprintf_s( szIndent , szIndentFormat , TEXT( " " ) ) ;
	}
	
	// 获得要输出的文本
	TCHAR szText[ 1024 * 4 ] = { 0 } ;
	va_list arg ;
	va_start( arg , pcszFormat ) ;
	_vsntprintf_s( szText , sizeof( szText ) / sizeof( TCHAR ) - 1 , pcszFormat , arg ) ;
	va_end( arg ) ;

	__try
	{
		// 进入写日志文件的临界区
		EnterLogCriticalSection() ;
		
		// 先写公共信息（进程ID、线程ID、时间）
		DWORD dwProcessID = 0 , dwThreadID = 0 ;
		dwProcessID = GetCurrentProcessId() ;
		dwThreadID  = GetCurrentThreadId() ;
		SYSTEMTIME timeNow = { 0 } ;
		GetLocalTime( &timeNow ) ;
		TCHAR szPublicText[ 256 ] = { 0 } ;
		_stprintf_s( szPublicText , TEXT( "[%04d-%02d-%02d %02d:%02d:%02d:%03d %04d:%04d]" ) , timeNow.wYear , timeNow.wMonth , timeNow.wDay , timeNow.wHour , timeNow.wMinute , timeNow.wSecond , timeNow.wMilliseconds , dwProcessID , dwThreadID ) ;
		DoWriteLog( szPublicText ) ;
		
		// 逐行写入文本
		LPTSTR pStart = szText ;
		LPTSTR pos = _tcschr( pStart , TEXT( '\n' ) ) ;
		TCHAR szOneLine[ 1024 ] = { 0 } ;
		while( NULL != pos )
		{ // 有换行符
			*pos = 0 ;
			_stprintf_s( szOneLine , TEXT( "%s%s\n%s" ) , szIndent , pStart , TEXT_SPACE ) ;	// 为了保证对齐
			DoWriteLog( szOneLine ) ;
			pStart = pos + 1 ;
			pos = _tcschr( pStart , TEXT( '\n' ) ) ;
		}
		_stprintf_s( szOneLine , TEXT( "%s%s\n" ) , szIndent , pStart ) ;
		DoWriteLog( szOneLine ) ;
	}
	__finally
	{
		// 离开写日志文件的临界区
		LeaveLogCriticalSection() ;
	}
	
	return TRUE ;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
BOOL CLogFileObj::WriteMemLog( LPCTSTR pcszTitle , LPVOID pvData , INT nSize )
{
	if ( !m_bEnableLog )
	{
		return TRUE;
	}

	// 获得缩近的文本
	TCHAR szIndent[ 64 ] = { 0 } ;
	if( m_nIndent > 0 )
	{ // 有缩近的时候
		TCHAR szIndentFormat[ 16 ] = { 0 } ;
		_stprintf_s( szIndentFormat , TEXT( "%%%ds" ) , m_nIndent ) ;
		_stprintf_s( szIndent , szIndentFormat , TEXT( "" ) ) ;
	}
	
	__try
	{
		// 进入写日志文件的临界区
		EnterLogCriticalSection() ;
		
		// 先写标题和内存块大小信息
		WriteLog( TEXT( "%s(%d):" ) , pcszTitle , nSize ) ;
		
		// 以每行MEM_BYTE_PER_LINE个字节的格式写内存块信息
		PBYTE pByte = ( PBYTE ) pvData ;
		while( nSize >= MEM_BYTE_PER_LINE )
		{
			WriteMemOneLine( pByte , MEM_BYTE_PER_LINE , szIndent ) ;
			pByte += MEM_BYTE_PER_LINE ;
			nSize -= MEM_BYTE_PER_LINE ;
		}
		WriteMemOneLine( pByte , nSize , szIndent ) ;
	}
	__finally
	{
		// 离开写日志文件的临界区
		LeaveLogCriticalSection() ;
	}	

	return TRUE ;
}



// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
BOOL CLogFileObj::WriteTextToFile( LPCTSTR pcszText )
{
	if ( !m_bEnableLog )
	{
		return TRUE;
	}

	__try
	{
		// 进入写日志文件的临界区
		EnterLogCriticalSection() ;
		
		// 看看这个日志文件打开了没有
		if( NULL == m_pLogFile )
		{ // 日志文件没有正确打开
			_tfopen_s( &m_pLogFile, m_szLogFileName , TEXT( "a+" ) ) ;	// 打开日志文件
			if( NULL == m_pLogFile )
			{ // 打开失败
				return FALSE ;
			}
		}
		
		// 写入这行文本
		_ftprintf_s( m_pLogFile , TEXT( "%s" ) , pcszText ) ;
		
		// 更新文件的缓冲区
		fflush( m_pLogFile ) ;
	}
	__finally
	{
		// 离开写日志文件的临界区
		LeaveLogCriticalSection() ;
	}
	
	return TRUE ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
BOOL CLogFileObj::DoWriteLog( LPCTSTR pcszText )
{
	return WriteTextToFile( pcszText ) ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
VOID CLogFileObj::WriteMemOneLine( PBYTE pByte , int nBytes , LPCTSTR szIndent )
{
	if ( !m_bEnableLog )
	{
		return;
	}

	if( nBytes <= 0 )
	{ // 没有字节需要输出
		return ;
	}
	
	TCHAR szOneLine[ MEM_BYTE_PER_LINE * 4 + 4 ] = { 0 } ;
	memset( szOneLine , 0x20 , sizeof( szOneLine ) - 2 ) ;	// 用空格填充
	
	for( int i = 0 ; i < nBytes ; i ++ )
	{
		// 先是内存块中字节的值
		_sntprintf_s( szOneLine + i * 3 , 2 , 1, TEXT( "%02X" ) , pByte[ i ] ) ;
		// 然后是字符形式的字节信息
		szOneLine[ MEM_BYTE_PER_LINE * 3 + 2 + i ] = isprint( pByte[ i ] ) ? pByte[ i ] : '.' ;
	}

	// 输出日志
	TCHAR szText[ 1024 ] = { 0 } ;
	_stprintf_s( szText , TEXT( "%s%s%s\n" ) , TEXT_SPACE , szIndent , szOneLine ) ;
	DoWriteLog( szText ) ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CBlockLogObj::CBlockLogObj( LPCTSTR lpcszBlockNameFormat , ... ) : CLogFileObj()
{
	// 获得要输出的文本
	TCHAR szText[ 1024 * 4 ] = { 0 } ;
	va_list arg ;
	va_start( arg , lpcszBlockNameFormat ) ;
	_vsntprintf_s( szText , sizeof( szText ) / sizeof( TCHAR ) - 1 , lpcszBlockNameFormat , arg ) ;
	va_end( arg ) ;

	// 输出文本
	WriteLog( szText ) ;
	WriteLog( TEXT( "{" ) ) ; 
	
	// 增加缩近
	OffsetIndent( 4 ) ;
	
	// 记录开始时间
	m_dwTickStart = GetTickCount() ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CBlockLogObj::~CBlockLogObj()
{
	// 记录结束时间
	DWORD dwTickEnd = GetTickCount() ;
	
	// 减少缩近
	OffsetIndent( -4 ) ;
	
	// 输出文本
	WriteLog( TEXT( "} // %d" ) , dwTickEnd - m_dwTickStart ) ; 
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CListLogObj::CListLogObj() : CLogFileObj()
{
	::InitializeCriticalSection( &m_csListLog ) ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
CListLogObj::~CListLogObj()
{
	::DeleteCriticalSection( &m_csListLog ) ;
}


// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CListLogObj::ResetLogList()
{

	__try
	{
        ResetLogListExe();
	}
	__finally
	{
		::LeaveCriticalSection( &m_csListLog ) ;
	}
}


void CListLogObj::ResetLogListExe()
{
    LogListIterator iter ;
    LPVOID pvItem = NULL ;

    ::EnterCriticalSection( &m_csListLog ) ;

    // 释放列表中元素的内存
    for( iter = m_LogList.begin() ; iter != m_LogList.end() ; iter ++ )
    { // 遍历这个列表的所有元素
        pvItem = ( LPVOID ) *iter ;
        free( pvItem ) ;
    }

    // 清除列表中的元素
    m_LogList.clear() ;
}

// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
BOOL CListLogObj::WriteLogListToFile( LPCTSTR pcszFormat , ... )
{
	__try
	{
        WriteLogListToFileExe(pcszFormat);
	}
	__finally
	{
		ResetLogList();
		::LeaveCriticalSection( &m_csListLog ) ;
	}

	return TRUE ;
}


void CListLogObj::WriteLogListToFileExe(LPCTSTR pcszFormat , ...)
{
    // 先把标题文本写入到文件中
    TCHAR szText[ 1024 ] = { 0 } ;
    va_list arg ;
    va_start( arg , pcszFormat ) ;
    _vsntprintf_s( szText , sizeof( szText ) / sizeof( TCHAR ) - 1 , pcszFormat , arg ) ;
    va_end( arg ) ;
    WriteLog( szText ) ;

    LogListIterator iter ;
    LPCTSTR pszText = NULL ;

    ::EnterCriticalSection( &m_csListLog ) ;

    // 逐一把列表中的文本写入到文件中
    for( iter = m_LogList.begin() ; iter != m_LogList.end() ; iter ++ )
    { // 遍历这个列表的所有元素
        pszText = *iter ;
        WriteTextToFile( pszText ) ;
    }
}

// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
BOOL CListLogObj::DoWriteLog( LPCTSTR pcszText )
{
	int nLen = _tcslen( pcszText ) + 1 ;	// 字符串长度
	LPTSTR pszItem = ( LPTSTR ) calloc( nLen , sizeof( TCHAR ) ) ;	// 分配内存块
	_tcscpy_s( pszItem , nLen, pcszText ) ;	// 拷贝字符串
	m_LogList.push_back( pszItem ) ;	// 把字符串加入列表中

	return TRUE ;
}


// 
// 成员函数的定义
// ************************************************************************************************************************

