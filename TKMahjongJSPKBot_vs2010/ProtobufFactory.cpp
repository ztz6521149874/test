#include "stdafx.h"
#include "ProtobufFactory.h"


ProtobufFactory::ProtobufFactory()
{
	// 默认不支持protobuf协议
	m_bUseProtobuf = false;
}

// 析构函数
ProtobufFactory::~ProtobufFactory()
{
}


ProtobufFactory& ProtobufFactory::GetInstance()
{
	static ProtobufFactory m_sProtobufFactory;
	return m_sProtobufFactory;
}

// 获取protobuf消息体
bool ProtobufFactory::GetAckMsg(const TKACKMAHJONGPROTOBUF* pMsg, MahJongTPAckMsg& ackMsg)
{
	int nMsgBodySize = pMsg->header.dwLength;

	BYTE* tempMsg = ((BYTE*)pMsg) + TKHEADERSIZE;

	TKMobileAckMsg tkMobileAckMsg;

	if (!tkMobileAckMsg.ParseFromArray(tempMsg, nMsgBodySize))
	{
		PROTOLOG("Error !!! ProtobufFactory::GetAckMsg parse failed!");
		return false;
	}

	if (tkMobileAckMsg.has_mahjongtp_ack_msg())
	{
		ackMsg.CopyFrom(tkMobileAckMsg.mahjongtp_ack_msg());
		return true;
	}

	return false;
}

// 收到的消息包进行转换成C++协议格式
BOOL ProtobufFactory::DeserializeProtobufMsg(const TKHEADER* pMsg, TKHEADER ** ppRetMsg)
{
	if (pMsg == nullptr)
	{
		PROTOLOG("ProtobufFactory::DeserializeNetAckMsg the msg is NULL, return");
		return FALSE;
	}

	if (pMsg->dwType != (TK_MSG_MAHJONG_PROTOBUF | TK_ACK))
	{
		PROTOLOG("ProtobufFactory::DeserializeProtobufMsg the msg type is not TK_MSG_MAHJONG_PROTOBUF");
		return FALSE;
	}

	// 设置此后都支持protobuf协议
	m_bUseProtobuf = true;

	PTKACKMAHJONGPROTOBUF pProtobufAckMsg = ( PTKACKMAHJONGPROTOBUF )pMsg;

	TKHEADER *pTransMsg = nullptr;
	MahJongTPAckMsg ackMsg;
	if ( !GetAckMsg( pProtobufAckMsg, ackMsg ) )
	{
		PROTOLOG("ProtobufFactory::DeserializeNetAckMsg GetAckMsg Error!!!");
		return FALSE;
	}

	// 设置本次比赛ID
	m_iMatchID = ackMsg.matchid();

	// 检测消息体
	if (ackMsg.has_mahjonggamebegin_ack_msg())
	{
		PROTOLOG("Protobuf[2] 比赛开始消息");
	}
	else if (ackMsg.has_mahjongruleinfo_ack_msg())
	{
		PROTOLOG("Protobuf[3] 游戏规则消息");

		*ppRetMsg = DeserializeMsgRulerInfoAck(pMsg, ackMsg);
	}
	else if (ackMsg.has_mahjongplace_ack_msg())
	{
		PROTOLOG("Protobuf[4] 游戏坐位、庄家消息");

		// 调用相应的解析函数
		*ppRetMsg = DeserializeMsgPlaceAck(pMsg, ackMsg);
	}
	else if (ackMsg.has_mahjongwindplace_ack_msg())
	{
		PROTOLOG("Protobuf[5] 门风、圈风消息"); 

		// 调用相应的解析函数
		*ppRetMsg = DeserializeMsgWindPlaceAck(pMsg, ackMsg);
	}
	else if (ackMsg.has_mahjongresult_ack_msg())
	{
		PROTOLOG("Protobuf[6] 结果消息");

		*ppRetMsg = DeserializeMsgResultTPAck( pMsg, ackMsg );
	}
	else if (ackMsg.has_mahjongshuffle_ack_msg())
	{
		PROTOLOG("Protobuf[7] 洗牌、砌牌消息"); 
		
		// 调用相应的解析函数
		*ppRetMsg = DeserializeMsgShuffleAck(pMsg, ackMsg);	
	}
	else if (ackMsg.has_mahjongcastdice_ack_msg())
	{
		PROTOLOG("Protobuf[8] 掷骰子消息");

		// 调用相应的解析函数
		*ppRetMsg = DeserializeMsgCastDice(pMsg, ackMsg);		
	}
	else if (ackMsg.has_mahjongopendoor_ack_msg())
	{
		PROTOLOG("Protobuf[9] 开牌消息"); 

		// 调用相应的解析函数
		*ppRetMsg = DeserializeMsgOpenDoorAck(pMsg, ackMsg);	
	}
	else if (ackMsg.has_mahjonghun_ack_msg())
	{
		PROTOLOG("Protobuf[10] 翻混消息"); 

		*ppRetMsg = DeserializeMsgHunAck( pMsg, ackMsg );
	}
	else if (ackMsg.has_mahjongaction_ack_msg())
	{
		PROTOLOG("Protobuf[11] 服务器请求客户端执行操作消息");

		*ppRetMsg = DeserializeMsgActionAck( pMsg, ackMsg );
	}
	else if (ackMsg.has_mahjongrequest_ack_msg())
	{
		PROTOLOG("Protobuf[12] 服务器向客户端广播其他玩家的操作消息");

		// 调用相应的解析函数
		*ppRetMsg = DeserializeMsgRequestAck( pMsg, ackMsg );
	}
	else if (ackMsg.has_mahjongchangeflower_ack_msg())
	{
		PROTOLOG("Protobuf[13] 通知补花消息");

		*ppRetMsg = DeserializeMsgChangeFlower( pMsg, ackMsg );
	}
	else if (ackMsg.has_mahjongdiscardtile_ack_msg())
	{
		PROTOLOG("Protobuf[14] 通知出牌消息");

		// 调用相应的解析函数
		*ppRetMsg = DeserializeMsgDiscardTile( pMsg, ackMsg );
	}
	else if (ackMsg.has_mahjongdrawtile_ack_msg())
	{
		PROTOLOG("Protobuf[15] 通知抓牌消息");

		// 调用相应的解析函数
		*ppRetMsg = DeserializeMsgDrawTile(pMsg, ackMsg);	
	}
	else if (ackMsg.has_mahjongcall_ack_msg())
	{
		PROTOLOG("Protobuf[16] 通知听牌消息");

		*ppRetMsg = DeserializeMsgCallAck( pMsg, ackMsg );
	}
	else if (ackMsg.has_mahjongchi_ack_msg())
	{
		PROTOLOG("Protobuf[17] 通知吃牌消息");

		*ppRetMsg = DeserializeMsgChiAck( pMsg, ackMsg );
	}
	else if (ackMsg.has_mahjongpeng_ack_msg())
	{
		PROTOLOG("Protobuf[18] 通知碰牌消息");

		*ppRetMsg = DeserializeMsgPengAck( pMsg, ackMsg );
	}
	else if (ackMsg.has_mahjonggang_ack_msg())
	{
		PROTOLOG("Protobuf[19] 通知杠牌消息");

		*ppRetMsg = DeserializeMsgGangAck( pMsg, ackMsg );
	}
	else if (ackMsg.has_mahjongwin_ack_msg())
	{
		PROTOLOG("Protobuf[20] 通知和牌消息");

		*ppRetMsg = DeserializeMsgWinTPAck( pMsg, ackMsg );
	}
	else if (ackMsg.has_mahjongwindetail_ack_msg())
	{
		PROTOLOG("Protobuf[21] 赢牌详细信息");

		*ppRetMsg = DeserializeMsgWinDetailAck( pMsg, ackMsg );
	}
	else if (ackMsg.has_mahjongwindetailex_ack_msg())
	{
		PROTOLOG("Protobuf[22] 赢牌详细信息");

		*ppRetMsg = DeserializeMsgWinDetailExTPAck( pMsg, ackMsg );
	}
	else if (ackMsg.has_mahjongnotify_ack_msg())
	{
		PROTOLOG("Protobuf[23] 通知消息");

		// 调用相应的解析函数
		*ppRetMsg = DeserializeMsgNotifyAck(pMsg, ackMsg);	
	}
	else if (ackMsg.has_mahjongtrustplay_ack_msg())
	{
		PROTOLOG("Protobuf[24] 托管消息");

		*ppRetMsg = DeserializeMsgTrustPlayAck( pMsg, ackMsg );
	}
	else if (ackMsg.has_mahjongscorechange_ack_msg())
	{
		PROTOLOG("Protobuf[25] 分数改变消息");

		*ppRetMsg = DeserializeMsgScoreChangeAck( pMsg, ackMsg );
	}
	else if (ackMsg.has_mahjongplayertiles_ack_msg())
	{
		PROTOLOG("Protobuf[26] 亮牌消息");

		*ppRetMsg = DeserializeMsgPlayerTilesAck( pMsg, ackMsg );
	}
	else if (ackMsg.has_mahjongdouble_ack_msg())
	{
		PROTOLOG("Protobuf[27] 加倍消息");

		*ppRetMsg = DeserializeMsgDoublingAck( pMsg, ackMsg );
	}
	else if (ackMsg.has_mahjongplayershowtiles_ack_msg())
	{
		PROTOLOG("Protobuf[28] 玩家吃碰杠的牌信息");

		*ppRetMsg = DeserializeMsgShowTiles( pMsg, ackMsg );
	}
	else if (ackMsg.has_mahjongtpspecialtile_ack_msg())
	{
		PROTOLOG("Protobuf[29] 加番牌信息");

		// 调用相应的解析函数
		*ppRetMsg = DeserializeMsgSpecialTileAck( pMsg, ackMsg );
	}
	else if (ackMsg.has_mahjongtpspecialcount_ack_msg())
	{
		PROTOLOG("Protobuf[30] 加番牌个数");

		*ppRetMsg = DeserializeMsgSpecialCountAck( pMsg, ackMsg );
	}

	if( ppRetMsg == NULL)
	{
		return FALSE;
	}

	return TRUE;
}

/************************************************************************/
/* 游戏规则消息                                                         */
/************************************************************************/
PTKHEADER ProtobufFactory::DeserializeMsgRulerInfoAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg)
{
	// 获取消息体
	auto ack = protoMsg.mahjongruleinfo_ack_msg();

	TKREQMAHJONGRULEINFO* pRulerInfoMsg = new TKREQMAHJONGRULEINFO();
	ASSERT( NULL != pRulerInfoMsg );
	memset( pRulerInfoMsg, 0, sizeof(TKREQMAHJONGRULEINFO) );

	pRulerInfoMsg->header.dwType   = ACK_TYPE( TK_MSG_MAHJONG_RULE_INFO ) ;
	pRulerInfoMsg->header.dwLength = MSG_LENGTH( TKREQMAHJONGRULEINFO ) ;
	pRulerInfoMsg->header.dwMagic = pMsg->dwMagic;
	pRulerInfoMsg->header.dwParam = pMsg->dwParam;
	pRulerInfoMsg->header.dwSerial = pMsg->dwSerial;
	pRulerInfoMsg->header.wOrigine = pMsg->wOrigine;
	pRulerInfoMsg->header.wReserve = pMsg->wReserve;

	pRulerInfoMsg->nRule          = ack.rule();
	pRulerInfoMsg->bWinSelfOnly   = ack.winselfonly();
	pRulerInfoMsg->nDiscardTime   = ack.discardtime();
	pRulerInfoMsg->nWaitTime      = ack.waittime();
	pRulerInfoMsg->nWinNeedMinFan = ack.winneedminfan();

	return (PTKHEADER) pRulerInfoMsg;
}


/************************************************************************/
/* 游戏坐位、庄家消息                                                   */
/************************************************************************/
PTKHEADER ProtobufFactory::DeserializeMsgPlaceAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg)
{
	// 获取消息体
	auto ack = protoMsg.mahjongplace_ack_msg();

	TKREQMAHJONGPLACE *pPlaceMsg = new TKREQMAHJONGPLACE();
	ASSERT( NULL != pPlaceMsg );
	memset( pPlaceMsg, 0, sizeof(TKREQMAHJONGPLACE) );

	pPlaceMsg->header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_PLACE ) ;
	pPlaceMsg->header.dwLength = MSG_LENGTH( TKREQMAHJONGPLACE ) ;
	pPlaceMsg->header.dwMagic = pMsg->dwMagic;
	pPlaceMsg->header.dwParam = pMsg->dwParam;
	pPlaceMsg->header.dwSerial = pMsg->dwSerial;
	pPlaceMsg->header.wOrigine = pMsg->wOrigine;
	pPlaceMsg->header.wReserve = pMsg->wReserve;

	pPlaceMsg->nBanker = ack.banker();
	pPlaceMsg->nRules = ack.rules();

	
	for (int i = 0; i < ack.gameseat_size(); i++) 
	{
		pPlaceMsg->anGameSeat[ i++ ] = ack.gameseat( i );
	}

	return (PTKHEADER)pPlaceMsg;
}

/************************************************************************/
/* 门风、圈风消息                                                       */
/************************************************************************/
PTKHEADER ProtobufFactory::DeserializeMsgWindPlaceAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg)
{
	// 获取消息体
	auto ack = protoMsg.mahjongwindplace_ack_msg();

	TKREQMAHJONGWINDPLACE *pReqWindPlace = new TKREQMAHJONGWINDPLACE();
	ASSERT( NULL != pReqWindPlace );
	memset( pReqWindPlace, 0, sizeof(TKREQMAHJONGWINDPLACE) );

	pReqWindPlace->header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_WIND_PLACE ) ;
	pReqWindPlace->header.dwLength = MSG_LENGTH( TKREQMAHJONGWINDPLACE ) ;
	pReqWindPlace->header.dwMagic = pMsg->dwMagic;
	pReqWindPlace->header.dwParam = pMsg->dwParam;
	pReqWindPlace->header.dwSerial = pMsg->dwSerial;
	pReqWindPlace->header.wOrigine = pMsg->wOrigine;
	pReqWindPlace->header.wReserve = pMsg->wReserve;

	pReqWindPlace->nRoundWind = ack.roundwind();

	return (PTKHEADER)pReqWindPlace;
}

/************************************************************************/
/* 结果消息，一局游戏结束时，服务器发送此消息给所有客户端                */
/************************************************************************/
PTKHEADER ProtobufFactory::DeserializeMsgResultTPAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg)
{
	// 获取消息体
	auto ack = protoMsg.mahjongresult_ack_msg();

	char* buf = new char[ 8192 ];
	ASSERT(NULL != buf);
	memset(buf, 0, 8192);

	PTKREQMAHJONGRESULT pResultMsg = ( PTKREQMAHJONGRESULT )buf;
	pResultMsg->header.dwType = REQ_TYPE( TK_MSG_MAHJONG_RESULTEX );
	pResultMsg->header.dwLength = MSG_LENGTH( TKREQMAHJONGRESULT ) + sizeof( TKACKMAHJONGWINDETAILEX ) * ack.wininfo();
	pResultMsg->header.dwMagic = pMsg->dwMagic;
	pResultMsg->header.dwParam = pMsg->dwParam;
	pResultMsg->header.dwSerial = pMsg->dwSerial;
	pResultMsg->header.wOrigine = pMsg->wOrigine;
	pResultMsg->header.wReserve = pMsg->wReserve;

	pResultMsg->cnWinInfo = ack.wininfo();
	pResultMsg->nTime = ack.time();

	
	for ( int iIndex = 0; iIndex < ack.playerresults_size(); iIndex ++ )
	{
		pResultMsg->asPlayerResult[ iIndex ].cnWin = ack.playerresults( iIndex ).win();
		pResultMsg->asPlayerResult[ iIndex ].cnPao = ack.playerresults( iIndex ).pao();
		pResultMsg->asPlayerResult[ iIndex ].cnGang = ack.playerresults( iIndex ).gang();
		pResultMsg->asPlayerResult[ iIndex ].nScore = ack.playerresults( iIndex ).score();
		pResultMsg->asPlayerResult[ iIndex ].nDouble = ack.playerresults( iIndex ).windouble();
		pResultMsg->asPlayerResult[ iIndex ].nTotalScore = ack.playerresults( iIndex ).totalscore();
		pResultMsg->asPlayerResult[ iIndex ].cnHandTile = ack.playerresults( iIndex ).handtilecount();

		
		for ( int j = 0; j < ack.playerresults( iIndex ).handtiles_size(); j++ )
		{
			pResultMsg->asPlayerResult[ iIndex ].anHandTile[ j ]  = ack.playerresults( iIndex ).handtiles( j );
		}
	}

	
	if ( pResultMsg->cnWinInfo > 0)
	{
		PTKACKMAHJONGWINDETAILEX pAckMahJongWin = ( PTKACKMAHJONGWINDETAILEX )( pResultMsg + 1 );

		for ( int nIndex = 0; nIndex < ack.windetail_size(); nIndex++ )
		{
			WinDetailExTP winDetailItem =  ack.windetail( nIndex );
			pAckMahJongWin->nWinSeat     = winDetailItem.winseat();
			pAckMahJongWin->winDouble    = winDetailItem.windouble();
			pAckMahJongWin->nPaoSeat     = winDetailItem.paoseat();
			pAckMahJongWin->nWinTile     = winDetailItem.wintile();
			pAckMahJongWin->nBaseScore   = winDetailItem.basescore();
			pAckMahJongWin->nTime        = winDetailItem.time();

			WinInfo sWinInfo             = winDetailItem.wininfo();
			pAckMahJongWin->sWinInfo.nMaxFans = sWinInfo.maxfans();

			for ( int i = 0; i < sWinInfo.fans_size(); i++ )
			{
				pAckMahJongWin->sWinInfo.anFans [ i ] = sWinInfo.fans( i );  
			}
			pAckMahJongWin->sWinInfo.nResultant = sWinInfo.resultant();

			for ( int i = 0; i < sWinInfo.groups_size(); i++)
			{
				StoneGroup stonegroup = sWinInfo.groups( i );
				for (int j = 0; j < stonegroup.stone_size(); j++ )
				{
					Stone stone = stonegroup.stone( j );
					pAckMahJongWin->sWinInfo.asGroup[ i ].asStone[ j ].nID = stone.id();
					pAckMahJongWin->sWinInfo.asGroup[ i ].asStone[ j ].nColor = stone.color();
					pAckMahJongWin->sWinInfo.asGroup[ i ].asStone[ j ].nWhat = stone.what();
				}
				pAckMahJongWin->sWinInfo.asGroup[ i ].nGroupStyle = stonegroup.groupstyle();
			}

			for ( int i = 0; i < sWinInfo.handstone_size(); i++ )
			{	
				pAckMahJongWin->sWinInfo.asHandStone[ i ].nID = sWinInfo.handstone( i ).id();
				pAckMahJongWin->sWinInfo.asHandStone[ i ].nColor = sWinInfo.handstone( i ).color();
				pAckMahJongWin->sWinInfo.asHandStone[ i ].nWhat = sWinInfo.handstone( i ).what();
			}

			pAckMahJongWin->sWinInfo.cnGroups    = sWinInfo.groupcount();
			pAckMahJongWin->sWinInfo.nHuTileID   = sWinInfo.wintileid();

			for ( int i = 0; i < sWinInfo.flowers_size(); i++ )
			{
				pAckMahJongWin->sWinInfo.asFlower[ i ].nID = sWinInfo.flowers( i ).id();
				pAckMahJongWin->sWinInfo.asFlower[ i ].nColor = sWinInfo.flowers( i ).color();
				pAckMahJongWin->sWinInfo.asFlower[ i ].nWhat = sWinInfo.flowers( i ).what();
			}

			pAckMahJongWin->sWinInfo.cnFlower    = sWinInfo.flowercount();
			pAckMahJongWin->sWinInfo.nQuanWind   = sWinInfo.quanwind();
			pAckMahJongWin->sWinInfo.nMenWind    = sWinInfo.menwind();
			pAckMahJongWin->sWinInfo.cnShowGroups = sWinInfo.showgroups();
			pAckMahJongWin->sWinInfo.nWinMode    = sWinInfo.winmode();
			pAckMahJongWin->sWinInfo.nScoreOfFan = sWinInfo.scoreoffan();

			for ( int i = 0; i < winDetailItem.scoredetails_size(); i++ )
			{
				ScoreDetailEx scoreDetail = winDetailItem.scoredetails( i );
				for ( int j = 0; j < scoreDetail.score_size(); j++ )
				{
					pAckMahJongWin->asScoreDetail[ i ].anScore[ j ] = scoreDetail.score( j ); 
				}
				pAckMahJongWin->asScoreDetail[ i ].nLeftScore         = scoreDetail.leftscore();
			}

			pAckMahJongWin ++;
		}	
	}
	
	return (PTKHEADER) pResultMsg;
}


/************************************************************************/
/* 洗牌、砌牌消息                                                       */
/************************************************************************/
PTKHEADER ProtobufFactory::DeserializeMsgShuffleAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg)
{
	// 获取消息体
	auto ack = protoMsg.mahjongshuffle_ack_msg();

	// 重组消息包
	TKREQMAHJONGSHUFFLE* pShuffleMsg =  new TKREQMAHJONGSHUFFLE();
	ASSERT(NULL != pShuffleMsg);
	memset(pShuffleMsg, 0, sizeof(TKREQMAHJONGSHUFFLE) );

	pShuffleMsg->header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_SHUFFLE ) ;
	pShuffleMsg->header.dwLength = MSG_LENGTH( TKREQMAHJONGSHUFFLE ) ;
	pShuffleMsg->header.dwMagic = pMsg->dwMagic;
	pShuffleMsg->header.dwParam = pMsg->dwParam;
	pShuffleMsg->header.dwSerial = pMsg->dwSerial;
	pShuffleMsg->header.wOrigine = pMsg->wOrigine;
	pShuffleMsg->header.wReserve = pMsg->wReserve;

	pShuffleMsg->cnTile = ack.tilecount();
	pShuffleMsg->cnMagicItem = ack.magicitem();

	
	for (int i = 0; i < ack.frustaofseat_size(); i++) 
	{
		pShuffleMsg->anFrustaOfSeat[ i++ ] = ack.frustaofseat( i );
	}

	return (PTKHEADER)pShuffleMsg;
}

/************************************************************************/
/* 掷骰子消息                                                           */
/************************************************************************/
PTKHEADER ProtobufFactory::DeserializeMsgCastDice(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg)
{
	// 获取消息体
	auto ack = protoMsg.mahjongcastdice_ack_msg();

	int len = sizeof( TKREQMAHJONGCASTDICE ) + sizeof( int ) * MAX_DICE_COUNT;
	// 重组消息包
	char* szTempCastDice = new char[ len ];
	ASSERT(NULL != szTempCastDice);
	memset(szTempCastDice, 0, len );

	PTKREQMAHJONGCASTDICE pReqMahJongDice = ( PTKREQMAHJONGCASTDICE ) szTempCastDice ;
	pReqMahJongDice->header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_CAST_DICE ) ;
	pReqMahJongDice->header.dwLength = MSG_LENGTH( TKREQMAHJONGCASTDICE ) + sizeof( int ) * MAX_DICE_COUNT;
	pReqMahJongDice->header.dwMagic = pMsg->dwMagic;
	pReqMahJongDice->header.dwParam = pMsg->dwParam;
	pReqMahJongDice->header.dwSerial = pMsg->dwSerial;
	pReqMahJongDice->header.wOrigine = pMsg->wOrigine;
	pReqMahJongDice->header.wReserve = pMsg->wReserve;

	pReqMahJongDice->nSeat = ack.seat();
	pReqMahJongDice->nCastDiceType = ack.castdicetype();
	pReqMahJongDice->cnDice = ack.dicecount();
	int *pnDice = ( int * ) ( pReqMahJongDice + 1 ) ;
	
	for (int i = 0; i < ack.dicevalue_size(); i++) 
	{
		pnDice[ i ] = ack.dicevalue( i );
	}

	return (PTKHEADER)pReqMahJongDice;
}

/************************************************************************/
/* 开牌消息                                                           */
/************************************************************************/
PTKHEADER ProtobufFactory::DeserializeMsgOpenDoorAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg)
{
	// 获取消息体
	auto ack = protoMsg.mahjongopendoor_ack_msg();

	int len = sizeof( TKREQMAHJONGOPENDOOR ) + sizeof( int ) * ( MAX_HAND_COUNT + 1 );
	char* szTempOpenDoor = new char[ len ];
	ASSERT(NULL != szTempOpenDoor);
	memset(szTempOpenDoor, 0, len );

	PTKREQMAHJONGOPENDOOR pReqMahJongOpenDoor = ( PTKREQMAHJONGOPENDOOR ) szTempOpenDoor ;
	pReqMahJongOpenDoor->header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_OPEN_DOOR ) ;
	pReqMahJongOpenDoor->header.dwLength = MSG_LENGTH( TKREQMAHJONGOPENDOOR ) + sizeof( int ) * ack.handtile();
	pReqMahJongOpenDoor->header.dwMagic = pMsg->dwMagic;
	pReqMahJongOpenDoor->header.dwParam = pMsg->dwParam;
	pReqMahJongOpenDoor->header.dwSerial = pMsg->dwSerial;
	pReqMahJongOpenDoor->header.wOrigine = pMsg->wOrigine;
	pReqMahJongOpenDoor->header.wReserve = pMsg->wReserve;

	pReqMahJongOpenDoor->nSeat = ack.seat();
	pReqMahJongOpenDoor->nOpenDoorSeat = ack.opendoorseat();
	pReqMahJongOpenDoor->nOpenDoorFrusta = ack.opendoorfrusta();
	pReqMahJongOpenDoor->cnHandTile = ack.handtile();

	int *pnTile = ( int * ) ( pReqMahJongOpenDoor + 1 ) ;	
	

	for (int i = 0; i < ack.tiles_size(); i++) 
	{
		pnTile[ i ] = ack.tiles( i );
	}

	return (PTKHEADER)pReqMahJongOpenDoor;
}

/************************************************************************/
/* 通知消息                                                           */
/************************************************************************/
PTKHEADER ProtobufFactory::DeserializeMsgNotifyAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg)
{
	// 获取消息体
	auto ack = protoMsg.mahjongnotify_ack_msg();

	int len = sizeof( TKREQMAHJONGNOTIFY ) + 1024;

	char* szTemp = new char[ len ];
	ASSERT(NULL != szTemp);
	memset(szTemp, 0, len );


	PTKREQMAHJONGNOTIFY pReqMahJongNotify = ( PTKREQMAHJONGNOTIFY ) szTemp ;

	pReqMahJongNotify->header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_NOTIFY ) ;
	pReqMahJongNotify->header.dwLength = MSG_LENGTH( TKREQMAHJONGNOTIFY ) ;
	pReqMahJongNotify->header.dwMagic = pMsg->dwMagic;
	pReqMahJongNotify->header.dwParam = pMsg->dwParam;
	pReqMahJongNotify->header.dwSerial = pMsg->dwSerial;
	pReqMahJongNotify->header.wOrigine = pMsg->wOrigine;
	pReqMahJongNotify->header.wReserve = pMsg->wReserve;

	pReqMahJongNotify->nNotify         = ack.notify();

	// 指到消息尾
	char *pEnd = ( char* ) ( pReqMahJongNotify + 1 ) ;

	int nLen = 0 ;
	if( ack.has_notifytype() )
	{ // 暂停接收网络消息
		nLen = sizeof( int ) ;	// 消息后面跟的是一个int

		int iNotifyType = ack.notifytype();

		memcpy( pEnd, &iNotifyType, nLen);
	}
	else if ( ack.has_notifystr() )
	{ // 其他情况
		// 			ASSERT( MAHJONG_NOTIFY_SHOW_CHAT_MSG == nNotify || MAHJONG_NOTIFY_URL_HELP == nNotify 
		// 				|| MAHJONG_NOTIFY_URL_MEMBER == nNotify || MAHJONG_NOTIFY_URL_MONEY == nNotify 
		// 				|| MAHJONG_NOTIFY_URL_FEEDBACK == nNotify  || MAHJONG_NOTIFY_ERROR == nNotify ) ;

		nLen = pMsg->dwLength - MSG_LENGTH( TKREQMAHJONGNOTIFY ) ;

		std::string strNotifyStr = ack.notifystr();

		memcpy( pEnd, &strNotifyStr, nLen);
	}


	pReqMahJongNotify->header.dwLength += nLen ;

	return (PTKHEADER)pReqMahJongNotify;
}


/************************************************************************/
/* 通知补花消息                                                          */
/************************************************************************/
PTKHEADER ProtobufFactory::DeserializeMsgChangeFlower(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg)
{
	// 获取消息体
	auto ack = protoMsg.mahjongchangeflower_ack_msg();

	// 重组消息包
	TKREQMAHJONGCHANGEFLOWER* pChangeFlowerMsg = new TKREQMAHJONGCHANGEFLOWER();
	ASSERT( NULL != pChangeFlowerMsg );
	memset(pChangeFlowerMsg , 0, sizeof(TKREQMAHJONGCHANGEFLOWER) );

	pChangeFlowerMsg->header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_CHANGE_FLOWER ) ;
	pChangeFlowerMsg->header.dwLength = MSG_LENGTH( TKREQMAHJONGCHANGEFLOWER ) ;
	pChangeFlowerMsg->header.dwMagic = pMsg->dwMagic;
	pChangeFlowerMsg->header.dwParam = pMsg->dwParam;
	pChangeFlowerMsg->header.dwSerial = pMsg->dwSerial;
	pChangeFlowerMsg->header.wOrigine = pMsg->wOrigine;
	pChangeFlowerMsg->header.wReserve = pMsg->wReserve;

	pChangeFlowerMsg->nSeat = ack.seat();	
	pChangeFlowerMsg->nRequestID = ack.requestid();
	pChangeFlowerMsg->nTileID = ack.tileid();

	return (PTKHEADER)pChangeFlowerMsg;
}



/************************************************************************/
/* 通知抓牌消息                                                           */
/************************************************************************/
PTKHEADER ProtobufFactory::DeserializeMsgDrawTile(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg)
{
	// 获取消息体
	auto ack = protoMsg.mahjongdrawtile_ack_msg();

	// 重组消息包
	TKREQMAHJONGDRAWTILE* pDrawTitleMsg = new TKREQMAHJONGDRAWTILE();
	ASSERT(NULL != pDrawTitleMsg);
	memset(pDrawTitleMsg, 0, sizeof(TKREQMAHJONGDRAWTILE));

	pDrawTitleMsg->header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_DRAW_TILE ) ;
	pDrawTitleMsg->header.dwLength = MSG_LENGTH( TKREQMAHJONGDRAWTILE ) ;
	pDrawTitleMsg->header.dwMagic = pMsg->dwMagic;
	pDrawTitleMsg->header.dwParam = pMsg->dwParam;
	pDrawTitleMsg->header.dwSerial = pMsg->dwSerial;
	pDrawTitleMsg->header.wOrigine = pMsg->wOrigine;
	pDrawTitleMsg->header.wReserve = pMsg->wReserve;

	pDrawTitleMsg->nSeat = ack.seat();	
	pDrawTitleMsg->nDrawTileType = ack.drawtiletype();
	pDrawTitleMsg->bFromWallHeader = ack.fromwallheader();
	pDrawTitleMsg->nTileOffset = ack.tileoffset();
	pDrawTitleMsg->nTileID = ack.tileid();

	return (PTKHEADER)pDrawTitleMsg;
}

/************************************************************************/
/* 服务器向客户端广播其他玩家的操作消息                                 */
/************************************************************************/
PTKHEADER ProtobufFactory::DeserializeMsgActionAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg)
{
	// 获取消息体
	auto req = protoMsg.mahjongaction_ack_msg();

	// 重组消息包
	TKREQMAHJONGREQUEST* pRequestMsg = new TKREQMAHJONGREQUEST();
	ASSERT(NULL != pRequestMsg);
	memset(pRequestMsg, 0, sizeof(TKREQMAHJONGREQUEST));

	pRequestMsg->header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_REQUEST ) ;
	pRequestMsg->header.dwLength = MSG_LENGTH( TKREQMAHJONGREQUEST ) ;
	pRequestMsg->header.dwMagic = pMsg->dwMagic;
	pRequestMsg->header.dwParam = pMsg->dwParam;
	pRequestMsg->header.dwSerial = pMsg->dwSerial;
	pRequestMsg->header.wOrigine = pMsg->wOrigine;
	pRequestMsg->header.wReserve = pMsg->wReserve;

	pRequestMsg->nSeat		 = req.seat();	
	pRequestMsg->nRequestID    = req.requestid();
	pRequestMsg->nRequestType  = req.requesttype();
	pRequestMsg->nWaitSecond   = req.waitsecond();


	return (PTKHEADER)pRequestMsg;
}


/************************************************************************/
/* 服务器向客户端广播其他玩家的操作消息                                 */
/************************************************************************/
PTKHEADER ProtobufFactory::DeserializeMsgRequestAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg)
{
	// 获取消息体
	auto ack = protoMsg.mahjongrequest_ack_msg();

	// 重组消息包
	TKACKMAHJONGREQUEST* pRequestMsg = new TKACKMAHJONGREQUEST();
	ASSERT(NULL != pRequestMsg);
	memset(pRequestMsg, 0, sizeof(TKREQMAHJONGREQUEST));

	pRequestMsg->header.dwType   = ACK_TYPE( TK_MSG_MAHJONG_REQUEST ) ;
	pRequestMsg->header.dwLength = MSG_LENGTH( TKREQMAHJONGREQUEST ) ;
	pRequestMsg->header.dwMagic = pMsg->dwMagic;
	pRequestMsg->header.dwParam = pMsg->dwParam;
	pRequestMsg->header.dwSerial = pMsg->dwSerial;
	pRequestMsg->header.wOrigine = pMsg->wOrigine;
	pRequestMsg->header.wReserve = pMsg->wReserve;

	pRequestMsg->nSeat		 = ack.seat();	
	pRequestMsg->nRequestID    = ack.requestid();
	pRequestMsg->nGiveUpRequestType  = ack.giveuprequesttype();

	return (PTKHEADER)pRequestMsg;
}

/************************************************************************/
/* 通知出牌消息                                 */
/************************************************************************/
PTKHEADER ProtobufFactory::DeserializeMsgDiscardTile(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg)
{
	// 获取消息体
	auto ack = protoMsg.mahjongdiscardtile_ack_msg();

	// 重组消息包
	TKREQMAHJONGDISCARDTILE* pDiscardTileMsg = new TKREQMAHJONGDISCARDTILE();
	ASSERT(NULL != pDiscardTileMsg);
	memset(pDiscardTileMsg, 0, sizeof(TKREQMAHJONGDISCARDTILE));

	pDiscardTileMsg->header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_DISCARD_TILE ) ;
	pDiscardTileMsg->header.dwLength = MSG_LENGTH( TKREQMAHJONGDISCARDTILE ) ;
	pDiscardTileMsg->header.dwMagic = pMsg->dwMagic;
	pDiscardTileMsg->header.dwParam = pMsg->dwParam;
	pDiscardTileMsg->header.dwSerial = pMsg->dwSerial;
	pDiscardTileMsg->header.wOrigine = pMsg->wOrigine;
	pDiscardTileMsg->header.wReserve = pMsg->wReserve;

	pDiscardTileMsg->nSeat		= ack.seat();	
	pDiscardTileMsg->nRequestID	= ack.requestid();
	pDiscardTileMsg->nTileID		= ack.tileid();

	return (PTKHEADER)pDiscardTileMsg;
}

/************************************************************************/
/*  翻混消息                                                            */
/************************************************************************/
PTKHEADER ProtobufFactory::DeserializeMsgHunAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg)
{
	// 获取消息体
	auto ack = protoMsg.mahjonghun_ack_msg();

	int len = sizeof( TKREQMAHJONGHUN ) + sizeof( int ) * 4;
	// 重组消息包
	char* szTempHun = new char[ len ];
	ASSERT(NULL != szTempHun);
	memset(szTempHun, 0, len );

	PTKREQMAHJONGHUN pReqMahJongHun = ( PTKREQMAHJONGHUN ) szTempHun ;
	pReqMahJongHun->header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_HUN ) ;
	pReqMahJongHun->header.dwLength = MSG_LENGTH( TKREQMAHJONGHUN ) + sizeof( int ) * ack.huntilecount();;
	pReqMahJongHun->header.dwMagic = pMsg->dwMagic;
	pReqMahJongHun->header.dwParam = pMsg->dwParam;
	pReqMahJongHun->header.dwSerial = pMsg->dwSerial;
	pReqMahJongHun->header.wOrigine = pMsg->wOrigine;
	pReqMahJongHun->header.wReserve = pMsg->wReserve;

	pReqMahJongHun->bFromWallHeader		= ack.fromwallheader();
	pReqMahJongHun->nTileOffset	        = ack.tileoffset();
	pReqMahJongHun->nTileID		        = ack.tileid();
	pReqMahJongHun->cnHunTile           = ack.huntilecount();

	// 记录混牌
	int * pHun = ( int * )( pReqMahJongHun + 1 );

	
	for ( int i = 0; i < ack.hunttileids_size(); i++)
	{
		pHun [ i ] = ack.hunttileids( i );
	}

	return (PTKHEADER)pReqMahJongHun;
}

/************************************************************************/
/*  通知听牌消息                                                            */
/************************************************************************/
PTKHEADER ProtobufFactory::DeserializeMsgCallAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg)
{
	// 获取消息体
	auto ack = protoMsg.mahjongcall_ack_msg();

	// 重组消息包
	TKREQMAHJONGCALL* pMahJongCallAck = new TKREQMAHJONGCALL();
	ASSERT(pMahJongCallAck != NULL);
	memset(pMahJongCallAck, 0, sizeof(TKREQMAHJONGCALL));

	pMahJongCallAck->header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_CALL ) ;
	pMahJongCallAck->header.dwLength = MSG_LENGTH( TKREQMAHJONGCALL ) ;
	pMahJongCallAck->header.dwMagic = pMsg->dwMagic;
	pMahJongCallAck->header.dwParam = pMsg->dwParam;
	pMahJongCallAck->header.dwSerial = pMsg->dwSerial;
	pMahJongCallAck->header.wOrigine = pMsg->wOrigine;
	pMahJongCallAck->header.wReserve = pMsg->wReserve;

	pMahJongCallAck->nSeat        = ack.seat();
	pMahJongCallAck->nRequestID   = ack.requestid();
	pMahJongCallAck->nTileID      = ack.tileid();
	pMahJongCallAck->bWinSelf     = ack.winself();
	pMahJongCallAck->bAutoGang    = ack.autogang();
	pMahJongCallAck->nCallType    = ack.calltype();
	pMahJongCallAck->bHide        = ack.hide();

	return (PTKHEADER)pMahJongCallAck;
}


/************************************************************************/
/*  通知吃牌消息                                                            */
/************************************************************************/
PTKHEADER ProtobufFactory::DeserializeMsgChiAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg)
{
	// 获取消息体
	auto ack = protoMsg.mahjongchi_ack_msg();

	// 重组消息包
	TKREQMAHJONGCHI* pMahJongChiAck = new TKREQMAHJONGCHI();
	ASSERT(pMahJongChiAck != NULL);
	memset(pMahJongChiAck, 0, sizeof(TKREQMAHJONGCHI));

	pMahJongChiAck->header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_CHI ) ;
	pMahJongChiAck->header.dwLength = MSG_LENGTH( TKREQMAHJONGCHI ) ;
	pMahJongChiAck->header.dwMagic = pMsg->dwMagic;
	pMahJongChiAck->header.dwParam = pMsg->dwParam;
	pMahJongChiAck->header.dwSerial = pMsg->dwSerial;
	pMahJongChiAck->header.wOrigine = pMsg->wOrigine;
	pMahJongChiAck->header.wReserve = pMsg->wReserve;

	pMahJongChiAck->nSeat        = ack.seat();
	pMahJongChiAck->nRequestID   = ack.requestid();
	pMahJongChiAck->nTileID      = ack.tileid();

	
	for (int i = 0; i < ack.chitileid_size(); i++ )
	{
		pMahJongChiAck->anChiTileID [ i ] = ack.chitileid( i ); 
	}

	return (PTKHEADER)pMahJongChiAck;
}

/************************************************************************/
/*  通知碰牌消息                                                            */
/************************************************************************/
PTKHEADER ProtobufFactory::DeserializeMsgPengAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg)
{
	// 获取消息体
	auto ack = protoMsg.mahjongpeng_ack_msg();

	// 重组消息包
	TKREQMAHJONGPENG *pMahJongPengAck = new TKREQMAHJONGPENG();
	ASSERT(pMahJongPengAck != NULL);
	memset(pMahJongPengAck, 0, sizeof(TKREQMAHJONGPENG));

	pMahJongPengAck->header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_PENG ) ;
	pMahJongPengAck->header.dwLength = MSG_LENGTH( TKREQMAHJONGPENG ) ;
	pMahJongPengAck->header.dwMagic = pMsg->dwMagic;
	pMahJongPengAck->header.dwParam = pMsg->dwParam;
	pMahJongPengAck->header.dwSerial = pMsg->dwSerial;
	pMahJongPengAck->header.wOrigine = pMsg->wOrigine;
	pMahJongPengAck->header.wReserve = pMsg->wReserve;

	pMahJongPengAck->nSeat        = ack.seat();
	pMahJongPengAck->nRequestID   = ack.requestid();
	pMahJongPengAck->nTileID      = ack.tileid();

	for (int i = 0; i < ack.pengtileid_size(); i++ )
	{
		pMahJongPengAck->anPengTileID[ i ] = ack.pengtileid( i ); 
	}

	return (PTKHEADER)pMahJongPengAck;
}

/************************************************************************/
/*  通知杠牌消息                                                            */
/************************************************************************/
PTKHEADER ProtobufFactory::DeserializeMsgGangAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg)
{
	// 获取消息体
	auto ack = protoMsg.mahjonggang_ack_msg();

	// 重组消息包
	TKREQMAHJONGGANG* pMahJongGangAck = new TKREQMAHJONGGANG();
	ASSERT(pMahJongGangAck != NULL);
	memset(pMahJongGangAck, 0, sizeof(TKREQMAHJONGGANG));

	pMahJongGangAck->header.dwType   = REQ_TYPE( TK_MSG_MAHJONG_GANG ) ;
	pMahJongGangAck->header.dwLength = MSG_LENGTH( TKREQMAHJONGGANG ) ;
	pMahJongGangAck->header.dwMagic = pMsg->dwMagic;
	pMahJongGangAck->header.dwParam = pMsg->dwParam;
	pMahJongGangAck->header.dwSerial = pMsg->dwSerial;
	pMahJongGangAck->header.wOrigine = pMsg->wOrigine;
	pMahJongGangAck->header.wReserve = pMsg->wReserve;

	pMahJongGangAck->nSeat        = ack.seat();
	pMahJongGangAck->nRequestID   = ack.requestid();
	pMahJongGangAck->nTileID      = ack.tileid();
	pMahJongGangAck->nGangType    = ack.gangtype();

	
	for (int i = 0; i < ack.gangtileid_size(); i++ )
	{
		pMahJongGangAck->anGangTileID[ i++ ] = ack.gangtileid( i ); 
	}

	return (PTKHEADER)pMahJongGangAck;
}

/************************************************************************/
/*  通知和牌消息                                                            */
/************************************************************************/
PTKHEADER ProtobufFactory::DeserializeMsgWinTPAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg)
{
	// 获取消息体
	auto ack = protoMsg.mahjongwin_ack_msg();

	// 重组消息包
	TKACKMAHJONGWIN *pMahJongWinTPAck = new TKACKMAHJONGWIN();
	ASSERT(pMahJongWinTPAck != NULL);
	memset(pMahJongWinTPAck, 0, sizeof(TKACKMAHJONGWIN));

	pMahJongWinTPAck->header.dwType   = ACK_TYPE( TK_MSG_MAHJONG_WIN ) ;
	pMahJongWinTPAck->header.dwLength = MSG_LENGTH( TKACKMAHJONGWIN ) ;
	pMahJongWinTPAck->header.dwMagic = pMsg->dwMagic;
	pMahJongWinTPAck->header.dwParam = pMsg->dwParam;
	pMahJongWinTPAck->header.dwSerial = pMsg->dwSerial;
	pMahJongWinTPAck->header.wOrigine = pMsg->wOrigine;
	pMahJongWinTPAck->header.wReserve = pMsg->wReserve;

	pMahJongWinTPAck->cnWinSeat        = ack.winseatcount();
	pMahJongWinTPAck->nPaoSeat         = ack.paoseat();
	pMahJongWinTPAck->nWinTile         = ack.wintileid();

	
	for (int i = 0; i < ack.winseats_size(); i++ )
	{
		pMahJongWinTPAck->anWinSeat[ i ] = ack.winseats( i ); 
	}

	for (int i = 0; i < ack.windouble_size(); i++ )
	{
		pMahJongWinTPAck->cnDouble[ i ] = ack.windouble( i ); 
	}

	for (int i = 0; i < ack.winmode_size(); i++ )
	{
		pMahJongWinTPAck->anWinMode[ i ] = ack.winmode( i ); 
	}

	return (PTKHEADER)pMahJongWinTPAck;
}

/************************************************************************/
/*  赢牌详细信息                                                            */
/************************************************************************/
PTKHEADER ProtobufFactory::DeserializeMsgWinDetailAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg)
{
	// 获取消息体
	auto ack = protoMsg.mahjongwindetail_ack_msg();

	TKACKMAHJONGWINDETAIL* pWinDetailMsg = new TKACKMAHJONGWINDETAIL();
	ASSERT(pWinDetailMsg != NULL);
	memset(pWinDetailMsg, 0, sizeof(TKACKMAHJONGWINDETAIL));

	pWinDetailMsg->header.dwType = ACK_TYPE( TK_MSG_MAHJONG_WINDETAIL );
	pWinDetailMsg->header.dwLength = MSG_LENGTH( TKACKMAHJONGWINDETAIL );
	pWinDetailMsg->header.dwMagic = pMsg->dwMagic;
	pWinDetailMsg->header.dwParam = pMsg->dwParam;
	pWinDetailMsg->header.dwSerial = pMsg->dwSerial;
	pWinDetailMsg->header.wOrigine = pMsg->wOrigine;
	pWinDetailMsg->header.wReserve = pMsg->wReserve;

	pWinDetailMsg->nWinSeat     = ack.winseat();
	pWinDetailMsg->nPaoSeat     = ack.paoseat();
	pWinDetailMsg->nWinTile     = ack.wintile();
	pWinDetailMsg->nBaseScore   = ack.basescore();
	pWinDetailMsg->nTime        = ack.time();

	WinInfo sWinInfo             = ack.wininfo();
	pWinDetailMsg->sWinInfo.nMaxFans = sWinInfo.maxfans();

	pWinDetailMsg->sWinInfo.nMaxFans = sWinInfo.maxfans();

	for ( int i = 0; i < sWinInfo.fans_size(); i++ )
	{
		pWinDetailMsg->sWinInfo.anFans [ i ] = sWinInfo.fans( i );  
	}
	pWinDetailMsg->sWinInfo.nResultant = sWinInfo.resultant();

	for ( int i = 0; i < sWinInfo.groups_size(); i++)
	{
		StoneGroup stonegroup = sWinInfo.groups( i );
		for (int j = 0; j < stonegroup.stone_size(); j++ )
		{
			Stone stone = stonegroup.stone( j );
			pWinDetailMsg->sWinInfo.asGroup[ i ].asStone[ j ].nID = stone.id();
			pWinDetailMsg->sWinInfo.asGroup[ i ].asStone[ j ].nColor = stone.color();
			pWinDetailMsg->sWinInfo.asGroup[ i ].asStone[ j ].nWhat = stone.what();
		}
		pWinDetailMsg->sWinInfo.asGroup[ i ].nGroupStyle = stonegroup.groupstyle();
	}

	for ( int i = 0; i < sWinInfo.handstone_size(); i++ )
	{	
		pWinDetailMsg->sWinInfo.asHandStone[ i ].nID = sWinInfo.handstone( i ).id();
		pWinDetailMsg->sWinInfo.asHandStone[ i ].nColor = sWinInfo.handstone( i ).color();
		pWinDetailMsg->sWinInfo.asHandStone[ i ].nWhat = sWinInfo.handstone( i ).what();
	}

	pWinDetailMsg->sWinInfo.cnGroups    = sWinInfo.groupcount();
	pWinDetailMsg->sWinInfo.nHuTileID   = sWinInfo.wintileid();

	for ( int i = 0; i < sWinInfo.flowers_size(); i++ )
	{
		pWinDetailMsg->sWinInfo.asFlower[ i ].nID = sWinInfo.flowers( i ).id();
		pWinDetailMsg->sWinInfo.asFlower[ i ].nColor = sWinInfo.flowers( i ).color();
		pWinDetailMsg->sWinInfo.asFlower[ i ].nWhat = sWinInfo.flowers( i ).what();
	}

	pWinDetailMsg->sWinInfo.cnFlower    = sWinInfo.flowercount();
	pWinDetailMsg->sWinInfo.nQuanWind   = sWinInfo.quanwind();
	pWinDetailMsg->sWinInfo.nMenWind    = sWinInfo.menwind();
	pWinDetailMsg->sWinInfo.cnShowGroups = sWinInfo.showgroups();
	pWinDetailMsg->sWinInfo.nWinMode    = sWinInfo.winmode();
	pWinDetailMsg->sWinInfo.nScoreOfFan = sWinInfo.scoreoffan();

	for ( int i = 0; i < ack.scoredetails_size(); i++ )
	{
		ScoreDetail scoreDetail = ack.scoredetails( i );
		for ( int j = 0; j < scoreDetail.score_size(); j++ )
		{
			pWinDetailMsg->asScoreDetail[ i ].anScore[ j ] = scoreDetail.score( j ); 
		}
		pWinDetailMsg->asScoreDetail[ i ].nLeftScore         = scoreDetail.leftscore();
	}

	return (PTKHEADER)pWinDetailMsg;
}

/************************************************************************/
/*  赢牌详细信息                                                            */
/************************************************************************/
PTKHEADER ProtobufFactory::DeserializeMsgWinDetailExTPAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg)
{
	// 获取消息体
	auto ack = protoMsg.mahjongwindetailex_ack_msg();

	TKACKMAHJONGWINDETAILEX* pWinDetailExMsg = new TKACKMAHJONGWINDETAILEX();
	ASSERT(pWinDetailExMsg != NULL);
	memset(pWinDetailExMsg, 0, sizeof(TKACKMAHJONGWINDETAILEX));

	pWinDetailExMsg->header.dwType = ACK_TYPE( TK_MSG_MAHJONG_WINDETAILEX );
	pWinDetailExMsg->header.dwLength = MSG_LENGTH( TKACKMAHJONGWINDETAILEX );
	pWinDetailExMsg->header.dwMagic = pMsg->dwMagic;
	pWinDetailExMsg->header.dwParam = pMsg->dwParam;
	pWinDetailExMsg->header.dwSerial = pMsg->dwSerial;
	pWinDetailExMsg->header.wOrigine = pMsg->wOrigine;
	pWinDetailExMsg->header.wReserve = pMsg->wReserve;

	pWinDetailExMsg->nWinSeat     = ack.winseat();
	pWinDetailExMsg->winDouble    = ack.windouble();
	pWinDetailExMsg->nPaoSeat     = ack.paoseat();
	pWinDetailExMsg->nWinTile     = ack.wintile();
	pWinDetailExMsg->nBaseScore   = ack.basescore();
	pWinDetailExMsg->nTime        = ack.time();

	WinInfo sWinInfo             = ack.wininfo();
	pWinDetailExMsg->sWinInfo.nMaxFans = sWinInfo.maxfans();

	pWinDetailExMsg->sWinInfo.nMaxFans = sWinInfo.maxfans();

	for ( int i = 0; i < sWinInfo.fans_size(); i++ )
	{
		pWinDetailExMsg->sWinInfo.anFans [ i ] = sWinInfo.fans( i );  
	}
	pWinDetailExMsg->sWinInfo.nResultant = sWinInfo.resultant();

	for ( int i = 0; i < sWinInfo.groups_size(); i++)
	{
		StoneGroup stonegroup = sWinInfo.groups( i );
		for (int j = 0; j < stonegroup.stone_size(); j++ )
		{
			Stone stone = stonegroup.stone( j );
			pWinDetailExMsg->sWinInfo.asGroup[ i ].asStone[ j ].nID = stone.id();
			pWinDetailExMsg->sWinInfo.asGroup[ i ].asStone[ j ].nColor = stone.color();
			pWinDetailExMsg->sWinInfo.asGroup[ i ].asStone[ j ].nWhat = stone.what();
		}
		pWinDetailExMsg->sWinInfo.asGroup[ i ].nGroupStyle = stonegroup.groupstyle();
	}

	for ( int i = 0; i < sWinInfo.handstone_size(); i++ )
	{	
		pWinDetailExMsg->sWinInfo.asHandStone[ i ].nID = sWinInfo.handstone( i ).id();
		pWinDetailExMsg->sWinInfo.asHandStone[ i ].nColor = sWinInfo.handstone( i ).color();
		pWinDetailExMsg->sWinInfo.asHandStone[ i ].nWhat = sWinInfo.handstone( i ).what();
	}

	pWinDetailExMsg->sWinInfo.cnGroups    = sWinInfo.groupcount();
	pWinDetailExMsg->sWinInfo.nHuTileID   = sWinInfo.wintileid();

	for ( int i = 0; i < sWinInfo.flowers_size(); i++ )
	{
		pWinDetailExMsg->sWinInfo.asFlower[ i ].nID = sWinInfo.flowers( i ).id();
		pWinDetailExMsg->sWinInfo.asFlower[ i ].nColor = sWinInfo.flowers( i ).color();
		pWinDetailExMsg->sWinInfo.asFlower[ i ].nWhat = sWinInfo.flowers( i ).what();
	}

	pWinDetailExMsg->sWinInfo.cnFlower    = sWinInfo.flowercount();
	pWinDetailExMsg->sWinInfo.nQuanWind   = sWinInfo.quanwind();
	pWinDetailExMsg->sWinInfo.nMenWind    = sWinInfo.menwind();
	pWinDetailExMsg->sWinInfo.cnShowGroups = sWinInfo.showgroups();
	pWinDetailExMsg->sWinInfo.nWinMode    = sWinInfo.winmode();
	pWinDetailExMsg->sWinInfo.nScoreOfFan = sWinInfo.scoreoffan();

	for ( int i = 0; i < ack.scoredetails_size(); i++ )
	{
		const ScoreDetailEx scoreDetail = ack.scoredetails( i );
		for ( int j = 0; j < scoreDetail.score_size(); j++ )
		{
			pWinDetailExMsg->asScoreDetail[ i ].anScore[ j ] = scoreDetail.score( j ); 
		}
		pWinDetailExMsg->asScoreDetail[ i ].nLeftScore         = scoreDetail.leftscore();
	}

	return (PTKHEADER)pWinDetailExMsg;
}

/************************************************************************/
/*  托管消息                                                            */
/************************************************************************/
PTKHEADER ProtobufFactory::DeserializeMsgTrustPlayAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg)
{
	// 获取消息体
	auto ack = protoMsg.mahjongtrustplay_ack_msg();

	// 重组消息包
	TKREQTRUSTPLAY *pTrustPlayMsg = new TKREQTRUSTPLAY();
	ASSERT(pTrustPlayMsg != NULL);
	memset(pTrustPlayMsg, 0, sizeof(TKREQTRUSTPLAY));

	pTrustPlayMsg->header.dwType   = REQ_TYPE( TK_MSG_TRUSTPLAY ) ;
	pTrustPlayMsg->header.dwLength = MSG_LENGTH( TKREQTRUSTPLAY ) ;
	pTrustPlayMsg->header.dwMagic = pMsg->dwMagic;
	pTrustPlayMsg->header.dwParam = pMsg->dwParam;
	pTrustPlayMsg->header.dwSerial = pMsg->dwSerial;
	pTrustPlayMsg->header.wOrigine = pMsg->wOrigine;
	pTrustPlayMsg->header.wReserve = pMsg->wReserve;

	pTrustPlayMsg->dwUserID   = ack.userid();
	pTrustPlayMsg->dwType     = ack.type();

	return (PTKHEADER)pTrustPlayMsg;
}

/************************************************************************/
/*  分数改变消息                                                            */
/************************************************************************/
PTKHEADER ProtobufFactory::DeserializeMsgScoreChangeAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg)
{
	// 获取消息体
	auto ack = protoMsg.mahjongscorechange_ack_msg();

	// 重组消息包
	TKREQSCORECHANGE *pScoreChangeMsg = new TKREQSCORECHANGE();
	ASSERT(pScoreChangeMsg != NULL);
	memset(pScoreChangeMsg, 0, sizeof(TKREQSCORECHANGE));

	pScoreChangeMsg->header.dwType   = REQ_TYPE( TK_MSG_SCORE_CHANGE );
	pScoreChangeMsg->header.dwLength = MSG_LENGTH( TKREQSCORECHANGE ) ;
	pScoreChangeMsg->header.dwMagic = pMsg->dwMagic;
	pScoreChangeMsg->header.dwParam = pMsg->dwParam;
	pScoreChangeMsg->header.dwSerial = pMsg->dwSerial;
	pScoreChangeMsg->header.wOrigine = pMsg->wOrigine;
	pScoreChangeMsg->header.wReserve = pMsg->wReserve;

	
	for (int i = 0; i < ack.incremental_size(); i++ )
	{
		pScoreChangeMsg->anIncremental[ i ] = ack.incremental( i );
	}

	return (PTKHEADER)pScoreChangeMsg;
}

/************************************************************************/
/* 通知亮牌消息                                 */
/************************************************************************/
PTKHEADER ProtobufFactory::DeserializeMsgPlayerTilesAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg)
{
	// 获取消息体
	auto ack = protoMsg.mahjongplayertiles_ack_msg();

	// 重组消息包
	TKACKTILES *pPlayerTilesMsg = new TKACKTILES();
	ASSERT(pPlayerTilesMsg != NULL);
	memset(pPlayerTilesMsg, 0, sizeof(TKACKTILES));

	pPlayerTilesMsg->header.dwType   = ACK_TYPE( TK_MSG_PLAYER_TILES ) ;
	pPlayerTilesMsg->header.dwLength = MSG_LENGTH( TKACKTILES ) ;
	pPlayerTilesMsg->header.dwMagic = pMsg->dwMagic;
	pPlayerTilesMsg->header.dwParam = pMsg->dwParam;
	pPlayerTilesMsg->header.dwSerial = pMsg->dwSerial;
	pPlayerTilesMsg->header.wOrigine = pMsg->wOrigine;
	pPlayerTilesMsg->header.wReserve = pMsg->wReserve;

	pPlayerTilesMsg->seat		    = ack.seat();	
	pPlayerTilesMsg->cnHandTile 	= ack.handtilecount();

	
	for ( int i = 0; i < ack.handtiles_size(); i++ )
	{
		pPlayerTilesMsg->anHandTile[ i ] = ack.handtiles(i);
	}

	return (PTKHEADER)pPlayerTilesMsg;
}


/************************************************************************/
/* 玩家吃碰杠的牌信息                                            */
/************************************************************************/
PTKHEADER ProtobufFactory::DeserializeMsgShowTiles(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg)
{
	// 获取消息体
	auto ack = protoMsg.mahjongplayershowtiles_ack_msg();

	// 重组消息包
	TKACKSHOWTILES *pShowTilesMsg = new TKACKSHOWTILES();
	ASSERT(pShowTilesMsg != NULL);
	memset(pShowTilesMsg, 0, sizeof(TKACKSHOWTILES));

	pShowTilesMsg->header.dwType   = ACK_TYPE( TK_MSG_PLAYER_SHOWTILES ) ;
	pShowTilesMsg->header.dwLength = MSG_LENGTH( TKACKSHOWTILES ) ;
	pShowTilesMsg->header.dwMagic = pMsg->dwMagic;
	pShowTilesMsg->header.dwParam = pMsg->dwParam;
	pShowTilesMsg->header.dwSerial = pMsg->dwSerial;
	pShowTilesMsg->header.wOrigine = pMsg->wOrigine;
	pShowTilesMsg->header.wReserve = pMsg->wReserve;

	pShowTilesMsg->seat		= ack.seat();	
	pShowTilesMsg->cnShow 	= ack.showcount();

	
	for ( int i = 0; i < ack.showtiles_size(); i++ )
	{
		pShowTilesMsg->anShowTiles[ i ] = ack.showtiles( i );
	}

	return (PTKHEADER)pShowTilesMsg;
}

/************************************************************************/
/* 加倍消息                                                            */
/************************************************************************/
PTKHEADER ProtobufFactory::DeserializeMsgDoublingAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg)
{
	// 获取消息体
	auto ack = protoMsg.mahjongdouble_ack_msg();

	// 重组消息包
	TKACKDOUBLING *pDoublingMsg = new TKACKDOUBLING();
	ASSERT(pDoublingMsg != NULL);
	memset(pDoublingMsg, 0, sizeof(TKACKDOUBLING));

	pDoublingMsg->header.dwType   = ACK_TYPE( TK_MSG_DOUBLING );
	pDoublingMsg->header.dwLength = MSG_LENGTH( TKACKDOUBLING );
	pDoublingMsg->header.dwMagic = pMsg->dwMagic;
	pDoublingMsg->header.dwParam = pMsg->dwParam;
	pDoublingMsg->header.dwSerial = pMsg->dwSerial;
	pDoublingMsg->header.wOrigine = pMsg->wOrigine;
	pDoublingMsg->header.wReserve = pMsg->wReserve;

	pDoublingMsg->seat       = ack.seat();
	pDoublingMsg->count      = ack.count();
	pDoublingMsg->tileId     = ack.tileid();

	return (PTKHEADER)pDoublingMsg;
}

/************************************************************************/
/* 加番牌信息                                                           */
/************************************************************************/
PTKHEADER ProtobufFactory::DeserializeMsgSpecialTileAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg)
{
	// 获取消息体
	auto ack = protoMsg.mahjongtpspecialtile_ack_msg();

	// 重组消息包
	TKACKMAHJONGSPECIALTILE *pSpecialTileMsg = new TKACKMAHJONGSPECIALTILE();
	ASSERT(pSpecialTileMsg != NULL);
	memset(pSpecialTileMsg, 0, sizeof(TKACKMAHJONGSPECIALTILE));

	pSpecialTileMsg->header.dwType   = ACK_TYPE( TK_MSG_MAHJONG_SPECIALTILE );
	pSpecialTileMsg->header.dwLength = MSG_LENGTH( TKACKMAHJONGSPECIALTILE ) ;
	pSpecialTileMsg->header.dwMagic = pMsg->dwMagic;
	pSpecialTileMsg->header.dwParam = pMsg->dwParam;
	pSpecialTileMsg->header.dwSerial = pMsg->dwSerial;
	pSpecialTileMsg->header.wOrigine = pMsg->wOrigine;
	pSpecialTileMsg->header.wReserve = pMsg->wReserve;

	pSpecialTileMsg->nSpecialTileID    = ack.id();
	pSpecialTileMsg->nSpecialTileFan   = ack.fan();
	pSpecialTileMsg->nSpecialTileCount = ack.count(); 

	return (PTKHEADER)pSpecialTileMsg;
}

/************************************************************************/
/* 加番牌个数                                                           */
/************************************************************************/
PTKHEADER ProtobufFactory::DeserializeMsgSpecialCountAck(const TKHEADER* pMsg, const MahJongTPAckMsg& protoMsg)
{
	// 获取消息体
	auto ack = protoMsg.mahjongtpspecialcount_ack_msg();

	// 重组消息包
	TKACKMAHJONGSPECIALCOUNT *pSpecialCountMsg = new TKACKMAHJONGSPECIALCOUNT();
	ASSERT(pSpecialCountMsg != NULL);
	memset(pSpecialCountMsg, 0, sizeof(TKACKMAHJONGSPECIALCOUNT));

	pSpecialCountMsg->header.dwType   = ACK_TYPE( TK_MSG_MAHJONG_SPECIALCOUNT ) ;
	pSpecialCountMsg->header.dwLength = MSG_LENGTH( TKACKMAHJONGSPECIALCOUNT ) ;
	pSpecialCountMsg->header.dwMagic = pMsg->dwMagic;
	pSpecialCountMsg->header.dwParam = pMsg->dwParam;
	pSpecialCountMsg->header.dwSerial = pMsg->dwSerial;
	pSpecialCountMsg->header.wOrigine = pMsg->wOrigine;
	pSpecialCountMsg->header.wReserve = pMsg->wReserve;

	pSpecialCountMsg->nSpecialCount    = ack.count();

	return (PTKHEADER)pSpecialCountMsg;
}

/************************************************************************/
/* 把普通协议重新填充到proto协议中										*/
// 参数1 原始的C++ 协议
// 参数2 返回的proto协议，有可能与参数1 相等
// 参数3 返回的proto协议使用之后是否要delete
// 返回值 是否成功
/************************************************************************/
bool ProtobufFactory::SerializeMsg2Protobuf( TKHEADER* pMsg, TKHEADER** pRetMsg, bool & bUseEndDel )
{
	TKREQMAHJONGPROTOBUF protoMsgReq = { 0 };
	*pRetMsg = NULL;
	bUseEndDel = false;

	// 序列化 ReqMsg
	SerializeProtobufMsg( pMsg, protoMsgReq );

	// 检测一下 protoMsgAck 是否被赋值过
	// 如果是原来消息只有消息头 则不会被赋值成 REQ_TYPE( TK_MSG_MAHJONG_PROTOBUF )
	if ( protoMsgReq.header.dwType == REQ_TYPE( TK_MSG_MAHJONG_PROTOBUF )  )
	{
		// 分配空间 保证发出去的消息包 地址空间连续
		*pRetMsg = CreateProtoReqMsgBuff( &protoMsgReq );
		if (*pRetMsg == NULL)
		{
			PROTOLOG( "%s : %d, Error! Protobuf() 分配空间 ", __FILE__, __LINE__);
			return false;
		}

		bUseEndDel = true;
	}

	return true;
}


// 把需要发送的protobuf消息进行重组，使地址连续
// 注：此函数返回的指针用完之后需要释放
TKHEADER* ProtobufFactory::CreateProtoReqMsgBuff(TKREQMAHJONGPROTOBUF* pReqMsg)
{
	// 分配空间 保证发出去的消息包 地址空间连续
	BYTE* pszPack = new BYTE[sizeof(TKHEADER) + pReqMsg->protoReqMsg.ByteSize()];
	ASSERT( NULL != pszPack );

	ZeroMemory(pszPack, sizeof(TKHEADER) + pReqMsg->protoReqMsg.ByteSize());

	// 向buf复制消息头
	BYTE* pData = pszPack;	
	memcpy(pData, &pReqMsg->header, sizeof(TKHEADER));

	// 向buf复制消息体
	pData += sizeof(TKHEADER);

	if ( !pReqMsg->protoReqMsg.SerializeToArray( pData,  pReqMsg->protoReqMsg.ByteSize() ) )
	{
		ASSERT( false );
		PROTOLOG( "%s : %d, Error! Protobuf() SerializeToString 错误 ", __FILE__, __LINE__);

		if (pszPack)
		{
			delete[] pszPack;
			pszPack = NULL;
		}

		return NULL;
	}
	return (PTKHEADER)pszPack;
}



// 组装消息头
void ProtobufFactory::SerializeCToSMsgHead(TKREQMAHJONGPROTOBUF& protoMsgReq, TKHEADER* pMsg)
{
	protoMsgReq.header.dwType = TK_REQ | TK_MSG_MAHJONG_PROTOBUF;
	protoMsgReq.header.dwMagic = pMsg->dwMagic;
	protoMsgReq.header.dwParam = pMsg->dwParam;
	protoMsgReq.header.dwSerial = pMsg->dwSerial;
	protoMsgReq.header.wOrigine = pMsg->wOrigine;
	protoMsgReq.header.wReserve = pMsg->wReserve;
}


/************************************************************************/
/* 对发送出去的消息包进行封装成protobuf格式                             */
/************************************************************************/
void ProtobufFactory::SerializeProtobufMsg( TKHEADER* pMsg, TKREQMAHJONGPROTOBUF& protoMsgReq )
{
	switch (pMsg->dwType)
	{
	case ACK_TYPE( TK_MSG_MAHJONG_OPEN_DOOR ):
		/************************************************************************/
		/*  开牌结束消息 玩家抓完自己的牌后，发送此消息到服务器                 */
		/************************************************************************/
		SerializeCToSMsgOpenDoorOver(protoMsgReq, pMsg);
		break;
	case ACK_TYPE( TK_MSG_MAHJONG_REQUEST ):
		/************************************************************************/
		/*  玩家向服务器返回放弃的操作类型                                      */
		/************************************************************************/
		SerializeCToSMsgRequestAck(protoMsgReq, pMsg);
		break;
	case REQ_TYPE( TK_MSG_MAHJONG_CHANGE_FLOWER ):
		/************************************************************************/
		/*  补花请求消息                                                        */
		/************************************************************************/
		SerializeCToSMsgChangeFlowerReq(protoMsgReq, pMsg);
		break;
	case REQ_TYPE( TK_MSG_MAHJONG_DISCARD_TILE ):
		/************************************************************************/
		/*  出牌请求消息                                                        */
		/************************************************************************/
		SerializeCToSMsgDiscardTileReq( protoMsgReq, pMsg );
		break;
	case REQ_TYPE( TK_MSG_MAHJONG_CALL ):
		/************************************************************************/
		/*  听牌请求消息                                                        */
		/************************************************************************/
		SerializeCToSMsgCallReq( protoMsgReq, pMsg);
		break;
	case REQ_TYPE( TK_MSG_MAHJONG_CHI ):
		/************************************************************************/
		/*  吃牌请求消息                                                        */
		/************************************************************************/
		SerializeCToSMsgChiReq( protoMsgReq, pMsg);
		break;
	case REQ_TYPE( TK_MSG_MAHJONG_PENG ):
		/************************************************************************/
		/*  碰牌请求消息                                                        */
		/************************************************************************/
		SerializeCToSMsgPengReq( protoMsgReq, pMsg);
		break;
	case REQ_TYPE( TK_MSG_MAHJONG_GANG ):
		/************************************************************************/
		/*  杠牌请求消息                                                        */
		/************************************************************************/
		SerializeCToSMsgGangReq( protoMsgReq, pMsg);
		break;
	case REQ_TYPE( TK_MSG_MAHJONG_WIN ):
		/************************************************************************/
		/*  和牌请求消息                                                        */
		/************************************************************************/
		SerializeCToSMsgWinReq( protoMsgReq, pMsg);
		break;
	case REQ_TYPE( TK_MSG_TRUSTPLAY ):
		/************************************************************************/
		/*  托管消息                                                            */
		/************************************************************************/
		SerializeCToSMsgTrustPlayReq( protoMsgReq, pMsg );
		break;
	case REQ_TYPE( TK_MSG_DOUBLING ):
		/************************************************************************/
		/* 加倍消息                                                            */
		/************************************************************************/
		SerializeCToSMsgDoublingReq( protoMsgReq, pMsg );
		break;
	case REQ_TYPE( TK_MSG_CHECKTIMEOUT ):
		/************************************************************************/
		/* 听牌消息                                                            */
		/************************************************************************/
		SerializeCToSMsgCheckTimeOutReq( protoMsgReq, pMsg );
		break;
	case REQ_TYPE( TK_MSG_MAHJONG_CALLTILE ):
		/************************************************************************/
		/* 听牌消息                                                            */
		/************************************************************************/
		SerializeCToSMsgCallTileReq( protoMsgReq, pMsg );
		break;
	default:
		break;
	}
}

/************************************************************************/
/*  开牌结束消息 玩家抓完自己的牌后，发送此消息到服务器                 */
/************************************************************************/
void ProtobufFactory::SerializeCToSMsgOpenDoorOver(TKREQMAHJONGPROTOBUF& protoMsgReq, TKHEADER* pMsg)
{
	// 组装消息头
	SerializeCToSMsgHead(protoMsgReq, pMsg);

	TKACKMAHJONGOPENDOOR* pOpenDoorMsg = (TKACKMAHJONGOPENDOOR*)pMsg;

	// 获取真正的消息体
	MahJongTPReqMsg* pTPReqMsg = protoMsgReq.protoReqMsg.mutable_mahjongtp_req_msg();
	ASSERT(NULL != pTPReqMsg);

	// 设置MatchID
	pTPReqMsg->set_matchid(m_iMatchID);

	MahJongOpenDoorOverReq* pPBOpenDoorOverReq = pTPReqMsg->mutable_mahjongopendoorover_req_msg();
	ASSERT(NULL != pPBOpenDoorOverReq);

	// 清空消息体 防止重入消息错误
	pPBOpenDoorOverReq->Clear();

	pPBOpenDoorOverReq->set_seat(pOpenDoorMsg->nSeat);

	// 在数据都添置好以后，来计算包体长度
	protoMsgReq.header.dwLength = protoMsgReq.protoReqMsg.ByteSize();
}

/************************************************************************/
/*  玩家向服务器返回放弃的操作类型                                      */
/************************************************************************/
void ProtobufFactory::SerializeCToSMsgRequestAck(TKREQMAHJONGPROTOBUF& protoMsgReq, TKHEADER* pMsg)
{
	// 组装消息头
	SerializeCToSMsgHead(protoMsgReq, pMsg);

	TKACKMAHJONGREQUEST* pRequstAckMsg = (TKACKMAHJONGREQUEST*)pMsg;

	// 获取真正的消息体
	MahJongTPReqMsg* pTPReqMsg = protoMsgReq.protoReqMsg.mutable_mahjongtp_req_msg();
	ASSERT(NULL != pTPReqMsg);

	// 设置MatchID
	pTPReqMsg->set_matchid(m_iMatchID);

	MahJongRequestReq* pPBRequstAck = pTPReqMsg->mutable_mahjongrequst_req_msg();
	ASSERT(NULL != pPBRequstAck);

	// 清空消息体 防止重入消息错误
	pPBRequstAck->Clear();

	pPBRequstAck->set_seat(pRequstAckMsg->nSeat);
	pPBRequstAck->set_requestid(pRequstAckMsg->nRequestID);
	pPBRequstAck->set_giveuprequesttype(pRequstAckMsg->nGiveUpRequestType);

	// 在数据都添置好以后，来计算包体长度
	protoMsgReq.header.dwLength = protoMsgReq.protoReqMsg.ByteSize();
}


/************************************************************************/
/*  补花请求消息                                                        */
/************************************************************************/
void ProtobufFactory::SerializeCToSMsgChangeFlowerReq(TKREQMAHJONGPROTOBUF& protoMsgReq, TKHEADER* pMsg)
{
	// 组装消息头
	SerializeCToSMsgHead(protoMsgReq, pMsg);

	TKREQMAHJONGCHANGEFLOWER* pChangeFlowerMsg = (TKREQMAHJONGCHANGEFLOWER*)pMsg;

	// 获取真正的消息体
	MahJongTPReqMsg* pTPReqMsg = protoMsgReq.protoReqMsg.mutable_mahjongtp_req_msg();
	ASSERT(NULL != pTPReqMsg);

	// 设置MatchID
	pTPReqMsg->set_matchid(m_iMatchID);

	MahJongChangeFlowerReq* pPBChangeFlowerReq = pTPReqMsg->mutable_mahjongchangeflower_req_msg();
	ASSERT(NULL != pPBChangeFlowerReq);

	// 清空消息体 防止重入消息错误
	pPBChangeFlowerReq->Clear();

	pPBChangeFlowerReq->set_seat(pChangeFlowerMsg->nSeat);
	pPBChangeFlowerReq->set_requestid(pChangeFlowerMsg->nRequestID);
	pPBChangeFlowerReq->set_tileid(pChangeFlowerMsg->nTileID);

	// 在数据都添置好以后，来计算包体长度
	protoMsgReq.header.dwLength = protoMsgReq.protoReqMsg.ByteSize();
}

/************************************************************************/
/*  出牌请求消息                                                        */
/************************************************************************/
void ProtobufFactory::SerializeCToSMsgDiscardTileReq(TKREQMAHJONGPROTOBUF& protoMsgReq, TKHEADER* pMsg)
{
	// 组装消息头
	SerializeCToSMsgHead(protoMsgReq, pMsg);

	TKREQMAHJONGDISCARDTILE* pDiscardTileMsg = (TKREQMAHJONGDISCARDTILE*)pMsg;

	// 获取真正的消息体
	MahJongTPReqMsg* pTPReqMsg = protoMsgReq.protoReqMsg.mutable_mahjongtp_req_msg();
	ASSERT(NULL != pTPReqMsg);

	// 设置MatchID
	pTPReqMsg->set_matchid(m_iMatchID);

	MahJongDiscardTileReq* pPBDiscardTileReq = pTPReqMsg->mutable_mahjongdiscardtile_req_msg();
	ASSERT(NULL != pPBDiscardTileReq);

	// 清空消息体 防止重入消息错误
	pPBDiscardTileReq->Clear();

	pPBDiscardTileReq->set_seat(pDiscardTileMsg->nSeat);
	pPBDiscardTileReq->set_requestid(pDiscardTileMsg->nRequestID);
	pPBDiscardTileReq->set_tileid(pDiscardTileMsg->nTileID);
	pPBDiscardTileReq->set_hide(pDiscardTileMsg->bHide);

	// 在数据都添置好以后，来计算包体长度
	protoMsgReq.header.dwLength = protoMsgReq.protoReqMsg.ByteSize();
}

/************************************************************************/
/*  听牌请求消息                                                        */
/************************************************************************/
void ProtobufFactory::SerializeCToSMsgCallReq(TKREQMAHJONGPROTOBUF& protoMsgReq, TKHEADER* pMsg)
{
	// 组装消息头
	SerializeCToSMsgHead(protoMsgReq, pMsg);

	TKREQMAHJONGCALL* pCallMsg = (TKREQMAHJONGCALL*)pMsg;

	// 获取真正的消息体
	MahJongTPReqMsg* pTPReqMsg = protoMsgReq.protoReqMsg.mutable_mahjongtp_req_msg();
	ASSERT(NULL != pTPReqMsg);

	// 设置MatchID
	pTPReqMsg->set_matchid(m_iMatchID);

	MahJongCallReq* pPBCallReq = pTPReqMsg->mutable_mahjongcall_req_msg();
	ASSERT(NULL != pPBCallReq);

	// 清空消息体 防止重入消息错误
	pPBCallReq->Clear();

	pPBCallReq->set_seat(pCallMsg->nSeat);
	pPBCallReq->set_requestid(pCallMsg->nRequestID);
	pPBCallReq->set_tileid(pCallMsg->nTileID);
	pPBCallReq->set_winself(pCallMsg->bWinSelf);
	pPBCallReq->set_autogang(pCallMsg->bAutoGang);
	pPBCallReq->set_calltype(pCallMsg->nCallType);
	pPBCallReq->set_hide(pCallMsg->bHide);

	// 在数据都添置好以后，来计算包体长度
	protoMsgReq.header.dwLength = protoMsgReq.protoReqMsg.ByteSize();
}

/************************************************************************/
/*  吃牌请求消息                                                        */
/************************************************************************/
void ProtobufFactory::SerializeCToSMsgChiReq(TKREQMAHJONGPROTOBUF& protoMsgReq, TKHEADER* pMsg)
{
	// 组装消息头
	SerializeCToSMsgHead(protoMsgReq, pMsg);

	TKREQMAHJONGCHI* pChiMsg = (TKREQMAHJONGCHI*)pMsg;

	// 获取真正的消息体
	MahJongTPReqMsg* pTPReqMsg = protoMsgReq.protoReqMsg.mutable_mahjongtp_req_msg();
	ASSERT(NULL != pTPReqMsg);

	// 设置MatchID
	pTPReqMsg->set_matchid(m_iMatchID);

	MahJongChiReq* pPBChiReq = pTPReqMsg->mutable_mahjongchi_req_msg();
	ASSERT(NULL != pPBChiReq);

	// 清空消息体 防止重入消息错误
	pPBChiReq->Clear();

	pPBChiReq->set_seat(pChiMsg->nSeat);
	pPBChiReq->set_requestid(pChiMsg->nRequestID);
	pPBChiReq->set_tileid(pChiMsg->nTileID);

	for (int i = 0; i < 3; i++ )
	{
		pPBChiReq->add_chitileid( pChiMsg->anChiTileID[ i ] );
	}

	// 在数据都添置好以后，来计算包体长度
	protoMsgReq.header.dwLength = protoMsgReq.protoReqMsg.ByteSize();
}

/************************************************************************/
/*  碰牌请求消息                                                        */
/************************************************************************/
void ProtobufFactory::SerializeCToSMsgPengReq( TKREQMAHJONGPROTOBUF& protoMsgReq, TKHEADER* pMsg )
{
	// 组装消息头
	SerializeCToSMsgHead(protoMsgReq, pMsg);

	TKREQMAHJONGPENG* pPengMsg = (TKREQMAHJONGPENG*)pMsg;

	// 获取真正的消息体
	MahJongTPReqMsg* pTPReqMsg = protoMsgReq.protoReqMsg.mutable_mahjongtp_req_msg();
	ASSERT(NULL != pTPReqMsg);

	// 设置MatchID
	pTPReqMsg->set_matchid(m_iMatchID);

	MahJongPengReq* pPBPengReq = pTPReqMsg->mutable_mahjongpeng_req_msg();
	ASSERT(NULL != pPBPengReq);

	// 清空消息体 防止重入消息错误
	pPBPengReq->Clear();

	pPBPengReq->set_seat(pPengMsg->nSeat);
	pPBPengReq->set_requestid(pPengMsg->nRequestID);
	pPBPengReq->set_tileid(pPengMsg->nTileID);

	for (int i = 0; i < 3; i++)
	{
		pPBPengReq->add_pengtileid( pPengMsg->anPengTileID[ i ] );
	}

	// 在数据都添置好以后，来计算包体长度
	protoMsgReq.header.dwLength = protoMsgReq.protoReqMsg.ByteSize();
}

/************************************************************************/
/*  杠牌请求消息                                                        */
/************************************************************************/
void ProtobufFactory::SerializeCToSMsgGangReq( TKREQMAHJONGPROTOBUF& protoMsgReq, TKHEADER* pMsg )
{
	// 组装消息头
	SerializeCToSMsgHead(protoMsgReq, pMsg);

	TKREQMAHJONGGANG* pGangMsg = (TKREQMAHJONGGANG*)pMsg;

	// 获取真正的消息体
	MahJongTPReqMsg* pTPReqMsg = protoMsgReq.protoReqMsg.mutable_mahjongtp_req_msg();
	ASSERT(NULL != pTPReqMsg);

	// 设置MatchID
	pTPReqMsg->set_matchid(m_iMatchID);

	MahJongGangReq* pPBGangReq = pTPReqMsg->mutable_mahjonggang_req_msg();
	ASSERT(NULL != pPBGangReq);

	// 清空消息体 防止重入消息错误
	pPBGangReq->Clear();

	pPBGangReq->set_seat(pGangMsg->nSeat);
	pPBGangReq->set_requestid(pGangMsg->nRequestID);
	pPBGangReq->set_tileid(pGangMsg->nTileID);
	pPBGangReq->set_gangtype(pGangMsg->nGangType);

	for (int i = 0; i < 4; i++)
	{
		pPBGangReq->add_gangtileid( pGangMsg->anGangTileID[ i ] );
	}

	// 在数据都添置好以后，来计算包体长度
	protoMsgReq.header.dwLength = protoMsgReq.protoReqMsg.ByteSize();
}

/************************************************************************/
/*  和牌请求消息                                                        */
/************************************************************************/
void ProtobufFactory::SerializeCToSMsgWinReq( TKREQMAHJONGPROTOBUF& protoMsgReq, TKHEADER* pMsg )
{
	// 组装消息头
	SerializeCToSMsgHead(protoMsgReq, pMsg);

	TKREQMAHJONGWIN* pWinMsg = (TKREQMAHJONGWIN*)pMsg;

	// 获取真正的消息体
	MahJongTPReqMsg* pTPReqMsg = protoMsgReq.protoReqMsg.mutable_mahjongtp_req_msg();
	ASSERT(NULL != pTPReqMsg);

	// 设置MatchID
	pTPReqMsg->set_matchid(m_iMatchID);

	MahJongWinReq* pPBWinReq = pTPReqMsg->mutable_mahjongwin_req_msg();
	ASSERT(NULL != pPBWinReq);

	// 清空消息体 防止重入消息错误
	pPBWinReq->Clear();

	pPBWinReq->set_seat(pWinMsg->nSeat);
	pPBWinReq->set_requestid(pWinMsg->nRequestID);
	pPBWinReq->set_tileid(pWinMsg->nTileID);
	pPBWinReq->set_paoseat(pWinMsg->nPaoSeat);
	pPBWinReq->set_resultant(pWinMsg->nResultant);
	pPBWinReq->set_groups(pWinMsg->cnGroups);
	pPBWinReq->set_showgroups(pWinMsg->cnShowGroups);

	for (int i = 0; i < 6; i++)
	{
		StoneGroup* pStoneGroup = pPBWinReq->add_group();
		ASSERT(NULL != pStoneGroup);

		for( int j = 0; j < 4; j++)
		{
			Stone* pStone = pStoneGroup->add_stone();
			ASSERT(NULL != pStone);

			pStone->set_id(pWinMsg->asGroup[ i ].asStone[ j ].nID); 
			pStone->set_color(pWinMsg->asGroup[ i ].asStone[ j ].nColor); 
			pStone->set_what(pWinMsg->asGroup[ i ].asStone[ j ].nWhat); 
		}

		pStoneGroup->set_groupstyle(pWinMsg->asGroup[ i ].nGroupStyle);
	}

	for( int i = 0; i < MAX_HAND_COUNT; i++)
	{
		Stone* pStone = pPBWinReq->add_handtile();
		ASSERT(NULL != pStone);

		pStone->set_id(pWinMsg->asHandTile[ i ].nID); 
		pStone->set_color(pWinMsg->asHandTile[ i ].nColor); 
		pStone->set_what(pWinMsg->asHandTile[ i ].nWhat); 
	}

	// 在数据都添置好以后，来计算包体长度
	protoMsgReq.header.dwLength = protoMsgReq.protoReqMsg.ByteSize();
}

/************************************************************************/
/*  托管消息                                                            */
/************************************************************************/
void ProtobufFactory::SerializeCToSMsgTrustPlayReq( TKREQMAHJONGPROTOBUF& protoMsgReq, TKHEADER* pMsg )
{
	// 组装消息头
	SerializeCToSMsgHead(protoMsgReq, pMsg);

	TKREQTRUSTPLAY* pTrustPlayMsg = (TKREQTRUSTPLAY*)pMsg;

	// 获取真正的消息体
	MahJongTPReqMsg* pTPReqMsg = protoMsgReq.protoReqMsg.mutable_mahjongtp_req_msg();
	ASSERT(NULL != pTPReqMsg);

	// 设置MatchID
	pTPReqMsg->set_matchid(m_iMatchID);

	MahJongTrustPlayReq* pPBTrustPlayReq = pTPReqMsg->mutable_mahjongtrustplay_req_msg();
	ASSERT(NULL != pPBTrustPlayReq);

	// 清空消息体 防止重入消息错误
	pPBTrustPlayReq->Clear();

	pPBTrustPlayReq->set_userid(pTrustPlayMsg->dwUserID);
	pPBTrustPlayReq->set_type(pTrustPlayMsg->dwType);

	// 在数据都添置好以后，来计算包体长度
	protoMsgReq.header.dwLength = protoMsgReq.protoReqMsg.ByteSize();
}

/************************************************************************/
/*  超时检测消息                                                        */
/************************************************************************/
void ProtobufFactory::SerializeCToSMsgCheckTimeOutReq( TKREQMAHJONGPROTOBUF& protoMsgReq, TKHEADER* pMsg )
{
	// 组装消息头
	SerializeCToSMsgHead(protoMsgReq, pMsg);

	TKREQCHECKTIMEOUT* pCheckTimeOutMsg = (TKREQCHECKTIMEOUT*)pMsg;

	// 获取真正的消息体
	MahJongTPReqMsg* pTPReqMsg = protoMsgReq.protoReqMsg.mutable_mahjongtp_req_msg();
	ASSERT(NULL != pTPReqMsg);

	// 设置MatchID
	pTPReqMsg->set_matchid(m_iMatchID);

	MahJongCheckTimeOutReq* pPBCheckTimeOutReq = pTPReqMsg->mutable_mahjongchecktimeout_req_msg();
	ASSERT(NULL != pPBCheckTimeOutReq);

	// 清空消息体 防止重入消息错误
	pPBCheckTimeOutReq->Clear();

	pPBCheckTimeOutReq->set_userid(pCheckTimeOutMsg->m_dwUserID);
	pPBCheckTimeOutReq->set_waittime(pCheckTimeOutMsg->nWaitSecond);

	// 在数据都添置好以后，来计算包体长度
	protoMsgReq.header.dwLength = protoMsgReq.protoReqMsg.ByteSize();
}

/************************************************************************/
/*  加倍消息                                                            */
/************************************************************************/
void ProtobufFactory::SerializeCToSMsgDoublingReq( TKREQMAHJONGPROTOBUF& protoMsgReq, TKHEADER* pMsg )
{
	// 组装消息头
	SerializeCToSMsgHead(protoMsgReq, pMsg);

	TKREQMAHJONGDBL* pDoublingMsg = (TKREQMAHJONGDBL*)pMsg;

	// 获取真正的消息体
	MahJongTPReqMsg* pTPReqMsg = protoMsgReq.protoReqMsg.mutable_mahjongtp_req_msg();
	ASSERT(NULL != pTPReqMsg);

	// 设置MatchID
	pTPReqMsg->set_matchid(m_iMatchID);

	MahJongDoubleTPReq* pPBDoublingReq = pTPReqMsg->mutable_mahjongdouble_req_msg();
	ASSERT(NULL != pPBDoublingReq);

	// 清空消息体 防止重入消息错误
	pPBDoublingReq->Clear();

	pPBDoublingReq->set_seat(pDoublingMsg->seat);
	pPBDoublingReq->set_requestid(pDoublingMsg->nRequestID);
	pPBDoublingReq->set_isdouble(pDoublingMsg->isDouble);
	pPBDoublingReq->set_doubletype(pDoublingMsg->dblType);

	MahJongWinReq *pPBWinReq = pPBDoublingReq->mutable_doublemsg();
	ASSERT(NULL != pPBWinReq);

	pPBWinReq->Clear();

	pPBWinReq->set_seat(pDoublingMsg->dblMsg.nSeat);
	pPBWinReq->set_requestid(pDoublingMsg->dblMsg.nRequestID);
	pPBWinReq->set_tileid(pDoublingMsg->dblMsg.nTileID);
	pPBWinReq->set_paoseat(pDoublingMsg->dblMsg.nPaoSeat);
	pPBWinReq->set_resultant(pDoublingMsg->dblMsg.nResultant);
	pPBWinReq->set_groups(pDoublingMsg->dblMsg.cnGroups);
	pPBWinReq->set_showgroups(pDoublingMsg->dblMsg.cnShowGroups);

	for (int i = 0; i < 6; i++)
	{
		StoneGroup* pStoneGroup = pPBWinReq->add_group();
		ASSERT(NULL != pStoneGroup);

		for( int j = 0; j < 4; j++)
		{
			Stone* pStone = pStoneGroup->add_stone();
			ASSERT(NULL != pStone);

			pStone->set_id(pDoublingMsg->dblMsg.asGroup[ i ].asStone[ j ].nID); 
			pStone->set_color(pDoublingMsg->dblMsg.asGroup[ i ].asStone[ j ].nColor); 
			pStone->set_what(pDoublingMsg->dblMsg.asGroup[ i ].asStone[ j ].nWhat); 
		}

		pStoneGroup->set_groupstyle(pDoublingMsg->dblMsg.asGroup[ i ].nGroupStyle);
	}

	for( int i = 0; i < MAX_HAND_COUNT; i++)
	{
		Stone* pStone = pPBWinReq->add_handtile();
		ASSERT(NULL != pStone);

		pStone->set_id(pDoublingMsg->dblMsg.asHandTile[ i ].nID); 
		pStone->set_color(pDoublingMsg->dblMsg.asHandTile[ i ].nColor); 
		pStone->set_what(pDoublingMsg->dblMsg.asHandTile[ i ].nWhat); 
	}

	// 在数据都添置好以后，来计算包体长度
	protoMsgReq.header.dwLength = protoMsgReq.protoReqMsg.ByteSize();
}

/************************************************************************/
/*  听牌信息                                                            */
/************************************************************************/
void ProtobufFactory::SerializeCToSMsgCallTileReq( TKREQMAHJONGPROTOBUF& protoMsgReq, TKHEADER* pMsg )
{
	// 组装消息头
	SerializeCToSMsgHead(protoMsgReq, pMsg);

	TKREQMAHJONGCALLTILE* pCallTileMsg = (TKREQMAHJONGCALLTILE*)pMsg;

	// 获取真正的消息体
	MahJongTPReqMsg* pTPReqMsg = protoMsgReq.protoReqMsg.mutable_mahjongtp_req_msg();
	ASSERT(NULL != pTPReqMsg);

	// 设置MatchID
	pTPReqMsg->set_matchid(m_iMatchID);

	MahJongCallTileReq* pPBCallTileReq = pTPReqMsg->mutable_mahjongcalltile_req_msg();
	ASSERT(NULL != pPBCallTileReq);

	// 清空消息体 防止重入消息错误
	pPBCallTileReq->Clear();

	pPBCallTileReq->set_seat(pCallTileMsg->nSeat);
	pPBCallTileReq->set_requestid(pCallTileMsg->nRequestID);
	pPBCallTileReq->set_count(pCallTileMsg->cnCallTileCount);

	for (int i = 0; i < 34; i++)
	{
		pPBCallTileReq->add_id( pCallTileMsg->nCallTileID[ i ] );
	}

	// 在数据都添置好以后，来计算包体长度
	protoMsgReq.header.dwLength = protoMsgReq.protoReqMsg.ByteSize();
}
