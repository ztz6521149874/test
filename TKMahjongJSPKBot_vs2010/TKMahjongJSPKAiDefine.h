#pragma once

#include <VECTOR>

using namespace std;

//无效牌值
#define INVALID_TILE_ID    	                ( -1 )	// 条

//花色宏
#define TILECOLOR_TIAO			            ( 0 )	// 条
#define TILECOLOR_CHARACTER					( 1 )	// 万
#define TILECOLOR_WIND						( 3 )	// 风
#define TILECOLOR_ARROW						( 4 )	// 箭

//杠牌类型
#define MING_GANG					( 0 )	// 明杠
#define AN_GANG						( 1 )	// 暗杠
#define BU_GANG						( 2 )	// 补杠

#define GAME_ID (1082) //游戏id
#define PLAYER_NUMBER (2)           //游戏人数
#define TIIL_TYPE_NUMBER (34)       //牌的种类

//动作宏  胡（Win）、吃（Chow）、碰（Pon）、杠（Kon）、打一张（Discard）、听（Listen）
const string ACTION_WIN = "Win";
const string ACTION_CHOW = "Chow";
const string ACTION_PON = "Pon";
const string ACTION_KON = "Kon";
const string ACTION_DISCARD = "Discard";
const string ACTION_LISTEN = "Listen";
const string ACTION_PASS = "Pass";

//玩家状态，0：游戏中, 1:听牌, 2:胡牌, -1:托管
#define STATE_PLAYING         ( 0 )
#define STATE_CALL            ( 1 )
#define STATE_WIN             ( 2 )
#define STATE_TRUSTEESHIP     (-1 )

struct PLAYER_MESS
{
	int			    nSeat;
	int			    nGameState;
	string			sHandCard;
	string			sChowCard;
	string			sPongCard;
	string			saKonCard;
	string			smKonCard; 
	string			saOutCard;
	string			sbOutCard;
	vector<string>	sHasHuCard;
};

struct RESPOND_MESS
{
	int		 	  nAiLevel;
	int			  nDealSeat;
	int			  nDealType;
	int			  nBanker;
	string		  sSpeacial;
	string		  sCardWall;
	int			  nLeftNum;
	string		  sLastHistory;
	int			  nsShowCount[TIIL_TYPE_NUMBER];
	PLAYER_MESS   asGameMess[PLAYER_NUMBER];
};

struct HTTP_POST
{
	int           nGameId;
	int           nRespTpye;
	RESPOND_MESS  asRespMess;
};

struct REQ_ACTION_MESS
{
	string sActionType;
	string sActionContent;
	string sNote;
};

struct HTTP_RETURN
{
	int		 	         nReqType;
	REQ_ACTION_MESS      asReqMess;
};
