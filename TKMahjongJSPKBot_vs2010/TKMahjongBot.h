#pragma once

#include "../Common/TKMahjongProtocol.h"
#include "TKBotPlayer.h"
#include "LogFileObj.h"
#include "../Common/TKMahjongDefine.h"
#include "../Common/GameRule.h"
#include "TKMahjongJSPKAi.h"

#include <functional>

class CMahJongTile;
class IJudge;
class CTKMahjongBot : public CTKBotPlayer
{
public:
	CTKMahjongBot(void);
	~CTKMahjongBot(void);

public:
	//机器人连接初始化
	virtual BOOL OnInitialUpdate();

	void RegistActionFun();

	void SendChowActionReq();
	void SendPonActionReq();
	void SendKonActionReq();
	void SendDiscardActionReq();
	void SendListenActionReq();
	void SendWinActionReq();
	void SendChangeFlowerReq(CMahJongTile *pTile);
	void SendPassActionReq();

	//定时调用的TickCount，可以调用void SetTickCountInterval(int nTickCountInterval);来设设置下一次调用的间隔
	virtual BOOL OnTickCount();

	//不用处理OnBreak,远程断线后，机器人自动删除

public:
	virtual BOOL OnMsg( PTKHEADER pMsg );

public:
	BOOL	OnAckEnterRound(PTKHEADER pMsg);
	BOOL	OnAckGameRulerInfo(PTKHEADER pMsg);
	BOOL	OnAckGameRulerInfoEx(PTKHEADER pMsg);
	BOOL	OnAckAddPlayerInfo(PTKHEADER pMsg);
	BOOL	OnAckPlayerArrived(PTKHEADER pMsg);
	BOOL	OnAckBeginHand(PTKHEADER pMsg);
	BOOL	OnAckPlayerNetBreak(PTKHEADER pMsg);
	BOOL	OnAckPlayerNetResume(PTKHEADER pMsg);

public:
	BOOL	OnAckTrustPlay(PTKHEADER pMsg);			//托管游戏
	BOOL	OnAckGameStagePlayerorder(PTKHEADER pMsg);	//收到stage的游戏数据，大
	BOOL	OnAckGameRoundPlayerorder(PTKHEADER pMsg); //收到Round的游戏数据，小
	BOOL	OnAckHistoryMsgBegin(PTKHEADER pMsg);		//收到开始接收历史消息
	BOOL	OnAckHistoryMsgEnd(PTKHEADER pMsg);		//收到结束接收历史消息

	// ----------------------------------------------------------------------------
	//
	// 游戏消息
	//
	// ----------------------------------------------------------------------------
	// 处理游戏基本信息消息
	void OnReqBaseInfo( PTKHEADER pMsg ) ;

	// 处理更新算番树消息
	void OnReqFanTree( PTKHEADER pMsg ) ;

	// 处理方位庄家消息
	void OnReqPlace(const MahJongPlaceAck& rMsg) ;

	// 处理加番牌消息
	void OnReqSpecialTile(const MahJongJSPKSpecialTileAck& rMsg);

	// 处理门风圈风消息
	void OnReqWindPlace(const MahJongWindPlaceAck& rMsg) ;

	// 处理结果消息
	void OnReqResult( PTKHEADER pMsg ) ;

	// 处理洗牌砌牌消息
	void OnReqShuffle( PTKHEADER pMsg ) ;

	// 处理掷骰子消息
	void OnReqCastDice( PTKHEADER pMsg ) ;

	// 处理开牌消息
	void OnReqOpenDoor(const MahJongOpenDoorAck& rMsg) ;

	// 处理翻混消息
	void OnReqHun(const MahJongHunAck& rMsg) ;

	// 处理请求操作消息
	void OnActionAck(const MahJongActionAck& rMsg) ;

	// 处理补花消息
	void OnReqChangeFlower(const MahJongChangeFlowerAck& rMsg) ;

	// 处理出牌消息
	void OnReqDiscardTile(const MahJongDiscardTileAck& rMsg) ;

	// 处理抓牌消息
	void OnReqDrawTile(const MahJongDrawTileAck& rMsg) ;

	// 处理听牌消息
	void OnReqCall(const MahJongCallAck& rMsg);

	// 处理吃牌消息
	void OnReqChi(const MahJongChiAck& rMsg) ;

	// 处理碰牌消息
	void OnReqPeng(const MahJongPengAck& rMsg) ;

	// 处理杠牌消息
	void OnReqGang(const MahJongGangAck& rMsg) ;

	// 处理和牌消息
	void OnAckWin( PTKHEADER pMsg ) ;

	// 处理游戏通知消息
	void OnReqNotify( PTKHEADER pMsg ) ;

	// 处理放弃操作消息
	void OnAckRequest( PTKHEADER pMsg ) ;

	void OnAckResult( PTKHEADER pMsg ) ;

	void OnReqLock(PTKHEADER pMsg);
protected:
	// 是否是请求了的类型
	BOOL IsRequestType( int nType ) { return ( 0 != ( m_nRecentRequestType & nType ) ) ; }

	// 是否听牌
	BOOL HasMeCalled() { return ( 1 == m_nCallType ) || ( 2 == m_nCallType ) ; }// 是否听牌

	// 取得一张要打的牌
	CMahJongTile* GetAutoPlayTile();
	CMahJongTile* GetDrawGangTile(); // 若抓的牌成杠则出抓的牌

	//操作请求消息处理
	void AiHandleAction(string actionType);

	// 打出一张牌
	void DiscardTile( CMahJongTile *pTile );

	// 回复请求
	void AckRequest(const TKMobileReqMsg& rMsg) ;

	// 取一张花牌
	/// 如果有花牌，那么返回一张花牌；否则返回NULL
	CMahJongTile* GetOneFlowerTile() ;

	// 从手中牌里面找一张牌
	/// nTileID是这张牌的ID号；如果玩家手中的牌不是真实的，那么就从牌尾（bGetLastTile为TRUE）或者牌头（bGetLastTile为FALSE）取一张牌
	CMahJongTile* FindHandTile( int nTileID ) ;

	// 删除手中的一张牌
	void DeleteHandTile( int nTileID );

	// 增加一张花牌（并不是指往手中加一张花牌，而是增加玩家花牌的信息）
	void AddFlowerTile( int nTileID ) ;


	// 是否有吃碰杠听和的权限
	BOOL IHaveAckPower( int nRequestType , int nSeat );

	// 填充手中牌等参数
	void FillCheckParam( CHECKPARAM * pCheckParam, int & nPaoSeat );

	// 将指定的牌ID填充到一个牌信息结构里
	void FillTileInfo( int nTileID, PMAHJONGTILE pMahJongTile );

	// 和牌方式
	int	WinMode( int nWinSeat, int nPaoSeat, int nWinTileID );

	// 看看是否可以和牌
	CHECKRESULT CheckWin( CHECKPARAM* pCheckParam , int nPaoSeat, MahJongWinReq * pReq) ;

	//检查听牌
	CHECKRESULT CheckTing(CHECKPARAM* pCheckParam, MahJongCallTileReq * pReq);

	IJudge * CreateJudge(int nMahjongType);

	void TraceHandTile( TCHAR * szTip = NULL );
private:
	void _sendReqMsg(const TKMobileReqMsg& rMsg);
	void _onPbMsg(PTKHEADER pMsg);

private:
	DWORD	m_dwMyUserID;
	int		m_nMySeat;

	DWORD	m_adwUserID[ MAX_PLAYER_COUNT ];
	BOOL	m_bAllIsBot;

	BOOL	m_bInRecvHistoryMsg;
	BOOL	m_bWaitPlayerNetResume;

	CGameRule m_GameRule;				// 游戏规则对象

	IJudge * m_pJudge;					// 吃碰杠听和判断

	int	m_nRecentRequestID ;			// 服务器发过来的最近一条请求ID
	int m_nRecentRequestType ;			// 服务器发送过来的最近一条请求的类型

	string m_sRecentHttpRespType;       //HTTP服回复的操作类型

	int m_nHandTileCount;				// 手中牌张数
	CMahJongTile * m_apHandTile[ MAX_HAND_COUNT ];// 手中牌

	// 吃碰杠牌的信息
	CMahJongTile* m_apShowGroup[ MAX_SHOW_COUNT ][ 4 ] ;	// 吃碰杠的牌
	int m_anShowGroupType[ MAX_SHOW_COUNT ] ;				// 吃碰杠牌的类型
	int m_nShowGroupCount ;									// 吃碰杠牌的组数

	int	m_nRule;						// 吃碰杠权限

	int	m_nRoundWind;					// 圈风
//	int m_nMenWind;						// 门风

	int m_nCurDiscardTileID;			// 刚打出来的那张牌
	int m_nCurDiscardSeat ;				// 当前出牌者座位号
										/// 玩家出牌前，这个是当前应该出牌的玩家；
										/// 玩家出牌后，询问大家是否吃碰杠和牌时，这个是已经出牌的玩家；
										/// 大家回复了吃碰杠和牌消息后，这个是下一个该出牌的玩家；

	int m_nDrawTileID;					// 刚抓的那张牌
	int m_nCurDrawType;					// 当前抓牌类型

	int m_nLastGangSeat;				// 刚杠牌的那个人
	int m_nLastGangTileID;				// 刚杠的那张牌

	int m_anFlowerTile[ MAX_FLOWER_COUNT ];// 花牌
	int m_cnFlowerTile;					// 花牌张数

	// 听牌类型
	int m_nCallType;

	CTKMahjongJSPKAi * m_pAI;

	typedef std::function<void()> ActionFunc;
	map<string, ActionFunc> m_mActionFuncPtr;
};
