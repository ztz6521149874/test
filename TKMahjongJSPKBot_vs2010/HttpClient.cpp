#include "HttpClient.h"
#include <afxinet.h>

#define  BUFFER_SIZE       1024

#define  NORMAL_CONNECT             INTERNET_FLAG_KEEP_CONNECTION
#define  SECURE_CONNECT             NORMAL_CONNECT | INTERNET_FLAG_SECURE
#define  NORMAL_REQUEST             INTERNET_FLAG_RELOAD | INTERNET_FLAG_DONT_CACHE 
#define  SECURE_REQUEST             NORMAL_REQUEST | INTERNET_FLAG_SECURE | INTERNET_FLAG_IGNORE_CERT_CN_INVALID
#define  IE_AGENT  _T("Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 2.0.50727)")

int HttpPost(const string& strUrl, const string& strPostData, string &strResponse)
{
    int result = FAILURE;



    CString strServer;
    CString strObject;
    DWORD dwServiceType;
    INTERNET_PORT nPort;
    AfxParseURL(strUrl.c_str(), dwServiceType, strServer, strObject, nPort);
    if (AFX_INET_SERVICE_HTTP != dwServiceType && AFX_INET_SERVICE_HTTPS != dwServiceType)
    {
        return FAILURE;
    }

	CInternetSession *m_pSession = new CInternetSession(IE_AGENT);
	CHttpConnection *m_pConnection = NULL;
	CHttpFile *m_pFile = NULL;

    try
    {
        m_pConnection = m_pSession->GetHttpConnection(strServer,
            dwServiceType == AFX_INET_SERVICE_HTTP ? NORMAL_CONNECT : SECURE_CONNECT, nPort);

		m_pFile = m_pConnection->OpenRequest(CHttpConnection::HTTP_VERB_POST, strObject, NULL, 1, NULL, NULL,
        (dwServiceType == AFX_INET_SERVICE_HTTP ? NORMAL_REQUEST : SECURE_REQUEST));

        /*设置请求相关参数*/
        m_pFile->AddRequestHeaders("Accept: */*,application/json");//accept请求报头域，表示客户端接受哪些类型的信息
        m_pFile->AddRequestHeaders("Accept-Charset:UTF8");
        m_pFile->AddRequestHeaders("Accept-Language: zh-cn;q=0.8,en;q=0.6,ja;q=0.4");
        m_pFile->AddRequestHeaders("Content-Type:application/json");//content为实体报头域，格式及编码

        if (strPostData.c_str() != NULL) {
            m_pFile->SendRequest(NULL, 0, (LPVOID)strPostData.c_str(), strlen(strPostData.c_str()));//发送请求
        }
        else {
            m_pFile->SendRequest(NULL, 0, NULL, 0);//发送请求
        }

        DWORD dwRet;
        m_pFile->QueryInfoStatusCode(dwRet);//查询执行状态

        if (dwRet == HTTP_STATUS_OK)
        {//http请求执行失败
            result = SUCCESS;
        }

        /*保存http响应*/
        char szChars[BUFFER_SIZE + 1] = { 0 };
        UINT nReaded = 0;
        while ((nReaded = m_pFile->Read((void*)szChars, BUFFER_SIZE)) > 0)
        {
            szChars[nReaded] = '\0';
            strResponse += szChars;
            memset(szChars, 0, BUFFER_SIZE + 1);
        }
    }
    catch (CInternetException* e)
    {
        DWORD dwErrorCode = e->m_dwError;
        e->Delete();
        if (ERROR_INTERNET_TIMEOUT == dwErrorCode)
        {
            result = OUTTIME;
        }
        else
        {
            result = FAILURE;
        }
    }

    if (NULL != m_pFile)
    {
        m_pFile->Close();
        delete m_pFile;
        m_pFile = NULL;
    }

    if (NULL != m_pConnection)
    {
        m_pConnection->Close();
        delete m_pConnection;
        m_pConnection = NULL;
    }

    if (NULL != m_pSession)
    {
        m_pSession->Close();
        delete m_pSession;
        m_pSession = NULL;
    }
    return result;
}