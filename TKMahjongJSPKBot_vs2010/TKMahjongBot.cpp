#include ".\tkmahjongbot.h"
#include "../Common/MahJongTile.h"
#include <assert.h>
#include "../Common/PublicJudge.h"
#include "TKMahjongJSPKAiDefine.h"

LPCTSTR g_szLogFileName = TEXT("MahjongJSPKBot.log");
IMPLEMENT_CREATEBOTPLAYER(CTKMahjongBot);

CTKMahjongBot::CTKMahjongBot(void)
{
	m_nHandTileCount = 0;
	for (int i = 0; i < MAX_HAND_COUNT; ++i)
	{
		m_apHandTile[i] = new CMahJongTile(&m_GameRule);
	}

	for (int i = 0; i < MAX_SHOW_COUNT; ++i)
	{
		for (int j = 0; j < 4; j++) {
			m_apShowGroup[i][j] = new CMahJongTile(&m_GameRule);
		}
		m_anShowGroupType[i] = 0;
	}

	m_nShowGroupCount = 0;

	m_nRecentRequestID = 0;
	m_nRecentRequestType = 0;
	m_dwMyUserID = 0;
	m_nMySeat = -1;
	memset(m_adwUserID, 0, sizeof(m_adwUserID));
	m_bAllIsBot = FALSE;
	m_bInRecvHistoryMsg = FALSE;
	m_bWaitPlayerNetResume = FALSE;
	m_nRule = 0;
	m_pJudge = NULL;

	m_nCurDiscardTileID = 0;
	m_nCurDiscardSeat = 0xFF;
	m_nDrawTileID = 0;
	m_nCurDrawType = 0;
	m_nLastGangSeat = -1;
	m_nLastGangTileID = 0;
	m_cnFlowerTile = 0;
	m_nCallType = 0;

}

CTKMahjongBot::~CTKMahjongBot(void)
{
	for (int i = 0; i < MAX_HAND_COUNT; ++i)
	{
		delete m_apHandTile[i];
	}
	memset(m_apHandTile, 0, sizeof(m_apHandTile));

	for (int i = 0; i < MAX_SHOW_COUNT; ++i)
	{
		for (int j = 0; j < 4; j++) {
			delete m_apShowGroup[i][j];
		}
	}
	memset(m_apShowGroup, 0, sizeof(m_apShowGroup));

	if (NULL != m_pJudge)
	{
		delete m_pJudge;
		m_pJudge = NULL;
	}
}



BOOL CTKMahjongBot::OnInitialUpdate()
{
	ENABLELOG();

	if (!__super::OnInitialUpdate()) return FALSE;

	m_pAI = new CTKMahjongJSPKAi();

	RegistActionFun();

	return TRUE;
}



BOOL CTKMahjongBot::OnMsg(PTKHEADER pMsg)
{
	int nLine = __LINE__;
	try
	{
		nLine = __LINE__;
		if (__super::OnMsg(pMsg)) return TRUE;
		nLine = __LINE__;

		//
		switch (pMsg->dwType)
		{
			//
		case TK_ACK | TK_MSG_GAME_ENTERROUND:
			nLine = __LINE__;
			return OnAckEnterRound(pMsg);
		case TK_ACK | TK_MSG_GAME_RULERINFO:
			nLine = __LINE__;
			return OnAckGameRulerInfo(pMsg);
		case TK_ACK | TK_MSG_GAME_RULEREXINFO:
			nLine = __LINE__;
			return OnAckGameRulerInfoEx(pMsg);
		case TK_ACK | TK_MSG_GAME_ADDGAMEPLAYERINFO:
			nLine = __LINE__;
			return OnAckAddPlayerInfo(pMsg);
		case TK_ACK | TK_MSG_GAME_PLAYERARRIVED:
			nLine = __LINE__;
			return OnAckPlayerArrived(pMsg);
		case TK_ACK | TK_MSG_GAME_BEGINHAND:
			nLine = __LINE__;
			return OnAckBeginHand(pMsg);
		case TK_ACK | TK_MSG_GAME_NETBREAK:
			nLine = __LINE__;
			return OnAckPlayerNetBreak(pMsg);
		case TK_ACK | TK_MSG_GAME_NETRESUME:
			nLine = __LINE__;
			return OnAckPlayerNetResume(pMsg);
		case TK_ACK | TK_MSG_GAME_HISTORYMSGBEGIN:
			nLine = __LINE__;
			return OnAckHistoryMsgBegin(pMsg);
		case TK_ACK | TK_MSG_GAME_HISTORYMSGEND:
			nLine = __LINE__;
			return OnAckHistoryMsgEnd(pMsg);

			//游戏消息处理
		case TK_REQ | TK_MSG_MAHJONG_BASE_INFO:	// 游戏基本信息
			nLine = __LINE__;
			OnReqBaseInfo(pMsg);
			break;
		case TK_REQ | TK_MSG_MAHJONG_FAN_TREE:	// 更新算番树
			nLine = __LINE__;
			OnReqFanTree(pMsg);
			break;
		case TK_REQ | TK_MSG_MAHJONG_PLACE:		// 设置玩家的游戏座位号和行牌规则
			nLine = __LINE__;
			//OnReqPlace(pMsg);
			break;
		case TK_REQ | TK_MSG_MAHJONG_WIND_PLACE:// 圈风信息
			nLine = __LINE__;
			//OnReqWindPlace(pMsg);
			break;
		case TK_REQ | TK_MSG_MAHJONG_RESULT:	// 结果消息
			nLine = __LINE__;
			OnReqResult(pMsg);
			break;
		case TK_REQ | TK_MSG_MAHJONG_SHUFFLE:	// 洗牌、砌牌消息
			nLine = __LINE__;
			OnReqShuffle(pMsg);
			break;
		case TK_REQ | TK_MSG_MAHJONG_CAST_DICE: // 掷骰子消息
			nLine = __LINE__;
			OnReqCastDice(pMsg);
			break;
		case TK_REQ | TK_MSG_MAHJONG_OPEN_DOOR:	// 开牌消息
			nLine = __LINE__;
			//OnReqOpenDoor(pMsg);
			break;
		case TK_REQ | TK_MSG_MAHJONG_HUN:		// 翻混消息
			nLine = __LINE__;
			//OnReqHun(pMsg);
			break;
		case TK_REQ | TK_MSG_MAHJONG_REQUEST:	// 请求客户端进行某个操作消息
			nLine = __LINE__;
			//OnReqRequest(pMsg);
			break;
		case TK_REQ | TK_MSG_MAHJONG_CHANGE_FLOWER:	// 有玩家补花了
			nLine = __LINE__;
			//OnReqChangeFlower(pMsg);
			break;
		case TK_REQ | TK_MSG_MAHJONG_DISCARD_TILE:	// 有玩家出牌了
			nLine = __LINE__;
			//OnReqDiscardTile(pMsg);
			break;
		case TK_REQ | TK_MSG_MAHJONG_DRAW_TILE:	// 有玩家抓牌了
			nLine = __LINE__;
			//OnReqDrawTile(pMsg);
			break;
		case TK_REQ | TK_MSG_MAHJONG_CALL:		// 有玩家听牌了
			nLine = __LINE__;
			//OnReqCall(pMsg);
			break;
		case TK_REQ | TK_MSG_MAHJONG_CHI:		// 有玩家吃牌了
			nLine = __LINE__;
			//OnReqChi(pMsg);
			break;
		case TK_REQ | TK_MSG_MAHJONG_PENG:		// 有玩家碰牌了
			nLine = __LINE__;
			//OnReqPeng(pMsg);
			break;
		case TK_REQ | TK_MSG_MAHJONG_GANG:		// 有玩家杠牌了
			nLine = __LINE__;
			//OnReqGang(pMsg);
			break;
		case TK_ACK | TK_MSG_MAHJONG_WIN:		// 有玩家和牌了
			nLine = __LINE__;
			OnAckWin(pMsg);
			break;
		case TK_REQ | TK_MSG_MAHJONG_NOTIFY:	// 通知消息
			nLine = __LINE__;
			OnReqNotify(pMsg);
			break;
		case TK_ACK | TK_MSG_MAHJONG_REQUEST:	// 放弃操作消息
			nLine = __LINE__;
			OnAckRequest(pMsg);
			break;
		case TK_REQ | TK_MSG_GAME_LOCKDOWN:
		{
			OnReqLock(pMsg);
			break;
		}
		case TK_ACK | (TK_MSG_MAHJONG_PROTOBUF) :
		{
			_onPbMsg(pMsg);
			break;
		}
		default:
			break;
		}


		//永远返回TRUE，因为游戏服务器有可能改动后发送不能识别的消息过来
		return TRUE;
	}
	catch (...)
	{
		WRITELOG("%s : %d, Exception", __FILE__, __LINE__);
		return TRUE;
	}
}

BOOL CTKMahjongBot::OnAckAddPlayerInfo(PTKHEADER pMsg)
{
	try
	{
		PTKACKGAMEADDPLAYERENTERINFO pAck = (PTKACKGAMEADDPLAYERENTERINFO)pMsg;
#ifdef _DEBUG
		TKWriteLog("Bot(%d) Recv : AddGamePlayer, UserID=%d, SeatOrder=%d, NickName=%s, Arrived=%d, NetStatus=%d",
			m_dwBotUserID, pAck->info.dwUserID, pAck->info.nSeatOrder, pAck->info.szNickName, pAck->info.bArrived, pAck->info.nNetStatus);
#endif

		if (pAck->info.nSeatOrder < 0 || pAck->info.nSeatOrder >= MAX_PLAYER_COUNT)
		{
			TKWriteLog("OnAckAddPlayerInfo Error, pAck->info.nSeatOrder = %d", pAck->info.nSeatOrder);
			return FALSE;
		}

		m_adwUserID[pAck->info.nSeatOrder] = pAck->info.dwUserID;

		return TRUE;
	}
	catch (...)
	{
		WRITELOG("%s : %d, Exception", __FILE__, __LINE__);
		return TRUE;
	}
}

BOOL CTKMahjongBot::OnAckPlayerArrived(PTKHEADER pMsg)
{
	try
	{
		PTKACKGAMEPLAYERARRIVED pAck = (PTKACKGAMEPLAYERARRIVED)pMsg;
#ifdef _DEBUG
		TKWriteLog("Bot(%d) Recv : GamePlayerArrived, UserID=%d, SeatOrder=%d",
			m_dwBotUserID, pAck->dwUserID, pAck->nSeatOrder);
#endif

		//不需要处理
		return TRUE;
	}
	catch (...)
	{
		WRITELOG("%s : %d, Exception", __FILE__, __LINE__);
		return TRUE;
	}
}

BOOL CTKMahjongBot::OnAckPlayerNetBreak(PTKHEADER pMsg)
{
	try
	{
		PTKACKGAMENETBREAK pAck = (PTKACKGAMENETBREAK)pMsg;
#ifdef _DEBUG
		TKWriteLog("Bot(%d) Recv : NetBreak, BreakUserID=%d, SeatOrder=%d",
			m_dwBotUserID, pAck->dwUserID, pAck->nSeatOrder);
#endif

		//不需要处理
		return TRUE;
	}
	catch (...)
	{
		WRITELOG("%s : %d, Exception", __FILE__, __LINE__);
		return TRUE;
	}
}

BOOL CTKMahjongBot::OnAckPlayerNetResume(PTKHEADER pMsg)
{
	try
	{
		PTKACKGAMENETRESUME pAck = (PTKACKGAMENETRESUME)pMsg;
#ifdef _DEBUG
		TKWriteLog("Bot(%d) Recv : NetResume, ResumeUserID=%d, SeatOrder=%d",
			m_dwBotUserID, pAck->dwUserID, pAck->nSeatOrder);
#endif

		//不需要处理
		return TRUE;
	}
	catch (...)
	{
		WRITELOG("%s : %d, Exception", __FILE__, __LINE__);
		return TRUE;
	}
}

BOOL CTKMahjongBot::OnAckHistoryMsgBegin(PTKHEADER pMsg)
{
	m_bInRecvHistoryMsg = TRUE;
	return TRUE;
}

BOOL CTKMahjongBot::OnAckHistoryMsgEnd(PTKHEADER pMsg)
{
	m_bInRecvHistoryMsg = FALSE;
	if (!ISBOTUSERID(m_dwMyUserID))
	{
		m_bWaitPlayerNetResume = TRUE;
	}
	return TRUE;
}

void CTKMahjongBot::OnAckResult(PTKHEADER pMsg)				//显示结果
{
	//不需要处理
	//	return TRUE;
}

BOOL CTKMahjongBot::OnAckTrustPlay(PTKHEADER pMsg)			//托管游戏
{
	TKWriteLog("trusteeship!!!!!!!!!!!");

	/*
	 PTKACKLORDTRUST pAck = (PTKACKLORDTRUST)pMsg;
	 #ifdef _DEBUG
	 TKTRACE("Bot(%d) Recv Trust uid=%u, seat=%d, trust=%d\n",m_dwMyUserID,pAck->dwUserID,pAck->nSeat,pAck->nTrust);
	 #endif
	 if (pAck->nTrust == 1)
	 {
	 CTKMahjongBot::OnTickCount();
	 if (!m_bInRecvHistoryMsg && m_dwMyUserID == pAck->dwUserID)
	 {
	 TKWriteLog("Bot(%d) Recv Trust uid=%u, seat=%d, trust=%d",m_dwMyUserID,pAck->dwUserID,pAck->nSeat,pAck->nTrust);

	 TKACKLORDTRUST reqTrustPlay ;
	 memset(&reqTrustPlay, 0, sizeof(TKACKLORDTRUST));
	 reqTrustPlay.header.dwType = TK_MSG_LORD_TRUST | TK_REQ ;
	 reqTrustPlay.header.dwLength = sizeof( reqTrustPlay ) - sizeof( TKHEADER ) ;
	 reqTrustPlay.nSeat = pAck->nSeat;
	 reqTrustPlay.nTrust = 0;
	 reqTrustPlay.dwUserID = pAck->dwUserID;
	 OnBotSendMsg((PTKHEADER)&reqTrustPlay);

	 return TRUE;
	 }
	 }
	 else
	 {
	 SendIdle();
	 }*/
	 //不需要处理
	return TRUE;
}

BOOL CTKMahjongBot::OnAckGameStagePlayerorder(PTKHEADER pMsg)	//收到stage的游戏数据，大
{
	//不需要处理
	return TRUE;
}

BOOL CTKMahjongBot::OnAckGameRoundPlayerorder(PTKHEADER pMsg) //收到Round的游戏数据，小
{
	//不需要处理
	return TRUE;
}
/////////////////////////////////////////

void CTKMahjongBot::RegistActionFun() {
	m_mActionFuncPtr[ACTION_WIN] = std::bind(&CTKMahjongBot::SendWinActionReq, this);
	m_mActionFuncPtr[ACTION_CHOW] = std::bind(&CTKMahjongBot::SendChowActionReq, this);
	m_mActionFuncPtr[ACTION_PON] = std::bind(&CTKMahjongBot::SendPonActionReq, this);
	m_mActionFuncPtr[ACTION_KON] = std::bind(&CTKMahjongBot::SendKonActionReq, this);
	m_mActionFuncPtr[ACTION_DISCARD] = std::bind(&CTKMahjongBot::SendDiscardActionReq, this);
	m_mActionFuncPtr[ACTION_LISTEN] = std::bind(&CTKMahjongBot::SendListenActionReq, this);
	m_mActionFuncPtr[ACTION_PASS] = std::bind(&CTKMahjongBot::SendPassActionReq, this);
}

void CTKMahjongBot::SendChowActionReq() {
	TKMobileReqMsg mobilemsg;
	MahJongJSPKReqMsg * pMsg = mobilemsg.mutable_mahjongjspk_req_msg();
	pMsg->set_matchid(m_dwMatchID);
	MahJongChiReq * pReq = pMsg->mutable_mahjongchi_req_msg();
	pReq->set_seat(m_nMySeat);
	pReq->set_requestid(m_nRecentRequestID);

	int canChowTileId = m_nCurDiscardTileID;
	pReq->set_tileid(canChowTileId);

	m_pAI->HttpRespStrToValues();

	for (int i = 0; i < 3; i++) {
		int val = m_pAI->GetRespValueArray(i);
		if (val == (canChowTileId >> 4)) {
			pReq->add_chitileid(canChowTileId);
		}
		else {
			int tileId = m_pAI->FindHandTileIdValue(val, 0);
			if (0 != tileId) {
				pReq->add_chitileid(tileId);
			}
		}
	}

	AckRequest(mobilemsg);
}

void CTKMahjongBot::SendPonActionReq() {
	TKMobileReqMsg mobilemsg;
	MahJongJSPKReqMsg * pMsg = mobilemsg.mutable_mahjongjspk_req_msg();
	pMsg->set_matchid(m_dwMatchID);
	MahJongPengReq * pReq = pMsg->mutable_mahjongpeng_req_msg();
	pReq->set_seat(m_nMySeat);
	pReq->set_requestid(m_nRecentRequestID);

	int canPonTileId = m_nCurDiscardTileID;
	pReq->set_tileid(canPonTileId);
	pReq->add_pengtileid(canPonTileId);

	m_pAI->HttpRespStrToValues();

	int pos = 0;
	for (int i = 0; i < 2; i++) {
		int tileId = m_pAI->FindHandTileIdValue(canPonTileId >> 4, pos);
		if (0 != tileId) {
			pReq->add_pengtileid(tileId);
			pos++;
		}
	}

	AckRequest(mobilemsg);
}

void CTKMahjongBot::SendKonActionReq() {
	TKMobileReqMsg mobilemsg;
	MahJongJSPKReqMsg * pMsg = mobilemsg.mutable_mahjongjspk_req_msg();
	pMsg->set_matchid(m_dwMatchID);
	MahJongGangReq * pReq = pMsg->mutable_mahjonggang_req_msg();
	pReq->set_seat(m_nMySeat);
	pReq->set_requestid(m_nRecentRequestID);

	m_pAI->HttpRespStrToValues();

	int val = m_pAI->GetRespValueArray(0);

	for (int i = 0; i < 4; i++) {
		int tileId = (val << 4) | (i + 1);
		pReq->add_gangtileid(tileId);
	}

	int gangType = BU_GANG;
	//检查目前要杠的牌是否是手中的碰牌，即是否是补杠
	if (!(m_pAI->IsBuGang())) {
		if (m_nCurDiscardSeat != m_nMySeat && (m_nCurDiscardTileID >> 4) == val) {
			//明杠
			gangType = MING_GANG;
		}
		else {
			gangType = AN_GANG;
		}
	}

	if ((m_nDrawTileID >> 4) == val)
	{
		pReq->set_tileid(m_nDrawTileID);
	}
	else if ((m_nCurDiscardTileID >> 4) == val)
	{
		pReq->set_tileid(m_nCurDiscardTileID);
	}
	else
	{
		pReq->set_tileid((val << 4) | 1);
	}

	pReq->set_gangtype(gangType);

	AckRequest(mobilemsg);
}

void CTKMahjongBot::SendDiscardActionReq() {
	TKMobileReqMsg mobilemsg;
	MahJongJSPKReqMsg * pMsg = mobilemsg.mutable_mahjongjspk_req_msg();
	pMsg->set_matchid(m_dwMatchID);
	MahJongDiscardTileReq * pReq = pMsg->mutable_mahjongdiscardtile_req_msg();
	pReq->set_seat(m_nMySeat);
	pReq->set_requestid(m_nRecentRequestID);

	m_pAI->HttpRespStrToValues();

	int val = m_pAI->GetRespValueArray(0);
	int tileId = m_pAI->FindHandTileIdValue(val, 0);

	if (0 != tileId) {
		pReq->set_tileid(tileId);
	}
	AckRequest(mobilemsg);
}

void CTKMahjongBot::SendListenActionReq() {
	m_pAI->HttpRespStrToValues();
	int val = m_pAI->GetRespValueArray(0);
	int tileId = m_pAI->FindHandTileIdValue(val, 0);

	int curequestID = m_nRecentRequestID;
	//MahJongCallTileReq
	{
		TKMobileReqMsg mobilemsg;
		MahJongJSPKReqMsg * pMsg = mobilemsg.mutable_mahjongjspk_req_msg();
		pMsg->set_matchid(m_dwMatchID);
		MahJongCallTileReq * pReq = pMsg->mutable_mahjongcalltile_req_msg();
		pReq->set_seat(m_nMySeat);
		pReq->set_requestid(curequestID);

		CHECKPARAM sCheckParam = { 0 };
		int nPaoSeat;
		FillCheckParam(&sCheckParam, nPaoSeat);
		CheckTing(&sCheckParam, pReq);

		AckRequest(mobilemsg);
	}

	//MahJongCallReq
	{
		TKMobileReqMsg mobilemsg;
		MahJongJSPKReqMsg * pMsg = mobilemsg.mutable_mahjongjspk_req_msg();
		pMsg->set_matchid(m_dwMatchID);
		MahJongCallReq * pReq = pMsg->mutable_mahjongcall_req_msg();
		pReq->set_seat(m_nMySeat);
		pReq->set_requestid(curequestID);
		pReq->set_winself(false);
		pReq->set_autogang(true);
		pReq->set_hide(false);
		pReq->set_tileid(tileId);
		AckRequest(mobilemsg);
	}
}

void CTKMahjongBot::SendWinActionReq() {
	TKMobileReqMsg mobilemsg;
	MahJongJSPKReqMsg * pMsg = mobilemsg.mutable_mahjongjspk_req_msg();
	pMsg->set_matchid(m_dwMatchID);
	MahJongWinReq * pReq = pMsg->mutable_mahjongwin_req_msg();

	TKREQMAHJONGWIN reqMahJongWin = { 0 };
	CHECKPARAM sCheckParam = { 0 };
	int nPaoSeat;
	FillCheckParam(&sCheckParam, nPaoSeat);
	CHECKRESULT eCheckWinResult = CheckWin(&sCheckParam, nPaoSeat, pReq);

	if (eCheckWinResult == T_OK)
	{
		// 如果可和，且玩家已听牌了,马上发送和牌消息
		AckRequest(mobilemsg);
	}
}

void CTKMahjongBot::SendChangeFlowerReq(CMahJongTile *pTile) {
	TKMobileReqMsg mobilemsg;
	MahJongJSPKReqMsg * pMsg = mobilemsg.mutable_mahjongjspk_req_msg();
	pMsg->set_matchid(m_dwMatchID);
	MahJongChangeFlowerReq * pReq = pMsg->mutable_mahjongchangeflower_req_msg();
	pReq->set_seat(m_nMySeat);
	pReq->set_requestid(m_nRecentRequestID);
	pReq->set_tileid(pTile->ID());

	AckRequest(mobilemsg);
}

void CTKMahjongBot::SendPassActionReq() {
	TKMobileReqMsg mobilemsg;
	MahJongJSPKReqMsg * pMsg = mobilemsg.mutable_mahjongjspk_req_msg();
	pMsg->set_matchid(m_dwMatchID);
	MahJongRequestReq * pReq = pMsg->mutable_mahjongrequst_req_msg();
	pReq->set_seat(m_nMySeat);
	pReq->set_requestid(m_nRecentRequestID);
	pReq->set_giveuprequesttype(m_nRecentRequestType);
	AckRequest(mobilemsg);
}

BOOL CTKMahjongBot::OnTickCount()
{
	try
	{
	
		map<string, ActionFunc>::iterator it = m_mActionFuncPtr.find(m_sRecentHttpRespType);
		if (it != m_mActionFuncPtr.end()) {
			it->second();
			return TRUE;
		}

		TKWriteLog("find action func error!!!!!");

		//调用基类发送Idle消息
		return __super::OnTickCount();
	}
	catch (...)
	{
		WRITELOG("%s : %d, Exception", __FILE__, __LINE__);
		return __super::OnTickCount();
	}
}

//选手进入
BOOL CTKMahjongBot::OnAckEnterRound(PTKHEADER pMsg)
{
	try
	{
		PTKACKGAMEPLAYERENTER pAck = (PTKACKGAMEPLAYERENTER)pMsg;
		if (pMsg->dwParam == TK_ACKRESULT_SUCCESS)
		{
			if (m_dwBotUserID != pAck->dwUserID)
			{
				TKWriteLog("!!! Warring !!!BotUserID(%d) != AckUserID(%d)", m_dwBotUserID, pAck->dwUserID);
				Terminate(TRUE);
				return TRUE;
			}
#ifdef _DEBUG
			TKWriteLog("Bot(%d) Recv Enter GameRound Ack: UserID=%d, SeatOrder=%d",
				m_dwBotUserID, pAck->dwUserID, pAck->nSeatOrder);
#endif
		}
		else
		{
#ifdef _DEBUG
			TKWriteLog("Bot(%d) Enter GameRound failed:", m_dwBotUserID);
#endif
			Terminate(TRUE);
			return TRUE;
		}

		//
		m_dwMyUserID = pAck->dwUserID;

		return TRUE;
	}
	catch (...)
	{
		WRITELOG("%s : %d, Exception", __FILE__, __LINE__);
		return TRUE;
	}
}

//游戏规则信息
BOOL CTKMahjongBot::OnAckGameRulerInfo(PTKHEADER pMsg)
{
	try
	{
		PTKACKGAMERULERINFO pAck = (PTKACKGAMERULERINFO)pMsg;
#ifdef _DEBUG
		TKWriteLog("Bot(%d) Recv : GameRulerInfo, GameTotalPlayer=%d, ScoreBase=%d, ScoreType=%d, TableNumber=%d, RoundRulerName=%s",
			m_dwBotUserID, pAck->nGameTotalPlayer, pAck->nScoreBase, pAck->nScoreType, pAck->nTableNumber, pAck->szRoundRulerName);
#endif

		return TRUE;
	}
	catch (...)
	{
		WRITELOG("%s : %d, Exception", __FILE__, __LINE__);
		return TRUE;
	}
}


//游戏规则信息
BOOL CTKMahjongBot::OnAckGameRulerInfoEx(PTKHEADER pMsg)
{
	try
	{
		PTKACKGAMERULEREXINFO pAck = (PTKACKGAMERULEREXINFO)pMsg;
#ifdef _DEBUG
#endif

		// 终于等到规则了
		//		TCHAR szProperty[ 256 ] = "2,1,127,2,2,3,3,1,1,1,1,0,0,1,1,1,0,15,10,1,0,1,0,0,0,0,30";
		m_GameRule.InitInstance(pAck->szPropertyEx);

		if (NULL == m_pJudge)
		{
			m_pJudge = CreateJudge(MAHJONG_TYPE_JSPK);
		}

		return TRUE;
	}
	catch (...)
	{
		WRITELOG("%s : %d, Exception", __FILE__, __LINE__);
		return TRUE;
	}
}

//游戏开始
BOOL CTKMahjongBot::OnAckBeginHand(PTKHEADER pMsg)
{
	//初始化AI数据
#ifdef _DEBUG
	TKWriteLog("Begin Hand......");
#endif

	m_pAI->InitData();

	m_pAI->SetAILevel(m_nAILevel);
	TKWriteLog("m_nAILevel: %d", m_nAILevel);

	string strAISerName = "MahjongJSPKAIWebServer";
	char strAIlSerAddr[256] = { 0 };
	GetPrivateProfileString("AISpecific", strAISerName.c_str(), "10.11.240.10:8888", strAIlSerAddr, sizeof(strAIlSerAddr), "./TKGeneralServer.ini");

	m_pAI->SetAIServerAddr(string(strAIlSerAddr));

	TKWriteLog("AIServerAddr: %s", string(strAIlSerAddr).c_str());

	try
	{
		PTKACKGAMEBEGINHAND pAck = (PTKACKGAMEBEGINHAND)pMsg;
#ifdef _DEBUG
		//    TKWriteLog("Bot(%d) Recv : BeginHand, iGameHand=%d",m_dwBotUserID,pAck->iGameHand);
#endif

		//
		m_bAllIsBot = TRUE;
		for (int i = 0; i < MAX_PLAYER_COUNT; i++)
		{
			if (!ISBOTUSERID(m_adwUserID[i])) m_bAllIsBot = FALSE;
		}


		memset(m_apHandTile, 0, sizeof(m_apHandTile));
		memset(m_apShowGroup, 0, sizeof(m_apShowGroup));

		m_nHandTileCount = 0;
		for (int i = 0; i < MAX_HAND_COUNT; ++i)
		{
			m_apHandTile[i] = new CMahJongTile(&m_GameRule);
		}

		for (int i = 0; i < MAX_SHOW_COUNT; ++i)
		{
			for (int j = 0; j < 4; j++) {
				m_apShowGroup[i][j] = new CMahJongTile(&m_GameRule);
			}
			m_anShowGroupType[i] = 0;
		}
		m_nShowGroupCount = 0;

		m_nRecentRequestID = 0;
		m_nRecentRequestType = 0;
		m_nHandTileCount = 0;
		m_nRule = 0;

		m_nCurDiscardTileID = 0;
		m_nCurDiscardSeat = 0xFF;
		m_nDrawTileID = 0;
		m_nCurDrawType = 0;
		m_nLastGangSeat = -1;
		m_nLastGangTileID = 0;
		m_cnFlowerTile = 0;
		m_nCallType = 0;

		return TRUE;
	}
	catch (...)
	{
		WRITELOG("%s : %d, Exception", __FILE__, __LINE__);
		return TRUE;
	}
}

// 处理游戏基本信息消息
void CTKMahjongBot::OnReqBaseInfo(PTKHEADER pMsg)
{

}

// 处理更新算番树消息
void CTKMahjongBot::OnReqFanTree(PTKHEADER pMsg)
{

}

// 处理方位庄家消息
void CTKMahjongBot::OnReqPlace(const MahJongPlaceAck& rMsg)
{
	m_pAI->SetDealerSeat(rMsg.banker());
	m_nRule = rMsg.rules();
}

// 处理加番牌消息
void CTKMahjongBot::OnReqSpecialTile(const MahJongJSPKSpecialTileAck& rMsg)
{
	m_pAI->HandleSpecialTile(rMsg.id());
}

// 处理门风圈风消息
void CTKMahjongBot::OnReqWindPlace(const MahJongWindPlaceAck& rMsg)
{
	m_nRoundWind = rMsg.roundwind();
}

// 处理结果消息
void CTKMahjongBot::OnReqResult(PTKHEADER pMsg)
{

}

// 处理洗牌砌牌消息
void CTKMahjongBot::OnReqShuffle(PTKHEADER pMsg)
{

}

// 处理掷骰子消息
void CTKMahjongBot::OnReqCastDice(PTKHEADER pMsg)
{

}

// 处理开牌消息
void CTKMahjongBot::OnReqOpenDoor(const MahJongOpenDoorAck& rMsg)
{
	try
	{
#ifdef _DEBUG
		TKWriteLog("OnReqOpenDoor my seat: %d", rMsg.seat());
#endif
		
		m_nMySeat = rMsg.seat();
		m_pAI->SetMySeat(m_nMySeat);
		
		if (rMsg.seat() != m_nMySeat)
		{
			// 不是发给我的
			return;
		}

		m_nHandTileCount = rMsg.handtile();
		if (m_nHandTileCount > MAX_HAND_COUNT)
		{
			m_nHandTileCount = MAX_HAND_COUNT;
		}
		for (int i = 0; i < rMsg.tiles_size(); ++i)
		{
			if (i >= m_nHandTileCount)
			{
				break;
			}
			m_apHandTile[i]->SetID(rMsg.tiles(i));
			m_pAI->AddHandTiles(rMsg.tiles(i));
			m_pAI->SetShowCount(rMsg.tiles(i));
		}
		m_pAI->HandleOpenDoor();

		TraceHandTile("发牌完毕: ");

		// 通知服务器我发完牌了
		TKMobileReqMsg mobilemsg;
		MahJongJSPKReqMsg * pMsg = mobilemsg.mutable_mahjongjspk_req_msg();
		pMsg->set_matchid(m_dwMatchID);
		MahJongOpenDoorOverReq * pReq = pMsg->mutable_mahjongopendoorover_req_msg();
		pReq->set_seat(m_nMySeat);

		_sendReqMsg(mobilemsg);
	}
	catch (...)
	{
		TKWriteLog("%s : %d, Exception", __FILE__, __LINE__);
	}
}

// 处理翻混消息
void CTKMahjongBot::OnReqHun(const MahJongHunAck& rMsg)
{
}

// 处理请求操作消息
void CTKMahjongBot::OnActionAck(const MahJongActionAck& rMsg)
{
	try
	{
		if (m_nMySeat != rMsg.seat())
		{
			// 不是发给自已的
			return;
		}

#ifdef _DEBUG
		TKWriteLog("MahJongActionAck: %s", rMsg.DebugString().c_str());
#endif
		
		m_nRecentRequestID = rMsg.requestid();
		m_nRecentRequestType = rMsg.requesttype();

		m_pAI->SetDealType(m_nRecentRequestType);

		if (IsRequestType(REQUEST_TYPE_CHANGE_FLOWER))
		{ // 请求补花
			// 
			// 请求补花有两种可能：
			// 1.补花                            补花阶段    要某人补花，询问这个玩家
			// 2.出牌＋补花＋听牌＋杠牌＋和牌    游戏阶段    某人抓牌后，询问这个抓牌的玩家
			// 
			// 有花牌必须补花，如果没有花牌，否则在情况1下告诉服务器不补花，在情况2下继续其他的判断
			// 
			TraceHandTile("请求补花: ");

			CMahJongTile *pTile = GetOneFlowerTile();	// 取一张花牌
			if (NULL != pTile)
			{ 
				// 找到一张花牌 告诉服务器要补花
				SendChangeFlowerReq(pTile);

#ifdef _DEBUG
				TKWriteLog("补花: %d", pTile->ID());
#endif
				
				return;
			}
			if (m_nRecentRequestType == REQUEST_TYPE_CHANGE_FLOWER)
			{
				//没有花牌但只有补花的选择 直接放弃返回
				SendPassActionReq();
				return;
			}
		}

		TKWriteLog("m_bPacerBot: %d", m_bPacerBot);

		//m_bPacerBot目前底层配置是true时为AI逻辑  不走托管机器人逻辑或听牌后
		if ((!m_bPacerBot) || HasMeCalled())
		{
			//走托管逻辑
			if (IsRequestType(REQUEST_TYPE_WIN))
			{
				// 有请求和牌,看看能不能和
				// 先看看我有没有和牌的权限
				if (IHaveAckPower(REQUEST_TYPE_WIN, m_nCurDiscardSeat))
				{ // 我有和牌的权限
					// 先看看能不能和
					TKMobileReqMsg mobilemsg;
					MahJongJSPKReqMsg * pMsg = mobilemsg.mutable_mahjongjspk_req_msg();
					pMsg->set_matchid(m_dwMatchID);
					MahJongWinReq * pReq = pMsg->mutable_mahjongwin_req_msg();

					TKREQMAHJONGWIN reqMahJongWin = { 0 };
					CHECKPARAM sCheckParam = { 0 };
					int nPaoSeat;
					FillCheckParam(&sCheckParam, nPaoSeat);
					CHECKRESULT eCheckWinResult = CheckWin(&sCheckParam, nPaoSeat, pReq);
					if (eCheckWinResult == T_OK)
					{
						// 如果可和，且玩家已听牌了,马上发送和牌消息
						AckRequest(mobilemsg);
						return;
					}
				}
			}

			if (IsRequestType(REQUEST_TYPE_DISCARD_TILE))
			{
				CMahJongTile *pTile = new CMahJongTile(&m_GameRule);
				if (m_nDrawTileID == 0)
				{
					pTile = m_apHandTile[0];//庄家首出出牌
#ifdef _DEBUG
					TKWriteLog("庄家首出出牌: %d", pTile->ID());
#endif	
				}
				else
				{
					pTile->SetID(m_nDrawTileID); // 打出刚摸的牌
#ifdef _DEBUG
					TKWriteLog("打出刚摸的牌: %d", pTile->ID());
#endif	
				}
				
				assert(NULL != pTile);	// 一定选择了一张
				DiscardTile(pTile);	// 出这张牌
				return;
			}

			//一律放弃

			SendPassActionReq();
			return;
		}


		const std::function<void(std::string)> handle_cb =
			std::bind(&CTKMahjongBot::AiHandleAction, this, std::placeholders::_1);

		//发送HTTP请求消息，回调函数为处理解析消息发送给游戏服
		m_pAI->run(handle_cb);

	}
	catch (...)
	{
		WRITELOG("%s : %d, Exception", __FILE__, __LINE__);
	}
}

//操作请求消息处理
void CTKMahjongBot::AiHandleAction(string actionType) {
	m_sRecentHttpRespType = actionType;

	int dTime = 10;
	int uTime = 11;

	if (actionType == ACTION_CHOW) {
		// 吃
		dTime = 500;
		uTime = 2000;
	}
	else if (actionType == ACTION_PON) {
		// 碰
		dTime = 500;
		uTime = 2000;
	}
	else if (actionType == ACTION_KON) {
		// 杠
		dTime = 500;
		uTime = 2000;
	}
	else if (actionType == ACTION_LISTEN) {
		// 听
		dTime = 1000;
		uTime = 2500;
	}
	else if (actionType == ACTION_WIN) {
		// 和
		dTime = 500;
		uTime = 1000;
	}
	else if (actionType == ACTION_DISCARD) {		// 出牌
		dTime = 500;
		uTime = 2000;
	}
	else if (actionType == ACTION_PASS) {	// 放弃
		dTime = 10;
		uTime = 11;
	}
	else {
		return;
		//assert(false);
	}

	//开启定时调用
	int gap = uTime - dTime;
	int m_nWaitTime = dTime;
	if (gap > 0)
	{
		m_nWaitTime = TKGenRandom() % gap + dTime;
	}
	SetThinkTickCount(m_nWaitTime);
}

// 处理补花消息
void CTKMahjongBot::OnReqChangeFlower(const MahJongChangeFlowerAck& rMsg)
{
	try
	{
		// 重置杠牌的信息（从而杠上补花之后和牌不算杠上开花，杠上补花之后放炮不算杠上炮）
		m_nLastGangSeat = -1;
		m_nLastGangTileID = 0;

		if (rMsg.seat() != m_nMySeat)
		{
			// 不是我的
			return;
		}

		int nTileID = rMsg.tileid();			// 要补的这张花牌的ID号

		DeleteHandTile(nTileID);
		AddFlowerTile(nTileID);
		TraceHandTile("补花剩下的牌: ");
	}
	catch (...)
	{
		WRITELOG("%s : %d, Exception", __FILE__, __LINE__);
	}
}

// 处理出牌消息
void CTKMahjongBot::OnReqDiscardTile(const MahJongDiscardTileAck& rMsg)
{
	try
	{

		m_pAI->HandleDiscardTile(rMsg.seat(), rMsg.tileid());

		// 保存一下
		m_nCurDiscardTileID = rMsg.tileid();
		m_nCurDiscardSeat = rMsg.seat();

		// 重置杠牌的信息
		m_nLastGangSeat = -1;
		m_nLastGangTileID = 0;
		if (m_nMySeat != rMsg.seat())
		{ // 不是我出牌
			return;
		}

		DeleteHandTile(m_nCurDiscardTileID);
	}
	catch (...)
	{
		WRITELOG("%s : %d, Exception", __FILE__, __LINE__);
	}
}

// 处理抓牌消息
void CTKMahjongBot::OnReqDrawTile(const MahJongDrawTileAck& rMsg)
{
	try
	{
		m_pAI->UpdateLeftNum();

		if (rMsg.seat() != m_nMySeat)
		{
			return;
		}

		if (m_nHandTileCount >= MAX_HAND_COUNT)
		{
			return;
		}

		m_pAI->HandleDrawTile(rMsg.tileid());

		// 保存
		m_nDrawTileID = rMsg.tileid();
		m_nCurDrawType = rMsg.drawtiletype();

		m_apHandTile[m_nHandTileCount]->SetID(m_nDrawTileID);
		m_nHandTileCount++;
		TraceHandTile("抓牌后: ");
	}
	catch (...)
	{
		WRITELOG("%s : %d, Exception", __FILE__, __LINE__);
	}
}

// 处理听牌消息
void CTKMahjongBot::OnReqCall(const MahJongCallAck& rMsg)
{
	UINT tilId = 0;
	if (rMsg.has_tileid())
	{
		tilId = rMsg.tileid();
	}
	else
	{
	}

	m_pAI->HandleListenTile(rMsg.seat(), tilId);

	// 听牌时的出牌也保存一下
	m_nCurDiscardTileID = tilId;
	m_nCurDiscardSeat = rMsg.seat();

	if (rMsg.seat() == m_nMySeat)
	{
		m_nCallType = rMsg.calltype();

		if (m_nCurDiscardTileID != 0)
		{
			//排除闲家天听
			DeleteHandTile(m_nCurDiscardTileID);
		}

	}
}


// 处理吃牌消息
void CTKMahjongBot::OnReqChi(const MahJongChiAck& rMsg)
{
	vector<UINT> chiTemp;
	for (int i = 0; i < rMsg.chitileid_size(); ++i) {
		int curId = rMsg.chitileid(i);
		chiTemp.push_back(curId);

		// 记录吃牌信息
		if (rMsg.seat() == m_nMySeat) {
			m_apShowGroup[m_nShowGroupCount][i]->SetID(curId);

			if (curId != rMsg.tileid())
			{
				DeleteHandTile(curId);
			}
		}
	}
	m_pAI->HandleChowTile(rMsg.seat(), chiTemp);

	if (rMsg.seat() == m_nMySeat) {
		m_anShowGroupType[m_nShowGroupCount] = GROUP_STYLE_SHUN;
		m_nShowGroupCount++;
	}

}

// 处理碰牌消息
void CTKMahjongBot::OnReqPeng(const MahJongPengAck& rMsg)
{
	vector<UINT> ponTemp;
	for (int i = 0; i < rMsg.pengtileid_size(); ++i) {
		int curId = rMsg.pengtileid(i);

		ponTemp.push_back(curId);

		// 记录碰牌信息
		if (rMsg.seat() == m_nMySeat) {
			m_apShowGroup[m_nShowGroupCount][i]->SetID(curId);
			if (curId != rMsg.tileid())
			{
				DeleteHandTile(curId);
			}
		}
	}
	m_pAI->HandlePonTile(rMsg.seat(), ponTemp);

	if (rMsg.seat() == m_nMySeat) {
		m_anShowGroupType[m_nShowGroupCount] = GROUP_STYLE_KE;
		m_nShowGroupCount++;
	}
}

// 处理杠牌消息
void CTKMahjongBot::OnReqGang(const MahJongGangAck& rMsg)
{
	vector<UINT> konTemp;
	for (int i = 0; i < rMsg.gangtileid_size(); ++i) {
		int curId = rMsg.gangtileid(i);

		konTemp.push_back(curId);

		// 记录杠牌信息
		if (rMsg.seat() == m_nMySeat) {
			if (rMsg.gangtype() == AN_GANG)
			{
				//暗杠
				m_apShowGroup[m_nShowGroupCount][i]->SetID(curId);
				DeleteHandTile(curId);
			}
			else if (rMsg.gangtype() == MING_GANG)
			{
				//明杠 
				m_apShowGroup[m_nShowGroupCount][i]->SetID(curId);
				if (rMsg.tileid() == m_nCurDiscardTileID && m_nCurDiscardSeat != m_nMySeat)
				{
					//杠牌来自对家
					if (curId != rMsg.tileid())
					{
						DeleteHandTile(curId);
					}
				}
				else
				{
					DeleteHandTile(curId);
				}
			}
			else
			{
				//补杠 看杠牌来自自己还是对家 且取出碰牌的数据
				if (rMsg.tileid() == m_nDrawTileID)
				{
					DeleteHandTile(curId);
				}

			}

		}
	}
	m_pAI->HandleKonTile(rMsg.seat(), rMsg.gangtype(), konTemp);

	m_nLastGangSeat = rMsg.seat();
	m_nLastGangTileID = rMsg.tileid();

	if (rMsg.seat() == m_nMySeat) {
		if (rMsg.gangtype() == BU_GANG)
		{
			CMahJongTile *pTile = new CMahJongTile(&m_GameRule);
			pTile->SetID(rMsg.tileid());
			for (int i = 0; i < m_nShowGroupCount; i++)
			{ // 遍历吃碰杠的分组
				if (GROUP_STYLE_KE == m_anShowGroupType[i])
				{ // 刻
					CMahJongTile *pTileShow = m_apShowGroup[i][0];	// 指向分组的第一张牌
					if (pTileShow->SameAs(pTile->Color(), pTile->What()))
					{ // 就是碰的这张牌
						m_anShowGroupType[i] = GROUP_STYLE_MINGGANG;	// 更改分组类型
						m_apShowGroup[i][3] = pTile;
						break;
					}
				}
			}
		}
		else
		{
			m_anShowGroupType[m_nShowGroupCount] = GROUP_STYLE_ANGANG;
		}

		if (rMsg.gangtype() != BU_GANG)
		{
			m_nShowGroupCount++;
		}

	}
}

// 处理和牌消息
void CTKMahjongBot::OnAckWin(PTKHEADER pMsg)
{
	m_pAI->ClearData();
}

// 处理游戏通知消息
void CTKMahjongBot::OnReqNotify(PTKHEADER pMsg)
{

}

// 处理放弃操作消息
void CTKMahjongBot::OnAckRequest(PTKHEADER pMsg)
{

}

void CTKMahjongBot::OnReqLock(PTKHEADER pMsg)
{
	PTKREQLOCKDOWNGAME pReq = (PTKREQLOCKDOWNGAME)pMsg;
	if (NULL == pReq)
	{
		return;
	}
	if (FALSE == pReq->bUnlock)
	{
		TKACKLOCKDOWNGAME ackLock;
		memset(&ackLock, 0, sizeof(TKACKLOCKDOWNGAME));
		ackLock.header.dwLength = MSG_LENGTH(TKACKLOCKDOWNGAME);
		ackLock.header.dwType = ACK_TYPE(TK_MSG_GAME_LOCKDOWN);
		ackLock.bAllow = FALSE;
		SendMsg((PTKHEADER)&ackLock);
	}
}

CMahJongTile* CTKMahjongBot::GetAutoPlayTile()
{
	try
	{
		int nTileCount = m_nHandTileCount;	// 手中牌的张数
		if (nTileCount <= 0)
		{
			return NULL;
		}

		assert(NULL == GetOneFlowerTile());	// 这时候手中肯定没有花牌
		qsort(m_apHandTile, nTileCount, sizeof(CMahJongTile*), CMahJongTile::Compare);	// 排序	

		// 
		// 选牌的原则是：
		// 1.不是混牌
		// 2.一张离相同花色的其他牌中最远的牌
		// 

		// 先计算每张牌离其他牌的距离
		int pnDistance[MAX_HAND_TILE_COUNT] = { 0 };	// 各张牌离其他牌的距离
		CMahJongTile *pTile = NULL, *pTemp = NULL;
		for (int i = 0; i < nTileCount; i++)
		{ // 遍历这些牌
			pTile = m_apHandTile[i];	// 这个位置的牌
			if (HasMeCalled() && m_apHandTile[i]->IDIs(m_nDrawTileID))//听牌后直接从出摸的那张牌
			{
				return m_apHandTile[i];
			}
			// 普通的牌
			pnDistance[i] = 0;

			// 看看前后的牌的值
			if (i > 0)
			{ // 不是第一张
				pTemp = m_apHandTile[i - 1];	// 取前一张
				pnDistance[i] += CMahJongTile::Distance(pTile, pTemp);
			}
			else
			{ // 第一张
				pnDistance[i] += 500;
			}
			if (i < nTileCount - 1)
			{ // 不是最后一张
				pTemp = m_apHandTile[i + 1];	// 取后一张
				pnDistance[i] += CMahJongTile::Distance(pTile, pTemp);
			}
			else
			{ // 最后一张
				pnDistance[i] += 500;
			}
		}

		// 从中选出最远的一张牌出来
		int nDistance = 0;	// 当前最大的距离
		int nIndex = 0;	// 当前最大距离这张牌的索引号
		for (int i = 0; i < nTileCount; i++)
		{ // 遍历所有的牌
			if (pnDistance[i] >= nDistance)
			{ // 这张牌离别的牌的距离也很远
				nIndex = i;
				nDistance = pnDistance[i];
			}
		}

		return m_apHandTile[nIndex];
	}
	catch (...)
	{
		WRITELOG("%s : %d, Exception", __FILE__, __LINE__);
		return NULL;
	}
}

CMahJongTile* CTKMahjongBot::GetDrawGangTile()
{
	if (m_nDrawTileID <= 0 || m_nHandTileCount <= 1)
	{
		return NULL;
	}

	int cnDraw = 0;
	for (int i = 0; i < m_nHandTileCount - 1; i++)
	{
		if (m_apHandTile[i]
			&& CMahJongTile::SameTile(m_nDrawTileID, m_apHandTile[i]->ID()))
		{
			cnDraw++;
		}
	}
	if (3 == cnDraw)
	{
		return m_apHandTile[m_nHandTileCount - 1];
	}

	return NULL;
}

void CTKMahjongBot::DiscardTile(CMahJongTile *pTile)
{
	try
	{
		if (pTile == NULL || !IsRequestType(REQUEST_TYPE_DISCARD_TILE))
		{
			return;
		}

		// 发送出牌消息
		TKMobileReqMsg mobilemsg;
		MahJongJSPKReqMsg * pMsg = mobilemsg.mutable_mahjongjspk_req_msg();
		pMsg->set_matchid(m_dwMatchID);
		MahJongDiscardTileReq * pReq = pMsg->mutable_mahjongdiscardtile_req_msg();
		pReq->set_seat(m_nMySeat);
		pReq->set_requestid(m_nRecentRequestID);
		pReq->set_tileid(pTile->ID());

		AckRequest(mobilemsg);
	}
	catch (...)
	{
		WRITELOG("%s : %d, Exception", __FILE__, __LINE__);
	}
}



void CTKMahjongBot::AckRequest(const TKMobileReqMsg& rMsg)
{
	m_nRecentRequestID = 0;
	m_nRecentRequestType = 0;

	_sendReqMsg(rMsg);
}



CMahJongTile* CTKMahjongBot::GetOneFlowerTile()
{
	try
	{
		for (int i = 0; i < m_nHandTileCount; i++)
		{ // 遍历手中的牌
			assert(NULL != m_apHandTile[i]);
			if (m_apHandTile[i]->IsFlower())
			{ // 花牌
				return m_apHandTile[i];
			}
		}

		return NULL;
	}
	catch (...)
	{
		WRITELOG("%s : %d, Exception", __FILE__, __LINE__);
		return NULL;
	}
}



CMahJongTile* CTKMahjongBot::FindHandTile(int nTileID)
{
	try
	{
		assert(m_nHandTileCount > 0);	// 这时候手中必须要有牌

		CMahJongTile *pTile = NULL;

		for (int i = 0; i < m_nHandTileCount; i++)
		{ // 遍历手中的牌
			if (m_apHandTile[i]->IDIs(nTileID))
			{ // 就是这张牌
				pTile = m_apHandTile[i];
				break;
			}
		}

		assert(NULL != pTile);

		return pTile;
	}
	catch (...)
	{
		WRITELOG("%s : %d, Exception", __FILE__, __LINE__);
		return NULL;
	}
}



BOOL CTKMahjongBot::IHaveAckPower(int nRequestType, int nSeat)
{
	/*if ( !ISBOTUSERID( m_dwMyUserID ) )
	{
	return FALSE;
	}*/

	int nRules = m_nRule;	// 玩家吃碰杠的规则

	switch (nRequestType)
	{
	case REQUEST_TYPE_CHI:		// 吃
		nRules = (nRules & 0xFF);
		break;

	case REQUEST_TYPE_PENG:	// 碰
		nRules = ((nRules >> 8) & 0xFF);
		break;

	case REQUEST_TYPE_GANG:	// 杠
		nRules = ((nRules >> 16) & 0xFF);// && !pMe->HasCalled();
		break;

	case REQUEST_TYPE_WIN:		// 和
		nRules = ((nRules >> 24) & 0xFF);
		break;
	}

	return (0 != (nRules & (1 << nSeat)));
}

void CTKMahjongBot::FillCheckParam(CHECKPARAM * pCheckParam, int & nPaoSeat)
{
	try
	{
		if (pCheckParam == NULL)
		{ // 参数无效
			assert(FALSE);
			return;
		}

		CMahJongTile * m_pDrawTile = NULL;
		if (m_nDrawTileID != 0)
		{
			m_pDrawTile = new CMahJongTile(&m_GameRule);
			m_pDrawTile->SetID(m_nDrawTileID);
		}

		// 手中的牌
		CMahJongTile * pDrawTile = NULL;
		pCheckParam->cnHandStone = m_nHandTileCount;

		if (m_pDrawTile != NULL && m_nHandTileCount % 3 == 2)
		{ // 如果刚抓了一张牌
		  // 放在最前面，否则第一个位置空着，Obj类会处理
			m_pDrawTile->FillTileInfo(pCheckParam->asHandStone);
			pDrawTile = m_pDrawTile;
			// 设为自摸
			pCheckParam->nWinMode |= WIN_MODE_ZIMO;

			nPaoSeat = m_nMySeat;
		}
		else
		{
			nPaoSeat = m_nCurDiscardSeat;
		}

		int nTileIndex = 1; // 第一个元素放的是要和的那张牌，自已手上的牌从第二个位置开始

		//庄家天听和天胡情况
		if (m_pDrawTile == NULL && m_nHandTileCount % 3 == 2)
		{
			nTileIndex = 0;
			nPaoSeat = m_nMySeat;
		}

		for (int i = 0; i < m_nHandTileCount; i++)
		{ // 遍历手中的牌
			assert(m_apHandTile[i] != NULL);
			if (m_apHandTile[i] == pDrawTile)
			{ // 刚抓的那张牌，已放到数组里了
				continue;
			}
			m_apHandTile[i]->FillTileInfo(pCheckParam->asHandStone + nTileIndex);
			nTileIndex++;
		}

		// 吃碰杠的牌
		pCheckParam->cnShowGroups = m_nShowGroupCount;
		pCheckParam->cnMinGang = 0;
		pCheckParam->cnAnGang = 0;
		for (int i = 0; i < m_nShowGroupCount; i++)
		{ // 遍历所有的分组
		  // 统计明暗杠个数
			if (m_anShowGroupType[i] == GROUP_STYLE_MINGGANG)
			{ // 明杠
				pCheckParam->cnMinGang++;
			}
			else if (m_anShowGroupType[i] == GROUP_STYLE_ANGANG)
			{ // 暗杠
				pCheckParam->cnAnGang++;
			}

			int j = 0;
			for (j = 0; j < 4; j++)
			{ // 遍历这一组的四张牌
				if (m_apShowGroup[i][j] == NULL)
				{ // 这个位置没牌（比如吃碰的第四张牌）
					break;
				}
				m_apShowGroup[i][j]->FillTileInfo(pCheckParam->asShowGroup[i].asStone + j);
			}
			assert(j >= 3);	// 这个分组中至少是3张牌
			pCheckParam->asShowGroup[i].nGroupStyle = m_anShowGroupType[i];
		}

		// 和牌方式，暂时没有

		// 和牌所需最小番数由Obj类填充

		// 圈风门风,门风号由obj类填充
		//pCheckParam->nMenWind = (COLOR_WIND << 8) + (m_nWind << 4);

	}
	catch (...)
	{
		WRITELOG("%s : %d, Exception", __FILE__, __LINE__);
	}
}


void CTKMahjongBot::FillTileInfo(int nTileID, PMAHJONGTILE pMahJongTile)
{
	pMahJongTile->nID = nTileID;
	pMahJongTile->nColor = CMahJongTile::TileColor(nTileID);
	pMahJongTile->nWhat = CMahJongTile::TileWhat(nTileID);
}


CHECKRESULT CTKMahjongBot::CheckWin(CHECKPARAM* pCheckParam, int nPaoSeat, MahJongWinReq * pReq)
{
	try
	{
		if (nPaoSeat != m_nMySeat)
		{ // 不是自摸
			int nID = m_nCurDiscardTileID;	// 和的这张牌的ID号
			pCheckParam->asHandStone[0].nID = nID;
			pCheckParam->asHandStone[0].nColor = CMahJongTile::TileColor(nID);
			pCheckParam->asHandStone[0].nWhat = CMahJongTile::TileWhat(nID);
			pCheckParam->cnHandStone++;
		}

		WININFO sCheckWinInfo;
		CHECKRESULT eCheckWinResult = m_pJudge->CheckWin(*pCheckParam, sCheckWinInfo);
		//	WRITEDEBUGLOG( "【CMahjongClientObj::CheckWin】user(%s)验和结果:%d" , pPlayer->UserName() , m_eCheckWinResult ) ;

		// 填写和牌消息
		pReq->set_requestid(m_nRecentRequestID);
		pReq->set_seat(m_nMySeat);
		pReq->set_paoseat(nPaoSeat);
		pReq->set_tileid(pCheckParam->asHandStone[0].nID);

		if (eCheckWinResult == T_OK)
		{ // 能和
			pReq->set_resultant(sCheckWinInfo.nResultant);
			pReq->set_groups(sCheckWinInfo.cnGroups);
			for (int i = 0; i < 6; ++i)
			{
				StoneGroup * pStoneGroup = pReq->add_group();
				pStoneGroup->set_groupstyle(sCheckWinInfo.asGroup[i].nGroupStyle);
				for (int j = 0; j < 4; ++j)
				{
					Stone * pStone = pStoneGroup->add_stone();
					pStone->set_id(sCheckWinInfo.asGroup[i].asStone[j].nID);
					pStone->set_color(sCheckWinInfo.asGroup[i].asStone[j].nColor);
					pStone->set_what(sCheckWinInfo.asGroup[i].asStone[j].nWhat);
				}
			}
			for (int i = 0; i < MAX_HAND_COUNT; ++i)
			{
				Stone * pStone = pReq->add_handtile();
				pStone->set_id(sCheckWinInfo.asHandStone[i].nID);
				pStone->set_color(sCheckWinInfo.asHandStone[i].nColor);
				pStone->set_what(sCheckWinInfo.asHandStone[i].nWhat);
			}
			pReq->set_showgroups(sCheckWinInfo.cnShowGroups);
		}
		else
		{ // 诈和
			pReq->set_resultant(WIN_FAIL);
		}

		return eCheckWinResult;
	}
	catch (...)
	{
		WRITELOG("%s : %d, Exception", __FILE__, __LINE__);
		return F_NOTTING;
	}
}

CHECKRESULT CTKMahjongBot::CheckTing(CHECKPARAM* pCheckParam, MahJongCallTileReq * pReq)
{
	try
	{
		CALLINFOVECTOR sCheckCallInfo;
		CHECKRESULT eCheckCallResult = m_pJudge->CheckTing(*pCheckParam, sCheckCallInfo);

		// 填写听牌消息可听张数
		if (!sCheckCallInfo.empty()) {
			pReq->set_count(sCheckCallInfo[0].cnCallTile);
		}

		return eCheckCallResult;
	}
	catch (...)
	{
		WRITELOG("%s : %d, Exception", __FILE__, __LINE__);
		return F_NOTTING;
	}
}


// **************************************************************************************
int	CTKMahjongBot::WinMode(int nWinSeat, int nPaoSeat, int nWinTileID)
{
	int nMode = 0;

	// 杠上开花 // 抢杠
	if (m_nLastGangSeat != -1)
	{ // 和牌前确实有人开杠了
		if (m_nLastGangSeat == nWinSeat)
		{ // 是我开杠（肯定是杠开）
			assert(DRAW_TILE_TYPE_GANG == m_nCurDrawType);	// 当前抓牌类型一定是杠后抓牌
			nMode |= WIN_MODE_GANGSHANGHU;
		}
		else
		{ // 抢杠
			assert(m_nLastGangSeat == nPaoSeat);
			nMode |= WIN_MODE_QIANGGANG;
		}
	}

	// 杠上炮
	if ((nWinSeat != nPaoSeat) && (DRAW_TILE_TYPE_GANG == m_nCurDrawType))
	{ // 放炮，并且最后的抓牌类型是杠后抓牌
		nMode |= WIN_MODE_GANGPAO;
	}

	// 和绝张
	//	if( IsOnlyOne( nWinTileID, nWinSeat == nPaoSeat ) )
	//	{ // 这张牌只有一张了
	//		nMode |= WIN_MODE_HUJUEZHANG ;
	//	}

	// 海底
	//	if( m_pTileWall->Empty() )
	//	{ // 牌墙中没牌了
	//		nMode |= WIN_MODE_HAIDI ;
	//	}

	// 自摸
	if (nWinSeat == nPaoSeat)
	{ // 自摸
		nMode |= WIN_MODE_ZIMO;
	}


	// 天和// 地和 // 人和
	//if (m_abTian[nWinSeat])
	//{ // 玩家还没有出过牌（也没有吃碰杠过牌）
	//	if (m_nBanker == nWinSeat)
	//	{ // 和牌的就是庄家（天和）
	//		nMode |= WIN_MODE_TIANHU;
	//	}
	//	else if (nWinSeat == nPaoSeat)
	//	{
	//		nMode |= WIN_MODE_DIHU;
	//	}
	//	else
	//	{
	//		nMode |= WIN_MODE_RENHU;
	//	}
	//}

	//// 天听
	//if (pPlayer->IsTianCall())
	//{
	//	nMode |= WIN_MODE_TIANTING;
	//}

	//// 立直
	//if (pPlayer->HasCalled())
	//{
	//	nMode |= WIN_MODE_LIZHI;
	//}


	return nMode;
}


IJudge * CTKMahjongBot::CreateJudge( int nMahjongType)
{
	IJudge * pJudge = NULL;
	switch (nMahjongType)
	{
	case MAHJONG_TYPE_GB:
	case MAHJONG_TYPE_PUBLIC:
	case MAHJONG_TYPE_TWO: /*FOR TWO PLAYER*/
	case MAHJONG_TYPE_JSPK: /*FOR TWO PLAYER*/
		pJudge = new CPublicJudge;
		break;
	default:
		break;
	}

	if (NULL != pJudge)
	{
		pJudge->Init(&m_GameRule);
	}
	return pJudge;
}


void CTKMahjongBot::DeleteHandTile(int nTileID)
{
	try
	{
		//assert(m_nHandTileCount > 0);

		for (int i = 0; i < m_nHandTileCount; i++)
		{ // 遍历手中的牌
			if (m_apHandTile[i]->IDIs(nTileID))
			{ // 就是这张牌
				// 把后面的牌移到这里来
				if (i != m_nHandTileCount - 1)
				{ // 不是最后一张
					m_apHandTile[i]->SetID(m_apHandTile[m_nHandTileCount - 1]->ID());
					//memmove( m_apHandTile + i , m_apHandTile + i + 1 , ( m_nHandTileCount - 1 - i ) * sizeof( CMahJongTile* ) ) ;
				}
				m_nHandTileCount--;
				return;
			}
		}

		//assert(FALSE);
	}
	catch (...)
	{
		WRITELOG("%s : %d, Exception", __FILE__, __LINE__);
	}
}



void CTKMahjongBot::AddFlowerTile(int nTileID)
{
	try
	{
		//assert(m_cnFlowerTile < MAX_FLOWER_COUNT - 1);

		m_anFlowerTile[m_cnFlowerTile++] = nTileID;
	}
	catch (...)
	{
		WRITELOG("%s : %d, Exception", __FILE__, __LINE__);
	}
}




// ************************************************************************************************************************
// 
// 
// 
// ************************************************************************************************************************
void CTKMahjongBot::TraceHandTile(TCHAR * szTip)
{
	char szText[1024 * 4] = { 0 };
	char szTemp[64] = { 0 };

	if (NULL != szTip)
	{
		_tcscpy_s(szText, szTip);
	}

	// 名字
	sprintf_s(szTemp, "%d ", m_dwMyUserID);
	strcat_s(szText, szTemp);

	// 手中牌
	strcat_s(szText, "手中牌:");
	for (int i = 0; i < m_nHandTileCount; i++)
	{ // 遍历手中牌
		sprintf_s(szTemp, "%s,", m_apHandTile[i]->NameID());
		strcat_s(szText, szTemp);
	}
	strcat_s(szText, " ");

	WRITEDEBUGLOG(TEXT("%s"), szText);
}
void CTKMahjongBot::_sendReqMsg(const TKMobileReqMsg& rMsg)
{
#ifdef _DEBUG
	TKWriteLog("_sendReqMsg: %s", rMsg.DebugString().c_str());
#endif
	
	try
	{
		// 序列化
		std::string szSerializeMsg;
		if (!rMsg.SerializeToString(&szSerializeMsg))
		{
	
			assert(false);
		}

		// 发送
		BYTE* pszPack = new BYTE[sizeof(TKHEADER) + rMsg.ByteSize()];    // 分配空间
		ZeroMemory(pszPack, sizeof(TKHEADER) + rMsg.ByteSize());

		((PTKHEADER)pszPack)->dwType = TK_REQ | (TK_MSG_MAHJONG_PROTOBUF);
		((PTKHEADER)pszPack)->dwLength = rMsg.ByteSize();   // size
		BYTE* pData = pszPack + sizeof(TKHEADER);
		memcpy(pData, szSerializeMsg.c_str(), rMsg.ByteSize());

		SendMsg((PTKHEADER)pszPack);
		delete[] pszPack;
		pszPack = NULL;
	}
	catch (...)
	{
		WRITELOG("%s : %d, Exception", __FILE__, __LINE__);
	}
}
void CTKMahjongBot::_onPbMsg(PTKHEADER pMsg)
{
	size_t szLen = pMsg->dwLength + sizeof(TKHEADER);
	std::string szSerializeData((BYTE*)pMsg + sizeof(TKHEADER), (BYTE*)pMsg + szLen);

	TKMobileAckMsg mobileAckMsg;
	mobileAckMsg.ParseFromString(szSerializeData);  // 反序列化
	const MahJongJSPKAckMsg& ackMsg = mobileAckMsg.mahjongjspk_ack_msg();

	if (ackMsg.has_mahjongplace_ack_msg())
	{
		OnReqPlace(ackMsg.mahjongplace_ack_msg());
	}
	if (ackMsg.has_mahjongjspkspecialtile_ack_msg())
	{
		OnReqSpecialTile(ackMsg.mahjongjspkspecialtile_ack_msg());
	}
	if (ackMsg.has_mahjongwindplace_ack_msg())
	{
		OnReqWindPlace(ackMsg.mahjongwindplace_ack_msg());
	}

	if (ackMsg.has_mahjongopendoor_ack_msg())
	{
		OnReqOpenDoor(ackMsg.mahjongopendoor_ack_msg());
	}

	if (ackMsg.has_mahjonghun_ack_msg())
	{
		OnReqHun(ackMsg.mahjonghun_ack_msg());
	}

	if (ackMsg.has_mahjongaction_ack_msg())
	{
		OnActionAck(ackMsg.mahjongaction_ack_msg());
	}

	if (ackMsg.has_mahjongchangeflower_ack_msg())
	{
		OnReqChangeFlower(ackMsg.mahjongchangeflower_ack_msg());
	}

	if (ackMsg.has_mahjongdiscardtile_ack_msg())
	{
		OnReqDiscardTile(ackMsg.mahjongdiscardtile_ack_msg());
	}

	if (ackMsg.has_mahjongdrawtile_ack_msg())
	{
		OnReqDrawTile(ackMsg.mahjongdrawtile_ack_msg());
	}
	if (ackMsg.has_mahjongcall_ack_msg())
	{
		OnReqCall(ackMsg.mahjongcall_ack_msg());
	}
	if (ackMsg.has_mahjongchi_ack_msg())
	{
		OnReqChi(ackMsg.mahjongchi_ack_msg());
	}
	if (ackMsg.has_mahjongpeng_ack_msg())
	{
		OnReqPeng(ackMsg.mahjongpeng_ack_msg());
	}
	if (ackMsg.has_mahjonggang_ack_msg())
	{
		OnReqGang(ackMsg.mahjonggang_ack_msg());
	}
	if (ackMsg.has_mahjongtrustplay_ack_msg())
	{
		OnAckTrustPlay(pMsg);
	}
}
