#ifndef _LOG_FILE_OBJ_H
#define _LOG_FILE_OBJ_H


#define   WIN32_LEAN_AND_MEAN   
#include <windows.h>
#include <stdio.h>

#pragma warning (disable: 4786)

#include <list>
using namespace std ;


// ************************************************************************************************************************
// 类的声明
// 

// 
// 日志文件对象类
// 
class CLogFileObj
{
private :
	TCHAR m_szLogFileName[ MAX_PATH ] ;	// 日志文件名
	FILE* m_pLogFile ;					// 日志文件句柄
	
	CRITICAL_SECTION m_CriticalSection ;	// 记录日志的临界区
	
public :
	// 构造函数
	CLogFileObj( LPCTSTR pcszLogFileName = NULL ) ;
	// 析构函数
	~CLogFileObj() ;

	// 开关
	void EnableLog( BOOL bEnable = TRUE );
	
	// 输出函数
	BOOL WriteLog( LPCTSTR pcszFormat , ... ) ;
	
	// 输出函数
	BOOL WriteMemLog( LPCTSTR pcszTitle , LPVOID pvData , INT nSize ) ;
	
	// 修改缩近的大小
	VOID OffsetIndent( int nOffset ) { m_nIndent += nOffset ; }

protected :
	// 把一行文本写入文件
	BOOL WriteTextToFile( LPCTSTR pcszText ) ;
	
protected :
	// 记录一个文本的日志信息
	virtual BOOL DoWriteLog( LPCTSTR pcszText ) ;

private :
	// 写一行内存日志信息
	VOID WriteMemOneLine( PBYTE pByte , int nBytes , LPCTSTR szIndent ) ;

	// 进入记录日志的临界区
	void EnterLogCriticalSection() { EnterCriticalSection( &m_CriticalSection ) ; }
	// 离开记录日志的临界区
	void LeaveLogCriticalSection() { LeaveCriticalSection( &m_CriticalSection ) ; }
	
private :
	static int m_nIndent ;	// 缩近的字符数
	BOOL m_bEnableLog;
} ;

// 
// 用于写块日志（带缩近）的对象类
// 
class CBlockLogObj : public CLogFileObj
{
private :
	DWORD m_dwTickStart ;	// 开始记录的时间（GetTickCount值）

public :
	// 构造函数
	CBlockLogObj( LPCTSTR lpcszBlockNameFormat , ... ) ;
	// 析构函数
	~CBlockLogObj() ;
} ;

// 
// 用于暂时把日志信息保存下来的对象类
// 
class CListLogObj : public CLogFileObj
{
private :
	list<LPCTSTR> m_LogList ;	// 日志列表（列表中的元素都是字符串）
	typedef list<LPCTSTR>::const_iterator LogListIterator ;

	CRITICAL_SECTION m_csListLog ;	// 临界区对象，用于遍历列表中元素时提供线程保护

public :
	// 构造函数
	CListLogObj() ;
	// 析构函数
	virtual ~CListLogObj() ;

	// 重置日志列表
	void ResetLogList() ;
    void ResetLogListExe();

	// 把保存的日志列表信息写入文件
	BOOL WriteLogListToFile( LPCTSTR pcszFormat , ... ) ;
    void WriteLogListToFileExe(LPCTSTR pcszFormat , ... );

protected :
	// 记录一个文本的日志信息
	virtual BOOL DoWriteLog( LPCTSTR pcszText ) ;
} ;

// 
// 类的声明
// ************************************************************************************************************************






// ************************************************************************************************************************
// 引用的全局变量
//

extern CLogFileObj __objLogFile__ ;	// 全局的日志文件对象
extern CListLogObj __objLogList__ ;	// 全局的日志列表对象

//
// 引用的全局变量
// ************************************************************************************************************************





// ************************************************************************************************************************
// 宏定义
// 

#define ENABLELOG			__objLogFile__.EnableLog
#define WRITELOG				__objLogFile__.WriteLog
#define WRITEMEMLOG		__objLogFile__.WriteMemLog
#define WRITEBLOCKLOG		CBlockLogObj __objBlockLog__
#define ADDLISTLOG			__objLogList__.WriteLog
#define ADDMEMLISTLOG	__objLogList__.WriteMemLog
#define RESETLISTLOG		__objLogList__.ResetLogList
#define WRITELISTLOG		__objLogList__.WriteLogListToFile

#if defined(DEBUG)||defined(_DEBUG)
	#define WRITEDEBUGLOG			WRITELOG
	#define WRITEDEBUGMEMLOG		WRITEMEMLOG
	#define WRITEDEBUGBLOCKLOG		WRITEBLOCKLOG
	#define ADDDEBUGLISTLOG			ADDLISTLOG
	#define ADDDEBUGMEMLISTLOG		ADDMEMLISTLOG
#else
	#if defined(_MSC_VER)&&(_MSC_VER>1300)	// MS.NET
		#define WRITEDEBUGLOG			__noop
		#define WRITEDEBUGMEMLOG		__noop
		#define WRITEDEBUGBLOCKLOG		__noop
		#define ADDDEBUGLISTLOG			__noop
		#define ADDDEBUGMEMLISTLOG		__noop
	#else									// MS6 or other compiler
		#define WRITEDEBUGLOG			((void)0)
		#define WRITEDEBUGMEMLOG		((void)0)
		#define WRITEDEBUGBLOCKLOG		((void)0)
		#define ADDDEBUGLISTLOG			((void)0)
		#define ADDDEBUGMEMLISTLOG		((void)0)
	#endif
#endif

// 
// 宏定义
// ************************************************************************************************************************


#endif
