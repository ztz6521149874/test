#ifndef HTTPCLIENT_H
#define HTTPCLIENT_H

#include <string>
using namespace std;

// 操作成功
#define SUCCESS        0
// 操作失败
#define FAILURE        1
// 操作超时 www.it165.net
#define OUTTIME        2

int HttpPost(const string& strUrl, const string& strPostData, string &strResponse);

#endif // HTTPCLIENT_H


